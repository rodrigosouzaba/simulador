<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Produtos
 * @property \Cake\ORM\Association\BelongsToMany $Regioes
 *
 * @method \App\Model\Entity\Tabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tabela findOrCreate($search, callable $callback = null)
 */
class TabelasTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Produtos', [
            'foreignKey' => 'produto_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Operadoras', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Ramos', [
            'foreignKey' => 'ramo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Modalidades', [
            'foreignKey' => 'modalidade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Regioes', [
            'foreignKey' => 'regiao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Abrangencias', [
            'foreignKey' => 'abrangencia_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tipos', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('Carencias', [
            'foreignKey' => 'carencia_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Informacoes', [
            'foreignKey' => 'informacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Observacoes', [
            'foreignKey' => 'observacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Opcionais', [
            'foreignKey' => 'opcional_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Redes', [
            'foreignKey' => 'rede_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Reembolsos', [
            'foreignKey' => 'reembolso_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('nome', 'create')
                ->notEmpty('nome');

        $validator
                ->date('vigencia')
                ->requirePresence('vigencia', 'create')
                ->notEmpty('vigencia');

      
        $validator
                ->add('faixa1', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa2', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa3', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa4', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa5', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa6', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa7', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa8', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa9', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa10', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['produto_id'], 'Produtos'));
        $rules->add($rules->existsIn(['ramo_id'], 'Ramos'));
        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));
        $rules->add($rules->existsIn(['tipo_id'], 'Tipos'));
        $rules->add($rules->existsIn(['modalidade_id'], 'Modalidades'));
        $rules->add($rules->existsIn(['abrangencia_id'], 'Abrangencias'));
        $rules->add($rules->existsIn(['regiao_id'], 'Regioes'));
        
        
        $rules->add($rules->existsIn(['carencia_id'], 'Carencias'));
        $rules->add($rules->existsIn(['informacao_id'], 'Informacoes'));
        $rules->add($rules->existsIn(['observacao_id'], 'Observacoes'));
        $rules->add($rules->existsIn(['opcional_id'], 'Opcionais'));
        $rules->add($rules->existsIn(['rede_id'], 'Redes'));
        $rules->add($rules->existsIn(['reembolso_id'], 'Reembolsos'));
        return $rules;
    }

}
