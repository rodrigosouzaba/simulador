<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Regioes Controller
 *
 * @property \App\Model\Table\RegioesTable $Regioes
 */
class RegioesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Estados']
        ];
        $regioes = $this->paginate($this->Regioes);

        $this->set(compact('regioes'));
        $this->set('_serialize', ['regioes']);
    }

    /**
     * View method
     *
     * @param string|null $id Regio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $regio = $this->Regioes->get($id, [
            'contain' => ['Estados', 'Tabelas']
        ]);

        $this->set('regio', $regio);
        $this->set('_serialize', ['regio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $regio = $this->Regioes->newEntity();
        if ($this->request->is('post')) {
            $regio = $this->Regioes->patchEntity($regio, $this->request->data);
            if ($this->Regioes->save($regio)) {
                $this->Flash->success(__('The regio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The regio could not be saved. Please, try again.'));
            }
        }
        $estados = $this->Regioes->Estados->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $tabelas = $this->Regioes->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('regio', 'estados', 'tabelas'));
        $this->set('_serialize', ['regio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Regio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $regio = $this->Regioes->get($id, [
            'contain' => ['Tabelas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $regio = $this->Regioes->patchEntity($regio, $this->request->data);
            if ($this->Regioes->save($regio)) {
                $this->Flash->success(__('The regio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The regio could not be saved. Please, try again.'));
            }
        }
        $estados = $this->Regioes->Estados->find('list', ['limit' => 200]);
        $tabelas = $this->Regioes->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('regio', 'estados', 'tabelas'));
        $this->set('_serialize', ['regio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Regio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $regio = $this->Regioes->get($id);
        if ($this->Regioes->delete($regio)) {
            $this->Flash->success(__('The regio has been deleted.'));
        } else {
            $this->Flash->error(__('The regio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
