<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Redes Model
 *
 * @property \Cake\ORM\Association\HasMany $Tabelas
 *
 * @method \App\Model\Entity\Rede get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rede newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rede[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rede|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rede patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rede[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rede findOrCreate($search, callable $callback = null)
 */
class RedesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('redes');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsTo('Operadoras', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER'
        ]);


        $this->hasMany('Tabelas', [
            'foreignKey' => 'rede_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('descricao', 'create')
                ->notEmpty('descricao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));

        return $rules;
    }

}
