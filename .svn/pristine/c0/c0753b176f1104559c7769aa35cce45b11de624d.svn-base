<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Reembolsos Controller
 *
 * @property \App\Model\Table\ReembolsosTable $Reembolsos
 */
class ReembolsosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->set('title', 'Reembolsos');

        $reembolsos = $this->paginate($this->Reembolsos);

        $this->set(compact('reembolsos'));
        $this->set('_serialize', ['reembolsos']);
    }

    /**
     * View method
     *
     * @param string|null $id Reembolso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $reembolso = $this->Reembolsos->get($id, [
            'contain' => ['Tabelas']
        ]);

        $this->set('reembolso', $reembolso);
        $this->set('_serialize', ['reembolso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
         $this->set('title', 'Novo Reembolso');
        $reembolso = $this->Reembolsos->newEntity();
        if ($this->request->is('post')) {
            $reembolso = $this->Reembolsos->patchEntity($reembolso, $this->request->data);
            if ($this->Reembolsos->save($reembolso)) {
                $this->Flash->success(__('Salvo com Sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente'));
            }
        }
        $this->set(compact('reembolso'));
        $this->set('_serialize', ['reembolso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Reembolso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $this->set('title', 'Editar Reembolso');
        $reembolso = $this->Reembolsos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reembolso = $this->Reembolsos->patchEntity($reembolso, $this->request->data);
            if ($this->Reembolsos->save($reembolso)) {
                $this->Flash->success(__('Alterado com Sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao alterar. Tente Novamente'));
            }
        }
        $this->set(compact('reembolso'));
        $this->set('_serialize', ['reembolso']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Reembolso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $reembolso = $this->Reembolsos->get($id);
        if ($this->Reembolsos->delete($reembolso)) {
            $this->Flash->success(__('Excluído com Sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao Excluir. Tente Novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
