-- Create syntax for 'grupos'

CREATE TABLE `grupos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) DEFAULT '0',
  `grupo_pai_id` int(11) unsigned DEFAULT NULL,
  `created` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `grupo_pai_id` (`grupo_pai_id`),
  CONSTRAINT `grupos_ibfk_1` FOREIGN KEY (`grupo_pai_id`) REFERENCES `grupos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
