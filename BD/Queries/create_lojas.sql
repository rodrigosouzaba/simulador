CREATE TABLE `lojas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ramo_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `descricao` text,
  `created` datetime NOT NULL,
  `status` varchar(5) DEFAULT '0',
  `url` text NOT NULL,
  `tipo_pessoa` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ramo_id` (`ramo_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `lojas_ibfk_1` FOREIGN KEY (`ramo_id`) REFERENCES `ramos` (`id`),
  CONSTRAINT `lojas_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;