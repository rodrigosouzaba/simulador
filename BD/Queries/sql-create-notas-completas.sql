CREATE TABLE `notas_completas` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created` DATETIME NULL DEFAULT NULL,
	`caminho` VARCHAR(255) NULL DEFAULT NULL,
	`nome_arquivo` VARCHAR(255) NULL DEFAULT NULL,
	`user_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `user - id -> notas_completas - user_id` (`user_id`),
	CONSTRAINT `user - id -> notas_completas - user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3
;
