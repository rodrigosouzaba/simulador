CREATE TABLE `notas_unicas` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`nota_completa_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`created` DATETIME NULL DEFAULT NULL,
	`caminho` VARCHAR(50) NULL DEFAULT 'uploads\\notas\\notas_unicas',
	`nome_arquivo` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK__notas_completas` (`nota_completa_id`),
	CONSTRAINT `FK__notas_completas` FOREIGN KEY (`nota_completa_id`) REFERENCES `notas_completas` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=9
;
