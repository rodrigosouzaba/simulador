CREATE TABLE `sessions` (
    `id` CHAR(40) NOT NULL COLLATE 'ascii_bin',
    `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
    `modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `data` BLOB NULL,
    `expires` INT(10) UNSIGNED NULL DEFAULT NULL,
    `user_id` INT(10) UNSIGNED NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    INDEX `FK_sessions_users` (`user_id`),
    CONSTRAINT `FK_sessions_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;