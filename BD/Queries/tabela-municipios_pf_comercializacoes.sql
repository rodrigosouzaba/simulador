-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.24 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para simulador
CREATE DATABASE IF NOT EXISTS `simulador` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `simulador`;

-- Copiando estrutura para tabela simulador.municipios_pf_comercializacoes
CREATE TABLE IF NOT EXISTS `municipios_pf_comercializacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pf_comercializacao_id` int(10) unsigned NOT NULL,
  `municipio_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pf_comercializacoes_municipios_pf_comercializacoes` (`pf_comercializacao_id`),
  KEY `FK_pf_comercializacoes_municipios_municipios` (`municipio_id`),
  CONSTRAINT `FK_pf_comercializacoes_municipios_municipios` FOREIGN KEY (`municipio_id`) REFERENCES `municipios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_pf_comercializacoes_municipios_pf_comercializacoes` FOREIGN KEY (`pf_comercializacao_id`) REFERENCES `pf_comercializacoes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela simulador.municipios_pf_comercializacoes: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `municipios_pf_comercializacoes` DISABLE KEYS */;
INSERT IGNORE INTO `municipios_pf_comercializacoes` (`id`, `pf_comercializacao_id`, `municipio_id`) VALUES
	(1, 55, 1898),
	(2, 55, 1904),
	(3, 55, 1932),
	(4, 55, 2080),
	(5, 55, 2162),
	(6, 55, 2163),
	(7, 55, 2232);
/*!40000 ALTER TABLE `municipios_pf_comercializacoes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
