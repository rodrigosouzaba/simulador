CREATE TABLE `sms_situacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `sms_cabecalhos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `sms_listas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `caminho` varchar(400) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `sms_envios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `mensagem` text NOT NULL,
  `sms_lista_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `sms_situacao_id` int(11) unsigned DEFAULT NULL,
  `data_envio` datetime DEFAULT NULL,
  `data_agendamento` datetime DEFAULT NULL,
  `autorizado` int(11) DEFAULT NULL,
  `descricao` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_lista_id` (`sms_lista_id`),
  KEY `sms_situacao_id` (`sms_situacao_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sms_envios_ibfk_1` FOREIGN KEY (`sms_lista_id`) REFERENCES `sms_listas` (`id`),
  CONSTRAINT `sms_envios_ibfk_2` FOREIGN KEY (`sms_situacao_id`) REFERENCES `sms_situacoes` (`id`),
  CONSTRAINT `sms_envios_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `sms_respostas_envios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sms_envio_id` int(11) unsigned DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `resultado` varchar(200) DEFAULT NULL,
  `porta_chipeira` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_envio_id` (`sms_envio_id`),
  CONSTRAINT `respostas_envios_ibfk_1` FOREIGN KEY (`sms_envio_id`) REFERENCES `sms_envios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;