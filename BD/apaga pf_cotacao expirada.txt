CREATE DEFINER=`root`@`localhost` EVENT `DEL_PF_FILTROS_EXP`
	ON SCHEDULE
		EVERY 1 SECOND STARTS '2019-06-03 17:26:28'
	ON COMPLETION NOT PRESERVE
	ENABLE
	COMMENT ''
	DO BEGIN
DELETE from pf_filtros where date(now()) > data_expiracao;
END