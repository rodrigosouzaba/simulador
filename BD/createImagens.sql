# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.34)
# Base de Dados: simulacoes
# Tempo de Geração: 2017-02-13 18:48:02 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela imagens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `imagens`;

CREATE TABLE `imagens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caminho` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `imagens` WRITE;
/*!40000 ALTER TABLE `imagens` DISABLE KEYS */;

INSERT INTO `imagens` (`id`, `nome`, `caminho`, `created`, `modified`, `status`)
VALUES
	(3,'amil.png','uploads/imagens/','2017-02-10 17:03:50','2017-02-10 17:03:50',1),
	(4,'amil.png','uploads/imagens/','2017-02-10 17:05:13','2017-02-10 17:05:13',1),
	(5,'amil.png','uploads/imagens/','2017-02-10 17:10:51','2017-02-10 17:10:51',1),
	(6,'amil.png','uploads/imagens/','2017-02-10 17:11:27','2017-02-10 17:11:27',1),
	(7,'amil.png','uploads/imagens/','2017-02-10 17:12:23','2017-02-10 17:12:23',1),
	(8,'amil.png','uploads/imagens/','2017-02-10 17:13:57','2017-02-10 17:13:57',1),
	(9,'Bradesco-saude-Logo.png','uploads/imagens/','2017-02-13 14:39:44','2017-02-13 14:39:44',1);

/*!40000 ALTER TABLE `imagens` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
