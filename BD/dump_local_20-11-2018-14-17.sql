# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Base de Dados: plataforma
# Tempo de Geração: 2018-11-20 17:17:12 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela abrangencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `abrangencias`;

CREATE TABLE `abrangencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;



# Dump da tabela agendamentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `agendamentos`;

CREATE TABLE `agendamentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data_alteracao` datetime NOT NULL,
  `percentual` float NOT NULL,
  `nova_vigencia` date NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `situacao` varchar(100) NOT NULL DEFAULT 'PENDENTE',
  `operadora_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `agendamentos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `agendamentos_ibfk_2` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;



# Dump da tabela agendamentos_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `agendamentos_tabelas`;

CREATE TABLE `agendamentos_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `agendamento_id` int(11) unsigned NOT NULL,
  `tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agendamento_id` (`agendamento_id`),
  KEY `tabela_id` (`tabela_id`),
  CONSTRAINT `agendamentos_tabelas_ibfk_1` FOREIGN KEY (`agendamento_id`) REFERENCES `agendamentos` (`id`),
  CONSTRAINT `agendamentos_tabelas_ibfk_2` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;



# Dump da tabela alertas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `alertas`;

CREATE TABLE `alertas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` date NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `model` varchar(200) NOT NULL DEFAULT '',
  `data_alerta` date DEFAULT NULL,
  `mensagem` text,
  `simulacao_id` int(11) unsigned DEFAULT NULL,
  `pf_calculo_id` int(11) unsigned DEFAULT NULL,
  `odonto_calculo_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `simulacao_id` (`simulacao_id`),
  KEY `pf_calculo_id` (`pf_calculo_id`),
  KEY `odonto_calculo_id` (`odonto_calculo_id`),
  CONSTRAINT `alertas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `alertas_ibfk_2` FOREIGN KEY (`simulacao_id`) REFERENCES `simulacoes` (`id`),
  CONSTRAINT `alertas_ibfk_3` FOREIGN KEY (`pf_calculo_id`) REFERENCES `pf_calculos` (`id`),
  CONSTRAINT `alertas_ibfk_4` FOREIGN KEY (`odonto_calculo_id`) REFERENCES `odonto_calculos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump da tabela arquivos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `arquivos`;

CREATE TABLE `arquivos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(300) NOT NULL DEFAULT '',
  `caminho` varchar(300) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `tipo` varchar(50) DEFAULT '',
  `exibicao_nome` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;



# Dump da tabela campanhas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `campanhas`;

CREATE TABLE `campanhas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_campanha` int(11) DEFAULT NULL,
  `nome` varchar(300) DEFAULT NULL,
  `url` varchar(400) DEFAULT NULL,
  `descricao` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;



# Dump da tabela carencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carencias`;

CREATE TABLE `carencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `carencias_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;



# Dump da tabela cnpjs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cnpjs`;

CREATE TABLE `cnpjs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



# Dump da tabela detalhes_perfis_empresariais
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalhes_perfis_empresariais`;

CREATE TABLE `detalhes_perfis_empresariais` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(40) NOT NULL DEFAULT '',
  `sexo` char(1) NOT NULL DEFAULT '',
  `data_nascimento` varchar(60) NOT NULL DEFAULT '',
  `idade` int(11) NOT NULL,
  `faixa` int(11) NOT NULL,
  `perfil_empresarial_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `perfil_empresarial_id` (`perfil_empresarial_id`),
  CONSTRAINT `detalhes_perfis_empresariais_ibfk_1` FOREIGN KEY (`perfil_empresarial_id`) REFERENCES `perfis_empresariais` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1411 DEFAULT CHARSET=latin1;



# Dump da tabela envios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `envios`;

CREATE TABLE `envios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `mensagem` text NOT NULL,
  `sms_lista_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `sms_situacao_id` int(11) unsigned DEFAULT NULL,
  `data_envio` datetime DEFAULT NULL,
  `data_agendamento` datetime DEFAULT NULL,
  `autorizado` int(11) DEFAULT NULL,
  `descricao` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_lista_id` (`sms_lista_id`),
  KEY `sms_situacao_id` (`sms_situacao_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `envios_ibfk_1` FOREIGN KEY (`sms_lista_id`) REFERENCES `sms_listas` (`id`),
  CONSTRAINT `envios_ibfk_2` FOREIGN KEY (`sms_situacao_id`) REFERENCES `sms_situacoes` (`id`),
  CONSTRAINT `envios_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump da tabela estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `estados`;

CREATE TABLE `estados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;



# Dump da tabela filtros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filtros`;

CREATE TABLE `filtros` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `simulacao_id` int(11) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `abrangencia_id` int(11) unsigned DEFAULT NULL,
  `tipo_id` int(10) unsigned DEFAULT NULL,
  `tipo_produto_id` int(11) unsigned DEFAULT NULL,
  `coparticipacao` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `informacao` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `reembolso` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `carencia` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `rede` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `observacao` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `simulacao_id` (`simulacao_id`),
  KEY `observacao_id` (`observacao`),
  KEY `informacao_id` (`informacao`),
  KEY `tipo_produto_id` (`tipo_produto_id`),
  KEY `abrangencia_id` (`abrangencia_id`),
  KEY `user_id` (`user_id`),
  KEY `tipo_id` (`tipo_id`),
  CONSTRAINT `filtros_ibfk_1` FOREIGN KEY (`simulacao_id`) REFERENCES `simulacoes` (`id`),
  CONSTRAINT `filtros_ibfk_2` FOREIGN KEY (`tipo_produto_id`) REFERENCES `tipos_produtos` (`id`),
  CONSTRAINT `filtros_ibfk_4` FOREIGN KEY (`abrangencia_id`) REFERENCES `abrangencias` (`id`),
  CONSTRAINT `filtros_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `filtros_ibfk_6` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12198 DEFAULT CHARSET=latin1;



# Dump da tabela formas_pagamentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `formas_pagamentos`;

CREATE TABLE `formas_pagamentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `formas_pagamentos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;



# Dump da tabela imagens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `imagens`;

CREATE TABLE `imagens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caminho` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=587 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump da tabela indicados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `indicados`;

CREATE TABLE `indicados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(300) NOT NULL DEFAULT '',
  `celular` varchar(20) DEFAULT '',
  `email` varchar(400) DEFAULT '',
  `created` datetime NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `situacao` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `indicados_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;



# Dump da tabela informacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `informacoes`;

CREATE TABLE `informacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(155) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `informacoes_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;



# Dump da tabela modalidades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modalidades`;

CREATE TABLE `modalidades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;



# Dump da tabela observacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `observacoes`;

CREATE TABLE `observacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `observacoes_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_atendimentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_atendimentos`;

CREATE TABLE `odonto_atendimentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_calculos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_calculos`;

CREATE TABLE `odonto_calculos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `contato` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT '',
  `telefone` varchar(13) DEFAULT NULL,
  `quantidade_vidas` int(11) NOT NULL,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned DEFAULT NULL,
  `calculoremoto` varchar(1) DEFAULT NULL,
  `tipo_pessoa` varchar(10) NOT NULL DEFAULT '',
  `status_id` int(10) unsigned DEFAULT NULL,
  `estado_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `status_id` (`status_id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `odonto_calculos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `odonto_calculos_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  CONSTRAINT `odonto_calculos_ibfk_3` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=970 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_calculos_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_calculos_tabelas`;

CREATE TABLE `odonto_calculos_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `odonto_calculo_id` int(11) unsigned DEFAULT NULL,
  `odonto_tabela_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_calculo_id` (`odonto_calculo_id`),
  KEY `odonto_tabela_id` (`odonto_tabela_id`),
  CONSTRAINT `odonto_calculos_tabelas_ibfk_1` FOREIGN KEY (`odonto_calculo_id`) REFERENCES `odonto_calculos` (`id`),
  CONSTRAINT `odonto_calculos_tabelas_ibfk_2` FOREIGN KEY (`odonto_tabela_id`) REFERENCES `odonto_tabelas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33357 DEFAULT CHARSET=latin1;



# Dump da tabela odonto_carencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_carencias`;

CREATE TABLE `odonto_carencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL,
  `odonto_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_operadora_id` (`odonto_operadora_id`),
  CONSTRAINT `odonto_carencias_ibfk_1` FOREIGN KEY (`odonto_operadora_id`) REFERENCES `odonto_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_comercializacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_comercializacoes`;

CREATE TABLE `odonto_comercializacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  `odonto_operadora_id` int(11) unsigned DEFAULT NULL,
  `descricao` text,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  KEY `odonto_operadora_id` (`odonto_operadora_id`),
  CONSTRAINT `odonto_comercializacoes_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `odonto_comercializacoes_ibfk_2` FOREIGN KEY (`odonto_operadora_id`) REFERENCES `odonto_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_dependentes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_dependentes`;

CREATE TABLE `odonto_dependentes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL,
  `odonto_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_operadora_id` (`odonto_operadora_id`),
  CONSTRAINT `odonto_dependentes_ibfk_1` FOREIGN KEY (`odonto_operadora_id`) REFERENCES `odonto_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_documentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_documentos`;

CREATE TABLE `odonto_documentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL,
  `odonto_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_operadora_id` (`odonto_operadora_id`),
  CONSTRAINT `odonto_documentos_ibfk_1` FOREIGN KEY (`odonto_operadora_id`) REFERENCES `odonto_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_filtros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_filtros`;

CREATE TABLE `odonto_filtros` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `odonto_calculo_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `odonto_carencia` int(11) DEFAULT NULL,
  `odonto_reembolso` int(11) DEFAULT NULL,
  `odonto_rede` int(11) DEFAULT NULL,
  `odonto_documento` int(11) DEFAULT NULL,
  `odonto_dependente` int(11) DEFAULT NULL,
  `odonto_observacao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_calculo_id` (`odonto_calculo_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `odonto_filtros_ibfk_1` FOREIGN KEY (`odonto_calculo_id`) REFERENCES `odonto_calculos` (`id`),
  CONSTRAINT `odonto_filtros_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=413 DEFAULT CHARSET=latin1;



# Dump da tabela odonto_filtros_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_filtros_tabelas`;

CREATE TABLE `odonto_filtros_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `odonto_filtro_id` int(11) unsigned NOT NULL,
  `odonto_tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_filtro_id` (`odonto_filtro_id`),
  KEY `odonto_tabela_id` (`odonto_tabela_id`),
  CONSTRAINT `odonto_filtros_tabelas_ibfk_1` FOREIGN KEY (`odonto_filtro_id`) REFERENCES `odonto_filtros` (`id`),
  CONSTRAINT `odonto_filtros_tabelas_ibfk_2` FOREIGN KEY (`odonto_tabela_id`) REFERENCES `odonto_tabelas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4314 DEFAULT CHARSET=latin1;



# Dump da tabela odonto_formas_pagamentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_formas_pagamentos`;

CREATE TABLE `odonto_formas_pagamentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `odonto_operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_observacaos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_observacaos`;

CREATE TABLE `odonto_observacaos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL,
  `odonto_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_operadora_id` (`odonto_operadora_id`),
  CONSTRAINT `odonto_obbservacaos_ibfk_1` FOREIGN KEY (`odonto_operadora_id`) REFERENCES `odonto_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_operadoras`;

CREATE TABLE `odonto_operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `imagem_id` int(11) unsigned DEFAULT NULL,
  `detalhe` varchar(300) DEFAULT NULL,
  `prioridade` float DEFAULT '0',
  `url` varchar(400) DEFAULT NULL,
  `url_rede` varchar(400) DEFAULT NULL,
  `estado_id` int(10) unsigned DEFAULT NULL,
  `tipo_pessoa` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `imagem_id` (`imagem_id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `odonto_operadoras_ibfk_1` FOREIGN KEY (`imagem_id`) REFERENCES `imagens` (`id`),
  CONSTRAINT `odonto_operadoras_ibfk_2` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_operadoras_fechadas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_operadoras_fechadas`;

CREATE TABLE `odonto_operadoras_fechadas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;



# Dump da tabela odonto_operadoras_fechadas_arquivos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_operadoras_fechadas_arquivos`;

CREATE TABLE `odonto_operadoras_fechadas_arquivos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `odonto_operadora_fechada_id` int(11) unsigned NOT NULL,
  `arquivo_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_operadora_fechada_id` (`odonto_operadora_fechada_id`),
  KEY `arquivo_id` (`arquivo_id`),
  CONSTRAINT `odonto_operadoras_fechadas_arquivos_ibfk_1` FOREIGN KEY (`odonto_operadora_fechada_id`) REFERENCES `odonto_operadoras_fechadas` (`id`),
  CONSTRAINT `odonto_operadoras_fechadas_arquivos_ibfk_2` FOREIGN KEY (`arquivo_id`) REFERENCES `arquivos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump da tabela odonto_produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_produtos`;

CREATE TABLE `odonto_produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `odonto_operadora_id` int(11) unsigned NOT NULL,
  `odonto_carencia_id` int(11) unsigned DEFAULT NULL,
  `odonto_rede_id` int(11) unsigned DEFAULT NULL,
  `odonto_reembolso_id` int(11) unsigned DEFAULT NULL,
  `odonto_documento_id` int(11) unsigned DEFAULT NULL,
  `odonto_dependente_id` int(11) unsigned DEFAULT NULL,
  `odonto_observacao_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_operadora_id` (`odonto_operadora_id`),
  KEY `carencia_id` (`odonto_carencia_id`),
  KEY `rede_id` (`odonto_rede_id`),
  KEY `reembolso_id` (`odonto_reembolso_id`),
  KEY `opcional_id` (`odonto_documento_id`),
  KEY `informacao_id` (`odonto_dependente_id`),
  KEY `observacao_id` (`odonto_observacao_id`),
  CONSTRAINT `odonto_produtos_ibfk_1` FOREIGN KEY (`odonto_operadora_id`) REFERENCES `odonto_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_redes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_redes`;

CREATE TABLE `odonto_redes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL,
  `odonto_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_operadora_id` (`odonto_operadora_id`),
  CONSTRAINT `odonto_redes_ibfk_1` FOREIGN KEY (`odonto_operadora_id`) REFERENCES `odonto_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_reembolsos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_reembolsos`;

CREATE TABLE `odonto_reembolsos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL,
  `odonto_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_operadora_id` (`odonto_operadora_id`),
  CONSTRAINT `odonto_reembolsos_ibfk_1` FOREIGN KEY (`odonto_operadora_id`) REFERENCES `odonto_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;



# Dump da tabela odonto_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `odonto_tabelas`;

CREATE TABLE `odonto_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `vigencia` date NOT NULL,
  `preco_vida` float DEFAULT NULL,
  `validade` char(1) DEFAULT NULL,
  `minimo_vidas` int(11) DEFAULT NULL,
  `maximo_vidas` int(11) DEFAULT NULL,
  `titulares` int(11) DEFAULT NULL,
  `prioridade` float DEFAULT '0',
  `reembolso` varchar(1) DEFAULT 'N',
  `tipo_pessoa` varchar(10) NOT NULL DEFAULT '',
  `odonto_comercializacao_id` int(11) unsigned DEFAULT NULL,
  `odonto_atendimento_id` int(11) unsigned DEFAULT NULL,
  `odonto_produto_id` int(11) unsigned NOT NULL,
  `estado_id` int(11) unsigned DEFAULT NULL,
  `odonto_operadora_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `odonto_produto_id` (`odonto_produto_id`),
  KEY `abrangencia_id` (`odonto_atendimento_id`),
  KEY `estado_id` (`estado_id`),
  KEY `regiao_id` (`odonto_comercializacao_id`),
  CONSTRAINT `odonto_tabelas_ibfk_1` FOREIGN KEY (`odonto_produto_id`) REFERENCES `odonto_produtos` (`id`),
  CONSTRAINT `odonto_tabelas_ibfk_10` FOREIGN KEY (`odonto_atendimento_id`) REFERENCES `odonto_atendimentos` (`id`),
  CONSTRAINT `odonto_tabelas_ibfk_8` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `odonto_tabelas_ibfk_9` FOREIGN KEY (`odonto_comercializacao_id`) REFERENCES `odonto_comercializacoes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=338 DEFAULT CHARSET=utf8;



# Dump da tabela opcionais
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opcionais`;

CREATE TABLE `opcionais` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `opcionais_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;



# Dump da tabela operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operadoras`;

CREATE TABLE `operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `imagem_id` int(11) unsigned DEFAULT NULL,
  `estado_id` int(10) unsigned DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  `detalhe` varchar(300) DEFAULT NULL,
  `prioridade` float DEFAULT '0',
  `url` varchar(400) DEFAULT NULL,
  `url_rede` varchar(400) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `imagem_id` (`imagem_id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `operadoras_ibfk_1` FOREIGN KEY (`imagem_id`) REFERENCES `imagens` (`id`),
  CONSTRAINT `operadoras_ibfk_2` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;



# Dump da tabela operadoras_fechadas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operadoras_fechadas`;

CREATE TABLE `operadoras_fechadas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



# Dump da tabela operadoras_fechadas_arquivos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operadoras_fechadas_arquivos`;

CREATE TABLE `operadoras_fechadas_arquivos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `operadora_fechada_id` int(11) unsigned NOT NULL,
  `arquivo_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_fechada_id` (`operadora_fechada_id`),
  KEY `arquivo_id` (`arquivo_id`),
  CONSTRAINT `operadoras_fechadas_arquivos_ibfk_1` FOREIGN KEY (`operadora_fechada_id`) REFERENCES `operadoras_fechadas` (`id`),
  CONSTRAINT `operadoras_fechadas_arquivos_ibfk_2` FOREIGN KEY (`arquivo_id`) REFERENCES `arquivos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump da tabela parceiros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parceiros`;

CREATE TABLE `parceiros` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rg` varchar(25) NOT NULL DEFAULT '',
  `nome` varchar(300) NOT NULL DEFAULT '',
  `cpf` varchar(15) NOT NULL DEFAULT '',
  `email` varchar(300) NOT NULL DEFAULT '',
  `data_nascimento` date NOT NULL,
  `created` datetime NOT NULL,
  `cnpj_corpar` varchar(25) NOT NULL DEFAULT '',
  `razao_social` varchar(400) NOT NULL DEFAULT '',
  `situacao` char(1) NOT NULL DEFAULT '',
  `perfil` char(1) NOT NULL DEFAULT '',
  `uf` varchar(2) NOT NULL DEFAULT '',
  `cidade` varchar(300) NOT NULL DEFAULT '',
  `ddd` varchar(2) NOT NULL DEFAULT '',
  `telefone` varchar(9) NOT NULL DEFAULT '',
  `unidade` varchar(30) NOT NULL DEFAULT '',
  `comissionado` varchar(30) NOT NULL DEFAULT '',
  `desligamento` date DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;



# Dump da tabela pdffiltros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pdffiltros`;

CREATE TABLE `pdffiltros` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `simulacao_id` int(11) unsigned NOT NULL,
  `abrangencia_id` int(11) DEFAULT NULL,
  `tipo_id` int(11) DEFAULT NULL,
  `tipo_produto_id` int(11) DEFAULT NULL,
  `coparticipacao` varchar(11) DEFAULT NULL,
  `tipo_contratacao` int(11) DEFAULT NULL,
  `filtroreembolso` varchar(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `simulacao_id` (`simulacao_id`),
  CONSTRAINT `pdffiltros_ibfk_1` FOREIGN KEY (`simulacao_id`) REFERENCES `simulacoes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;



# Dump da tabela perfis_empresariais
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perfis_empresariais`;

CREATE TABLE `perfis_empresariais` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT '',
  `nome` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `perfis_empresariais_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;



# Dump da tabela pf_acomodacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_acomodacoes`;

CREATE TABLE `pf_acomodacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;



# Dump da tabela pf_atendimentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_atendimentos`;

CREATE TABLE `pf_atendimentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;



# Dump da tabela pf_calculos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_calculos`;

CREATE TABLE `pf_calculos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `idade_titular` int(11) DEFAULT NULL,
  `pf_profissao_id` int(11) unsigned DEFAULT NULL,
  `modalidade` varchar(20) DEFAULT NULL,
  `faixa1` int(11) DEFAULT NULL,
  `faixa2` int(11) DEFAULT NULL,
  `faixa3` int(11) DEFAULT NULL,
  `faixa4` int(11) DEFAULT NULL,
  `faixa5` int(11) DEFAULT NULL,
  `faixa6` int(11) DEFAULT NULL,
  `faixa7` int(11) DEFAULT NULL,
  `faixa8` int(11) DEFAULT NULL,
  `faixa9` int(11) DEFAULT NULL,
  `faixa10` int(11) DEFAULT NULL,
  `faixa11` int(11) DEFAULT NULL,
  `faixa12` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned DEFAULT NULL,
  `calculoremoto` varchar(1) DEFAULT NULL,
  `status_id` int(11) unsigned DEFAULT NULL,
  `estado_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `pf_calculos_ibfk_2` (`pf_profissao_id`),
  KEY `status_id` (`status_id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `pf_calculos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `pf_calculos_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  CONSTRAINT `pf_calculos_ibfk_4` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8581 DEFAULT CHARSET=utf8;



# Dump da tabela pf_calculos_dependentes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_calculos_dependentes`;

CREATE TABLE `pf_calculos_dependentes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_calculo_id` int(11) unsigned NOT NULL,
  `idade_dependente` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_calculo_id` (`pf_calculo_id`),
  CONSTRAINT `pf_calculos_dependentes_ibfk_1` FOREIGN KEY (`pf_calculo_id`) REFERENCES `pf_calculos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4175 DEFAULT CHARSET=utf8;



# Dump da tabela pf_calculos_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_calculos_tabelas`;

CREATE TABLE `pf_calculos_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_calculo_id` int(11) unsigned NOT NULL,
  `pf_tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_calculo_id` (`pf_calculo_id`),
  KEY `pf_tabela_id` (`pf_tabela_id`),
  CONSTRAINT `pf_calculo_tabelas_ibfk_1` FOREIGN KEY (`pf_calculo_id`) REFERENCES `pf_calculos` (`id`),
  CONSTRAINT `pf_calculo_tabelas_ibfk_2` FOREIGN KEY (`pf_tabela_id`) REFERENCES `pf_tabelas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=433700 DEFAULT CHARSET=utf8;



# Dump da tabela pf_carencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_carencias`;

CREATE TABLE `pf_carencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `pf_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_carencias_ibfk_1` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;



# Dump da tabela pf_comercializacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_comercializacoes`;

CREATE TABLE `pf_comercializacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  `pf_operadora_id` int(11) unsigned DEFAULT NULL,
  `descricao` text,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_comercializacoes_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `pf_comercializacoes_ibfk_2` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;



# Dump da tabela pf_dependentes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_dependentes`;

CREATE TABLE `pf_dependentes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `pf_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_dependentes_ibfk_1` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;



# Dump da tabela pf_documentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_documentos`;

CREATE TABLE `pf_documentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `pf_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_documentos_ibfk_1` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;



# Dump da tabela pf_entidades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_entidades`;

CREATE TABLE `pf_entidades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text,
  `nome` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;



# Dump da tabela pf_entidades_pf_operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_entidades_pf_operadoras`;

CREATE TABLE `pf_entidades_pf_operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_entidade_id` int(11) unsigned NOT NULL,
  `pf_operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_entidade_id` (`pf_entidade_id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_entidades_pf_operadoras_ibfk_1` FOREIGN KEY (`pf_entidade_id`) REFERENCES `pf_entidades` (`id`),
  CONSTRAINT `pf_entidades_pf_operadoras_ibfk_2` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=509 DEFAULT CHARSET=latin1;



# Dump da tabela pf_entidades_profissoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_entidades_profissoes`;

CREATE TABLE `pf_entidades_profissoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_entidade_id` int(11) unsigned NOT NULL,
  `pf_profissao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_entidade_id` (`pf_entidade_id`),
  KEY `pf_profissao_id` (`pf_profissao_id`),
  CONSTRAINT `pf_entidades_profissoes_ibfk_1` FOREIGN KEY (`pf_entidade_id`) REFERENCES `pf_entidades` (`id`),
  CONSTRAINT `pf_entidades_profissoes_ibfk_2` FOREIGN KEY (`pf_profissao_id`) REFERENCES `pf_profissoes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1081 DEFAULT CHARSET=utf8;



# Dump da tabela pf_entidades_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_entidades_tabelas`;

CREATE TABLE `pf_entidades_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_entidade_id` int(11) unsigned NOT NULL,
  `pf_tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_entidade_id` (`pf_entidade_id`),
  KEY `pf_tabela_id` (`pf_tabela_id`),
  CONSTRAINT `pf_entidades_tabelas_ibfk_1` FOREIGN KEY (`pf_entidade_id`) REFERENCES `pf_entidades` (`id`),
  CONSTRAINT `pf_entidades_tabelas_ibfk_2` FOREIGN KEY (`pf_tabela_id`) REFERENCES `pf_tabelas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9769 DEFAULT CHARSET=utf8;



# Dump da tabela pf_filtros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_filtros`;

CREATE TABLE `pf_filtros` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_calculo_id` int(11) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pf_atendimento_id` int(10) unsigned DEFAULT NULL,
  `coparticipacao` varchar(20) DEFAULT NULL,
  `pf_acomodacao_id` int(10) unsigned DEFAULT NULL,
  `tipo_produto_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `carencia` int(11) DEFAULT NULL,
  `reembolso` int(11) DEFAULT NULL,
  `rede` int(11) DEFAULT NULL,
  `documento` int(11) DEFAULT NULL,
  `dependente` int(11) DEFAULT NULL,
  `observacao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_calculo_id` (`pf_calculo_id`),
  KEY `user_id` (`user_id`),
  KEY `tipo_produto_id` (`tipo_produto_id`),
  KEY `pf_acomodacao_id` (`pf_acomodacao_id`),
  KEY `pf_atendimento_id` (`pf_atendimento_id`),
  CONSTRAINT `pf_filtros_ibfk_1` FOREIGN KEY (`pf_calculo_id`) REFERENCES `pf_calculos` (`id`),
  CONSTRAINT `pf_filtros_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `pf_filtros_ibfk_3` FOREIGN KEY (`tipo_produto_id`) REFERENCES `tipos_produtos` (`id`),
  CONSTRAINT `pf_filtros_ibfk_4` FOREIGN KEY (`pf_acomodacao_id`) REFERENCES `pf_acomodacoes` (`id`),
  CONSTRAINT `pf_filtros_ibfk_5` FOREIGN KEY (`pf_atendimento_id`) REFERENCES `pf_atendimentos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4266 DEFAULT CHARSET=latin1;



# Dump da tabela pf_filtros_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_filtros_tabelas`;

CREATE TABLE `pf_filtros_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_filtro_id` int(11) unsigned NOT NULL,
  `pf_tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_filtro_id` (`pf_filtro_id`),
  KEY `pf_tabela_id` (`pf_tabela_id`),
  CONSTRAINT `pf_filtros_tabelas_ibfk_1` FOREIGN KEY (`pf_filtro_id`) REFERENCES `pf_filtros` (`id`),
  CONSTRAINT `pf_filtros_tabelas_ibfk_2` FOREIGN KEY (`pf_tabela_id`) REFERENCES `pf_tabelas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38179 DEFAULT CHARSET=latin1;



# Dump da tabela pf_formas_pagamentos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_formas_pagamentos`;

CREATE TABLE `pf_formas_pagamentos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `pf_operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_formas_pagamentos_ibfk_1` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;



# Dump da tabela pf_observacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_observacoes`;

CREATE TABLE `pf_observacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `pf_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_observacoes_ibfk_1` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;



# Dump da tabela pf_operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_operadoras`;

CREATE TABLE `pf_operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `imagem_id` int(11) unsigned DEFAULT NULL,
  `detalhe` varchar(300) DEFAULT NULL,
  `prioridade` float DEFAULT '0',
  `url` varchar(400) DEFAULT NULL,
  `url_rede` varchar(400) DEFAULT NULL,
  `estado_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `imagem_id` (`imagem_id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `pf_operadoras_ibfk_1` FOREIGN KEY (`imagem_id`) REFERENCES `imagens` (`id`),
  CONSTRAINT `pf_operadoras_ibfk_2` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;



# Dump da tabela pf_operadoras_fechadas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_operadoras_fechadas`;

CREATE TABLE `pf_operadoras_fechadas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



# Dump da tabela pf_operadoras_fechadas_arquivos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_operadoras_fechadas_arquivos`;

CREATE TABLE `pf_operadoras_fechadas_arquivos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_operadora_fechada_id` int(11) unsigned NOT NULL,
  `arquivo_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_operadora_fechada_id` (`pf_operadora_fechada_id`),
  KEY `arquivo_id` (`arquivo_id`),
  CONSTRAINT `pf_operadoras_fechadas_arquivos_ibfk_1` FOREIGN KEY (`pf_operadora_fechada_id`) REFERENCES `pf_operadoras_fechadas` (`id`),
  CONSTRAINT `pf_operadoras_fechadas_arquivos_ibfk_2` FOREIGN KEY (`arquivo_id`) REFERENCES `arquivos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump da tabela pf_pdffiltros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_pdffiltros`;

CREATE TABLE `pf_pdffiltros` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_calculo_id` int(11) unsigned NOT NULL,
  `pf_atendimento_id` int(10) unsigned DEFAULT NULL,
  `pf_acomodacao_id` int(10) unsigned DEFAULT NULL,
  `tipo_produto_id` int(10) unsigned DEFAULT NULL,
  `coparticipacao` varchar(10) DEFAULT NULL,
  `filtroreembolso` varchar(10) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_calculo_id` (`pf_calculo_id`),
  KEY `pf_atendimento_id` (`pf_atendimento_id`),
  KEY `pf_acomodacao_id` (`pf_acomodacao_id`),
  KEY `tipo_produto_id` (`tipo_produto_id`),
  CONSTRAINT `pf_pdffiltros_ibfk_1` FOREIGN KEY (`pf_calculo_id`) REFERENCES `pf_calculos` (`id`),
  CONSTRAINT `pf_pdffiltros_ibfk_2` FOREIGN KEY (`pf_atendimento_id`) REFERENCES `pf_atendimentos` (`id`),
  CONSTRAINT `pf_pdffiltros_ibfk_3` FOREIGN KEY (`pf_acomodacao_id`) REFERENCES `pf_acomodacoes` (`id`),
  CONSTRAINT `pf_pdffiltros_ibfk_4` FOREIGN KEY (`tipo_produto_id`) REFERENCES `tipos_produtos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;



# Dump da tabela pf_produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_produtos`;

CREATE TABLE `pf_produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `descricao` text,
  `pf_operadora_id` int(11) unsigned NOT NULL,
  `pf_carencia_id` int(11) unsigned DEFAULT NULL,
  `pf_rede_id` int(11) unsigned DEFAULT NULL,
  `pf_reembolso_id` int(11) unsigned DEFAULT NULL,
  `pf_documento_id` int(11) unsigned DEFAULT NULL,
  `pf_dependente_id` int(11) unsigned DEFAULT NULL,
  `pf_observacao_id` int(10) unsigned DEFAULT NULL,
  `tipo_produto_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  KEY `pf_carencia_id` (`pf_carencia_id`),
  KEY `pf_rede_id` (`pf_rede_id`),
  KEY `pf_reembolso_id` (`pf_reembolso_id`),
  KEY `pf_dependente_id` (`pf_dependente_id`),
  KEY `pf_documento_id` (`pf_documento_id`),
  KEY `pf_observacao_id` (`pf_observacao_id`),
  KEY `tipo_produto_id` (`tipo_produto_id`),
  CONSTRAINT `pf_produtos_ibfk_1` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`),
  CONSTRAINT `pf_produtos_ibfk_2` FOREIGN KEY (`pf_carencia_id`) REFERENCES `pf_carencias` (`id`),
  CONSTRAINT `pf_produtos_ibfk_3` FOREIGN KEY (`pf_rede_id`) REFERENCES `pf_redes` (`id`),
  CONSTRAINT `pf_produtos_ibfk_4` FOREIGN KEY (`pf_reembolso_id`) REFERENCES `pf_reembolsos` (`id`),
  CONSTRAINT `pf_produtos_ibfk_5` FOREIGN KEY (`pf_dependente_id`) REFERENCES `pf_dependentes` (`id`),
  CONSTRAINT `pf_produtos_ibfk_6` FOREIGN KEY (`pf_documento_id`) REFERENCES `pf_documentos` (`id`),
  CONSTRAINT `pf_produtos_ibfk_7` FOREIGN KEY (`pf_observacao_id`) REFERENCES `pf_observacoes` (`id`),
  CONSTRAINT `pf_produtos_ibfk_8` FOREIGN KEY (`tipo_produto_id`) REFERENCES `tipos_produtos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;



# Dump da tabela pf_profissoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_profissoes`;

CREATE TABLE `pf_profissoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `idade_min` int(11) DEFAULT NULL,
  `idade_max` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;



# Dump da tabela pf_profissoes_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_profissoes_tabelas`;

CREATE TABLE `pf_profissoes_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pf_profissao_id` int(11) unsigned NOT NULL,
  `pf_tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_profissao_id` (`pf_profissao_id`),
  KEY `pf_tabela_id` (`pf_tabela_id`),
  CONSTRAINT `pf_profissoes_tabelas_ibfk_1` FOREIGN KEY (`pf_profissao_id`) REFERENCES `pf_profissoes` (`id`),
  CONSTRAINT `pf_profissoes_tabelas_ibfk_2` FOREIGN KEY (`pf_tabela_id`) REFERENCES `pf_tabelas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela pf_redes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_redes`;

CREATE TABLE `pf_redes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `pf_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_redes_ibfk_1` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;



# Dump da tabela pf_reembolsos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_reembolsos`;

CREATE TABLE `pf_reembolsos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `pf_operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_reembolsos_ibfk_1` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;



# Dump da tabela pf_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pf_tabelas`;

CREATE TABLE `pf_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `vigencia` date NOT NULL,
  `faixa1` float DEFAULT NULL,
  `faixa2` float DEFAULT NULL,
  `faixa3` float DEFAULT NULL,
  `faixa4` float DEFAULT NULL,
  `faixa5` float DEFAULT NULL,
  `faixa6` float DEFAULT NULL,
  `faixa7` float DEFAULT NULL,
  `faixa8` float DEFAULT NULL,
  `faixa9` float DEFAULT NULL,
  `faixa10` float DEFAULT NULL,
  `faixa11` float DEFAULT NULL,
  `faixa12` float DEFAULT NULL,
  `pf_produto_id` int(11) unsigned NOT NULL,
  `pf_atendimento_id` int(11) unsigned NOT NULL,
  `pf_acomodacao_id` int(11) unsigned NOT NULL,
  `estado_id` int(11) unsigned DEFAULT NULL,
  `pf_operadora_id` int(11) unsigned DEFAULT NULL,
  `pf_comercializacao_id` int(11) unsigned DEFAULT NULL,
  `validade` char(1) DEFAULT NULL,
  `cod_ans` varchar(50) DEFAULT NULL,
  `minimo_vidas` int(11) DEFAULT NULL,
  `maximo_vidas` int(11) DEFAULT NULL,
  `prioridade` float DEFAULT '0',
  `coparticipacao` char(1) DEFAULT NULL,
  `detalhe_coparticipacao` varchar(300) DEFAULT NULL,
  `modalidade` varchar(20) NOT NULL,
  `reembolso` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `pf_produto_id` (`pf_produto_id`),
  KEY `pf_atendimento_id` (`pf_atendimento_id`),
  KEY `pf_acomodacao_id` (`pf_acomodacao_id`),
  KEY `estado_id` (`estado_id`),
  KEY `pf_comercializacao_id` (`pf_comercializacao_id`),
  KEY `pf_operadora_id` (`pf_operadora_id`),
  CONSTRAINT `pf_tabelas_ibfk_1` FOREIGN KEY (`pf_produto_id`) REFERENCES `pf_produtos` (`id`),
  CONSTRAINT `pf_tabelas_ibfk_10` FOREIGN KEY (`pf_operadora_id`) REFERENCES `pf_operadoras` (`id`),
  CONSTRAINT `pf_tabelas_ibfk_11` FOREIGN KEY (`pf_comercializacao_id`) REFERENCES `pf_comercializacoes` (`id`),
  CONSTRAINT `pf_tabelas_ibfk_4` FOREIGN KEY (`pf_atendimento_id`) REFERENCES `pf_atendimentos` (`id`),
  CONSTRAINT `pf_tabelas_ibfk_5` FOREIGN KEY (`pf_acomodacao_id`) REFERENCES `pf_acomodacoes` (`id`),
  CONSTRAINT `pf_tabelas_ibfk_8` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8;



# Dump da tabela produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produtos`;

CREATE TABLE `produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  `carencia_id` int(11) unsigned DEFAULT NULL,
  `rede_id` int(11) unsigned DEFAULT NULL,
  `reembolso_id` int(11) unsigned DEFAULT NULL,
  `opcional_id` int(11) unsigned DEFAULT NULL,
  `informacao_id` int(11) unsigned DEFAULT NULL,
  `observacao_id` int(10) unsigned DEFAULT NULL,
  `tipo_produto_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  KEY `carencia_id` (`carencia_id`),
  KEY `rede_id` (`rede_id`),
  KEY `reembolso_id` (`reembolso_id`),
  KEY `opcional_id` (`opcional_id`),
  KEY `informacao_id` (`informacao_id`),
  KEY `observacao_id` (`observacao_id`),
  KEY `tipo_produto_id` (`tipo_produto_id`),
  CONSTRAINT `produtos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`),
  CONSTRAINT `produtos_ibfk_8` FOREIGN KEY (`tipo_produto_id`) REFERENCES `tipos_produtos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=998 DEFAULT CHARSET=utf8;



# Dump da tabela produtos_sugeridos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produtos_sugeridos`;

CREATE TABLE `produtos_sugeridos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  `operadora` varchar(300) NOT NULL DEFAULT '',
  `comercializacao` text NOT NULL,
  `area_atendimento` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `produtos_sugeridos_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;



# Dump da tabela produtos_sugeridos_arquivos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produtos_sugeridos_arquivos`;

CREATE TABLE `produtos_sugeridos_arquivos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `produto_sugerido_id` int(11) unsigned NOT NULL,
  `arquivo_id` int(11) unsigned NOT NULL,
  `tipo` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `produto_sugerido_id` (`produto_sugerido_id`),
  KEY `arquivo_id` (`arquivo_id`),
  CONSTRAINT `produtos_sugeridos_arquivos_ibfk_1` FOREIGN KEY (`produto_sugerido_id`) REFERENCES `produtos_sugeridos` (`id`),
  CONSTRAINT `produtos_sugeridos_arquivos_ibfk_2` FOREIGN KEY (`arquivo_id`) REFERENCES `arquivos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;



# Dump da tabela prospects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prospects`;

CREATE TABLE `prospects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(300) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `created` datetime NOT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `sobrenome` varchar(200) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `cnpj` varchar(40) DEFAULT NULL,
  `susep` varchar(5) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `celular_secundario` varchar(20) DEFAULT NULL,
  `whatsapp` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;



# Dump da tabela ramos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ramos`;

CREATE TABLE `ramos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



# Dump da tabela redes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redes`;

CREATE TABLE `redes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `redes_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;



# Dump da tabela reembolsos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reembolsos`;

CREATE TABLE `reembolsos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `reembolsos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;



# Dump da tabela regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regioes`;

CREATE TABLE `regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  `operadora_id` int(11) unsigned DEFAULT NULL,
  `descricao` text,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `regioes_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `regioes_ibfk_2` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;



# Dump da tabela simulacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes`;

CREATE TABLE `simulacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `contato` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `faixa1` int(11) DEFAULT NULL,
  `faixa2` int(11) DEFAULT NULL,
  `faixa3` int(11) DEFAULT NULL,
  `faixa4` int(11) DEFAULT NULL,
  `faixa5` int(11) DEFAULT NULL,
  `faixa6` int(11) DEFAULT NULL,
  `faixa7` int(11) DEFAULT NULL,
  `faixa8` int(11) DEFAULT NULL,
  `faixa9` int(11) DEFAULT NULL,
  `faixa10` int(11) DEFAULT NULL,
  `faixa11` int(11) DEFAULT NULL,
  `faixa12` int(11) DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `telefone` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `calculoremoto` varchar(1) DEFAULT NULL,
  `status_id` int(11) unsigned DEFAULT NULL,
  `estado_id` int(10) unsigned DEFAULT NULL,
  `nome_titular` varchar(200) DEFAULT NULL,
  `id_primario` int(10) DEFAULT NULL,
  `tipo_cnpj_id` int(11) unsigned DEFAULT NULL,
  `familia` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `status_id` (`status_id`),
  KEY `estado_id` (`estado_id`),
  KEY `tipo_cnpj_id` (`tipo_cnpj_id`),
  CONSTRAINT `simulacoes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `simulacoes_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  CONSTRAINT `simulacoes_ibfk_3` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `tipo_cnpj_id` FOREIGN KEY (`tipo_cnpj_id`) REFERENCES `cnpjs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20819 DEFAULT CHARSET=utf8;



# Dump da tabela simulacoes_comentarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes_comentarios`;

CREATE TABLE `simulacoes_comentarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `mensagem` text NOT NULL,
  `alerta` char(1) DEFAULT 'N',
  `data_alerta` date DEFAULT NULL,
  `simulacao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `simulacao_id` (`simulacao_id`),
  CONSTRAINT `simulacoes_comentarios_ibfk_1` FOREIGN KEY (`simulacao_id`) REFERENCES `simulacoes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;



# Dump da tabela simulacoes_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes_tabelas`;

CREATE TABLE `simulacoes_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `simulacao_id` int(11) unsigned NOT NULL,
  `tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `simulacao_id` (`simulacao_id`),
  KEY `tabela_id` (`tabela_id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_1` FOREIGN KEY (`simulacao_id`) REFERENCES `simulacoes` (`id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_2` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3406644 DEFAULT CHARSET=utf8;



# Dump da tabela sms_cabecalhos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_cabecalhos`;

CREATE TABLE `sms_cabecalhos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;



# Dump da tabela sms_envios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_envios`;

CREATE TABLE `sms_envios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sms_lista_id` int(11) unsigned NOT NULL,
  `sms_situacao_id` int(11) unsigned NOT NULL DEFAULT '2',
  `sms_cabecalho_id` int(10) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `mensagem` text NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `data_envio` datetime DEFAULT NULL,
  `data_agendamento` datetime DEFAULT NULL,
  `autorizado` int(11) DEFAULT NULL,
  `descricao` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_lista_id` (`sms_lista_id`),
  KEY `sms_situacao_id` (`sms_situacao_id`),
  KEY `user_id` (`user_id`),
  KEY `sms_cabecalho_id` (`sms_cabecalho_id`),
  CONSTRAINT `sms_envios_ibfk_1` FOREIGN KEY (`sms_lista_id`) REFERENCES `sms_listas` (`id`),
  CONSTRAINT `sms_envios_ibfk_2` FOREIGN KEY (`sms_situacao_id`) REFERENCES `sms_situacoes` (`id`),
  CONSTRAINT `sms_envios_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `sms_envios_ibfk_4` FOREIGN KEY (`sms_cabecalho_id`) REFERENCES `sms_cabecalhos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;



# Dump da tabela sms_listas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_listas`;

CREATE TABLE `sms_listas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `caminho` varchar(400) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;



# Dump da tabela sms_respostas_envios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_respostas_envios`;

CREATE TABLE `sms_respostas_envios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sms_envio_id` int(11) unsigned DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL,
  `data` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `resultado` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_envio_id` (`sms_envio_id`),
  CONSTRAINT `respostas_envios_ibfk_1` FOREIGN KEY (`sms_envio_id`) REFERENCES `sms_envios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=latin1;



# Dump da tabela sms_situacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_situacoes`;

CREATE TABLE `sms_situacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;



# Dump da tabela smsenviados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `smsenviados`;

CREATE TABLE `smsenviados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `destinatario` varchar(50) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `cod_resultado` varchar(11) DEFAULT NULL,
  `resultado` varchar(200) DEFAULT NULL,
  `mensagem` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1236 DEFAULT CHARSET=latin1;



# Dump da tabela status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;



# Dump da tabela subprodutos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subprodutos`;

CREATE TABLE `subprodutos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `tipo_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_id` (`tipo_id`),
  CONSTRAINT `subprodutos_ibfk_1` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



# Dump da tabela tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas`;

CREATE TABLE `tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `vigencia` date NOT NULL,
  `faixa1` float DEFAULT NULL,
  `faixa2` float DEFAULT NULL,
  `faixa3` float DEFAULT NULL,
  `faixa4` float DEFAULT NULL,
  `faixa5` float DEFAULT NULL,
  `faixa6` float DEFAULT NULL,
  `faixa7` float DEFAULT NULL,
  `faixa8` float DEFAULT NULL,
  `faixa9` float DEFAULT NULL,
  `faixa10` float DEFAULT NULL,
  `faixa11` float DEFAULT NULL,
  `faixa12` float DEFAULT NULL,
  `produto_id` int(11) unsigned NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  `ramo_id` int(11) unsigned NOT NULL,
  `abrangencia_id` int(11) unsigned NOT NULL,
  `tipo_id` int(11) unsigned NOT NULL,
  `modalidade_id` int(11) unsigned NOT NULL,
  `validade` char(1) DEFAULT NULL,
  `estado_id` int(11) unsigned DEFAULT NULL,
  `regiao_id` int(11) unsigned DEFAULT NULL,
  `cod_ans` varchar(50) DEFAULT NULL,
  `minimo_vidas` int(11) DEFAULT NULL,
  `maximo_vidas` int(11) DEFAULT NULL,
  `titulares` int(11) DEFAULT NULL,
  `prioridade` float DEFAULT '0',
  `tipo_cnpj` int(11) DEFAULT '0',
  `coparticipacao` char(1) DEFAULT NULL,
  `detalhe_coparticipacao` varchar(300) DEFAULT NULL,
  `tipo_contratacao` char(1) NOT NULL DEFAULT '0',
  `reembolso` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `produto_id` (`produto_id`),
  KEY `operadora_id` (`operadora_id`),
  KEY `ramo_id` (`ramo_id`),
  KEY `abrangencia_id` (`abrangencia_id`),
  KEY `tipo_id` (`tipo_id`),
  KEY `modalidade_id` (`modalidade_id`),
  KEY `estado_id` (`estado_id`),
  KEY `regiao_id` (`regiao_id`),
  CONSTRAINT `tabelas_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`),
  CONSTRAINT `tabelas_ibfk_2` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`),
  CONSTRAINT `tabelas_ibfk_3` FOREIGN KEY (`ramo_id`) REFERENCES `ramos` (`id`),
  CONSTRAINT `tabelas_ibfk_4` FOREIGN KEY (`abrangencia_id`) REFERENCES `abrangencias` (`id`),
  CONSTRAINT `tabelas_ibfk_5` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`),
  CONSTRAINT `tabelas_ibfk_6` FOREIGN KEY (`modalidade_id`) REFERENCES `modalidades` (`id`),
  CONSTRAINT `tabelas_ibfk_8` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `tabelas_ibfk_9` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1870 DEFAULT CHARSET=utf8;



# Dump da tabela tabelas_cnpjs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_cnpjs`;

CREATE TABLE `tabelas_cnpjs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tabela_id` int(11) unsigned NOT NULL,
  `cnpj_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_id` (`tabela_id`),
  KEY `cnpj_id` (`cnpj_id`),
  CONSTRAINT `tabelas_cnpjs_ibfk_1` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`),
  CONSTRAINT `tabelas_cnpjs_ibfk_2` FOREIGN KEY (`cnpj_id`) REFERENCES `cnpjs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4845 DEFAULT CHARSET=latin1;



# Dump da tabela tabelas_filtradas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_filtradas`;

CREATE TABLE `tabelas_filtradas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filtro_id` int(11) unsigned NOT NULL,
  `tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filtro_id` (`filtro_id`),
  KEY `tabela_id` (`tabela_id`),
  CONSTRAINT `tabelas_filtradas_ibfk_1` FOREIGN KEY (`filtro_id`) REFERENCES `filtros` (`id`),
  CONSTRAINT `tabelas_filtradas_ibfk_2` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134258 DEFAULT CHARSET=latin1;



# Dump da tabela tabelas_geradas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_geradas`;

CREATE TABLE `tabelas_geradas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `estado_id` int(11) unsigned NOT NULL,
  `ramo` varchar(50) NOT NULL DEFAULT '',
  `operadora` varchar(200) NOT NULL DEFAULT '',
  `vidas` varchar(50) DEFAULT NULL,
  `coparticipacao` varchar(50) DEFAULT NULL,
  `created` datetime NOT NULL,
  `nome` varchar(300) DEFAULT '',
  `pdf` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `tabelas_geradas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `tabelas_geradas_ibfk_2` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=683 DEFAULT CHARSET=latin1;



# Dump da tabela tabelas_geradas_produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_geradas_produtos`;

CREATE TABLE `tabelas_geradas_produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tabela_gerada_id` int(11) unsigned NOT NULL,
  `produto_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_gerada_id` (`tabela_gerada_id`),
  CONSTRAINT `tabelas_geradas_produtos_ibfk_1` FOREIGN KEY (`tabela_gerada_id`) REFERENCES `tabelas_geradas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;



# Dump da tabela tabelas_regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_regioes`;

CREATE TABLE `tabelas_regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tabela_id` int(11) unsigned NOT NULL,
  `regiao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_id` (`tabela_id`),
  KEY `regiao_id` (`regiao_id`),
  CONSTRAINT `tabelas_regioes_ibfk_1` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`),
  CONSTRAINT `tabelas_regioes_ibfk_2` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1594 DEFAULT CHARSET=utf8;



# Dump da tabela tipos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipos`;

CREATE TABLE `tipos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `flag` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;



# Dump da tabela tipos_produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipos_produtos`;

CREATE TABLE `tipos_produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;



# Dump da tabela users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_pai_id` int(11) unsigned DEFAULT NULL,
  `imagem_id` int(11) unsigned DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `sobrenome` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `validacao` varchar(11) DEFAULT 'INATIVO',
  `codigo` int(11) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT 'PF',
  `ultimologin` datetime DEFAULT NULL,
  `ultimocalculo` datetime DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `site` varchar(400) DEFAULT NULL,
  `whatsapp` varchar(20) DEFAULT NULL,
  `facebook` varchar(400) DEFAULT NULL,
  `senha_provisoria` char(1) DEFAULT 'N',
  `estado_id` int(11) unsigned DEFAULT '5',
  `logado` char(1) DEFAULT NULL,
  `codigo_email` int(11) DEFAULT NULL,
  `validacao_email` varchar(11) DEFAULT 'ATIVO',
  PRIMARY KEY (`id`),
  KEY `imagem_id` (`imagem_id`),
  KEY `user_pai_id` (`user_pai_id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`imagem_id`) REFERENCES `imagens` (`id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`user_pai_id`) REFERENCES `users` (`id`),
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1515 DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`natus_multi`@`%` */ /*!50003 TRIGGER `users_i` AFTER INSERT ON `users` FOR EACH ROW mytrigger: BEGIN
  IF (@DISABLE_TRIGER = TRUE) THEN
    LEAVE mytrigger;
  END IF;
  DELETE FROM users_tracking_store WHERE  `id` = NEW.`id`;
  INSERT INTO users_tracking_store (`id`, `tr_timemark`, `tr_operation`) VALUES (NEW.`id`, UTC_TIMESTAMP(), 'i');
END */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`natus_multi`@`%` */ /*!50003 TRIGGER `users_u` AFTER UPDATE ON `users` FOR EACH ROW mytrigger: BEGIN
  DECLARE time_mark TIMESTAMP;
  DECLARE count int;

  IF (@DISABLE_TRIGER = TRUE) THEN
    LEAVE mytrigger;
  END IF;
  
  SET time_mark = UTC_TIMESTAMP();
  SELECT count(*) INTO count FROM users_tracking_store WHERE  `id` = OLD.`id`;
  IF (count = 0) THEN 
     INSERT INTO users_tracking_store (`id`, `tr_timemark` , `tr_operation`) VALUES (NEW.`id`, time_mark, 'u');
  ELSE
     UPDATE users_tracking_store SET `tr_timemark` = time_mark, `tr_operation`= 'u' WHERE  `id` = OLD.`id`;
  END IF;
END */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`natus_multi`@`%` */ /*!50003 TRIGGER `users_d` AFTER DELETE ON `users` FOR EACH ROW mytrigger: BEGIN
  DECLARE time_mark TIMESTAMP;
  DECLARE count int;

  IF (@DISABLE_TRIGER = TRUE) THEN
    LEAVE mytrigger;
  END IF;
  
  SET time_mark = UTC_TIMESTAMP();
  SELECT count(*) INTO count FROM users_tracking_store WHERE  `id` = OLD.`id`;
  IF (count = 0) THEN 
     INSERT INTO users_tracking_store (`id`, `tr_timemark` , `tr_operation`) VALUES (OLD.`id`, time_mark, 'd');
  ELSE
     UPDATE users_tracking_store SET `tr_timemark` = time_mark, `tr_operation`= 'd' WHERE  `id` = OLD.`id`;
  END IF;
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump da tabela users_tracking_store
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_tracking_store`;

CREATE TABLE `users_tracking_store` (
  `id` bigint(20) NOT NULL,
  `tr_timemark` datetime DEFAULT NULL,
  `tr_operation` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela vw_calculos
# ------------------------------------------------------------

DROP VIEW IF EXISTS `vw_calculos`;

CREATE TABLE `vw_calculos` (
   `user_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
   `id` INT(11) UNSIGNED NULL DEFAULT NULL,
   `ramo` VARCHAR(3) NOT NULL DEFAULT '',
   `data` TIMESTAMP NULL DEFAULT NULL,
   `nome` VARCHAR(100) NULL DEFAULT NULL,
   `status_id` INT(11) UNSIGNED NULL DEFAULT NULL,
   `status` VARCHAR(30) NULL DEFAULT NULL
) ENGINE=MyISAM;



# Dump da tabela vw_sms
# ------------------------------------------------------------

DROP VIEW IF EXISTS `vw_sms`;

CREATE TABLE `vw_sms` (
   `destinatario_enviado` VARCHAR(16) NULL DEFAULT NULL,
   `sms_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
   `destinatario` VARCHAR(50) NULL DEFAULT NULL,
   `data_envio` DATETIME NULL DEFAULT NULL,
   `cod_resultado` VARCHAR(11) NULL DEFAULT NULL,
   `resultado` VARCHAR(200) NULL DEFAULT NULL,
   `mensagem` VARCHAR(200) NULL DEFAULT NULL,
   `estado` VARCHAR(100) NULL DEFAULT '',
   `id` INT(10) UNSIGNED NULL DEFAULT '0',
   `user_pai_id` INT(11) UNSIGNED NULL DEFAULT NULL,
   `imagem_id` INT(11) UNSIGNED NULL DEFAULT NULL,
   `username` VARCHAR(50) NULL DEFAULT NULL,
   `password` VARCHAR(255) NULL DEFAULT NULL,
   `role` VARCHAR(20) NULL DEFAULT NULL,
   `nome` VARCHAR(200) NULL DEFAULT NULL,
   `sobrenome` VARCHAR(200) NULL DEFAULT NULL,
   `created` DATETIME NULL DEFAULT NULL,
   `modified` DATETIME NULL DEFAULT NULL,
   `email` VARCHAR(200) NULL DEFAULT NULL,
   `validacao` VARCHAR(11) NULL DEFAULT 'INATIVO',
   `codigo` INT(11) NULL DEFAULT NULL,
   `celular` VARCHAR(20) NULL DEFAULT NULL,
   `tipo` VARCHAR(50) NULL DEFAULT 'PF',
   `ultimologin` DATETIME NULL DEFAULT NULL,
   `ultimocalculo` DATETIME NULL DEFAULT NULL,
   `data_nascimento` DATE NULL DEFAULT NULL,
   `site` VARCHAR(400) NULL DEFAULT NULL,
   `whatsapp` VARCHAR(20) NULL DEFAULT NULL,
   `facebook` VARCHAR(400) NULL DEFAULT NULL,
   `senha_provisoria` CHAR(1) NULL DEFAULT 'N',
   `estado_id` INT(11) UNSIGNED NULL DEFAULT '5',
   `logado` CHAR(1) NULL DEFAULT NULL,
   `codigo_email` INT(11) NULL DEFAULT NULL,
   `validacao_email` VARCHAR(11) NULL DEFAULT 'ATIVO'
) ENGINE=MyISAM;



# Dump da tabela vw_usuarios
# ------------------------------------------------------------

DROP VIEW IF EXISTS `vw_usuarios`;

CREATE TABLE `vw_usuarios` (
   `calculosmes_pme` BIGINT(21) NULL DEFAULT NULL,
   `calculosmes_pf` BIGINT(21) NULL DEFAULT NULL,
   `calculosmes_odontopf` BIGINT(21) NULL DEFAULT NULL,
   `calculosmes_odontopj` BIGINT(21) NULL DEFAULT NULL,
   `tabelasgeradasmes` BIGINT(21) NULL DEFAULT NULL,
   `calculosdia_pme` BIGINT(21) NULL DEFAULT NULL,
   `calculos_pme` BIGINT(21) NULL DEFAULT NULL,
   `tabelas_geradas_dia` BIGINT(21) NULL DEFAULT NULL,
   `total_tabelas_geradas` BIGINT(21) NULL DEFAULT NULL,
   `calculosdia_pf` BIGINT(21) NULL DEFAULT NULL,
   `calculos_pf` BIGINT(21) NULL DEFAULT NULL,
   `calculosdia_odonto_pf` BIGINT(21) NULL DEFAULT NULL,
   `calculos_odonto_pf` BIGINT(21) NULL DEFAULT NULL,
   `calculosdia_odonto_pj` BIGINT(21) NULL DEFAULT NULL,
   `calculos_odonto_pj` BIGINT(21) NULL DEFAULT NULL,
   `total_calculos` BIGINT(24) NULL DEFAULT NULL,
   `id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
   `user_pai_id` INT(11) UNSIGNED NULL DEFAULT NULL,
   `imagem_id` INT(11) UNSIGNED NULL DEFAULT NULL,
   `username` VARCHAR(50) NULL DEFAULT NULL,
   `password` VARCHAR(255) NULL DEFAULT NULL,
   `role` VARCHAR(20) NULL DEFAULT NULL,
   `nome` VARCHAR(200) NULL DEFAULT NULL,
   `sobrenome` VARCHAR(200) NULL DEFAULT NULL,
   `created` DATETIME NULL DEFAULT NULL,
   `modified` DATETIME NULL DEFAULT NULL,
   `email` VARCHAR(200) NULL DEFAULT NULL,
   `validacao` VARCHAR(11) NULL DEFAULT 'INATIVO',
   `codigo` INT(11) NULL DEFAULT NULL,
   `celular` VARCHAR(20) NULL DEFAULT NULL,
   `tipo` VARCHAR(50) NULL DEFAULT 'PF',
   `ultimologin` DATETIME NULL DEFAULT NULL,
   `ultimocalculo` DATETIME NULL DEFAULT NULL,
   `data_nascimento` DATE NULL DEFAULT NULL,
   `site` VARCHAR(400) NULL DEFAULT NULL,
   `whatsapp` VARCHAR(20) NULL DEFAULT NULL,
   `facebook` VARCHAR(400) NULL DEFAULT NULL,
   `senha_provisoria` CHAR(1) NULL DEFAULT 'N',
   `estado_id` INT(11) UNSIGNED NULL DEFAULT '5',
   `logado` CHAR(1) NULL DEFAULT NULL,
   `codigo_email` INT(11) NULL DEFAULT NULL,
   `validacao_email` VARCHAR(11) NULL DEFAULT 'ATIVO',
   `estado_nome` VARCHAR(100) NULL DEFAULT '',
   `id_da_imagem` INT(11) UNSIGNED NULL DEFAULT '0',
   `imagem_caminho` VARCHAR(255) NULL DEFAULT NULL,
   `imagem_nome` VARCHAR(255) NULL DEFAULT NULL
) ENGINE=MyISAM;



# Dump da tabela vw_usuariosold
# ------------------------------------------------------------

DROP VIEW IF EXISTS `vw_usuariosold`;

CREATE TABLE `vw_usuariosold` (
   `calculosdia_pme` BIGINT(21) NULL DEFAULT NULL,
   `calculos_pme` BIGINT(21) NULL DEFAULT NULL,
   `tabelas_geradas_dia` BIGINT(21) NULL DEFAULT NULL,
   `total_tabelas_geradas` BIGINT(21) NULL DEFAULT NULL,
   `calculosdia_pf` BIGINT(21) NULL DEFAULT NULL,
   `calculos_pf` BIGINT(21) NULL DEFAULT NULL,
   `calculosdia_odonto_pf` BIGINT(21) NULL DEFAULT NULL,
   `calculos_odonto_pf` BIGINT(21) NULL DEFAULT NULL,
   `calculosdia_odonto_pj` BIGINT(21) NULL DEFAULT NULL,
   `calculos_odonto_pj` BIGINT(21) NULL DEFAULT NULL,
   `total_calculos` BIGINT(24) NULL DEFAULT NULL,
   `id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
   `user_pai_id` INT(11) UNSIGNED NULL DEFAULT NULL,
   `imagem_id` INT(11) UNSIGNED NULL DEFAULT NULL,
   `username` VARCHAR(50) NULL DEFAULT NULL,
   `password` VARCHAR(255) NULL DEFAULT NULL,
   `role` VARCHAR(20) NULL DEFAULT NULL,
   `nome` VARCHAR(200) NULL DEFAULT NULL,
   `sobrenome` VARCHAR(200) NULL DEFAULT NULL,
   `created` DATETIME NULL DEFAULT NULL,
   `modified` DATETIME NULL DEFAULT NULL,
   `email` VARCHAR(200) NULL DEFAULT NULL,
   `validacao` VARCHAR(11) NULL DEFAULT 'INATIVO',
   `codigo` INT(11) NULL DEFAULT NULL,
   `celular` VARCHAR(20) NULL DEFAULT NULL,
   `tipo` VARCHAR(50) NULL DEFAULT 'PF',
   `ultimologin` DATETIME NULL DEFAULT NULL,
   `ultimocalculo` DATETIME NULL DEFAULT NULL,
   `data_nascimento` DATE NULL DEFAULT NULL,
   `site` VARCHAR(400) NULL DEFAULT NULL,
   `whatsapp` VARCHAR(20) NULL DEFAULT NULL,
   `facebook` VARCHAR(400) NULL DEFAULT NULL,
   `senha_provisoria` CHAR(1) NULL DEFAULT 'N',
   `estado_id` INT(11) UNSIGNED NULL DEFAULT '5',
   `logado` CHAR(1) NULL DEFAULT NULL,
   `codigo_email` INT(11) NULL DEFAULT NULL,
   `validacao_email` VARCHAR(11) NULL DEFAULT 'ATIVO',
   `estado_nome` VARCHAR(100) NULL DEFAULT '',
   `id_da_imagem` INT(11) UNSIGNED NULL DEFAULT '0',
   `imagem_caminho` VARCHAR(255) NULL DEFAULT NULL,
   `imagem_nome` VARCHAR(255) NULL DEFAULT NULL
) ENGINE=MyISAM;





# Replace placeholder table for vw_usuarios with correct view syntax
# ------------------------------------------------------------

DROP TABLE `vw_usuarios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`natus_multi`@`%` SQL SECURITY DEFINER VIEW `vw_usuarios`
AS SELECT
   (select count(`s`.`id`)
FROM `simulacoes` `s` where ((`s`.`data` >= (now() - interval 30 day)) and (`s`.`user_id` = `u`.`id`))) AS `calculosmes_pme`,(select count(`pf`.`id`) from `pf_calculos` `pf` where ((`pf`.`created` >= (now() - interval 30 day)) and (`pf`.`user_id` = `u`.`id`))) AS `calculosmes_pf`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 30 day)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF'))) AS `calculosmes_odontopf`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 30 day)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ'))) AS `calculosmes_odontopj`,(select count(`tg`.`id`) from `tabelas_geradas` `tg` where ((`tg`.`user_id` = `u`.`id`) and (`tg`.`pdf` is not null) and (`tg`.`created` >= (now() - interval 30 day)))) AS `tabelasgeradasmes`,(select count(`s`.`data`) from `simulacoes` `s` where ((`s`.`data` >= (now() - interval 36 hour)) and (`s`.`user_id` = `u`.`id`))) AS `calculosdia_pme`,(select count(`s`.`id`) from `simulacoes` `s` where (`s`.`user_id` = `u`.`id`)) AS `calculos_pme`,(select count(`tg`.`id`) from `tabelas_geradas` `tg` where ((`tg`.`created` >= (now() - interval 36 hour)) and (`tg`.`user_id` = `u`.`id`) and (`tg`.`pdf` is not null))) AS `tabelas_geradas_dia`,(select count(`tg`.`id`) from `tabelas_geradas` `tg` where ((`tg`.`user_id` = `u`.`id`) and (`tg`.`pdf` is not null))) AS `total_tabelas_geradas`,(select count(`pf`.`id`) from `pf_calculos` `pf` where ((`pf`.`created` >= (now() - interval 36 hour)) and (`pf`.`user_id` = `u`.`id`))) AS `calculosdia_pf`,(select count(`pf`.`id`) from `pf_calculos` `pf` where (`pf`.`user_id` = `u`.`id`)) AS `calculos_pf`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 36 hour)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF'))) AS `calculosdia_odonto_pf`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF'))) AS `calculos_odonto_pf`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 36 hour)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ'))) AS `calculosdia_odonto_pj`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ'))) AS `calculos_odonto_pj`,((((select count(`s`.`id`) from `simulacoes` `s` where (`s`.`user_id` = `u`.`id`)) + (select count(`pf`.`id`) from `pf_calculos` `pf` where (`pf`.`user_id` = `u`.`id`))) + (select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF')))) + (select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ')))) AS `total_calculos`,`u`.`id` AS `id`,`u`.`user_pai_id` AS `user_pai_id`,`u`.`imagem_id` AS `imagem_id`,`u`.`username` AS `username`,`u`.`password` AS `password`,`u`.`role` AS `role`,`u`.`nome` AS `nome`,`u`.`sobrenome` AS `sobrenome`,`u`.`created` AS `created`,`u`.`modified` AS `modified`,`u`.`email` AS `email`,`u`.`validacao` AS `validacao`,`u`.`codigo` AS `codigo`,`u`.`celular` AS `celular`,`u`.`tipo` AS `tipo`,`u`.`ultimologin` AS `ultimologin`,`u`.`ultimocalculo` AS `ultimocalculo`,`u`.`data_nascimento` AS `data_nascimento`,`u`.`site` AS `site`,`u`.`whatsapp` AS `whatsapp`,`u`.`facebook` AS `facebook`,`u`.`senha_provisoria` AS `senha_provisoria`,`u`.`estado_id` AS `estado_id`,`u`.`logado` AS `logado`,`u`.`codigo_email` AS `codigo_email`,`u`.`validacao_email` AS `validacao_email`,`e`.`nome` AS `estado_nome`,`i`.`id` AS `id_da_imagem`,`i`.`caminho` AS `imagem_caminho`,`i`.`nome` AS `imagem_nome` from ((((`users` `u` left join `estados` `e` on((`e`.`id` = `u`.`estado_id`))) left join `pf_calculos` `pf` on((`pf`.`user_id` = `u`.`id`))) left join `simulacoes` `s` on((`s`.`user_id` = `u`.`id`))) left join `imagens` `i` on((`i`.`id` = `u`.`imagem_id`))) group by `u`.`id`;


# Replace placeholder table for vw_sms with correct view syntax
# ------------------------------------------------------------

DROP TABLE `vw_sms`;

CREATE ALGORITHM=UNDEFINED DEFINER=`natus_multi`@`%` SQL SECURITY DEFINER VIEW `vw_sms`
AS SELECT
   concat('(',substr(`s`.`destinatario`,3,2),')',' ',substr(`s`.`destinatario`,5,1),' ',substr(`s`.`destinatario`,6,4),'-',substr(`s`.`destinatario`,10,4)) AS `destinatario_enviado`,
   `s`.`id` AS `sms_id`,
   `s`.`destinatario` AS `destinatario`,
   `s`.`created` AS `data_envio`,
   `s`.`cod_resultado` AS `cod_resultado`,
   `s`.`resultado` AS `resultado`,
   `s`.`mensagem` AS `mensagem`,
   `e`.`nome` AS `estado`,
   `u`.`id` AS `id`,
   `u`.`user_pai_id` AS `user_pai_id`,
   `u`.`imagem_id` AS `imagem_id`,
   `u`.`username` AS `username`,
   `u`.`password` AS `password`,
   `u`.`role` AS `role`,
   `u`.`nome` AS `nome`,
   `u`.`sobrenome` AS `sobrenome`,
   `u`.`created` AS `created`,
   `u`.`modified` AS `modified`,
   `u`.`email` AS `email`,
   `u`.`validacao` AS `validacao`,
   `u`.`codigo` AS `codigo`,
   `u`.`celular` AS `celular`,
   `u`.`tipo` AS `tipo`,
   `u`.`ultimologin` AS `ultimologin`,
   `u`.`ultimocalculo` AS `ultimocalculo`,
   `u`.`data_nascimento` AS `data_nascimento`,
   `u`.`site` AS `site`,
   `u`.`whatsapp` AS `whatsapp`,
   `u`.`facebook` AS `facebook`,
   `u`.`senha_provisoria` AS `senha_provisoria`,
   `u`.`estado_id` AS `estado_id`,
   `u`.`logado` AS `logado`,
   `u`.`codigo_email` AS `codigo_email`,
   `u`.`validacao_email` AS `validacao_email`
FROM ((`smsenviados` `s` left join `users` `u` on((`u`.`celular` = convert(concat('(',substr(`s`.`destinatario`,3,2),')',' ',substr(`s`.`destinatario`,5,1),' ',substr(`s`.`destinatario`,6,4),'-',substr(`s`.`destinatario`,10,4)) using utf8)))) left join `estados` `e` on((`e`.`id` = `u`.`estado_id`)));


# Replace placeholder table for vw_calculos with correct view syntax
# ------------------------------------------------------------

DROP TABLE `vw_calculos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_calculos`
AS SELECT
   `u`.`id` AS `user_id`,
   `s`.`id` AS `id`,'SPJ' AS `ramo`,
   `s`.`data` AS `data`,
   `s`.`nome` AS `nome`,
   `st`.`id` AS `status_id`,
   `st`.`nome` AS `status`
FROM ((`users` `u` left join `simulacoes` `s` on((`s`.`user_id` = `u`.`id`))) left join `status` `st` on((`st`.`id` = `s`.`status_id`))) union select `u`.`id` AS `user_id`,`pf`.`id` AS `id`,'SPF' AS `ramo`,`pf`.`created` AS `pf_data`,`pf`.`nome` AS `nome`,`st`.`id` AS `status_id`,`st`.`nome` AS `status` from ((`users` `u` left join `pf_calculos` `pf` on((`pf`.`user_id` = `u`.`id`))) left join `status` `st` on((`st`.`id` = `pf`.`status_id`))) union select `u`.`id` AS `user_id`,`o`.`id` AS `id`,if((`o`.`tipo_pessoa` = 'PF'),'OPF','OPJ') AS `ramo`,`o`.`data` AS `data`,`o`.`nome` AS `nome`,`st`.`id` AS `status_id`,`st`.`nome` AS `status` from ((`users` `u` left join `odonto_calculos` `o` on((`o`.`user_id` = `u`.`id`))) left join `status` `st` on((`st`.`id` = `o`.`status_id`)));


# Replace placeholder table for vw_usuariosold with correct view syntax
# ------------------------------------------------------------

DROP TABLE `vw_usuariosold`;

CREATE ALGORITHM=UNDEFINED DEFINER=`natus_multi`@`%` SQL SECURITY DEFINER VIEW `vw_usuariosold`
AS SELECT
   (select count(`s`.`data`)
FROM `simulacoes` `s` where ((`s`.`data` >= (now() - interval 36 hour)) and (`s`.`user_id` = `u`.`id`))) AS `calculosdia_pme`,(select count(`s`.`id`) from `simulacoes` `s` where (`s`.`user_id` = `u`.`id`)) AS `calculos_pme`,(select count(`tg`.`id`) from `tabelas_geradas` `tg` where ((`tg`.`created` >= (now() - interval 36 hour)) and (`tg`.`user_id` = `u`.`id`) and (`tg`.`pdf` is not null))) AS `tabelas_geradas_dia`,(select count(`tg`.`id`) from `tabelas_geradas` `tg` where ((`tg`.`user_id` = `u`.`id`) and (`tg`.`pdf` is not null))) AS `total_tabelas_geradas`,(select count(`pf`.`id`) from `pf_calculos` `pf` where ((`pf`.`created` >= (now() - interval 36 hour)) and (`pf`.`user_id` = `u`.`id`))) AS `calculosdia_pf`,(select count(`pf`.`id`) from `pf_calculos` `pf` where (`pf`.`user_id` = `u`.`id`)) AS `calculos_pf`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 36 hour)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF'))) AS `calculosdia_odonto_pf`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF'))) AS `calculos_odonto_pf`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 36 hour)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ'))) AS `calculosdia_odonto_pj`,(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ'))) AS `calculos_odonto_pj`,((((select count(`s`.`id`) from `simulacoes` `s` where (`s`.`user_id` = `u`.`id`)) + (select count(`pf`.`id`) from `pf_calculos` `pf` where (`pf`.`user_id` = `u`.`id`))) + (select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF')))) + (select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ')))) AS `total_calculos`,`u`.`id` AS `id`,`u`.`user_pai_id` AS `user_pai_id`,`u`.`imagem_id` AS `imagem_id`,`u`.`username` AS `username`,`u`.`password` AS `password`,`u`.`role` AS `role`,`u`.`nome` AS `nome`,`u`.`sobrenome` AS `sobrenome`,`u`.`created` AS `created`,`u`.`modified` AS `modified`,`u`.`email` AS `email`,`u`.`validacao` AS `validacao`,`u`.`codigo` AS `codigo`,`u`.`celular` AS `celular`,`u`.`tipo` AS `tipo`,`u`.`ultimologin` AS `ultimologin`,`u`.`ultimocalculo` AS `ultimocalculo`,`u`.`data_nascimento` AS `data_nascimento`,`u`.`site` AS `site`,`u`.`whatsapp` AS `whatsapp`,`u`.`facebook` AS `facebook`,`u`.`senha_provisoria` AS `senha_provisoria`,`u`.`estado_id` AS `estado_id`,`u`.`logado` AS `logado`,`u`.`codigo_email` AS `codigo_email`,`u`.`validacao_email` AS `validacao_email`,`e`.`nome` AS `estado_nome`,`i`.`id` AS `id_da_imagem`,`i`.`caminho` AS `imagem_caminho`,`i`.`nome` AS `imagem_nome` from ((((`users` `u` left join `estados` `e` on((`e`.`id` = `u`.`estado_id`))) left join `pf_calculos` `pf` on((`pf`.`user_id` = `u`.`id`))) left join `simulacoes` `s` on((`s`.`user_id` = `u`.`id`))) left join `imagens` `i` on((`i`.`id` = `u`.`imagem_id`))) group by `u`.`id`;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
