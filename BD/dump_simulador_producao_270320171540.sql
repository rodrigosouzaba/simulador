# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: robb0353.publiccloud.com.br (MySQL 5.6.35-80.0-log)
# Base de Dados: natusegweb2_simulacoes
# Tempo de Geração: 2017-03-27 18:41:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela abrangencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `abrangencias`;

CREATE TABLE `abrangencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `abrangencias` WRITE;
/*!40000 ALTER TABLE `abrangencias` DISABLE KEYS */;

INSERT INTO `abrangencias` (`id`, `descricao`, `nome`)
VALUES
	(1,'NACIONAL','NACIONAL'),
	(2,'ESTADUAL','ESTADUAL'),
	(3,'MUNICIPAL','MUNICIPAL');

/*!40000 ALTER TABLE `abrangencias` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela carencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carencias`;

CREATE TABLE `carencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `carencias_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `carencias` WRITE;
/*!40000 ALTER TABLE `carencias` DISABLE KEYS */;

INSERT INTO `carencias` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'15 Dias		Consulta e Exames Simples.\r\n90 Dias		Fisioterapias por Doença.\r\n180 Dias		\"Exames Especiais ( Autorização Prévia), Internações, Cirurgias em Geral, Hemodiálise e Diálise Peritoneal, Radioterapia e Quimioterapia, Cirurgias Cardíacas, Vasculares e Neurológicas.\"\r\n		\r\n300 Dias		Parto.\r\n','BRADESCO HOSPITALAR NACIONAL',NULL),
	(2,'24h a partir da vigência. ','AMIL – PLANO PME 02 À 29 VIDAS e 30 à 99 VIDAS',2),
	(3,'Sem Informação.','BRADESCO ',1),
	(4,'Carências a cumprir (em dias):\r\nN. Vidas   |  A    |    B    |    C      |     D     |    E1   |      E2       |\r\n03 à 09:   |  24h |  15D. | 180D. | 300D. |   180D . |    180D.  |\r\n10 à 20:   |  24h |  15D. |   0D.   | 300D. |     0D.    |  180D.    |\r\n21 à 29 :  |   0    |     0    |    0     |     0    |       0      |       0       |\r\n\r\nA - 24 Horas, para os casos de urgência e emergência.\r\nB - 15 Dias para consultas médicas e exames simples, relacionados nas alíneas “a\" até “g\" do subitem 3.2.2 da Cláusula de Coberturas, das Condições Gerais.\r\nC - 180 Dias para fisioterapia, exceto nos casos de acidente pessoal.\r\nD - 300 Dias para parto a termo.\r\nE - 180 Dias para os demais casos, quais sejam:          \r\n     E1 - Cirurgias em geral e internações clínicas, bem como exames complementares e terapias que necessitem de autorização prévia, conforme disposto na Cláusula Mecanismos de Regulação, das Condições Gerais. Exceção será feita aos procedimentos previstos nas alíneas “c\" e “e2\".\r\n     E2 - Hemodiálise e diálise peritoneal, radioterapia e quimioterapia, transplantes e implantes de qualquer natureza, cirurgias neoplásicas malignas, cirurgias cardíacas, vasculares e neurológicas (inclusive hérnia de disco intervertebral). ','BRADESCO - COMPULSÓRIA',1),
	(5,'N. Vidas |    A   |       B       |     C 1     |     C 2      |\r\n03 à 09  |  24h. |   300 D.   |   180 D.  |   180 D.   |\r\n10 à 20  | 24h.  |   300 D.   |       0      |    180 D.  |\r\n21 à 29  |    0    |   300 D.   |       0      |       0        |\r\n\r\nAs carências serão estabelecidas pela totalidade de segurados contemplados nesta proposta:\r\n\r\nA - 24 Horas para os casos de urgência e emergência.\r\nB - 300 Dias para parto a termo. \r\nC - 180 Dias para os demais casos, quais sejam: \r\n      C1 - Cirurgias em geral e internações clínicas. \r\n            - Exceção será feita aos procedimentos previstos na alínea “c2?\r\n      C2 - Procedimentos considerados especiais, descritos no item 3.1.5 das Condições Gerais, transplantes e implantes de qualquer natureza, cirurgias neoplásicas malignas, cirurgias cardíacas, vasculares e neurológicas (inclusive hérnia de disco intervertebral).\r\n ','BRADESCO - OPCIONAL',1),
	(6,'N. Vidas   |     A     |      B      |         C         |         D         |        E1        |        E2       |\r\n03 à 09     |   24h.  |   15 D.   |     180 D.     |      300 D.    |     180 D.    |     180 D.    |\r\n10 à 20     |   24h.  |   15 D.   |          0         |      300 D.    |         0        |     180 D.    |\r\n21 à 29     |     0     |       0      |          0         |          0        |         0        |         0         |\r\n29 à 99     |     0     |       0      |          0         |          0        |         0        |         0         |\r\n\r\nA - 24 Horas, para os casos de urgência e emergência. \r\nB - 15 Dias para consultas médicas e exames simples, relacionados nas alíneas “a\" até “g\" do subitem 3.2.2 da Cláusula de Coberturas, das Condições Gerais. \r\nC - 180 dias para fisioterapia, exceto nos casos de acidente pessoal. \r\nD - 300 Dias para parto a termo. \r\nE - 180 Dias para os demais casos, quais sejam:         \r\n      E1) Cirurgias em geral e internações clínicas, bem como exames complementares e terapias que necessitem de autorização prévia, conforme disposto na Cláusula Mecanismos de Regulação, das Condições Gerais. Exceção será feita aos procedimentos previstos nas alíneas “c\" e “e2\".       \r\n      E2) Hemodiálise e diálise peritoneal, radioterapia e quimioterapia, transplantes e implantes de qualquer natureza, cirurgias neoplásicas malignas, cirurgias cardíacas, vasculares e neurológicas (inclusive hérnia de disco intervertebral).\r\n \r\n\r\n','BRADESCO 30 à 99 VIDAS',1),
	(7,'Não informado.','CAIXA',5),
	(8,'24h:	Urgência e emergência. \r\n30 Dias: Patologia Clínica, Exame Laboratoriais e Radiologia Simples\r\n60 Dias: Mapeamento de Retina, Anatomopatologia Ocular,Exame de Fundo de Olho.\r\n90 Dias: Ultrassonografia (exceto em Angiologia, Transretal e Vaginal), Eletroencefalograma Convencional.\r\n120 Dias:	Eletroneuromiografia,Provas de função Pulmonar,Mamografia,Liquorologia,Radiodiagnóstico.\r\n180 Dias:	Cirurgias de Pequeno Porte,Fisioterapia,Acupuntura,Quimioterapia,Radioterapia, Hemodiálise e Diálise em caso Renal,Hemoterapia,Ultrassonografia Transretal/Vaginal, Ultrassonografia com Doppler com fluxo a cores,Holter,MAPA,Teste Ergométrico, Ecocardiograma, Ressonância Magnética, Tomografia.\r\n300 Dias:	Parto a termo, exceto partos prematuros.\r\n','CASSEB',6),
	(9,'24 Horas: Urgência e/ou Emergência no Hosp. Teresa de Liseux (Av. Acm),\r\n30 Dias: Consultas e exames básicos (exceto hormônicos e imunológicos), ECG, Rx simples, preventivo  ginecológico,  tonometria.\r\n120 Dias:	Demais procedimentos de alto custo, USG,tomografia, EEG,testes alérgicos, endoscopia,mamografia,etc.\r\n180 Dias: Internações eletivas  clin. E cirúrgicas, fisioterapias, quimioterapia, radioterapia, hemodiálise e  (renal), litotripsia, hemoterapia, medicina nuclear, USG, tomografia;\r\n300 Dias:	Parto;\r\n24 Meses: CPT - Cobertura Parcial Temporária.\r\n','HAPVIDA ',10),
	(10,'24 Horas: Urgência e Emergência na Segmentação Ambulatorial.\r\n30 Dias: Consultas e Exames Básicos\r\n90 Dias: Exames Especiais\r\n180 Dias: Internações\r\n24 Meses: Cobertura Parcial Temporária para Eventos Cirúrgicos, Leitos de Alta Tecnologia e Procedimentos de Alta Complexidade relacionados com Doenças ou Lesões Preexistentes.\r\n','AMI',26),
	(11,'24 Horas: Acidentes pessoais, urgência, e/ou emergência\r\n30 Dias: Consultas e exames simples\r\n180 Dias:	Exames complementares, cirurgia e internamento\r\n300 Dias:	Parto (Planos com Obstetrícia)\r\n720 Dias:	Pré existência\r\n','UNIÃO MÉDICA ',25),
	(12,'Não Informado.','UNIMED CNU',17),
	(13,'NÃO COMPRAMOS CARÊNCIAS. \r\nNo ato da contratação, isnetamos as carências das consultas médicas e exames simples.\r\n\r\n6 Meses: Demais Procedimentos\r\n6 Meses: Internações Clinicas ou Cirúrgicas\r\n10 Meses: Partos (Contratos Hospitalares)\r\n24 Meses: Cobertura Parcial Temporária - Pré-Existência','VITALLIS SAÚDE ',23),
	(14,'24 Horas		Urgência e Emergência;\r\n30 Dias: Patologia Clínica, Exames Laboratoriais e Radiologia Simples;\r\n60 Dias: Mapeamento de Retina, Anatomopatologia Ocular,Exame de Fundo de Olho;\r\n90 Dias: Ultrassonografia (Exceto em Angiologia, Transretal e Vaginal), Eletroencefalograma Convencional;\r\n120 Dias:	Eletroneuromiografia, Provas de Função Pulmonar, Mamografia, Liquorologia, Radiodiagnóstico;\r\n180 Dias: Cirurgias de Pequeno Porte, Fisioterapia, Acupuntura, Quimioterapia, Radioterapia, Hemodiálise e Diálise em Caso Renal, Hemoterapia, Ultrassonografia Transretal/Vaginal, Ultrassonografia com Doppler com Fluxo a Cores, Holter, MAPA, Teste Ergométrico, Ecocardiograma, Ressonância Magnética, Tomografia.','BOA SAÚDE',4),
	(15,'Cumprimento normal dos prazos de carências, com redução para os casos oriundos de outras Operadoras.\r\n\r\nIndependente do número de vidas, as inclusões após o prazo de 30 dias obedecerão as carências contratuais, prevista das Condições Gerais do produto contratado.\r\n\r\n24 Horas: Atendimento de Urgência e Emergência \r\n15 Dias: Consulta\r\n15 Dias: Exames Básicos\r\n30 Dias: Exames Especiais\r\n30 Dias: Procedimentos Ambulatoriais Especiais\r\n180 Dias: Internações Clínicas\r\n180 Dias: Internações Psiquiátricas\r\n180 Dias: Internações Psiquiátricas em Hospital Dia\r\n180 Dias: Internações Cirúrgicas\r\n180 Dias: Atendimento Médico/Hospitalar relacionado à internação obstétrica, exceto parto à internação à termo.\r\n180 Dias: Transplantes\r\n180 Dias: Assistência Domiciliar\r\n300 Dias: Parto à termo\r\n','UNIMED SEGUROS - 04 à 09 VIDAS',18),
	(16,'Cumprimento normal dos prazos de carências, com redução para os casos oriundos de outras Operadoras, exceto Parto  à Termo.\r\n\r\nIndependente do número de vidas, as inclusões após o prazo de 30 dias obedecerão as carências contratuais, prevista das Condições Gerais do produto contratado.\r\n\r\n24 Horas: Atendimento de Urgência e Emergência \r\n15 Dias: Consulta\r\n15 Dias: Exames Básicos\r\n30 Dias: Exames Especiais\r\n30 Dias: Procedimentos Ambulatoriais Especiais\r\n120 Dias: Internações Clínicas\r\n120 Dias: Internações Psiquiátricas\r\n120 Dias: Internações Psiquiátricas em Hospital Dia\r\n120 Dias: Internações Cirúrgicas\r\n120 Dias: Atendimento Médico/Hospitalar relacionado à internação obstétrica, exceto parto à internação à termo.\r\n120 Dias: Transplantes\r\n180 Dias: Assistência Domiciliar\r\n300 Dias: Parto à termo\r\n','UNIMED SEGUROS - 10 à 29 VIDAS',18),
	(17,'Não haverá aplicação de carência para os Segurados inscritos dentro do prazo.\r\n\r\nIndependente do número de vidas, as inclusões após o prazo de 30 dias obedecerão as carências contratuais, prevista das Condições Gerais do produto contratado.','UNIMED SEGUROS - 30 à 99 VIDAS',18),
	(18,'Não Informado.','ONE HEALTH (GRUPO AMIL)',13);

/*!40000 ALTER TABLE `carencias` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `estados`;

CREATE TABLE `estados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;

INSERT INTO `estados` (`id`, `nome`)
VALUES
	(1,'Acre'),
	(2,'Alagoas'),
	(3,'Amapá'),
	(4,'Amazonas'),
	(5,'Bahia'),
	(6,'Ceará'),
	(7,'Distrito Federal'),
	(8,'Espírito Santo'),
	(9,'Goiás'),
	(10,'Maranhão'),
	(11,'Mato Grosso'),
	(12,'Mato Grosso do Sul'),
	(13,'Minas Gerais'),
	(14,'Pará'),
	(15,'Paraíba'),
	(16,'Paraná'),
	(17,'Pernambuco'),
	(18,'Piauí'),
	(19,'Rio de Janeiro'),
	(20,'Rio Grande do Norte'),
	(21,'Rio Grande do Sul'),
	(22,'Rondônia'),
	(23,'Roraima'),
	(24,'Santa Catarina'),
	(25,'São Paulo'),
	(26,'Sergipe'),
	(27,'Tocantins');

/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela imagens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `imagens`;

CREATE TABLE `imagens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caminho` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `imagens` WRITE;
/*!40000 ALTER TABLE `imagens` DISABLE KEYS */;

INSERT INTO `imagens` (`id`, `nome`, `caminho`, `created`, `modified`, `status`)
VALUES
	(1,'bradesco.jpg','uploads/imagens/','2017-02-13 17:01:19','2017-02-13 17:01:19',1),
	(2,'Bradesco-saude-Logo.png','uploads/imagens/','2017-02-13 17:04:16','2017-02-13 17:04:16',1),
	(3,'amil logo.jpg','uploads/imagens/','2017-02-13 17:04:52','2017-02-13 17:04:52',1),
	(4,'cnu pme.jpg','uploads/imagens/','2017-02-13 17:13:22','2017-02-13 17:13:22',1),
	(5,'resize-240x147_saude-casseb-3.jpg','uploads/imagens/','2017-03-14 12:02:17','2017-03-14 12:02:17',1),
	(6,'logo.png','uploads/imagens/','2017-03-14 12:02:59','2017-03-14 12:02:59',1),
	(7,'ami1.png','uploads/imagens/','2017-03-15 11:27:38','2017-03-15 11:27:38',1),
	(8,'unimed_feira_jovem_aprendiz.jpg','uploads/imagens/','2017-03-15 14:11:51','2017-03-15 14:11:51',1),
	(9,'boasaude.png','uploads/imagens/','2017-03-16 09:53:12','2017-03-16 09:53:12',1),
	(10,'hapvida.png','uploads/imagens/','2017-03-16 10:01:37','2017-03-16 10:01:37',1),
	(11,'one.png','uploads/imagens/','2017-03-16 10:02:21','2017-03-16 10:02:21',1),
	(12,'sulamerica.png','uploads/imagens/','2017-03-16 10:02:50','2017-03-16 10:02:50',1),
	(13,'unnamed.png','uploads/imagens/','2017-03-16 10:03:24','2017-03-16 10:03:24',1),
	(14,'unnamed.png','uploads/imagens/','2017-03-16 10:03:36','2017-03-16 10:03:36',1),
	(15,'seguros-unimed2.png','uploads/imagens/','2017-03-16 10:05:10','2017-03-16 10:05:10',1),
	(16,'uni-3-1.jpg','uploads/imagens/','2017-03-16 10:06:21','2017-03-16 10:06:21',1),
	(17,'logo_vitallis-slide.png','uploads/imagens/','2017-03-16 10:07:16','2017-03-16 10:07:16',1);

/*!40000 ALTER TABLE `imagens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela informacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `informacoes`;

CREATE TABLE `informacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(155) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `informacoes_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `informacoes` WRITE;
/*!40000 ALTER TABLE `informacoes` DISABLE KEYS */;

INSERT INTO `informacoes` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'Documentos Necessários: Último Contrato Social da Empresa, com as devidas alterações, ou Estatutos e Atas no caso de grupos específicos.\r\nFGTS atualizado e quitado, relação de empregados e guia de recolhimento, CNPJ e CRF.\r\nCartões (Ficha de Inclusão) devidamente preenchidos e assinados pelo proponente\r\nPara proponentes com mais de 65 anos é necessário enviar exame realizado pelo médico da Bradesco Saúde\r\nProposta Mestra com a assinatura de um representante legal e carimbos do Estipulante e do Corretor\r\nCCB quitada, exceto nos casos que necessitem de análise prévia.\r\n','BRADESCO HOSPITALAR NACIONAL',NULL),
	(2,'Documentos da Empresa: Cópia do Contrato Social da Empresa, Cópia da Listagem de FGTS da Empresa (com até 2 Meses Retroativos), Cópia do Documento de Identificação (com Foto e Assinatura) do Responsável Legal da Empresa, Cópia de Cartão CNPJ da Empresa. Formulário de Solicitação de Adesão Empresa até 99 Vidas. \r\n','AMIL – PLANO PME 02 À 29 VIDAS',2),
	(3,'Documentos da Empresa: Cópia do Contrato Social da Empresa, Cópia da Listagem de FGTS da Empresa (com até 2 Meses Retroativos), Cópia do Documento de Identificação (com Foto e Assinatura) do Responsável Legal da Empresa, Cópia de Cartão CNPJ da Empresa. Formulário de Solicitação de Adesão Empresa até 99 Vidas. ','AMIL – PLANO PME 30 À 99 VIDAS',2),
	(4,'Último Contrato Social da Empresa, com as devidas alterações, ou Estatutos e Atas no caso de grupos específicos. \r\nFGTS atualizado e quitado, relação de empregados e guia de recolhimento, CNPJ e CRF.\r\nCartões (Ficha de Inclusão) devidamente preenchidos e assinados pelo proponente. \r\nPara proponentes com mais de 65 anos é necessário enviar exame realizado pelo médico da Bradesco Saúde.\r\nProposta Mestra com a assinatura de um representante legal e carimbos do Estipulante e do Corretor.\r\nCCB quitada, exceto nos casos que necessitem de análise prévia.\r\n','BRADESCO ',1),
	(5,'EMPRESA: Proposta assinada e carimbada, FGTS + quitação + relação de empregados, Contrato Social e alteração, se houver, CNPJ, RG e CPF do responsável pela empresa.\r\nSEGURADO: Contrato Social e alteração, se houver, CNPJ, RG e CPF do responsável pela empresa.\r\n\r\n\r\n','CAIXA',5),
	(6,'TITULAR: RG, CPF e Comprovante de Residência (Exceto Embasa e Coelba).\r\nEMPRESA:  Contrato social e última alteração consolidada,  RG/CPF e comprovante de residência  dos sócios, Comprovante de endereço da empresa, FGTS completo e quitado e CNPJ.','CASSEB',6),
	(7,'Empresa:\r\nCópia do contrato Social(última alteração) ou declaração de firma individual.\r\nCópia do CNPJ / relação do FGTS (GFIP) / carta de responsabilidade assinada pelo proprietário.\r\n\r\nTitular: \r\nCópia do RG, CPF e comprovante de endereço (exceto conta de água);\r\n\r\n','HAPVIDA ',10),
	(8,'Cadastro de Novas Empresas:\r\nProposta Contratual com todas as informações devidamente preenchidas, assinada e carimbada pelo representante da empresa.\r\nCópia do Contrato Social ou estatuto da empresa (Aditivos).\r\nCópia do RG e CPF do responsável pela empresa.\r\nCópia do comprovante de Endereço do responsável pela empresa.\r\nGFIP juntamente com o comprovante de pagamento. \r\nCópia do CNPJ (Atualizado).\r\nCópia do comprovante de Endereço no nome da empresa.\r\n \r\nInclusão de Titulares:\r\nDeclaração de Saúde preenchida e assinada pelo proponente.\r\nCópia do RG.\r\nCópia do CPF ou CNH dentro do prazo de validade.\r\nComprovante de Endereço. \r\nEntrevista qualificada (conforme critérios AMI).\r\nPreenchimento do Layout.\r\nCartão do SUS.','AMI',26),
	(9,'Cópia do CNPJ, Contrato Social, e comprovante de residência.\r\nCópia do RG, CPF, Cartão do SUS, Contra-cheque e contrato de trabalho.\r\nCertidão de Nascimento (Crianças).','UNIÃO MÉDICA ',25),
	(10,'Empresa: Empresa Cópia do Cartão CNPJ atualizado, Cópia do RG e CPF dos sócios, Cópia do Contrato Social e alterações.\r\nSócios: Cópia do Contrato Social com a última alteração;\r\nColaborador: Cópia do FGTS/ Carteira de Trabalho;\r\nEstagiário: Cópia do contrato de estágio e carta assinada pela instituição de ensino;\r\nPrestador de Serviço: Cópia de contrato de prestação de serviços entre as partes;\r\nMicroempreendedor Individual: Somente após 6 meses da abertura, Cópia de registro CNPJ ','UNIMED CNU',17),
	(11,'Contrato Emitido assinado em duas vias. \r\nCNPJ com pelo menos 8 meses de cadastro vigente; Contrato Social da Empresa.\r\nCópia dos Doc. Pessoais do responsável pela empresa.\r\nCópia dos Doc. Dos participantes do plano, incluindo Cartão do Sus.\r\nPlanilha para Cadastro.\r\nÚltima GFIP - no máximo do mês anterior à entrega dos documentos.\r\n\r\nEmpresa: Cópia do Contrato Social e alterações, cópia do cartão CNPJ atualizado, cópia do RG e CPF dos sócios, Inscrição Estadual (quando houver), comprovante de endereço da empresa, telefone e e-mail.\r\nFuncionários Titulares: Comprovação de vínculo empregatício, comprovante de endereço.\r\nTitulares todas as idades e dependentes a partir de 18 anos de idade: RG e CPF, comprovante de endereço.\r\n\r\n\r\n','UNIMED FEIRA - 02 à 29 VIDAS',19),
	(12,'Cônjuge: Cópias do RG + CPF + certidão de casamento.\r\nCompanheiro(a): Cópia do RG + CPF, Declaração de União Estável padrão Unimed ou expedido por órgão oficial, contendo o n° do RG e do CPF do(a) companheiro(a), endereço, tempo de convívio, n° do RG e assinatura de duas testemunhas, firma reconhecida do titular e do(a) companheiro(a).\r\nFilho(a) solteiro(a) menores de 24 anos : Cópias RG + CPF. Para recém nascidos, a certidão de nascimento.\r\nFilho(a) inválido(a) de qualquer idade: Cópias RG + CPF + certidão de invalidez emitida pelo INSS.\r\nEnteado(a) solteiro(a) menores de 24 anos: Cópias RG + CPF do(a) enteado(a) + certidão de óbito, de um dos genitores ou declaração judicial de ausência.\r\nMenor sob guarda ou tutela do segurado titular solteiro(a) menores de 24 anos: Cópias RG + CPF do(a) tutelado(a), tutela ou do \"termo de guarda\" expedido por órgão oficial.\r\n\r\n** Para titulares e dependentes com idade a partir de 16 anos, solicita-se cópia do CPF.\r\n*** Caso o beneficiário possua o cartão do SUS, poderá fornecer à Operadora.','UNIMED FEIRA - 03 à 29 VIDAS',19),
	(13,'Empresa:\r\nCartão de CNPJ.\r\nContrato Social.\r\nComprovante de endereço.\r\nFicha de inscrição Estadual/Municipal.','VITALLIS SAÚDE ',23),
	(14,'Documentação da Empresa: Contrato Social e alterações, Cartão CNJP, Relação de FGTS completa e atual, RG e CPF do sócio administrador, Comprovante de endereço da empresa.\r\nDocumentação dos Funcionários: RG, CPF, comprovante de residencia.\r\n','UNIMED SEGUROS',18),
	(15,'Empresa:\r\nCópia do contrato Social(última alteração) ou declaração de firma individual;\r\nCópia do CNPJ / relação do FGTS (GFIP) / carta de responsabilidade assinada pelo proprietário;\r\n','HAPVIDA AFIX',27),
	(16,'Empresa: \r\nContrato Social e última alteração consolidada, RG/CPF  e comprovante de residência dos sócios, comprovante de endereço da empresa, FGTS completo e quitado e CNPJ.\r\nObs: Obrigatório CPF da Criança (titular)\r\n','BOA SAÚDE',4),
	(17,'Cópia do Contrato Social da Empresa, Cópia da Listagem de FGTS da Empresa (com até 2 Meses Retroativos), Cópia do Documento de Identificação (Com Foto e Assinatura) do Responsável Legal da Empresa, Cópia de Cartão CNPJ da Empresa. \r\nFormulário de Solicitação de Adesão Empresa até 99 Vidas.\r\n','ONE HEALTH (GRUPO AMIL)',13);

/*!40000 ALTER TABLE `informacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela modalidades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modalidades`;

CREATE TABLE `modalidades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `modalidades` WRITE;
/*!40000 ALTER TABLE `modalidades` DISABLE KEYS */;

INSERT INTO `modalidades` (`id`, `nome`, `descricao`)
VALUES
	(1,'ADESÃO ',''),
	(2,'INDIVIDUAL',''),
	(3,'PME','');

/*!40000 ALTER TABLE `modalidades` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela observacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `observacoes`;

CREATE TABLE `observacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `observacoes_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `observacoes` WRITE;
/*!40000 ALTER TABLE `observacoes` DISABLE KEYS */;

INSERT INTO `observacoes` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'IOF: 2,38%  /  TAXA DE CADASTRO*: R$ 7,50 POR VIDA  (COBRADA NO 1º PAGAMENTO) \r\nO cliente pode mesclar os planos dos segurados TITULARES.\r\n\r\nBRADESCO DENTAL: Odontologia – Abrangência Nacional  / Plano Cobertura Padrão: R$ 19,56 por pessoa deverão aderir todas as pessoas incluídas no SPG.\r\nPERÍCIA A PARTIR DE 65 ANOS: Dr.José Alberto da Matta - 3353-3261/3351-5704 ;  Dr.Valdir Ayres - 2109-2216/2217; Dr.Thiers Chagas – 3331-0780.\r\nIMPORTANTE: No Plano PERFIL o reembolso tem livre escolha de prestadores, respeitando os valores da tabela  de honorários e serviços da Bradesco Saúde.\r\n\r\n\r\n\r\n','BRADESCO HOSPITALAR NACIONAL',NULL),
	(3,'Quem Pode Aderir: Beneficiários, Sócios e Seus Respectivos Dependentes, Funcionários com Vínculo Empregatício e Consanguíneos (Cunhados, Enteados, Filhos, Irmãos, Netos, Pais e Sogros).\r\n\r\nTaxa de Cadastro: R$ 50,00 por Contrato Plano PME.\r\n\r\nAmil Resgate Saúde: R$ 20,00\r\nAssistência Multiviagem ao Exterior: R$ 20,00','AMIL – PLANO PME 02 À 29 VIDAS',2),
	(4,'Quem Pode Aderir: Beneficiários, Sócios e Seus Respectivos Dependentes, Funcionários com Vínculo Empregatício e Consanguíneos (Cunhados, Enteados, Filhos, Irmãos, Netos, Pais e Sogros).\r\n\r\nTaxa de Cadastro: R$ 50,00 por Contrato Plano PME.\r\n\r\nAmil Resgate Saúde: R$ 20,00\r\nAssistência Multiviagem ao Exterior: R$ 20,00\r\n\r\n','AMIL – PLANO PME 30 À 99 VIDAS',2),
	(5,'IOF: 2,38% / TAXA DE CADASTRO*: R$ 7,50 POR VIDA (COBRADA NO 1º PAGAMENTO) O cliente pode mesclar os planos dos segurados TITULARES.\r\n\r\nBradesco Dental: \r\n- Odontologia – Abrangência Nacional. \r\n- Plano Cobertura Padrão: R$ 19,56 por pessoa deverão aderir todas as pessoas incluídas no SPG.\r\n\r\nIMPORTANTE: No Plano PERFIL o reembolso tem livre escolha de prestadores, respeitando os valores da tabela de honorários e serviços da Bradesco Saúde.\r\n','BRADESCO ',1),
	(6,'Não informado.','CAIXA',5),
	(7,'ESPECIALIDADES CASSEB  ESPECIALIDADES: Especialidades do Centro Médico Casseb: Acupuntura, Alergologia, Angiologia, Audiometria, Cardiologia, Cirurgia Geral, Cirurgia Cabeça e Pescoço, Clinica da dor, Clinica Médica, Dermatologia, Endocrinologia, Fonoaudiologia, Gastroentologia, Geriatria, Ginecologia, Laborátorio de Analises Clínicas, Neurologia, Nutrição, Mamografia, Obstetrícia, Oftalmologia, Ortopedia / Traumatologia, Otorrinolaringologia, Pediatria, Perícia Médica, Pneumologia, Proctologia, Psicologia, Psiquiatria, Raio X, Terapia Ocupacional, Ultrassonografia, Urologia.\r\n\r\nENTREVISTA QUALIFICADA:\r\nObrigatória: Para crianças até 6 anos, 11meses e 29 dias, mulheres e  homens  a partir dos 45 anos. \r\nMarcação: (71) 3444-0500 - Saúde Casseb. \r\nObrigatório: Teste do pezinho(Crianças até 1 ano, 11 meses e 29 dias).\r\nCartão de Vacina: Crianças até 1 ano, 11 meses e 29 dias). \r\n','CASSEB - SEM COPARTICIPAÇÃO',6),
	(8,'ESPECIALIDADES CASSEB  ESPECIALIDADES: Especialidades do Centro Médico Casseb: Acupuntura, Alergologia, Angiologia, Audiometria, Cardiologia, Cirurgia Geral, Cirurgia Cabeça e Pescoço, Clinica da dor, Clinica Médica, Dermatologia, Endocrinologia, Fonoaudiologia, Gastroentologia, Geriatria, Ginecologia, Laborátorio de Analises Clínicas, Neurologia, Nutrição, Mamografia, Obstetrícia, Oftalmologia, Ortopedia / Traumatologia, Otorrinolaringologia, Pediatria, Perícia Médica, Pneumologia, Proctologia, Psicologia, Psiquiatria, Raio X, Terapia Ocupacional, Ultrassonografia, Urologia.\r\n\r\nENTREVISTA QUALIFICADA:\r\nObrigatória: Para crianças até 6 anos, 11meses e 29 dias, mulheres e  homens  a partir dos 45 anos. \r\nMarcação: (71) 3444-0500 - Saúde Casseb. \r\nObrigatório: Teste do pezinho(Crianças até 1 ano, 11 meses e 29 dias).\r\nCartão de Vacina: Crianças até 1 ano, 11 meses e 29 dias). \r\n\r\nVALORES DE COPARTICIPAÇÃO:\r\nConsultas Eletivas: R$ 21,00\r\nConsultas P/S: R$ 35,00\r\nExames Simples: R$ 6,00\r\nExames Complexos: R$ 30,00\r\n\r\nCMC - CENTRO MÉDICO CASSEB\r\nConsultas Eletivas: R$ 8,00\r\nExames Simples: R$ 6,00\r\nExames Complexos: R$ 15,00\r\n\r\nObs: Valores aproximados que podem variar conforme procedimentos realizados e locais de atendimento.*\r\n','CASSEB - COM COPARTICIPAÇÃO',6),
	(9,'Taxa de Adesão: R$ 10,00 por usuário.\r\n\r\nATENÇÃO:\r\nBeneficiários são sócios e seus respectivos dependentes, funcionários com vínculo empregatício e consaguineos (cunhados, enteados, filhos, irmãos, netos, pais e sogros) até 43 anos, exceto sobrinhos sendo  até os 18 anos. \r\nHaverá isenção total das carências nas empresas acima de 30 usuários. \r\n\r\nPROCEDIMENTO\r\nConsultas Eletivas: R$ 9,58\r\nConsultas de Urgências: R$ 15,85\r\nExames Simples: R$ 7,87\r\nExames Complexos: R$ 47,77\r\n\r\nNOSSO PLANO: Internações em hospitais da Rede própria,consultas nas Hapclínicas e exames nas clínicas Vida & Imagem, atendimento eletivo na Rede própria Norte e Nordeste e de urgência e  emergência em todo Brasil através do ABRAMGE. \r\n\r\nMIX: Internações nos hospitais da Rede própria,exames  clinicas Vida & Imagem,atendimento eletivo para  consultas na rede credenciada Norte e Nordeste,urgência e emergência em todo Brasil através do ABRAMGE.\r\n\r\n\r\n\r\n','NOSSO PLANO ',10),
	(11,'TX. ADESÃO: R$15,00 \r\n \r\nAssistência Médica ¹: Plano de assistência médica com desconto no valor da mensalidade, em caso de contratação concomitante do plano de assistência odontológica.\r\nAssistência Médica ²: Plano de assistência médica sem desconto no valor da mensalidade, em caso de não contratação concomitante do plano de assistência odontológica.\r\n\r\n- A contratação do plano de assistência odontológica será realizada por instrumento próprio, em termo de adesão distinto do plano de assistência médica.\r\n- Os planos de assistência médica para grupos familiares, a partir de 02 (duas) vidas, poderão sofrer desconto de até 5% (cinco por cento) no valor de sua mensalidade no ato da contratação.\r\n - Os planos de assistência médica para grupos familiares, a partir de 04 (quatro) vidas, poderão sofrer desconto de até 10% (dez por cento) no valor de sua mensalidade no ato da contratação.\r\n- Os preços contidos na tabela dos produtos \"Nosso Plano\" e \"Mix\" abrangem exclusivamente a cobertura de assistência médica. Os valores relativos aos produtos de assistência odontológica estão contidos na tabela \"Planos Odontológicos\".\r\n\r\nAtenção: \r\n- No verso desta folha estão contidos os índices de reajuste por mudança de faixa etária, permitindo assim o pleno conhecimento da mudança do valor de sua mensalidade em decorrência da variação de idade.     \r\n- Para os descontos do Plano Familiar, só será considerada a quantidade de beneficiários inscritos no ato da contratação. Casa haja inclusões posteriores à contratação, não haverá incidência de novo desconto. Caso haja cancelamento posterior à contratação e que faça o contrato não se classificar mais no grupo de desconto, o mesmo será reduzido ou até mesmo deixará de ser aplicado.','NOSSO PLANO - INDIVIDUAL - SEM COPARTICIPAÇÃO',NULL),
	(14,'ACIMA DE 59 ANOS: VENDA ADMINISTRATIVA.\r\nTaxas de Cadastro: R$ 10,00 (Dez Reais) por vida.\r\nConsulte prêmio médio para empresa de 30 a 90 vidas (sujeito à aprovação, com mudança de regra de comissionamento).\r\n\r\nTaxa de Coparticipação:\r\nConsultas: R$ 20,00\r\nConsultas de Urgência/Emergência: R$30,00\r\nDemais Procedimentos (Exceto Internação): 20%','AMI',26),
	(15,'Taxa de adesão empresarial: R$ 60,00\r\n\r\nCoparticipação de 30% sobre exames, limitados aos respectivos valores: \r\nExames Simples: R$ 4,30\r\nExames Complementares: R$ 31,50\r\n\r\nCoparticipação: R$ 25,00 por consulta\r\n','UNIÃO MÉDICA ',25),
	(16,'Taxa de Implantação : R$ 10,00 (por beneficiário).\r\nQuem pode aderir: Sócios, Colaborador, Estagiário, Prestador de Serviço, Microempreendedor Individual.\r\n\r\nValores de Coparticipação:\r\nConsultas Eletivas: R$ 25,00\r\nConsultas P/S: R$ 35,00\r\nExames Simples: R$ 8,00\r\nExames Complexos: R$ 40,00\r\nFranquia internação: R$ 60,00\r\nPlano Básico: R$ 60,00\r\nFranquia internação: R$ 90,00\r\nPlano Especial: R$ 90,00\r\n\r\nRELAÇÃO DE CONGÊNERES: Allianz, Amil, Care Plus, Bradesco Saúde, Golden Cross, Lincx, Marítima ,Medial, Notre Dame, Omint, Porto Seguro.\r\nDocumentação necessária: Cópia das carteirinhas, 3 últimos boletos pagos e comprovação de início da vigência.','UNIMED CNU',17),
	(17,'Coparticipação em consultas: R$ 23,00','UNIMED FEIRA - 02 à 29 VIDAS',NULL),
	(18,'Coparticipação em consultas: R$ 23,00','UNIMED FEIRA - 02 à 29 VIDAS',19),
	(19,'Para vidas acima de 58 anos, consultar a operadora.\r\n\r\nCoparticipativo em consultas: R$ 20,00\r\n\r\n','UNIMED FEIRA - 03 à 29 VIDAS',19),
	(20,'VENDAS PERMITIDA APENAS NAS CIDADES DE ATUAÇÃO DO PRODUTO.\r\n\r\nCNPJ - Sendo mínimo um ano no contrato social da empresa.\r\nMínimo de 2 Titulares.\r\n\r\nCoparticipação:\r\nConsultas Eletivas: R$ 20,00\r\nConsultas em pronto soccoro: R$ 40,00\r\nExames e procedimentos ambulatoriais: R$ 6,00','VITALLIS SAÚDE ',23),
	(21,'Este estudo não contempla funcionários afastados, agregados, demitidos, aposentados, aposentados por invalidez, gestantes e remidos.\r\nEste estudo não considera casos crônicos, em tratamento, internados e home care.\r\n\r\nBreak Even de 70%.\r\n100% Compulsório.\r\nSeguro Saúde PME Corporativo – Seguros Unimed de 04 à 99 Vidas, Sendo 02 Titulares.\r\nCo-participação de 20% em Consultas e Exames Simples, Revertida para à Seguros Unimed.\r\n\r\nCarregamentos:\r\nAgenc.: 200%\r\nCorretagem: 5%\r\n\r\nAs tabelas prêmios apresentadas têm validade apenas para as vidas na BAHIA, havendo vidas em outros estados, por gentileza, informar a auantidade por UF para realizarmos um novo estudo.\r\n\r\nAs tabelas não são válidas para a quantidade de vida femininas acima de 60% e também não estarão válidas caso haja concentração igual ou superior a 10% de vidas na última faixa etária  (59 Anos ou Mais). Para ambos os casos, favor enviar o pedido para análise do escritório regional.\r\n\r\n\r\n','UNIMED SEGUROS ',18),
	(22,'Taxa de Adesão: R$ 10,00 por usuário.\r\n\r\nATENÇÃO:\r\nBeneficiários são sócios e seus respectivos dependentes, funcionários com vínculo empregatício e consaguineos (cunhados, enteados, filhos, irmãos, netos, pais e sogros) até 43 anos, exceto sobrinhos sendo  até os 18 anos. \r\nHaverá isenção total das carências nas empresas acima de 30 usuários. \r\n\r\nPROCEDIMENTO:\r\nConsultas Eletivas: R$ 19,15\r\nConsultas de Urgências: R$ 23,83\r\nExames Simples: R$ 7,87\r\nExames Complexos: R$ 47,77\r\n\r\nNOSSO PLANO: Internações em hospitais da Rede própria,consultas nas Hapclínicas e exames nas clínicas Vida & Imagem, atendimento eletivo na Rede própria Norte e Nordeste e de urgência e  emergência em todo Brasil através do ABRAMGE. \r\n\r\nMIX: Internações nos hospitais da Rede própria,exames  clinicas Vida & Imagem,atendimento eletivo para  consultas na rede credenciada Norte e Nordeste,urgência e emergência em todo Brasil através do ABRAMGE.\r\n\r\n','MIX',10),
	(23,'Taxa de Adesão: R$ 10,00 por usuário.\r\n\r\nQuem pode aderir:\r\nEmpresas que estejam ativas na Receita Federal na data de Assinatura do Termo de Adesão Affix.\r\nTermo e documentos necessários tenham sido recebidos e aprovados nos prazos.\r\nNão terem Contrato ativo na Hapvida.\r\n\r\nDiferenciais dos planos: \r\nNOSSO PLANO: Internações em hospitais da Rede própria,consultas nas Hapclínicas e exames nas clínicas Vida & Imagem, atendimento eletivo na Rede própria Norte e Nordeste e de urgência e emergência em todo Brasil através do ABRAMGE. \r\n\r\nMIX: Internações nos hospitais da Rede própria, exames clinicas Vida & Imagem,atendimento eletivo para consultas na rede credenciada Norte e Nordeste,urgência e emergência em todo Brasil através do ABRAMGE.\r\n\r\n\r\n','HAPVIDA AFIX',27),
	(24,'Para pagamento das mensalidades em Débito em conta do plano ambulatorial, os bancos autorizados são:\r\nITAÚ E BRADESCO\r\n\r\nCoparticipação¹ de 30%: Em consultas, exames terapias e outros. Atendimento ambulatoriais.\r\n','BOA SAÚDA - COM COPARTICIPAÇÃO',4),
	(25,'Para pagamento das mensalidades em Débito em conta do plano ambulatorial, os bancos autorizados são:\r\nITAÚ E BRADESCO\r\n','BOA SAÚDE - SEM COPARTICIPAÇÃO',4),
	(26,'Aditivo - Claching em Saúde: R$14,00 Por Beneficiário\r\nTaixa de Cadastro: 	R$50,00\r\nPreços e Planos Válidso, a Partir de 01/02/2017 até 31/05/2017, Exclusivamente Para Contratos PJ 015-1 O e Para Contratação em Todos os Estados do Brasil, Exteto Bahia.		\r\n\r\nQuem Pode Aderir: \"Beneficiários, Sócios e Seus Respectivos Dependentes, Funcionários Com Vínculo Empregatício e Consaguineos  (Cunhados, Enteados, Filhos, Irmãos, Netos, Pais e Sogros).\"\r\n		\r\nVantagens: \r\nReembolso em até 24 Horas.\r\nAtendimento Diferenciado.\r\nAplicativo One Health Mobile.\r\nAbrangência Nacional.\r\nColeta Domiciliar de Exames.\r\nCoberturas de Vacinas.\r\nResgate Saúde (Válido para RJ e SP).\r\nAssistência Viagem Internacional.\r\n','ONE HEALTH (GRUPO AMIL)',13);

/*!40000 ALTER TABLE `observacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela opcionais
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opcionais`;

CREATE TABLE `opcionais` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `opcionais_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `opcionais` WRITE;
/*!40000 ALTER TABLE `opcionais` DISABLE KEYS */;

INSERT INTO `opcionais` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'Dependentes: Serão aceitos os filhos até 29 anos completos, independente de ser ou não universitário.\r\nOs filhos inválidos com qualquer idade, desde que dependentes para efeito de IR.\r\nÚltimo Contrato Social da Empresa, com as devidas alterações, ou Estatutos e Atas no caso de grupos específicos.\r\n\r\n','BRADESCO HOSPITALAR NACIONAL',NULL),
	(2,'Cônjuge até 68 Anos: RG, Declaração de União Estável, Documento de Identificação de Filhos ou Certidão de Casamento. \r\n\r\nOs demais Dependentes até 58 Anos:\r\nFilhos: Certidão de Nascimento ou RG. \r\nEnteados: Doc. que Comprove a Filiação com o Cônjuge do Titular. \r\nPais: Doc. que Comprove Parentesco de Identificação do Beneficiário e do Titular, Comprove o Parentesco com o Titular. \r\nIrmãos: Certidão de Nascimento ou Doc. que Comprove Parentesco. \r\nNeto(a): Certidão de nascimento do beneficiário, nome do titular como avô(ó), Identidade dos pais do beneficiário. \r\nBisneto: Doc. dos Pais, Avós, e Bisavós que Comprovem Parentesco. \r\nAvós: Doc. de Identificação do Beneficiário, a Certidão de Nascimento do Titular Onde Conste o Nome dos Avós. \r\nTios: Doc. De Identificação do Beneficiário com o Nome dos Seus Pais, Doc. dos Pais do Titular. \r\nSobrinhos, Sogro(a), Genro e Nora, Padrasto e Madrasta, Cunhado(a) e Concunhado(a):  Verificar Documentação na Tabela Original.','AMIL – PLANO PME 02 À 29 VIDAS',2),
	(3,'Cônjuge até 68 Anos: Documento de Identificação do Beneficiário, Junto com a Declaração de União Estável, Documento de Identificação de Filhos em Comum ou Certidão de Casamento.\r\n\r\nOs Demais Dependentes até 58 Anos:\r\nFilhos: Certidão de Nascimento ou Documento de Identificação que Comprove o Nome do Titular como Pai/Mãe.\r\nEnteados: Documento de Identificação que Comprove a Filiação com o Cônjuge do Titular, Junto com a Doc. que Comprova o Parentesco do Próprio Cônjuge com o Titular. \r\nPais: Documento de Identificação do Beneficiário e do Titular, que Comprove o Parentesco com o Titular. \r\nIrmãos: Certidão de Nascimento ou Documento de Identificação que Comprove os Pais em Comum com o Titular. \r\nNeto(a): Certidão de Nascimento do Beneficiário com o Nome do Titular como Avô(ó) ou Doc. de Identificação que Comprove a Filiação do Beneficiário, Junto com o Doc. de Identidade dos Pais do Beneficiário, Comprovando que o Beneficiário é Filho do Filho do Titular.\r\nBisneto: Certidão de Nascimento do Beneficiário com o Nome do Filho do Titular como Avô(ó), Junto com o Doc. do Filho do Titular ou Doc. de Identificação que Comprove a Filiação do Beneficiário, Junto com o Doc. De Identidade dos Pais e dos Avós do Beneficiário, Comprovando que o Beneficiário é Neto do Filho do Titular. \r\nAvós: Documento de Identificação do Beneficiário, Junto com a Certidão de Nascimento do Titular Onde Conste o Nome dos Avós ou Doc. de Identificação dos Pais do Titular para Comprovação do Parentesco. \r\nTios: Doc. De Identificação do Beneficiário com o Nome dos Seus Pais e Doc. de Identificação dos Pais do Titular, Comprovando que o Tio Possui a Mesma Filiação dos Pais Titular.\r\nSobrinhos, Sogro(a), Genro e Nora, Padrasto e Madrasta, Cunhado(a) e Concunhado(a): Verificar Documentação na Tabela Original.','AMIL – PLANO PME 30 À 99 VIDAS',2),
	(4,'Serão aceitos os filhos até 29 anos completos, independente de ser ou não universitário.\r\nOs filhos inválidos com qualquer idade, desde que dependentes para efeito de IR.\r\nÚltimo Contrato Social da Empresa, com as devidas alterações, ou Estatutos e Atas no caso de grupos específicos','BRADESCO HOSPITALAR NACIONAL 03 à 29 VIDAS e  04 à 29 VIDAS',1),
	(5,'Serão aceitos os filhos até 29 anos completos, independente de ser ou não universitário.\r\nOs filhos inválidos com qualquer idade, desde que dependentes para efeito de IR.\r\nÚltimo Contrato Social da Empresa, com as devidas alterações, ou Estatutos e Atas no caso de grupos específicos.\r\n','BRADESCO',1),
	(6,'MENOR: Certidão de Nascimento ou RG.\r\nMAIOR: RG e CPF.\r\nCOMPANHEIRO(A): Certidão de Casamento ou Declaração de União Estável registrada em cartório.\r\n\r\n','CAIXA',5),
	(7,'CÔNJUGE OU COMPANHEIRO: Declaração de união estável/Certidão de casamento.\r\nFILHOS(AS)  SOLTEIROS ATÉ 24 ANOS: Comprovante de matrícula da faculdade. \r\nFILHOS (AS) INVÁLIDOS (AS): Certidão de invalidez emitido pelo INSS / MENOR SOB GUARDA: Tutela/termo de guarda.\r\nENTEADOS SOLTEIROS  ATÉ 24 ANOS: Comprovante de matrícula da faculdade.\r\nCARTÃO DO SUS – Todos os beneficiários – link: https://portaldocidadao.saude.gov.br/portalcidadao/areacadastro.htm\r\n','CASSEB',6),
	(8,'Cópia do RG ou Certidão de Nascimento.\r\n','HAPVIDA',10),
	(9,'Cópia da Certidão de Nascimento ou cópia do RG (menores de 18 anos).\r\nCópia do RG ou CNH dentro do prazo de validade (maiores de 18 anos).\r\nCópia do CPF (maiores de 18 anos).\r\nCertidão de Casamento.\r\nDeclaração de convivência com firma reconhecida pelos declarantes.\r\nEntrevista Qualificada (crianças de 0 a 2 anos, mulheres a partir dos 40 anos e homens a partir dos 50 anos).\r\nCartão do SUS.\r\n\r\nACIMA DE 59 ANOS: VENDA ADMINISTRATIVA\r\nO corretor não está autorizado a receber qualquer importância para contratos que necessitem de entrevista médica qualificada, de acordo com os critérios da AMI.\r\n\r\nDependentes: São considerados dependente cônjuge, companheiro (a), companheiro (a) homoafetivo, filhos ou enteados até 21 anos ou 24 anos, 11 meses e 29 dias, se estiverem cursando faculdade.','AMI',26),
	(10,'Cópia do RG, CPF, Cartão do SUS, Contra-cheque e contrato de trabalho.\r\nCertidão de Nascimento (Crianças).\r\n','UNIÃO MÉDICA ',25),
	(11,'Cônjuge: Cópias - RG + CPF + Certidão de Casamento.\r\nCompanheiro(a): Declaração de União Estável de próprio punho, contendo o n° do RG e do CPF do(a) companheiro(a), endereço, tempo de convívio, n° do RG e assinatura de duas testemunhas, firma reconhecida do titular e do(a) companheiro(a). Cópia - RD do companheiro(a).\r\nFilho(a) Solteiro(a): Menores de 30 anos - Cópias RG + CPF.\r\nFilho(a) Inválido(a) de Qualquer Idade: Cópias - RG + CPF + Certidão de Invalidez emitida pelo INSS.\r\nEnteado(a) Solteiro(a): Menores de 30 anos - Cópias - RG + CPF do(a) Enteado(a) + Certidão de Óbito, de um dos genitores ou declaração judicial de ausência certidão de casamento ou declaração de união estável.\r\nMenor Sob Guarda ou Tutela do Segurado Titular: Cópias - RG + CPF do(a) tutelado(a), tutela ou do \"termo de guarda\" expedido por órgão oficial.\r\n\r\n\r\n\r\n','UNIMED CNU',17),
	(12,'Recém Nascidos: Certidão de Nascimento.\r\nCônjuges: Certidão de Casamento.\r\nCompanheiros: Declaração de União Estável.\r\nFilhos Inválidos de qualquer idade: Certidão de Invalidez emitida pelo INSS.\r\nMenor sob guarda ou tutela do segurado titular, solteiro e menor de 24 anos: Tutela ou \"Termo de Guarda\".','UNIMED FEIRA - 02 à 29 VIDAS',19),
	(13,'Empresa: Cópia do Contrato Social e alterações, cópia do cartão CNPJ atualizado, cópia do RG e CPF dos sócios, Inscrição Estadual (quando houver), comprovante de endereço da empresa, telefone e e-mail.\r\nTitular: Comprovação de vínculo trabalhista (cópia da cateira de trabalho ou da folha de registro da empresa), Cópia do RG, CPF e comprovante de endereço atualizado.\r\n','UNIMED FEIRA - 03 à 29 VIDAS',19),
	(14,'Cônjuge.\r\nFilhos até 29 anos.','VITALLIS SAÚDE ',23),
	(15,'Cônjuge: Certidão de Casamento ou União Estável\r\nDependente Menor: Certidão de Nascimento ou RG não Sendo Necessário CPF para Menor de 18 Anos.\r\n','UNIMED SEGUROS',18),
	(16,'Titular Sócio: Cópia do Rg e Cpf ou Cnh, Cópia do comprovante de residência para envio de boleto.\r\nCônjuge: Cópia de Certidão de casamento e Rg e Cpf.\r\nCompanheiro: Declaração união estável companheiro e titular. \r\nFilho(a) até 21 anos ou 24 anos se universitário: Cópia de Certidão de nascimento, Rg e Cpf.\r\nFilho(a) Inválido(a): Cópia do Rg e Cpf (Acima de 14 anos), Cópia da Guarda Judicial.\r\nMenor sob Tutela/Guarda: Cópia autenticada da Tutela Judicial e Guarda Judicial.\r\n','HAPVIDA AFIX',27),
	(17,'Cônjuge e Companheiro: Cópia do RG, Declaração de União Estável. Certidão de Casamento, Certidão de Nascimento de Filho em Comum.\r\nFilhos Solteiros: Menores de 18 Anos e de 18 à 24 Anos se Solteiros e Cursando Faculdade, Cópia Certidão (menores), ou RG (maior).\r\nFilhos Inválidos: Certidão de Invalidez Emitido do INSS.\r\nEnteados Solteiros: Até 24 anos* Certidão de Casamento.\r\nMenor S/Guarda: Cópia da Tutela / Termo de Guarda.\r\nPais: Cópia do RG e CPF.\r\nIrmãos: Cópia do RG e CPF.\r\nAvós: Cópia do RG e CPF.\r\nSogros: Cópia do RG e CPF.\r\n\r\n','BOA SAÚDE',4),
	(18,'Cônjuge até 68 Anos: RG, Declaração de União Estável, Documento de Identificação de Filhos ou Certidão de Casamento.\r\n\r\nOs Demais Dependentes até 58 Anos		\r\nFilhos: Certidão de Nascimento ou RG. \r\nEnteados: Doc. que Comprove a Filiação com o Cônjuge do Ttular.\r\nPais: Doc. que Comprove Parentesco de Identificação do Benefiário e do Titular, Comprove o Parentesco com o Titular. \r\nIrmãos: Certidão de Nascimento ou Doc. que Comprove Parentesco. \r\nNeto(a): Certidão de Nascimento do Beneficiário, Nome do Titular como Avô(ó) Identidade dos Pais do Beneficiário.\r\nBisneto: Doc dos Pais, Avós, e Bisavós que Comprovem Parentesco. \r\nAvós: Doc. de Identificação do Beneficiária, a Certidão de Nascimento do Titular Onde Conste o Nome dos Avós. \r\nTios: Doc. De Identificação do Beneficiário com o Nome dos Seus Pais, Doc dos Pais do Titular.\r\nSobrinhos, Sogro(a), Genro e Nora, Padrasto e Madrasta, Cunhado(a) e Concunhado(a):	Verificar Documentação na Tabela Original.\r\n','ONE HEALTH (GRUPO AMIL)',13);

/*!40000 ALTER TABLE `opcionais` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operadoras`;

CREATE TABLE `operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `imagem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `imagem_id` (`imagem_id`),
  CONSTRAINT `operadoras_ibfk_1` FOREIGN KEY (`imagem_id`) REFERENCES `imagens` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `operadoras` WRITE;
/*!40000 ALTER TABLE `operadoras` DISABLE KEYS */;

INSERT INTO `operadoras` (`id`, `nome`, `imagem_id`)
VALUES
	(1,'BRADESCO',2),
	(2,'AMIL',3),
	(4,'BOA SAÚDE',9),
	(5,'CAIXA SAÚDE',6),
	(6,'CASSEB',5),
	(10,'HAPVIDA',10),
	(13,'ONE HEALTH (GRUPO AMIL)',11),
	(16,'SULAMERICA',12),
	(17,'UNIMED - CENTRAL NACIONAL UNIMED',4),
	(18,'UNIMED - SEGUROS UNIMED',15),
	(19,'UNIMED - UNIMED FEIRA DE SANTANA',8),
	(23,'VITALLIS',17),
	(25,'UNIÃO MÉDICA',14),
	(26,'SAÚDE A.M.I.',7),
	(27,'HAPVIDA AFIX',NULL);

/*!40000 ALTER TABLE `operadoras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produtos`;

CREATE TABLE `produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `produtos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;

INSERT INTO `produtos` (`id`, `nome`, `descricao`, `operadora_id`)
VALUES
	(84,'400 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','400 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO\r\n',2),
	(85,'500 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','500 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO\r\n',2),
	(86,'700 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','700 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO',2),
	(87,'400 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','400 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',2),
	(88,'500 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','500 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',2),
	(89,'700 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','700 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',2),
	(90,'400 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','400 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',2),
	(91,'500 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','500 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',2),
	(92,'700 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','700 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',2),
	(93,'400 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','400 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',2),
	(94,'500 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','500 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',2),
	(95,'700 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','700 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO\r\n',2),
	(96,'HOSPITALAR - 03 à 29 VIDAS - OPCIONAL','HOSPITALAR - 03 à 29 VIDAS - OPCIONAL\r\n',1),
	(98,'HOSPITALAR - 03 à 29 VIDAS - COMPULSÓRIA','HOSPITALAR - 03 à 29 VIDAS - COMPULSÓRIA\r\n',1),
	(99,'HOSPITALAR - 04 à 29 VIDAS - OPCIONAL','HOSPITALAR - 04 à 29 VIDAS - OPCIONAL\r\n',1),
	(100,'HOSPITALAR - 04 à 29 VIDAS - COMPULSÓRIA','HOSPITALAR - 04 à 29 VIDAS - COMPULSÓRIA\r\n',1),
	(101,'PERFIL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - SEM COPARTICIPAÇÃO','PERFIL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - SEM COPARTICIPAÇÃO\r\n',1),
	(102,'PERFIL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - SEM COPARTICIPAÇÃO','PERFIL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - SEM COPARTICIPAÇÃO\r\n',1),
	(103,'PERFIL - 04 à 29 VIDAS - OPCIONAL - SEM COPARTICIPAÇÃO','PERFIL - 04 à 29 VIDAS - OPCIONAL - SEM COPARTICIPAÇÃO\r\n',1),
	(104,'PERFIL - 04 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 10%','PERFIL - 04 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 10%\r\n',1),
	(105,'PERFIL - 04 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 20%','PERFIL - 04 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 20%\r\n',1),
	(106,'PERFIL - 04 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 30%','PERFIL - 04 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 30%\r\n',1),
	(107,'PERFIL - 04 à 29 VIDAS - COMPULSÓRIA - SEM COPARTICIPAÇÃO ','PERFIL - 04 à 29 VIDAS - COMPULSÓRIA - SEM COPARTICIPAÇÃO \r\n',1),
	(108,'PERFIL - 04 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 10%','PERFIL - 04 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 10%\r\n',1),
	(109,'PERFIL - 04 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 20%','PERFIL - 04 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 20%\r\n',1),
	(110,'PERFIL - 04 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 30%','PERFIL - 04 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 30%\r\n',1),
	(111,'PERFIL - 30 à 99 VIDAS - OPCIONAL - SEM COPARTICIPAÇÃO','PERFIL - 30 à 99 VIDAS - OPCIONAL - SEM COPARTICIPAÇÃO\r\n',1),
	(112,'PERFIL - 30 à 99 VIDAS - COMPULSÓRIA - SEM COPARTICIPAÇÃO ','PERFIL - 30 à 99 VIDAS - COMPULSÓRIA - SEM COPARTICIPAÇÃO \r\n',1),
	(113,'FLEX NACIONAL - 03 à 29 VIDAS - OPCIONAL - SEM COPARTICIPAÇÃO','FLEX NACIONAL - 03 à 29 VIDAS - OPCIONAL - SEM COPARTICIPAÇÃO\r\n',1),
	(114,'FLEX NACIONAL - 03 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 10%','FLEX NACIONAL - 03 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 10%\r\n',1),
	(115,'FLEX NACIONAL - 03 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 20%','FLEX NACIONAL - 03 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 20%\r\n',1),
	(116,'FLEX NACIONAL - 03 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 30%','FLEX NACIONAL - 03 à 29 VIDAS - OPCIONAL - COPARTICIPAÇÃO 30%\r\n',1),
	(117,'FLEX NACIONAL - 03 à 29 VIDAS - COMPULSÓRIA - SEM COPARTICIPAÇÃO','FLEX NACIONAL - 03 à 29 VIDAS - COMPULSÓRIA - SEM COPARTICIPAÇÃO\r\n',1),
	(118,'FLEX NACIONAL - 03 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 10%','FLEX NACIONAL - 03 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 10%\r\n',1),
	(119,'FLEX NACIONAL - 03 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 30%','FLEX NACIONAL - 03 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 30%\r\n',1),
	(120,'FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - OPCIONAL - SEM COPARTICIPAÇÃO','FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - OPCIONAL - SEM COPARTICIPAÇÃO\r\n',1),
	(121,'FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - OPCIONAL - COPARTICIPAÇÃO 10%','FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - OPCIONAL - COPARTICIPAÇÃO 10%\r\n',1),
	(122,'FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - OPCIONAL - COPARTICIPAÇÃO 20%','FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - OPCIONAL - COPARTICIPAÇÃO 20%\r\n',1),
	(123,'FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - OPCIONAL - COPARTICIPAÇÃO 30%','FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - OPCIONAL - COPARTICIPAÇÃO 30%\r\n',1),
	(124,'FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA - SEM COPARTICIPAÇÃO','FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA - SEM COPARTICIPAÇÃO\r\n',1),
	(125,'FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 10%','FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 10%\r\n',1),
	(126,'FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 20%','FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 20%\r\n',1),
	(127,'FLEX NACIONAL - 04 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 30%','FLEX NACIONAL  - 04 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 30%\r\n',1),
	(128,'FLEX NACIONAL - 30 à 99 VIDAS - 2 TITULARES - OPCIONAL - SEM COPARTICIPAÇÃO','FLEX NACIONAL - 30 à 99 VIDAS - 2 TITULARES - OPCIONAL - SEM COPARTICIPAÇÃO\r\n',1),
	(129,'FLEX NACIONAL - 30 à 99 VIDAS - 2 TITULARES - COMPULSÓRIA - SEM COPARTICIPAÇÃO','FLEX NACIONAL - 30 à 99 VIDAS - 2 TITULARES - COMPULSÓRIA - SEM COPARTICIPAÇÃO',1),
	(130,'TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - SEM COPARTICIPAÇÃO','TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - SEM COPARTICIPAÇÃO\r\n',1),
	(131,'TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - COPARTICIPAÇÃO 10%','TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - COPARTICIPAÇÃO 10%\r\n',1),
	(132,'TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - COPARTICIPAÇÃO 20%','TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - COPARTICIPAÇÃO 20%\r\n',1),
	(133,'TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - COPARTICIPAÇÃO 30%','TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - OPCIONAL - COPARTICIPAÇÃO 30%\r\n',1),
	(134,'TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - SEM COPARTICIPAÇÃO','TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - SEM COPARTICIPAÇÃO\r\n',1),
	(135,'TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - COPARTICIPAÇÃO 10%','TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - COPARTICIPAÇÃO 10%\r\n',1),
	(136,'TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - COPARTICIPAÇÃO 20%','TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - COPARTICIPAÇÃO 20%\r\n',1),
	(137,'TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - COPARTICIPAÇÃO 30%','TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - COPARTICIPAÇÃO 30%\r\n',1),
	(138,'TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - OPCIONAL - SEM COPARTICIPAÇÃO','TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - OPCIONAL - SEM COPARTICIPAÇÃO\r\n',1),
	(139,'TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - OPCIONAL - COPARTICIPAÇÃO 10%','TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - OPCIONAL - COPARTICIPAÇÃO 10%',1),
	(140,'TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - OPCIONAL- COPARTICIPAÇÃO 20%','TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - OPCIONAL- COPARTICIPAÇÃO 20%',1),
	(141,'TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - OPCIONAL- COPARTICIPAÇÃO 30%','TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - OPCIONAL- COPARTICIPAÇÃO 30%',1),
	(142,'TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - COMPULSÓRIA - SEM COPARTICIPAÇÃO','TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - COMPULSÓRIA - SEM COPARTICIPAÇÃO',1),
	(143,'TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 10%','TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 10%\r\n',1),
	(144,'TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 20%','TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 20%',1),
	(145,'TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 30%','TOP NACIONAL - 04 à 29 VIDAS - A PARTIR DE 2 TITULARES - COMPULSÓRIA - COPARTICIPAÇÃO 30%',1),
	(146,'TOP NACIONAL - 30 a 99 VIDAS - OPCIONAL - SEM COPARTICIPAÇÃO','TOP NACIONAL - 30 a 99 VIDAS - OPCIONAL - SEM COPARTICIPAÇÃO',1),
	(147,'TOP NACIONAL - 30 a 99 VIDAS - COMPULSÓRIA - SEM COPARTICIPAÇÃO','TOP NACIONAL - 30 a 99 VIDAS - COMPULSÓRIA - SEM COPARTICIPAÇÃO\r\n',1),
	(235,'INTEGRAL V - 02 à 29 VIDAS - 2 TITULARES - COM COPARTICIPAÇÃO','INTEGRAL V - 02 à 29 VIDAS - 2 TITULARES - COM COPARTICIPAÇÃO',23),
	(236,'FLEX NACIONAL - 03 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 20%','FLEX NACIONAL - 03 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO  20%',1),
	(237,'EXCLUIR','EXCLUIR',1),
	(238,'FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO',5),
	(239,'FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO  ',5),
	(240,'VITAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','VITAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO',5),
	(241,'VITAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','VITAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO',5),
	(242,'PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(243,'COMPLETO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO',5),
	(245,'FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(246,'FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO  ',5),
	(247,'VITAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','VITAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO ',5),
	(248,'VITAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','VITAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO',5),
	(249,'PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO ',5),
	(250,'COMPLETO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','COMPLETO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO ',5),
	(251,'COMPLETO + P121 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','COMPLETO + P121 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO ',5),
	(252,'COMPLETO + P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO + P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(253,'FUNDAMENTAL P211 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P211 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(254,'FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%',5),
	(255,'VITAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','VITAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(256,'VITAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','VITAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%',5),
	(257,'PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(258,'COMPLETO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(259,'COMPLETO + P221 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO + P221 ‐ 03 à 29 VIDAS - 1 TITULAR  - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(260,'FUNDAMENTAL P211 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P211 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%  ',5),
	(261,'FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%',5),
	(262,'VITAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%','VITAL P111 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%  ',5),
	(263,'VITAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%','VITAL P121 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%',5),
	(264,'PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%','PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20% ',5),
	(265,'COMPLETO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%','COMPLETO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20% ',5),
	(266,'COMPLETO + P221 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20%','COMPLETO + P221 ‐ 03 à 29 VIDAS - 1 TITULAR  - VOLUNTÁRIA  ‐ COPARTICIPAÇÃO 20% ',5),
	(267,'FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(268,'FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO',5),
	(269,'VITAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','VITAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO',5),
	(270,'VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO',5),
	(271,'PRONTO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','PRONTO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(272,'COMPLETO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(273,'COMPLETO + P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO + P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(274,'FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO',5),
	(275,'FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(276,'VITAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','VITAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(277,'VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(278,'PRONTO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','PRONTO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(279,'COMPLETO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO',5),
	(280,'COMPLETO + P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO + P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(281,'FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%',5),
	(282,'FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(283,'VITAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','VITAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(284,'VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%',5),
	(285,'PRONTO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','PRONTO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(286,'COMPLETO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(287,'COMPLETO + P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO + P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(288,'FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(289,'FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(290,'VITAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','VITAL P111 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(291,'VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(292,'PRONTO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','PRONTO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(293,'COMPLETO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(294,'COMPLETO + P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO + P121 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(295,'COLETIVO EMPRESARIAL - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','COLETIVO EMPRESARIAL - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO ',6),
	(296,'COLETIVO EMPRESARIAL - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','COLETIVO EMPRESARIAL - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',6),
	(312,'FLEX I - 02 à 99 VIDAS - COM COPARTICIPAÇÃO','FLEX I - 02 à 99 VIDAS - COM COPARTICIPAÇÃO',26),
	(313,'FLEX II - 02 à 99 VIDAS - COM COPARTICIPAÇÃO','FLEX II - 02 à 99 VIDAS - COM COPARTICIPAÇÃO',26),
	(314,'OURO I - 02 à 99 VIDAS - SEM COPARTICIPAÇÃO','OURO I - 02 à 99 VIDAS - SEM COPARTICIPAÇÃO',26),
	(315,'OUTRO II - 02 à 99 VIDAS - SEM COPARTICIPAÇÃO','OUTRO II - 02 à 99 VIDAS - SEM COPARTICIPAÇÃO',26),
	(316,'UNIÃO MÉDICA - 02 à 29 VIDAS - COPARTICIPAÇÃO EM CONSULTAS E EXAMES','UNIÃO MÉDICA - 02 à 29 VIDAS - COPARTICIPAÇÃO EM CONSULTAS E EXAMES',25),
	(317,'UNIÃO MÉDICA - 02 à 29 VIDAS - COPARTICIPAÇÃO EM CONSULTAS','UNIÃO MÉDICA - 02 à 29 VIDAS - COPARTICIPAÇÃO EM CONSULTAS',25),
	(318,'UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - BÁSICO - COM COPARTICIPAÇÃO','UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - BÁSICO - COM COPARTICIPAÇÃO  ',17),
	(319,'UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - BÁSICO - SEM COPARTICIPAÇÃO','UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - BÁSICO - SEM COPARTICIPAÇÃO  ',17),
	(320,'UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - ESPECIAL - COM COPARTICIPAÇÃO','UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - ESPECIAL - COM COPARTICIPAÇÃO  ',17),
	(321,'UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - ESPECIAL - SEM COPARTICIPAÇÃO','UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - ESPECIAL - SEM COPARTICIPAÇÃO  ',17),
	(322,'UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - MASTER - SEM COPARTICIPAÇÃO','UNIMED - CENTRAL NACIONAL UNIMED - 03 à 99 VIDAS - TABELA PROMOCIONAL - MASTER - SEM COPARTICIPAÇÃO  ',17),
	(323,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 02 à 08 VIDAS - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 02 à 08 VIDAS - COM COPARTICIPAÇÃO',19),
	(324,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 09 à 15 VIDAS - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 09 à 15 VIDAS - COM COPARTICIPAÇÃO  ',19),
	(325,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 16 à 22 VIDAS - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 16 à 22 VIDAS - COM COPARTICIPAÇÃO  ',19),
	(326,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 23 à 29 VIDAS - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 23 à 29 VIDAS - COM COPARTICIPAÇÃO',19),
	(327,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 03 à 06 VIDAS - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 03 à 06 VIDAS - COM COPARTICIPAÇÃO',19),
	(328,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 07 à 10 VIDAS - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 07 à 10 VIDAS - COM COPARTICIPAÇÃO ',19),
	(329,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 11 à 19 VIDAS - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 11 à 19 VIDAS - COM COPARTICIPAÇÃO',19),
	(330,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 20 à 29 VIDAS - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 20 à 29 VIDAS - COM COPARTICIPAÇÃO',19),
	(331,'PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','PRONTO P121 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%',5),
	(332,'PRONTO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','PRONTO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(333,'PRONTO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','PRONTO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO',5),
	(334,'NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL - SEM COPARTICIPAÇÃO','NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL - SEM COPARTICIPAÇÃO',10),
	(335,'NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO',10),
	(336,'NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL - COM COPARTICIPAÇÃO','NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL - COM COPARTICIPAÇÃO',10),
	(337,'NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO',10),
	(338,'MIX - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','MIX - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO',10),
	(340,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 04 à 09 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 04 à 09 VIDAS - COM COPARTICIPAÇÃO',18),
	(341,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 04 à 09 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 04 à 09 VIDAS - COM COPARTICIPAÇÃO',18),
	(342,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 04 à 09 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 04 à 09 VIDAS - COM COPARTICIPAÇÃO',18),
	(343,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 04 à 09 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 04 à 09 VIDAS - COM COPARTICIPAÇÃO',18),
	(344,'UNISEG ESSENCIAL - 04 à 09 VIDAS - COM COPARTICIPAÇÃO','UNISEG ESSENCIAL - 04 à 09 VIDAS - COM COPARTICIPAÇÃO',18),
	(345,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO',18),
	(346,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO',18),
	(347,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO',18),
	(348,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO',18),
	(349,'UNISEG ESSENCIAL - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','UNISEG ESSENCIAL - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO',18),
	(350,'PRONTO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','PRONTO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%',5),
	(351,'PRONTO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','PRONTO P221 ‐ 03 à 29 VIDAS - 1 TITULAR - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ',5),
	(352,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 10 à 29 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 10 à 29 VIDAS - COM COPARTICIPAÇÃO',18),
	(353,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 10 à 29 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 10 à 29 VIDAS - COM COPARTICIPAÇÃO',18),
	(354,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 10 à 29 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 10 à 29 VIDAS - COM COPARTICIPAÇÃO',18),
	(355,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 10 à 29 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 10 à 29 VIDAS - COM COPARTICIPAÇÃO',18),
	(356,'UNISEG ESSENCIAL - 10 à 29 VIDAS - COM COPARTICIPAÇÃO','UNISEG ESSENCIAL - 10 à 29 VIDAS - COM COPARTICIPAÇÃO',18),
	(357,'PEPEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO','PEPEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO',18),
	(358,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO',18),
	(359,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO',18),
	(360,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO',18),
	(361,'UNISEG ESSENCIAL - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO','UNISEG ESSENCIAL - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO',18),
	(362,'UNISEG ESSENCIAL - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','UNISEG ESSENCIAL - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',18),
	(363,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',18),
	(364,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',18),
	(365,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',18),
	(366,'PEPEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','PEPEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO\r\n',18),
	(367,'UNISEG ESSENCIAL - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','UNISEG ESSENCIAL - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',18),
	(368,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',18),
	(369,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',18),
	(370,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',18),
	(371,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',18),
	(372,'FUNDAMENTAL P211 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P211 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(373,'FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(374,'FUNDAMENTAL P211 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P211 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(375,'FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(376,'PRONTO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','PRONTO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(377,'COMPLETO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(378,'COMPLETO + P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO + P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%',5),
	(379,'PRONTO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','PRONTO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%',5),
	(380,'COMPLETO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(381,'COMPLETO + P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO + P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ',5),
	(382,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO',18),
	(383,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO – 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO – 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',18),
	(384,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO',18),
	(385,'MIX - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','MIX - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO',10),
	(386,'NOSSO PLANO - 01 à 99 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA','NOSSO PLANO - 01 à 99 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA',27),
	(387,'MIX - 01 à 99 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA','MIX - 01 à 99 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA ',27),
	(388,'AMBULATORIAL EMPRESARIAL - 03 à 29 VIDAS - SEM COPARTICIPAÇÃO','AMBULATORIAL EMPRESARIAL - 03 à 29 VIDAS - SEM COPARTICIPAÇÃO',4),
	(389,'AMBULATORIAL EMPRESARIAL - 03 à 29 VIDAS - COM COPARTICIPAÇÃO','AMBULATORIAL EMPRESARIAL - 03 à 29 VIDAS - COM COPARTICIPAÇÃO',4),
	(390,'AMBULATORIAL EMPRESARIAL - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','AMBULATORIAL EMPRESARIAL - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',4),
	(393,'LINCX LT3 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','LINCX LT3 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO',13),
	(394,'LINCX LT4 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','LINCX LT4 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO',13),
	(395,'BLACK T2 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','BLACK T2 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO',13),
	(396,'BLACK T3 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','BLACK T3 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO',13),
	(397,'BLACK T4 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','BLACK T4 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO',13),
	(398,'BLACK T5 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','BLACK T5 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO',13),
	(399,'LINCX LT3 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','LINCX LT3 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',13),
	(400,'LINCX LT4 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','LINCX LT4 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',13),
	(401,'BLACK T2 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','BLACK T2 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',13),
	(402,'BLACK T3 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','BLACK T3 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',13),
	(403,'BLACK T5 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','BLACK T5 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',13),
	(404,'BLACK T4 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','BLACK T4 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO',13),
	(405,'LINCX LT3 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','LINCX LT3 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',13),
	(406,'LINCX LT4 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','LINCX LT4 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',13),
	(407,'BLACK T2 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','BLACK T2 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',13),
	(408,'BLACK T3 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','BLACK T3 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',13),
	(409,'BLACK T4 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','BLACK T4 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',13),
	(410,'BLACK T5 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','BLACK T5 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO',13),
	(411,'LINCX LT3 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','LINCX LT3 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',13),
	(412,'LINCX LT4 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','LINCX LT4 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',13),
	(413,'BLACK T2 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','BLACK T2 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',13),
	(414,'BLACK T3 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','BLACK T3 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',13),
	(415,'BLACK T4 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','BLACK T4 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',13),
	(416,'BLACK T5 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','BLACK T5 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO',13);

/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela ramos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ramos`;

CREATE TABLE `ramos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ramos` WRITE;
/*!40000 ALTER TABLE `ramos` DISABLE KEYS */;

INSERT INTO `ramos` (`id`, `descricao`, `nome`)
VALUES
	(1,'SAÚDE','SAÚDE'),
	(2,'ODONTOLÓGICO','ODONTOLÓGICO');

/*!40000 ALTER TABLE `ramos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela redes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redes`;

CREATE TABLE `redes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `redes_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `redes` WRITE;
/*!40000 ALTER TABLE `redes` DISABLE KEYS */;

INSERT INTO `redes` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'PLANO TOP - SALVADOR: Ceparh, Clínica São Bernardo, Clisur, COT Canela, CTO Médico Agenor Paiva, Day Hospital Promédica, Fund. Baiano de olhos.  Cardiologia, Hosp. Evangelico, Hosp Prof Jorge Valente, Hosp. Da Cidade, Hosp. Sagrada Familia, Hosp. De Cunha, Hosp. Jaar Andrade, Hosp. Portugues, Hosp. Santo Amaro, Hosp. São Rafael,  INSBOT, Sobaby, SOMED, Instituto Cardio Pulmonar, Insituto dos Olhos, Itaigara Memorial Hosp Dia, Life Center, Oftalmoclin, Orto Clinica, Probaby.\r\n','BRADESCO HOSPITALAR NACIONAL',NULL),
	(3,'400: Hosp. Sagrada Família, Hosp. Santo Amaro, Day Horc, Cato, Clisur, COF, COT, Espaço Bom Viver, HBA, Hosp. Aeroporto, Hosp. Agenor Paiva, Hosp. Evangélico da Bahia, Hosp. Jaar Andrade, Hosp. Salvador, Hosp. Santa Izabel, Insbot, Nupsi, Probaby, Sobaby, Ortra, Semeca, Ortra, Alclin Hosp. de Olhos Lavigne, Caparh, Clinnaza, Day Hospital Louis Pateur, Hosp. S&A, Hosp. Santa luzia, IBOPC.\r\n','400 - 02 À 29 VIDAS',2),
	(4,'500: Hosp. Sagrada Família, Hosp. Santo Amaro, Day Horc, Cato, Clisur, COF, COT, Espaço Bom Viver, HBA, Hosp. Aeroporto, Hosp. Agenor Paiva, Hosp. Evangélico da Bahia, Hosp. Jaar Andrade, Hosp. Salvador, Hosp. Santa Izabel, Insbot, Nupsi, Probaby, Sobaby, Ortra, Semeca, Ortra, Alclin Hosp. de Olhos Lavigne, Caparh, Clinnaza, Day Hospital Louis Pateur, Hosp. S&A, Hosp. Santa luzia, IBOPC, Hospital São Rafael, Instituto Cárdio Pulmonar, Instituto de Olhos Freitas, Clínica de Urologia Modesto Jacobino.\r\n','500 - 02 À 29 VIDAS',2),
	(5,'700: Hosp. Sagrada Família, Hosp. Santo Amaro, Day Horc, Cato, Clisur, COF, COT, Espaço Bom Viver, HBA, Hosp. Aeroporto, Hosp. Agenor Paiva, Hosp. Evangélico da Bahia, Hosp. Jaar Andrade, Hosp. Salvador, Hosp. Santa Izabel, Insbot, Nupsi, Probaby, Sobaby, Ortra, Semeca, Ortra, Alclin Hosp. de Olhos Lavigne, Caparh, Clinnaza, Day Hospital Louis Pateur, Hosp. S&A, Hosp. Santa luzia, IBOPC,  Hospital São Rafael, Instituto Cárdio Pulmonar, Instituto de Olhos Freitas, Clínica de Urologia Modesto Jacobino, Hospital Aliança.','700 - 02 à 29 VIDAS',2),
	(6,'400: Hosp. Sagrada Família, Hosp. Santo Amaro, Day Horc, Cato, Clisur, COF, COT, Espaço Bom Viver, HBA, Hosp. Aeroporto, Hosp. Agenor Paiva, Hosp. Evangélico da Bahia, Hosp. Jaar Andrade, Hosp. Salvador, Hosp. Santa Izabel, Insbot, Nupsi, Probaby, Sobaby, Ortra, Semeca, Ortra, Alclin Hosp. de Olhos Lavigne, Caparh, Clinnaza, Day Hospital Louis Pateur, Hosp. S&A, Hosp. Santa luzia, IBOPC.\r\n','400 - 30 à 99 Vidas',2),
	(7,'500: Hosp. Sagrada Família, Hosp. Santo Amaro, Day Horc, Cato, Clisur, COF, COT, Espaço Bom Viver, HBA, Hosp. Aeroporto, Hosp. Agenor Paiva, Hosp. Evangélico da Bahia, Hosp. Jaar Andrade, Hosp. Salvador, Hosp. Santa Izabel, Insbot, Nupsi, Probaby, Sobaby, Ortra, Semeca, Ortra, Alclin Hosp. de Olhos Lavigne, Caparh, Clinnaza, Day Hospital Louis Pateur, Hosp. S&A, Hosp. Santa luzia, IBOPC,  Hospital São Rafael, Instituto Cárdio Pulmonar, Instituto de Olhos Freitas, Clínica de Urologia Modesto Jacobino.\r\n','500 - 30 à 99 VIDAS',2),
	(8,'700: Hosp. Sagrada Família, Hosp. Santo Amaro, Day Horc, Cato, Clisur, COF, COT, Espaço Bom Viver, HBA, Hosp. Aeroporto, Hosp. Agenor Paiva, Hosp. Evangélico da Bahia, Hosp. Jaar Andrade, Hosp. Salvador, Hosp. Santa Izabel, Insbot, Nupsi, Probaby, Sobaby, Ortra, Semeca, Ortra, Alclin Hosp. de Olhos Lavigne, Caparh, Clinnaza, Day Hospital Louis Pateur, Hosp. S&A, Hosp. Santa luzia, IBOPC, Hospital São Rafael, Instituto Cárdio Pulmonar, Instituto de Olhos Freitas, Clínica de Urologia Modesto Jacobino, Hospital Aliança.','700 - 30 à 99 VIDAS',2),
	(9,'PLANO TOP - SALVADOR: \r\n- Ceparh, Clínica São Bernardo, Clisur, COT Canela, CTO Médico Agenor Paiva, Day Hospital Promédica, Fund. Baiano de olhos.\r\n- Cardiologia, Hosp. Evangelico, Hosp. Prof Jorge Valente, Hosp. Da Cidade, Hosp. Sagrada Familia, Hosp. De Cunha, Hosp. Jaar Andrade, Hosp. Portugues, Hosp. Santo Amaro, Hosp. São Rafael, INSBOT, Sobaby, SOMED, Instituto Cardio Pulmonar, Insituto dos Olhos, Itaigara Memorial, Hosp. Dia, Life Center, Oftalmoclin, Orto Clinica, Probaby.\r\n','BRADESCO ',1),
	(10,'FUNDAMENTAL: HOSP. DA BAHIA-HBA, HOSP. SALVADOR, INSBOT, SOMED SOCORROS MÉDICOS, HOSP. DAY HORC, DAYHORC-HOSP. DE OLHOS, CLIVALE-CALÇADA, C.M. HOSP. AGENOR PAIVA, HOSP. EVANGELICO DA BAHIA, HOSP. DA SAGRADA FAMILIA, INST. DE OLHOS FREITAS, PRO BABY, SAMES, FISIORT CLINICA MÉDICA.\r\nVITAL: Todos do fundamental + HOSP. DE OLHOS JOSÉ EUTROPIO, HOSP. SANTA LUZIA, POLICLINICA SANTA CLARA.\r\nPRONTO: Todos os anteriores + HOSPITAL SÃO RAFAEL, INSTITUTO CARDIO PULMONAR DA BAHIA, HOSPITAL ALIANÇA E HOSPITAL PORTUGUÊS.\r\nCOMPLETO: Todos os anteriores +  Sírio Libanês, Albert Einsten (São Paulo) -  Samaritano ( Rio de Janeiro).\r\n\r\n\r\n\r\n\r\n\r\n','CAIXA',5),
	(11,'Centro Médico Casseb, Hospital Santa Isabel,  Hospital Agenor Paiva, Hospital da Cidade, Hospital Salvador, Hospital Salvador, Hospital Sagrada Família, Hospital Sobaby, Sames, Somed, CDI, Clab, Sermeca, Clinica Ecco, Nephron, Cenob, Clinica Ultra Gin, Clinica Oftalmológica torres. \r\n','CASSEB',6),
	(12,'Hospital Tereza de Lisieux, Hapclínica Dendezeiros, Hapclínica Lauro de Freitas, Hapclínica, Cajazeiras, Hapclínica Cajazeiras, Hapclínica Camaçari, Hapclínica Ondina.\r\n','HAPVIDA - PME',10),
	(13,'Não Informado.','AMI',26),
	(15,'Feira de Santana: EMEC, MATER DEI, Hospital Ortopédico, HTO, GYNETOCO, Inst. De Cardiologia, SÓBABY, Otorrinos, IHEF - Laboratório, São Matheus, Santa Cecilia, Laboratório Análise, Bambino, Pró-Diagnóstico, Maternidade Santa Emilia','UNIÃO MÉDICA ',25),
	(16,'Hospitais:\r\nHospital Agenor Paiva, Hospital Agnus Dei, Hospital Cidade, Hospital Cot, Hospital das Clínicas Alagoinhas, Hospital Evangélico, Hospital Jorge Valente, Hospital Português, Hospital Prohope, Hospital Sagrada Família, Hospital Salvador, Hospital Santa Helena, Hospital Santa Izabel, Hospital Santo Amaro, Hospital São Rafael, Insbot, Ortoped, Probaby, Sobaby.\r\n\r\nClinicas: \r\nCardiokids, Cardioprev, Clínica Anna Paola Gatto, Clínica Biocheck Up, Itaigara Memorial, Clivale, Clínica de Brotas, Clínica CEHON, Clínica DERMAE, Clínica Guri, Clínica Leitão Guerra, Clínica Master Vida, Clínica Multiclin,  Clínica Vida, DAYHORC, Humana, IPSE, Otorrino Center, Proneuro, Sermec, Sermeca, Univisão.\r\n\r\nLaboratórios:\r\nLaboratório DNA, Laboratório LABCHECAP, Laboratório Leme, Laboratório LPC, Laboratório SABIN, Laboratório Spalazanni.\r\n\r\nCentros de Diagnose (Planos Exames de imagem):\r\nBahia Imagem, Diagnoson, Image Memorial, Multi Imagem, Master Vida, Delfin.\r\n','CNU - BÁSICO',17),
	(17,'Hospitais:\r\nHospital Agenor Paiva, Hospital Agnus Dei, Hospital Cidade, Hospital Cot, Hospital das Clínicas Alagoinhas, Hospital Evangélico, Hospital Jorge Valente, Hospital Português, Hospital Prohope, Hospital Sagrada Família, Hospital Salvador, Hospital Santa Helena, Hospital Santa Izabel, Hospital Santo Amaro, Hospital São Rafael, Insbot, Ortoped, Probaby, Sobaby, \r\n\r\nClinicas: \r\nCardiokids, Cardioprev, Clínica Anna Paola Gatto, Clínica Biocheck Up, Itaigara Memorial, Clivale, Clínica de Brotas, Clínica CEHON, Clínica DERMAE, Clínica Guri, Clínica Leitão Guerra, Clínica Master Vida, Clínica Multiclin,  Clínica Vida, DAYHORC, Humana, IPSE, Otorrino Center, Proneuro, Sermec, Sermeca, Univisão.\r\n\r\nLaboratórios:\r\nLaboratório DNA, Laboratório LABCHECAP, Laboratório Leme, Laboratório LPC, Laboratório SABIN, Laboratório Spalazanni.\r\n\r\nCentros de Diagnose (Planos Exames de imagem):\r\nBahia Imagem, Diagnoson, Image Memorial, Multi Imagem, Master Vida, Delfin.\r\n','CNU - ESPECIAL',17),
	(18,'Hospitais:\r\nHospital Agenor Paiva, Hospital Agnus Dei, Hospital Cidade, Hospital Cot, Hospital das Clínicas Alagoinhas, Hospital Evangélico, Hospital Jorge Valente, Hospital Português, Hospital Prohope, Hospital Sagrada Família, Hospital Salvador, Hospital Santa Helena, Hospital Santa Izabel, Hospital Santo Amaro, Hospital São Rafael, Insbot, Ortoped, Probaby, Sobaby, Hospital Aliança. \r\n\r\nClinicas: \r\nCardiokids, Cardioprev, Clínica Anna Paola Gatto, Clínica Biocheck Up, Itaigara Memorial, Clivale, Clínica de Brotas, Clínica CEHON, Clínica DERMAE, Clínica Guri, Clínica Leitão Guerra, Clínica Master Vida, Clínica Multiclin,  Clínica Vida, DAYHORC, Humana, IPSE, Otorrino Center, Proneuro, Sermec, Sermeca, Univisão.\r\n\r\nLaboratórios:\r\nLaboratório DNA, Laboratório LABCHECAP, Laboratório Leme, Laboratório LPC, Laboratório SABIN, Laboratório Spalazanni.\r\n\r\nCentros de Diagnose (Planos Exames de imagem):\r\nBahia Imagem, Diagnoson, Image Memorial, Multi Imagem, Master Vida, Delfin.\r\n\r\n','CNU - MASTER',17),
	(19,'Não Informado.','UNIMED FEIRA',19),
	(20,'Hospital Maternidade Luiz Argolo, Hospital da Cidade, Hospital Aeroporto, Hospital Agnus Dei, Clinica São Roque, Sobaby, Clisur, Hospital Prohope, Hospital Evangelico da Bahia, Hospital Santa Helena, Hospital SEMED, Hospital São Paulo, Hospital Agenor Paiva, Hospital Santa Izabel, Cot Canela, Hospital da Sagrada Familia, Clinica Bom Viver, Hospital e Maternidade de Santo Amaro, Clima, Oftamodiagnose Hospital dos Olhos, Climege, Hospital Jorge Valente, CEPARH, Hospital Santo Amaro, Probaby, Inooa, Otorrino Center.','UNIMED SEGUROS - COMPACTO - EFETIVO',18),
	(21,'Hospital Tereza de Lisieux, Hapclínica Dendezeiros, Hapclínica Lauro de Freitas, Hapclínica, Cajazeiras, Hapclínica Cajazeiras, Hapclínica Camaçari, Hapclínica Ondina.','HAPVIDA AFIX',27),
	(22,'Local: Imagem Memorial e CAM, Labchecap e Spalazanni, Clínica Ocular, Hospital Santa Clara, NOOBA, Alergoderma, Clínica Fé e APAE, Fund.de Neurologia, Holos, Rodolfo Dantas\r\nEcco, Sermeca e Sobaby.\r\n\r\n','BOA SAÚDE',4),
	(23,'Hospital Maternidade Luiz Argolo, Hospital da Cidade, Hospital Aeroporto, Hospital Agnus Dei, Clinica São Roque, Sobaby, Clisur, Hospital Prohope, Hospital Evangelico da Bahia, Hospital Santa Helena, Hospital SEMED, Hospital São Paulo, Hospital Agenor Paiva, Hospital Santa Izabel, Cot Canela, Hospital da Sagrada Familia, Clinica Bom Viver, Hospital e Maternidade de Santo Amaro, Clima, Oftamodiagnose Hospital dos Olhos, Climege, Hospital Jorge Valente, CEPARH, Hospital Santo Amaro, Probaby, Inooa, Otorrino Center, Otorrino Center, INCAR.','UNIMED SEGUROS - COMPLETO',18),
	(24,'Hospital Maternidade Luiz Argolo, Hospital da Cidade, Hospital Aeroporto, Hospital Agnus Dei, Clinica São Roque, Sobaby, Clisur, Hospital Prohope, Hospital Evangelico da Bahia, Hospital Santa Helena, Hospital SEMED, Hospital São Paulo, Hospital Agenor Paiva, Hospital Santa Izabel, Cot Canela, Hospital da Sagrada Familia, Clinica Bom Viver, Hospital e Maternidade de Santo Amaro, Clima, Oftamodiagnose Hospital dos Olhos, Climege, Hospital Jorge Valente, CEPARH, Hospital Santo Amaro, Probaby, Inooa, Otorrino Center, Otorrino Center, INCAR, Itaigara Memorial Hospital Dia, Day Horc, Hospital São Rafael, Oftalmoclin, Hospital Portugues, Fundação Baiana de Cardiologia, COF, Instituto de Olhos Freitas.','UNIMED SEGUROS  - SUPERIOR',18),
	(25,'Hospital Maternidade Luiz Argolo, Hospital da Cidade, Hospital Aeroporto, Hospital Agnus Dei, Clinica São Roque, Sobaby, Clisur, Hospital Prohope, Hospital Evangelico da Bahia, Hospital Santa Helena, Hospital SEMED, Hospital São Paulo, Hospital Agenor Paiva, Hospital Santa Izabel, Cot Canela, Hospital da Sagrada Familia, Clinica Bom Viver, Hospital e Maternidade de Santo Amaro, Clima, Oftamodiagnose Hospital dos Olhos, Climege, Hospital Jorge Valente, CEPARH, Hospital Santo Amaro, Probaby, Inooa, Otorrino Center, Otorrino Center, INCAR, Itaigara Memorial Hospital Dia, Day Horc, Hospital São Rafael, Oftalmoclin, Hospital Portugues, Fundação Baiana de Cardiologia, COF, Instituto de Olhos Freitas, Instituto Cardio Pulmonar da Bahia, Hospital Aliança.','UNIMED SEGUROS - SÊNIOR',18),
	(26,'Hospital Sagrada Família, Hospital Santo Amaro, Day horc, Cato, Clisur, COF, COT, Espaço Bom Viver, HBA, Hospital Aeroporto, Hosp. Agenor Paiva, Hosp. Evangélico da Bahia, Hosp. Jaar Andrade, Hosp. Salvador, Hosp. Santa Izabel, Insbot, Nupsi, Probaby, Sobaby, Ortra, Semeca, Ortra, Alclin Hosp. de Olhos lavigne, Caparh, Clinnaza, Day Hospital Louis Pateur, Hosp. S&A, Hosp. Santa luzia, IBOPC, Hospital São Rafael, Instituto Cárdio Pulmonar, Instituto de Olhos Freitas, Clínica de Urologia Modesto Jacobino, Hospital Aliança.\r\n','ONE HEALTH (GRUPO AMIL)',13);

/*!40000 ALTER TABLE `redes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela reembolsos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reembolsos`;

CREATE TABLE `reembolsos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `reembolsos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `reembolsos` WRITE;
/*!40000 ALTER TABLE `reembolsos` DISABLE KEYS */;

INSERT INTO `reembolsos` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'....','BRADESCO HOSPITALAR NACIONAL',NULL),
	(2,'EXCLUIR','EXCLUIR',5),
	(3,'Fundamental P211 - Enfermaria: R$  R$ 70,00 \r\nFundamental P221 - Apartamento: R$  R$ 70,00 \r\nVital P111 - Enfermaria: R$ 70,00 \r\nVital P121 - Apartamento: R$ 70,00\r\nPronto P221 - Apartamento: R$ 116,66 \r\nCompleto P221 - Apartamento: R$ 186,66    \r\nCompleto + P221 - Apartamento: R$ 404,43','Coparticipação 20%',5),
	(4,'Fundamental P111 - Enfermaria: R$ 70,00 \r\nFundamental P121 - Apartamento: R$ 70,00\r\nVital P111 - Enfermaria: R$ 70,00 \r\nVital P121 - Apartamento: R$ 70,00\r\nPronto P121 - Apartamento: R$ 116,66 \r\nCompleto P121 - Apartamento: R$ 186,66    \r\nCompleto + P121 - Apartamento: R$ 404,43\r\n','Sem coparticipação',5),
	(5,'Compacto e Efetivo: 1x\r\nCompleto: 1,5x\r\nSuperior: 3x\r\nSênior: 8x\r\n\r\nExemplos - Reembolso para Consultas:\r\nCompacto e Efetivo: R$ 75,00\r\nCompleto: 1,5x: R$ 112,50\r\nSuperior: 3x: R$ 225,00\r\nSênior: 8x: R$ 600,00','UNIMED SEGUROS ',18),
	(6,'Consulta: R$ 180,00\r\nSessão de Psicologia (até): R$99,54 \r\nSessão de Fonoaudiologia: R$99,54 \r\nHemograma Completo: R$50,40 \r\nMamografia: R$200,34\r\nEletrocardiograma: R$88,20 \r\nRessonância Magnética de Crânio: R$2.268,00\r\nEndoscopia Digestiva: R$340,20 \r\nTeste Ergométrico: R$226,80 \r\nTomografia de Crânio: R$938,70 \r\nUltrassom Obstétrico: R$176,40 \r\nParto: R$3.874,50 \r\nHisterectomia Total: R$3.780,00\r\nGastrectomia Total Via Abdominal: R$7.843,50 \r\nRevascularização Miocárdio s/ Extracorpórea: R$8.505,00 \r\nColecistectomia c/ Colangiografia: R$4.819,50 \r\nHemorroidectomia Aberta ou Fechada: R$2.173,50 \r\nApendicectomia: R$2.664,90 \r\nArtroscopia do Joelho para Cirurgia: R$2.683,80 ','LINCX LT3',13),
	(7,'Consulta: R$270,00 \r\nSessão de Psicologia (até): R$149,31  \r\nSessão de Fonoaudiologia: R$149,31  \r\nHemograma Completo: R$75,60  \r\nMamografia: R$300,51 \r\nEletrocardiograma: R$132,30 \r\nRessonância Magnética de Crânio: R$3.402,00\r\nEndoscopia Digestiva: R$510,30 \r\nTeste Ergométrico: R$340,20\r\nTomografia de Crânio: R$1.408,05\r\nUltrassom Obstétrico: R$264,60\r\nParto: R$5.166,00 \r\nHisterectomia Total: R$5.040,00\r\nGastrectomia Total Via Abdominal: R$10.458,00 \r\nRevascularização Miocárdio s/ Extracorpórea: R$11.340,00 \r\nColecistectomia c/ Colangiografia: R$6.426,00 \r\nHemorroidectomia Aberta ou Fechada: R$2.898,00\r\nApendicectomia: R$3.553,20\r\nArtroscopia do Joelho para Cirurgia: R$3.578,40','LINCX LT4',13),
	(8,'Consulta: R$360,00 \r\nSessão de Psicologia (até): R$149,31  \r\nSessão de Fonoaudiologia: R$149,31  \r\nHemograma Completo: R$75,60  \r\nMamografia: R$300,51 \r\nEletrocardiograma: R$132,30 \r\nRessonância Magnética de Crânio: R$3.402,00\r\nEndoscopia Digestiva: R$510,30 \r\nTeste Ergométrico: R$340,20\r\nTomografia de Crânio: R$1.408,05\r\nUltrassom Obstétrico: R$264,60\r\nParto: R$9.040,50\r\nHisterectomia Total: R$8.820,00\r\nGastrectomia Total Via Abdominal: R$18.301,50 \r\nRevascularização Miocárdio s/ Extracorpórea: R$19.845,00 \r\nColecistectomia c/ Colangiografia: R$11.245,50  \r\nHemorroidectomia Aberta ou Fechada: R$5.071,50 \r\nApendicectomia: R$6.218,10 \r\nArtroscopia do Joelho para Cirurgia: R$6.262,20 ','BLACK T2',13),
	(9,'Consulta: R$510,00 \r\nSessão de Psicologia (até): R$248,85  \r\nSessão de Fonoaudiologia: R$248,85  \r\nHemograma Completo: R$126,00  \r\nMamografia: R$500,85\r\nEletrocardiograma: R$220,50\r\nRessonância Magnética de Crânio: R$5.670,00\r\nEndoscopia Digestiva: R$850,50\r\nTeste Ergométrico: R$567,00\r\nTomografia de Crânio: R$2.346,75\r\nUltrassom Obstétrico: R$441,00\r\nParto: R$11.623,50\r\nHisterectomia Total: R$11.340,00\r\nGastrectomia Total Via Abdominal: R$23.530,50 \r\nRevascularização Miocárdio s/ Extracorpórea: R$25.515,00 \r\nColecistectomia c/ Colangiografia: R$14.458,50 \r\nHemorroidectomia Aberta ou Fechada: R$6.520,50 \r\nApendicectomia: R$7.994,70\r\nArtroscopia do Joelho para Cirurgia: R$8.051,40 ','BLACK T3 ',13),
	(10,'Consulta: R$600,00  \r\nSessão de Psicologia (até): R$248,85  \r\nSessão de Fonoaudiologia: R$248,85  \r\nHemograma Completo: R$126,00  \r\nMamografia: R$500,85\r\nEletrocardiograma: R$220,50\r\nRessonância Magnética de Crânio: R$5.670,00\r\nEndoscopia Digestiva: R$850,50\r\nTeste Ergométrico: R$567,00\r\nTomografia de Crânio: R$2.346,75\r\nUltrassom Obstétrico: R$441,00\r\nParto: R$18.081,00\r\nHisterectomia Total: R$17.640,00\r\nGastrectomia Total Via Abdominal: R$36.603,00 \r\nRevascularização Miocárdio s/ Extracorpórea: R$39.690,00 \r\nColecistectomia c/ Colangiografia: R$22.491,00 \r\nHemorroidectomia Aberta ou Fechada: R$10.143,00\r\nApendicectomia: R$12.436,20\r\nArtroscopia do Joelho para Cirurgia: R$12.524,40 ','BLACK T4',13),
	(11,'Consulta: R$810,00   \r\nSessão de Psicologia (até): R$248,85  \r\nSessão de Fonoaudiologia: R$248,85  \r\nHemograma Completo: R$126,00  \r\nMamografia: R$500,85\r\nEletrocardiograma: R$220,50\r\nRessonância Magnética de Crânio: R$5.670,00\r\nEndoscopia Digestiva: R$850,50\r\nTeste Ergométrico: R$567,00\r\nTomografia de Crânio: R$2.346,75\r\nUltrassom Obstétrico: R$441,00\r\nParto: R$25.830,00\r\nHisterectomia Total: R$25.200,00\r\nGastrectomia Total Via Abdominal: R$52.290,00 \r\nRevascularização Miocárdio s/ Extracorpórea: R$56.700,00\r\nColecistectomia c/ Colangiografia: R$32.130,00\r\nHemorroidectomia Aberta ou Fechada: R$14.490,00\r\nApendicectomia: R$17.766,00\r\nArtroscopia do Joelho para Cirurgia: R$17.892,00 ','BLACK T5 ',13);

/*!40000 ALTER TABLE `reembolsos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regioes`;

CREATE TABLE `regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  `operadora_id` int(11) unsigned DEFAULT NULL,
  `descricao` text,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `regioes_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `regioes_ibfk_2` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `regioes` WRITE;
/*!40000 ALTER TABLE `regioes` DISABLE KEYS */;

INSERT INTO `regioes` (`id`, `nome`, `estado_id`, `operadora_id`, `descricao`)
VALUES
	(1,'Bahia',5,17,'Bahia'),
	(2,'Bahia',5,2,'Bahia'),
	(3,'Bahia',5,1,'Bahia'),
	(4,'Bahia',5,5,'Bahia'),
	(5,'MUNICÍPIO SALVADOR',5,6,'Município Salvador'),
	(6,'HAPVIDA',5,10,'Salvador, Camaçari, Candeias, Conde,Dias D´Ávila, Itaparica, Lauro de Freitas, Madre de Deus,  Vera Cruz, Mata de São João,  Saubara, S. Sebastião do Passé,   Pojuca, Santo Amaro, Simões Filho, S.Francisco do Conde.\r\n'),
	(7,'Salvador e Região Metropolitana',5,26,'Salvador e Região Metropolitana'),
	(9,'Interior da Bahia',5,26,'Interior da Bahia'),
	(10,'Bahia',5,25,'Feira de Santana e Microrregião, Conceição do Coité, Conceição do Jacuípe, Capim Grosso, Campo Formoso, Castro Alves, Cruz das Almas, Ipirá, Itaberaba, Jacobina, Ribeira do Pombal, Senhor do Bonfim, Santo Amaro, Santaluz, Santo Antônio de Jesus, Santo Estevão, Santaluz, Serrinha e Valente.'),
	(11,'Nacional',5,17,'Salvador, Lauro de Freitas, Camaçari, Candeias, Dias D\' Ávila, Simões Filho, Alagoinhas, Catu, Pojuca, São Sebastião do Passé, Madre de Deus, Mata de São João, São Francisco do\r\nConde, Entre Rios, Esplanada, Acajutiba e Santo Amaro.'),
	(12,'Bahia - 02 à 29 Vidas',5,19,'FEIRA DE SANTANA: Feira de Santana, Água Fria, Amélia Rodrigues, Anguera, Antônio Cardoso, Araci, Baixa Grande, Biritinga, Conceição de Feira, Conceição do Jacuípe, Coração de Maria, Fátima, Ipecaetá, Ipirá, Irará, Itatim, Novo Triunfo, Pintadas, Piritiba, Quintino, Rafael Jambeiro, Santa Bárbara, Santanópolis, Santo Estevão, São Gonçalo dos Campos, Serrinha, Tapiramutá, Teofilândia, Terra Nova, Tucano.\r\n\r\nCHAPADA DE DIAMANTINA: Andaraí, Boa Vista do Tupim, Boninal, Bonito, Boquira, Érico Cardoso, Iaçu, Ibiquera, Ibitiara, Iraquara, Itaberaba, Itaeté, Lajedinho, Lençóis, Macajuba, Marcionílio Dias, Mucugê, Nova Redenção, Novo Horizonte, Palmeiras, Piatã, Ruy Barbosa, Seabra, Souto Soares, Tanque Novo, Utinga, Wagner. SISALEIRA: Candeal, Cansanção, Canudos, Capela do Alto Alegre, Conceição do Coité, Euclides da Cunha, Gavião, Ichú, Monte Santo, Nordestina, Nova Fátima, Pé de Serra, Queimadas, Retirolândia, Riachão do Jacuípe, Santa Luz, São Domingos, Serra Preta, Tanquinho, Valente.'),
	(13,'Bahia - 03 à 29 VIDAS ',5,19,'Feira de Santana\r\n'),
	(14,'Bahia e Minas Gerais',5,23,'Minas Gerais: Belo Horizonte e Contagem.\r\n\r\nBahia: Alagoinhas, Barreiras, Brumado, Camaçari, Candeias, Catu, Dias D\'Ávila, Eunápolis, Feira de Santana, Ilhéus, Itabuna, Itapetinga, Jequié, Lauro de Freitas, Mucuri, Pojuca, Porto Seguro, Salvador, Serrinha, Simões Filho, Teixeira de Freitas, Valença e Vitória da Consquista.\r\n'),
	(15,'Bahia',5,18,'Bahia'),
	(16,'Bahia',5,27,'Bahia'),
	(17,'MUNICÍPIO SALVADOR',5,4,'MUNICÍPIO SALVADOR'),
	(18,'Bahia',5,13,'Bahia');

/*!40000 ALTER TABLE `regioes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela simulacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes`;

CREATE TABLE `simulacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `contato` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `faixa1` int(11) DEFAULT NULL,
  `faixa2` int(11) DEFAULT NULL,
  `faixa3` int(11) DEFAULT NULL,
  `faixa4` int(11) DEFAULT NULL,
  `faixa5` int(11) DEFAULT NULL,
  `faixa6` int(11) DEFAULT NULL,
  `faixa7` int(11) DEFAULT NULL,
  `faixa8` int(11) DEFAULT NULL,
  `faixa9` int(11) DEFAULT NULL,
  `faixa10` int(11) DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `telefone` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela simulacoes_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes_tabelas`;

CREATE TABLE `simulacoes_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `simulacao_id` int(11) unsigned NOT NULL,
  `tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `simulacao_id` (`simulacao_id`),
  KEY `tabela_id` (`tabela_id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_1` FOREIGN KEY (`simulacao_id`) REFERENCES `simulacoes` (`id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_2` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela subprodutos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subprodutos`;

CREATE TABLE `subprodutos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `tipo_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_id` (`tipo_id`),
  CONSTRAINT `subprodutos_ibfk_1` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `subprodutos` WRITE;
/*!40000 ALTER TABLE `subprodutos` DISABLE KEYS */;

INSERT INTO `subprodutos` (`id`, `descricao`, `nome`, `tipo_id`)
VALUES
	(1,'TESTE','TESTE',1);

/*!40000 ALTER TABLE `subprodutos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas`;

CREATE TABLE `tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `vigencia` date NOT NULL,
  `faixa1` float DEFAULT NULL,
  `faixa2` float DEFAULT NULL,
  `faixa3` float DEFAULT NULL,
  `faixa4` float DEFAULT NULL,
  `faixa5` float DEFAULT NULL,
  `faixa6` float DEFAULT NULL,
  `faixa7` float DEFAULT NULL,
  `faixa8` float DEFAULT NULL,
  `faixa9` float DEFAULT NULL,
  `faixa10` float DEFAULT NULL,
  `produto_id` int(11) unsigned NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  `ramo_id` int(11) unsigned NOT NULL,
  `abrangencia_id` int(11) unsigned NOT NULL,
  `tipo_id` int(11) unsigned NOT NULL,
  `modalidade_id` int(11) unsigned NOT NULL,
  `carencia_id` int(11) unsigned DEFAULT NULL,
  `opcional_id` int(11) unsigned DEFAULT NULL,
  `reembolso_id` int(11) unsigned DEFAULT NULL,
  `rede_id` int(11) unsigned DEFAULT NULL,
  `observacao_id` int(11) unsigned DEFAULT NULL,
  `informacao_id` int(11) unsigned DEFAULT NULL,
  `validade` char(1) DEFAULT NULL,
  `estado_id` int(11) unsigned DEFAULT NULL,
  `regiao_id` int(11) unsigned DEFAULT NULL,
  `cod_ans` varchar(50) DEFAULT NULL,
  `minimo_vidas` int(11) DEFAULT NULL,
  `maximo_vidas` int(11) DEFAULT NULL,
  `titulares` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_id` (`produto_id`),
  KEY `operadora_id` (`operadora_id`),
  KEY `ramo_id` (`ramo_id`),
  KEY `abrangencia_id` (`abrangencia_id`),
  KEY `tipo_id` (`tipo_id`),
  KEY `modalidade_id` (`modalidade_id`),
  KEY `estado_id` (`estado_id`),
  KEY `regiao_id` (`regiao_id`),
  KEY `reembolso_id` (`reembolso_id`),
  KEY `rede_id` (`rede_id`),
  KEY `observacao_id` (`observacao_id`),
  KEY `informacao_id` (`informacao_id`),
  KEY `carencia_id` (`carencia_id`),
  KEY `opcional_id` (`opcional_id`),
  CONSTRAINT `tabelas_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`),
  CONSTRAINT `tabelas_ibfk_10` FOREIGN KEY (`reembolso_id`) REFERENCES `reembolsos` (`id`),
  CONSTRAINT `tabelas_ibfk_11` FOREIGN KEY (`rede_id`) REFERENCES `redes` (`id`),
  CONSTRAINT `tabelas_ibfk_12` FOREIGN KEY (`observacao_id`) REFERENCES `observacoes` (`id`),
  CONSTRAINT `tabelas_ibfk_13` FOREIGN KEY (`informacao_id`) REFERENCES `informacoes` (`id`),
  CONSTRAINT `tabelas_ibfk_14` FOREIGN KEY (`carencia_id`) REFERENCES `carencias` (`id`),
  CONSTRAINT `tabelas_ibfk_15` FOREIGN KEY (`opcional_id`) REFERENCES `opcionais` (`id`),
  CONSTRAINT `tabelas_ibfk_2` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`),
  CONSTRAINT `tabelas_ibfk_3` FOREIGN KEY (`ramo_id`) REFERENCES `ramos` (`id`),
  CONSTRAINT `tabelas_ibfk_4` FOREIGN KEY (`abrangencia_id`) REFERENCES `abrangencias` (`id`),
  CONSTRAINT `tabelas_ibfk_5` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`),
  CONSTRAINT `tabelas_ibfk_6` FOREIGN KEY (`modalidade_id`) REFERENCES `modalidades` (`id`),
  CONSTRAINT `tabelas_ibfk_8` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `tabelas_ibfk_9` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tabelas` WRITE;
/*!40000 ALTER TABLE `tabelas` DISABLE KEYS */;

INSERT INTO `tabelas` (`id`, `nome`, `descricao`, `vigencia`, `faixa1`, `faixa2`, `faixa3`, `faixa4`, `faixa5`, `faixa6`, `faixa7`, `faixa8`, `faixa9`, `faixa10`, `produto_id`, `operadora_id`, `ramo_id`, `abrangencia_id`, `tipo_id`, `modalidade_id`, `carencia_id`, `opcional_id`, `reembolso_id`, `rede_id`, `observacao_id`, `informacao_id`, `validade`, `estado_id`, `regiao_id`, `cod_ans`, `minimo_vidas`, `maximo_vidas`, `titulares`)
VALUES
	(5,'400 – PLANO - 02 À 29 VIDAS - SEM COPARTICIPAÇÃO','400 – PLANO -  02 À 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',266.89,333.61,417.01,458.71,481.65,529.82,662.28,728.51,910.64,1593.62,84,2,1,1,1,3,2,2,NULL,3,3,2,NULL,5,2,'472937144',2,29,NULL),
	(6,'400 – PLANO PME 02 À 29 VIDAS - SEM COPARTICIPAÇÃO','400 – PLANO PME 02 À 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',301.71,377.14,471.43,518.57,544.5,598.95,748.69,823.56,1029.45,1801.54,84,2,1,2,2,3,2,2,NULL,3,3,2,NULL,5,2,'472940144',2,29,NULL),
	(8,'500 – PLANO - 02 À 29 VIDAS - SEM COPARTICIPAÇÃO','500 – PLANO - 02 À 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',333.24,416.55,520.69,572.76,601.4,661.54,826.93,909.62,1137.03,1989.8,85,2,1,2,2,3,2,2,NULL,4,3,2,NULL,5,2,'472942141',2,29,NULL),
	(10,'700 – PLANO - 02 À 29 VIDAS - SEM COPARTICIPAÇÃO','700 – PLANO - 02 À 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',415.05,518.81,648.51,713.36,749.03,823.93,1029.91,1132.9,1416.13,2478.23,86,2,1,2,2,3,2,2,NULL,5,3,2,NULL,5,2,'472841146',2,29,NULL),
	(11,'400 – PLANO PME 02 À 29 VIDAS - COM COPARTICIPAÇÃO','400 – PLANO PME 02 À 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',213.49,266.86,333.58,366.94,385.29,423.82,529.78,582.76,728.45,1274.79,87,2,1,2,1,3,2,2,NULL,3,3,2,NULL,5,2,'472936146',2,29,NULL),
	(12,'400 – PLANO - 02 À 29 VIDAS - COM COPARTICIPAÇÃO','400 – PLANO - 02 À 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',241.38,301.73,377.16,414.88,435.62,479.18,598.98,658.88,823.6,1441.3,87,2,1,2,2,3,2,2,NULL,3,3,2,'0',5,2,'472939141',2,29,NULL),
	(13,'500 – PLANO - 02 À 29 VIDAS - COM COPARTICIPAÇÃO','500 – PLANO - 02 À 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',266.61,333.26,416.58,458.24,481.15,529.27,661.59,727.75,909.69,1591.96,88,2,1,2,2,3,2,2,NULL,4,3,2,'0',5,2,'472835141',2,29,NULL),
	(14,'700 – PLANO - 02 À 29 VIDAS - COM COPARTICIPAÇÃO','700 – PLANO - 02 À 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',332.04,415.05,518.81,570.69,599.22,659.14,823.93,906.32,1132.9,1982.58,89,2,1,2,2,3,2,2,NULL,5,3,2,NULL,5,2,'472840148',2,29,NULL),
	(15,'400 – PLANO - 30 À 99 VIDAS - SEM COPARTICIPAÇÃO','400 – PLANO - 30 À 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',253.53,316.91,396.14,435.75,457.54,503.29,629.11,692.02,865.03,1513.8,90,2,1,2,1,3,2,3,NULL,6,4,3,NULL,5,2,'472937144',30,99,NULL),
	(16,'400 – PLANO - 30 À 99 VIDAS - SEM COPARTICIPAÇÃO','400 – PLANO - 30 À 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',286.62,358.28,447.85,492.64,517.27,569,711.25,782.38,977.98,1711.47,90,2,1,2,2,3,2,3,NULL,6,4,3,NULL,5,2,'472940144',30,99,NULL),
	(17,'500 – PLANO - 30 À 99 VIDAS - SEM COPARTICIPAÇÃO','500 – PLANO - 30 À 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',316.58,395.73,494.66,544.13,571.34,628.47,785.59,864.15,1080.19,1890.33,91,2,1,2,2,3,2,3,NULL,7,4,3,NULL,5,2,'472942141',30,99,NULL),
	(18,'700 – PLANO PME 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','700 – PLANO PME 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',394.29,492.86,616.08,677.69,711.57,782.73,978.41,1076.25,1345.31,2354.29,92,2,1,1,2,3,2,3,NULL,8,4,3,NULL,5,2,'472841146',30,99,NULL),
	(19,'400 – PLANO PME 30 À 99 VIDAS - COM COPARTICIPAÇÃO','400 – PLANO PME 30 À 99 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',202.82,253.53,316.91,348.6,366.03,402.63,503.29,553.62,692.03,1211.05,93,2,1,1,1,3,2,3,NULL,6,4,3,NULL,5,2,'472936146',30,99,NULL),
	(20,'AMIL – PLANO PME 30 À 99 VIDAS','AMIL – PLANO PME 30 À 99 VIDAS','2017-04-30',229.31,286.64,358.3,394.13,413.84,455.22,569.03,625.93,782.41,1369.22,93,2,1,1,2,3,2,3,NULL,6,4,3,NULL,5,2,' 472939141',30,99,NULL),
	(21,'AMIL – PLANO PME 30 À 99 VIDAS','AMIL – PLANO PME 30 À 99 VIDAS','2017-04-30',253.29,316.61,395.76,435.34,457.11,502.82,628.53,691.38,864.23,1512.4,94,2,1,1,2,3,2,3,NULL,7,4,3,NULL,5,2,' 472835141',30,99,NULL),
	(22,'AMIL – PLANO PME 30 À 99 VIDAS','AMIL – PLANO PME 30 À 99 VIDAS','2017-04-30',315.43,394.29,492.86,542.15,569.26,626.19,782.74,861.01,1076.26,1883.46,95,2,1,1,2,3,2,3,NULL,8,4,3,NULL,5,2,'472840148',30,99,NULL),
	(23,'HOSPITALAR NACIONAL - OPCIONAL ','HOSPITALAR NACIONAL - OPCIONAL - 03 à 29 VIDAS','2017-04-30',214.51,262.13,325.04,357.54,379,439.63,525.56,611.05,727.16,1287.06,96,1,1,1,1,3,5,5,NULL,9,5,4,'0',5,3,'',3,29,NULL),
	(24,'HOSPITALAR NACIONAL - OPCIONAL ','HOSPITALAR NACIONAL - OPCIONAL - 03 à 29 VIDAS','2017-04-30',255.37,312.05,386.95,425.65,451.18,523.37,625.66,727.44,865.66,1532.22,96,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(25,'HOSPITALAR NACIONAL - COMPULSÓRIA','HOSPITALAR NACIONAL - COMPULSÓRIA - 03 à 29 VIDAS ','2017-04-30',193.06,235.92,292.53,321.79,341.1,395.67,473.01,549.95,654.44,1158.36,98,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(26,'HOSPITALAR NACIONAL - COMPULSÓRIA','HOSPITALAR NACIONAL - COMPULSÓRIA - 03 à 29 VIDAS','2017-04-30',229.83,280.85,348.25,383.09,406.07,471.04,563.09,654.7,779.09,1379,98,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(27,'HOSPITALAR NACIONAL - OPCIONAL ','HOSPITALAR NACIONAL - OPCIONAL - 04 à 29 VIDAS','2017-04-30',194.99,238.28,295.46,325.01,344.51,399.63,477.74,555.45,660.99,1169.95,99,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(28,'HOSPITALAR NACIONAL - OPCIONAL ','HOSPITALAR NACIONAL - OPCIONAL - 04 à 29 VIDAS','2017-04-30',232.13,283.66,351.74,386.92,410.13,475.75,568.73,661.25,786.89,1392.8,99,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(29,'HOSPITALAR NACIONAL - COMPULSÓRIA','HOSPITALAR NACIONAL - COMPULSÓRIA - 04 à 29','2017-04-30',175.49,214.45,265.91,292.51,310.06,359.67,429.97,499.91,594.89,1052.96,100,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(30,'HOSPITALAR NACIONAL - COMPULSÓRIA','HOSPITALAR NACIONAL - COMPULSÓRIA - 04 à 29 VIDAS\r\n','2017-04-30',208.92,255.29,316.57,348.23,369.12,428.18,511.86,595.13,708.2,1253.52,100,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(31,'PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO ','PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO ','2017-04-30',268.34,327.9,406.6,447.26,474.1,549.96,657.44,764.38,909.62,1610.03,103,1,1,3,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(32,'PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO ','PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO ','2017-04-30',295.2,360.72,447.3,492.03,521.56,605.01,723.25,840.89,1000.67,1771.19,101,1,1,3,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(33,'PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO','PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',298.15,364.34,451.78,496.96,526.78,611.06,730.49,849.32,1010.69,1788.92,103,1,1,3,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(34,'PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO ','PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO ','2017-04-30',327.99,400.81,497,546.71,579.51,672.23,803.61,934.34,1111.86,1967.99,101,1,1,3,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(35,'PERFIL - OPCIONAL - COPARTICIPAÇÃO 10%','PERFIL - OPCIONAL - COPARTICIPAÇÃO 10%','2017-04-30',260.1,317.83,394.12,433.53,459.55,533.08,637.26,740.91,881.69,1560.6,104,1,1,3,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(37,'PERFIL - OPCIONAL - COPARTICIPAÇÃO 10%','PERFIL - OPCIONAL - COPARTICIPAÇÃO 10%','2017-04-30',289,353.15,437.91,481.7,510.61,592.3,708.06,823.25,979.66,1734,104,1,1,3,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(38,'PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',265.68,324.65,402.57,442.83,469.4,544.51,650.92,756.8,900.61,1594.07,102,1,1,3,1,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(39,'PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',295.2,360.73,447.3,492.04,521.56,605,723.25,840.9,1000.67,1771.19,102,1,1,3,2,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(40,'PERFIL - OPCIONAL - COPARTICIPAÇÃO 20%','PERFIL - OPCIONAL - COPARTICIPAÇÃO 20%','2017-04-30',251.86,307.77,381.63,419.8,444.99,516.19,617.07,717.45,853.77,1511.17,105,1,1,3,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(41,'PERFIL - OPCIONAL - COPARTICIPAÇÃO 20%','PERFIL - OPCIONAL - COPARTICIPAÇÃO 20%','2017-03-30',279.84,341.97,424.04,466.45,494.44,573.54,685.64,797.17,948.63,1679.08,105,1,1,3,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(42,'PERFIL - OPCIONAL - COPARTICIPAÇÃO 30%','PERFIL - OPCIONAL - COPARTICIPAÇÃO 30%','2017-04-30',238.61,291.57,361.55,397.7,421.57,489.02,584.6,679.69,808.83,1431.64,106,1,1,3,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(43,'PERFIL - OPCIONAL - COPARTICIPAÇÃO 30%','PERFIL - OPCIONAL - COPARTICIPAÇÃO 30%','2017-04-30',265.11,323.97,401.72,441.9,468.41,543.35,649.55,755.22,898.71,1590.71,106,1,1,3,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(44,'PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO','PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',248.83,304.06,377.04,414.74,439.62,509.96,609.63,708.8,843.47,1492.95,111,1,1,3,1,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,NULL),
	(46,'PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO ','PERFIL - OPCIONAL - SEM COPARTICIPAÇÃO ','2017-04-30',276.47,337.84,418.93,460.82,488.47,566.63,677.37,787.56,937.19,1658.84,111,1,1,3,2,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,NULL),
	(47,'PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO ','PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO ','2017-04-30',213.86,261.34,324.07,356.47,377.85,438.31,523.98,609.21,724.96,1283.19,112,1,1,3,1,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,NULL),
	(48,'PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',237.62,290.37,360.07,396.07,419.84,487.02,582.2,676.91,805.51,1425.77,112,1,1,3,2,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,NULL),
	(49,'PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',241.51,295.11,365.94,402.53,426.69,494.96,591.7,687.94,818.66,1449.03,107,1,1,3,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(50,'PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','PERFIL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',268.34,327.91,406.6,447.26,474.1,549.95,657.44,764.39,909.62,1610.03,107,1,1,3,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(51,'PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','2017-04-30',234.09,286.05,354.71,390.18,413.59,479.77,573.53,666.82,793.53,1404.54,108,1,1,3,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(52,'PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','2017-04-30',260.1,317.84,394.12,433.53,459.55,533.07,637.26,740.92,881.7,1560.6,108,1,1,3,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(53,'PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 20% ','PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 20% ','2017-04-30',226.68,276.99,343.47,377.82,400.49,464.57,555.37,645.7,768.39,1360.06,109,1,1,3,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(54,'PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','2017-04-30',251.86,307.77,381.64,419.8,444.99,516.19,617.07,717.45,853.77,1511.17,109,1,1,3,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(56,'PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','2017-04-30',214.75,262.41,325.39,357.93,379.41,440.12,526.14,611.72,727.95,1288.47,110,1,1,3,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(57,'PERFIL - OPCIONAL - COPARTICIPAÇÃO 30%','PERFIL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','2017-04-30',238.6,291.57,361.55,397.71,421.57,489.02,584.6,679.69,808.83,1431.64,110,1,1,3,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,NULL),
	(58,'FLEX NACIONAL – OPCIONAL – SEM COPARTICIPAÇÃO','FLEX NACIONAL – OPCIONAL – SEM COPARTICIPAÇÃO','2017-04-30',321.77,393.2,487.56,536.32,568.5,659.45,788.34,916.58,1090.73,1930.6,113,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(59,'FLEX NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','FLEX NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',357.52,436.88,541.73,595.91,631.67,732.73,875.93,1018.43,1211.93,2145.11,113,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(60,'FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','2017-04-30',311.89,381.13,472.6,519.86,551.05,639.21,764.14,888.44,1057.24,1871.33,114,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(62,'FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','2017-04-30',346.55,423.47,525.1,577.62,612.27,710.24,849.04,987.16,1174.72,2079.25,114,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(63,'FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','2017-04-30',302.01,369.06,457.63,503.39,533.59,618.96,739.94,860.3,1023.76,1812.06,115,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(64,'FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','2017-04-30',335.27,410.06,508.47,559.32,592.88,687.74,822.15,955.9,1137.51,2013.4,115,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(65,'FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','2017-04-30',286.12,349.63,433.54,476.9,505.51,586.39,700.99,815.02,969.87,1716.69,116,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(66,'FLEX NACIONAL - OPCIONAL – SEM COPARTICIPAÇÃO','FLEX NACIONAL - OPCIONAL – SEM COPARTICIPAÇÃO','2017-04-30',324.99,397.13,492.44,541.69,574.19,666.06,796.23,925.76,1101.65,1949.92,120,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(67,'FLEX NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO ','FLEX NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO ','2017-04-30',292.49,357.42,443.2,487.52,516.77,599.45,716.61,833.18,991.48,1754.93,120,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(68,'FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','FLEX NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','2017-04-30',317.91,388.48,481.71,529.89,561.68,651.55,778.88,905.59,1077.64,1907.43,116,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(69,'FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 10%','FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 10%','2017-04-30',283.51,346.45,429.59,472.55,500.91,581.05,694.61,807.6,961.04,1701.05,121,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(70,'FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 10% ','FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 10% ','2017-04-30',315.01,384.94,477.32,525.06,556.56,645.61,771.79,897.34,1067.83,1890.06,121,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(71,'FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 20%','FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 20%','2017-04-30',274.53,335.47,415.99,457.59,485.04,562.64,672.61,782.02,930.6,1647.18,122,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(72,'FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 20% ','FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 20% ','2017-04-30',305.04,372.75,462.2,508.43,538.93,625.16,747.34,868.92,1034.01,1830.19,122,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(73,'FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 30%','FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 30%','2017-04-30',260.08,317.82,394.09,433.5,459.51,533.03,637.21,740.86,881.62,1560.48,123,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(74,'FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 30%','FLEX NACIONAL - OPCIONAL – COPARTICIPAÇÃO 30%','2017-04-30',288.98,353.13,437.88,481.67,510.57,592.26,708.01,823.19,979.59,1733.87,123,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(75,'FLEX NACIONAL - COMPULSÓRIA – SEM COPARTICIPAÇÃO','FLEX NACIONAL - COMPULSÓRIA – SEM COPARTICIPAÇÃO','2017-04-30',263.24,321.68,398.88,438.77,465.09,539.51,644.95,749.86,892.33,1579.44,124,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(76,'FLEX NACIONAL - COMPULSÓRIA – SEM COPARTICIPAÇÃO ','FLEX NACIONAL -  COMPULSÓRIA – SEM COPARTICIPAÇÃO ','2017-04-30',292.49,357.42,443.2,487.52,516.77,599.45,716.61,833.18,991.49,1754.93,124,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(77,'FLEX NACIONAL - COMPULSÓRIA – COPARTICIPAÇÃO 10% ','FLEX NACIONAL  - COMPULSÓRIA – COPARTICIPAÇÃO 10% ','2017-04-30',255.16,311.8,386.63,425.3,450.81,522.94,625.15,726.84,864.94,1530.95,125,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(78,'FLEX NACIONAL - COMPULSÓRIA – COPARTICIPAÇÃO 10%','FLEX NACIONAL -  COMPULSÓRIA – COPARTICIPAÇÃO 10%','2017-04-30',283.51,346.44,429.59,472.55,500.91,581.05,694.61,807.61,961.05,1701.05,125,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(79,'FLEX NACIONAL - COMPULSÓRIA – COPARTICIPAÇÃO 20%','FLEX NACIONAL -  COMPULSÓRIA – COPARTICIPAÇÃO 20%','2017-04-30',247.08,301.93,374.39,411.83,436.54,506.38,605.35,703.82,837.54,1482.46,126,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(80,'FLEX NACIONAL - COMPULSÓRIA – COPARTICIPAÇÃO 20%','FLEX NACIONAL - COMPULSÓRIA – COPARTICIPAÇÃO 20%','2017-04-30',274.53,335.47,415.98,457.59,485.04,562.65,672.61,782.03,930.61,1647.18,126,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(81,'FLEX NACIONAL - COMPULSÓRIA – COPARTICIPAÇÃO 30% ','FLEX NACIONAL - COMPULSÓRIA – COPARTICIPAÇÃO 30% ','2017-04-30',234.07,286.04,354.68,390.15,413.56,479.73,573.49,666.78,793.46,1404.44,127,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(82,'FLEX NACIONAL - COMPULSÓRIA – COPARTICIPAÇÃO 30%','FLEX NACIONAL - COMPULSÓRIA – COPARTICIPAÇÃO 30%','2017-04-30',260.08,317.82,394.09,433.5,459.51,533.03,637.21,740.87,881.63,1560.48,127,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(83,'FLEX NACIONAL - OPCIONAL – SEM COPARTICIPAÇÃO','FLEX NACIONAL - OPCIONAL – SEM COPARTICIPAÇÃO\r\n','2017-04-30',292.49,357.42,443.2,487.52,516.77,599.45,716.61,833.18,991.48,1754.93,128,1,1,1,1,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,2),
	(84,'FLEX NACIONAL - 30 à 99 VIDAS - OPCIONAL – SEM COPARTICIPAÇÃO','FLEX NACIONAL - 30 à 99 VIDAS -  OPCIONAL – SEM COPARTICIPAÇÃO','2017-04-30',324.99,397.13,492.44,541.69,574.19,666.06,796.23,925.76,1101.65,1949.92,128,1,1,1,2,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,2),
	(88,'FLEX NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','FLEX NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',289.59,353.88,438.81,482.69,511.65,593.51,709.51,824.92,981.65,1737.54,117,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(89,'FLEX NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','FLEX NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',321.77,393.19,487.56,536.32,568.5,659.46,788.34,916.59,1090.73,1930.6,117,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(90,'FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','2017-04-30',280.7,343.01,425.34,467.87,495.94,575.29,687.73,799.6,951.52,1684.2,118,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(91,'FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','2017-04-30',311.89,381.12,472.59,519.86,551.05,639.21,764.14,888.45,1057.25,1871.33,118,1,1,1,2,3,3,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(92,'FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','2017-04-30',271.81,332.15,411.87,453.05,480.23,557.07,665.94,774.27,921.38,1630.85,236,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(93,'FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','2017-04-30',257.5,314.67,390.19,429.21,454.96,527.75,630.89,733.52,872.89,1545.02,119,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(94,'FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20% ','FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20% ','2017-04-30',302.01,369.05,457.62,503.39,533.59,618.97,739.94,860.31,1023.76,1812.06,236,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(95,'FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','2017-04-30',286.12,349.63,433.54,476.9,505.51,586.39,700.99,815.03,969.88,1716.69,236,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,NULL),
	(96,'FLEX NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO ','FLEX NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO ','2017-04-30',251.39,307.2,380.93,419.02,444.16,515.23,615.93,716.12,852.18,1508.36,129,1,1,1,1,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,2),
	(97,'FLEX NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO ','FLEX NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO ','2017-04-30',279.32,341.33,423.25,465.58,493.52,572.48,684.36,795.69,946.87,1675.96,129,1,1,1,2,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,2),
	(98,' TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO',' TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',357.52,436.88,541.73,595.91,631.67,732.73,875.93,1018.43,1211.93,2145.11,130,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(99,'TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',425.62,520.09,644.92,709.41,751.98,872.3,1042.77,1212.41,1442.77,2553.71,130,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(100,' TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%',' TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','2017-04-30',346.55,423.47,525.1,577.62,612.27,710.24,849.04,987.16,1174.72,2079.25,131,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(101,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','2017-04-30',413.27,505.01,626.22,688.84,730.18,847.01,1012.53,1177.25,1400.93,2479.65,131,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(103,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','2017-04-30',335.57,410.06,508.47,559.32,592.88,687.74,822.15,955.9,1137.51,2013.4,132,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(104,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','2017-04-30',400.93,489.93,607.52,668.26,708.37,821.71,982.29,1142.09,1359.09,2405.59,132,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(105,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','2017-04-30',317.91,388.48,481.71,529.89,561.68,651.55,778.88,905.59,1077.64,1907.43,133,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(106,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','2017-04-30',381.69,466.42,578.37,636.2,674.38,782.28,935.16,1087.29,1293.88,2290.16,133,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(108,'TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',383.06,468.08,580.43,638.47,676.79,785.07,938.5,1091.17,1298.49,2298.34,134,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(109,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','2017-04-30',311.89,381.12,472.59,519.86,551.05,639.21,764.14,888.45,1057.25,1871.33,135,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(110,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','TOP NACIONAL - O - COPARTICIPAÇÃO 10%','2017-03-29',371.95,454.51,563.6,619.95,657.16,762.3,911.28,1059.52,1260.84,2231.68,237,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,NULL,NULL,NULL,NULL),
	(111,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','2017-04-30',302.01,369.05,457.62,503.39,533.59,618.97,739.94,860.31,1023.76,1812.06,136,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',30,29,1),
	(112,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','2017-04-30',328.01,400.81,497.01,546.71,579.52,672.25,803.62,934.35,1111.88,1968.03,144,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(113,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','2017-03-30',286.12,349.63,433.54,476.9,505.51,586.39,700.99,815.03,969.88,1716.69,137,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(114,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','2017-04-30',343.52,419.78,520.53,572.58,606.94,704.05,841.64,978.56,1164.49,2061.15,137,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(115,'TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',324.99,397.13,492.44,541.69,574.19,666.06,796.23,925.76,1101.65,1949.92,138,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(116,'TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',386.89,472.77,586.24,644.86,683.56,792.93,947.89,1102.09,1311.49,2321.34,138,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(117,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','2017-04-30',315.01,384.94,477.32,525.06,556.56,645.61,771.79,897.34,1067.83,1890.06,139,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(118,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 10%','2017-04-30',375.67,459.06,569.24,626.16,663.74,769.94,920.4,1070.13,1273.46,2254.02,139,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(119,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','2017-04-30',305.04,372.75,462.2,508.43,538.93,625.16,747.34,868.92,1034.01,1830.19,140,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(120,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 20%','2017-04-30',364.45,445.35,552.24,607.46,643.91,746.94,892.91,1038.17,1235.42,2186.7,140,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(121,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','2017-04-30',288.98,353.13,437.88,481.67,510.57,592.26,708.01,823.19,979.59,1733.87,141,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(122,'TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','TOP NACIONAL - OPCIONAL - COPARTICIPAÇÃO 30%','2017-04-30',346.96,423.98,525.74,578.31,613.02,711.1,850.07,988.35,1176.14,2081.78,141,1,1,1,2,3,5,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(123,'TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',292.49,357.42,443.2,487.52,516.77,599.45,716.61,833.18,991.49,1754.93,142,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(124,'TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',348.2,425.49,527.62,580.37,615.2,713.64,853.1,991.88,1180.34,2089.21,142,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(125,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','2017-04-30',283.51,346.44,429.59,472.55,500.91,581.05,694.61,807.61,961.05,1701.05,143,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(126,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','2017-04-30',338.1,413.15,512.32,563.54,597.36,692.94,828.36,963.12,1146.11,2028.62,143,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(127,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','2017-04-30',274.53,335.47,415.98,457.59,485.04,562.65,672.61,782.03,930.61,1647.18,144,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(128,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','2017-04-30',260.08,317.82,394.09,433.5,459.51,533.03,637.21,740.87,881.63,1560.48,145,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(129,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','2017-04-30',312.27,381.58,473.17,520.48,551.71,639.99,765.06,889.52,1058.53,1873.6,145,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',4,29,2),
	(130,'TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',324.99,397.13,492.44,541.69,574.19,666.06,796.23,925.76,1101.65,1949.92,146,1,1,1,1,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,NULL),
	(131,'TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',386.89,472.77,586.24,644.86,683.56,792.93,947.89,1102.09,1311.49,2321.34,146,1,1,1,2,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,NULL),
	(133,'TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',279.32,341.33,423.25,465.58,493.52,572.48,684.36,795.69,946.87,1675.96,147,1,1,1,1,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,NULL),
	(134,'TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO','2017-04-30',332.53,406.34,503.87,554.26,587.52,681.52,814.71,947.25,1127.23,1995.19,147,1,1,1,2,3,6,5,NULL,9,5,4,NULL,5,3,'',30,99,NULL),
	(135,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 10%','2017-04-30',371.95,454.51,563.6,619.95,657.16,762.3,911.28,1059.52,1260.84,2231.68,135,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(136,'TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','TOP NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 20%','2017-04-30',360.84,440.94,546.77,601.44,637.53,739.54,884.06,1027.88,1223.18,2165.03,136,1,1,1,2,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(137,'TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','TOP NACIONAL - OPCIONAL - SEM COPARTICIPAÇÃO','2017-04-30',357.52,436.88,541.73,595.91,631.67,732.73,875.93,1018.43,1211.93,2145.11,130,1,1,1,1,3,5,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(138,'FUNDAMENTAL P111 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','FUNDAMENTAL P111 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',283.25,307.33,347.29,404.94,477.82,580.07,730.91,888.04,1110.05,1698.4,238,5,1,1,1,3,7,6,4,10,6,5,NULL,5,4,'474.916/15-2',3,29,1),
	(139,'FUNDAMENTAL P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','FUNDAMENTAL P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',324.38,351.94,397.71,463.72,547.2,664.28,836.99,1016.95,1271.2,1944.94,239,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'474.915/15-4',3,29,1),
	(140,'VITAL P111 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','VITAL P111 ‐  COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',284.88,309.09,349.29,407.28,480.57,583.42,735.11,893.14,1116.44,1708.15,240,5,1,1,1,3,7,6,4,NULL,6,5,NULL,5,4,'474.917/15-7',3,29,1),
	(142,'VITAL P121 - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','VITAL P121 - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',326.24,353.97,399.99,466.39,550.34,668.1,841.8,1022.8,1278.5,1956.11,241,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'474.923/15-5',3,29,1),
	(143,'PRONTO P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','PRONTO P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',363.32,394.22,445.47,519.4,612.89,744.06,937.53,1139.1,1423.88,2178.53,242,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'475.050/15-1',3,29,1),
	(144,'COMPLETO P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',593.46,643.88,727.59,848.38,1001.1,1215.33,1531.3,1860.55,2325.7,3558.32,243,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'475.046/15-2',3,29,1),
	(145,'COMPLETO + P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO + P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',814.77,884.03,998.95,1164.77,1374.44,1668.58,2102.41,2554.43,3193.03,4885.34,252,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'475.088/15-8',3,29,1),
	(146,'FUNDAMENTAL P111 ‐ VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ','FUNDAMENTAL P111 ‐ VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',325.74,353.43,399.38,465.69,549.5,667.1,840.54,1021.25,1276.56,1953.15,245,5,1,1,1,3,7,6,4,10,6,5,NULL,5,4,'474.916/15-2',3,29,1),
	(147,'FUNDAMENTAL P121 ‐ VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO ','FUNDAMENTAL P121 ‐ 1 TITULAR  - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO ','2017-04-30',373.03,404.74,457.36,533.28,629.28,763.92,962.55,1169.5,1461.89,2236.68,246,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'474.915/15-4',3,29,1),
	(156,'VITAL P111 - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','VITAL P111 - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','2017-04-30',32762,355.46,401.67,468.36,552.65,670.92,845.38,1027.11,1283.91,1964.38,247,5,1,1,1,3,7,6,4,10,6,5,NULL,5,4,'474.971/15-1',3,29,1),
	(157,'VITAL P121 - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','VITAL P121 - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','2017-04-30',375.17,407.06,459.99,536.36,632.89,768.32,968.08,1176.22,1470.28,2249.53,248,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'474.923/15-5',3,29,1),
	(158,'PRONTO P121 - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','PRONTO P121 - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','2017-04-30',417.83,453.35,512.28,597.32,704.83,855.67,1078.16,1309.96,1637.45,2505.31,249,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'475.050/15-1',3,29,1),
	(159,'COMPLETO P121 ‐ VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','COMPLETO P121 - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','2017-04-30',682.46,740.47,836.73,975.63,1151.25,1397.62,1761,2139.63,2674.56,4092.08,250,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'475.046/15-2',3,29,1),
	(160,'COMPLETO + P121 - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','COMPLETO + P121 - VOLUNTÁRIA  ‐ SEM COPARTICIPAÇÃO','2017-04-30',936.99,1016.63,1148.79,1339.5,1580.61,1918.86,2417.77,2937.6,3671.98,5618.13,251,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'475.088/15-8',3,29,1),
	(161,'FUNDAMENTAL P211 ‐ COMPULSÓRIA ‐ COM COPARTICIPAÇÃO','FUNDAMENTAL P211 ‐ COMPULSÓRIA ‐ COM COPARTICIPAÇÃO','2017-04-30',242.37,262.97,297.14,346.46,408.81,496.31,625.35,759.81,949.77,1453.15,253,5,1,1,1,3,7,6,3,10,6,5,NULL,5,4,'474.924/15-3',3,29,1),
	(162,'FUNDAMENTAL P221 ‐ COMPULSÓRIA ‐ COM COPARTICIPAÇÃO','FUNDAMENTAL P221 ‐ COMPULSÓRIA ‐ COM COPARTICIPAÇÃO','2017-04-30',281.78,305.73,345.48,402.83,475.35,577.08,727.12,883.46,1104.32,1689.61,254,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'474.925/15-1',3,29,1),
	(163,'VITAL P111 ‐ COMPULSÓRIA ‐ COM COPARTICIPAÇÃO ','VITAL P111 ‐  COMPULSÓRIA ‐ COM COPARTICIPAÇÃO ','2017-04-30',243.75,264.47,298.86,348.45,411.17,499.17,628.94,764.17,955.22,1461.51,255,5,1,1,1,3,7,6,3,10,6,5,NULL,5,4,'474.922/15-7',3,29,1),
	(164,'VITAL P121 - COMPULSÓRIA ‐ COM COPARTICIPAÇÃO ','VITAL P121 - COMPULSÓRIA ‐ COM COPARTICIPAÇÃO ','2017-04-30',283.4,307.49,347.46,405.16,478.09,580.41,731.3,888.53,1110.67,1699.32,256,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'474.926/15-0',3,29,1),
	(165,'COMPLETO P221 - COMPULSÓRIA ‐ COM COPARTICIPAÇÃO','COMPLETO P221 ‐ COMPULSÓRIA ‐ COM COPARTICIPAÇÃO','2017-04-30',540.34,586.27,662.49,772.44,911.48,1106.53,1394.23,1693.99,2117.5,3239.76,258,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.047/15-1',3,29,1),
	(166,'COMPLETO + P221 ‐ COMPULSÓRIA ‐ COM COPARTICIPAÇÃO','COMPLETO + P221 ‐ COMPULSÓRIA ‐ COM COPARTICIPAÇÃO','2017-04-30',738.09,800.83,904.93,1055.15,1245.08,1511.53,1904.52,2313.99,2892.47,4425.48,259,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.086/15-1',3,29,1),
	(167,'FUNDAMENTAL P211 ‐ VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO ','FUNDAMENTAL P211 ‐ VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO ','2017-04-30',278.72,302.42,341.71,398.43,470.14,570.75,719.15,873.78,1092.23,1671.13,260,5,1,1,1,3,7,6,3,10,6,5,NULL,5,4,'474.924/15-3',3,29,1),
	(168,'FUNDAMENTAL P221 ‐ VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO','FUNDAMENTAL P221 ‐ VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO','2017-04-30',324.04,351.59,397.3,463.27,546.66,663.66,836.2,1015.97,1269.97,1943.05,261,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'474.925/15-1',3,29,1),
	(169,'VITAL P111 ‐ VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO','VITAL P111 ‐ VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO','2017-04-30',280.32,304.15,343.68,400.72,472.84,574.03,723.28,878.8,1098.51,1680.73,262,5,1,1,1,3,7,6,3,10,6,5,NULL,5,4,'474.922/15-7',3,29,1),
	(170,'VITAL P121 - VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO ','VITAL P121 - VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO ','2017-04-30',325.9,353.61,399.58,465.93,549.8,667.46,841,1021.82,1277.27,1954.22,263,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'474.926/15-0',3,29,1),
	(171,'COMPLETO P221 ‐ VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO -','COMPLETO P221 - VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO -','2017-04-30',621.39,674.22,761.85,888.31,1048.19,1272.51,1603.38,1948.09,2435.12,3725.73,265,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.047/15-1',3,29,1),
	(172,'COMPLETO + P221 - VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO','COMPLETO + P221 - VOLUNTÁRIA  ‐ COM COPARTICIPAÇÃO','2017-04-30',848.8,920.94,1040.67,1213.43,1431.84,1738.27,2190.19,2661.08,3326.35,5089.31,266,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.086/15-1',3,29,1),
	(173,'FUNDAMENTAL P111 - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','FUNDAMENTAL P111 - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',264.72,287.23,324.57,378.45,446.57,542.13,683.09,829.95,1037.43,1587.29,267,5,1,1,1,3,7,6,4,10,6,5,NULL,5,4,'474.916/15-2',3,29,2),
	(175,'FUNDAMENTAL P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',303.15,328.92,371.69,433.39,511.4,620.82,782.23,950.42,1188.04,1817.7,268,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'474.915/15-4',3,29,2),
	(176,'VITAL P111 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','VITAL P111 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',266.25,288.87,326.44,380.63,449.13,545.25,687.02,834.71,1043.4,1596.4,269,5,1,1,1,3,7,6,4,10,6,5,NULL,5,4,'474.915/15-4',3,29,2),
	(177,'VITAL P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','VITAL P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',304.9,330.81,373.82,435.88,514.34,624.39,786.73,955.89,1194.86,1828.14,269,5,1,1,2,3,7,6,2,10,6,5,NULL,5,4,'',3,29,2),
	(178,'PRONTO P121 - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','PRONTO P121 ‐ COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',339.56,368.43,416.32,485.43,572.8,695.39,876.19,1064.58,1330.73,2036.01,271,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'475.050/15-1',3,29,2),
	(179,'COMPLETO P121 - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO P121 - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',554.63,601.76,679.99,792.88,935.6,1135.82,1431.13,1738.84,2173.55,3325.54,272,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'475.046/15-2',3,29,2),
	(180,'COMPLETO + P121 - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO ','COMPLETO + P121 ‐ SEM COPARTICIPAÇÃO ','2017-04-30',761.46,826.2,933.6,1088.57,1284.52,1559.42,1964.87,2387.32,2984.14,4565.74,273,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'475.088/15-8',3,29,2),
	(183,'PRONTO P221 ‐ COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','PRONTO P221 ‐ COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',321.05,348.33,393.62,458.96,541.59,657.48,828.44,1006.55,1258.19,1925.04,332,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.051/15-9',3,29,1),
	(184,'PRONTO P221 ‐ VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','PRONTO P221 ‐ VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',369.21,400.58,452.67,527.81,622.82,756.1,952.7,1157.54,1446.93,2213.79,350,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,' 475.051/15-9 ',3,29,1),
	(185,'FUNDAMENTAL P111 - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','FUNDAMENTAL P111  - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',304.43,330.31,373.25,435.22,513.56,623.45,785.55,954.44,1193.05,1825.38,274,5,1,1,1,3,7,6,4,10,6,5,NULL,5,4,' 474.916/15-2 ',3,29,2),
	(186,'FUNDAMENTAL P121 - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ','FUNDAMENTAL P121 ‐ VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',348.63,378.26,427.44,498.39,588.11,713.95,899.58,1092.99,1366.25,2090.35,275,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,' 474.915/15-4 ',3,29,2),
	(187,'VITAL P111 - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ','VITAL P111 ‐ VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',306.19,332.2,375.39,437.72,516.5,627.03,790.07,959.92,1199.91,1835.87,276,5,1,1,1,3,7,6,4,NULL,6,5,NULL,5,4,' 474.917/15-1 ',3,29,2),
	(188,'VITAL P121 - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','VITAL P121 - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',350.63,380.43,429.9,501.27,591.48,718.05,904.75,1099.27,1374.09,2102.36,277,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,' 474.923/15-5 ',3,29,2),
	(189,'PRONTO P121 - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','PRONTO P121 - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',390.49,423.69,478.77,558.24,658.72,799.69,1007.62,1224.26,1530.33,2341.41,278,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,' 475.050/15-1 ',3,29,2),
	(190,'COMPLETO P121 ‐ VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','COMPLETO P121 ‐ VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',637.81,692.03,781.99,911.81,1075.93,1306.19,1645.79,1999.66,2499.59,3824.37,279,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,' 475.046/15-2 ',3,29,2),
	(191,'COMPLETO + P121 - VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ','COMPLETO + P121 ‐ VOLUNTÁRIA ‐ SEM COPARTICIPAÇÃO ','2017-04-30',875.69,950.12,1073.64,1251.87,1477.2,1793.32,2259.6,2745.42,3431.76,5250.59,280,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,' 475.088/15-8',3,29,2),
	(192,'VITAL P111 - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','VITAL P111 - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',227.81,247.17,279.31,325.65,384.27,466.51,587.8,714.18,892.73,1365.9,283,5,1,1,1,3,7,6,3,10,6,5,NULL,5,4,' 474.922/15-7 ',3,29,2),
	(193,'VITAL P121 - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','VITAL P121 - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',264.86,287.37,324.73,378.65,446.81,542.44,683.46,830.4,1038,1588.15,284,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,' 474.926/15-0 ',3,29,2),
	(194,'VITAL P111 ‐ VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','VITAL P111 ‐ VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',261.98,284.26,321.19,374.51,441.91,536.48,675.96,821.31,1026.65,1570.78,290,5,1,1,1,3,7,6,3,10,6,5,NULL,5,1,' 474.922/15-7 ',3,29,2),
	(195,'FLEX I - COM COPARTICIPAÇÃO','FLEX I - COM COPARTICIPAÇÃO','2017-04-30',104.37,118.54,132.93,168.97,175.06,230.13,289.64,393.27,463.12,622.2,312,26,1,3,1,3,10,9,NULL,13,14,8,NULL,5,7,'475.444/16-1',2,99,NULL),
	(196,'FLEX II -  COM COPARTICIPAÇÃO ','FLEX II -  COM COPARTICIPAÇÃO ','2017-04-30',138,156.74,175.77,223.42,231.46,304.28,382.97,519.99,612.34,822.69,313,26,1,3,2,3,10,9,NULL,13,14,8,NULL,5,7,'475.442/16-5',2,99,NULL),
	(197,'OURO I - SEM COPARTICIPAÇÃO','OURO I - SEM COPARTICIPAÇÃO','2017-04-30',116,131.75,147.75,187.8,194.56,255.77,321.92,437.1,514.72,691.53,314,26,1,3,1,3,10,9,NULL,13,14,8,NULL,5,7,'475.443/16-3',2,99,NULL),
	(198,'OUTRO II - SEM COPARTICIPAÇÃO','OUTRO II - SEM COPARTICIPAÇÃO','2017-04-30',165,187.41,210.16,267.13,276.75,363.81,457.9,621.73,732.15,983.65,315,26,1,3,2,3,10,9,NULL,13,14,8,NULL,5,7,'475.445/16-0',2,99,NULL),
	(199,'VITAL P121 - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','VITAL P121 - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','2017-04-30',304.58,330.48,373.44,435.44,513.83,623.8,785.98,954.97,1193.71,1826.37,291,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,' 474.926/15-0 ',3,29,2),
	(200,'FLEX I - COM COPARTICIPAÇÃO','FLEX I - COM COPARTICIPAÇÃO','2017-04-30',104.37,118.54,132.93,168.97,175.06,230.13,289.64,393.27,463.12,622.2,312,26,1,3,1,3,10,9,NULL,13,14,8,NULL,5,9,'475.444/16-1',2,99,NULL),
	(201,'FLEX II -  COM COPARTICIPAÇÃO ','FLEX II -  COM COPARTICIPAÇÃO ','2017-04-30',138,156.74,175.77,223.42,231.46,304.28,382.97,519.99,612.34,822.69,313,26,1,3,2,3,10,9,NULL,13,14,8,NULL,5,9,'475.442/16-5',2,99,NULL),
	(202,'OURO I - SEM COPARTICIPAÇÃO','OURO I - SEM COPARTICIPAÇÃO','2017-04-30',150.8,171.28,192.08,244.14,252.93,332.5,418.5,568.23,669.14,898.99,314,26,1,3,1,3,10,9,NULL,13,14,8,NULL,5,9,'475.445/16-0',2,99,NULL),
	(203,'OUTRO II - SEM COPARTICIPAÇÃO','OUTRO II - SEM COPARTICIPAÇÃO','2017-04-30',214.5,243.63,273.21,347.27,359.78,472.95,595.27,808.25,951.8,1278.75,315,26,1,3,2,3,10,9,NULL,13,14,8,NULL,5,9,'475.445/16-0',2,99,NULL),
	(204,'COPARTICIPAÇÃO EM CONSULTAS E EXAMES','COPARTICIPAÇÃO EM CONSULTAS E EXAMES','2017-04-30',96.3,113.53,129.49,149.77,155.09,185.57,236.67,307.67,384.58,576.88,316,25,1,1,1,3,11,10,NULL,15,15,9,NULL,5,10,'',2,29,NULL),
	(205,'COPARTICIPAÇÃO EM CONSULTAS E EXAMES','COPARTICIPAÇÃO EM CONSULTAS E EXAMES','2017-04-30',134.82,158.94,181.29,209.68,217.13,259.8,331.34,430.74,538.41,807.63,316,25,1,1,2,3,11,10,NULL,15,15,9,NULL,5,10,'',2,29,NULL),
	(206,'COPARTICIPAÇÃO EM CONSULTAS','COPARTICIPAÇÃO EM CONSULTAS','2017-04-30',115.2,135.7,154.79,179.03,185.38,222.18,282.9,367.77,459.72,690.11,317,25,1,1,1,3,11,10,NULL,15,15,9,NULL,5,10,'',2,29,NULL),
	(207,'COPARTICIPAÇÃO EM CONSULTAS','COPARTICIPAÇÃO EM CONSULTAS','2017-04-30',161.28,189.98,216.7,250.64,259.53,311.05,396.06,514.87,643.6,966.16,317,25,1,1,2,3,11,10,NULL,15,15,9,NULL,5,10,'',2,29,NULL),
	(208,'UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - BÁSICO - COM COPARTICIPAÇÃO ','UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - BÁSICO - COM COPARTICIPAÇÃO  ','2017-04-30',137.18,159.84,185.31,227.04,268.75,319.44,335.43,451.58,608.11,819.04,318,17,1,1,1,3,12,11,NULL,16,16,10,NULL,5,11,'',3,99,NULL),
	(209,'UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - BÁSICO - SEM COPARTICIPAÇÃO','UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - BÁSICO - SEM COPARTICIPAÇÃO','2017-04-30',185.17,215.8,250.17,306.52,362.83,431.29,452.89,609.71,821.02,1105.8,319,17,1,1,1,3,12,11,NULL,16,16,10,NULL,5,11,'',3,99,NULL),
	(210,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 02 à 08 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',110.2,126.74,145.74,167.6,192.74,223.58,270.55,351.7,474.82,10,323,19,1,3,1,3,NULL,12,NULL,19,18,11,NULL,5,12,'',2,8,NULL),
	(211,'UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - ESPECIAL - COM COPARTICIPAÇÃ','UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - ESPECIAL - COM COPARTICIPAÇÃO','2017-04-30',191.86,223.58,259.29,317.76,376.24,447.26,469.67,632.42,851.73,1147.3,320,17,1,1,2,3,12,11,NULL,17,16,10,NULL,5,11,'',3,99,NULL),
	(213,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 09 à 15 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',104.95,120.7,138.8,159.62,183.56,212.93,257.66,334.96,452.21,10,324,19,1,3,1,3,NULL,12,NULL,19,18,11,NULL,5,12,'',9,15,NULL),
	(214,'UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - ESPECIAL - SEM COPARTICIPAÇÃ','UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - ESPECIAL - SEM COPARTICIPAÇÃO','2017-04-30',258.97,301.81,350.06,428.96,507.89,603.76,634.01,853.73,1149.79,1548.76,321,17,1,1,2,3,12,11,NULL,17,16,10,NULL,5,11,'',3,99,NULL),
	(215,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','2017-04-30',100.43,115.5,132.82,152.75,175.65,203.76,246.57,320.53,432.73,10,325,19,1,1,1,3,NULL,12,NULL,19,18,11,NULL,5,12,'',16,22,NULL),
	(216,'UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - MASTER - SEM COPARTICIPAÇÃO','UNIMED - CENTRAL NACIONAL UNIMED - TABELA PROMOCIONAL - MASTER - SEM COPARTICIPAÇÃO','2017-04-30',387.84,452.21,524.63,643.29,761.89,905.97,951.48,1278.65,1726.56,2326.17,322,17,1,1,2,3,12,11,NULL,18,16,10,NULL,5,11,'',3,99,NULL),
	(217,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 23 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',96.11,110.53,127.1,146.17,168.09,194.99,235.95,306.73,414.1,10,326,19,1,1,1,3,NULL,12,NULL,19,18,11,NULL,5,12,'',23,29,NULL),
	(218,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 02 à 08 VIDAS - COM COPARTICIPAÇÃO  ','2017-04-30',137.76,158.42,182.18,209.49,240.92,279.48,338.19,439.65,593.53,10,323,19,1,3,2,3,NULL,12,NULL,19,18,11,NULL,5,12,'',2,8,NULL),
	(219,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 09 à 15 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',131.2,150.87,173.5,199.51,229.45,266.17,322.08,418.72,565.26,10,324,19,1,3,2,3,NULL,12,NULL,19,18,11,NULL,5,12,'',9,15,NULL),
	(220,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 16 à 22 VIDAS - COM COPARTICIPAÇÃO ','2017-04-30',125.55,144.38,166.03,190.92,219.56,254.71,308.21,400.68,540.92,10,325,19,1,3,2,3,NULL,12,NULL,19,18,11,NULL,5,12,'',16,22,NULL),
	(222,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 23 à 29 VIDAS - COM COPARTICIPAÇÃO ','2017-04-30',120.14,138.16,158.88,182.7,210.11,243.74,294.94,383.43,517.63,10,326,19,1,3,2,3,NULL,12,NULL,19,18,11,NULL,5,12,'',23,29,NULL),
	(223,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 03 à 06 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',137.76,158.42,182.18,209.49,240.92,279.48,338.19,439.65,593.53,10,327,19,1,3,2,3,NULL,13,NULL,19,19,12,NULL,5,13,'',3,6,NULL),
	(224,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','2017-04-30',131.2,150.87,173.5,199.51,229.45,266.17,322.08,418.72,565.26,10,328,19,1,3,2,3,NULL,13,NULL,19,19,12,NULL,5,13,'',7,10,NULL),
	(225,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 11 à 19 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',125.55,144.38,166.03,190.92,219.56,254.71,308.21,400.68,540.92,10,329,19,1,3,2,3,NULL,13,NULL,19,19,12,NULL,5,13,'',11,19,NULL),
	(226,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 20 à 29 VIDAS - COM COPARTICIPAÇÃO ','2017-04-30',120.14,138.16,158.88,182.7,210.11,243.74,294.94,383.43,517.63,10,330,19,1,3,2,3,NULL,13,NULL,19,19,12,NULL,5,13,'',20,29,NULL),
	(227,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','2017-04-30',110.2,126.74,145.74,167.6,192.74,223.58,270.55,351.7,474.82,10,327,19,1,3,1,3,NULL,13,NULL,19,19,12,NULL,5,13,'',3,6,NULL),
	(228,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 07 à 10 VIDAS - COM COPARTICIPAÇÃO  ','2017-04-30',104.95,120.7,138.8,159.62,183.56,212.93,257.66,334.96,452.21,10,328,19,1,3,1,3,NULL,13,NULL,19,19,12,NULL,5,13,'',7,10,NULL),
	(230,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','2017-04-30',100.43,115.2,132.82,152.75,175.65,203.76,246.57,320.53,432.73,10,329,19,1,3,1,3,NULL,13,NULL,19,19,12,NULL,5,13,'',11,19,NULL),
	(231,'MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - COM COPARTICIPAÇÃO','MICRO | MÉDIA | PEQUENAS EMPRESA - CITY PLAN - 20 à 29 VIDAS - COM COPARTICIPAÇÃO ','2017-04-30',96.11,110.53,127.1,146.17,168.09,194.99,235.95,306.73,414.1,10,330,19,1,3,1,3,NULL,13,NULL,19,19,12,NULL,5,13,'',20,29,NULL),
	(232,'INTEGRAL V - COM COPARTICIPAÇÃO','INTEGRAL V - COM COPARTICIPAÇÃO','2017-04-30',122.8,135.08,157.37,192.45,231.73,268.22,316.5,389.29,506.08,736.33,235,23,1,1,1,3,13,14,NULL,NULL,20,13,NULL,5,14,'',2,29,2),
	(233,'FUNDAMENTAL P211 - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','FUNDAMENTAL P211 - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',226.51,245.77,277.7,323.79,382.07,463.84,584.44,710.11,887.63,1358.08,372,5,1,1,1,3,7,6,3,10,6,5,NULL,5,4,'474.924/15-3 ',3,29,2),
	(235,'FUNDAMENTAL P221 - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P221 - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','2017-04-30',263.34,285.73,322.87,376.48,444.26,539.33,679.55,825.66,1032.08,1579.07,373,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.051/15-9',3,29,2),
	(237,'COMPLETO P221 ‐ COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','COMPLETO P221 ‐ COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',504.99,547.92,619.15,721.91,851.85,1034.14,1303.02,1583.17,1978.97,3027.82,377,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.047/15-1',3,29,2),
	(239,'COMPLETO + P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','COMPLETO + P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20%','2017-04-30',689.81,748.44,845.73,986.12,1163.63,1412.65,1779.93,2162.6,2703.24,4135.96,378,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.086/15-1',3,29,2),
	(240,'FUNDAMENTAL P211 - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','FUNDAMENTAL P211 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20%','2017-04-30',260.49,282.63,319.36,372.36,439.38,533.41,672.1,816.62,1020.78,1561.8,374,5,1,1,1,3,7,6,3,10,6,5,NULL,5,4,'474.924/15-3',3,29,2),
	(241,'FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','FUNDAMENTAL P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',302.84,328.59,371.3,432.96,510.9,620.24,781.49,949.51,1186.89,1815.93,375,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'474.925/15-1',3,29,2),
	(242,'PRONTO P221 - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','PRONTO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',345.05,374.38,423.05,493.28,582.08,706.64,890.37,1081.82,1352.27,2068.96,379,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.051/15-9',3,29,2),
	(243,'COMPLETO P221 - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','COMPLETO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',580.73,630.11,712.01,830.2,979.61,1189.26,1498.48,1820.65,2275.81,3481.99,380,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.047/15-1',3,29,2),
	(244,'COMPLETO + P221 - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','COMPLETO + P221 ‐ 03 à 29 VIDAS - 2 TITULARES - VOLUNTÁRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',793.27,860.69,972.59,1134.05,1338.17,1624.55,2046.91,2486.99,3108.74,4756.37,381,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.086/15-1',3,29,2),
	(245,'CASSEB - SEM COPARTICIPAÇÃO','CASSEB - SEM COPARTICIPAÇÃO','2017-04-30',130.42,158.83,180,200.77,219.89,253.24,322.4,400.39,488.65,759.94,295,6,1,3,1,3,8,7,NULL,11,7,6,NULL,5,5,'',2,29,NULL),
	(246,'CASSEB - SEM COPARTICIPAÇÃO','CASSEB - SEM COPARTICIPAÇÃO','2017-04-30',172.77,210.4,238.44,265.97,291.29,335.47,427.09,530.4,647.32,1006.7,295,6,1,3,2,3,8,7,NULL,11,7,6,NULL,5,5,'',2,29,NULL),
	(247,'CASSEB - COM COPARTICIPAÇÃO','CASSEB - COM COPARTICIPAÇÃO','2017-04-30',98.07,119.42,135.34,150.96,165.34,190.41,242.42,301.06,367.42,571.4,296,6,1,3,1,3,8,7,NULL,11,8,6,NULL,5,5,'',2,29,NULL),
	(248,'CASSEB - COM COPARTICIPAÇÃO','CASSEB - COM COPARTICIPAÇÃO','2017-04-30',130.12,158.46,179.57,200.3,219.38,252.65,321.65,399.45,487.51,758.16,296,6,1,3,2,3,8,7,NULL,11,8,6,NULL,5,5,'',2,29,NULL),
	(250,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 04 à 09 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',261.47,320.06,400.75,442.86,471.69,547.15,654.06,784.43,931.34,1568.9,340,18,1,1,1,3,15,15,5,20,21,14,NULL,5,15,'475.571/165',4,9,NULL),
	(251,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 04 à 09 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',292.59,358.13,448.43,495.55,527.8,612.25,731.9,877.8,1042.14,1755.56,341,18,1,1,2,3,15,15,5,20,21,14,NULL,5,15,'475.568/165',4,9,NULL),
	(252,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 04 à 09 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',387.12,473.82,593.31,655.63,698.3,810.03,968.33,1161.35,1378.82,2322.71,342,18,1,1,2,3,15,15,5,20,21,14,NULL,5,15,'475.580/164',4,9,NULL),
	(253,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 04 à 09 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',1032.89,1264.22,1583.03,1749.32,1863.16,2161.28,2583.62,3098.65,3678.86,6197.28,343,18,1,1,2,3,15,15,5,20,21,14,NULL,5,15,'475.583/169',4,9,NULL),
	(255,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - COM COPARTICIPAÇÃO','2017-04-30',237.76,291.04,364.42,402.71,428.93,497.55,594.76,713.31,846.9,1426.66,371,18,1,1,1,3,17,15,5,20,21,14,NULL,5,15,'475.571/165',30,99,NULL),
	(256,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 04 à 09 - SEM COPARTICIPAÇÃO','2017-04-30',277.68,339.87,425.58,470.27,500.88,581.02,694.57,833.02,988.99,1666.01,345,18,1,1,1,3,15,15,5,20,21,14,NULL,5,15,'475.571/165',4,9,NULL),
	(257,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',310.72,380.3,476.19,526.22,560.49,650.16,777.2,932.14,1106.67,1864.25,346,18,1,1,2,3,15,15,5,20,21,14,NULL,5,15,'475.568/165',4,9,NULL),
	(258,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO -  COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - COM COPARTICIPAÇÃO','2017-04-30',266.06,325.66,407.78,450.62,479.94,556.74,665.54,798.21,947.66,1596.4,370,18,1,1,2,3,17,15,5,20,21,14,NULL,5,15,'475.568/165',30,99,NULL),
	(259,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',411.1,503.15,630.03,696.22,741.55,860.19,1028.27,1233.27,1464.18,2466.5,347,18,1,1,2,3,15,15,5,20,21,14,NULL,5,15,'475.580/164',4,9,NULL),
	(260,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',1096.84,1342.49,1681.03,1857.62,1978.52,2295.09,2743.57,3290.48,3906.6,6580.94,348,18,1,1,2,3,15,15,5,20,21,14,NULL,5,15,'475.583/169',4,9,NULL),
	(262,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - COM COPARTICIPAÇÃO','2017-04-30',352.03,430.86,539.52,596.19,634.99,736.59,880.54,1056.06,1253.81,2112.13,369,18,1,1,2,3,17,15,5,20,21,14,NULL,5,15,'475.580/164',30,99,NULL),
	(263,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - 04 à 09 VIDAS - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - SEM COPARTICIPAÇÃO','2017-04-30',369.53,452.3,566.33,625.85,666.57,773.21,924.33,1108.57,1316.15,2217.14,382,18,1,1,2,3,15,15,5,20,21,14,NULL,5,15,'475.576/166',4,9,NULL),
	(264,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - COM COPARTICIPAÇÃO','2017-04-30',939.24,1149.6,1439.51,1590.72,1694.24,1965.33,2349.39,2817.72,3345.33,5635.42,368,18,1,1,2,3,17,15,5,20,21,14,NULL,5,15,'475.583/169',30,99,NULL),
	(266,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - SEM COPARTICIPAÇÃO','2017-04-30',252.5,309.05,387,427.63,455.47,528.34,631.59,757.5,899.33,1514.97,366,18,1,1,1,3,17,15,5,20,21,14,NULL,5,15,'475.571/165',30,99,NULL),
	(267,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - SEM COPARTICIPAÇÃO','2017-04-30',282.55,345.82,433.02,478.51,509.67,591.22,706.74,847.63,1006.34,1695.24,365,18,1,1,2,3,16,15,5,20,21,14,NULL,5,15,'475.568/165',30,99,NULL),
	(268,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR -  SEM COPARTICIPAÇÃO','2017-04-30',373.83,457.53,572.91,633.1,674.32,782.2,935.05,1121.46,1331.43,2242.89,364,18,1,1,2,3,17,15,5,20,21,14,NULL,5,15,'475.580/164',30,99,NULL),
	(269,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - SEM COPARTICIPAÇÃO','2017-04-30',997.39,1220.78,1528.63,1689.2,1799.15,2087.01,2494.83,2992.16,3552.42,5984.3,363,18,1,1,2,3,17,15,5,20,21,14,NULL,5,15,'475.583/169',30,99,NULL),
	(271,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - 10 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',249.62,305.55,382.59,422.78,450.31,522.35,624.41,748.87,889.12,1497.78,352,18,1,1,1,3,16,15,5,20,21,14,NULL,5,15,'475.571/165',10,29,NULL),
	(272,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - 10 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',279.33,341.9,428.11,473.09,503.87,584.49,698.72,838.01,994.9,1675.98,353,18,1,1,2,3,16,15,5,20,21,14,NULL,5,15,'475.568/165',10,29,NULL),
	(273,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 10 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',369.57,452.34,566.42,625.91,666.64,773.31,924.44,1108.7,1316.31,2217.42,354,18,1,1,2,3,16,15,5,20,21,14,NULL,5,15,'475.580/164',10,29,NULL),
	(274,'PEQUENAS I MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - SEM COPARTICIPAÇÃO','PEQUENAS I MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - SEM COPARTICIPAÇÃO ','2017-04-30',336.02,411.3,514.98,569.11,606.14,703.11,840.53,1008.06,1196.82,2016.13,383,18,1,1,2,3,17,15,5,20,21,14,NULL,5,15,'475.576/166',30,99,NULL),
	(275,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - COM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 10 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',986.06,1206.91,1511.27,1670.02,1778.7,2063.3,2466.5,2958.18,3512.09,5916.35,355,18,1,1,2,3,16,15,5,20,21,14,NULL,5,15,'475.583/169',10,29,NULL),
	(277,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPACTO - SEM COPARTICIPAÇÃO','2017-04-30',265.09,324.46,406.29,448.95,478.18,554.68,663.08,795.26,944.16,1590.49,357,18,1,1,1,3,16,15,5,20,21,14,NULL,5,15,'475.571/165',10,29,NULL),
	(278,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - EFETIVO - SEM COPARTICIPAÇÃO','2017-04-30',296.63,363.06,454.61,502.36,535.08,620.69,741.97,889.89,1056.51,1779.75,358,18,1,1,2,3,16,15,5,20,21,14,NULL,5,15,'475.568/165',10,29,NULL),
	(279,'NOSSO PLANO - AMBULATORIAL - SEM COPARTICIPAÇÃO ','NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL - SEM COPARTICIPAÇÃO ','2017-04-30',103.28,115.67,129.55,148.98,171.32,203.87,254.83,318.53,541.5,606.48,334,10,1,1,5,3,9,8,NULL,12,9,7,NULL,5,6,'700.367/99-6',2,29,NULL),
	(280,'NOSSO PLANO - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','2017-04-30',138.61,155.24,173.86,199.93,229.91,273.59,341.98,427.47,726.69,813.89,335,10,1,1,1,3,9,8,NULL,12,9,7,NULL,5,6,'700.370/99-6',2,29,NULL),
	(281,'NOSSO PLANO - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','2017-04-30',207.92,232.87,260.81,299.93,344.91,410.44,513.05,641.31,1090.22,1221.04,335,10,1,1,2,3,9,8,NULL,12,9,7,NULL,5,6,'459.806/09-7',2,29,NULL),
	(282,'NOSSO PLANO - AMBULATORIAL - COM COPARTICIPAÇÃO ','NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL - COM COPARTICIPAÇÃO ','2017-04-30',88.81,99.46,111.39,128.09,147.3,175.28,219.1,273.87,465.57,521.43,336,10,1,1,5,3,9,8,NULL,12,9,7,NULL,5,6,'461.632/10-7',2,29,NULL),
	(283,'NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','NOSSO PLANO - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','2017-04-30',123.36,138.16,154.73,177.93,204.61,243.48,304.35,380.43,646.73,724.33,337,10,1,1,1,3,9,8,NULL,12,9,7,NULL,5,6,'461.569/10-7',2,29,NULL),
	(284,'NOSSO PLANO - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','NOSSO PLANO - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','2017-04-30',185.05,207.25,232.12,266.93,306.96,365.28,456.6,570.75,970.27,1086.7,337,10,1,1,2,3,9,8,NULL,12,9,7,NULL,5,6,'461.566/10-2',2,29,NULL),
	(285,'MIX - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','MIX - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','2017-04-30',155.27,173.9,194.76,223.97,257.56,306.49,383.11,478.88,814.09,911.78,385,10,1,1,1,3,9,8,NULL,12,22,7,NULL,5,6,'461.571/10-9',2,29,NULL),
	(286,'MIX - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','MIX - 02 à 29 VIDAS- AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - COM COPARTICIPAÇÃO','2017-04-30',232.9,260.84,292.14,335.96,386.35,459.75,574.68,718.35,1221.19,1367.73,385,10,1,1,2,3,9,8,NULL,12,22,7,NULL,5,6,'461.564/10-6',2,29,NULL),
	(287,'MIX - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','MIX - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','2017-04-30',174.46,195.39,218.83,251.65,289.39,344.37,430.46,538.07,914.71,1024.47,338,10,1,1,1,3,9,8,NULL,12,22,7,NULL,5,6,'458.980/08-7',2,29,NULL),
	(288,'MIX - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','MIX - 02 à 29 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA - SEM COPARTICIPAÇÃO','2017-04-30',261.7,293.1,328.27,377.51,434.13,516.61,645.76,807.2,1372.24,1536.9,338,10,1,1,2,3,9,8,NULL,12,22,7,NULL,5,6,'459.791/09-5',2,29,NULL),
	(289,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - COMPLETO - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',352.78,431.8,540.65,597.48,636.35,738.16,882.43,1058.32,1256.49,2116.63,384,18,1,1,2,3,16,15,5,20,21,14,NULL,5,15,'475.576/166',10,29,NULL),
	(291,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SUPERIOR - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',392.46,480.34,601.47,664.66,707.94,821.2,981.66,1177.36,1397.81,2354.69,359,18,1,1,2,3,16,15,5,20,21,14,NULL,5,15,'475.580/164',10,29,NULL),
	(292,'PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - SEM COPARTICIPAÇÃO','PEQUENAS | MEDIAS EMPRESAS - CORPORATIVO - SÊNIOR - 10 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',1047.11,1281.63,1604.83,1773.41,1888.83,2191.05,2619.2,3141.32,3729.51,6282.62,360,18,1,1,2,3,16,15,5,20,21,14,NULL,5,15,'475.583/169',10,29,NULL),
	(294,'NOSSO PLANO - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA','NOSSO PLANO - 01 à 99 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA','2017-04-30',145.54,163,182.55,209.93,241.41,287.27,359.08,448.84,763.02,854.58,386,27,1,1,1,3,NULL,16,NULL,21,23,15,NULL,5,16,'9.656/98',1,99,NULL),
	(295,'NOSSO PLANO - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA','NOSSO PLANO - 01 à 99 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA','2017-04-30',218.32,244.51,273.85,314.93,362.16,430.96,538.7,673.38,1144.73,1282.09,386,27,1,1,2,3,NULL,16,NULL,21,23,15,NULL,5,16,'9.656/98',1,99,NULL),
	(296,'MIX - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA ','MIX - 01 à 99 VIDAS - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA ','2017-04-30',183.18,205.16,229.77,264.23,303.86,361.59,451.98,564.97,960.45,1075.69,387,27,1,1,1,3,NULL,16,NULL,21,23,15,NULL,5,16,'9.656/98',1,99,NULL),
	(297,'MIX - AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA ','MIX - 01 à 99 VIDAS- AMBULATORIAL + HOSPITALAR + OBSTETRÍCIA ','2017-04-30',274.79,307.76,344.68,396.39,455.84,542.44,678.05,847.56,1440.85,1613.75,387,27,1,1,2,3,NULL,16,NULL,21,23,15,NULL,5,16,'9.656/98',1,99,NULL),
	(298,'AMBULATORIAL EMPRESARIAL - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','AMBULATORIAL EMPRESARIAL - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',38.18,51.54,56.31,62.04,65.86,73.49,91.63,104.99,123.12,217.61,390,4,1,1,5,3,14,17,NULL,22,25,16,NULL,5,17,'',30,99,NULL),
	(299,'AMBULATORIAL EMPRESARIAL - SEM COPARTICIPAÇÃO','AMBULATORIAL EMPRESARIAL - 03 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',65.04,87.8,95.93,105.69,112.18,125.2,156.09,178.85,209.74,370.72,388,4,1,3,5,3,14,17,NULL,22,25,16,NULL,5,17,'',3,29,NULL),
	(300,'AMBULATORIAL EMPRESARIAL - COM COPARTICIPAÇÃO','AMBULATORIAL EMPRESARIAL - 03 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',48.59,65.59,71.66,78.95,83.81,93.52,116.6,133.6,156.68,276.93,389,4,1,1,5,3,14,17,NULL,22,24,16,NULL,5,17,'',3,29,NULL),
	(301,'FLEX NACIONAL - COMPULSÓRIA - COPARTICIPAÇÃO 30%','FLEX NACIONAL - 03 à 29 VIDAS - COMPULSÓRIA - COPARTICIPAÇÃO 30%','2017-04-30',286.12,349.63,433.54,476.9,505.51,586.39,700.99,815.03,969.88,1716.69,119,1,1,1,2,3,4,5,NULL,9,NULL,4,NULL,5,3,'',3,29,NULL),
	(302,'TOP NACIONAL - COMPULSÓRIA - SEM COPARTICIPAÇÃO	','TOP NACIONAL - 03 à 29 VIDAS - 1 TITULAR - COMPULSÓRIA - SEM COPARTICIPAÇÃO	','2017-04-30',321.77,393.19,487.56,536.32,568.5,659.46,788.34,916.59,1090.73,1930.6,134,1,1,1,1,3,4,5,NULL,9,5,4,NULL,5,3,'',3,29,1),
	(303,'VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','VITAL P121 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ SEM COPARTICIPAÇÃO','2017-04-30',304.9,330.81,373.82,435.88,514.34,624.39,786.73,955.89,1194.86,1828.14,270,5,1,1,2,3,7,6,4,10,6,5,NULL,5,4,'474.923/15-5',3,29,2),
	(304,'PRONTO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','PRONTO P221 ‐ 03 à 29 VIDAS - 2 TITULARES - COMPULSÓRIA ‐ COPARTICIPAÇÃO 20% ','2017-04-30',300.05,325.54,367.87,428.93,506.15,614.46,774.24,940.7,940.7,1799.1,376,5,1,1,2,3,7,6,3,10,6,5,NULL,5,4,'475.051/15-9',3,29,2),
	(305,'Lincx LT3 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','Lincx LT3 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',486.84,608.55,760.69,836.76,878.6,966.46,1208.08,1328.89,1661.11,2906.94,393,13,1,1,2,3,18,18,6,26,26,17,NULL,5,18,'467730127',2,29,NULL),
	(306,'Lincx LT4 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','Lincx LT4 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',851.28,1064.1,1330.13,1463.14,1536.3,1689.93,2112.41,2323.65,2904.56,5082.98,394,13,1,1,2,3,18,18,7,26,26,17,NULL,5,18,'467728125',2,29,NULL),
	(307,'BLACK T2 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','BLACK T2 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',959.12,1198.9,1498.63,1648.49,1730.91,1904,2380,2618,3272.5,5726.88,395,13,1,1,2,3,18,18,8,26,26,17,NULL,5,18,'462851109',2,29,NULL),
	(308,'BLACK T3 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','BLACK T3 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',1141.8,1427.25,1784.06,1962.47,2060.59,2266.65,2833.31,3116.64,3895.8,6817.65,396,13,1,1,2,3,18,18,9,26,26,17,NULL,5,18,'462851109',2,29,NULL),
	(309,'BLACK T4 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','BLACK T4 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',1378.8,1723.03,2153.79,2369.17,2487.63,2736.39,3420.49,3762.54,4703.18,8230.57,397,13,1,1,2,3,18,18,10,26,26,17,NULL,5,18,'462851109',2,29,NULL),
	(310,'BLACK T5 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','BLACK T5 - 02 à 29 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',1760.61,2200.76,2750.95,3026.05,3177.35,3495.09,4368.86,4805.75,6007.19,10512.6,398,13,1,1,2,3,18,18,11,26,26,17,NULL,5,18,'462851109',2,29,NULL),
	(311,'Lincx LT3 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','Lincx LT3 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',389.45,486.81,608.51,669.36,702.83,773.11,966.39,1063.03,1328.79,2325.38,399,13,1,1,2,3,18,18,6,26,26,17,NULL,5,18,'473484150',2,29,NULL),
	(312,'Lincx LT4 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','Lincx LT4 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',681.05,851.31,1064.14,1170.55,1229.08,1351.99,1689.99,1858.99,2323.74,4066.55,400,13,1,1,2,3,18,18,7,26,26,17,NULL,5,18,'473490154',2,29,NULL),
	(313,'BLACK T2 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','BLACK T2 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',767.3,959.13,1198.91,1318.8,1384.74,1523.21,1904.01,2094.41,2618.01,4581.52,401,13,1,1,2,3,18,18,8,26,26,17,NULL,5,18,'462852107',2,29,NULL),
	(314,'BLACK T3 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','BLACK T3 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',913.45,1141.81,1427.26,1569.99,1648.49,1813.34,2266.68,2493.35,3116.69,5454.21,402,13,1,1,2,3,18,18,9,26,26,17,NULL,5,18,'462852107',2,29,NULL),
	(315,'BLACK T4 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','BLACK T4 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',1102.73,1378.41,1723.01,1895.31,1990.08,2189.09,2736.36,3010,3762.5,6584.38,404,13,1,1,2,3,18,18,10,26,26,17,NULL,5,18,'462852107',2,29,1),
	(317,'BLACK T5 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','BLACK T5 - 02 à 29 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',1408.49,1760.61,2200.76,2420.84,2541.88,2796.07,3495.09,3844.6,4805.75,8410.06,403,13,1,1,2,3,18,18,11,26,26,17,NULL,5,18,'462852107',2,29,NULL),
	(318,'LINCX LT3 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','LINCX LT3 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',462.51,578.14,722.68,794.95,834.7,918.17,1147.71,1262.48,1578.1,2761.68,405,13,1,1,2,3,18,18,6,26,26,17,NULL,5,18,'467730127',30,99,NULL),
	(319,'LINCX LT4 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','LINCX LT4 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',808.72,1010.9,1263.63,1389.99,1459.49,1605.44,2006.8,2207.48,2759.35,4828.86,406,13,1,1,2,3,18,18,7,26,26,17,NULL,5,18,'467728125',30,99,NULL),
	(320,'BLACK T2 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','BLACK T2 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',911.16,1138.95,1423.69,1566.06,1644.36,1808.8,2261,2487.1,3108.88,5440.54,407,13,1,1,2,3,18,18,8,26,26,17,NULL,5,18,'462851109',30,99,NULL),
	(321,'BLACK T3 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','BLACK T3 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',1084.71,1355.89,1694.86,1864.35,1957.57,2153.33,2691.66,2960.83,3701.04,6476.82,408,13,1,1,2,3,18,18,9,26,26,17,NULL,5,18,'462851109',30,99,NULL),
	(322,'BLACK T4 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','BLACK T4 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',1309.5,1636.88,2046.1,2250.71,2363.25,2599.58,3249.48,3574.43,4468.04,7819.07,409,13,1,1,2,3,18,18,10,26,26,17,NULL,5,18,'462851109',30,99,NULL),
	(323,'BLACK T5 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','BLACK T5 - 30 à 99 VIDAS - SEM COPARTICIPAÇÃO','2017-04-30',1672.59,2090.74,2613.43,2874.77,3018.51,3320.36,4150.45,4565.5,5706.88,9987.04,410,13,1,1,2,3,18,18,11,26,26,17,NULL,5,18,'462851109',30,99,NULL),
	(324,'LINCX LT3 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','LINCX LT3 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',369.97,462.46,578.08,635.89,667.68,734.45,918.06,1009.87,1262.34,2209.1,411,13,1,1,2,3,18,18,6,26,26,17,NULL,5,18,'473484150',30,99,NULL),
	(325,'LINCX LT4 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','LINCX LT4 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',646.99,808.74,1010.93,1112.02,1167.62,1284.38,1605.48,1766.03,2207.54,3863.2,412,13,1,1,2,3,18,18,7,26,26,17,NULL,5,18,'473490154',30,99,NULL),
	(326,'BLACK T2 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','BLACK T2 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',728.94,911.18,1138.98,1252.88,1315.52,1447.07,1808.84,1989.72,2487.15,4352.51,413,13,1,1,2,3,18,18,8,26,26,17,NULL,5,18,'462852107',30,99,NULL),
	(327,'BLACK T3 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','BLACK T3 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',867.78,1084.73,1355.91,1491.5,1566.08,1722.69,2153.36,2368.7,2960.88,5181.54,414,13,1,1,2,3,18,18,9,26,26,17,NULL,5,18,'462852107',30,99,NULL),
	(328,'BLACK T4 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','BLACK T4 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',1047.59,1309.49,1636.86,1800.55,1890.58,2079.64,2599.55,2859.51,3574.39,6255.18,415,13,1,1,2,3,18,18,10,26,26,17,NULL,5,18,'462852107',30,99,NULL),
	(329,'BLACK T5 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','BLACK T5 - 30 à 99 VIDAS - COM COPARTICIPAÇÃO','2017-04-30',1338.07,1672.59,2090.74,2299.81,2414.8,2656.28,3320.35,3652.39,4565.49,7989.61,416,13,1,1,2,3,18,18,11,26,26,17,NULL,5,18,'462852107',30,99,NULL);

/*!40000 ALTER TABLE `tabelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tabelas_regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_regioes`;

CREATE TABLE `tabelas_regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tabela_id` int(11) unsigned NOT NULL,
  `regiao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_id` (`tabela_id`),
  KEY `regiao_id` (`regiao_id`),
  CONSTRAINT `tabelas_regioes_ibfk_1` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`),
  CONSTRAINT `tabelas_regioes_ibfk_2` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tabelas_regioes` WRITE;
/*!40000 ALTER TABLE `tabelas_regioes` DISABLE KEYS */;

INSERT INTO `tabelas_regioes` (`id`, `tabela_id`, `regiao_id`)
VALUES
	(3,5,2),
	(4,6,2),
	(6,8,2),
	(8,10,2),
	(9,11,2),
	(10,12,2),
	(11,13,2),
	(12,14,2),
	(13,15,2),
	(14,16,2),
	(15,17,2),
	(16,18,2),
	(17,19,2),
	(18,20,2),
	(19,21,2),
	(20,22,2),
	(21,23,3),
	(22,24,3),
	(23,25,3),
	(24,26,3),
	(25,27,3),
	(26,28,3),
	(27,29,3),
	(28,30,3),
	(29,31,3),
	(30,32,3),
	(31,33,3),
	(32,34,3),
	(33,35,3),
	(35,37,3),
	(36,38,3),
	(37,39,3),
	(38,40,3),
	(39,41,3),
	(40,42,3),
	(41,43,3),
	(42,44,3),
	(44,46,3),
	(45,47,3),
	(46,48,3),
	(47,49,3),
	(48,50,3),
	(49,51,3),
	(50,52,3),
	(51,53,3),
	(52,54,3),
	(53,56,3),
	(54,57,3),
	(55,58,3),
	(56,59,3),
	(57,60,3),
	(58,62,3),
	(59,63,3),
	(60,64,3),
	(61,65,3),
	(62,66,3),
	(63,67,3),
	(64,68,3),
	(65,69,3),
	(66,70,3),
	(67,71,3),
	(68,72,3),
	(69,73,3),
	(70,74,3),
	(71,75,3),
	(72,76,3),
	(73,77,3),
	(74,78,3),
	(75,79,3),
	(76,80,3),
	(77,81,3),
	(78,82,3),
	(79,83,3),
	(80,84,3),
	(81,88,3),
	(82,89,3),
	(83,90,3),
	(84,91,3),
	(85,92,3),
	(86,93,3),
	(87,94,3),
	(88,95,3),
	(89,96,3),
	(90,97,3),
	(91,98,3),
	(92,99,3),
	(93,100,3),
	(94,101,3),
	(96,103,3),
	(97,104,3),
	(98,105,3),
	(99,106,3),
	(101,108,3),
	(102,109,3),
	(103,110,3),
	(104,111,3),
	(105,112,3),
	(106,113,3),
	(107,114,3),
	(108,115,3),
	(109,116,3),
	(110,117,3),
	(111,118,3),
	(112,119,3),
	(113,120,3),
	(114,121,3),
	(115,122,3),
	(116,123,3),
	(117,124,3),
	(118,125,3),
	(119,126,3),
	(120,127,3),
	(121,128,3),
	(122,129,3),
	(123,130,3),
	(124,131,3),
	(126,133,3),
	(127,134,3),
	(128,135,3),
	(129,136,3),
	(130,137,3),
	(131,138,4),
	(132,139,4),
	(133,140,4),
	(134,142,4),
	(135,143,4),
	(136,144,4),
	(137,145,4),
	(138,146,4),
	(139,147,4),
	(142,156,4),
	(143,157,4),
	(144,158,4),
	(145,159,4),
	(146,160,4),
	(147,161,4),
	(148,162,4),
	(149,163,4),
	(150,164,4),
	(151,165,4),
	(152,166,4),
	(153,167,4),
	(154,168,4),
	(155,169,4),
	(156,170,4),
	(157,171,4),
	(158,172,4),
	(159,173,4),
	(161,175,4),
	(162,176,4),
	(163,177,4),
	(164,178,4),
	(165,179,4),
	(166,180,4),
	(169,183,4),
	(170,184,4),
	(171,185,4),
	(172,186,4),
	(173,187,4),
	(174,188,4),
	(175,189,4),
	(176,190,4),
	(177,191,4),
	(178,192,4),
	(179,193,4),
	(180,194,4),
	(181,195,7),
	(182,196,7),
	(183,197,7),
	(184,198,7),
	(185,199,4),
	(186,200,9),
	(187,201,9),
	(188,202,9),
	(189,203,9),
	(190,204,10),
	(191,205,10),
	(192,206,10),
	(193,207,10),
	(194,208,11),
	(195,209,11),
	(196,210,12),
	(197,211,11),
	(199,213,12),
	(200,214,11),
	(201,215,12),
	(202,216,11),
	(203,217,12),
	(204,218,12),
	(205,219,12),
	(206,220,12),
	(208,222,12),
	(209,223,13),
	(210,224,13),
	(211,225,13),
	(212,226,13),
	(213,227,13),
	(214,228,13),
	(216,230,13),
	(217,231,13),
	(218,232,14),
	(219,233,4),
	(221,235,4),
	(223,237,4),
	(225,239,4),
	(226,240,4),
	(227,241,4),
	(228,242,4),
	(229,243,4),
	(230,244,4),
	(231,245,5),
	(232,246,5),
	(233,247,5),
	(234,248,5),
	(235,250,15),
	(236,251,15),
	(237,252,15),
	(238,253,15),
	(240,255,15),
	(241,256,15),
	(242,257,15),
	(243,258,15),
	(244,259,15),
	(245,260,15),
	(247,262,15),
	(248,263,15),
	(249,264,15),
	(251,266,15),
	(252,267,15),
	(253,268,15),
	(254,269,15),
	(256,271,15),
	(257,272,15),
	(258,273,15),
	(259,274,15),
	(260,275,15),
	(262,277,15),
	(263,278,15),
	(264,279,6),
	(265,280,6),
	(266,281,6),
	(267,282,6),
	(268,283,6),
	(269,284,6),
	(270,285,6),
	(271,286,6),
	(272,287,6),
	(273,288,6),
	(274,289,15),
	(276,291,15),
	(277,292,15),
	(279,294,16),
	(280,295,16),
	(281,296,16),
	(282,297,16),
	(283,298,17),
	(284,299,17),
	(285,300,17),
	(286,301,3),
	(287,302,3),
	(288,303,4),
	(289,304,4),
	(290,305,18),
	(291,306,18),
	(292,307,18),
	(293,308,18),
	(294,309,18),
	(295,310,18),
	(296,311,18),
	(297,312,18),
	(298,313,18),
	(299,314,18),
	(300,315,18),
	(301,317,18),
	(302,318,18),
	(303,319,18),
	(304,320,18),
	(305,321,18),
	(306,322,18),
	(307,323,18),
	(308,324,18),
	(309,325,18),
	(310,326,18),
	(311,327,18),
	(312,328,18),
	(313,329,18);

/*!40000 ALTER TABLE `tabelas_regioes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tipos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipos`;

CREATE TABLE `tipos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `flag` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tipos` WRITE;
/*!40000 ALTER TABLE `tipos` DISABLE KEYS */;

INSERT INTO `tipos` (`id`, `descricao`, `nome`, `flag`)
VALUES
	(1,'','ENFERMARIA','0'),
	(2,'','APARTAMENTO','0'),
	(3,'','AMBULATORIAL','0'),
	(4,'','HOSPITALAR','0'),
	(5,'','SEM ACOMODAÇÃO','0'),
	(6,'Não Informado.','Não Informado.','0');

/*!40000 ALTER TABLE `tipos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `role`, `nome`, `created`, `modified`)
VALUES
	(1,'021.008.705-64','$2y$10$UJdX8pm7SRIHLF7tv7KtZu.EqOVQOX0vQK2Zv8mFSwOpiEHP5ZMOq','admin','Rodrigo Souza','2017-01-19 17:32:32','2017-01-26 20:06:26'),
	(2,'857.985.465-27','$2y$10$9QcwyMAZ2tWQsQH3RJJc9.KICe5yQAXrsweeddizuxcWbvdeLXU/i','admin','Mariana Pedral','2017-02-02 15:43:08','2017-02-02 15:43:21'),
	(3,'063.183.795-76','$2y$10$O1/JagFO4TSOMin5QllQg.nqkuEkMNkJxhXdSW2Km7b58IlLsOsPK','padrao','Taís Silva','2017-03-13 10:07:12','2017-03-13 10:12:49');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
