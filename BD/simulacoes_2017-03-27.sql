# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.17)
# Base de Dados: simulacoes
# Tempo de Geração: 2017-03-27 18:42:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela abrangencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `abrangencias`;

CREATE TABLE `abrangencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `abrangencias` WRITE;
/*!40000 ALTER TABLE `abrangencias` DISABLE KEYS */;

INSERT INTO `abrangencias` (`id`, `descricao`, `nome`)
VALUES
	(1,'desc teste','Nacional'),
	(2,'','Estadual'),
	(3,'','Municipal');

/*!40000 ALTER TABLE `abrangencias` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela carencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carencias`;

CREATE TABLE `carencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `carencias_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `carencias` WRITE;
/*!40000 ALTER TABLE `carencias` DISABLE KEYS */;

INSERT INTO `carencias` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'Carência de consultas e carência de exames','TESTE',2),
	(2,'Carencia AMIL I','AMIL I',1),
	(3,'Carência de consultas e carência de exames','Padrão II',2),
	(4,'Carencia AMIL I','AMIL II',1),
	(5,'Carência de consultas e carência de exames','Padrão III',2),
	(6,'Carencia AMIL I','AMIL III',1);

/*!40000 ALTER TABLE `carencias` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `estados`;

CREATE TABLE `estados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;

INSERT INTO `estados` (`id`, `nome`)
VALUES
	(2,'Acre'),
	(3,'Alagoas'),
	(4,'Amapá'),
	(5,'Amazonas'),
	(6,'Bahia'),
	(7,'Ceará'),
	(8,'Distrito Federal'),
	(9,'Espírito Santo'),
	(10,'Goiás'),
	(11,'Maranhão'),
	(12,'Mato Grosso'),
	(13,'Mato Grosso do Sul'),
	(14,'Minas Gerais'),
	(15,'Pará'),
	(16,'Paraíba'),
	(17,'Paraná'),
	(18,'Pernambuco'),
	(19,'Piauí'),
	(20,'Rio de Janeiro'),
	(21,'Rio Grande do Norte'),
	(22,'Rio Grande do Sul'),
	(23,'Rondônia'),
	(24,'Roraima'),
	(25,'Santa Catarina'),
	(26,'São Paulo'),
	(27,'Sergipe'),
	(28,'Tocantins');

/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela imagens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `imagens`;

CREATE TABLE `imagens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caminho` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `imagens` WRITE;
/*!40000 ALTER TABLE `imagens` DISABLE KEYS */;

INSERT INTO `imagens` (`id`, `nome`, `caminho`, `created`, `modified`, `status`)
VALUES
	(3,'amil.png','uploads/imagens/','2017-02-10 17:03:50','2017-02-10 17:03:50',1),
	(4,'amil.png','uploads/imagens/','2017-02-10 17:05:13','2017-02-10 17:05:13',1),
	(5,'amil.png','uploads/imagens/','2017-02-10 17:10:51','2017-02-10 17:10:51',1),
	(6,'amil.png','uploads/imagens/','2017-02-10 17:11:27','2017-02-10 17:11:27',1),
	(7,'amil.png','uploads/imagens/','2017-02-10 17:12:23','2017-02-10 17:12:23',1),
	(8,'amil.png','uploads/imagens/','2017-02-10 17:13:57','2017-02-10 17:13:57',1),
	(9,'Bradesco-saude-Logo.png','uploads/imagens/','2017-02-13 14:39:44','2017-02-13 14:39:44',1);

/*!40000 ALTER TABLE `imagens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela informacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `informacoes`;

CREATE TABLE `informacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(155) NOT NULL DEFAULT '',
  `operadora_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `informacoes_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `informacoes` WRITE;
/*!40000 ALTER TABLE `informacoes` DISABLE KEYS */;

INSERT INTO `informacoes` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'1. Os valores acima são individuais para cada faixa etária, os totais incluem a soma de vidas por cada padrão.\r\n2. As carências apresentadas são as contratuais.\r\n3. No caso de possuir plano anterior, as operadoras estudam a redução das suas carências.','INFO I',1);

/*!40000 ALTER TABLE `informacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela modalidades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modalidades`;

CREATE TABLE `modalidades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `modalidades` WRITE;
/*!40000 ALTER TABLE `modalidades` DISABLE KEYS */;

INSERT INTO `modalidades` (`id`, `nome`, `descricao`)
VALUES
	(1,'Adesão','1'),
	(2,'Individual','2'),
	(3,'PME','3');

/*!40000 ALTER TABLE `modalidades` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela observacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `observacoes`;

CREATE TABLE `observacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `observacoes_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `observacoes` WRITE;
/*!40000 ALTER TABLE `observacoes` DISABLE KEYS */;

INSERT INTO `observacoes` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'Observações adicionais de Tabela','OBS I',1);

/*!40000 ALTER TABLE `observacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela opcionais
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opcionais`;

CREATE TABLE `opcionais` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `opcionais_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `opcionais` WRITE;
/*!40000 ALTER TABLE `opcionais` DISABLE KEYS */;

INSERT INTO `opcionais` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'Opcionais de Tabela','OPCIONAL I',1);

/*!40000 ALTER TABLE `opcionais` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operadoras`;

CREATE TABLE `operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `imagem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `imagem_id` (`imagem_id`),
  CONSTRAINT `operadoras_ibfk_1` FOREIGN KEY (`imagem_id`) REFERENCES `imagens` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `operadoras` WRITE;
/*!40000 ALTER TABLE `operadoras` DISABLE KEYS */;

INSERT INTO `operadoras` (`id`, `nome`, `imagem_id`)
VALUES
	(1,'AMIL',8),
	(2,'BRADESCO',9),
	(3,'TESTE§',NULL);

/*!40000 ALTER TABLE `operadoras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produtos`;

CREATE TABLE `produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `produtos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;

INSERT INTO `produtos` (`id`, `nome`, `descricao`, `operadora_id`)
VALUES
	(1,'TOP','Plano Odontológico',1),
	(2,'VIP','Plano VIP	',1),
	(5,'Básico','Plano Básico',2);

/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela ramos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ramos`;

CREATE TABLE `ramos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ramos` WRITE;
/*!40000 ALTER TABLE `ramos` DISABLE KEYS */;

INSERT INTO `ramos` (`id`, `descricao`, `nome`)
VALUES
	(1,'desc teste','SAÚDE'),
	(2,'Desc Odontológico','ODONTOLÓGICO');

/*!40000 ALTER TABLE `ramos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela redes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redes`;

CREATE TABLE `redes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `redes_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `redes` WRITE;
/*!40000 ALTER TABLE `redes` DISABLE KEYS */;

INSERT INTO `redes` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'Principal diferencial é a utilização da rede preferencial Hapvida. Consultas e tratamentos com hora marcada. Internação em enfermaria ou apartamento. Exames nas clínicas Vida & Imagem e nos postos de coleta do Laboratório Antonio Prudente. Os planos HAPVIDA tem abrangência em sua rede preferencial nos seguintes estados: AL/ AM/ BA/ CE/ MA/ PA/ PB/ PE/ PI/ RN/ SE. Mix Este plano utiliza a rede preferencial Hapvida e mais uma ampla rede de médicos credenciados. Consultas e tratamentos com hora marcada, em clínicas preferenciais e particulares credenciadas para este plano. Possui internação em hospitais da rede preferencial em enfermaria ou apartamento, conforme plano escolhido. Exames nas clínicas Vida & Imagem e nos postos de coleta do Laboratório Antonio Prudente. Os planos HAPVIDA tem abrangência em sua rede preferencial nos seguintes estados: AL/ AM/ BA/ CE/ MA/ PA/ PB/ PE/ PI/ RN/ SE.','REDE I',1);

/*!40000 ALTER TABLE `redes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela reembolsos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reembolsos`;

CREATE TABLE `reembolsos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `operadora_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `reembolsos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `reembolsos` WRITE;
/*!40000 ALTER TABLE `reembolsos` DISABLE KEYS */;

INSERT INTO `reembolsos` (`id`, `descricao`, `nome`, `operadora_id`)
VALUES
	(1,'Características para REEMBOLSO','REEMBOLSO PADRÃO',1),
	(2,'asfasfsafasfassaf','REEMBOLSO I',NULL);

/*!40000 ALTER TABLE `reembolsos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regioes`;

CREATE TABLE `regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  `operadora_id` int(10) unsigned NOT NULL,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `regioes_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `regioes_ibfk_2` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `regioes` WRITE;
/*!40000 ALTER TABLE `regioes` DISABLE KEYS */;

INSERT INTO `regioes` (`id`, `nome`, `estado_id`, `operadora_id`, `descricao`)
VALUES
	(1,'RMS',6,2,''),
	(2,'Reconcavo',6,1,''),
	(3,'Mata',2,1,'');

/*!40000 ALTER TABLE `regioes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela simulacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes`;

CREATE TABLE `simulacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `contato` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `faixa1` int(11) DEFAULT NULL,
  `faixa2` int(11) DEFAULT NULL,
  `faixa3` int(11) DEFAULT NULL,
  `faixa4` int(11) DEFAULT NULL,
  `faixa5` int(11) DEFAULT NULL,
  `faixa6` int(11) DEFAULT NULL,
  `faixa7` int(11) DEFAULT NULL,
  `faixa8` int(11) DEFAULT NULL,
  `faixa9` int(11) DEFAULT NULL,
  `faixa10` int(11) DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `telefone` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `simulacoes` WRITE;
/*!40000 ALTER TABLE `simulacoes` DISABLE KEYS */;

INSERT INTO `simulacoes` (`id`, `nome`, `contato`, `email`, `faixa1`, `faixa2`, `faixa3`, `faixa4`, `faixa5`, `faixa6`, `faixa7`, `faixa8`, `faixa9`, `faixa10`, `data`, `telefone`)
VALUES
	(17,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL),
	(18,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL),
	(19,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL),
	(20,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL),
	(21,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-06 02:27:45',NULL),
	(22,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-06 02:28:48',NULL),
	(23,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-06 02:29:40',NULL),
	(24,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-06 02:31:20',NULL),
	(25,'TESTE 99',' TESTE ((','rodrigo@gowebdesign.com.br',1,2,1,1,1,1,1,1,1,1,'2017-02-06 16:36:19',NULL),
	(26,'lero','asdasdsd','sara.plech@hotmail.com',1,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-02-08 16:25:06',0),
	(27,'lero','asdasdsd','sara.plech@hotmail.com',1,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-02-08 16:26:42',0),
	(28,'q','q','q',NULL,NULL,NULL,NULL,NULL,1,1,NULL,NULL,NULL,'2017-03-13 16:55:43',NULL),
	(29,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-22 16:56:12',NULL),
	(30,'asdas','dasdasd','asdasd',1,1,1,1,1,NULL,NULL,NULL,NULL,NULL,'2017-03-22 16:56:49',NULL),
	(31,'asdasd','adasdsd','aasdas',1,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-23 14:21:57',NULL);

/*!40000 ALTER TABLE `simulacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela simulacoes_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes_tabelas`;

CREATE TABLE `simulacoes_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `simulacao_id` int(11) unsigned NOT NULL,
  `tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `simulacao_id` (`simulacao_id`),
  KEY `tabela_id` (`tabela_id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_1` FOREIGN KEY (`simulacao_id`) REFERENCES `simulacoes` (`id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_2` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `simulacoes_tabelas` WRITE;
/*!40000 ALTER TABLE `simulacoes_tabelas` DISABLE KEYS */;

INSERT INTO `simulacoes_tabelas` (`id`, `simulacao_id`, `tabela_id`)
VALUES
	(1,29,13),
	(2,29,14),
	(3,30,13),
	(4,30,14),
	(5,31,13),
	(6,31,14);

/*!40000 ALTER TABLE `simulacoes_tabelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela subprodutos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subprodutos`;

CREATE TABLE `subprodutos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `tipo_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_id` (`tipo_id`),
  CONSTRAINT `subprodutos_ibfk_1` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `subprodutos` WRITE;
/*!40000 ALTER TABLE `subprodutos` DISABLE KEYS */;

INSERT INTO `subprodutos` (`id`, `descricao`, `nome`, `tipo_id`)
VALUES
	(1,'SEM SUBPRODUTO','SEM SUBPRODUTO',1);

/*!40000 ALTER TABLE `subprodutos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas`;

CREATE TABLE `tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `vigencia` date NOT NULL,
  `faixa1` float DEFAULT NULL,
  `faixa2` float DEFAULT NULL,
  `faixa3` float DEFAULT NULL,
  `faixa4` float DEFAULT NULL,
  `faixa5` float DEFAULT NULL,
  `faixa6` float DEFAULT NULL,
  `faixa7` float DEFAULT NULL,
  `faixa8` float DEFAULT NULL,
  `faixa9` float DEFAULT NULL,
  `faixa10` float DEFAULT NULL,
  `produto_id` int(11) unsigned NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  `ramo_id` int(11) unsigned NOT NULL,
  `abrangencia_id` int(11) unsigned NOT NULL,
  `tipo_id` int(11) unsigned NOT NULL,
  `modalidade_id` int(11) unsigned NOT NULL,
  `carencia_id` int(11) unsigned DEFAULT NULL,
  `opcional_id` int(11) unsigned DEFAULT NULL,
  `reembolso_id` int(11) unsigned DEFAULT NULL,
  `rede_id` int(11) unsigned DEFAULT NULL,
  `observacao_id` int(11) unsigned DEFAULT NULL,
  `informacao_id` int(11) unsigned DEFAULT NULL,
  `validade` char(1) DEFAULT NULL,
  `estado_id` int(11) unsigned DEFAULT NULL,
  `regiao_id` int(11) unsigned DEFAULT NULL,
  `cod_ans` varchar(50) DEFAULT NULL,
  `minimo_vidas` int(11) DEFAULT NULL,
  `maximo_vidas` int(11) DEFAULT NULL,
  `titulares` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_id` (`produto_id`),
  KEY `operadora_id` (`operadora_id`),
  KEY `ramo_id` (`ramo_id`),
  KEY `abrangencia_id` (`abrangencia_id`),
  KEY `tipo_id` (`tipo_id`),
  KEY `modalidade_id` (`modalidade_id`),
  KEY `carencia_id` (`carencia_id`),
  KEY `opcional_id` (`opcional_id`),
  KEY `reembolso_id` (`reembolso_id`),
  KEY `rede_id` (`rede_id`),
  KEY `observacao_id` (`observacao_id`),
  KEY `informacao_id` (`informacao_id`),
  KEY `estado_id` (`estado_id`),
  KEY `regiao_id` (`regiao_id`),
  CONSTRAINT `tabelas_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`),
  CONSTRAINT `tabelas_ibfk_10` FOREIGN KEY (`reembolso_id`) REFERENCES `reembolsos` (`id`),
  CONSTRAINT `tabelas_ibfk_11` FOREIGN KEY (`rede_id`) REFERENCES `redes` (`id`),
  CONSTRAINT `tabelas_ibfk_12` FOREIGN KEY (`observacao_id`) REFERENCES `observacoes` (`id`),
  CONSTRAINT `tabelas_ibfk_13` FOREIGN KEY (`informacao_id`) REFERENCES `informacoes` (`id`),
  CONSTRAINT `tabelas_ibfk_14` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  CONSTRAINT `tabelas_ibfk_16` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`),
  CONSTRAINT `tabelas_ibfk_2` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`),
  CONSTRAINT `tabelas_ibfk_3` FOREIGN KEY (`ramo_id`) REFERENCES `ramos` (`id`),
  CONSTRAINT `tabelas_ibfk_4` FOREIGN KEY (`abrangencia_id`) REFERENCES `abrangencias` (`id`),
  CONSTRAINT `tabelas_ibfk_5` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`),
  CONSTRAINT `tabelas_ibfk_6` FOREIGN KEY (`modalidade_id`) REFERENCES `modalidades` (`id`),
  CONSTRAINT `tabelas_ibfk_8` FOREIGN KEY (`carencia_id`) REFERENCES `carencias` (`id`),
  CONSTRAINT `tabelas_ibfk_9` FOREIGN KEY (`opcional_id`) REFERENCES `opcionais` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tabelas` WRITE;
/*!40000 ALTER TABLE `tabelas` DISABLE KEYS */;

INSERT INTO `tabelas` (`id`, `nome`, `descricao`, `vigencia`, `faixa1`, `faixa2`, `faixa3`, `faixa4`, `faixa5`, `faixa6`, `faixa7`, `faixa8`, `faixa9`, `faixa10`, `produto_id`, `operadora_id`, `ramo_id`, `abrangencia_id`, `tipo_id`, `modalidade_id`, `carencia_id`, `opcional_id`, `reembolso_id`, `rede_id`, `observacao_id`, `informacao_id`, `validade`, `estado_id`, `regiao_id`, `cod_ans`, `minimo_vidas`, `maximo_vidas`, `titulares`)
VALUES
	(13,'asdasdasdasd','qwfqwfwqf','2017-04-17',12.1,12.1,12.1,12.1,12.1,12.1,12.1,12.1,12.1,12.1,1,1,1,1,2,3,2,1,1,1,1,1,'1',6,2,'qweqwerqwrqw',3,99,1),
	(14,'ttttttt','tttttt','2017-04-17',24.2,24.2,24.2,24.2,24.2,24.2,24.2,24.2,24.2,24.2,2,1,1,1,1,3,6,1,1,1,1,1,'1',6,2,'tttttt',2,29,1);

/*!40000 ALTER TABLE `tabelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tabelas_regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_regioes`;

CREATE TABLE `tabelas_regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tabela_id` int(11) unsigned NOT NULL,
  `regiao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_id` (`tabela_id`),
  KEY `regiao_id` (`regiao_id`),
  CONSTRAINT `tabelas_regioes_ibfk_1` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`),
  CONSTRAINT `tabelas_regioes_ibfk_2` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tabelas_regioes` WRITE;
/*!40000 ALTER TABLE `tabelas_regioes` DISABLE KEYS */;

INSERT INTO `tabelas_regioes` (`id`, `tabela_id`, `regiao_id`)
VALUES
	(4,13,2),
	(5,14,2);

/*!40000 ALTER TABLE `tabelas_regioes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tipos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipos`;

CREATE TABLE `tipos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `flag` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tipos` WRITE;
/*!40000 ALTER TABLE `tipos` DISABLE KEYS */;

INSERT INTO `tipos` (`id`, `descricao`, `nome`, `flag`)
VALUES
	(1,'teste','Enfermaria','0'),
	(2,'','Apartamento','0'),
	(3,'','Ambulatorial','0'),
	(4,'','Hospitalar','0');

/*!40000 ALTER TABLE `tipos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `role`, `nome`, `created`, `modified`)
VALUES
	(1,'021.008.705-64','$2y$10$D3nCoWzHtRcLMkJDveQhEuWrme154m6ziTYGZxrxcrcxcT.ACx/HC','admin','Rodrigo Souza','2016-11-10 12:34:08','2016-11-10 12:34:08');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
