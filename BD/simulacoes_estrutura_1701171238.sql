# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.33)
# Base de Dados: simulacoes
# Tempo de Geração: 2017-01-17 15:39:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela abrangencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `abrangencias`;

CREATE TABLE `abrangencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela carencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carencias`;

CREATE TABLE `carencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `estados`;

CREATE TABLE `estados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela informacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `informacoes`;

CREATE TABLE `informacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(155) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela modalidades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modalidades`;

CREATE TABLE `modalidades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `carencia_id` int(11) unsigned NOT NULL,
  `informacao_id` int(11) unsigned NOT NULL,
  `observacao_id` int(11) unsigned NOT NULL,
  `opcional_id` int(11) unsigned NOT NULL,
  `rede_id` int(11) unsigned NOT NULL,
  `reembolso_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `carencia_id` (`carencia_id`),
  KEY `informacao_id` (`informacao_id`),
  KEY `observacao_id` (`observacao_id`),
  KEY `opcional_id` (`opcional_id`),
  KEY `rede_id` (`rede_id`),
  KEY `reembolso_id` (`reembolso_id`),
  CONSTRAINT `modalidades_ibfk_1` FOREIGN KEY (`carencia_id`) REFERENCES `carencias` (`id`),
  CONSTRAINT `modalidades_ibfk_2` FOREIGN KEY (`informacao_id`) REFERENCES `informacoes` (`id`),
  CONSTRAINT `modalidades_ibfk_3` FOREIGN KEY (`observacao_id`) REFERENCES `observacoes` (`id`),
  CONSTRAINT `modalidades_ibfk_4` FOREIGN KEY (`opcional_id`) REFERENCES `opcionais` (`id`),
  CONSTRAINT `modalidades_ibfk_5` FOREIGN KEY (`rede_id`) REFERENCES `redes` (`id`),
  CONSTRAINT `modalidades_ibfk_6` FOREIGN KEY (`reembolso_id`) REFERENCES `reembolsos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela observacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `observacoes`;

CREATE TABLE `observacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela opcionais
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opcionais`;

CREATE TABLE `opcionais` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operadoras`;

CREATE TABLE `operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produtos`;

CREATE TABLE `produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `produtos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela ramos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ramos`;

CREATE TABLE `ramos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela redes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redes`;

CREATE TABLE `redes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela reembolsos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reembolsos`;

CREATE TABLE `reembolsos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regioes`;

CREATE TABLE `regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `regioes_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela simulacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes`;

CREATE TABLE `simulacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `contato` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `faixa1` int(11) DEFAULT NULL,
  `faixa2` int(11) DEFAULT NULL,
  `faixa3` int(11) DEFAULT NULL,
  `faixa4` int(11) DEFAULT NULL,
  `faixa5` int(11) DEFAULT NULL,
  `faixa6` int(11) DEFAULT NULL,
  `faixa7` int(11) DEFAULT NULL,
  `faixa8` int(11) DEFAULT NULL,
  `faixa9` int(11) DEFAULT NULL,
  `faixa10` int(11) DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela simulacoes_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes_tabelas`;

CREATE TABLE `simulacoes_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `simulacao_id` int(11) unsigned NOT NULL,
  `tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `simulacao_id` (`simulacao_id`),
  KEY `tabela_id` (`tabela_id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_1` FOREIGN KEY (`simulacao_id`) REFERENCES `simulacoes` (`id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_2` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela subprodutos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subprodutos`;

CREATE TABLE `subprodutos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `tipo_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_id` (`tipo_id`),
  CONSTRAINT `subprodutos_ibfk_1` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas`;

CREATE TABLE `tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `vigencia` date NOT NULL,
  `faixa1` float DEFAULT NULL,
  `faixa2` float DEFAULT NULL,
  `faixa3` float DEFAULT NULL,
  `faixa4` float DEFAULT NULL,
  `faixa5` float DEFAULT NULL,
  `faixa6` float DEFAULT NULL,
  `faixa7` float DEFAULT NULL,
  `faixa8` float DEFAULT NULL,
  `faixa9` float DEFAULT NULL,
  `faixa10` float DEFAULT NULL,
  `produto_id` int(11) unsigned NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  `ramo_id` int(11) unsigned NOT NULL,
  `abrangencia_id` int(11) unsigned NOT NULL,
  `tipo_id` int(11) unsigned NOT NULL,
  `modalidade_id` int(11) unsigned NOT NULL,
  `subproduto_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_id` (`produto_id`),
  KEY `operadora_id` (`operadora_id`),
  KEY `ramo_id` (`ramo_id`),
  KEY `abrangencia_id` (`abrangencia_id`),
  KEY `tipo_id` (`tipo_id`),
  KEY `modalidade_id` (`modalidade_id`),
  KEY `subproduto_id` (`subproduto_id`),
  CONSTRAINT `tabelas_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`),
  CONSTRAINT `tabelas_ibfk_2` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`),
  CONSTRAINT `tabelas_ibfk_3` FOREIGN KEY (`ramo_id`) REFERENCES `ramos` (`id`),
  CONSTRAINT `tabelas_ibfk_4` FOREIGN KEY (`abrangencia_id`) REFERENCES `abrangencias` (`id`),
  CONSTRAINT `tabelas_ibfk_5` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`),
  CONSTRAINT `tabelas_ibfk_6` FOREIGN KEY (`modalidade_id`) REFERENCES `modalidades` (`id`),
  CONSTRAINT `tabelas_ibfk_7` FOREIGN KEY (`subproduto_id`) REFERENCES `subprodutos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela tabelas_regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_regioes`;

CREATE TABLE `tabelas_regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tabela_id` int(11) unsigned NOT NULL,
  `regiao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_id` (`tabela_id`),
  KEY `regiao_id` (`regiao_id`),
  CONSTRAINT `tabelas_regioes_ibfk_1` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`),
  CONSTRAINT `tabelas_regioes_ibfk_2` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela tipos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipos`;

CREATE TABLE `tipos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `flag` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
