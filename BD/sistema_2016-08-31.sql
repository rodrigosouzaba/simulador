# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.42)
# Base de Dados: sistema
# Tempo de Geração: 2016-08-31 14:37:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `estados`;

CREATE TABLE `estados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;

INSERT INTO `estados` (`id`, `nome`)
VALUES
	(2,'Acre'),
	(3,'Alagoas'),
	(4,'Amapá'),
	(5,'Amazonas'),
	(6,'Bahia'),
	(7,'Ceará'),
	(8,'Distrito Federal'),
	(9,'Espírito Santo'),
	(10,'Goiás'),
	(11,'Maranhão'),
	(12,'Mato Grosso'),
	(13,'Mato Grosso do Sul'),
	(14,'Minas Gerais'),
	(15,'Pará'),
	(16,'Paraíba'),
	(17,'Paraná'),
	(18,'Pernambuco'),
	(19,'Piauí'),
	(20,'Rio de Janeiro'),
	(21,'Rio Grande do Norte'),
	(22,'Rio Grande do Sul'),
	(23,'Rondônia'),
	(24,'Roraima'),
	(25,'Santa Catarina'),
	(26,'São Paulo'),
	(27,'Sergipe'),
	(28,'Tocantins');

/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operadoras`;

CREATE TABLE `operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `operadoras` WRITE;
/*!40000 ALTER TABLE `operadoras` DISABLE KEYS */;

INSERT INTO `operadoras` (`id`, `nome`)
VALUES
	(1,'AMIL'),
	(2,'BRADESCO');

/*!40000 ALTER TABLE `operadoras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produtos`;

CREATE TABLE `produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `produtos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;

INSERT INTO `produtos` (`id`, `nome`, `descricao`, `operadora_id`)
VALUES
	(1,'DENTAL','Plano Odontológico',1);

/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regioes`;

CREATE TABLE `regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `regioes_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `regioes` WRITE;
/*!40000 ALTER TABLE `regioes` DISABLE KEYS */;

INSERT INTO `regioes` (`id`, `nome`, `estado_id`)
VALUES
	(1,'RMS',6),
	(2,'Reconcavo',6),
	(3,'Mata',2);

/*!40000 ALTER TABLE `regioes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela simulacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes`;

CREATE TABLE `simulacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL,
  `faixa1` int(11) NOT NULL,
  `faixa2` int(11) NOT NULL,
  `faixa3` int(11) NOT NULL,
  `tabela_regiao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_regiao_id` (`tabela_regiao_id`),
  CONSTRAINT `simulacoes_ibfk_1` FOREIGN KEY (`tabela_regiao_id`) REFERENCES `tabelas_regioes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas`;

CREATE TABLE `tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `produto_id` int(11) unsigned NOT NULL,
  `acomodacao` char(1) NOT NULL DEFAULT '',
  `vigencia` date NOT NULL,
  `faixa1` int(11) NOT NULL,
  `faixa2` int(11) NOT NULL,
  `faixa3` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_id` (`produto_id`),
  CONSTRAINT `tabelas_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `tabelas` WRITE;
/*!40000 ALTER TABLE `tabelas` DISABLE KEYS */;

INSERT INTO `tabelas` (`id`, `nome`, `descricao`, `produto_id`, `acomodacao`, `vigencia`, `faixa1`, `faixa2`, `faixa3`)
VALUES
	(1,'Tabela 1','Tabela Janeiro',1,'E','2016-08-22',100,200,300),
	(2,'TAbela 2','Fevereiro',1,'A','2016-08-22',400,500,600);

/*!40000 ALTER TABLE `tabelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tabelas_regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_regioes`;

CREATE TABLE `tabelas_regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tabela_id` int(11) unsigned NOT NULL,
  `regiao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_id` (`tabela_id`),
  KEY `regiao_id` (`regiao_id`),
  CONSTRAINT `tabelas_regioes_ibfk_1` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`),
  CONSTRAINT `tabelas_regioes_ibfk_2` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `tabelas_regioes` WRITE;
/*!40000 ALTER TABLE `tabelas_regioes` DISABLE KEYS */;

INSERT INTO `tabelas_regioes` (`id`, `tabela_id`, `regiao_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,2,1),
	(4,2,3);

/*!40000 ALTER TABLE `tabelas_regioes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
