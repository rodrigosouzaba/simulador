# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.42)
# Base de Dados: sistema
# Tempo de Geração: 2016-11-03 19:34:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela carencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carencias`;

CREATE TABLE `carencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `carencias` WRITE;
/*!40000 ALTER TABLE `carencias` DISABLE KEYS */;

INSERT INTO `carencias` (`id`, `descricao`)
VALUES
	(1,'Carência de consultas e carência de exames');

/*!40000 ALTER TABLE `carencias` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `estados`;

CREATE TABLE `estados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;

INSERT INTO `estados` (`id`, `nome`)
VALUES
	(2,'Acre'),
	(3,'Alagoas'),
	(4,'Amapá'),
	(5,'Amazonas'),
	(6,'Bahia'),
	(7,'Ceará'),
	(8,'Distrito Federal'),
	(9,'Espírito Santo'),
	(10,'Goiás'),
	(11,'Maranhão'),
	(12,'Mato Grosso'),
	(13,'Mato Grosso do Sul'),
	(14,'Minas Gerais'),
	(15,'Pará'),
	(16,'Paraíba'),
	(17,'Paraná'),
	(18,'Pernambuco'),
	(19,'Piauí'),
	(20,'Rio de Janeiro'),
	(21,'Rio Grande do Norte'),
	(22,'Rio Grande do Sul'),
	(23,'Rondônia'),
	(24,'Roraima'),
	(25,'Santa Catarina'),
	(26,'São Paulo'),
	(27,'Sergipe'),
	(28,'Tocantins');

/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela informacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `informacoes`;

CREATE TABLE `informacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `informacoes` WRITE;
/*!40000 ALTER TABLE `informacoes` DISABLE KEYS */;

INSERT INTO `informacoes` (`id`, `descricao`)
VALUES
	(1,'1. Os valores acima são individuais para cada faixa etária, os totais incluem a soma de vidas por cada padrão.\n2. As carências apresentadas são as contratuais.\n3. No caso de possuir plano anterior, as operadoras estudam a redução das suas carências.');

/*!40000 ALTER TABLE `informacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela observacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `observacoes`;

CREATE TABLE `observacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `observacoes` WRITE;
/*!40000 ALTER TABLE `observacoes` DISABLE KEYS */;

INSERT INTO `observacoes` (`id`, `descricao`)
VALUES
	(1,'Observações adicionais de Tabela');

/*!40000 ALTER TABLE `observacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela opcionais
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opcionais`;

CREATE TABLE `opcionais` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `opcionais` WRITE;
/*!40000 ALTER TABLE `opcionais` DISABLE KEYS */;

INSERT INTO `opcionais` (`id`, `descricao`)
VALUES
	(1,'Opcionais de Tabela');

/*!40000 ALTER TABLE `opcionais` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operadoras`;

CREATE TABLE `operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `operadoras` WRITE;
/*!40000 ALTER TABLE `operadoras` DISABLE KEYS */;

INSERT INTO `operadoras` (`id`, `nome`)
VALUES
	(1,'AMIL'),
	(2,'BRADESCO');

/*!40000 ALTER TABLE `operadoras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produtos`;

CREATE TABLE `produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `produtos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;

INSERT INTO `produtos` (`id`, `nome`, `descricao`, `operadora_id`)
VALUES
	(1,'DENTAL','Plano Odontológico',1);

/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela redes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redes`;

CREATE TABLE `redes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `redes` WRITE;
/*!40000 ALTER TABLE `redes` DISABLE KEYS */;

INSERT INTO `redes` (`id`, `descricao`)
VALUES
	(1,'Principal diferencial é a utilização da rede preferencial Hapvida. Consultas e tratamentos com hora marcada. Internação em enfermaria ou apartamento. Exames nas clínicas Vida & Imagem e nos postos de coleta do Laboratório Antonio Prudente. Os planos HAPVIDA tem abrangência em sua rede preferencial nos seguintes estados: AL/ AM/ BA/ CE/ MA/ PA/ PB/ PE/ PI/ RN/ SE. Mix Este plano utiliza a rede preferencial Hapvida e mais uma ampla rede de médicos credenciados. Consultas e tratamentos com hora marcada, em clínicas preferenciais e particulares credenciadas para este plano. Possui internação em hospitais da rede preferencial em enfermaria ou apartamento, conforme plano escolhido. Exames nas clínicas Vida & Imagem e nos postos de coleta do Laboratório Antonio Prudente. Os planos HAPVIDA tem abrangência em sua rede preferencial nos seguintes estados: AL/ AM/ BA/ CE/ MA/ PA/ PB/ PE/ PI/ RN/ SE.');

/*!40000 ALTER TABLE `redes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela reembolsos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reembolsos`;

CREATE TABLE `reembolsos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `reembolsos` WRITE;
/*!40000 ALTER TABLE `reembolsos` DISABLE KEYS */;

INSERT INTO `reembolsos` (`id`, `descricao`)
VALUES
	(1,'Características para REEMBOLSO');

/*!40000 ALTER TABLE `reembolsos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regioes`;

CREATE TABLE `regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `regioes_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `regioes` WRITE;
/*!40000 ALTER TABLE `regioes` DISABLE KEYS */;

INSERT INTO `regioes` (`id`, `nome`, `estado_id`)
VALUES
	(1,'RMS',6),
	(2,'Reconcavo',6),
	(3,'Mata',2);

/*!40000 ALTER TABLE `regioes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela simulacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes`;

CREATE TABLE `simulacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL,
  `faixa1` int(11) NOT NULL,
  `faixa2` int(11) NOT NULL,
  `faixa3` int(11) NOT NULL,
  `faixa4` int(11) NOT NULL,
  `faixa5` int(11) NOT NULL,
  `faixa6` int(11) NOT NULL,
  `faixa7` int(11) NOT NULL,
  `faixa8` int(11) NOT NULL,
  `faixa9` int(11) NOT NULL,
  `faixa10` int(11) NOT NULL,
  `tabela_regiao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_regiao_id` (`tabela_regiao_id`),
  CONSTRAINT `simulacoes_ibfk_1` FOREIGN KEY (`tabela_regiao_id`) REFERENCES `tabelas_regioes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas`;

CREATE TABLE `tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `produto_id` int(11) unsigned NOT NULL,
  `acomodacao` char(1) NOT NULL DEFAULT '',
  `vigencia` date NOT NULL,
  `faixa1` int(11) NOT NULL,
  `faixa2` int(11) NOT NULL,
  `faixa3` int(11) NOT NULL,
  `faixa4` int(11) NOT NULL,
  `faixa5` int(11) NOT NULL,
  `faixa6` int(11) NOT NULL,
  `faixa7` int(11) NOT NULL,
  `faixa8` int(11) NOT NULL,
  `faixa9` int(11) NOT NULL,
  `faixa10` int(11) NOT NULL,
  `rede_id` int(11) unsigned DEFAULT NULL,
  `carencia_id` int(11) unsigned DEFAULT NULL,
  `informacao_id` int(11) unsigned DEFAULT NULL,
  `reembolso_id` int(11) unsigned DEFAULT NULL,
  `opcional_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_id` (`produto_id`),
  KEY `rede_id` (`rede_id`),
  KEY `carencia_id` (`carencia_id`),
  KEY `informacao_id` (`informacao_id`),
  KEY `reembolso_id` (`reembolso_id`),
  KEY `opcional_id` (`opcional_id`),
  CONSTRAINT `tabelas_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`),
  CONSTRAINT `tabelas_ibfk_2` FOREIGN KEY (`rede_id`) REFERENCES `redes` (`id`),
  CONSTRAINT `tabelas_ibfk_3` FOREIGN KEY (`carencia_id`) REFERENCES `carencias` (`id`),
  CONSTRAINT `tabelas_ibfk_4` FOREIGN KEY (`informacao_id`) REFERENCES `informacoes` (`id`),
  CONSTRAINT `tabelas_ibfk_5` FOREIGN KEY (`reembolso_id`) REFERENCES `reembolsos` (`id`),
  CONSTRAINT `tabelas_ibfk_6` FOREIGN KEY (`opcional_id`) REFERENCES `opcionais` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tabelas` WRITE;
/*!40000 ALTER TABLE `tabelas` DISABLE KEYS */;

INSERT INTO `tabelas` (`id`, `nome`, `descricao`, `produto_id`, `acomodacao`, `vigencia`, `faixa1`, `faixa2`, `faixa3`, `faixa4`, `faixa5`, `faixa6`, `faixa7`, `faixa8`, `faixa9`, `faixa10`, `rede_id`, `carencia_id`, `informacao_id`, `reembolso_id`, `opcional_id`)
VALUES
	(1,'Tabela 1','Tabela Janeiro',1,'E','2016-08-22',100,200,300,20,30,40,50,60,70,90,1,1,1,1,1),
	(2,'TAbela 2','Fevereiro',1,'A','2016-08-22',400,500,600,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tabelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tabelas_regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_regioes`;

CREATE TABLE `tabelas_regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tabela_id` int(11) unsigned NOT NULL,
  `regiao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_id` (`tabela_id`),
  KEY `regiao_id` (`regiao_id`),
  CONSTRAINT `tabelas_regioes_ibfk_1` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`),
  CONSTRAINT `tabelas_regioes_ibfk_2` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tabelas_regioes` WRITE;
/*!40000 ALTER TABLE `tabelas_regioes` DISABLE KEYS */;

INSERT INTO `tabelas_regioes` (`id`, `tabela_id`, `regiao_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,2,1),
	(4,2,3);

/*!40000 ALTER TABLE `tabelas_regioes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
