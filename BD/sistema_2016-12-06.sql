# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.42)
# Base de Dados: sistema
# Tempo de Geração: 2016-12-06 06:05:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela abrangencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `abrangencias`;

CREATE TABLE `abrangencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `abrangencias` WRITE;
/*!40000 ALTER TABLE `abrangencias` DISABLE KEYS */;

INSERT INTO `abrangencias` (`id`, `descricao`, `nome`)
VALUES
	(1,'desc teste','Nacional'),
	(2,'','Estadual'),
	(3,'','Municipal');

/*!40000 ALTER TABLE `abrangencias` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela carencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carencias`;

CREATE TABLE `carencias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `carencias` WRITE;
/*!40000 ALTER TABLE `carencias` DISABLE KEYS */;

INSERT INTO `carencias` (`id`, `descricao`, `nome`)
VALUES
	(1,'Carência de consultas e carência de exames','');

/*!40000 ALTER TABLE `carencias` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela estados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `estados`;

CREATE TABLE `estados` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;

INSERT INTO `estados` (`id`, `nome`)
VALUES
	(2,'Acre'),
	(3,'Alagoas'),
	(4,'Amapá'),
	(5,'Amazonas'),
	(6,'Bahia'),
	(7,'Ceará'),
	(8,'Distrito Federal'),
	(9,'Espírito Santo'),
	(10,'Goiás'),
	(11,'Maranhão'),
	(12,'Mato Grosso'),
	(13,'Mato Grosso do Sul'),
	(14,'Minas Gerais'),
	(15,'Pará'),
	(16,'Paraíba'),
	(17,'Paraná'),
	(18,'Pernambuco'),
	(19,'Piauí'),
	(20,'Rio de Janeiro'),
	(21,'Rio Grande do Norte'),
	(22,'Rio Grande do Sul'),
	(23,'Rondônia'),
	(24,'Roraima'),
	(25,'Santa Catarina'),
	(26,'São Paulo'),
	(27,'Sergipe'),
	(28,'Tocantins');

/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela informacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `informacoes`;

CREATE TABLE `informacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(155) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `informacoes` WRITE;
/*!40000 ALTER TABLE `informacoes` DISABLE KEYS */;

INSERT INTO `informacoes` (`id`, `descricao`, `nome`)
VALUES
	(1,'1. Os valores acima são individuais para cada faixa etária, os totais incluem a soma de vidas por cada padrão.\n2. As carências apresentadas são as contratuais.\n3. No caso de possuir plano anterior, as operadoras estudam a redução das suas carências.','');

/*!40000 ALTER TABLE `informacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela modalidades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modalidades`;

CREATE TABLE `modalidades` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL DEFAULT '',
  `descricao` varchar(100) NOT NULL DEFAULT '',
  `carencia_id` int(11) unsigned NOT NULL,
  `informacao_id` int(11) unsigned NOT NULL,
  `observacao_id` int(11) unsigned NOT NULL,
  `opcional_id` int(11) unsigned NOT NULL,
  `rede_id` int(11) unsigned NOT NULL,
  `reembolso_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `carencia_id` (`carencia_id`),
  KEY `informacao_id` (`informacao_id`),
  KEY `observacao_id` (`observacao_id`),
  KEY `opcional_id` (`opcional_id`),
  KEY `rede_id` (`rede_id`),
  KEY `reembolso_id` (`reembolso_id`),
  CONSTRAINT `modalidades_ibfk_1` FOREIGN KEY (`carencia_id`) REFERENCES `carencias` (`id`),
  CONSTRAINT `modalidades_ibfk_2` FOREIGN KEY (`informacao_id`) REFERENCES `informacoes` (`id`),
  CONSTRAINT `modalidades_ibfk_3` FOREIGN KEY (`observacao_id`) REFERENCES `observacoes` (`id`),
  CONSTRAINT `modalidades_ibfk_4` FOREIGN KEY (`opcional_id`) REFERENCES `opcionais` (`id`),
  CONSTRAINT `modalidades_ibfk_5` FOREIGN KEY (`rede_id`) REFERENCES `redes` (`id`),
  CONSTRAINT `modalidades_ibfk_6` FOREIGN KEY (`reembolso_id`) REFERENCES `reembolsos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `modalidades` WRITE;
/*!40000 ALTER TABLE `modalidades` DISABLE KEYS */;

INSERT INTO `modalidades` (`id`, `nome`, `descricao`, `carencia_id`, `informacao_id`, `observacao_id`, `opcional_id`, `rede_id`, `reembolso_id`)
VALUES
	(1,'Adesão','1',1,1,1,1,1,1),
	(2,'Individual','2',1,1,1,1,1,1),
	(3,'PME','3',1,1,1,1,1,1);

/*!40000 ALTER TABLE `modalidades` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela observacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `observacoes`;

CREATE TABLE `observacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `observacoes` WRITE;
/*!40000 ALTER TABLE `observacoes` DISABLE KEYS */;

INSERT INTO `observacoes` (`id`, `descricao`, `nome`)
VALUES
	(1,'Observações adicionais de Tabela','');

/*!40000 ALTER TABLE `observacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela opcionais
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opcionais`;

CREATE TABLE `opcionais` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `opcionais` WRITE;
/*!40000 ALTER TABLE `opcionais` DISABLE KEYS */;

INSERT INTO `opcionais` (`id`, `descricao`, `nome`)
VALUES
	(1,'Opcionais de Tabela','');

/*!40000 ALTER TABLE `opcionais` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela operadoras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operadoras`;

CREATE TABLE `operadoras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `operadoras` WRITE;
/*!40000 ALTER TABLE `operadoras` DISABLE KEYS */;

INSERT INTO `operadoras` (`id`, `nome`)
VALUES
	(1,'AMIL'),
	(2,'BRADESCO');

/*!40000 ALTER TABLE `operadoras` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela produtos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `produtos`;

CREATE TABLE `produtos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operadora_id` (`operadora_id`),
  CONSTRAINT `produtos_ibfk_1` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;

INSERT INTO `produtos` (`id`, `nome`, `descricao`, `operadora_id`)
VALUES
	(1,'TOP','Plano Odontológico',1),
	(2,'VIP','Plano VIP	',1),
	(5,'Básico','Plano Básico',1);

/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela ramos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ramos`;

CREATE TABLE `ramos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ramos` WRITE;
/*!40000 ALTER TABLE `ramos` DISABLE KEYS */;

INSERT INTO `ramos` (`id`, `descricao`, `nome`)
VALUES
	(1,'desc teste','SAÚDE'),
	(2,'Desc Odontológico','ODONTOLÓGICO');

/*!40000 ALTER TABLE `ramos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela redes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redes`;

CREATE TABLE `redes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `redes` WRITE;
/*!40000 ALTER TABLE `redes` DISABLE KEYS */;

INSERT INTO `redes` (`id`, `descricao`, `nome`)
VALUES
	(1,'Principal diferencial é a utilização da rede preferencial Hapvida. Consultas e tratamentos com hora marcada. Internação em enfermaria ou apartamento. Exames nas clínicas Vida & Imagem e nos postos de coleta do Laboratório Antonio Prudente. Os planos HAPVIDA tem abrangência em sua rede preferencial nos seguintes estados: AL/ AM/ BA/ CE/ MA/ PA/ PB/ PE/ PI/ RN/ SE. Mix Este plano utiliza a rede preferencial Hapvida e mais uma ampla rede de médicos credenciados. Consultas e tratamentos com hora marcada, em clínicas preferenciais e particulares credenciadas para este plano. Possui internação em hospitais da rede preferencial em enfermaria ou apartamento, conforme plano escolhido. Exames nas clínicas Vida & Imagem e nos postos de coleta do Laboratório Antonio Prudente. Os planos HAPVIDA tem abrangência em sua rede preferencial nos seguintes estados: AL/ AM/ BA/ CE/ MA/ PA/ PB/ PE/ PI/ RN/ SE.','');

/*!40000 ALTER TABLE `redes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela reembolsos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reembolsos`;

CREATE TABLE `reembolsos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `reembolsos` WRITE;
/*!40000 ALTER TABLE `reembolsos` DISABLE KEYS */;

INSERT INTO `reembolsos` (`id`, `descricao`, `nome`)
VALUES
	(1,'Características para REEMBOLSO','');

/*!40000 ALTER TABLE `reembolsos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regioes`;

CREATE TABLE `regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL DEFAULT '',
  `estado_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `regioes_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `regioes` WRITE;
/*!40000 ALTER TABLE `regioes` DISABLE KEYS */;

INSERT INTO `regioes` (`id`, `nome`, `estado_id`)
VALUES
	(1,'RMS',6),
	(2,'Reconcavo',6),
	(3,'Mata',2);

/*!40000 ALTER TABLE `regioes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela simulacoes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes`;

CREATE TABLE `simulacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `contato` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `faixa1` int(11) DEFAULT NULL,
  `faixa2` int(11) DEFAULT NULL,
  `faixa3` int(11) DEFAULT NULL,
  `faixa4` int(11) DEFAULT NULL,
  `faixa5` int(11) DEFAULT NULL,
  `faixa6` int(11) DEFAULT NULL,
  `faixa7` int(11) DEFAULT NULL,
  `faixa8` int(11) DEFAULT NULL,
  `faixa9` int(11) DEFAULT NULL,
  `faixa10` int(11) DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `simulacoes` WRITE;
/*!40000 ALTER TABLE `simulacoes` DISABLE KEYS */;

INSERT INTO `simulacoes` (`id`, `nome`, `contato`, `email`, `faixa1`, `faixa2`, `faixa3`, `faixa4`, `faixa5`, `faixa6`, `faixa7`, `faixa8`, `faixa9`, `faixa10`, `data`)
VALUES
	(17,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00'),
	(18,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00'),
	(19,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00'),
	(20,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00'),
	(21,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-06 02:27:45'),
	(22,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-06 02:28:48'),
	(23,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-06 02:29:40'),
	(24,'Gowebdesign','Rodrigo','rodrigo@gowebdesign.com.br',2,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-06 02:31:20');

/*!40000 ALTER TABLE `simulacoes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela simulacoes_tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simulacoes_tabelas`;

CREATE TABLE `simulacoes_tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `simulacao_id` int(11) unsigned NOT NULL,
  `tabela_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `simulacao_id` (`simulacao_id`),
  KEY `tabela_id` (`tabela_id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_2` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`),
  CONSTRAINT `simulacoes_tabelas_ibfk_1` FOREIGN KEY (`simulacao_id`) REFERENCES `simulacoes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `simulacoes_tabelas` WRITE;
/*!40000 ALTER TABLE `simulacoes_tabelas` DISABLE KEYS */;

INSERT INTO `simulacoes_tabelas` (`id`, `simulacao_id`, `tabela_id`)
VALUES
	(2,17,5),
	(3,17,7),
	(4,17,8),
	(5,18,5),
	(6,18,7),
	(7,18,8),
	(8,19,5),
	(9,19,7),
	(10,19,8),
	(11,20,5),
	(12,20,7),
	(13,20,8),
	(14,21,5),
	(15,21,7),
	(16,21,8),
	(17,22,5),
	(18,22,7),
	(19,22,8),
	(20,23,5),
	(21,23,7),
	(22,23,8),
	(23,24,5),
	(24,24,7),
	(25,24,8);

/*!40000 ALTER TABLE `simulacoes_tabelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela subprodutos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subprodutos`;

CREATE TABLE `subprodutos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `tipo_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_id` (`tipo_id`),
  CONSTRAINT `subprodutos_ibfk_1` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `subprodutos` WRITE;
/*!40000 ALTER TABLE `subprodutos` DISABLE KEYS */;

INSERT INTO `subprodutos` (`id`, `descricao`, `nome`, `tipo_id`)
VALUES
	(1,'SEM SUBPRODUTO','SEM SUBPRODUTO',1);

/*!40000 ALTER TABLE `subprodutos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tabelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas`;

CREATE TABLE `tabelas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descricao` text NOT NULL,
  `vigencia` date NOT NULL,
  `faixa1` float DEFAULT NULL,
  `faixa2` float DEFAULT NULL,
  `faixa3` float DEFAULT NULL,
  `faixa4` float DEFAULT NULL,
  `faixa5` float DEFAULT NULL,
  `faixa6` float DEFAULT NULL,
  `faixa7` float DEFAULT NULL,
  `faixa8` float DEFAULT NULL,
  `faixa9` float DEFAULT NULL,
  `faixa10` float DEFAULT NULL,
  `produto_id` int(11) unsigned NOT NULL,
  `operadora_id` int(11) unsigned NOT NULL,
  `ramo_id` int(11) unsigned NOT NULL,
  `abrangencia_id` int(11) unsigned NOT NULL,
  `tipo_id` int(11) unsigned NOT NULL,
  `modalidade_id` int(11) unsigned NOT NULL,
  `subproduto_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_id` (`produto_id`),
  KEY `operadora_id` (`operadora_id`),
  KEY `ramo_id` (`ramo_id`),
  KEY `abrangencia_id` (`abrangencia_id`),
  KEY `tipo_id` (`tipo_id`),
  KEY `modalidade_id` (`modalidade_id`),
  KEY `subproduto_id` (`subproduto_id`),
  CONSTRAINT `tabelas_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`),
  CONSTRAINT `tabelas_ibfk_2` FOREIGN KEY (`operadora_id`) REFERENCES `operadoras` (`id`),
  CONSTRAINT `tabelas_ibfk_3` FOREIGN KEY (`ramo_id`) REFERENCES `ramos` (`id`),
  CONSTRAINT `tabelas_ibfk_4` FOREIGN KEY (`abrangencia_id`) REFERENCES `abrangencias` (`id`),
  CONSTRAINT `tabelas_ibfk_5` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`),
  CONSTRAINT `tabelas_ibfk_6` FOREIGN KEY (`modalidade_id`) REFERENCES `modalidades` (`id`),
  CONSTRAINT `tabelas_ibfk_7` FOREIGN KEY (`subproduto_id`) REFERENCES `subprodutos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tabelas` WRITE;
/*!40000 ALTER TABLE `tabelas` DISABLE KEYS */;

INSERT INTO `tabelas` (`id`, `nome`, `descricao`, `vigencia`, `faixa1`, `faixa2`, `faixa3`, `faixa4`, `faixa5`, `faixa6`, `faixa7`, `faixa8`, `faixa9`, `faixa10`, `produto_id`, `operadora_id`, `ramo_id`, `abrangencia_id`, `tipo_id`, `modalidade_id`, `subproduto_id`)
VALUES
	(5,'Lero Lero','Lero Lero','2017-01-30',100.9,99.99,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,1,1,1,1,1),
	(7,'Tabela 2','Tabela 2','2017-03-05',100,200,300,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,1,1,1,1,1,1),
	(8,'Tabela 3','Tabela 3','2017-12-12',99,99,99,99,NULL,NULL,NULL,NULL,NULL,NULL,2,2,1,1,1,1,1);

/*!40000 ALTER TABLE `tabelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela tabelas_regioes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tabelas_regioes`;

CREATE TABLE `tabelas_regioes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tabela_id` int(11) unsigned NOT NULL,
  `regiao_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tabela_id` (`tabela_id`),
  KEY `regiao_id` (`regiao_id`),
  CONSTRAINT `tabelas_regioes_ibfk_1` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`),
  CONSTRAINT `tabelas_regioes_ibfk_2` FOREIGN KEY (`regiao_id`) REFERENCES `regioes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump da tabela tipos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipos`;

CREATE TABLE `tipos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` text NOT NULL,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `flag` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tipos` WRITE;
/*!40000 ALTER TABLE `tipos` DISABLE KEYS */;

INSERT INTO `tipos` (`id`, `descricao`, `nome`, `flag`)
VALUES
	(1,'teste','Enfermaria','0'),
	(2,'','Apto','0'),
	(3,'','Ambulatorial','0'),
	(4,'','Hospitalar','0');

/*!40000 ALTER TABLE `tipos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `role`, `nome`, `created`, `modified`)
VALUES
	(1,'02100870564','$2y$10$D3nCoWzHtRcLMkJDveQhEuWrme154m6ziTYGZxrxcrcxcT.ACx/HC','admin','Rodrigo Souza','2016-11-10 12:34:08','2016-11-10 12:34:08');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
