CREATE TABLE `envios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `mensagem` text NOT NULL,
  `sms_lista_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `sms_situacao_id` int(11) unsigned DEFAULT NULL,
  `data_envio` datetime DEFAULT NULL,
  `data_agendamento` datetime DEFAULT NULL,
  `autorizado` int(11) DEFAULT NULL,
  `descricao` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_lista_id` (`sms_lista_id`),
  KEY `sms_situacao_id` (`sms_situacao_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `envios_ibfk_1` FOREIGN KEY (`sms_lista_id`) REFERENCES `sms_listas` (`id`),
  CONSTRAINT `envios_ibfk_2` FOREIGN KEY (`sms_situacao_id`) REFERENCES `sms_situacoes` (`id`),
  CONSTRAINT `envios_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;