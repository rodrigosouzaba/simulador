create view vw_usuarios as SELECT
   `u`.`id` AS `id`,
   (select count(`s`.`data`) FROM `simulacoes` `s` where ((`s`.`data` >= (now() - interval 36 hour)) and (`s`.`user_id` = `u`.`id`))) AS `calculosdia_pme`,
   (select count(`s`.`id`) FROM `simulacoes` `s` where (`s`.`user_id` = `u`.`id`)) AS `calculos_pme`,

   (select count(`pf`.`id`) FROM `pf_calculos` `pf` where ((`pf`.`created` >= (now() - interval 36 hour)) and (`pf`.`user_id` = `u`.`id`))) AS `calculosdia_pf`,
   (select count(`pf`.`id`) FROM `pf_calculos` `pf` where (`pf`.`user_id` = `u`.`id`)) AS `calculos_pf`,

   (select count(`o`.`id`) FROM `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 36 hour)) and `s`.`user_id` = `u`.`id` and o.`tipo_pessoa` = 'PF')) AS `calculosdia_odonto_pf`,
   (select count(`o`.`id`) FROM `odonto_calculos` `o` where (`o`.`user_id` = `u`.`id` and o.`tipo_pessoa` = 'PF')) AS `calculos_odonto_pf`,
   
   (select count(`o`.`id`) FROM `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 36 hour)) and `s`.`user_id` = `u`.`id` and o.`tipo_pessoa` = 'PJ')) AS `calculosdia_odonto_pj`,
   (select count(`o`.`id`) FROM `odonto_calculos` `o` where (`o`.`user_id` = `u`.`id` and o.`tipo_pessoa` = 'PJ')) AS `calculos_odonto_pj`,
   
   ((select count(`s`.`id`) FROM `simulacoes` `s` where (`s`.`user_id` = `u`.`id`))+(select count(`pf`.`id`) FROM `pf_calculos` `pf` where (`pf`.`user_id` = `u`.`id`))+(select count(`o`.`id`) FROM `odonto_calculos` `o` where         
   (`o`.`user_id` = `u`.`id` and o.`tipo_pessoa` = 'PF'))+(select count(`o`.`id`) FROM `odonto_calculos` `o` where (`o`.`user_id` = `u`.`id` and o.`tipo_pessoa` = 'PJ'))) as total_calculos,
   
   `u`.`estado_id` AS `estado_id`,
   `u`.`site` AS `site`,
   `u`.`facebook` AS `facebook`,
   `u`.`whatsapp` AS `whatsapp`,
   `u`.`imagem_id` AS `imagem_id`,
   `u`.`user_pai_id` AS `user_pai_id`,
   `u`.`username` AS `username`,
   `u`.`data_nascimento` AS `data_nascimento`,
   `u`.`password` AS `password`,
   `u`.`ultimologin` AS `ultimologin`,
   `u`.`ultimocalculo` AS `ultimocalculo`,
   `u`.`role` AS `role`,
   `u`.`nome` AS `nome`,
   `u`.`sobrenome` AS `sobrenome`,
   `u`.`created` AS `created`,
   `u`.`modified` AS `modified`,
   `u`.`email` AS `email`,
   `u`.`validacao` AS `validacao`,
   `u`.`codigo` AS `codigo`,
   `u`.`celular` AS `celular`,
   `u`.`tipo` AS `tipo`,
   `e`.`nome` AS `estado_nome`,
   i.id as id_da_imagem,
   i.`caminho` as imagem_caminho,
   i.`nome` as imagem_nome
from 
`users` `u` 
left join `estados` `e` on `e`.`id` = `u`.`estado_id` 
left join `pf_calculos` `pf` on `pf`.`user_id` = `u`.`id` 
left join `simulacoes` `s` on `s`.`user_id` = `u`.`id`
left join imagens i on i.id = u.`imagem_id`
group by `u`.`id`;



