#!/bin/bash
root="/var/www/app"

cd $root

echo -e '\e[1m\e[34mPulling code from remote..\e[0m\n'

git pull origin master

echo -e '\e[1m\e[34m\nInstalling required packages..\e[0m\n'

php composer.phar install

echo "Deploy realizado"