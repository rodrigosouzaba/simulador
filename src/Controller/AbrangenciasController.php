<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Abrangencias Controller
 *
 * @property \App\Model\Table\AbrangenciasTable $Abrangencias
 */
class AbrangenciasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->set('title', 'Áreas de Comercialização');

        $abrangencias = $this->paginate($this->Abrangencias);

        $this->set(compact('abrangencias'));
        $this->set('_serialize', ['abrangencias']);
    }

    /**
     * View method
     *
     * @param string|null $id Abrangencia id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $abrangencia = $this->Abrangencias->get($id, [
            'contain' => ['Tabelas']
        ]);

        $this->set('abrangencia', $abrangencia);
        $this->set('_serialize', ['abrangencia']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $abrangencia = $this->Abrangencias->newEntity();
        if ($this->request->is('post')) {
            $abrangencia = $this->Abrangencias->patchEntity($abrangencia, $this->request->data);
            if ($this->Abrangencias->save($abrangencia)) {
                $this->Flash->success(__('Áreas de Comercialização salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar Áreas de Comercialização. Tente novamente.'));
            }
        }
        $this->set(compact('abrangencia'));
        $this->set('_serialize', ['abrangencia']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Abrangencia id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $abrangencia = $this->Abrangencias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $abrangencia = $this->Abrangencias->patchEntity($abrangencia, $this->request->data);
            if ($this->Abrangencias->save($abrangencia)) {
                $this->Flash->success(__('Áreas de Comercialização salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar Áreas de Comercialização. Tente novamente.'));
            }
        }
        $this->set(compact('abrangencia'));
        $this->set('_serialize', ['abrangencia']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Abrangencia id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $abrangencia = $this->Abrangencias->get($id);
        if ($this->Abrangencias->delete($abrangencia)) {
            $this->Flash->success(__('The abrangencia has been deleted.'));
        } else {
            $this->Flash->error(__('The abrangencia could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
