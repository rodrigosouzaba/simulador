<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Agendamentos Controller
 *
 * @property \App\Model\Table\AgendamentosTable $Agendamentos
 */
class AgendamentosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Users', 'Operadoras']
        ];
        $operadoras = $this->Agendamentos->Operadoras->find('list', ['valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['Operadoras.nome' => 'ASC']]);
        $agendamentos = $this->paginate($this->Agendamentos);

        $this->set(compact('agendamentos', 'operadoras'));
        $this->set('_serialize', ['agendamentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Agendamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $agendamento = $this->Agendamentos->get($id, [
            'contain' => ['Users', 'Tabelas']
        ]);

        $this->set('agendamento', $agendamento);
        $this->set('_serialize', ['agendamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $agendamento = $this->Agendamentos->newEntity();
        if ($this->request->is('post')) {
            $dados = $this->request->data;
//            debug($dados);
            $agendamento = $this->Agendamentos->patchEntity($agendamento, $this->request->data);
//            debug($agendamento);
//            die();
            if ($this->Agendamentos->save($agendamento)) {
                $this->Flash->success(__('Salvo com sucesso'));
                $this->loadModel('AgendamentosTabelas');
                foreach ($dados['Tabelas'] as $dado) {

                    if ($dado <> 'false') {
                        $agendamento_tabela['agendamento_id'] = $agendamento->id;
                        $agendamento_tabela['tabela_id'] = $dado;
                        $entidade = $this->AgendamentosTabelas->newEntity();

                        $entidade = $this->AgendamentosTabelas->patchEntity($entidade, $agendamento_tabela);
                        $this->AgendamentosTabelas->save($entidade);
                    }
                }

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $operadoras = $this->Agendamentos->Operadoras->find('list', ['valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['Operadoras.nome' => 'ASC']]);
        $this->set(compact('agendamento', 'operadoras'));
        $this->set('_serialize', ['agendamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Agendamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $agendamento = $this->Agendamentos->get($id, [
            'contain' => ['Tabelas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $agendamento = $this->Agendamentos->patchEntity($agendamento, $this->request->data);
            if ($this->Agendamentos->save($agendamento)) {
                $this->Flash->success(__('Salvo com Sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $operadoras = $this->Agendamentos->Operadoras->find('list', ['valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['Operadoras.nome' => 'ASC']]);
        
        $this->set(compact('agendamento', 'operadoras'));
        $this->set('_serialize', ['agendamento']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Agendamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $agendamento = $this->Agendamentos->get($id);
        $this->loadModel('AgendamentosTabelas');
        $agendamentostabelas = $this->AgendamentosTabelas->find('all')->where(['AgendamentosTabelas.agendamento_id' => $id]);
        foreach ($agendamentostabelas as $agdTab) {
            $ag = $this->AgendamentosTabelas->get($agdTab['id']);
            $this->AgendamentosTabelas->delete($ag);
        }
        if ($this->Agendamentos->delete($agendamento)) {
            $this->Flash->success(__('The agendamento has been deleted.'));
        } else {
            $this->Flash->error(__('The agendamento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Filtro de Agendamentos por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($operadora_id = null) {


        $agendamentos = $this->Agendamentos->find('all')->where(['Agendamentos.operadora_id' => $operadora_id])->contain(['Users', 'Operadoras'])->toArray();

        $this->set(compact('agendamentos'));
    }

    /**
     * Filtro de tabelas por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtrarTabelas($operadora_id = null) {
        $this->loadModel('Tabelas');
        $tabelas = $this->Tabelas->find('all')->where(['Tabelas.operadora_id' => $operadora_id])->toArray();

        $this->set(compact('tabelas'));
    }

}
