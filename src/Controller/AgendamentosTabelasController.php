<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AgendamentosTabelas Controller
 *
 * @property \App\Model\Table\AgendamentosTabelasTable $AgendamentosTabelas
 */
class AgendamentosTabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Agendamentos', 'Tabelas']
        ];
        $agendamentosTabelas = $this->paginate($this->AgendamentosTabelas);

        $this->set(compact('agendamentosTabelas'));
        $this->set('_serialize', ['agendamentosTabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Agendamentos Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agendamentosTabela = $this->AgendamentosTabelas->get($id, [
            'contain' => ['Agendamentos', 'Tabelas']
        ]);

        $this->set('agendamentosTabela', $agendamentosTabela);
        $this->set('_serialize', ['agendamentosTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $agendamentosTabela = $this->AgendamentosTabelas->newEntity();
        if ($this->request->is('post')) {
            $agendamentosTabela = $this->AgendamentosTabelas->patchEntity($agendamentosTabela, $this->request->data);
            if ($this->AgendamentosTabelas->save($agendamentosTabela)) {
                $this->Flash->success(__('The agendamentos tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The agendamentos tabela could not be saved. Please, try again.'));
            }
        }
        $agendamentos = $this->AgendamentosTabelas->Agendamentos->find('list', ['limit' => 200]);
        $tabelas = $this->AgendamentosTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('agendamentosTabela', 'agendamentos', 'tabelas'));
        $this->set('_serialize', ['agendamentosTabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Agendamentos Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $agendamentosTabela = $this->AgendamentosTabelas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $agendamentosTabela = $this->AgendamentosTabelas->patchEntity($agendamentosTabela, $this->request->data);
            if ($this->AgendamentosTabelas->save($agendamentosTabela)) {
                $this->Flash->success(__('The agendamentos tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The agendamentos tabela could not be saved. Please, try again.'));
            }
        }
        $agendamentos = $this->AgendamentosTabelas->Agendamentos->find('list', ['limit' => 200]);
        $tabelas = $this->AgendamentosTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('agendamentosTabela', 'agendamentos', 'tabelas'));
        $this->set('_serialize', ['agendamentosTabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Agendamentos Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agendamentosTabela = $this->AgendamentosTabelas->get($id);
        if ($this->AgendamentosTabelas->delete($agendamentosTabela)) {
            $this->Flash->success(__('The agendamentos tabela has been deleted.'));
        } else {
            $this->Flash->error(__('The agendamentos tabela could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
