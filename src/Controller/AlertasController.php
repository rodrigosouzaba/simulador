<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Alertas Controller
 *
 * @property \App\Model\Table\AlertasTable $Alertas
 */
class AlertasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($ramo = null, $calculo = null)
    {
/*
        $this->paginate = [
            'contain' => ['Users', 'Simulacoes', 'OdontoCalculos']
        ];
        $alertas = $this->paginate($this->Alertas);
*/
		switch($ramo){
			case "SPF":
				$colunaId = "pf_calculo_id";
				break;	
			case "OPF":
				$colunaId = "odonto_calculo_id";
				break;				
			case "SPJ":
				$colunaId = "simulacao_id";
			break;	
			case "OPJ":
				$colunaId = "odonto_calculo_id";
				break;	
		}
		
		$session = $this->request->session();
        $sessao = $session->read('Auth.User');
        
		$query = $this->Alertas->find('all')->where(['model' => $ramo, $colunaId => $calculo, "user_id" => $sessao["id"]])->contain(["Status"])->order(["Alertas.id" => "DESC"]);
		$this->set('alertas', $this->paginate($query));
    
        $this->set(compact('sessao', 'ramo', 'calculo'));

    }

    /**
     * View method
     *
     * @param string|null $id Alerta id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $alerta = $this->Alertas->get($id, [
            'contain' => ['Users', 'Simulacoes', 'OdontoCalculos']
        ]);

        $this->set('alerta', $alerta);
        $this->set('_serialize', ['alerta']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($ramo = null, $calculo = null, $visualizar = null)
    {
        $alerta = $this->Alertas->newEntity();
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        switch($ramo){
				case "SPF":
					$model = "PfCalculos";
					$colunaId = "pf_calculo_id";
					break;	
				case "OPF":
					$model = "OdontoCalculos";
					$colunaId = "odonto_calculo_id";					
					break;				
				case "SPJ":
					$model = "Simulacoes";
					$colunaId = "simulacao_id";
				break;	
				case "OPJ":
					$model = "OdontoCalculos";
					$colunaId = "odonto_calculo_id";
					break;	
		}
		if($visualizar == true){
			$this->Alertas->visualizarAlerta($ramo, $calculo,$sessao["id"]);
		}
        if ($this->request->is(['put','post'])) {
			if($this->request->data["data_alerta_check"] <>1){
				$this->request->data["data_alerta"] = null;
			}
			$this->loadModel($model);
			$status = $this->$model->find("all")->select([$model.".status_id"])->where([$model.".id" => $calculo])->contain("Status")->first();

			$this->request->data["status_id"] = $status["status_id"];
				        
            $alerta = $this->Alertas->patchEntity($alerta, $this->request->data);
            $this->Alertas->save($alerta);
		}
       
        
		$alertas = $this->Alertas->find('all')->where(['model' => $ramo, $colunaId => $calculo, "user_id" => $sessao["id"]])->contain(["Status"])->order(["Alertas.created" => "DESC"])->toArray();
	
        $this->set(compact('alerta','ramo', 'sessao', 'calculo','alertas'));
        $this->set('_serialize', ['alerta']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Alerta id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $alerta = $this->Alertas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $alerta = $this->Alertas->patchEntity($alerta, $this->request->data);
            $this->Alertas->save($alerta);
        }
        $users = $this->Alertas->Users->find('list', ['limit' => 200]);
        $simulacoes = $this->Alertas->Simulacoes->find('list', ['limit' => 200]);
        $odontoCalculos = $this->Alertas->OdontoCalculos->find('list', ['limit' => 200]);
        $this->set(compact('alerta', 'users', 'simulacoes', 'odontoCalculos'));
        $this->set('_serialize', ['alerta']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Alerta id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $alerta = $this->Alertas->get($id);
        if ($this->Alertas->delete($alerta)) {
            $this->Flash->success(__('The alerta has been deleted.'));
        } else {
            $this->Flash->error(__('The alerta could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
