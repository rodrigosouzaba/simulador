<?php
namespace App\Controller\Api;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Crud\Controller\Component\CrudComponent;
use Firebase\JWT\JWT;
use Cake\Http\Response;

class AppController extends Controller
{

    use \Crud\Controller\ControllerTrait;

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Menu');
        $this->loadComponent('Crud.Crud', [
           'actions'=>[
               'Crud.Index',
               'Crud.View',
               'Crud.Add',
               'Crud.Edit',
               'Crud.Delete',
           ] ,
            'listeners'=>[
                'Crud.Api',
                'Crud.ApiPagination'
            ]
        ]);

        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ],
                    'userModel' => 'Users',
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'userModel' => 'Users',
                    'fields' => [
                        'username' => 'username'
                    ],
                    'parameter' => '_token',
                    // Boolean indicating whether the "sub" claim of JWT payload
                    // should be used to query the Users model and get user info.
                    // If set to `false` JWT's payload is directly returned.
                    'queryDatasource' => true,
                ]
            ],

            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize',

            // If you don't have a login action in your application set
            // 'loginAction' to false to prevent getting a MissingRouteException.
            'loginAction' => false
        ]);
    }
    
    public function validaJWT($token)
    {
        try { 
            $decodejwt = JWT::decode($token, Security::salt(), ['HS256']); 
            return $decodejwt;
		  } catch (\Exception $e) {
			return $e; 
		  }

    }

}
?>