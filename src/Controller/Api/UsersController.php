<?php

namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event; //if you dont have this allready

use Cake\Controller\Component\MenuComponent;

use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Crud\Controller\Component\CrudComponent;
use Firebase\JWT\JWT;
use Cake\Http\Response;

class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'menuFerramentas', 'resUser', 'listUsers', 'getUser']);
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Menu');
    }

    public function beforeRender(event $event)
    {
        $this->setCorsHeaders();
    }

    private function setCorsHeaders()
    {
        $this->response->cors($this->request)
            ->allowOrigin(['*'])
            ->allowMethods(['*'])
            ->allowHeaders(['X-CSRF-Token', 'x-xsrf-token', 'Origin', 'Content-Type', 'X-Auth-Token', 'Bearer Token'])
            ->allowCredentials(['false'])
            ->exposeHeaders(['Link'])
            ->maxAge(300)
            ->build();
    }

    public function login()
    {

        try {
            $user = $this->Auth->identify();
            $checaUsuario = $this->Users->find("all")->where(["username" => $this->request->data["username"]])->first();
            if ($checaUsuario) { //CPF EXISTE

                if (!$user) {
                    throw new UnauthorizedException('Usuário e senha invalido');
                }

                //Usuário não BLOQUEADO PELA ADMINISTRADOR
                if ($user["bloqueio"] <> "S") {

                    //SALVA ULTIMO LOGIN DO USUARIO
                    $userLogado = $this->Users->get($user['id']);
                    $userLogado->ultimologin = date('Y-m-d H:i:s');
                    $userLogado->dirty('ultimologin', true);
                    $this->Users->save($userLogado);

                    $this->Auth->setUser($user);
                    $session = $this->request->session();

                    $this->loadModel('UsersGrupos');
                    $user_grupos = $this->UsersGrupos->find('list')->where(['user_id' => $user['id']])->toArray();
                    $session->write([
                        'Auth.User.grupos' => $user_grupos
                    ]);

                    $user['perfisUser'] = array_values($this->Users->UsersSisGrupos->getPerfis($user['id']));

                    $userCtlr = new \App\Controller\UsersController;
                    $dadosMenuPrincipal = $this->Menu->dadosMenu($user);
                    $menu = $userCtlr->menuAdicional($dadosMenuPrincipal);
                    ksort($menu['linha'][1]);
                    $session->write([
                        'Auth.menu' => $menu
                    ]);

                    // Novas Interações
                    $this->loadModel('Avaliacaos');
                    $interacoes = $this->Avaliacaos->find('list')->where("created > '$userLogado->ultimologin'")->count();
                    $session->write(['Auth.User.new_interacoes' => $interacoes]);
                    // FIM - Novas Interações

                    $this->loadModel('Imagens');
                    $imagem = $this->Imagens->find('list')->where(['id' => $user['imagem_id']])->count();
                    if ($imagem > 0) {
                        $imagem = $this->Imagens->get($user['imagem_id']);
                        $session->write(['Auth.User.imagem' => $imagem]);
                        $user['imagem'] = $imagem;
                    }

                    //VERIFICA SE HÁ SESSAO NO BANCO E APAGA (SESSAO EM OUTROS DISPOSITIVOS)
                    $this->loadModel('Sessions');
                    $this->Sessions->deleteAll(['id <>' => $this->request->session()->id(), 'user_id' => $user["id"]]);
                    $token = JWT::encode(
                        [
                            'sub' => $user['id'],
                            'data' => $user,
                            'exp' =>  time() + 1380
                        ],
                        Security::getSalt()
                    );
                    $this->set([
                        'success' => true,
                        'data' => [
                            'token_type' => 'Bearer',
                            'expires_in' => 1380,
                            'token' => $token,
                            'perfisUser' => $user['perfisUser'],
                            'grupos' => $user_grupos
                        ],
                        '_serialize' => ['success', 'data']
                    ]);
                    $this->request->getSession()->write('Auth.token', $token);
                } else {
                    $resposta["msg"] = 'Seu período de teste expirou.<br>Entre em contato com <br>o suporte a corretores.';

                    $this->set([
                        'success' => true,
                        'data' => $resposta,
                        '_serialize' => ['success', 'data']
                    ]);

                    $this->Auth->logout();
                }
            } else {
                //USUÁRIO NÃO CADASTRADO
                $resposta["msg"] = 'Usuário não Cadastrado';
                $this->set([
                    'success' => true,
                    'data' => $resposta,
                    '_serialize' => ['success', 'data']
                ]);
            }
        } catch (UnauthorizedException $e) {
            throw new UnauthorizedException($e->getMessage(), 401);
        }
    }

    public function menuFerramentas($headGet = null)
    {
        $userCtlr = new \App\Controller\UsersController;
        $this->autoRender = false;
        $dadosUser = '';
        if ($headGet != null) {
            $head = $headGet;
        } else {
            $head = $this->request->getHeaderLine('Authorization');
        }

        if (isset($head) && !empty($head)) {
            $token = explode(' ', $head);
            $dadosUser = $this->validaJWT($token[1]);
            $tratamento = json_decode(json_encode($dadosUser), true);
            $dadosUser = $tratamento['data'];
        }

        $dadosMenuPrincipal = $this->Menu->dadosMenu($dadosUser);
        $dados = $userCtlr->menuAdicional($dadosMenuPrincipal);
        ksort($dados['linha'][1]);

        $this->request->getSession()->write('Auth.menu', $dadosMenuPrincipal);

        $this->response->type('json');
        $this->response->body(json_encode($dados));
    }

    public function resUser($headGet = null)
    {
        $this->autoRender = false;
        $dados = [];
        if ($headGet != null) {
            $head = $headGet;
        } else {
            $head = $this->request->getHeaderLine('Authorization');
        }

        if (isset($head) && !empty($head)) {
            $token = explode(' ', $head);
            $dados = $this->validaJWT($token[1]);

            $this->loadModel('SisGrupos');
            $dados->data->permissions = $this->SisGrupos->getFuncionalidadesPermissoes($dados->data->id);

            $tratamento = json_decode(json_encode($dados), true);

            $dados = $tratamento['data'];
        } else {
            $dados = [$head];
        }

        $this->response->body(json_encode($dados));
        $this->response->type('json');
    }

    public function listUsers()
    {
        $this->autoRender = false;
        $dados = $this->Users->find("all")->select(['email', 'username', 'nome', 'sobrenome', 'data_nascimento', 'celular', 'bloqueio']);
        $this->response->type('json');
        $this->response->body(json_encode($dados));
    }

    public function getUser($email)
    {
        $this->autoRender = false;
        $dados = $this->Users->find("all")->select(['email', 'username', 'nome', 'sobrenome', 'data_nascimento', 'celular', 'bloqueio'])->where(['email' => $email]);
        $this->response->type('json');
        $this->response->body(json_encode($dados));
    }
}
