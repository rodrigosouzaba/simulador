<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Database\Type;
use Cake\Network\Response;
use Cake\I18n\I18n;

use Cake\ORM\TableRegistry;

// Habilita o parseamento de datas localizadas
Type::build('date')
    ->useLocaleParser()
    ->setLocaleFormat('dd/MM/yyyy');
Type::build('datetime')
    ->useLocaleParser()
    ->setLocaleFormat('dd/MM/yyyy HH:mm:ss');
Type::build('timestamp')
    ->useLocaleParser()
    ->setLocaleFormat('dd/MM/yyyy HH:mm:ss');

// Habilita o parseamento de decimal localizaddos
Type::build('decimal')
    ->useLocaleParser();
Type::build('float')
    ->useLocaleParser();

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        I18n::locale('pt_BR');

        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Global');
        $this->loadComponent('Menu');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authorize' => ['Controller'], // Adicione está linha
            'authError' => 'Você não tem permissão para acessar: ',
            //             'loginRedirect' => "https://corretorparceiro.com.br/ferramentas",
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'central'
            ],
            'logoutRedirect' => 'https://corretorparceiro.com.br',
            'unauthorizedRedirect' => 'https://corretorparceiro.com.br'
            //            'loginRedirect' => "https://corretorparceiro.com.br",
            //LOCAL
            //             'logoutRedirect' => ["controller" => "Users", "action" => "login"]

            //PRODUCAO
            //            'logoutRedirect' => "https://corretorparceiro.com.br",
        ]);
    }

    /*
     *
     * Usuário do tipo admin pode acessar tudo
     *
     */
    public function isAuthorized($user)
    {
        // VERIFICA SE IRÁ CARREGAR OS DADOS DE PERMISSÃO DA BASE OU DA SESSÃO
        // if (Configure::read('checkPermissionInSession') == 1 && $this->Session->check('Auth.User.Permission')) {
        if ($this->request->session()->check('Auth.User.Permission')) {
            $data = $this->request->session()->read('Auth.User.Permission');
        } else {
            // PESQUISA PERMISSÕES DO GRUPO
            $this->loadModel('SisGrupos');
            $data = $this->SisGrupos->getFuncionalidadesPermissoes($user['id']);
            $this->request->session()->write('Auth.User.Permission', $data);
        }

        // PEGA CONTROLLER E ACTION REQUISITADA
        $controller = $this->request->params['controller'];
        $action = $this->request->params['action'];

        // CASO SEJA REQUISITADO O CONTROLLER PAGES O SISTEMA DARAR 
        // ACESSO IRRESTRITO A TODAS O ACESSO A TODAS AS ACTIONS
        if ($controller == 'pages') {
            return true;
        }

        // VERIFICA SE O USUÁRIO TEM PERMISSÃO OU NÃO PARA ACESSAR 
        // A FUNCIONALIDADE REQUISITADA
        $permissao = lcfirst($controller) . '.' . $action;

        $temPermissao = in_array($permissao, $data);
        return true;
        return $temPermissao;

        // // Admin pode acessar todas as actions
        // if (isset($user['role']) && $user['role'] === 'admin') {
        //     return true;
        // }
        // if (isset($user['role']) && $user['role'] === 'padrao') {
        //     $controller = $this->request->controller;
        //     if ($controller == 'Tabelas') {
        //         $this->Flash->success(__('Sem permissão para acessar este módulo do Sistema.'));

        //         return false;
        //     } else {
        //         return true;
        //     }
        // }
        // // Bloqueia acesso por padrão
        // return true;
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (
            !array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['logout']);
        if ($this->request->is('options')) {
            return $this->response;
            $this->response->header('Access-Control-Allow-Origin', '*');
            $this->response->header('Access-Control-Allow-Methods', '*');
            $this->response->header('Access-Control-Allow-Headers', 'X-Requested-With');
            $this->response->header('Access-Control-Allow-Headers', 'Content-Type, x-xsrf-token');
            $this->response->header('Access-Control-Max-Age', '172800');
        }
    }

    public function validarCPF($cpf = '')
    {

        $cpf = str_pad(preg_replace('/[^0-9]/', '', $cpf), 11, '0', STR_PAD_LEFT);
        // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
            return FALSE;
        } else { // Calcula os números para verificar se o CPF é verdadeiro
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{
                        $c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{
                    $c} != $d) {
                    return FALSE;
                }
            }
            return TRUE;
        }
    }

    // PREPARA CAMPOS QUE SÃO DA TABELA. ESTA FUNCIONALIDADE É UTILIZADA PARA
    // TRATAR OS CAMPO QUE SERÃO UTILIZADO NO INDEX DA GRID (W2UI -> W2GRUI)
    public function _preparaCampos($data)
    {
        // INICIALIZA VARIÁVEIS
        $campos = [];
        foreach ($data as $model => $field) {
            if (is_array($field)) {
                foreach ($field as $k => $v) {
                    if ($v != '' && $model != 'selectAlldata' && $model != 'selectGroupdata' && $model != 'selectItemdata') {
                        $campos[$model . '.' . $k] = $v;
                    }
                }
            } else {
                $campos[$this->modelClass . '.' . $model] = $field;
            }
        }
        return $campos;
    }

    /**
     * _index method
     * 
     * @desciption Prepara a consulta motando $conditions e o $order  
     * @return array($order, $conditions)
     */
    public function _index()
    {
        // INICIALIZA VARIÁVEIS
        $conditions = '';

        // CASO SEJA INFORMADO CAMPOS DE PESQUISA CRIA CONDITION
        if ($this->request->is(['post', 'get'])) {
            // VERIFICA SE EXISTEM CAMPOS DE OUTRAS TABELAS PARA PREPARAR A QUERY
            $this->request->data = $this->_preparaCampos($this->request->data);

            // TODO: TRATA CAMPOS COM VALUE_ID IGUAL A NULL
            // SETA CONDITIONS
            $conditions = $this->request->data;
        }
        return $conditions; //compact('conditions');
    }

    /**
     * _delete method
     * 
     * @desciption Deleta registros de uma tabela 
     * @return json
     */
    public function _delete($id = null, $cascade = null, $modelAcess = null)
    {
        // INICIALIZA VARIÁVEIS
        $error = 0;
        $exception = '';
        $code = '';
        $msg = 'Registro excluído com sucesso.';
        $this->autoRender = ($this->request->is('ajax')) ? false : true;
        $model = ($modelAcess == null) ? $this->modelClass : $modelAcess;

        try {
            if ($modelAcess == null) {
                if (!$this->request->is('post') || $this->autoRender == true) {
                    throw new MethodNotAllowedException();
                }
            }

            $this->loadModel($model);
            $dado = $this->$model->get($id);

            if (!$this->$model->exists(['id' => $id])) {
                $this->Flash->error(__('Registro inexistente'));
            }

            if (!$this->$model->delete($dado)) {
                $msg = 'Erro desconhecido.';
                if (isset($this->$model->validationErrors['msg'])) {
                    throw new CakeException($this->$model->validationErrors['msg'], 1);
                } else {
                    throw new CakeException('Erro desconhecido.');
                }
            }
        } catch (Exception $exception) {
            $error = 1;
            $code = $exception->getCode();
            switch ($code) {
                case '405':
                    $msg = 'Não foi possível excluir o registro selecionado, pois o método útilizado não é permitido.';
                    break;
                case '501':
                    $msg = 'Não foi possível excluir o registro selecionado, pois o mesmo não existe mais. Provavelmente, este registro foi excluído por outro usuário do sistema.';
                    break;
                case '404':
                    $msg = 'Não foi possível excluir o registro selecionado, pois o mesmo não existe mais. Provavelmente, este registro foi excluído por outro usuário do sistema.';
                    break;
                case '23000':
                    $msg = 'Não foi possível excluir o registro selecionado, pois o mesmo encontra-se em uso no sistema.';
                    break;
                case '1':
                    $msg = $exception->getMessage();
                    break;
                default:
                    $msg = 'Não foi possível excluir o registro.<br/>Favor tentar novamente. Caso o problema persista favor entrar em contato com o administrador do sistema.';
                    break;
            }
        }

        echo json_encode(compact('error', 'msg', 'exception', 'code'));
        exit;
    }

    public function removeAcentuacao($param)
    {
        $temp = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $param));
        return $temp;
    }
}
