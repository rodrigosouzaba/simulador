<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * AreasComercializacoes Controller
 *
 * @property \App\Model\Table\AreasComercializacoesTable $AreasComercializacoes
 */
class AreasComercializacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Operadoras']
        ];
        $areasComercializacoes = $this->paginate($this->AreasComercializacoes);

        $this->set(compact('areasComercializacoes'));
        $this->set('_serialize', ['areasComercializacoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Areas Comercializaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $areasComercializaco = $this->AreasComercializacoes->get($id, [
            'contain' => ['Operadoras']
        ]);

        $this->set('areasComercializaco', $areasComercializaco);
        $this->set('_serialize', ['areasComercializaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $areasComercializaco = $this->AreasComercializacoes->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['municipios'] = $this->getPostMunicipios();
            $areasComercializaco = $this->AreasComercializacoes->patchEntity($areasComercializaco, $data);
            $areaComercializacao = $this->AreasComercializacoes->save($areasComercializaco);

            if ($areaComercializacao) {
                $this->Flash->success(__('Area de Comercialização salva com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Area de Comercialização não pode ser salva. Tente Novamente.'));
            }
        }

        $operadoras = $this->AreasComercializacoes->Operadoras
            ->find('list', ['valueField' => function ($e) {
                return $e->nome . ' - ' . $e->detalhe;
            }])
            ->orderAsc('nome')
            ->where([
                'OR' => [
                    'status IS NOT' => 'INATIVA',
                    'status IS' => null
                ],
                'nova' => 1
            ]);
        $this->loadModel('Estados');
        $estados = $this->Estados
            ->find('all')
            ->contain([
                'Municipios' => ['sort' => ['nome' => 'ASC']],
                'Metropoles' => ['sort' => ['nome' => 'ASC']]
            ])
            ->toArray();
        // $metropoles = $this->AreasComercializacoes->Metropoles->find('all')->contain('Municipios')->toArray();

        $this->set(compact('areasComercializaco', 'abrangencias', 'estados', 'operadoras', 'metropoles'));
        $this->set('_serialize', ['areasComercializaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Areas Comercializaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $areasComercializaco = $this->AreasComercializacoes->get($id, [
            'contain' => [
                'Municipios' => ['sort' => ['nome' => 'ASC']],
                'Metropoles' => ['sort' => ['nome' => 'ASC']]
            ]
        ]);
        $locaisSalvos['municipios'] = [];
        $locaisSalvos['metropoles'] = [];
        foreach ($areasComercializaco->municipios as $municipio) {
            $locaisSalvos['municipios'][$municipio->id] = $municipio->estado_id;
        }
        foreach ($areasComercializaco->metropoles as $metropole) {
            $locaisSalvos['metropoles'][$metropole->id] = $metropole->estado_id;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            unset($data['municipios']);
            $metropoles = $data['metropoles'];
            unset($data['metropoles']);
            $data['municipios'] = $this->getPostMunicipios();

            // ORDER METROPOLES
            foreach ($metropoles as $estado => $metropoles) {
                foreach ($metropoles as $metropole) {
                    $data['metropoles'][] = [
                        'id' => $metropole,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }

            $areasComercializaco = $this->AreasComercializacoes->patchEntity($areasComercializaco, $data);
            $areasComercializaco = $this->AreasComercializacoes->save($areasComercializaco);
            if ($areasComercializaco) {
                $this->Flash->success(__('The areas comercializaco has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The areas comercializaco could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('AreasComercializacoes');
        $operadoras = $this->AreasComercializacoes->Operadoras
            ->find('list', ['valueField' => function ($e) {
                return $e->nome . ' - ' . $e->detalhe;
            }])
            ->orderAsc('nome')
            ->where([
                'OR' => [
                    'status IS NOT' => 'INATIVA',
                    'status IS' => null
                ],
                'nova' => 1
            ]);
        $this->loadModel('Estados');
        $estados = $this->Estados
            ->find('all')
            ->contain([
                'Municipios' => ['sort' => ['nome' => 'ASC']],
                'Metropoles' => ['sort' => ['nome' => 'ASC']]
            ])
            ->toArray();
        $this->set(compact('abrangencias', 'areasComercializaco', 'estados', 'locaisSalvos', 'operadoras'));
        $this->set('_serialize', ['areasComercializaco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Areas Comercializaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $areasComercializaco = $this->AreasComercializacoes->get($id);
        if ($this->AreasComercializacoes->delete($areasComercializaco)) {
            $this->Flash->success(__('The areas comercializaco has been deleted.'));
        } else {
            $this->Flash->error(__('The areas comercializaco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function duplicar(int $id = null)
    {
        $this->request->allowMethod(['post']);
        $areasComercializaco = $this->AreasComercializacoes->get($id, ['contain' => ['Municipios']]);
        $data = clone $areasComercializaco;
        unset($data->id);
        $data->new = true;
        $data->created = null;
        $data->modified = null;
        $data = $data->toArray();

        $new_area = $this->AreasComercializacoes->newEntity();
        $new_area = $this->AreasComercializacoes->patchEntity($new_area, $data, ['associated' => ['Municipios']]);

        if ($this->AreasComercializacoes->save($new_area)) {
            $this->Flash->success(__('Area de Comercialização duplicada com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao Duplicar Area de Comercialização.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getPostMunicipios()
    {
        $f = file_get_contents("php://input");
        $municipios = [];
        foreach (explode('&', urldecode($f)) as $item) {
            if (substr($item, 0, 9) == 'municipio' && substr($item, 0, 10) != 'municipios') {
                (int) $estado = str_replace(']', '', substr($item, 10, 2));
                $municipios[] = ['id' => substr($item, strpos($item, '=') + 1), '_joinData' => ['estado_id' => $estado]];
            } elseif (substr($item, 0, 10) == 'municipios') {
                (int) $estado = str_replace(']', '', substr($item, 11, 2));
                $municipios[] = ['id' => substr($item, strpos($item, '=') + 1), '_joinData' => ['estado_id' => $estado]];
            }
        };
        return $municipios;
    }
}
