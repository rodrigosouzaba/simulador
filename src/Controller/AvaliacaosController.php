<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Event\Event;

/**
 * Avaliacaos Controller
 *
 * @property \App\Model\Table\AvaliacaosTable $Avaliacaos */
class AvaliacaosController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.

        $this->Auth->allow(['save']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
            'order' => ['id' => 'desc']
        ];
        $avaliacaos = $this->paginate($this->Avaliacaos);
        $avaliacao = $this->Avaliacaos->newEntity();
        $session = $this->request->getSession()->write(['Auth.User.new_interacoes' => '0']);

        $this->set(compact('avaliacaos', 'avaliacao'));
        $this->set('_serialize', ['avaliacaos']);
    }

    /**
     * View method
     *
     * @param string|null $id Avaliacao id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $avaliacao = $this->Avaliacaos->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('avaliacao', $avaliacao);
        $this->set('_serialize', ['avaliacao']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $avaliacao = $this->Avaliacaos->newEntity();
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $data['user_id'] = $sessao['id'];
            $lastAvaliations = $this->Avaliacaos->find('list')->where(['user_id' => $sessao['user_id'], 'created > DATE_SUB(NOW(), INTERVAL 6 HOUR)', 'nota' => 99]);
            // BLOQQUEIA NOVAS SOLICITAÇÕES DE CONTATO POR 6 HORAS
            if ($lastAvaliations->count() == 0) {
                $firstAvaliation = $this->Avaliacaos->find('list')->where(['user_id' => $sessao['user_id'], 'created > DATE_SUB(NOW(), INTERVAL 5 MINUTE)'])->orderDesc('id')->first();
                // EVITA A CRIAÇÃO DE NOVA AVALIAÇÃO A CADA EDIÇÃO
                if (!is_null($firstAvaliation)) {
                    $avaliacao = $this->Avaliacaos->get($firstAvaliation);
                }
                $avaliacao = $this->Avaliacaos->patchEntity($avaliacao, $data);
                if ($this->Avaliacaos->save($avaliacao)) {

                    if ($avaliacao->nota == 99) {
                        $this->informarSolicitacao($avaliacao->user_id, $avaliacao->tipo);
                    }

                    $this->Flash->success(__('A avaliação foi salva.'));
                    return $this->redirect(['controller' => 'users', 'action' => 'central']);
                } else {
                    $this->Flash->error(__('A avaliação não pode ser salva, tente novamente.'));
                }
            }
        }

        $this->set(compact('avaliacao', 'sessao'));
        $this->set('_serialize', ['avaliacao']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Avaliacao id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $avaliacao = $this->Avaliacaos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $avaliacao = $this->Avaliacaos->patchEntity($avaliacao, $this->request->data);
            if ($this->Avaliacaos->save($avaliacao)) {
                $this->Flash->success(__('Agradecemos pela colaboração.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The avaliacao could not be saved. Please, try again.'));
            }
        }
        $users = $this->Avaliacaos->Users->find('list', ['limit' => 200]);
        $this->set(compact('avaliacao', 'users'));
        $this->set('_serialize', ['avaliacao']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Avaliacao id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $avaliacao = $this->Avaliacaos->get($id);
        if ($this->Avaliacaos->delete($avaliacao)) {
            $this->Flash->success(__('The avaliacao has been deleted.'));
        } else {
            $this->Flash->error(__('The avaliacao could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function save()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {

            $session = $this->request->session();
            $sessao = $session->read('Auth.User');

            $retorno['ok'] = true;
            $retorno['msg'] = 'Salvo com sucesso';

            $dados = $this->request->data;

            if (isset($sessao['id'])) {
                $dados['user_id'] = $sessao['id'];
            } else {
                $dados['visitante'] = 1;
            }

            $this->loadModel('Avaliacaos');
            $avaliacao = $this->Avaliacaos->newEntity();
            $avaliacao = $this->Avaliacaos->patchEntity($avaliacao, $dados);

            if (!$this->Avaliacaos->save($avaliacao)) {
                $retorno['ok'] = false;
                $retorno['msg'] = 'Não foi possivel salvar o registro';
            }

            $this->response->type('json');
            $this->response->body(json_encode($retorno));
        }
    }

    public function filtroTipo()
    {
        $this->paginate = [
            'conditions' => [
                'tipo' => $this->request->data['tipo'],
            ]
        ];
        // $avaliacaos = $this->Avaliacaos->find('all')->contain(['Users'])->where(['Avaliacaos.tipo' => $this->request->data['tipo']])->toArray();
        $avaliacaos = $this->paginate($this->Avaliacaos);
        $this->set(compact('avaliacaos'));
        // $this->viewBuilder('ajax');
        $this->render('Index');
    }

    public function responder($id)
    {
        $avaliacao = $this->Avaliacaos->get($id, ['contain' => 'Users']);
        $avaliacao->resposta = $this->request->data('resposta');
        $email = new Email('avaliacaos');
        $email->to($avaliacao->user->email)->subject('Resposta à Avaliação');
        $email->send($this->request->data('resposta'));
        $this->Avaliacaos->save($avaliacao);
        $this->redirect(['action' => 'Index']);
    }

    public function getUSer($id)
    {
        $this->loadModel('Users');
        if (strlen($id) == 14) {
            $user = $this->Users->find('all')->contain(['Estados', 'Municipios'])->where(['username' => $id])->first()->toArray();
        } else {
            $user = $this->Users->get($id, ['contain' => ['Estados', 'Municipios']]);
        }
        $this->set(compact('user'));
    }

    public function informarSolicitacao($user, $tipo)
    {

        $user = $this->Avaliacaos->Users->get($user, ['contain' => ['Imagens', 'Estados']]);

        $msg = "
            <html>
            O usuário abaixo solicita contato.
            <br/>
            <br/>
            " . $user->nome . " " . $user->sobrenome . "<br/>
            " . $user->username . "<br/>
            " . $user->estado->nome . "<br/>
            " . $user->celular . "<br/>
            " . $user->email . "<br/>
            <br/>
            <img src='https://corretorparceiro.com.br/app/uploads/imagens/usuarios/logos/" . rawurlencode($user->imagen->nome) . "' width='230'><br/>
            </html>
            ";

        $email = new Email('avaliacaos');
        $email->to('comercial@natuseg.com.br')
            ->subject($tipo)
            ->emailFormat('html')
            ->send($msg);

        return;
    }
}
