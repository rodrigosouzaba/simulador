<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * BannerImagens Controller
 *
 * @property \App\Model\Table\BannerImagensTable $BannerImagens */
class BannerImagensController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $bannerImagens = $this->paginate($this->BannerImagens);

        $this->set(compact('bannerImagens'));
        $this->set('_serialize', ['bannerImagens']);
    }

    /**
     * View method
     *
     * @param string|null $id Banner Imagen id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bannerImagen = $this->BannerImagens->get($id, [
            'contain' => []
        ]);

        $this->set('bannerImagen', $bannerImagen);
        $this->set('_serialize', ['bannerImagen']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bannerImagen = $this->BannerImagens->newEntity();
        $banners = $this->BannerImagens->Banners->find('list', ['valueField' => 'local'])->toArray();
        if ($this->request->is('post')) {
            $dados = $this->request->data();
            $nome = substr($dados['imagem']['name'], 0, -4) . "_";
            $extensao = substr($dados['imagem']['name'], -4);
            //FAZENDO UPLOAD DA IMAGEM

            $local = "img/banners/imagens/" . $nome . date("dmyhis") . $extensao;
            $temp = $dados['imagem']['tmp_name'];
            $this->request->data['nome'] = $nome . date("dmyhis") . $extensao;
            if ($extensao == ".png" || $extensao == ".jpeg" || $extensao == ".jpg" || $extensao == ".gif") {
                move_uploaded_file($temp, $local);
                $bannerImagen = $this->BannerImagens->patchEntity($bannerImagen, $this->request->data);
                if ($this->BannerImagens->save($bannerImagen)) {
                    $this->Flash->success(__('Nova imagem adicionada.'));

                    return $this->redirect(['controller' => 'Banners', 'action' => 'view', $this->request->data['banner_id']]);
                } else {
                    $this->Flash->error(__('The banner imagen could not be saved. Please, try again.'));
                }
            }
        }
        $this->set(compact('bannerImagen', 'banners'));
        $this->set('_serialize', ['bannerImagen']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Banner Imagen id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bannerImagen = $this->BannerImagens->get($id, [
            'contain' => ['Banners']
        ]);
        $this->loadModel("Banners");
        $banners = $this->Banners->find('list', ['keyField' => 'id', 'valueField' => 'local']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bannerImagen = $this->BannerImagens->patchEntity($bannerImagen, $this->request->data);
            if ($this->BannerImagens->save($bannerImagen)) {
                $this->Flash->success(__('Banner Editado Com sucesso.'));

                return $this->redirect(['controller' => 'Banners', 'action' => 'view', $this->request->data['banner_id']]);
            } else {
                $this->Flash->error(__('The banner imagen could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('bannerImagen', 'banners'));
        $this->set('_serialize', ['bannerImagen']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Banner Imagen id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bannerImagen = $this->BannerImagens->get($id);
        $imagem = $bannerImagen['nome'];
        if ($this->BannerImagens->delete($bannerImagen)) {
            unlink("img/banners/imagens/" . $imagem);
            $this->Flash->success(__('Imagem excluida com sucesso.'));
        } else {
            $this->Flash->error(__('The banner imagen could not be deleted. Please, try again.'));
        }
        return $this->redirect(['controller' => 'Banners', 'action' => 'view', $bannerImagen['banner_id']]);
    }
}
