<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Campanhas Controller
 *
 * @property \App\Model\Table\CampanhasTable $Campanhas
 */
class CampanhasController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $campanhas = $this->paginate($this->Campanhas);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->set(compact('campanhas', 'sessao'));
        $this->set('_serialize', ['campanhas']);
    }

    /**
     * View method
     *
     * @param string|null $id Campanha id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $campanha = $this->Campanhas->get($id, [
            'contain' => []
        ]);

        $this->set('campanha', $campanha);
        $this->set('_serialize', ['campanha']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $uploadData = '';
        $campanha = $this->Campanhas->newEntity();
        if ($this->request->is('post')) {
//            debug($this->request->data);
//            die();

            if (!empty($this->request->data['url'])) {
                if ($this->request->data['url']['type'] === 'image/png' ||
                        $this->request->data['url']['type'] === 'image/jpeg' ||
                        $this->request->data['url']['type'] === 'image/jpg' ||
                        $this->request->data['url']['type'] === 'image/gif') {
                    $fileName = $this->request->data['url']['name'];
                    $uploadPath = 'uploads/imagens/campanhas/';
                    $uploadFile = $uploadPath . $fileName;

                    if (move_uploaded_file($this->request->data['url']['tmp_name'], $uploadFile)) {
                        $uploadData = $this->Campanhas->newEntity();
                        $uploadData->nome = $this->request->data['nome'];
                        $uploadData->url = $uploadFile;
                        $uploadData->tipo_campanha = $this->request->data['tipo_campanha'];
                        $uploadData->descricao = $this->request->data['descricao'];

                        if ($this->Campanhas->save($uploadData)) {
                            $this->Flash->success(__('Campanha criada com sucesso.'));
                            return $this->redirect(['action' => 'index']);
                        } else {
                            $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                        }
                    } else {
                        $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                    }
                } else {
                    $this->Flash->error(__('Formato de arquivo não permitido. Formatos permitidos são: PNG, JPG, JPEG ou GIF.'));
                }
            }
//            $campanha = $this->Campanhas->patchEntity($campanha, $this->request->data);
//            if ($this->Campanhas->save($campanha)) {
//                $this->Flash->success(__('Salva com sucesso.'));
//
//                return $this->redirect(['action' => 'index']);
//            } else {
//                $this->Flash->error(__('Erro ao salvar campanha. Tente novamente.'));
//            }
        }
        $this->set('uploadData', $uploadData);
        $this->set(compact('campanha'));
        $this->set('_serialize', ['campanha']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Campanha id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $campanha = $this->Campanhas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $campanha = $this->Campanhas->patchEntity($campanha, $this->request->data);
            if ($this->Campanhas->save($campanha)) {
                $this->Flash->success(__('The campanha has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The campanha could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('campanha'));
        $this->set('_serialize', ['campanha']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Campanha id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $campanha = $this->Campanhas->get($id);
        if ($this->Campanhas->delete($campanha)) {
            $this->Flash->success(__('The campanha has been deleted.'));
        } else {
            $this->Flash->error(__('The campanha could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /*
     * 
     * 
     * 
     * Gerar campanhas 
     * 
     *     
     *  
     */

    public function gerarcampanha($id = null) {
        $campanha = $this->Campanhas->get($id);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
//        debug($sessao);
//        die();

        if (isset($sessao['imagem']['nome'])) {
            //Tipo do arquivo da logo do corretor
            $formato = substr($sessao['imagem']['nome'], strpos($sessao['imagem']['nome'], ".") + 1);
            switch ($formato) {
                case 'jpeg':
                case 'jpg':
                case 'JPEG':
                case 'JPG':
                    $logocorretor = imagecreatefromjpeg(WWW_ROOT . $sessao['imagem']['caminho'] . $sessao['imagem']['nome']);
                    break;
                case 'png':
                case 'PNG':
                    $logocorretor = imagecreatefrompng(WWW_ROOT . $sessao['imagem']['caminho'] . $sessao['imagem']['nome']);
                    break;
                case 'gif':
                case 'GIF':
                    $logocorretor = imagecreatefromgif(WWW_ROOT . $sessao['imagem']['caminho'] . $sessao['imagem']['nome']);
                    break;
            }
            $data = getimagesize(WWW_ROOT . $sessao['imagem']['caminho'] . $sessao['imagem']['nome']);
            $width = $data[0];
            $height = $data[1];
//            debug($width);
//            debug($height);
//            die();
//            $ratio = ($height / $width);
//            $width = 130;
//            $height = ($width * $ratio);
////////////////////////////////////////////////////////// File and new size
            if ($width >= 120) {
                $percent = 25000 / $width / 100;
                $newwidth = $width * $percent;
                $newheight = $height * $percent;
                $margemesquerdalogo = 20;
            } else {
                $newwidth = $width;
                $newheight = $height;
                $margemesquerdalogo = 90;
            }

            // Load
            $thumb = imagecreatetruecolor($newwidth, $newheight);


//            $image_p = imagecreatetruecolor($width, $height);
//            $image = imagecreatefromjpeg($filename);
//            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            // Resize
            $whiteBackground = imagecolorallocate($thumb, 255, 255, 255);
            imagefill($thumb, 0, 0, $whiteBackground);

            imagecopyresampled($thumb, $logocorretor, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

//            $background = imagecreatetruecolor($width, $height); //create the background 130x130
            // Output
////////////////////////////////////////////////////////
//            $new_image = imagecreatetruecolor($width, $height);
//            imagecopyresampled($new_image, $logocorretor, 0, 0, 0, 0, $width, $height, $width, $height);
//            $logocorretor = $new_image;
        }


        $imagemcampanha = imagecreatefromjpeg(WWW_ROOT . $campanha['url']);
        $data2 = getimagesize(WWW_ROOT . $campanha['url']);
        $larguraretangulo = 0.8 * $data2[0];
        $margemesquerda = 0.1 * $data2[0];
        $alturaretangulo = 0.15 * $data2[1];
        $margemtop = 0.83 * $data2[1];
        
        
        header('Content-type: image/jpeg');
        
        //Criar Retangulo 
        $image = imagecreatetruecolor($larguraretangulo, $alturaretangulo);
        // Define cor do retangulo
        $white = imagecolorallocate($image, 255, 255, 255);
        //Preenche Ratangulo com cor
        imagefill($image, 0, 0, $white);
        //Coloca retangulo na imagem da campanha. Parametros (Imagem_destino, imagem_origem, margem_esquerda, margem_topo, )
        imagecopymerge($imagemcampanha, $image, $margemesquerda, $margemtop, 0, 0, $larguraretangulo, $alturaretangulo, 50);
        
        
        $cordafonte = imagecolorallocate($imagemcampanha, 0, 0, 0);
        $fontetexto = WWW_ROOT . 'uploads/imagens/campanhas/Arial.TTF';
        if (isset($logocorretor)) {
            imagecopymerge($imagemcampanha, $thumb, $margemesquerdalogo, 850, 0, 0, $newwidth, $newheight, 100);
        }


        imagettftext($imagemcampanha, 20, 0, 300, 850, $cordafonte, $fontetexto, $sessao['nome'] . " " . $sessao['sobrenome']);
        imagettftext($imagemcampanha, 16, 0, 300, 1100, $cordafonte, $fontetexto, $sessao['celular'] . " / " . $sessao['whatsapp'] . " - Whatsapp");
        imagettftext($imagemcampanha, 16, 0, 300, 1125, $cordafonte, $fontetexto, $sessao['email']);
        imagettftext($imagemcampanha, 16, 0, 300, 1150, $cordafonte, $fontetexto, $sessao['site']);
        imagettftext($imagemcampanha, 16, 0, 300, 1175, $cordafonte, $fontetexto, $sessao['facebook']);
// Output and free from memory
        imagejpeg($imagemcampanha, null, 100);
        imagedestroy($imagemcampanha);
        if (isset($logocorretor)) {
            imagedestroy($logocorretor);
        }
    }

}
