<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cnpjs Controller
 *
 * @property \App\Model\Table\CnpjsTable $Cnpjs
 */
class CnpjsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $cnpjs = $this->paginate($this->Cnpjs);

        $this->set(compact('cnpjs'));
        $this->set('_serialize', ['cnpjs']);
    }

    /**
     * View method
     *
     * @param string|null $id Cnpj id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cnpj = $this->Cnpjs->get($id, [
            'contain' => ['Tabelas']
        ]);

        $this->set('cnpj', $cnpj);
        $this->set('_serialize', ['cnpj']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cnpj = $this->Cnpjs->newEntity();
        if ($this->request->is('post')) {
            $cnpj = $this->Cnpjs->patchEntity($cnpj, $this->request->data);
            if ($this->Cnpjs->save($cnpj)) {
                $this->Flash->success(__('The cnpj has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cnpj could not be saved. Please, try again.'));
            }
        }
        $tabelas = $this->Cnpjs->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('cnpj', 'tabelas'));
        $this->set('_serialize', ['cnpj']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cnpj id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cnpj = $this->Cnpjs->get($id, [
            'contain' => ['Tabelas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cnpj = $this->Cnpjs->patchEntity($cnpj, $this->request->data);
            if ($this->Cnpjs->save($cnpj)) {
                $this->Flash->success(__('The cnpj has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cnpj could not be saved. Please, try again.'));
            }
        }
        $tabelas = $this->Cnpjs->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('cnpj', 'tabelas'));
        $this->set('_serialize', ['cnpj']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cnpj id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cnpj = $this->Cnpjs->get($id);
        if ($this->Cnpjs->delete($cnpj)) {
            $this->Flash->success(__('The cnpj has been deleted.'));
        } else {
            $this->Flash->error(__('The cnpj could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
