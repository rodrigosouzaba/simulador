<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Core\App;

use ReflectionClass;
use ReflectionMethod;

// https://en.it1352.com/article/af2bff533ced420f9e5dec13db1a0a9c.html
/**
 * CakePHP controller_list
 * @author Anderson 
 */
class ControllerListComponent extends Component {
    
    public function get() {
        
        // PEGA TODAS OS CONTROLLERS EXISTENTES
        $controllerClasses = scandir('../src/Controller/');
        $controllers = [];
        $ignoreList = [
            '.', 
            '..', 
            '.DS_Store', 
            'Component', 
            'Api', 
            'AppController.php',
        ];

        foreach($controllerClasses as $file){
            if(!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                array_push($controllers, $this->getActions($controller));
            }            
        }
        
        return $controllers;  
    }

    public function getActions($controllerName) {
    
        $className = 'App\\Controller\\'.$controllerName;
        $class = new ReflectionClass($className);
        $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $controllerNameClear = str_replace("Controller", "", $controllerName);
        $results = [$controllerNameClear => []];
        $ignoreList = ['beforeFilter', 'afterFilter', 'initialize'];
        foreach($actions as $action){
            if($action->class == $className && !in_array($action->name, $ignoreList)){
                array_push($results[$controllerNameClear], $action->name);
            }   
        }
        return $results;
    }
}
