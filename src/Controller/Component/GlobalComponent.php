<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GlobalComponent
 *
 * @author rodrigo
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class GlobalComponent extends Component
{

    /**
     * Filtro por operadora
     *
     * @param string|null $id_operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id_operadora = null, $nome_chave_operadora = null, $nome_model = null)
    {

        $controller = $this->_registry->getController();

        $model = $controller->name;
        $this->$model = TableRegistry::get($model);
        $filtradas = $this->$model->find('all', ['contain' => [$nome_model => ['Imagens']]])
            ->where([$model . "." . $nome_chave_operadora => $id_operadora])
            ->order($model . '.nome')->toArray();
        return $filtradas;
    }

    function move_to_tmp($path)
    {
        $imageExts = ['jpg', 'png', 'gif', 'jpeg'];
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $conteudo = file_get_contents($path);
        $temp = tmpfile();
        if (in_array($type, $imageExts)) {
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($conteudo);
        } else {
            throw new Exception("Função implemantada somente para imagens, complete-a", 1);
        }
        fwrite($temp, $base64);
        $metaDatas = stream_get_meta_data($temp);
        $tmpFilename = $metaDatas['uri'];
        return $tmpFilename;
    }
}
