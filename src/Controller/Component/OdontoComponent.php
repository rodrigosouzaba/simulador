<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use \Cake\ORM\TableRegistry;


class OdontoComponent extends Component
{
    public $controller = '';

    public function startup()
    {
        $this->controller = $this->_registry->getController()->name;
    }

    public function filtroIndex($nova = null, $tipo = null, $estado = null, $operadora = null)
    {
        if (is_null($estado)) {
            if ($nova) {

                $estados =  TableRegistry::get($this->controller)->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('OdontoAreasComercializacoes.OdontoOperadoras', function ($q) use ($tipo) {
                        return $q->where([
                            'tipo_pessoa' => $tipo,
                            'OR' => [
                                ['status <>' => 'INATIVA'],
                                ['status IS' => null]
                            ],
                            'nova' => 1
                        ]);
                    });
            } else {
                $estados = TableRegistry::get($this->controller)->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('OdontoOperadoras', function ($q) use ($tipo) {
                        return $q->where([
                            'tipo_pessoa' => $tipo,
                            'OR' => [
                                ['status <>' => 'INATIVA'],
                                ['status IS' => null]
                            ],
                            'NOT' => 'nova <=> 1'
                        ]);
                    });
            }
            $retorno['estados'] = $estados;
        } elseif (is_null($operadora)) {
            if ((int) $nova) {
                $operadoras = TableRegistry::get($this->controller)->OdontoOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return property_exists($e, 'estado') ? $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome") : $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
                    }])
                    ->innerJoinWith('OdontoAreasComercializacoes.Estados')
                    // ->innerJoinWith('OdontoAreasComercializacoes.Metropoles')
                    ->where([
                        'OR' => [
                            ['Estados.id' => $estado],
                            // ['Metropoles.estado_id' => $estado]
                        ],
                        'OdontoOperadoras.tipo_pessoa' => $tipo,
                        'OdontoOperadoras.nova' => 1
                    ]);
            } else {
                $operadoras = TableRegistry::get($this->controller)->OdontoOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return property_exists($e, 'estado') ? $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome") : $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
                    }])
                    ->where(['estado_id' => $estado, 'tipo_pessoa' => $tipo, 'NOT(status <=> "INATIVA")'])
                    ->orderAsc('nome');
            }
            $retorno['operadoras'] = $operadoras;
        }


        return $retorno;
    }

    public function getEstadosFromNova($nova)
    {
        if ($nova) {
            $estados =  TableRegistry::get($this->controller)->Estados
                ->find('list', ['valueField' => 'nome'])
                ->matching('OdontoAreasComercializacoes.OdontoOperadoras', function ($q) {
                    return $q->where([
                        'OR' => [
                            ['status <>' => 'INATIVA'],
                            ['status IS' => null]
                        ],
                        'nova' => 1
                    ]);
                });
        } else {
            $estados = TableRegistry::get($this->controller)->Estados
                ->find('list', ['valueField' => 'nome'])
                ->matching('OdontoOperadoras', function ($q) {
                    return $q->where([
                        'OR' => [
                            ['status <>' => 'INATIVA'],
                            ['status IS' => null]
                        ],
                        'NOT' => 'nova <=> 1'
                    ]);
                });
        }

        return $estados;
    }

    public function getOperadoras($nova, $tipo)
    {
        $nova = $nova ? ['nova' => 1] : ['OR' => ['nova' => 0, 'nova IS NULL']];
        $operadoras = TableRegistry::get($this->controller)->OdontoOperadoras
            ->find('list', ['valueField' => function ($e) {
                return  $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            }])
            ->where(['OdontoOperadoras.tipo_pessoa' => $tipo, 'NOT(status <=> "INATIVA")', $nova])
            ->order('OdontoOperadoras.nome');


        return $operadoras;
    }
}
