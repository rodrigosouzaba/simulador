<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DetalhesPerfisEmpresariais Controller
 *
 * @property \App\Model\Table\DetalhesPerfisEmpresariaisTable $DetalhesPerfisEmpresariais
 */
class DetalhesPerfisEmpresariaisController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PerfisEmpresariais']
        ];
        $detalhesPerfisEmpresariais = $this->paginate($this->DetalhesPerfisEmpresariais);

        $this->set(compact('detalhesPerfisEmpresariais'));
        $this->set('_serialize', ['detalhesPerfisEmpresariais']);
    }

    /**
     * View method
     *
     * @param string|null $id Detalhes Perfis Empresariai id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $detalhesPerfisEmpresariai = $this->DetalhesPerfisEmpresariais->get($id, [
            'contain' => ['PerfisEmpresariais']
        ]);

        $this->set('detalhesPerfisEmpresariai', $detalhesPerfisEmpresariai);
        $this->set('_serialize', ['detalhesPerfisEmpresariai']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $detalhesPerfisEmpresariai = $this->DetalhesPerfisEmpresariais->newEntity();
        if ($this->request->is('post')) {
            $detalhesPerfisEmpresariai = $this->DetalhesPerfisEmpresariais->patchEntity($detalhesPerfisEmpresariai, $this->request->data);
            if ($this->DetalhesPerfisEmpresariais->save($detalhesPerfisEmpresariai)) {
                $this->Flash->success(__('The detalhes perfis empresariai has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The detalhes perfis empresariai could not be saved. Please, try again.'));
            }
        }
        $perfisEmpresariais = $this->DetalhesPerfisEmpresariais->PerfisEmpresariais->find('list', ['limit' => 200]);
        $this->set(compact('detalhesPerfisEmpresariai', 'perfisEmpresariais'));
        $this->set('_serialize', ['detalhesPerfisEmpresariai']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Detalhes Perfis Empresariai id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $detalhesPerfisEmpresariai = $this->DetalhesPerfisEmpresariais->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $detalhesPerfisEmpresariai = $this->DetalhesPerfisEmpresariais->patchEntity($detalhesPerfisEmpresariai, $this->request->data);
            if ($this->DetalhesPerfisEmpresariais->save($detalhesPerfisEmpresariai)) {
                $this->Flash->success(__('The detalhes perfis empresariai has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The detalhes perfis empresariai could not be saved. Please, try again.'));
            }
        }
        $perfisEmpresariais = $this->DetalhesPerfisEmpresariais->PerfisEmpresariais->find('list', ['limit' => 200]);
        $this->set(compact('detalhesPerfisEmpresariai', 'perfisEmpresariais'));
        $this->set('_serialize', ['detalhesPerfisEmpresariai']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Detalhes Perfis Empresariai id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $detalhesPerfisEmpresariai = $this->DetalhesPerfisEmpresariais->get($id);
        if ($this->DetalhesPerfisEmpresariais->delete($detalhesPerfisEmpresariai)) {
            $this->Flash->success(__('The detalhes perfis empresariai has been deleted.'));
        } else {
            $this->Flash->error(__('The detalhes perfis empresariai could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
