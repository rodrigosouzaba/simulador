<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Filtros Controller
 *
 * @property \App\Model\Table\FiltrosTable $Filtros
 */
class FiltrosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Simulacoes', 'TiposProdutos', 'Tipos', 'Regioes']
        ];
        $filtros = $this->paginate($this->Filtros);

        $this->set(compact('filtros'));
        $this->set('_serialize', ['filtros']);
    }

    /**
     * View method
     *
     * @param string|null $id Filtro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $filtro = $this->Filtros->get($id, [
            'contain' => ['Simulacoes', 'TiposProdutos', 'Tipos', 'TabelasFiltradas']
        ]);

        $this->set('filtro', $filtro);
        $this->set('_serialize', ['filtro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $filtro = $this->Filtros->newEntity();
        if ($this->request->is('post')) {

            foreach ($this->request->data['tabelas'] as $chave => $valor) {
                if ($valor == 0) {
                    unset($this->request->data['tabelas'][$chave]);
                }
            }

            $filtro = $this->Filtros->patchEntity($filtro, $this->request->data);
            if ($this->Filtros->save($filtro)) {
                $this->loadModel('TabelasFiltradas');
                foreach ($this->request->data['tabelas'] as $tabelas_filtradas) {
                    $tabela['filtro_id'] = $filtro->id;
                    $tabela['tabela_id'] = $tabelas_filtradas;
                    $tabelasfiltradas = $this->TabelasFiltradas->newEntity();
                    $tabelasfiltradas = $this->TabelasFiltradas->patchEntity($tabelasfiltradas, $tabela);
                    $this->TabelasFiltradas->save($tabelasfiltradas);
                }
                $this->Flash->success(__('Filtro Salvo com Sucesso.'));
//                debug($this->request->data);
//                die();
                $this->request->session()->write(
                        'filtro_id', $filtro->id
                );
                return $this->redirect(array('controller' => 'simulacoes', 'action' => 'filtro', $filtro->id));
            } else {
                $this->Flash->error(__('Erro ao Executar Filtragem'));
            }
        }
        $simulacoes = $this->Filtros->Simulacoes->find('list', ['limit' => 200]);
        $tiposProdutos = $this->Filtros->TiposProdutos->find('list', ['limit' => 200]);
        $tipos = $this->Filtros->Tipos->find('list', ['limit' => 200]);
        $regioes = $this->Filtros->Regioes->find('list', ['limit' => 200]);
        $this->set(compact('filtro', 'simulacoes', 'tiposProdutos', 'tipos', 'regioes'));
        $this->set('_serialize', ['filtro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Filtro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $filtro = $this->Filtros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $filtro = $this->Filtros->patchEntity($filtro, $this->request->data);
            if ($this->Filtros->save($filtro)) {
                $this->Flash->success(__('The filtro has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The filtro could not be saved. Please, try again.'));
            }
        }
        $simulacoes = $this->Filtros->Simulacoes->find('list', ['limit' => 200]);
        $tiposProdutos = $this->Filtros->TiposProdutos->find('list', ['limit' => 200]);
        $tipos = $this->Filtros->Tipos->find('list', ['limit' => 200]);
        $regioes = $this->Filtros->Regioes->find('list', ['limit' => 200]);
        $this->set(compact('filtro', 'simulacoes', 'tiposProdutos', 'tipos', 'regioes'));
        $this->set('_serialize', ['filtro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Filtro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $filtro = $this->Filtros->get($id);
        if ($this->Filtros->delete($filtro)) {
            $this->Flash->success(__('The filtro has been deleted.'));
        } else {
            $this->Flash->error(__('The filtro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
