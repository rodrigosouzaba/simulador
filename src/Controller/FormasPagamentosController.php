<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * FormasPagamentos Controller
 *
 * @property \App\Model\Table\FormasPagamentosTable $FormasPagamentos
 */
class FormasPagamentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            $conditions_operadoras = [
                'estado_id' => $estado,

            ];
            if ($nova) {
                $operadoras = $this->FormasPagamentos->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            } else {
                $operadoras = $this->FormasPagamentos->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'Operadoras.estado_id' => $estado, 'nova' => 0], 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['Operadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['Operadoras.nova'] =  1;
                $formasPagamentos = $this->FormasPagamentos
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->matching('Operadoras.AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('FormasPagamentos.id')
                    ->orderAsc('Operadoras.prioridade');
            } else {
                $conditions['Operadoras.estado_id'] = $estado;
                $conditions['OR'] = ['Operadoras.nova' => 0, 'Operadoras.nova IS NULL'];
                $formasPagamentos = $this->FormasPagamentos
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->where($conditions)
                    ->orderAsc('Operadoras.prioridade');
            }
        } else {
            $formasPagamentos = $this->FormasPagamentos
                ->find('all')
                ->contain(['Operadoras'])
                ->orderAsc('Operadoras.prioridade');
        }

        $formasPagamentos = $this->paginate($formasPagamentos);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('AreasComercializacoes.Operadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'operadoras', 'alias' => 'Operadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = Operadoras.estado_id']]);
            }
        }
        $this->set(compact('formasPagamentos', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }

    /**
     * View method
     *
     * @param string|null $id Formas Pagamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $formasPagamento = $this->FormasPagamentos->get($id, [
            'contain' => ['Operadoras']
        ]);

        $this->set('formasPagamento', $formasPagamento);
        $this->set('_serialize', ['formasPagamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $formasPagamento = $this->FormasPagamentos->newEntity();
        if ($this->request->is('post')) {
            $formasPagamento = $this->FormasPagamentos->patchEntity($formasPagamento, $this->request->data);
            if ($this->FormasPagamentos->save($formasPagamento)) {
                $this->Flash->success(__('The formas pagamento has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The formas pagamento could not be saved. Please, try again.'));
            }
        }
        $operadoras = $this->FormasPagamentos->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $this->set(compact('formasPagamento', 'operadoras'));
        $this->set('_serialize', ['formasPagamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Formas Pagamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $formasPagamento = $this->FormasPagamentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $formasPagamento = $this->FormasPagamentos->patchEntity($formasPagamento, $this->request->data);
            if ($this->FormasPagamentos->save($formasPagamento)) {
                $this->Flash->success(__('The formas pagamento has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The formas pagamento could not be saved. Please, try again.'));
            }
        }
        $operadoras = $this->FormasPagamentos->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $this->set(compact('formasPagamento', 'operadoras'));
        $this->set('_serialize', ['formasPagamento']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Formas Pagamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $formasPagamento = $this->FormasPagamentos->get($id);
        if ($this->FormasPagamentos->delete($formasPagamento)) {
            $this->Flash->success(__('The formas pagamento has been deleted.'));
        } else {
            $this->Flash->error(__('The formas pagamento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }


    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        //        debug($id);
        $formasPagamentos = $this->FormasPagamentos->find('all', ['contain' => ['Operadoras' => ['Imagens']]])
            ->where(['FormasPagamentos.operadora_id' => $id])
            ->order('FormasPagamentos.nome')->toArray();
        //        debug($produtos);die();
        $operadoras = $this->FormasPagamentos->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $this->set(compact('formasPagamentos', 'operadoras'));
        $this->set('_serialize', ['redes']);
    }

    public function filtroEstado()
    {
        $estado = $this->request->data()['estados'];
        $operadoras = $this->FormasPagamentos->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact('operadoras'));
    }

    function filtroNova($nova)
    {
        $conditions = ['NOT' => ['status <=>' => 'inativa']];
        if ($nova == 1) {
            $conditions[] = ['Operadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['Operadoras.nova IS NULL', 'Operadoras.nova' => 0];
        }
        $this->loadModel('Operadoras');
        $operadoras = $this->Operadoras->find('list', [
            'valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }
}
