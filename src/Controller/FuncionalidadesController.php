<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Funcionalidades Controller
 *
 * @property \App\Model\Table\FuncionalidadesTable $Funcionalidades */
class FuncionalidadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Grupos']
        ];
        $funcoes = $this->paginate($this->Funcionalidades);

        $this->set(compact('funcoes'));
        $this->set('_serialize', ['funcoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Funcionalidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $funcionalidade = $this->Funcionalidades->get($id, [
            'contain' => ['Grupos']
        ]);

        $this->set('funcionalidade', $funcionalidade);
        $this->set('_serialize', ['funcionalidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $funcionalidade = $this->Funcionalidades->newEntity();
        if ($this->request->is('post')) {
            $funcionalidade = $this->Funcionalidades->patchEntity($funcionalidade, $this->request->data);
            if ($this->Funcionalidades->save($funcionalidade)) {
                $this->Flash->success(__('The funcionalidade has been saved.'));

                return $this->redirect(['controller' => 'grupos', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The funcionalidade could not be saved. Please, try again.'));
            }
        }
        $grupos = $this->Funcionalidades->Grupos->find('list', ['keyField' => 'id', 'valueField' => 'nome']);
        $grupos_selecionados = [];
        $this->set(compact('funcionalidade', 'grupos', 'grupos_selecionados'));
        $this->set('_serialize', ['funcionalidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Funcionalidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $funcionalidade = $this->Funcionalidades->get($id, [
            'contain' => ['Grupos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $funcionalidade = $this->Funcionalidades->patchEntity($funcionalidade, $this->request->data);
            if ($this->Funcionalidades->save($funcionalidade)) {
                $this->Flash->success(__('The funcionalidade has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The funcionalidade could not be saved. Please, try again.'));
            }
        }
        $grupos = $this->Funcionalidades->Grupos->find('list', ['keyField' => 'id', 'valueField' => 'nome'])->toArray();
        $this->loadModel('GruposFuncionalidades');
        $grupos_funcionalidade = $this->GruposFuncionalidades->find('all')->where(['funcionalidade_id' => $id])->toArray();
        $grupos_selecionados = array();
        foreach ($grupos_funcionalidade as $funcao_id => $funcao) {
            if (key_exists($funcao->grupo_id, $grupos)) {
                $grupos_selecionados[] = $funcao->grupo_id;
            }
        }
        $this->set(compact('funcionalidade', 'grupos', 'grupos_selecionados'));
        $this->set('_serialize', ['funcionalidade']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Funcionalidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $funcionalidade = $this->Funcionalidades->get($id);
        if ($this->Funcionalidades->delete($funcionalidade)) {
            $this->Flash->success(__('The funcionalidade has been deleted.'));
        } else {
            $this->Flash->error(__('The funcionalidade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
