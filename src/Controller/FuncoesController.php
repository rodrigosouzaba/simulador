<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Funcoes Controller
 *
 * @property \App\Model\Table\FuncoesTable $funcaoes
 */
class FuncoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $funcoes = $this->paginate($this->Funcoes->find("list", ["valueField" => "nome"])->order(["nome" => "ASC"]));

        $this->set(compact('funcoes'));
        $this->set('_serialize', ['funcoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Funco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $funcao = $this->Funcoes->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('funcao', $funcao);
        $this->set('_serialize', ['funcao']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $funcao = $this->Funcoes->newEntity();
        if ($this->request->is('post')) {
            $funcao = $this->Funcoes->patchEntity($funcao, $this->request->data);
            if ($this->Funcoes->save($funcao)) {
                $this->Flash->success(__('The funcao has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The funcao could not be saved. Please, try again.'));
            }
        }
        $users = $this->Funcoes->Users->find('list', ['limit' => 200]);
        $this->set(compact('funcao', 'users'));
        $this->set('_serialize', ['funcao']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Funco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $funcao = $this->Funcoes->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $funcao = $this->Funcoes->patchEntity($funcao, $this->request->data);
            if ($this->Funcoes->save($funcao)) {
                $this->Flash->success(__('The funcao has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The funcao could not be saved. Please, try again.'));
            }
        }
        $users = $this->Funcoes->Users->find('list', ['limit' => 200]);
        $this->set(compact('funcao', 'users'));
        $this->set('_serialize', ['funcao']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Funco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $funcao = $this->Funcoes->get($id);
        if ($this->Funcoes->delete($funcao)) {
            $this->Flash->success(__('The funco has been deleted.'));
        } else {
            $this->Flash->error(__('The funco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    /*
	    
	    Filtro de Usuários por tipo de Funções
	    
    */
    
    public function filtro(){
	    $this->loadModel("UsersFuncoes");
	    $users = $this->paginate($this->UsersFuncoes->find("all")->contain(["Users"])->where(["UsersFuncoes.funcao_id" => $this->request->data["funcao_id"]]));
	    $this->set(compact('users'));
    }
}
