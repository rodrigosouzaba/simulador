<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Grupos Controller
 *
 * @property \App\Model\Table\GruposTable $Grupos
 */
class GruposController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['GrupoPai', 'Users'],
            'order' => [
                'nome' => 'ASC',
                'grupo_pai_id' => 'ASC'
            ]
        ];
        $grupos = $this->paginate($this->Grupos);

        $this->set(compact('grupos', 'funcoes'));
        $this->set('_serialize', ['grupos']);
    }

    /**
     * View method
     *
     * @param string|null $id Grupo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grupo = $this->Grupos->get($id, [
            'contain' => ['GrupoPai']
        ]);

        $this->set('grupo', $grupo);
        $this->set('_serialize', ['grupo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $grupo = $this->Grupos->newEntity();
        if ($this->request->is('post')) {
            $grupo = $this->Grupos->patchEntity($grupo, $this->request->data);
            if ($this->Grupos->save($grupo)) {
                $this->Flash->success(__('Grupo salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar Grupo. Tente novamente.'));
            }
        }

        $this->loadModel('Users');
        $users = $this->Users->find('all')->contain('Estados')->orderASC('Users.nome');
        $usersGrupos = $this->Users->UsersGrupos->find('all')->contain('Grupos');
        foreach ($users as $user) {
            $user->grupos = array();
            foreach ($usersGrupos as $userGrupo) {
                if ($user->id == $userGrupo->user_id) {
                    $user->grupos[$userGrupo->grupo->id] = $userGrupo->grupo->nome;
                }
            }
        }

        $grupos = $this->Grupos->GrupoPai->find('list', ['valueField' => 'nome'])->where(['is_dad' => 'on']);
        $this->set(compact('grupos', 'grupo', 'users'));
        $this->set('_serialize', ['grupo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grupo = $this->Grupos->get($id, [
            'contain' => ['GrupoPai']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dados = $this->request->data;

            //Resolvendo conflito de nome das chaves do Array
            $dados['Users'] = $dados['id'];
            unset($dados['id']);
            //FIM - Resolvendo conflito de nome das chaves do Array
            $users = [];
            foreach ($dados['Users'] as $user) {
                $users[] = $this->Grupos->Users->get($user);
            }

            $grupo = $this->Grupos->patchEntity($grupo, $dados);
            $this->Grupos->Users->link($grupo, $users);
            if ($this->Grupos->save($grupo)) {

                $this->Flash->success(__('Grupo salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar Grupo. Tente novamente.'));
            }
        }

        $grupos = $this->Grupos->GrupoPai->find('list', ['limit' => 200, 'valueField' => 'nome']);

        $this->set(compact('grupo', 'grupos', 'users'));
        $this->set('_serialize', ['grupo']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $grupo = $this->Grupos->get($id);
        if ($this->Grupos->delete($grupo)) {
            $this->Flash->success(__('Grupo excluído com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao excluir Grupo. Tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function removeUser()
    {
        $grupo_id = $this->request->data('grupo');
        $user_id = $this->request->data('user');
        // $grupo = $this->Grupos->get($grupo_id);
        // $user = $this->Grupos->Users->get($user_id);
        $this->loadModel('UsersGrupos');
        $usersGrupos = $this->UsersGrupos->find('list')->where(['user_id' => $user_id, 'grupo_id' => $grupo_id])->first();
        $usersGrupos = $this->UsersGrupos->get($usersGrupos);
        $this->UsersGrupos->delete($usersGrupos);
        $this->autoRender = false;
    }

    public function addUser()
    {
        $grupo_id = $this->request->data('grupo');
        $user_id = $this->request->data('user');
        $grupo = $this->Grupos->get($grupo_id);
        $user = $this->Grupos->Users->get($user_id);
        $gruposUsers = $this->Grupos->Users->link($grupo, [$user]);
        $this->autoRender = false;
    }

    public function getUsers()
    {
        $this->loadModel('VwIndividual');
        $users = $this->VwIndividual->find('all')->select([
            'id',
            'ultimologin',
            'nome',
            'sobrenome',
            'estado',
            'email',
            'grupos',
            'celular',
            'username'
        ])->toArray();

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);

        $this->set(compact('users', 'estados'));
    }
}
