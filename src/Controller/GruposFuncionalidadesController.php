<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GruposFuncionalidades Controller
 *
 * @property \App\Model\Table\GruposFuncionalidadesTable $GruposFuncionalidades */
class GruposFuncionalidadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Grupos', 'Funcionalidades']
        ];
        $gruposFuncionalidades = $this->paginate($this->GruposFuncionalidades);

        $this->set(compact('gruposFuncionalidades'));
        $this->set('_serialize', ['gruposFuncionalidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Grupos Funcionalidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gruposFuncionalidade = $this->GruposFuncionalidades->get($id, [
            'contain' => ['Grupos', 'Funcionalidades']
        ]);

        $this->set('gruposFuncionalidade', $gruposFuncionalidade);
        $this->set('_serialize', ['gruposFuncionalidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gruposFuncionalidade = $this->GruposFuncionalidades->newEntity();
        if ($this->request->is('post')) {
            $gruposFuncionalidade = $this->GruposFuncionalidades->patchEntity($gruposFuncionalidade, $this->request->data);
            if ($this->GruposFuncionalidades->save($gruposFuncionalidade)) {
                $this->Flash->success(__('The grupos funcionalidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The grupos funcionalidade could not be saved. Please, try again.'));
            }
        }
        $grupos = $this->GruposFuncionalidades->Grupos->find('list', ['limit' => 200]);
        $funcionalidades = $this->GruposFuncionalidades->Funcionalidades->find('list', ['limit' => 200]);
        $this->set(compact('gruposFuncionalidade', 'grupos', 'funcionalidades'));
        $this->set('_serialize', ['gruposFuncionalidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupos Funcionalidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gruposFuncionalidade = $this->GruposFuncionalidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gruposFuncionalidade = $this->GruposFuncionalidades->patchEntity($gruposFuncionalidade, $this->request->data);
            if ($this->GruposFuncionalidades->save($gruposFuncionalidade)) {
                $this->Flash->success(__('The grupos funcionalidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The grupos funcionalidade could not be saved. Please, try again.'));
            }
        }
        $grupos = $this->GruposFuncionalidades->Grupos->find('list', ['limit' => 200]);
        $funcionalidades = $this->GruposFuncionalidades->Funcionalidades->find('list', ['limit' => 200]);
        $this->set(compact('gruposFuncionalidade', 'grupos', 'funcionalidades'));
        $this->set('_serialize', ['gruposFuncionalidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupos Funcionalidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gruposFuncionalidade = $this->GruposFuncionalidades->get($id);
        if ($this->GruposFuncionalidades->delete($gruposFuncionalidade)) {
            $this->Flash->success(__('The grupos funcionalidade has been deleted.'));
        } else {
            $this->Flash->error(__('The grupos funcionalidade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
