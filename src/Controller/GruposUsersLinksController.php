<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GruposUsersLinks Controller
 *
 * @property \App\Model\Table\GruposUsersLinksTable $GruposUsersLinks */
class GruposUsersLinksController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Grupos', 'Users']
        ];
        $gruposUsersLinks = $this->paginate($this->GruposUsersLinks);

        $this->set(compact('gruposUsersLinks'));
        $this->set('_serialize', ['gruposUsersLinks']);
    }

    /**
     * View method
     *
     * @param string|null $id Grupos Users Link id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gruposUsersLink = $this->GruposUsersLinks->get($id, [
            'contain' => ['Grupos', 'Users']
        ]);

        $this->set('gruposUsersLink', $gruposUsersLink);
        $this->set('_serialize', ['gruposUsersLink']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gruposUsersLink = $this->GruposUsersLinks->newEntity();
        if ($this->request->is('post')) {
            $gruposUsersLink = $this->GruposUsersLinks->patchEntity($gruposUsersLink, $this->request->data);
            if ($this->GruposUsersLinks->save($gruposUsersLink)) {
                $this->Flash->success(__('The grupos users link has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The grupos users link could not be saved. Please, try again.'));
            }
        }
        $grupos = $this->GruposUsersLinks->Grupos->find('list', ['limit' => 200]);
        $users = $this->GruposUsersLinks->Users->find('list', ['limit' => 200]);
        $this->set(compact('gruposUsersLink', 'grupos', 'users'));
        $this->set('_serialize', ['gruposUsersLink']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupos Users Link id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gruposUsersLink = $this->GruposUsersLinks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gruposUsersLink = $this->GruposUsersLinks->patchEntity($gruposUsersLink, $this->request->data);
            if ($this->GruposUsersLinks->save($gruposUsersLink)) {
                $this->Flash->success(__('The grupos users link has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The grupos users link could not be saved. Please, try again.'));
            }
        }
        $grupos = $this->GruposUsersLinks->Grupos->find('list', ['limit' => 200]);
        $users = $this->GruposUsersLinks->Users->find('list', ['limit' => 200]);
        $this->set(compact('gruposUsersLink', 'grupos', 'users'));
        $this->set('_serialize', ['gruposUsersLink']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupos Users Link id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gruposUsersLink = $this->GruposUsersLinks->get($id);
        if ($this->GruposUsersLinks->delete($gruposUsersLink)) {
            $this->Flash->success(__('The grupos users link has been deleted.'));
        } else {
            $this->Flash->error(__('The grupos users link could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
