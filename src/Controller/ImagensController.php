<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * imagens Controller
 *
 * @property \App\Model\Table\imagensTable $imagens
 */
class ImagensController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {           $this->set('title', 'Imagens');

        $imagens = $this->paginate($this->Imagens);

        $this->set(compact('imagens'));
        $this->set('_serialize', ['imagens']);
    }

    /**
     * View method
     *
     * @param string|null $id Estado id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $imagem = $this->Imagens->get($id);

        $this->set('imagem', $imagem);
        $this->set('_serialize', ['imagem']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $imagem = $this->Imagens->newEntity();
        if ($this->request->is('post')) {
            $imagem = $this->imagens->patchEntity($imagem, $this->request->data);
            if ($this->Imagens->save($imagem)) {
                $this->Flash->success(__('The estado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The estado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('imagem'));
        $this->set('_serialize', ['estado']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Estado id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $imagem = $this->Imagens->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $imagem = $this->Imagens->patchEntity($imagem, $this->request->data);
            if ($this->Imagens->save($imagem)) {
                $this->Flash->success(__('The estado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The estado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('imagem'));
        $this->set('_serialize', ['imagem']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Estado id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $imagem = $this->Imagens->get($id);
        if ($this->Imagens->delete($imagem)) {
            $this->Flash->success(__('The estado has been deleted.'));
        } else {
            $this->Flash->error(__('The estado could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
