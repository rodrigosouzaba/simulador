<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Indicados Controller
 *
 * @property \App\Model\Table\IndicadosTable $Indicados
 */
class IndicadosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $indicados = $this->paginate($this->Indicados);

        $this->set(compact('indicados'));
        $this->set('_serialize', ['indicados']);
    }

    /**
     * View method
     *
     * @param string|null $id Indicado id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $indicado = $this->Indicados->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('indicado', $indicado);
        $this->set('_serialize', ['indicado']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $indicado = $this->Indicados->newEntity();
        if ($this->request->is('post')) {
            $indicado = $this->Indicados->patchEntity($indicado, $this->request->data);
            if ($this->Indicados->save($indicado)) {
                $this->Flash->success(__('The indicado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The indicado could not be saved. Please, try again.'));
            }
        }
        $users = $this->Indicados->Users->find('list', ['limit' => 200]);
        $this->set(compact('indicado', 'users'));
        $this->set('_serialize', ['indicado']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Indicado id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $indicado = $this->Indicados->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $indicado = $this->Indicados->patchEntity($indicado, $this->request->data);
            if ($this->Indicados->save($indicado)) {
                $this->Flash->success(__('The indicado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The indicado could not be saved. Please, try again.'));
            }
        }
        $users = $this->Indicados->Users->find('list', ['limit' => 200]);
        $this->set(compact('indicado', 'users'));
        $this->set('_serialize', ['indicado']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Indicado id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $indicado = $this->Indicados->get($id);
        if ($this->Indicados->delete($indicado)) {
            $this->Flash->success(__('The indicado has been deleted.'));
        } else {
            $this->Flash->error(__('The indicado could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
