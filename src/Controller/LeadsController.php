<?php

namespace App\Controller;

use App\Controller\Api\AppController as ApiAppController;
use App\Controller\AppController;


class LeadsController extends AppController
{

    public function index()
    {
        $controller = $this->request->getParam('controller');
        $action = $this->request->getParam('action');
        $aqui = lcfirst($controller) . '.' . $action;
        $permissions = $this->request->session()->read('Auth.User.Permission');

        if (in_array($aqui, $permissions)) {
            $url = "https://app.leadmark.com.br/login";
            $this->redirect($url);
            $this->set(compact('url'));
        } else {
            $this->redirect(['controller' => 'Users', 'action' => 'central']);
        }
    }
}
