<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Links Controller
 *
 * @property \App\Model\Table\LinksTable $Links
 */
class LinksController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.

        $this->Auth->allow(['view', "pdf", "whatsapp", "email"]);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $links = $this->paginate($this->Links->find('all')->orderDesc('id'));
        // debug($this->Links);
        $this->set(compact('links'));
        $this->set('_serialize', ['links']);
    }

    /**
     * View method
     *
     * @param string|null $id Link id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $link = $this->Links->get($id);
        $dados = $link;

        switch ($link->ramo) {
            case "SPF":

                $data = $this->Links->get($id, ['contain' => [
                    'PfTabelas' => [
                        'sort' => ['PfTabelas.prioridade' => 'ASC'],
                        'conditions' => ['PfTabelas.validade' => 1],
                        'PfAtendimentos',
                        'PfAcomodacoes',
                        'PfCoberturas',
                        'PfFormasPagamentos',
                        'PfCarencias',
                        'PfRedes',
                        'PfReembolsos',
                        'PfDocumentos',
                        'PfDependentes',
                        'PfComercializacoes',
                        'PfObservacoes',
                        'PfEntidadesGrupos' => [
                            'PfEntidades' => [
                                'PfProfissoes' =>  function ($q) {
                                    return $q->order('nome');
                                }
                            ],
                        ],
                        'PfEntidades' => [
                            'PfProfissoes' =>  ['sort' => 'nome']
                        ],
                        'PfAreasComercializacoes' => [
                            'Metropoles',
                            'Municipios' => ['Estados'],
                            'PfOperadoras' => ['Imagens']
                        ]
                    ]
                ]])
                    ->toArray();

                $tabelas_ordenadas = [];
                $area_adicionada = [];
                $grupo_adicionado = [];
                if (!empty($data['pf_tabelas'])) {
                    foreach ($data['pf_tabelas'] as $tabela) {
                        $operadora_id = $tabela['pf_areas_comercializaco']['pf_operadora']['id'];
                        @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['pf_areas_comercializaco']['pf_operadora'];
                        @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['pf_atendimento']['nome']] .=  $tabela['nome'] . ' • ';

                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        if (!in_array($tabela['pf_areas_comercializaco']['id'], $area_adicionada)) {
                            foreach ($tabela['pf_areas_comercializaco']['metropoles'] as $metropole) {
                                @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                            }
                            foreach ($tabela['pf_areas_comercializaco']['municipios'] as $municipio) {
                                @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                            }
                            $area_adicionada[] = $tabela['pf_areas_comercializaco']['id'];
                        }
                        unset($tabela['pf_areas_comercializaco']);

                        // NÃO REPETIÇÃO DOS GRUPOS DE ENTIDADES NAS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['tabelas'] .= $tabela['nome'] . ' • ';
                        if (!empty($tabela['pf_entidades_grupo'])) {
                            if (!in_array($tabela['pf_entidades_grupo']['id'], $grupo_adicionado)) {
                                foreach ($tabela['pf_entidades_grupo']['pf_entidades'] as $entidade) {
                                    @$tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] .= '<strong>' . $entidade['nome'] . ':</strong> ';
                                    foreach ($entidade['pf_profissoes'] as $profissao) {
                                        $tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] .= $profissao['nome'] . ', ';
                                    }
                                    $tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] = substr($tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'], 0, -2) . '; ';
                                }
                                $grupo_adicionado[] = $tabela['pf_entidades_grupo']['id'];
                            }
                        }
                        unset($tabela['pf_entidades_grupo']);

                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['data'] =  nl2br($tabela['pf_observaco']['descricao']);
                        unset($tabela['pf_observaco']);
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['data'] =  nl2br($tabela['pf_rede']['descricao']);
                        unset($tabela['pf_rede']);
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
                        unset($tabela['reembolsoEntity']);
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['data'] =  nl2br($tabela['pf_carencia']['descricao']);
                        unset($tabela['pf_carencia']);
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['data'] =  nl2br($tabela['pf_dependente']['descricao']);
                        unset($tabela['pf_dependente']);
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['data'] =  nl2br($tabela['pf_documento']['descricao']);
                        unset($tabela['pf_documento']);
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['data'] =  nl2br($tabela['pf_formas_pagamento']['descricao']);
                        unset($tabela['pf_formas_pagamento']);

                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['nome'] =  $tabela['nome'];
                        foreach ($tabela['pf_entidades'] as $entidade) {
                            @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .=  '<strong>' . $entidade['nome'] . '</strong>: ';
                            foreach ($entidade['pf_profissoes'] as $profissao) {
                                @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .= $profissao['nome'] . ', ';
                            }
                            @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] = substr($tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'], 0, -2);
                            @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .= '. ';
                        }
                        @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .= '</br>';
                        // unset($tabela['pf_entidade']);

                        $tabela['cobertura'] = $tabela['pf_cobertura'];
                        $tabela['acomodacao'] = $tabela['pf_acomodaco'];

                        @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
                    }

                    $ids_vitalmed = [92];
                    if (in_array($operadora_id, $ids_vitalmed)) {
                        $is_vitalmed = true;
                        $faixa_6 = (new \Cake\Collection\Collection($tabelas_ordenadas[$operadora_id]['tabelas']))
                            ->filter(function ($tabela) {
                                return $tabela['tipo_faixa'] === '6';
                            })
                            ->chunk(5)
                            ->toArray();
                        $faixa_3 = (new \Cake\Collection\Collection($tabelas_ordenadas[$operadora_id]['tabelas']))
                            ->filter(function ($tabela) {
                                return $tabela['tipo_faixa'] === '3';
                            })
                            ->chunk(5)
                            ->toArray();
                        $default = (new \Cake\Collection\Collection($tabelas_ordenadas[$operadora_id]['tabelas']))
                            ->reject(function ($tabela) {
                                return $tabela['tipo_faixa'] === '3' || $tabela['tipo_faixa'] === '6';
                            })
                            ->chunk(5)
                            ->toArray();

                        $tabelas_ordenadas[$operadora_id]['tabelas'] = [];
                        $tabelas_ordenadas[$operadora_id]['tabelas']['faixa_3'] = $faixa_3;
                        $tabelas_ordenadas[$operadora_id]['tabelas']['faixa_6'] = $faixa_6;
                        $tabelas_ordenadas[$operadora_id]['tabelas']['default'] = $default;
                    } else {
                        $tabelas_ordenadas[$operadora_id]['tabelas'] = array_chunk($tabelas_ordenadas[$operadora_id]['tabelas'], 5, true);
                    }

                    $tabelasOrdenadas = $tabelas_ordenadas;

                    $this->loadModel('PfOperadoras');
                    $operadora = $this->PfOperadoras->get($operadora_id, ['contain' => 'Imagens']);
                    $this->set(compact('is_vitalmed', 'dados', 'operadora'));
                } else {
                    $this->Flash->error('Link sem Tabelas');
                    $this->redirect($this->referer());
                }

                break;
            case "SPJ":
                $data = $this->Links->get($id, [
                    'contain' => [
                        'Tabelas' => [
                            'sort' => ['Tabelas.prioridade' => 'ASC'],
                            'conditions' => ['Tabelas.validade' => 1],
                            'Abrangencias',
                            'Tipos',
                            'Coberturas',
                            'Carencias',
                            'Redes',
                            'Observacoes',
                            'Informacoes',
                            'Opcionais',
                            'Reembolsos',
                            'FormasPagamentos',
                            "TabelasCnpjs" => [
                                "Cnpjs"
                            ],
                            'AreasComercializacoes' => [
                                'Metropoles',
                                'Municipios' => ['Estados'],
                                'Operadoras' => ['Imagens']
                            ]
                        ]
                    ]
                ])
                    ->toArray();

                $tabelas_ordenadas = [];
                $area_adicionada = [];
                foreach ($data['tabelas'] as $tabela) {
                    $operadora_id = $tabela['areas_comercializaco']['operadora']['id'];
                    @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['areas_comercializaco']['operadora'];

                    @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['abrangencia']['nome']] .=  $tabela['nome'] . ' • ';

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    if (!in_array($tabela['areas_comercializaco']['id'], $area_adicionada)) {
                        foreach ($tabela['areas_comercializaco']['metropoles'] as $metropoles) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['data'] .= $metropoles['nome'] . ', ';
                        }
                        foreach ($tabela['areas_comercializaco']['municipios'] as $municipio) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                        }
                        $area_adicionada[] = $tabela['areas_comercializaco']['id'];
                    }
                    unset($tabela['areas_comercializaco']);

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['data'] =  nl2br($tabela['observaco']['descricao']);
                    unset($tabela['observaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['data'] =  nl2br($tabela['rede']['descricao']);
                    unset($tabela['rede']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
                    unset($tabela['reembolsoEntity']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['data'] =  nl2br($tabela['carencia']['descricao']);
                    unset($tabela['carencia']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['data'] =  nl2br($tabela['opcionai']['descricao']);
                    unset($tabela['opcionai']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['data'] =  nl2br($tabela['informaco']['descricao']);
                    unset($tabela['informaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['data'] =  nl2br($tabela['formas_pagamento']['descricao']);
                    unset($tabela['formas_pagamento']);

                    $tabela['cobertura'] = $tabela['cobertura'];
                    $tabela['acomodacao'] = $tabela['tipo'];

                    @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
                }
                $tabelas_ordenadas[$operadora_id]['tabelas'] = array_chunk($tabelas_ordenadas[$operadora_id]['tabelas'], 5, true);
                $tabelasOrdenadas = $tabelas_ordenadas;

                $this->loadModel('Operadoras');
                $operadora = $this->Operadoras->get($operadora_id);

                $vidas["min"] = $data["tabelas"][0]["minimo_vidas"];
                $vidas["max"] = $data["tabelas"][0]["maximo_vidas"];

                $dados = $link;
                $ids_vitalmed = [228];
                $is_vitalmed = false;
                if (in_array($operadora['id'], $ids_vitalmed)) {
                    $is_vitalmed = true;
                }

                $this->set(compact('is_vitalmed', 'dados', 'operadora', 'vidas'));
                break;
            case 'OPF':
            case 'OPJ':
                $link = $this->Links->get($id);

                if ($link->new == null) {

                    $link = $this->Links->get($id, [
                        'contain' => [
                            'Tabelas' => ['sort' => ['Tabelas.prioridade' => 'ASC'], 'conditions' => ['Tabelas.validade' => 1], "Produtos", 'Tipos', "TiposProdutos", "TabelasCnpjs" => ["Cnpjs"], "Abrangencias", "Operadoras"],
                            'PfTabelas' => ['sort' => ['PfTabelas.prioridade' => 'ASC'], 'conditions' => ['PfTabelas.validade' => 1], "PfProdutos", "PfAtendimentos", "PfAcomodacoes", "Estados", "PfOperadoras" => ["PfCarencias", "PfRedes", "PfDocumentos"], "PfComercializacoes", "PfCoberturas"],
                            'OdontoTabelas' => [
                                'sort' => ['OdontoTabelas.prioridade' => 'ASC'],
                                'conditions' => ['OdontoTabelas.validade' => 1],
                                'OdontoProdutos' => ["OdontoOperadoras" => [
                                    'Imagens', 'OdontoCarencias', 'OdontoComercializacoes', 'OdontoObservacaos',
                                    'OdontoDocumentos',
                                    'OdontoReembolsos',
                                    'OdontoRedes',
                                    'Estados',
                                    'OdontoDependentes',
                                    'OdontoFormasPagamentos'
                                ]],
                                "OdontoAtendimentos"
                            ]
                        ]
                    ]);
                    $operadora = $this->Links->OdontoTabelas->OdontoProdutos->OdontoOperadoras->find("all")->contain([
                        'Imagens',
                        'OdontoCarencias',
                        'OdontoComercializacoes',
                        'OdontoObservacaos',
                        'OdontoDocumentos',
                        'OdontoReembolsos',
                        'OdontoRedes',
                        'Estados',
                        'OdontoDependentes',
                        'OdontoFormasPagamentos'
                    ])
                        ->where(["OdontoOperadoras.id" => $link["odonto_tabelas"][0]["odonto_produto"]["odonto_operadora"]["id"]])->first();

                    $tabelasDivididas = array_chunk($link["odonto_tabelas"], 8, true);
                    $totalTabelas = count($link["odonto_tabelas"]);
                    foreach ($tabelasDivididas as $chave => $tabelas) {
                        foreach ($tabelas as $tabela) {
                            $tabelasOrdenadas[$chave][$tabela["odonto_produto"]["nome"]][$tabela["descricao"]][$tabela["id"] . "-" .  $tabela["detalhe_coparticipacao"]] = $tabela;
                        }
                    }
                } else {

                    $data = $this->Links->get($id, [
                        'contain' => [
                            'OdontoTabelas' => [
                                'sort' => ['OdontoTabelas.prioridade' => 'ASC'],
                                'conditions' => ['OdontoTabelas.validade' => 1],
                                'OdontoCarencias',
                                'OdontoComercializacoes',
                                'OdontoObservacaos',
                                'OdontoDocumentos',
                                'OdontoReembolsos',
                                'OdontoRedes',
                                'OdontoDependentes',
                                'OdontoFormasPagamentos',
                                "OdontoAtendimentos",
                                'OdontoAreasComercializacoes' => [
                                    'Metropoles',
                                    'Municipios' => ['Estados'],
                                    'OdontoOperadoras' => ['Imagens']
                                ]
                            ]
                        ]
                    ])
                        ->toArray();

                    $tabelas_ordenadas = [];
                    $area_adicionada = [];
                    foreach ($data['odonto_tabelas'] as $tabela) {
                        $operadora_id = $tabela['odonto_areas_comercializaco']['odonto_operadora']['id'];
                        @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['odonto_areas_comercializaco']['odonto_operadora'];
                        @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['odonto_atendimento']['nome']] .=  $tabela['nome'] . ' • ';

                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        if (!empty($tabela['odonto_areas_comercializaco']['metropoles']) || !empty($tabela['odonto_areas_comercializaco']['municipios'])) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                            if (!in_array($tabela['odonto_areas_comercializaco']['id'], $area_adicionada)) {
                                foreach ($tabela['odonto_areas_comercializaco']['metropoles'] as $metropole) {
                                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                                }
                                foreach ($tabela['odonto_areas_comercializaco']['municipios'] as $municipio) {
                                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                                }
                                $area_adicionada[] = $tabela['odonto_areas_comercializaco']['id'];
                            }
                        } else {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'] = '';
                        }
                        unset($tabela['odonto_areas_comercializaco']);

                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        if (!empty($tabela['odonto_observacao']['id'])) {
                            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observacao']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observacao']['id']]['data'] =  nl2br($tabela['odonto_observacao']['descricao']);
                            unset($tabela['odonto_observacao']);
                        }

                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        if (!empty($tabela['odonto_rede']['id'])) {
                            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['data'] =  nl2br($tabela['odonto_rede']['descricao']);
                            unset($tabela['odonto_rede']);
                        }
                        if (!empty($tabela['reembolsoEntity']['id'])) {
                            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
                            unset($tabela['reembolsoEntity']);
                        }
                        if (!empty($tabela['odonto_carencia']['id'])) {
                            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['data'] =  nl2br($tabela['odonto_carencia']['descricao']);
                            unset($tabela['odonto_carencia']);
                        }
                        if (!empty($tabela['odonto_dependente']['id'])) {
                            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['data'] =  nl2br($tabela['odonto_dependente']['descricao']);
                            unset($tabela['odonto_dependente']);
                        }
                        if (!empty($tabela['odonto_documento']['id'])) {
                            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['data'] =  nl2br($tabela['odonto_documento']['descricao']);
                            unset($tabela['odonto_documento']);
                        }
                        if (!empty($tabela['odonto_formas_pagamento']['id'])) {
                            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['data'] =  nl2br($tabela['odonto_formas_pagamento']['descricao']);
                            unset($tabela['odonto_formas_pagamento']);
                        }

                        // $tabela['cobertura'] = $tabela->cobertura;
                        // $tabela['acomodacao'] = $tabela->acomodacao;

                        @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
                    }
                    $tabelas_ordenadas[$operadora_id]['tabelas'] = array_chunk($tabelas_ordenadas[$operadora_id]['tabelas'], 5, true);
                    $tabelasOrdenadas = $tabelas_ordenadas;

                    $this->loadModel('OdontoOperadoras');
                    $operadora = $this->OdontoOperadoras->get($operadora_id, ['contain' => ['Imagens']]);
                }
                break;
        }

        // die;
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');


        $this->set('_serialize', ['link']);
        $this->set('link', $link);
        $nomePDF = $link->nome . ".pdf";

        $this->set(compact("tabelasOrdenadas", "operadora", "nomePDF", "sessao"));
        if ($sessao["role"] <> "admin") {
            $this->viewBuilder()->setLayout('link_tela');
        }
    }


    /**
     * Gerar PDF das tabelas
     *
     * @param string|null $id Link id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function pdf()
    {
        $id = $this->request->data["link_id"];
        $acao = $this->request->data["tipo"];

        $link = $this->Links->get($id, [
            'contain' => [
                'Tabelas' => ["Produtos", 'Tipos', "TiposProdutos", "TabelasCnpjs" => ["Cnpjs"], "Abrangencias", "Operadoras"],
                'PfTabelas' => ["PfProdutos", "PfAtendimentos", "PfAcomodacoes", "Estados", "PfOperadoras" => ["PfCarencias", "PfRedes", "PfDocumentos"], "PfComercializacoes", "TiposProdutos"],
                'OdontoTabelas' => [
                    'OdontoProdutos' => ["OdontoOperadoras" => [
                        'Imagens', 'OdontoCarencias', 'OdontoComercializacoes', 'OdontoObservacaos',
                        'OdontoDocumentos',
                        'OdontoReembolsos',
                        'OdontoRedes',
                        'Estados',
                        'OdontoDependentes',
                        'OdontoFormasPagamentos'
                    ]],
                    "OdontoAtendimentos"
                ]
            ]
        ]);

        $array = $link->toArray();
        // debug($link);
        switch ($link["ramo"]) {
            case "SPF":


                $data = $this->Links->get($id, ['contain' => [
                    'PfTabelas' => [
                        'sort' => ['PfTabelas.prioridade' => 'ASC'],
                        'conditions' => ['PfTabelas.validade' => 1],
                        'PfAtendimentos',
                        'PfAcomodacoes',
                        'PfCoberturas',
                        'PfFormasPagamentos',
                        'PfCarencias',
                        'PfRedes',
                        'PfReembolsos',
                        'PfDocumentos',
                        'PfDependentes',
                        'PfComercializacoes',
                        'PfObservacoes',
                        'PfEntidades' => [
                            'PfProfissoes' =>  ['sort' => 'nome']
                        ],
                        'PfAreasComercializacoes' => [
                            'Municipios' => ['Estados'],
                            'PfOperadoras' => ['Imagens']
                        ]
                    ]
                ]])
                    ->toArray();

                $tabelas_ordenadas = [];

                foreach ($data['pf_tabelas'] as $tabela) {
                    $operadora_id = $tabela['pf_areas_comercializaco']['pf_operadora']['id'];

                    $operadora = $tabela['pf_areas_comercializaco']['pf_operadora'];
                    @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['pf_areas_comercializaco']['pf_operadora'];
                    @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['pf_atendimento']['nome']] .=  $tabela['nome'] . ' • ';

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] =  $tabela['pf_areas_comercializaco']['nome'] . '* Vide Anexo.';
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['municipios'] =  $tabela['pf_areas_comercializaco']['municipios'];
                    unset($tabela['areas_comercializaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['data'] =  $tabela['pf_observaco']['descricao'];
                    unset($tabela['observaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['data'] =  $tabela['pf_rede']['descricao'];
                    unset($tabela['rede']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  $tabela['reembolsoEntity']['descricao'];
                    unset($tabela['reembolsoEntity']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['data'] =  $tabela['pf_carencia']['descricao'];
                    unset($tabela['carencia']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['data'] =  $tabela['pf_dependente']['descricao'];
                    unset($tabela['opcionai']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['data'] =  $tabela['pf_documento']['descricao'];
                    unset($tabela['informaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['data'] =  $tabela['pf_formas_pagamento']['descricao'];
                    unset($tabela['formas_pagamento']);

                    $tabela['cobertura'] = $tabela['pf_cobertura'];
                    $tabela['acomodacao'] = $tabela['pf_acomodaco'];

                    @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
                }
                $tabelas_ordenadas[$operadora_id]['tabelas'] = array_chunk($tabelas_ordenadas[$operadora_id]['tabelas'], 4, true);
                $tabelasOrdenadas = $tabelas_ordenadas;

                $this->set('operadora', $operadora);

                $dados = $link;
                $this->set('dados', $dados);


                break;
            case "SPJ":
                $vidas["min"] = $link["tabelas"][0]["minimo_vidas"];
                $vidas["max"] = $link["tabelas"][0]["maximo_vidas"];

                $operadora = $this->Links->Tabelas->Operadoras->find("all")->contain([
                    'Imagens',
                    'Carencias',
                    'Regioes',
                    'Observacoes',
                    'Informacoes',
                    'Reembolsos',
                    'Redes',
                    'Estados',
                    'Opcionais',
                    'FormasPagamentos'
                ])
                    ->where(["Operadoras.id" => $link["tabelas"][0]["operadora_id"]])
                    ->first();
                $tabelasDivididas = array_chunk($link["tabelas"], 5, true);
                $totalTabelas = count($link["tabelas"]);
                foreach ($tabelasDivididas as $chave => $tabelas) {
                    foreach ($tabelas as $tabela) {
                        switch ($tabela["coparticipacao"]) {
                            case "s":
                                $co = "COPARTICIPAÇÃO " . $tabela["detalhe_coparticipacao"];
                                break;
                            case "n":
                                $co = "SEM COPARTICIPAÇÃO";
                                break;
                            default:
                                $co = "SEM COPARTICIPAÇÃO";
                                break;
                        }
                        $tabelasOrdenadas[$chave][$tabela["produto"]["nome"]][$tabela["tipos_produto"]["nome"]][$tabela["tipo"]["nome"]][$tabela["id"] . "-" . $co] = $tabela;
                    }
                }

                break;

            case "OPF":
            case "OPJ":
                $operadora = $this->Links->OdontoTabelas->OdontoProdutos->OdontoOperadoras->find("all")->contain([
                    'Imagens',
                    'OdontoCarencias',
                    'OdontoComercializacoes',
                    'OdontoObservacaos',
                    'OdontoDocumentos',
                    'OdontoReembolsos',
                    'OdontoRedes',
                    'Estados',
                    'OdontoDependentes',
                    'OdontoFormasPagamentos'
                ])
                    ->where(["OdontoOperadoras.id" => $link["odonto_tabelas"][0]["odonto_produto"]["odonto_operadora"]["id"]])->first();

                $tabelasDivididas = array_chunk($link["odonto_tabelas"], 8, true);
                $totalTabelas = count($link["odonto_tabelas"]);
                foreach ($tabelasDivididas as $chave => $tabelas) {
                    foreach ($tabelas as $chave => $tabela) {
                        $tabelasOrdenadas[$chave][$tabela["odonto_produto"]["nome"]][$tabela["descricao"]][$tabela["id"] . "-" .  $tabela["detalhe_coparticipacao"]] = $tabela;
                    }
                }
                break;
        }

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $nomePDF = $link->nome . ".pdf";
        $nomePDF = transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove', $nomePDF);

        $this->set(compact("tabelasOrdenadas", "link", "totalTabelas", "operadora", "vidas", "entidades", "nomePDF", "sessao", "acao"));
        $this->viewBuilder()->layout('link_pdf');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $link = $this->Links->newEntity();
        if ($this->request->is('post')) {

            /************/
            $dados = $this->request->data();
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            if (isset($dados["vidas"])) {
                $vidas = explode("-", $dados["vidas"]);
            }
            foreach ($dados["Produtos"] as $id => $produto) {
                if ($produto == '0') {
                    unset($dados["Produtos"][$id]);
                }
            }
            $dados["coparticipacao"] = $dados["coparticipacao"] == '0' ? 99 : $dados["coparticipacao"];

            //RAMO SELECIONADO
            switch ($dados["ramo_id"]) {
                case "SPF":
                    $this->loadModel("PfTabelas");
                    if ($dados["coparticipacao"] == 99) {
                        $coparticipacao = "";
                    } else {
                        $coparticipacao = ["PfTabelas.coparticipacao" => $dados["coparticipacao"]];
                    }
                    $tabelas = $this->PfTabelas->find("all")
                        ->contain([
                            "Estados",
                            'PfAcomodacoes',
                            "TiposProdutos",
                            "PfAtendimentos",
                            'PfAreasComercializacoes' => "PfOperadoras"
                        ])
                        ->where(["PfTabelas.id IN" =>  $dados["Produtos"]])
                        ->order(["PfTabelas.prioridade" => "ASC"])
                        ->toArray();

                    switch ($tabelas[0]["modalidade"]) {
                        case "ADESAO":
                            $modalidade = "Saúde Coletivo por Adesão";
                            break;
                        default:
                            $modalidade = "Saúde Individual";
                            break;
                    }

                    switch ($this->request->data["coparticipacao"]) {
                        case "s":
                        case 1:
                            $coparticipacao = "Com Coparticipação - ";
                            break;
                        case "n":
                        case 0:
                            $coparticipacao = "Sem Coparticipação - ";
                            break;
                        case "99":
                            $coparticipacao = "";
                            break;
                    }
                    $this->request->data["nome"] = $tabelas[0]["estado"]["nome"] . " - " . $tabelas[0]['pf_areas_comercializaco']["pf_operadora"]["nome"] . " - " . $modalidade . " - " . $coparticipacao;

                    $this->loadModel("PfEntidadesPfOperadoras");
                    $entidades = $this->PfEntidadesPfOperadoras->find("all")->contain(["PfEntidades" => ["PfEntidadesProfissoes" => ["PfProfissoes"]]])->where(["pf_operadora_id" => $dados["operadora_id"]])->toArray();

                    //DADOS DA OPERADORA
                    $this->loadModel("PfOperadoras");
                    $operadora = $this->PfOperadoras
                        ->find("all")
                        ->contain([
                            'Imagens',
                            'PfCarencias',
                            'PfComercializacoes',
                            'PfObservacoes',
                            'PfDocumentos',
                            'PfReembolsos',
                            'PfRedes',
                            'Estados',
                            'PfDependentes',
                            'PfFormasPagamentos'
                        ])
                        ->where(["PfOperadoras.id" => $dados["operadora_id"]])
                        ->first();
                    break;
                case "SPJ":

                    if ($dados["coparticipacao"] == 99) {
                        $coparticipacao = "";
                    } else {
                        $coparticipacao = array("Tabelas.coparticipacao" => $dados["coparticipacao"]);
                    }
                    $this->loadModel("Tabelas");
                    $tabelas = $this->Tabelas->find("all")
                        ->contain(["Estados", "Operadoras", "Produtos", "TiposProdutos", "Tipos", "Abrangencias"])
                        ->where(["Tabelas.id IN" => $dados["Produtos"]])
                        ->toArray();

                    //NOME DO PDF A SER BAIXADO
                    $coparticipacao = $this->request->data["coparticipacao"];
                    switch ($this->request->data["coparticipacao"]) {
                        case "s":
                        case 1:
                            $coparticipacao = "Com Coparticipação - ";
                            break;
                        case "n":
                        case 0:
                            $coparticipacao = "Sem Coparticipação - ";
                            break;
                        case "99":
                            $coparticipacao = "";
                            break;
                    }
                    $this->request->data["nome"] = $tabelas[0]["estado"]["nome"] . " - " . $tabelas[0]["operadora"]["nome"] . " - Saúde PME - " . $tabelas[0]["minimo_vidas"] . " a " . $tabelas[0]["maximo_vidas"] . " vidas - " . $coparticipacao;

                    //EXCLUSÃO DE TABELAS 3 A 29 CASO SEJA SELECIONADO 4 A 29
                    $bradesco = array(1, 28, 29, 30, 31);
                    if (in_array($dados["operadora_id"], $bradesco)) {
                        foreach ($tabelas as $chave => $tabela) {
                            if ($vidas[0] == 4 && $vidas[1] == 29 && in_array($tabela["operadora_id"], $bradesco)) {
                                if ($tabela["minimo_vidas"] == 3) {
                                    unset($tabelas[$chave]);
                                }
                            }
                        }
                    }


                    //DADOS DA OPERADORA
                    $this->loadModel("Operadoras");
                    $operadora = $this->Operadoras
                        ->find("all")
                        ->contain([
                            'Imagens',
                            'Carencias',
                            'Regioes',
                            'Observacoes',
                            'Informacoes',
                            'Reembolsos',
                            'Redes',
                            'Estados',
                            'Opcionais',
                            'FormasPagamentos'
                        ])
                        ->where(["Operadoras.id" => $dados["operadora_id"]])
                        ->first();
                    break;
                case "OPF":
                case "OPJ":
                    $this->loadModel("OdontoTabelas");
                    $tabelas = $this->OdontoTabelas
                        ->find("all")
                        ->contain([
                            'OdontoCarencias',
                            'OdontoComercializacoes',
                            'OdontoObservacaos',
                            'OdontoDocumentos',
                            'OdontoReembolsos',
                            'OdontoRedes',
                            'OdontoDependentes',
                            'OdontoFormasPagamentos',
                            "OdontoAtendimentos",
                            'OdontoAreasComercializacoes' => [
                                'Municipios' => ['Estados'],
                                'OdontoOperadoras' => ['Imagens']
                            ]
                        ])
                        ->where([
                            "OdontoTabelas.id IN" =>  $dados["Produtos"],
                        ])
                        ->order([
                            "OdontoTabelas.prioridade" => "ASC",
                        ])
                        ->toArray();

                    //NOME DO PDF A SER BAIXADO
                    switch ($tabelas[0]["modalidade"]) {
                        case "ADESAO":
                            $modalidade = "Odonto Coletivo por Adesão";
                            break;
                        default:
                            $modalidade = "Odonto Individual";
                            break;
                    }
                    switch ($this->request->data["coparticipacao"]) {
                        case "s":
                            $coparticipacao = " - Com Coparticipação - ";
                            break;
                        case "n":
                            $coparticipacao = " - Sem Coparticipação - ";
                            break;
                        case "99":
                            $coparticipacao = " - ";
                            break;
                    }

                    $this->request->data["nome"] = $tabelas[0]['odonto_areas_comercializaco']["odonto_operadora"]["nome"] . " - " . $modalidade . " - " . $coparticipacao;

                    //DADOS DA OPERADORA
                    $this->loadModel("OdontoOperadoras");
                    $operadora = $this->OdontoOperadoras
                        ->find("all")
                        ->contain([
                            'Imagens',
                            'OdontoCarencias',
                            'OdontoComercializacoes',
                            'OdontoObservacaos',
                            'OdontoDocumentos',
                            'OdontoReembolsos',
                            'OdontoRedes',
                            'Estados',
                            'OdontoDependentes',
                            'OdontoFormasPagamentos'
                        ])
                        ->where(["OdontoOperadoras.id" => $dados["operadora_id"]])
                        ->first();
                    break;
            }

            $totalTabelas = count($tabelas);

            //Divisão do ARRAY por 8 tabelas no máximo
            $tabelasDivididas = array_chunk($tabelas, 8, true);

            foreach ($tabelasDivididas as $chave => $tabelasD) {
                foreach ($tabelasD as $tabela) {
                    switch ($tabela["coparticipacao"]) {
                        case "s":
                            $co = "COPARTICIPAÇÃO ";
                            break;
                        case "n":
                            $co = "SEM COPARTICIPAÇÃO";
                            break;
                        default:
                            $co = "SEM COPARTICIPAÇÃO";
                            break;
                    }
                    //ORDENACAO DO VETOR DE ACORDO COM O RAMO SELECIONADO
                    switch ($dados["ramo_id"]) {
                        case "SPF":
                            $tabelasOrdenadas[$chave][$tabela["pf_produto"]["nome"]][$tabela["tipos_produto"]["nome"]][$tabela["pf_acomodaco"]["nome"]][$tabela["id"] . "-" . $co . $tabela["detalhe_coparticipacao"]] = $tabela;
                            break;
                        case "SPJ":
                            $tabelasOrdenadas[$chave][$tabela["produto"]["nome"]][$tabela["tipos_produto"]["nome"]][$tabela["tipo"]["nome"]][$tabela["id"] . "-" . $co . $tabela["detalhe_coparticipacao"]] = $tabela;
                            break;

                        case "OPF":
                            $tabelasOrdenadas[$chave][$tabela["odonto_produto"]["nome"]][$tabela["descricao"]][$tabela["id"] . "-" . $co . $tabela["detalhe_coparticipacao"]] = $tabela;
                            break;
                        case "OPJ":
                            $tabelasOrdenadas[$chave][$tabela["odonto_produto"]["nome"]][$tabela["tipo_pessoa"]][$tabela["id"] . "-" . $co . $tabela["detalhe_coparticipacao"]] = $tabela;
                            break;
                    }
                }
            }

            $this->set(compact('vidas', 'entidades', 'operadora', 'sessao', "totalTabelas", "tabelasOrdenadas", "dadosOperadoras", "tabelas", "dados"));

            $this->request->data["usuario"] = $sessao["nome"] . " " . $sessao["sobrenome"];
            $this->request->data["ramo"] = $dados["ramo_id"];

            //API KEY GOOGLE
            $apiKey = "AIzaSyCygKIkHFrhZQk1OMMoKM9KbciH4eLl0tE";


            // This is the URL you want to shorten
            $longUrl = "https://corretorparceiro.com.br/app/links/";

            $postData = array('longUrl' => $longUrl);
            $jsonData = json_encode($postData);

            $curlObj = curl_init();

            curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key=' . $apiKey);
            // 			$curl_obj = curl_init(sprintf('%s/url?key=%s', 'https://www.googleapis.com/urlshortener/v1', $apiKey));
            curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curlObj, CURLOPT_HEADER, 0);
            curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
            curl_setopt($curlObj, CURLOPT_POST, 1);
            curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

            $response = curl_exec($curlObj);

            // Change the response json string to object
            $json = json_decode($response);

            curl_close($curlObj);

            $link = $this->Links->patchEntity($link, $this->request->data);
            if ($this->Links->save($link)) {
                $this->loadModel("LinksTabelas");
                foreach ($tabelas as $tabela) {
                    $links_tabelas["link_id"] = $link->id;
                    switch ($dados["ramo_id"]) {
                        case "SPJ":
                            $campo_tabela = "tabela_id";
                            break;
                        case "SPF":
                            $campo_tabela = "pf_tabela_id";
                            break;
                        default:
                            $campo_tabela = "odonto_tabela_id";
                            break;
                    }
                    $links_tabelas[$campo_tabela] = $tabela["id"];
                    $links_tabelas_entidade = $this->LinksTabelas->newEntity();
                    $links_tabelas_entidade = $this->LinksTabelas->patchEntity($links_tabelas_entidade, $links_tabelas);
                    /*
                                        debug($links_tabelas);
                                        debug($links_tabelas_entidade);
                                        debug($this->LinksTabelas->save($links_tabelas_entidade));
                                        die();
                    */
                    $this->LinksTabelas->save($links_tabelas_entidade);
                }
                $this->Flash->success(__('The link has been saved.'));

                //                 return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The link could not be saved. Please, try again.'));
            }
        }
        $tabelas = $this->Links->Tabelas->find('list', ['limit' => 200, 'conditions' => '1 = 1']);
        $this->set(compact('link', 'tabelas'));
        $this->set('_serialize', ['link']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Link id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $link = $this->Links->get($id, [
            'contain' => [
                'Tabelas'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dados = $this->request->data;
            foreach ($dados['atendimentos'] as $atendimento) {
                if ($atendimento != '') {
                    $this->request->data['atendimentos'] = $atendimento;
                }
            }
            $link = $this->Links->patchEntity($link, $this->request->data);
            if ($this->Links->save($link)) {
                $this->Flash->success(__('The link has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The link could not be saved. Please, try again.'));
            }
        }
        $tabelas = $this->Links->Tabelas->find('list', ['limit' => 200, 'conditions' => '1 = 1']);
        $this->loadModel('Abrangencias');
        $abrangencias = $this->Abrangencias->find('list', ['keyField' => 'nome', 'valueField' => 'nome', 'limit' => 200]);
        $this->loadModel('PfAtendimentos');
        $pfAtendimentos = $this->PfAtendimentos->find('list', ['keyField' => 'nome', 'valueField' => 'nome', 'limit' => 200]);
        $this->loadModel('OdontoAtendimentos');
        $odontoAtendimentos = $this->OdontoAtendimentos->find('list', ['keyField' => 'nome', 'valueField' => 'nome']);
        $this->set(compact('link', 'tabelas', 'abrangencias', 'pfAtendimentos', 'odontoAtendimentos'));
        $this->set('_serialize', ['link']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Link id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $link = $this->Links->get($id);
        if ($this->Links->delete($link)) {
            $this->Flash->success(__('Link da Tabela excluído com sucesso. Remover também do CORRETOR PARCEIRO'));
        } else {
            $this->Flash->error(__('Erro ao excluir o Link da Tabela. Tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function whatsapp()
    {
    }


    /**
     * Enviar link via email
     *
     * @param string|null $id Link id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function email($id = null)
    {
        $this->request->allowMethod(['post', 'get']);
        if ($this->request->is("post")) {
            // debug($this->request->data);
            // debug($id);
            // die;
            $link = $this->Links->get($id);
            switch ($link->ramo) {
                case "SPF":
                    $assunto = "Tabela de Preços - Plano Saúde Pessoa Física";
                    break;
                case "SPJ":
                    $assunto = "Tabela de Preços - Plano Saúde PME";
                    break;
                case "OPF":
                case "OPJ":
                    $assunto = "Tabela de Preços - Plano Odontológico";
                    break;
            }
            // $assunto = "Tabela de Preços - ";
            $msg = "Agradecemos pela oportunidade. Segue abaixo tabela de preços dos planos para análise.<br/>
        Informamos que os valores, regras de comercialização e condições contratuais são determinadas<br/>
        pelas seguradoras/operadoras e podem ser alterados pelas mesmas a qualquer momento.<br/>
        Os preços e condições estão sujeitos a confirmação no ato do fechamento do contrato.<br/><br/><br/><b>
        <a href='https://corretorparceiro.com.br/app/links/view/" . $id . "' target='_blank' style='text-align: center !important; color:#C9302C; font-size:110%'> Clique aqui para visualizar a Tabela de Preços</a></b>
        <br/><br/>";
            $destinatario = $this->request->data["email"];
            $this->loadModel("Users");
            $this->Users->email($msg, $destinatario, $assunto);
        }
    }
}
