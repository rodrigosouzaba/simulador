<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LinksPersonalizados Controller
 *
 * @property \App\Model\Table\LinksPersonalizadosTable $LinksPersonalizados */
class LinksPersonalizadosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['VendasOnlinesOperadoras']
        ];
        $linksPersonalizados = $this->paginate($this->LinksPersonalizados);

        $this->set(compact('linksPersonalizados'));
        $this->set('_serialize', ['linksPersonalizados']);
    }

    /**
     * View method
     *
     * @param string|null $id Links Personalizado id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $linksPersonalizado = $this->LinksPersonalizados->get($id, [
            'contain' => ['VendasOnlinesOperadoras']
        ]);

        $this->set('linksPersonalizado', $linksPersonalizado);
        $this->set('_serialize', ['linksPersonalizado']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $linksPersonalizado = $this->LinksPersonalizados->newEntity();
        if ($this->request->is('post')) {
            $linksPersonalizado = $this->LinksPersonalizados->patchEntity($linksPersonalizado, $this->request->data);
            if ($this->LinksPersonalizados->save($linksPersonalizado)) {
                $this->Flash->success(__('The links personalizado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The links personalizado could not be saved. Please, try again.'));
            }
        }
        $vendasOnlinesOperadoras = $this->LinksPersonalizados->VendasOnlinesOperadoras->find('list', ['limit' => 200]);
        $this->set(compact('linksPersonalizado', 'vendasOnlinesOperadoras'));
        $this->set('_serialize', ['linksPersonalizado']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Links Personalizado id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $linksPersonalizado = $this->LinksPersonalizados->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $linksPersonalizado = $this->LinksPersonalizados->patchEntity($linksPersonalizado, $this->request->data);
            if ($this->LinksPersonalizados->save($linksPersonalizado)) {
                $this->Flash->success(__('The links personalizado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The links personalizado could not be saved. Please, try again.'));
            }
        }
        $vendasOnlinesOperadoras = $this->LinksPersonalizados->VendasOnlinesOperadoras->find('list', ['limit' => 200]);
        $this->set(compact('linksPersonalizado', 'vendasOnlinesOperadoras'));
        $this->set('_serialize', ['linksPersonalizado']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Links Personalizado id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $linksPersonalizado = $this->LinksPersonalizados->get($id);
        if ($this->LinksPersonalizados->delete($linksPersonalizado)) {
            $this->Flash->success(__('The links personalizado has been deleted.'));
        } else {
            $this->Flash->error(__('The links personalizado could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
