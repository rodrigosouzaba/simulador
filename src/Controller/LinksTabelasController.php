<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LinksTabelas Controller
 *
 * @property \App\Model\Table\LinksTabelasTable $LinksTabelas
 */
class LinksTabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Links', 'Tabelas', 'PfTabelas', 'OdontoTabelas']
        ];
        $linksTabelas = $this->paginate($this->LinksTabelas);

        $this->set(compact('linksTabelas'));
        $this->set('_serialize', ['linksTabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Links Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $linksTabela = $this->LinksTabelas->get($id, [
            'contain' => ['Links', 'Tabelas', 'PfTabelas', 'OdontoTabelas']
        ]);

        $this->set('linksTabela', $linksTabela);
        $this->set('_serialize', ['linksTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $linksTabela = $this->LinksTabelas->newEntity();
        if ($this->request->is('post')) {
            $linksTabela = $this->LinksTabelas->patchEntity($linksTabela, $this->request->data);
            if ($this->LinksTabelas->save($linksTabela)) {
                $this->Flash->success(__('The links tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The links tabela could not be saved. Please, try again.'));
            }
        }
        $links = $this->LinksTabelas->Links->find('list', ['limit' => 200]);
        $tabelas = $this->LinksTabelas->Tabelas->find('list', ['limit' => 200]);
        $pfTabelas = $this->LinksTabelas->PfTabelas->find('list', ['limit' => 200]);
        $odontoTabelas = $this->LinksTabelas->OdontoTabelas->find('list', ['limit' => 200]);
        $this->set(compact('linksTabela', 'links', 'tabelas', 'pfTabelas', 'odontoTabelas'));
        $this->set('_serialize', ['linksTabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Links Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $linksTabela = $this->LinksTabelas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $linksTabela = $this->LinksTabelas->patchEntity($linksTabela, $this->request->data);
            if ($this->LinksTabelas->save($linksTabela)) {
                $this->Flash->success(__('The links tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The links tabela could not be saved. Please, try again.'));
            }
        }
        $links = $this->LinksTabelas->Links->find('list', ['limit' => 200]);
        $tabelas = $this->LinksTabelas->Tabelas->find('list', ['limit' => 200]);
        $pfTabelas = $this->LinksTabelas->PfTabelas->find('list', ['limit' => 200]);
        $odontoTabelas = $this->LinksTabelas->OdontoTabelas->find('list', ['limit' => 200]);
        $this->set(compact('linksTabela', 'links', 'tabelas', 'pfTabelas', 'odontoTabelas'));
        $this->set('_serialize', ['linksTabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Links Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $linksTabela = $this->LinksTabelas->get($id);
        if ($this->LinksTabelas->delete($linksTabela)) {
            $this->Flash->success(__('The links tabela has been deleted.'));
        } else {
            $this->Flash->error(__('The links tabela could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
