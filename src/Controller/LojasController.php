<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Lojas Controller
 *
 * @property \App\Model\Table\LojasTable $Lojas
 */
class LojasController extends AppController
{

	/**
	 * Index method
	 *
	 * @return \Cake\Network\Response|null
	 */
	public function index()
	{
		$session = $this->request->session();
		$sessao = $session->read('Auth.User');

		$lojasCompleta = $this->Lojas->find("all")->where(["user_id" => $sessao["id"], "status" => 1])->contain(["Ramos"]);
		//         debug($lojasCompleta->toArray());die();
		$lojas = array();
		foreach($lojasCompleta as $completa){
			$lojas[$completa["ramo"]["nome"]][$completa["tipo_pessoa"]][$completa["id"]] = $completa;
		}
		$this->set(compact('lojas'));
		$this->set('_serialize', ['lojas']);
	}

	/**
	 * View method
	 *
	 * @param string|null $id Loja id.
	 * @return \Cake\Network\Response|null
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$loja = $this->Lojas->get($id, [
			'contain' => ['Ramos', 'Users']
			]);

		$this->set('loja', $loja);
		$this->set('_serialize', ['loja']);
	}

	/**
	 * Add method
	 *
	 * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$loja = $this->Lojas->newEntity();
		if ($this->request->is('post')) {
			$loja = $this->Lojas->patchEntity($loja, $this->request->data);
			if ($this->Lojas->save($loja)) {
				$this->Flash->success(__('The loja has been saved.'));

				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The loja could not be saved. Please, try again.'));
			}
		}
		$ramos = $this->Lojas->Ramos->find('list', ['limit' => 200]);
		$users = $this->Lojas->Users->find('list', ['limit' => 200]);
		$this->set(compact('loja', 'ramos', 'users'));
		$this->set('_serialize', ['loja']);
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Loja id.
	 * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$loja = $this->Lojas->get($id, [
			'contain' => []
			]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$loja = $this->Lojas->patchEntity($loja, $this->request->data);
			if ($this->Lojas->save($loja)) {
				$this->Flash->success(__('The loja has been saved.'));

				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The loja could not be saved. Please, try again.'));
			}
		}
		$ramos = $this->Lojas->Ramos->find('list', ['limit' => 200]);
		$users = $this->Lojas->Users->find('list', ['limit' => 200]);
		$this->set(compact('loja', 'ramos', 'users'));
		$this->set('_serialize', ['loja']);
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Loja id.
	 * @return \Cake\Network\Response|null Redirects to index.
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$loja = $this->Lojas->get($id);
		if ($this->Lojas->delete($loja)) {
			$this->Flash->success(__('The loja has been deleted.'));
		} else {
			$this->Flash->error(__('The loja could not be deleted. Please, try again.'));
		}

		return $this->redirect(['action' => 'index']);
	}
}
