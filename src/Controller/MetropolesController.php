<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Metropoles Controller
 *
 * @property \App\Model\Table\MetropolesTable $Metropoles
 *
 * @method \App\Model\Entity\Metropole[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MetropolesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $metropoles = $this->paginate($this->Metropoles);

        $this->set(compact('metropoles'));
    }

    /**
     * View method
     *
     * @param string|null $id Metropole id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $metropole = $this->Metropoles->get($id, [
            'contain' => ['Municipios'],
        ]);

        $this->set('metropole', $metropole);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $metropole = $this->Metropoles->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $municipios = $data['municipios'];
            unset($data['municipios']);
            foreach ($municipios as $estado => $municipios) {
                foreach ($municipios as $municipio) {
                    $data['municipios'][] = [
                        'id' => $municipio,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }
            $metropole = $this->Metropoles->patchEntity($metropole, $data);
            if ($this->Metropoles->save($metropole)) {
                $this->Flash->success(__('The metropole has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The metropole could not be saved. Please, try again.'));
        }
        $municipios = $this->Metropoles->Municipios->Estados
            ->find('all', ['valueField' => 'nome'])
            ->contain(['Municipios'])
            // ->enableHydration(false)
            ->toArray();
        $estados = $this->Metropoles->Municipios->Estados->find('list', ['valueField' => 'nome']);
        $this->set(compact('metropole', 'municipios', 'estados'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Metropole id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $metropole = $this->Metropoles->get($id, [
            'contain' => ['Municipios'],
        ]);
        $locaisSalvos['municipios'] = [];
        foreach ($metropole->municipios as $municipio) {
            $locaisSalvos['municipios'][$municipio->id] = $municipio->estado_id;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $municipios = $data['municipios'];
            unset($data['municipios']);

            foreach ($municipios as $estado => $municipios) {
                foreach ($municipios as $municipio) {
                    $data['municipios'][] = [
                        'id' => $municipio,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }
            $metropole = $this->Metropoles->patchEntity($metropole, $data);
            if ($this->Metropoles->save($metropole)) {
                $this->Flash->success(__('The metropole has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The metropole could not be saved. Please, try again.'));
        }
        $municipios = $this->Metropoles->Municipios->Estados
            ->find('all', ['valueField' => 'nome'])
            ->contain(['Municipios'])
            ->toArray();
        $estados = $this->Metropoles->Municipios->Estados->find('list', ['valueField' => 'nome']);
        $this->set(compact('metropole', 'municipios', 'estados', 'locaisSalvos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Metropole id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $metropole = $this->Metropoles->get($id);
        if ($this->Metropoles->delete($metropole)) {
            $this->Flash->success(__('The metropole has been deleted.'));
        } else {
            $this->Flash->error(__('The metropole could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
