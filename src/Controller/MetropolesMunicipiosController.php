<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MetropolesMunicipios Controller
 *
 * @property \App\Model\Table\MetropolesMunicipiosTable $MetropolesMunicipios
 *
 * @method \App\Model\Entity\MetropolesMunicipio[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MetropolesMunicipiosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Metropoles', 'Municipios'],
        ];
        $metropolesMunicipios = $this->paginate($this->MetropolesMunicipios);

        $this->set(compact('metropolesMunicipios'));
    }

    /**
     * View method
     *
     * @param string|null $id Metropoles Municipio id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $metropolesMunicipio = $this->MetropolesMunicipios->get($id, [
            'contain' => ['Metropoles', 'Municipios'],
        ]);

        $this->set('metropolesMunicipio', $metropolesMunicipio);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $metropolesMunicipio = $this->MetropolesMunicipios->newEntity();
        if ($this->request->is('post')) {
            $metropolesMunicipio = $this->MetropolesMunicipios->patchEntity($metropolesMunicipio, $this->request->getData());
            if ($this->MetropolesMunicipios->save($metropolesMunicipio)) {
                $this->Flash->success(__('The metropoles municipio has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The metropoles municipio could not be saved. Please, try again.'));
        }
        $metropoles = $this->MetropolesMunicipios->Metropoles->find('list', ['limit' => 200]);
        $municipios = $this->MetropolesMunicipios->Municipios->find('list', ['limit' => 200]);
        $this->set(compact('metropolesMunicipio', 'metropoles', 'municipios'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Metropoles Municipio id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $metropolesMunicipio = $this->MetropolesMunicipios->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $metropolesMunicipio = $this->MetropolesMunicipios->patchEntity($metropolesMunicipio, $this->request->getData());
            if ($this->MetropolesMunicipios->save($metropolesMunicipio)) {
                $this->Flash->success(__('The metropoles municipio has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The metropoles municipio could not be saved. Please, try again.'));
        }
        $metropoles = $this->MetropolesMunicipios->Metropoles->find('list', ['limit' => 200]);
        $municipios = $this->MetropolesMunicipios->Municipios->find('list', ['limit' => 200]);
        $this->set(compact('metropolesMunicipio', 'metropoles', 'municipios'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Metropoles Municipio id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $metropolesMunicipio = $this->MetropolesMunicipios->get($id);
        if ($this->MetropolesMunicipios->delete($metropolesMunicipio)) {
            $this->Flash->success(__('The metropoles municipio has been deleted.'));
        } else {
            $this->Flash->error(__('The metropoles municipio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
