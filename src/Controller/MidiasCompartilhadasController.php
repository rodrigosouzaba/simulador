<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * MidiasCompartilhadas Controller
 *
 * @property \App\Model\Table\MidiasCompartilhadasTable $MidiasCompartilhadas
 */
class MidiasCompartilhadasController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['divulgacao', 'filtro', 'compartilhar']);
    }

    private $ramos = [
        'SPF' => 'Saúde Pessoa Física',
        'SPJ' => 'Saúde Pessoa Jurídica',
        'OPF' => 'Odonto Pessoa Física',
        'OPJ' => 'Odonto Pessoa Jurídica'
    ];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $midiasCompartilhadas = $this->MidiasCompartilhadas->find('all')->contain('MidiasCompartilhadasOperadoras')->toArray();

        $this->set(compact('midiasCompartilhadas'));
        $this->set('_serialize', ['midiasCompartilhadas']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function show()
    {
        $this->paginate = [
            'conditions' => [
                'visibilidade' => 1
            ]
        ];
        $midiasCompartilhadas = $this->paginate($this->MidiasCompartilhadas);
        $session = $this->getRequest()->getSession();
        $session = $session->read('Auth.User');

        $user['nome'] = $session['nome'] . ' ' . $session['sobrenome'];
        $user['celular'] = $session['celular'];
        $user['whatsapp'] = $session['whatsapp'];
        $user['email'] = $session['email'];
        $user['logo'] = isset($session['imagem']['nome']) ? $session['imagem']['nome'] : "";

        $this->set(compact('midiasCompartilhadas', 'user'));
        $this->set('_serialize', ['midiasCompartilhadas']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->layout = 'ajax';

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tempFile = WWW_ROOT .  $data['path'];
            $caminhoDestino = WWW_ROOT . 'uploads/midias_compartilhadas/' . $data['nome'];

            try {
                $date = Time::parse(str_replace('/', '-', $data['validade']));
                $data['validade'] = $date;
            } catch (\Exception $e) {
                unset($data['validade']);
            }

            if (copy($tempFile, $caminhoDestino)) {

                $midiasCompartilhada = $this->MidiasCompartilhadas->newEntity();
                $midiasCompartilhada = $this->MidiasCompartilhadas->patchEntity($midiasCompartilhada, $data);

                if ($this->MidiasCompartilhadas->save($midiasCompartilhada)) {
                    unlink($tempFile);
                    $this->Flash->success(__('Midias salva com sucesso'));
                } else {
                    debug($midiasCompartilhada);
                    $this->Flash->error(__('A mídia não foi salva, Tente novamente.'));
                }
            } else {
                $this->Flash->error(__('Não foi possível fazer o upload da imagem.'));
            }
        } else {

            $this->loadModel('Estados');
            $estados = $this->Estados->find('list');
            $ramos = $this->ramos;
            $this->set(compact('midiasCompartilhada', 'estados', 'ramos'));
            $this->set('_serialize', ['midiasCompartilhada']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Midias Compartilhada id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $midiasCompartilhada = $this->MidiasCompartilhadas->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            $ok = true;
            if (!empty($data['path'])) {
                $tempFile = WWW_ROOT .  $data['path'];
                $caminhoDestino = WWW_ROOT . 'uploads/midias_compartilhadas/' . $data['nome'];

                if (!copy($tempFile, $caminhoDestino)) {
                    $ok = false;
                    $this->Flash->error(__('Não foi possível fazer o upload da imagem.'));
                }
            }

            try {
                $date = Time::parse(str_replace('/', '-', $data['validade']));
                $data['validade'] = $date;
            } catch (\Exception $e) {
                unset($data['validade']);
            }
            if ($ok) {
                $midiasCompartilhada = $this->MidiasCompartilhadas->patchEntity($midiasCompartilhada, $data);
                if ($this->MidiasCompartilhadas->save($midiasCompartilhada)) {
                    if (!empty($data['imagem'])) {
                        unlink($tempFile);
                    }
                    $this->Flash->success(__('Midias atualizado com sucesso'));
                } else {
                    $this->Flash->error(__('A mídia não foi salva, Tente novamente.'));
                }
            }
        }

        $operadoras = $this->resOperadoras($midiasCompartilhada->ramo);

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list');

        $ramos = $this->ramos;
        $this->set(compact('midiasCompartilhada', 'estados', 'ramos', 'operadoras'));
        $this->set('_serialize', ['midiasCompartilhada']);
    }

    public function delete($id = null)
    {
        parent::_delete($id);

        $obj = $this->midia->get($id);
        $imagem = $obj->nome;
        if (!empty($imagem) && file_exists(WWW_ROOT . 'uploads/midias_compartilhadas/' . $imagem)) {
            unlink(WWW_ROOT . 'uploads/midias_compartilhadas/' . $imagem);
        }
    }

    protected function resOperadoras($ramo)
    {
        switch ($ramo) {
            case 'SPF':
                $this->loadModel('PfOperadoras');
                $operadoras = $this->PfOperadoras->find('list', ['valueField' => 'nome'])
                    ->where([
                        'NOT' => ['status <=>' => 'INATIVA'],
                        'nova' => 1
                    ])
                    ->orderAsc('nome');
                break;
            case 'SPJ':
                $this->loadModel('Operadoras');
                $operadoras = $this->Operadoras->find('list', ['valueField' => 'nome'])
                    ->where([
                        'NOT' => ['status <=>' => 'INATIVA'],
                        ['nova' => 1]
                    ])
                    ->orderAsc('nome');
                break;
            case 'OPF':
                $this->loadModel('OdontoOperadoras');
                $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome'])
                    ->where([
                        'tipo_pessoa' => 'PF',
                        'NOT' => ['status <=>' => 'INATIVA']
                    ])
                    ->orderAsc('nome');
                break;
            case 'OPJ':
                $this->loadModel('OdontoOperadoras');
                $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome'])
                    ->where([
                        'tipo_pessoa' => 'PJ',
                        'NOT' => ['status <=>' => 'INATIVA']
                    ])
                    ->orderAsc('nome');
                break;
        }
        return $operadoras;
    }

    public function findOperadoras($ramo, $action = null)
    {
        $operadoras = $this->resOperadoras($ramo);
        $this->set(compact('operadoras', 'action'));
    }

    /**
     * Funcao para upload das imagens
     *
     * @return void
     */
    public function imagens()
    {
        $this->autoRender = false;
        $res = $this->request->data;
        // debug($_FILES);
        if (!empty($_FILES)) {
            $user_id = $this->getRequest()->getSession()->read('Auth.User.id');
            $res['ok'] = true;
            $file = $this->request->getData('file');

            $tempFile = $file['tmp_name'];
            $targetPath = WWW_ROOT . "tmp/$user_id/gallery/";
            $newFileName = strtolower(str_replace(' ', '_', $this->removeAcentuacao($file['name'])));
            $targetFile = $targetPath . $newFileName;

            if (is_dir($targetPath)) {

                if (move_uploaded_file($tempFile, $targetFile)) {
                    $res['path'] = "tmp/$user_id/gallery/" . $newFileName;
                    $res['name'] = $newFileName;
                } else {
                    $res['ok'] = false;
                    $res['error_mov'] = 1;
                }
            } else {
                if (mkdir($targetPath, 0755, true)) {
                    if (!move_uploaded_file($tempFile, $targetFile)) {
                        $res['error_mov'] = 1;
                    }
                }
            }
        }

        return $this->response->withType('application/json')
            ->withStringBody(json_encode($res));
    }

    /**
     * Exibe material de divulgação
     */
    public function viewMaterial($id)
    {
        $this->layout = 'ajax';

        $midia = $this->MidiasCompartilhadas->MidiasCompartilhadasOperadoras->get($id);

        $this->set(compact('midia'));
    }

    /**
     * filtro de ramo e operadora
     *
     * @param  mixed $ramo
     * @param  mixed $operadora
     * @return void
     */
    public function filtro($ramo, $operadora)
    {
        $midiasCompartilhadas = $this->MidiasCompartilhadas->find('all')->where(['ramo' => $ramo, 'operadora_id' => $operadora]);
        $this->set(compact('midiasCompartilhadas'));
    }

    /**
     * Pagina destinada para exibir o material de divulgacao
     *
     * @param  mixed $midiasCompartilhadaId
     * @param  mixed $estadoId
     * @return void
     */
    public function divulgacao($midiasCompartilhadaId, $estadoId)
    {
        $userCtlr = new \App\Controller\UsersController;
        $menuVendas = $userCtlr->menuAdicional([]);
        $estadosOdonto = TableRegistry::get('OdontoOperadoras')->getEstados();
        $estadosSaude = TableRegistry::get('Operadoras')->getEstados();

        $midiasCompartilhadas = $this->MidiasCompartilhadas->find('all', [
            'contain' => ['MidiasCompartilhadasOperadoras'],
            'conditions' => ['id' => $midiasCompartilhadaId],
        ])->toArray();

        $this->set(compact('midiasCompartilhadas', 'estadosOdonto', 'estadosSaude', 'menuVendas'));
    }

    /**
     * Pagina destinada para exibir o material de divulgacao
     *
     * @param  mixed $midiasCompartilhadaId
     * @param  mixed $estadoId
     * @return void
     */
    public function compartilhar($id)
    {
        $this->layout = 'ajax';
        $session = $this->getRequest()->getSession();
        $session = $session->read('Auth.User');

        $user['nome'] = $session['nome'] . ' ' . $session['sobrenome'];
        $user['celular'] = $session['celular'];
        $user['whatsapp'] = $session['whatsapp'];
        $user['email'] = $session['email'];
        $user['logo'] = isset($session['imagem']['nome']) ? $session['imagem']['nome'] : "";

        $midia = $this->MidiasCompartilhadas->MidiasCompartilhadasOperadoras->get($id);
        $this->set(compact('midia', 'user'));
    }
}
