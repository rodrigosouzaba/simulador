<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * MidiasCompartilhadasOperadoras Controller
 *
 * @property \App\Model\Table\MidiasCompartilhadasOperadorasTable $MidiasCompartilhadasOperadoras
 */
class MidiasCompartilhadasOperadorasController extends AppController
{
    private $ramos = [
        'SPF' => 'Saúde Pessoa Física',
        'SPJ' => 'Saúde Pessoa Jurídica',
        'OPF' => 'Odonto Pessoa Física',
        'OPJ' => 'Odonto Pessoa Jurídica'
    ];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $midiasCompartilhadasOperadoras = $this->MidiasCompartilhadasOperadoras->find('all')->toArray();
        
        $this->set(compact('midiasCompartilhadasOperadoras'));
        $this->set('_serialize', ['midiasCompartilhadasOperadoras']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function show()
    {
        $this->paginate = [
            'conditions' => [
                'visibilidade' => 1
            ]
        ];
        $midiasCompartilhadasOperadoras = $this->paginate($this->MidiasCompartilhadasOperadoras);
        $session = $this->getRequest()->getSession();
        $session = $session->read('Auth.User');

        $user['nome'] = $session['nome'] . ' ' . $session['sobrenome'];
        $user['celular'] = $session['celular'];
        $user['whatsapp'] = $session['whatsapp'];
        $user['email'] = $session['email'];
        $user['logo'] = isset($session['imagem']['nome']) ? $session['imagem']['nome'] : "";

        $this->set(compact('midiasCompartilhadasOperadoras', 'user'));
        $this->set('_serialize', ['midiasCompartilhadasOperadoras']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($midiaCompartilhadaId = null)
    {
        $this->layout = 'ajax';

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tempFile = WWW_ROOT .  $data['path'];
            $caminhoDestino = WWW_ROOT . 'uploads/midias_compartilhadas/' . $data['imagem'];

            if (copy($tempFile, $caminhoDestino)) {

                $MidiasCompartilhadasOperadoras = $this->MidiasCompartilhadasOperadoras->newEntity();
                $MidiasCompartilhadasOperadoras = $this->MidiasCompartilhadasOperadoras->patchEntity($MidiasCompartilhadasOperadoras, $data);
                
                if ($this->MidiasCompartilhadasOperadoras->save($MidiasCompartilhadasOperadoras)) {
                    unlink($tempFile);
                    $this->Flash->success(__('Midias salva com sucesso'));

                } else {
                    $this->Flash->error(__('A mídia não foi salva, Tente novamente.'));
                }
            } else {
                $this->Flash->error(__('Não foi possível fazer o upload da imagem.'));
            }
        }else{
            $midiasCompartilhadasOperadoras = $this->MidiasCompartilhadasOperadoras->newEntity();
            $this->loadModel('Estados');
            $estados = $this->Estados->find('list');
            $ramos = $this->ramos;

            $this->set(compact('midiasCompartilhadasOperadoras', 'estados', 'ramos', 'midiaCompartilhadaId'));
            $this->set('_serialize', ['midiasCompartilhada']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Midias Compartilhada id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($midiaCompartilhadaId = null, $id = null)
    {
        $this->layout = 'ajax';
        $midiasCompartilhadasOperadoras = $this->MidiasCompartilhadasOperadoras->get($id, [
			'contain' => ['MidiasCompartilhadasOperadorasEstados']
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            if(!empty($data['imagem'])){
                $tempFile = WWW_ROOT .  $data['path'];
                $caminhoDestino = WWW_ROOT . 'uploads/midias_compartilhadas/' . $data['imagem'];
                if(file_exists($tempFile))
                    copy($tempFile, $caminhoDestino);
            }
            try {
                $date = Time::parse(str_replace('/', '-', $data['validade']));
                $data['validade'] = $date;
            } catch (\Exception $e) {
                unset($data['validade']);
            }
            $midiasCompartilhadasOperadoras = $this->MidiasCompartilhadasOperadoras->patchEntity($midiasCompartilhadasOperadoras, $data);
            if ($this->MidiasCompartilhadasOperadoras->save($midiasCompartilhadasOperadoras)) {
                if(!empty($data['imagem'])){
                    unlink($tempFile);
                }
                $this->Flash->success(__('Midias salva com sucesso'));

            } else {
                $this->Flash->error(__('A mídia não foi salva, Tente novamente.'));
            }
        }

        $operadoras = $this->resOperadoras($midiasCompartilhadasOperadoras->ramo);

        $estadosSelecionados = [];
        if(!empty($midiasCompartilhadasOperadoras->midias_compartilhadas_operadoras_estados)){
            foreach ($midiasCompartilhadasOperadoras->midias_compartilhadas_operadoras_estados as $value) {
                array_push($estadosSelecionados, $value->estado_id);
            }
        }

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list');

        $ramos = $this->ramos;
        $this->set(compact('midiasCompartilhadasOperadoras', 'estados', 'ramos', 'operadoras', 'estadosSelecionados'));
        $this->set('_serialize', ['midiasCompartilhadasOperadoras']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Midias Compartilhada id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        parent::_delete($id);

        $obj = $this->MidiasCompartilhadasOperadoras->get($id);
        $imagem = $obj->nome;
        if (!empty($imagem) && file_exists(WWW_ROOT . 'uploads/midias_compartilhadas/' . $imagem)) {
            unlink(WWW_ROOT . 'uploads/midias_compartilhadas/' . $imagem);
        }

    }

    public function findOperadoras($ramo, $action = null)
    {
        $operadoras = $this->resOperadoras($ramo);
        $this->set(compact('operadoras', 'action'));
    }

    protected function resOperadoras($ramo)
    {
        switch ($ramo) {
            case 'SPF':
                $this->loadModel('PfOperadoras');
                $operadoras = $this->PfOperadoras->find('list', ['valueField' => 'nome'])
                    ->where([
                        'NOT' => ['status <=>' => 'INATIVA'],
                        'nova' => 1
                    ])
                    ->orderAsc('nome');
                break;
            case 'SPJ':
                $this->loadModel('Operadoras');
                $operadoras = $this->Operadoras->find('list', ['valueField' => 'nome'])
                    ->where([
                        'NOT' => ['status <=>' => 'INATIVA'],
                        ['nova' => 1]
                    ])
                    ->orderAsc('nome');
                break;
            case 'OPF':
                $this->loadModel('OdontoOperadoras');
                $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome'])
                    ->where([
                        'tipo_pessoa' => 'PF',
                        'NOT' => ['status <=>' => 'INATIVA']
                    ])
                    ->orderAsc('nome');
                break;
            case 'OPJ':
                $this->loadModel('OdontoOperadoras');
                $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome'])
                    ->where([
                        'tipo_pessoa' => 'PJ',
                        'NOT' => ['status <=>' => 'INATIVA']
                    ])
                    ->orderAsc('nome');
                break;
        }
        return $operadoras;
    }

    public function imagens()
    {
        $this->autoRender = false;
        $res = $this->request->data;

        if (!empty($_FILES)) {
            $user_id = $this->getRequest()->getSession()->read('Auth.User.id');
            $res['ok'] = true;
            $file = $this->request->getData('file');

            $tempFile = $file['tmp_name'];
            $targetPath = WWW_ROOT . "tmp/$user_id/gallery/";
            $newFileName = strtolower(str_replace(' ', '_', $this->removeAcentuacao($file['name'])));
            $targetFile = $targetPath . $newFileName;

            if(is_dir($targetPath)){
                
                if (move_uploaded_file($tempFile, $targetFile)) {
                    $res['path'] = "tmp/$user_id/gallery/" . $newFileName;
                    $res['name'] = $newFileName;
                }else{
                    $res['ok'] = false;
                    $res['error_mov'] = 1;
                }
            }else{ 
                if(mkdir($targetPath, 0755, true)) {
                    if (!move_uploaded_file($tempFile, $targetFile)) {
                        $res['error_mov'] = 1;
                    }
                }
            }
        }

        return $this->response->withType('application/json')
                                ->withStringBody(json_encode($res));
    }

    public function filtro($ramo, $operadora)
    {
        $midiasCompartilhadasOperadoras = $this->MidiasCompartilhadasOperadoras->find('all')->where(['ramo' => $ramo, 'operadora_id' => $operadora]);
        $this->set(compact('midiasCompartilhadasOperadoras'));
    }
}
