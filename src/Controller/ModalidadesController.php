<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Modalidades Controller
 *
 * @property \App\Model\Table\ModalidadesTable $Modalidades
 */
class ModalidadesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
//        $this->paginate = [
//            'contain' => ['Carencias', 'Informacoes', 'Observacoes', 'Opcionais', 'Redes', 'Reembolsos']
//        ];
        $this->set('title', 'Modalidades');
        $modalidades = $this->paginate($this->Modalidades);

        $this->set(compact('modalidades'));
        $this->set('_serialize', ['modalidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Modalidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $modalidade = $this->Modalidades->get($id);

        $this->set('modalidade', $modalidade);
        $this->set('_serialize', ['modalidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->set('title', 'Nova Modalidade');
        $modalidade = $this->Modalidades->newEntity();
        if ($this->request->is('post')) {
            $modalidade = $this->Modalidades->patchEntity($modalidade, $this->request->data);
            if ($this->Modalidades->save($modalidade)) {
                $this->Flash->success(__('Salvo com Sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente'));
            }
        }
       
        $this->set(compact('modalidade'));
        $this->set('_serialize', ['modalidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Modalidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
       $this->set('title', 'Editar Modalidade');
       $modalidade = $this->Modalidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $modalidade = $this->Modalidades->patchEntity($modalidade, $this->request->data);
            if ($this->Modalidades->save($modalidade)) {
                $this->Flash->success(__('Editado com Sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao Editar. Tente Novamente'));
            }
        }
        
        $this->set(compact('modalidade'));
        $this->set('_serialize', ['modalidade']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Modalidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $modalidade = $this->Modalidades->get($id);
        if ($this->Modalidades->delete($modalidade)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente Novamente'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
