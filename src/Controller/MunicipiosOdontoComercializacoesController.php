<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MunicipiosOdontoComercializacoes Controller
 *
 * @property \App\Model\Table\MunicipiosOdontoComercializacoesTable $MunicipiosOdontoComercializacoes */
class MunicipiosOdontoComercializacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Municipios', 'OdontoComercializacoes']
        ];
        $municipiosOdontoComercializacoes = $this->paginate($this->MunicipiosOdontoComercializacoes);

        $this->set(compact('municipiosOdontoComercializacoes'));
        $this->set('_serialize', ['municipiosOdontoComercializacoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Municipios Odonto Comercializaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $municipiosOdontoComercializaco = $this->MunicipiosOdontoComercializacoes->get($id, [
            'contain' => ['Municipios', 'OdontoComercializacoes']
        ]);

        $this->set('municipiosOdontoComercializaco', $municipiosOdontoComercializaco);
        $this->set('_serialize', ['municipiosOdontoComercializaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $municipiosOdontoComercializaco = $this->MunicipiosOdontoComercializacoes->newEntity();
        if ($this->request->is('post')) {
            $municipiosOdontoComercializaco = $this->MunicipiosOdontoComercializacoes->patchEntity($municipiosOdontoComercializaco, $this->request->data);
            if ($this->MunicipiosOdontoComercializacoes->save($municipiosOdontoComercializaco)) {
                $this->Flash->success(__('The municipios odonto comercializaco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The municipios odonto comercializaco could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->MunicipiosOdontoComercializacoes->Municipios->find('list', ['limit' => 200]);
        $odontoComercializacoes = $this->MunicipiosOdontoComercializacoes->OdontoComercializacoes->find('list', ['limit' => 200]);
        $this->set(compact('municipiosOdontoComercializaco', 'municipios', 'odontoComercializacoes'));
        $this->set('_serialize', ['municipiosOdontoComercializaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Municipios Odonto Comercializaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $municipiosOdontoComercializaco = $this->MunicipiosOdontoComercializacoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $municipiosOdontoComercializaco = $this->MunicipiosOdontoComercializacoes->patchEntity($municipiosOdontoComercializaco, $this->request->data);
            if ($this->MunicipiosOdontoComercializacoes->save($municipiosOdontoComercializaco)) {
                $this->Flash->success(__('The municipios odonto comercializaco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The municipios odonto comercializaco could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->MunicipiosOdontoComercializacoes->Municipios->find('list', ['limit' => 200]);
        $odontoComercializacoes = $this->MunicipiosOdontoComercializacoes->OdontoComercializacoes->find('list', ['limit' => 200]);
        $this->set(compact('municipiosOdontoComercializaco', 'municipios', 'odontoComercializacoes'));
        $this->set('_serialize', ['municipiosOdontoComercializaco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Municipios Odonto Comercializaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $municipiosOdontoComercializaco = $this->MunicipiosOdontoComercializacoes->get($id);
        if ($this->MunicipiosOdontoComercializacoes->delete($municipiosOdontoComercializaco)) {
            $this->Flash->success(__('The municipios odonto comercializaco has been deleted.'));
        } else {
            $this->Flash->error(__('The municipios odonto comercializaco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
