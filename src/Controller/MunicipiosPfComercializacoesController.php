<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MunicipiosPfComercializacoes Controller
 *
 * @property \App\Model\Table\MunicipiosPfComercializacoesTable $MunicipiosPfComercializacoes */
class MunicipiosPfComercializacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfComercializacoes', 'Municipios']
        ];
        $municipiosPfComercializacoes = $this->paginate($this->MunicipiosPfComercializacoes);

        $this->set(compact('municipiosPfComercializacoes'));
        $this->set('_serialize', ['municipiosPfComercializacoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Municipios Pf Comercializaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $municipiosPfComercializaco = $this->MunicipiosPfComercializacoes->get($id, [
            'contain' => ['PfComercializacoes', 'Municipios']
        ]);

        $this->set('municipiosPfComercializaco', $municipiosPfComercializaco);
        $this->set('_serialize', ['municipiosPfComercializaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $municipiosPfComercializaco = $this->MunicipiosPfComercializacoes->newEntity();
        if ($this->request->is('post')) {
            $municipiosPfComercializaco = $this->MunicipiosPfComercializacoes->patchEntity($municipiosPfComercializaco, $this->request->data);
            if ($this->MunicipiosPfComercializacoes->save($municipiosPfComercializaco)) {
                $this->Flash->success(__('The municipios pf comercializaco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The municipios pf comercializaco could not be saved. Please, try again.'));
            }
        }
        $pfComercializacoes = $this->MunicipiosPfComercializacoes->PfComercializacoes->find('list', ['limit' => 200]);
        $municipios = $this->MunicipiosPfComercializacoes->Municipios->find('list', ['limit' => 200]);
        $this->set(compact('municipiosPfComercializaco', 'pfComercializacoes', 'municipios'));
        $this->set('_serialize', ['municipiosPfComercializaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Municipios Pf Comercializaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $municipiosPfComercializaco = $this->MunicipiosPfComercializacoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $municipiosPfComercializaco = $this->MunicipiosPfComercializacoes->patchEntity($municipiosPfComercializaco, $this->request->data);
            if ($this->MunicipiosPfComercializacoes->save($municipiosPfComercializaco)) {
                $this->Flash->success(__('The municipios pf comercializaco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The municipios pf comercializaco could not be saved. Please, try again.'));
            }
        }
        $pfComercializacoes = $this->MunicipiosPfComercializacoes->PfComercializacoes->find('list', ['limit' => 200]);
        $municipios = $this->MunicipiosPfComercializacoes->Municipios->find('list', ['limit' => 200]);
        $this->set(compact('municipiosPfComercializaco', 'pfComercializacoes', 'municipios'));
        $this->set('_serialize', ['municipiosPfComercializaco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Municipios Pf Comercializaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $municipiosPfComercializaco = $this->MunicipiosPfComercializacoes->get($id);
        if ($this->MunicipiosPfComercializacoes->delete($municipiosPfComercializaco)) {
            $this->Flash->success(__('The municipios pf comercializaco has been deleted.'));
        } else {
            $this->Flash->error(__('The municipios pf comercializaco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
