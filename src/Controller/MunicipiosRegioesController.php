<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MunicipiosRegioes Controller
 *
 * @property \App\Model\Table\MunicipiosRegioesTable $MunicipiosRegioes */
class MunicipiosRegioesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Municipios', 'Regioes']
        ];
        $municipiosRegioes = $this->paginate($this->MunicipiosRegioes);

        $this->set(compact('municipiosRegioes'));
        $this->set('_serialize', ['municipiosRegioes']);
    }

    /**
     * View method
     *
     * @param string|null $id Municipios Regio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $municipiosRegio = $this->MunicipiosRegioes->get($id, [
            'contain' => ['Municipios', 'Regioes']
        ]);

        $this->set('municipiosRegio', $municipiosRegio);
        $this->set('_serialize', ['municipiosRegio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $municipiosRegio = $this->MunicipiosRegioes->newEntity();
        if ($this->request->is('post')) {
            $municipiosRegio = $this->MunicipiosRegioes->patchEntity($municipiosRegio, $this->request->data);
            if ($this->MunicipiosRegioes->save($municipiosRegio)) {
                $this->Flash->success(__('The municipios regio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The municipios regio could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->MunicipiosRegioes->Municipios->find('list', ['limit' => 200]);
        $regioes = $this->MunicipiosRegioes->Regioes->find('list', ['limit' => 200]);
        $this->set(compact('municipiosRegio', 'municipios', 'regioes'));
        $this->set('_serialize', ['municipiosRegio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Municipios Regio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $municipiosRegio = $this->MunicipiosRegioes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $municipiosRegio = $this->MunicipiosRegioes->patchEntity($municipiosRegio, $this->request->data);
            if ($this->MunicipiosRegioes->save($municipiosRegio)) {
                $this->Flash->success(__('The municipios regio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The municipios regio could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->MunicipiosRegioes->Municipios->find('list', ['limit' => 200]);
        $regioes = $this->MunicipiosRegioes->Regioes->find('list', ['limit' => 200]);
        $this->set(compact('municipiosRegio', 'municipios', 'regioes'));
        $this->set('_serialize', ['municipiosRegio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Municipios Regio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $municipiosRegio = $this->MunicipiosRegioes->get($id);
        if ($this->MunicipiosRegioes->delete($municipiosRegio)) {
            $this->Flash->success(__('The municipios regio has been deleted.'));
        } else {
            $this->Flash->error(__('The municipios regio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
