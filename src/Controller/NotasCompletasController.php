<?php
namespace App\Controller;

use App\Controller\AppController;
use ZipArchive;
use Cake\Filesystem\File;

/**
 * NotasCompletas Controller
 *
 * @property \App\Model\Table\NotasCompletasTable $NotasCompletas */
class NotasCompletasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {   
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $userId = $sessao['id'];

        $notas = $this->NotasCompletas->find("all")->where(["user_id" => $userId]);

        $this->paginate = [
            'contain' => ['Users']
        ];
        $notasCompletas = $this->paginate($notas);

        $this->set(compact('notasCompletas'));
        $this->set('_serialize', ['notasCompletas']);
    }

    /**
     * View method
     *
     * @param string|null $id Notas Completa id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $notasCompleta = $this->NotasCompletas->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('notasCompleta', $notasCompleta);
        $this->set('_serialize', ['notasCompleta']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $notasCompleta = $this->NotasCompletas->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $notasCompleta = $this->NotasCompletas->patchEntity($notasCompleta, $this->request->data);
            $notasCompleta->nome_arquivo = substr($this->request->data['arquivo']['name'], 0,-4).'-'.date("dmyhis").".xml"; 

            $notasCompleta->caminho = "uploads/notas/notas_completas/"; 
            
            //vetifica a extensão do arquivo
            if ($notasCompleta->arquivo['type'] === 'text/xml') {
                if ($this->NotasCompletas->save($notasCompleta)) {
                    if(file_exists($notasCompleta->caminho.$notasCompleta->nome_arquivo)){

                        $notasCompleta->nome_arquivo = substr($notasCompleta->nome_arquivo, 0,-4)."(".date("dmyhis").").xml";
                    }


                    //Transfere o arquivo da pasta temporaria para a pasta de uploads
                    if (move_uploaded_file($this->request->data['arquivo']['tmp_name'], $notasCompleta->caminho.$notasCompleta->nome_arquivo)) { 


                        $header = "<?xml version='1.0' encoding='ISO-8859-1'?>
                        <ConsultarNfseResposta xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.abrasf.org.br/ABRASF/arquivos/nfse.xsd'>
                        <ListaNfse>
                        <CompNfse>
                        <Nfse>
                        ";
                        $footer = "
                        </Nfse>
                        </CompNfse>
                        </ListaNfse>
                        </ConsultarNfseResposta>
                        ";

                        $diretorio = "uploads/notas/";
                        $arquivo = $notasCompleta->caminho.$notasCompleta->nome_arquivo;

                        if (file_exists($arquivo)) {

                            $xml = simplexml_load_file($arquivo);
                            $conjunto = $xml->ListaNfse->CompNfse;


                            $zip = new ZipArchive();
                            $zip->open($diretorio."notas_unicas/".substr($notasCompleta->nome_arquivo, 0,-4).".zip", ZipArchive::CREATE);

                            $this->loadModel('NotasUnicas');

                            foreach ($conjunto as $chave => $item) {


                                $ind = $item->Nfse->InfNfse;
                                $conteudo = $ind->asXML();
                                $conteudo = $header.$conteudo.$footer;
                                $numero = $ind->Numero;
                                $nome = $notasCompleta->id."-NF".$numero;
                                
                                $notaUnica = $this->NotasUnicas->newEntity();
                                $notaUnica = $this->NotasUnicas->patchEntity($notaUnica, $this->request->data);

                                $notaUnica->nota_completa_id = $notasCompleta->id;
                                $notaUnica->nome_arquivo = $nome;
                                
                                $this->NotasUnicas->save($notaUnica);


                                
                                file_put_contents($diretorio."notas_unicas/".$nome.".xml", $conteudo);

                                $zip->addFile(realpath($diretorio."notas_unicas/".$nome.".xml"), basename($diretorio."notas_unicas/".$nome.".xml"));
                            }
                            $zip->close();

                        //Apagando os XMLs e deixando somente o ZIP
                            $mascara = $diretorio."notas_unicas/*.xml";
                            array_map("unlink", glob( $mascara));

                            $zip_file = "uploads/notas/notas_unicas/".substr($notasCompleta->nome_arquivo, 0,-4).".zip";

                            header('Content-type: application/zip');
                            header('Content-Disposition: attachment; filename="'.basename($zip_file).'"');
                            header("Content-length: " . filesize($zip_file));
                            header("Pragma: no-cache");
                            header("Expires: 0");

                            ob_clean();
                            flush();

                            readfile($zip_file);
                            exit;
                        } else {
                            exit('Falha ao abrir '.$nome.'.');
                        }
                        $this->redirect(['action' => 'index']);
                        // $this->Flash->success(__('A nota foi adicionada com sucesso.'));
                    } else {
                        $this->Flash->error(__('Esta nota não poder ser salva. Por favor, Tente novamente.'));
                    }
                } 
                else { 
                    echo "Erro, o arquivo nâo pode ser enviado."; 
                    
                }
            }
        }

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->set(compact('notasCompleta', 'sessao'));
        $this->set('_serialize', ['notasCompleta']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Notas Completa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $notasCompleta = $this->NotasCompletas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $notasCompleta = $this->NotasCompletas->patchEntity($notasCompleta, $this->request->data);
            if ($this->NotasCompletas->save($notasCompleta)) {
                $this->Flash->success(__('The notas completa has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The notas completa could not be saved. Please, try again.'));
            }
        }
        $users = $this->NotasCompletas->Users->find('list', ['limit' => 200]);
        $this->set(compact('notasCompleta', 'users'));
        $this->set('_serialize', ['notasCompleta']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Notas Completa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $diretorio = "uploads/notas/";
        $this->request->allowMethod(['post', 'delete']);
        $notasCompleta = $this->NotasCompletas->get($id);
        if ($this->NotasCompletas->delete($notasCompleta)) {
            $mascara = $diretorio."notas_unicas/".substr($notasCompleta->nome_arquivo, 0,-4).".zip";
            array_map("unlink", glob( $mascara));
            $this->Flash->success(__('A nota foi excluida com sucesso.'));
        } else {
            $this->Flash->error(__('Esta nota não pode ser deletada. Por Favor, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
