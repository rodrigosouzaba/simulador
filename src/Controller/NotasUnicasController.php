<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NotasUnicas Controller
 *
 * @property \App\Model\Table\NotasUnicasTable $NotasUnicas */
class NotasUnicasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['NotasCompletas']
        ];
        $notasUnicas = $this->paginate($this->NotasUnicas);

        $this->set(compact('notasUnicas'));
        $this->set('_serialize', ['notasUnicas']);
    }

    /**
     * View method
     *
     * @param string|null $id Notas Unica id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $notasUnica = $this->NotasUnicas->get($id, [
            'contain' => ['NotasCompletas']
        ]);

        $this->set('notasUnica', $notasUnica);
        $this->set('_serialize', ['notasUnica']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $notasUnica = $this->NotasUnicas->newEntity();
        if ($this->request->is('post')) {
            $notasUnica = $this->NotasUnicas->patchEntity($notasUnica, $this->request->data);
            if ($this->NotasUnicas->save($notasUnica)) {
                $this->Flash->success(__('The notas unica has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The notas unica could not be saved. Please, try again.'));
            }
        }
        $notasCompletas = $this->NotasUnicas->NotasCompletas->find('list', ['limit' => 200]);
        $this->set(compact('notasUnica', 'notasCompletas'));
        $this->set('_serialize', ['notasUnica']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Notas Unica id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $notasUnica = $this->NotasUnicas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $notasUnica = $this->NotasUnicas->patchEntity($notasUnica, $this->request->data);
            if ($this->NotasUnicas->save($notasUnica)) {
                $this->Flash->success(__('The notas unica has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The notas unica could not be saved. Please, try again.'));
            }
        }
        $notasCompletas = $this->NotasUnicas->NotasCompletas->find('list', ['limit' => 200]);
        $this->set(compact('notasUnica', 'notasCompletas'));
        $this->set('_serialize', ['notasUnica']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Notas Unica id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $notasUnica = $this->NotasUnicas->get($id);
        if ($this->NotasUnicas->delete($notasUnica)) {
            $this->Flash->success(__('The notas unica has been deleted.'));
        } else {
            $this->Flash->error(__('The notas unica could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
