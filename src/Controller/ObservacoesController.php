<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Observacoes Controller
 *
 * @property \App\Model\Table\ObservacoesTable $Observacoes
 */
class ObservacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            $conditions_operadoras = [
                'estado_id' => $estado,

            ];
            if ($nova) {
                $operadoras = $this->Observacoes->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            } else {
                $operadoras = $this->Observacoes->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'Operadoras.estado_id' => $estado, 'nova' => 0], 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['Operadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['Operadoras.nova'] =  1;
                $observacoes = $this->Observacoes
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->matching('Operadoras.AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('Observacoes.id')
                    ->orderAsc('Operadoras.prioridade');
            } else {
                $conditions['Operadoras.estado_id'] = $estado;
                $conditions['OR'] = ['Operadoras.nova' => 0, 'Operadoras.nova IS NULL'];
                $observacoes = $this->Observacoes
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->where($conditions)
                    ->orderAsc('Operadoras.prioridade');
            }
        } else {
            $observacoes = $this->Observacoes
                ->find('all')
                ->contain(['Operadoras'])
                ->orderAsc('Operadoras.prioridade');
        }

        $observacoes = $this->paginate($observacoes);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('AreasComercializacoes.Operadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'operadoras', 'alias' => 'Operadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = Operadoras.estado_id']]);
            }
        }
        $this->set(compact('observacoes', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }


    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        //        debug($id);
        $observacoes = $this->Observacoes->find('all', ['contain' => ['Operadoras' => ['Imagens']]])
            ->where(['Observacoes.operadora_id' => $id])
            ->order('Observacoes.nome')->toArray();
        //        debug($produtos);die();
        $operadoras = $this->Observacoes->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $this->set(compact('observacoes', 'operadoras'));
        $this->set('_serialize', ['observacoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Observaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $observaco = $this->Observacoes->get($id, [
            'contain' => []
        ]);

        $this->set('observaco', $observaco);
        $this->set('_serialize', ['observaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nova Observação');
        $observaco = $this->Observacoes->newEntity();
        if ($this->request->is('post')) {
            $observaco = $this->Observacoes->patchEntity($observaco, $this->request->data);
            if ($this->Observacoes->save($observaco)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente'));
            }
        }
        $operadoras = [];

        $this->set(compact('observaco', 'operadoras'));
        $this->set('_serialize', ['observaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Observaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Editar Observações');
        $observaco = $this->Observacoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $observaco = $this->Observacoes->patchEntity($observaco, $this->request->data);
            if ($this->Observacoes->save($observaco)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente'));
            }
        }
        $operadoras = $this->Observacoes->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);

        $this->set(compact('observaco', 'operadoras'));
        $this->set('_serialize', ['observaco']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Observaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $observaco = $this->Observacoes->get($id);
        if ($this->Observacoes->delete($observaco)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao Excluir. Tente Novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    /*
        Filtro de operadora por Estado
    */
    public function filtroEstado()
    {
        $estado = $this->request->data()['estado_id'];
        $operadoras = $this->Observacoes->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            }
        ])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact('operadoras'));
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $operadoras = $this->Observacoes->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status' => ''],
                    ['estado_id' => $estado, 'status IS' => null]
                ],
                'NOT' => 'status <=> "INATIVA"',
                '1 = 1'
            ])
            ->orderASC('Operadoras.nome')
            ->toArray();

        $conditions[] = ['Operadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['Operadoras.id' => $operadora];
        }
        $this->paginate = [
            'valueField' => 'nome', 'order' => ['Tabelas.prioridade' => 'ASC'],
            'contain' => [
                'Operadoras'
            ],
            'order' => [
                'Tabelas.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $observacoes = $this->paginate($this->Observacoes);

        $estados = $this->Observacoes->Operadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->Observacoes->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('observacoes', 'operadoras', 'estados', 'operadora', 'sessao', 'estado'));
    }


    function filtroNova($nova)
    {
        $conditions = [];
        if ($nova == 1) {
            $conditions[] = ['Operadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['Operadoras.nova IS NULL', 'Operadoras.nova' => 0];
        }
        $operadoras = $this->Observacoes->Operadoras->find('list', [
            'valueField' => function ($q) use ($nova) {
                if ($q->estado)
                    return $q->get('nome') . ' - ' . $q->get('detalhe') . ' - ' . $q->estado->get('detalhe');
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }
}
