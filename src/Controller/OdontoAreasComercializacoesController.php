<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoAreasComercializacoes Controller
 *
 * @property \App\Model\Table\OdontoAreasComercializacoesTable $OdontoAreasComercializacoes
 *
 * @method \App\Model\Entity\OdontoAreasComercializaco[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OdontoAreasComercializacoesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OdontoOperadoras'],
        ];
        $odontoAreasComercializacoes = $this->paginate($this->OdontoAreasComercializacoes);

        $this->set(compact('odontoAreasComercializacoes'));
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Areas Comercializaco id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoAreasComercializaco = $this->OdontoAreasComercializacoes->get($id, [
            'contain' => ['OdontoOperadoras'],
        ]);

        $this->set('odontoAreasComercializaco', $odontoAreasComercializaco);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoAreasComercializaco = $this->OdontoAreasComercializacoes->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $municipios = $this->getPostMunicipios();
            unset($data['municipio']);
            $data['municipios'] = [];
            foreach ($municipios as $estado => $municipios) {
                foreach ($municipios as $municipio) {
                    $data['municipios'][] = [
                        'id' => $municipio,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }
            $odontoAreasComercializaco = $this->OdontoAreasComercializacoes->patchEntity($odontoAreasComercializaco, $data);
            $toSave = $this->OdontoAreasComercializacoes->save($odontoAreasComercializaco);
            if ($toSave) {
                $this->Flash->success(__('The odonto areas comercializaco has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The odonto areas comercializaco could not be saved. Please, try again.'));
        }
        $odontoOperadoras = $this->OdontoAreasComercializacoes->OdontoOperadoras->find('list', ['valueField' => function ($q) {
            return $q->get('nome') . ' - ' . $q->get('tipo_pessoa');
        }])
            ->where([
                'OR' => [
                    'status IS NOT' => 'INATIVA',
                    'status IS' => null
                ],
                'nova' => 1
            ])->orderAsc('nome');

        $this->loadModel('Estados');
        $estados = $this->Estados->find('all')->contain([
            'Municipios' => ['sort' => ['nome' => 'ASC']],
            'Metropoles' => ['sort' => ['nome' => 'ASC']]
        ]);

        $locaisSalvos['municipios'] = [];
        $locaisSalvos['metropoles'] = [];
        $this->set(compact('estados', 'odontoAreasComercializaco', 'odontoOperadoras', 'locaisSalvos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Areas Comercializaco id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoAreasComercializaco = $this->OdontoAreasComercializacoes->get($id, [
            'contain' => ['Municipios', 'Metropoles'],
        ]);
        $locaisSalvos['municipios'] = [];
        $locaisSalvos['metropoles'] = [];
        foreach ($odontoAreasComercializaco->municipios as $municipio) {
            $locaisSalvos['municipios'][$municipio->id] = $municipio->estado_id;
        }
        foreach ($odontoAreasComercializaco->metropoles as $metropole) {
            $locaisSalvos['metropoles'][$metropole->id] = $metropole->estado_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $municipios = $this->getPostMunicipios();
            unset($data['municipios']);
            $metropoles = $data['metropoles'];
            $data['metropoles'] = [];

            // ORDER METROPOLES
            foreach ($metropoles as $estado => $metropoles) {
                foreach ($metropoles as $metropole) {
                    $data['metropoles'][] = [
                        'id' => $metropole,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }
            foreach ($municipios as $estado => $municipios) {
                foreach ($municipios as $municipio) {
                    $data['municipios'][] = [
                        'id' => $municipio,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }

            $odontoAreasComercializaco = $this->OdontoAreasComercializacoes->patchEntity($odontoAreasComercializaco, $data);
            if ($this->OdontoAreasComercializacoes->save($odontoAreasComercializaco)) {
                $this->Flash->success(__('The odonto areas comercializaco has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The odonto areas comercializaco could not be saved. Please, try again.'));
        }
        $odontoOperadoras = $this->OdontoAreasComercializacoes->OdontoOperadoras->find('list', ['valueField' => 'nome'])
            ->where([
                'OR' => [
                    'status IS NOT' => 'INATIVA',
                    'status IS' => null
                ],
                'nova' => 1
            ])->orderAsc('nome');
        $this->loadModel('Estados');
        $estados = $this->Estados->find('all')->contain([
            'Municipios' => ['sort' => ['nome' => 'ASC']],
            'Metropoles' => ['sort' => ['nome' => 'ASC']]
        ]);
        $this->set(compact('estados', 'odontoAreasComercializaco', 'odontoOperadoras', 'locaisSalvos'));
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Areas Comercializaco id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoAreasComercializaco = $this->OdontoAreasComercializacoes->get($id);
        if ($this->OdontoAreasComercializacoes->delete($odontoAreasComercializaco)) {
            $this->Flash->success(__('The odonto areas comercializaco has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto areas comercializaco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getPostMunicipios()
    {
        $f = file_get_contents("php://input");
        $municipios = [];
        foreach (explode('&', urldecode($f)) as $item) {
            if (substr($item, 0, 10) == 'municipios') {
                (int) $posicao = str_replace(']', '', substr($item, 11, 2));
                if (intval($posicao)) {
                    $municipios[$posicao] = substr($item, strpos($item, '=') + 1);
                } else {
                    $municipios[] = substr($item, strpos($item, '=') + 1);
                }
            } elseif (substr($item, 0, 9) == 'municipio') {
                (int) $posicao = str_replace(']', '', substr($item, 10, 2));
                if (intval($posicao)) {
                    $municipios[$posicao][] = substr($item, strpos($item, '=') + 1);
                } else {
                    $municipios[] = substr($item, strpos($item, '=') + 1);
                }
            }
        };
        return $municipios;
    }
}
