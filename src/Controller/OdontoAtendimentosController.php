<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoAtendimentos Controller
 *
 * @property \App\Model\Table\OdontoAtendimentosTable $OdontoAtendimentos
 */
class OdontoAtendimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $odontoAtendimentos = $this->paginate($this->OdontoAtendimentos);

        $this->set(compact('odontoAtendimentos'));
        $this->set('_serialize', ['odontoAtendimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Atendimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoAtendimento = $this->OdontoAtendimentos->get($id, [
            'contain' => []
        ]);

        $this->set('odontoAtendimento', $odontoAtendimento);
        $this->set('_serialize', ['odontoAtendimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoAtendimento = $this->OdontoAtendimentos->newEntity();
        if ($this->request->is('post')) {
            $odontoAtendimento = $this->OdontoAtendimentos->patchEntity($odontoAtendimento, $this->request->data);
            if ($this->OdontoAtendimentos->save($odontoAtendimento)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users','action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $this->set(compact('odontoAtendimento'));
        $this->set('_serialize', ['odontoAtendimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Atendimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoAtendimento = $this->OdontoAtendimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoAtendimento = $this->OdontoAtendimentos->patchEntity($odontoAtendimento, $this->request->data);
            if ($this->OdontoAtendimentos->save($odontoAtendimento)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users','action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $this->set(compact('odontoAtendimento'));
        $this->set('_serialize', ['odontoAtendimento']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Atendimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoAtendimento = $this->OdontoAtendimentos->get($id);
        if ($this->OdontoAtendimentos->delete($odontoAtendimento)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users','action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }
}