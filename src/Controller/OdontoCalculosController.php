<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\OdontoOperadora;
//use App\Utility\PdfWriter;
use Cake\I18n\Number;
use Cake\View\ViewBuilder;
use Knp\Snappy\Pdf;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\ORM\Entity;
use Cake\Core\Exception\Exception;
use Cake\Collection\Collection;
use Cake\Event\Event;

/**
 * OdontoCalculos Controller
 *
 * @property \App\Model\Table\OdontoCalculosTable $OdontoCalculos
 */
class OdontoCalculosController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.

        $this->Auth->allow(['cotacao']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($tipo_pessoa = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->paginate = [
            'contain' => ['Users'],
            'conditions' => [
                'OdontoCalculos.tipo_pessoa' => $tipo_pessoa,
                'OdontoCalculos.user_id' => $sessao['id']
            ]
        ];
        if ($tipo_pessoa == 'pf') {
            $titulo = 'Cálculos Odontológicos Pessoa Física';
        } else {
            $titulo = 'Cálculos Odontológicos PME';
        }
        $this->loadModel('Status');
        $status = $this->Status->find('list', ['valueField' => 'nome'])->toArray();
        //        $this->paginate = [
        //        'conditions' => [
        //            'Bookmarks.user_id' => $this->Auth->user('id'),
        //        ]
        //    ];
        $odontoCalculos = $this->paginate($this->OdontoCalculos);
        //        debug($odontoCalculos->toArray());

        $this->set(compact('odontoCalculos', 'titulo', 'status'));
        $this->set('_serialize', ['odontoCalculos']);
    }

    public function tabelas(int $id = NULL)
    {
        // ID PODE CHEGAR VIA GET, OU VIA POST CASO FILTRO
        $id = is_null($id) ? $this->request->getData('simulacao_id') : $id;

        $sessao = $this->request->getSession()->read('Auth.User');
        $odonto_calculo = $simulacao = $this->OdontoCalculos->get($id, [
            'contain' => ['Users']
        ]);

        $this->loadModel('OdontoOperadoras');
        $operadoras = $this->OdontoOperadoras->find('all', ['contain' => ['Imagens']])->where(['OdontoOperadoras.tipo_pessoa' => $odonto_calculo->tipo_pessoa, 'NOT(OdontoOperadoras.status <=> \'INATIVA\')'])
            ->toArray();

        if ($odonto_calculo['user_id'] == $sessao['id'] || $sessao['role'] == 'admin') {

            $this->loadModel('OdontoTabelas');
            $tabelas = $precos = $this->OdontoTabelas->find('all')
                ->matching('OdontoAreasComercializacoes.Municipios', function ($q) use ($simulacao) {
                    return $q->where(['Municipios.estado_id' => $simulacao['estado_id']]);
                })
                ->contain([
                    'OdontoComercializacoes',
                    'OdontoAtendimentos',
                    'OdontoAreasComercializacoes' => [
                        'OdontoOperadoras' => [
                            'Imagens',
                            'OdontoCarencias',
                            'OdontoRedes',
                            'OdontoReembolsos',
                            'OdontoDocumentos',
                            'OdontoDependentes',
                            'OdontoObservacaos',
                            'OdontoFormasPagamentos'
                        ],
                        'Municipios' => ['Estados'],
                    ]
                ])
                ->where([
                    'OdontoTabelas.new' => 1,
                    'OdontoTabelas.validade' => 1,
                    'OdontoTabelas.tipo_pessoa' => $odonto_calculo['tipo_pessoa'],
                    'OdontoTabelas.minimo_vidas <=' => $odonto_calculo['quantidade_vidas'],
                    'OdontoTabelas.maximo_vidas >=' => $odonto_calculo['quantidade_vidas']
                ])
                ->order([
                    // 'OdontoOperadoras.prioridade' => 'ASC',
                    'OdontoTabelas.prioridade' => 'ASC'
                ])
                ->group('OdontoTabelas.id')
                ->toArray();

            $total_tabelas = count($tabelas);
            foreach ($precos as $a) {
                if ($a['preco_vida'] > 0) {
                    $menor_preco = $a;
                    break;
                }
            }
            #NOTICE é exibido caso não sejam encontradas tabelas dentro das condições do WHERE
            #@ será colocado para ocultar o NOTICE
            @$intermediario = $precos[count($tabelas) / 2];
            $maior_preco = end($precos);

            $tabelas_ordenadas = [];
            foreach ($tabelas as $tabela) {
                $tabelas_ordenadas[$tabela['odonto_areas_comercializaco']['odonto_operadora']['id']]['dados'] =  $tabela['odonto_areas_comercializaco']['odonto_operadora'];
                $tabelas_ordenadas[$tabela['odonto_areas_comercializaco']['odonto_operadora']['id']]['tabelas'][] = $tabela;
            }

            // DADOS PARA FILTRO
            $totalTabelas = $total_tabelas; //ALIAS ELEMENT FILTRO

            $this->loadModel('OdontoAtendimentos');
            $abrangencias = $this->OdontoAtendimentos->find('list', ['valueField' => 'nome', 'order' => ['prioridade' => 'ASC']])->toArray();

            $this->loadModel('OdontoCoberturas');
            $coberturas = $this->OdontoCoberturas->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

            $simulacao['rede'] = 1;
            $simulacao['carencia'] = 1;
            $simulacao['reembolso'] = 1;
            $simulacao['info'] = 1;
            $simulacao['opcionais'] = 1;
            $simulacao['observaco'] = 1;

            // ALIAS
            $total_vidas = $totalVidas = $odonto_calculo['tipo_pessoa'];

            $this->set(compact('cotacao', 'odonto_calculo', 'tabelas_ordenadas', 'total_tabelas', 'totalTabelas', 'menor_preco', 'intermediario', 'maior_preco', 'total_vidas', 'operadoras', 'acomodacoes', 'abrangencias', 'coberturas', 'is_filtro'));
        } else {
            $this->Flash->error(__('Sem permissão de visualizar este Cálculo. Cálculo pertence a outro usuário.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Calculo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sessao = $this->request->getSession()->read('Auth.User');

        $this->loadModel('OdontoOperadoras');
        $odonto_calculo = $cotacao = $this->OdontoCalculos
            ->get($id, ['contain' => ['Users']]);

        if ($odonto_calculo['user_id'] == $sessao['id'] || $sessao['role'] == 'admin') {
            $this->loadModel('OdontoTabelas');
            $tabelas = $this->OdontoTabelas
                ->find('all', [
                    'contain' => [
                        'OdontoAtendimentos',
                        'OdontoFormasPagamentos',
                        'OdontoCarencias',
                        'OdontoRedes',
                        'OdontoReembolsos',
                        'OdontoDocumentos',
                        'OdontoDependentes',
                        'OdontoObservacaos',
                        'OdontoAreasComercializacoes' => [
                            'Metropoles',
                            'Municipios',
                            'OdontoOperadoras' => ['Imagens']
                        ]
                    ]
                ])
                ->where([
                    'OdontoTabelas.new' => 1,
                    'OdontoTabelas.validade' => 1,
                    'OdontoTabelas.tipo_pessoa' => $odonto_calculo->tipo_pessoa,
                    'OdontoTabelas.minimo_vidas <=' => $odonto_calculo->quantidade_vidas,
                    'OdontoTabelas.maximo_vidas >=' => $odonto_calculo->quantidade_vidas
                ]);
            $precos = collection($tabelas->order(['OdontoTabelas.preco_vida' => 'ASC']))->combine('id', 'preco_vida')->toArray();
            $tabelas = $tabelas->order(['OdontoTabelas.prioridade' => 'ASC'])->toArray();

            $menor_preco = collection($tabelas)->filter(function ($tabela) use ($precos) {
                return $tabela->id == (int) array_keys($precos, min($precos))[0];
            })->first();
            $maior_preco = collection($tabelas)->filter(function ($tabela) use ($precos) {
                return $tabela->id == (int) array_keys($precos, max($precos))[0];
            })->first();
            $intermediario = collection($tabelas)->filter(function () use ($precos) {
                return array_keys($precos)[(int) (count($precos) - 1) / 2];
            })->first();

            $tabelas_ordenadas = [];
            $area_adicionada = [];
            foreach ($tabelas as $tabela) {
                $operadora_id = $tabela->odonto_areas_comercializaco->odonto_operadora_id;
                $tabelas_ordenadas[$operadora_id]['operadora'] = $tabela->odonto_areas_comercializaco->odonto_operadora;

                @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['odonto_atendimento']['nome']] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_atendimento');

                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_areas_comercializaco');
                if (!in_array($tabela['odonto_areas_comercializaco']['id'], $area_adicionada)) {
                    foreach ($tabela['odonto_areas_comercializaco']['metropoles'] as $metropole) {
                        @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                    }
                    foreach ($tabela['odonto_areas_comercializaco']['municipios'] as $municipio) {
                        @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                    }
                    $area_adicionada[] = $tabela['odonto_areas_comercializaco']['id'];
                }
                unset($tabela['odonto_areas_comercializaco']['municipios']);
                unset($tabela['odonto_areas_comercializaco']['metropoles']);

                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_observaco');
                @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observaco']['id']]['data'] =  nl2br($tabela['odonto_observaco']['descricao']);
                unset($tabela['odonto_observaco']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_rede');
                @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['data'] =  nl2br($tabela['odonto_rede']['descricao']);
                unset($tabela['odonto_rede']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'reembolsoEntity');
                @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
                unset($tabela['reembolsoEntity']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_carencia');
                @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['data'] =  nl2br($tabela['odonto_carencia']['descricao']);
                unset($tabela['odonto_carencia']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_dependente');
                @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['data'] =  nl2br($tabela['odonto_dependente']['descricao']);
                unset($tabela['odonto_dependente']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_documento');
                @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['data'] =  nl2br($tabela['odonto_documento']['descricao']);
                unset($tabela['odonto_documento']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_formas_pagamento');
                @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['data'] =  nl2br($tabela['odonto_formas_pagamento']['descricao']);
                unset($tabela['odonto_formas_pagamento']);

                @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
            }

            $this->loadModel('OdontoOperadoras');
            $operadoras = $this->OdontoOperadoras
                ->find('all')
                ->matching('OdontoAreasComercializacoes.OdontoTabelas', function ($q) use ($cotacao) {
                    return $q->where([
                        'OdontoTabelas.tipo_pessoa' => $cotacao['tipo_pessoa'],
                        'OdontoTabelas.new' => 1
                    ]);
                })
                ->group('OdontoOperadoras.id');
        } else {
            $this->Flash->error(__('Sem permissão de visualizar este Cálculo. Cálculo pertence a outro usuário.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('menor_preco', 'maior_preco', 'intermediario', 'cotacao', 'odonto_calculo', 'tabelas_ordenadas', 'operadoras'));
    }

    public function auxGetNomeProduto(&$grupo_adicionado, $produto, $campo)
    {
        $grupo_id = $produto->odonto_produtos_grupo_id;

        if (is_null($grupo_id) || empty($grupo_id)) {
            $nome = $produto->nome . ' • ';
        } else {
            if (in_array($produto->$campo->id, $grupo_adicionado[$grupo_id][$campo])) {
                $nome = "";
            } else {
                $nome = $produto->odonto_produtos_grupo->nome . ' • ';
                $grupo_adicionado[$grupo_id][$campo][] = $produto->$campo->id;
            }
        }
        return $nome;
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($tipo_pessoa = null)
    {
        $sessao  = $this->request->session()->read('Auth.User');
        $odontoCalculo = $this->OdontoCalculos->newEntity();

        if ($this->request->is('post')) {
            $odontoCalculo->tipo_pessoa = $tipo_pessoa;
            $odontoCalculo = $this->OdontoCalculos->patchEntity($odontoCalculo, $this->request->data);
            if ($this->OdontoCalculos->save($odontoCalculo)) {
                $this->Flash->success(__('The odonto calculo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto calculo could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('Estados');
        $estados = $this->Estados
            ->find('list', ['valueField' => 'nome', 'empty' => 'SELECIONE'])
            ->matching('OdontoAreasComercializacoes.OdontoTabelas', function ($q) {
                return $q->where(['OdontoTabelas.validade' => 1]);
            })
            ->order(['Estados.nome' => 'ASC']);

        $this->set(compact('odontoCalculo', 'tipo_pessoa', 'sessao', 'estados'));
        $this->set('_serialize', ['odontoCalculo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Calculo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $odontoCalculo = $this->OdontoCalculos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoCalculo = $this->OdontoCalculos->patchEntity($odontoCalculo, $this->request->data);
            if ($this->OdontoCalculos->save($odontoCalculo)) {
                $this->Flash->success(__('The odonto calculo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto calculo could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('OdontoTabelas');
        $estadosContados = $this->OdontoTabelas
            ->find('all', ['empty' => 'SELECIONE'])
            ->select(['estado_id'])
            ->distinct(['estado_id'])
            ->where(['validade' => 1])
            ->toArray();
        $array = array();
        foreach ($estadosContados as $estadosContados) {
            $array[] = $estadosContados['estado_id'];
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC'], 'empty' => 'SELECIONE'])->where(['id IN' => $array]);
        $this->set(compact('odontoCalculo', 'sessao', 'estados'));
        $this->set('_serialize', ['odontoCalculo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Calculo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoCalculo = $this->OdontoCalculos->get($id);
        if ($this->OdontoCalculos->delete($odontoCalculo)) {
            $this->Flash->success(__('The odonto calculo has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto calculo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function cotar()
    {
        $user_id = $this->request->getSession()->read('Auth.User.id');
        $cotacao = $this->request->getData();

        if ($cotacao['tipo_pessoa'] == "PF") {
            $cotacao['titulares'] = 1;
        }

        switch ($cotacao['tipo_pessoa']) {
            case 'PF':
                $cotacao['quantidade_vidas'] = intval($cotacao['titularesPF']) + intval($cotacao['dependentesPF']);
                break;
            case 'PJ':
                $cotacao['quantidade_vidas'] = intval($cotacao['titularesPJ']) + intval($cotacao['dependentesPJ']);
                break;
        }

        $cotacao['user_id'] = $user_id;
        $cotacao['data'] = Time::now();

        $calculo = $this->OdontoCalculos->newEntity();
        $calculo = $this->OdontoCalculos->patchEntity($calculo, $cotacao);

        if ($this->OdontoCalculos->save($calculo)) {
            $this->loadModel('Users');
            $userLogado = $this->Users->get($calculo['user_id']);

            $userLogado->ultimocalculo = Time::now();
            $userLogado->dirty('ultimocalculo', true);
            $this->Users->save($userLogado);

            $dados['id'] = $calculo->id;
            $dados['msg'] = 'Registro salvo com sucesso.';
            $dados['ok'] = true;
        } else {
            $dados['msg'] = 'Erro ao Salvar Simulação. Tente Novamente.';
            $dados['ok'] = false;
        }

        $this->autoRender = false;
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($dados));
    }


    /**
     * Método para emitir Simulações
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function encontrarTabelas($data = null)
    {
        $this->request->data['data'] = Time::now();

        $calculoArray = $this->request->data();
        if ($calculoArray['tipo_pessoa'] == "PF") {
            $calculoArray['titulares'] = 1;
        }

        $this->loadModel('OdontoOperadoras');
        $odonto_operadoras = $this->OdontoOperadoras->find('all', ['contain' => ['Imagens']]);

        switch ($calculoArray['tipo_pessoa']) {
            case 'PF':
                $calculoArray['quantidade_vidas'] = intval($calculoArray['titularesPF']) + intval($calculoArray['dependentesPF']);
                break;
            case 'PJ':
                $calculoArray['quantidade_vidas'] = intval($calculoArray['titularesPJ']) + intval($calculoArray['dependentesPJ']);
                break;
        }

        $this->loadModel('OdontoTabelas');
        $odonto_tabelas = $this->OdontoTabelas->find('all', [
            'contain' => [
                'OdontoComercializacoes',
                'OdontoAtendimentos',
                'OdontoProdutos' => [
                    'OdontoOperadoras' => [
                        'OdontoCarencias',
                        'OdontoRedes',
                        'OdontoReembolsos',
                        'OdontoDocumentos',
                        'OdontoDependentes',
                        'OdontoObservacaos'
                    ]
                ],
                'Estados'
            ]
        ], ['group' => ['OdontoProdutos.operadora_id']])
            ->order([
                'OdontoOperadoras.prioridade' => 'ASC',
                'OdontoTabelas.prioridade' => 'ASC'
            ])
            ->where([
                'OdontoTabelas.validade' => 1,
                'OdontoTabelas.estado_id' => $calculoArray['estado_id'],
                'OdontoTabelas.tipo_pessoa' => $calculoArray['tipo_pessoa'],
                'OdontoTabelas.minimo_vidas <=' => $calculoArray['quantidade_vidas'],
                'OdontoTabelas.maximo_vidas >=' => $calculoArray['quantidade_vidas']
            ]);

        //        die();
        if (isset($calculoArray['calculo_id'])) {
            $odontoCalculo = $this->OdontoCalculos->get($calculoArray['calculo_id'], [
                'contain' => []
            ]);

            if ($odontoCalculo['edicoes'] <= 10 || $odontoCalculo['edicoes'] == "null") {
                $calculoArray['edicoes'] = $odontoCalculo['edicoes'] + 1;

                $odontoCalculo = $this->OdontoCalculos->patchEntity($odontoCalculo, $calculoArray);
                if ($this->OdontoCalculos->save($odontoCalculo)) {
                    $dados['id'] = $odontoCalculo->id;
                    $dados['msg'] = 'Registro salvo com sucesso.';
                    $dados['ok'] = true;
                } else {
                    $dados['msg'] = 'Erro ao Salvar Simulação. Tente Novamente.';
                    $dados['ok'] = false;
                }
                // return $this->redirect(['action' => 'view', $odontoCalculo->id]);
            }
        }

        if (count($odonto_tabelas->toArray()) > 0) {
            $calculo = $this->OdontoCalculos->newEntity();
            $calculo = $this->OdontoCalculos->patchEntity($calculo, $calculoArray);

            if ($this->OdontoCalculos->save($calculo)) {
                $this->loadModel('Users');
                $userLogado = $this->Users->get($calculo['user_id']);

                $userLogado->ultimocalculo = Time::now();
                $userLogado->dirty('ultimocalculo', true);
                $this->Users->save($userLogado);

                $dados['id'] = $calculo->id;
                $dados['msg'] = 'Registro salvo com sucesso.';
                $dados['ok'] = true;
            } else {
                $dados['msg'] = 'Erro ao Salvar Simulação. Tente Novamente.';
                $dados['ok'] = false;
                // $this->Flash->error(__('Erro ao Salvar Simulação. Tente Novamente'));
            }
        }

        $this->autoRender = false;
        $this->response->type('json');
        $this->response->body(json_encode($dados));
        // foreach ($odonto_tabelas as $tabela) {
        //     $tabelasOrdenadas[$tabela['odonto_produto']['odonto_operadora']['id']][$tabela['odonto_produto']['nome']][$tabela['nome']] = $tabela;
        // }

        // $btnCancelar = '#';
        // $this->set(compact('odonto_tabelas', 'calculo', 'tabelasOrdenadas', 'btnCancelar', 'odonto_operadoras'));
        // $this->render('view');
    }

    /**
     *
     *
     *  Salva filtros do PDF incluind tabelas selecionadas e Gera PDF
     *
     *
     */
    public function pdf()
    {
        $tabelasSelecionadas = $this->request->data;
        foreach ($tabelasSelecionadas as $chave => $tabela) {
            if (
                $chave <> 'coparticipacao' &&
                $chave <> 'tipo_produto_id' &&
                $chave <> 'tipo_id' &&
                $chave <> 'abrangencia_id' &&
                $chave <> 'calculo_id' &&
                $chave <> 'observacao' &&
                $chave <> 'rede' &&
                $chave <> 'reembolso' &&
                $chave <> 'carencia' &&
                $chave <> 'informacao'
            ) {
                if (substr($chave, 0, 8) === 'checkAll') {
                    unset($tabelasSelecionadas[$chave]);
                } else {
                    if ($tabela == 0) {
                        unset($tabelasSelecionadas[$chave]);
                    } else {
                        $tabelasFiltradas[] = $tabela;
                        $findTabelas[] = $tabela;
                    }
                }
            }
        }

        if (!empty($tabelasFiltradas)) {
            //* SALVAR FILTROS E TABELAS */
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $tabelasSelecionadas['user_id'] = $sessao['id'];
            $tabelasSelecionadas['odonto_calculo_id'] = $this->request->data['calculo_id'];
            $tabelasSelecionadas['created'] = Time::now();

            $this->loadModel('OdontoFiltros');
            $filtro = $this->OdontoFiltros->newEntity();

            $filtro = $this->OdontoFiltros->patchEntity($filtro, $tabelasSelecionadas);
            if ($this->OdontoFiltros->save($filtro)) {
                $this->loadModel('OdontoFiltrosTabelas');
                foreach ($tabelasFiltradas as $tabela_filtrada) {
                    $dadosTabelasFiltradas['odonto_filtro_id'] = $filtro->id;
                    $dadosTabelasFiltradas['odonto_tabela_id'] = $tabela_filtrada;

                    $tabelasfiltradas = $this->OdontoFiltrosTabelas->newEntity();
                    $tabelasfiltradas = $this->OdontoFiltrosTabelas->patchEntity($tabelasfiltradas, $dadosTabelasFiltradas);
                    $this->OdontoFiltrosTabelas->save($tabelasfiltradas);
                }
            }

            $calculo = $this->OdontoCalculos->get($this->request->data['calculo_id']);
            $simulacao = $calculo;

            $this->loadModel('OdontoTabelas');
            $odonto_tabelas = $this->OdontoTabelas->find('all', [
                'contain' => [
                    'OdontoComercializacoes',
                    'OdontoAtendimentos',
                    'Estados',
                    'OdontoProdutos' => [
                        'OdontoOperadoras' => [
                            'Imagens',
                            'OdontoFormasPagamentos',
                            'OdontoRedes',
                            'OdontoCarencias',
                            'OdontoReembolsos',
                            'OdontoDocumentos',
                            'OdontoDependentes',
                            'OdontoObservacaos',
                            'OdontoComercializacoes'
                        ],
                    ]
                ]
            ], [
                'order' => ['OdontoOperadoras.nome' => 'ASC']
            ], ['group' => ['OdontoProdutos.odonto_operadora_id']])
                ->where([
                    'OdontoTabelas.id IN' => $findTabelas
                ])->toArray();
            $tabelas = $odonto_tabelas;
            foreach ($odonto_tabelas as $tabela) {
                $tabelasOrdenadas[$tabela['odonto_produto']['odonto_operadora']['id']][$tabela['odonto_produto']['nome']][$tabela['nome']] = $tabela;
            }
            //            debug($this->reque/* s */t->data);
            //            debug($tabelasOrdenadas);
            //            die();


            $calculo['observaco'] = $this->request->data['observacao'];
            $calculo['rede'] = $this->request->data['rede'];
            $calculo['reembolso'] = $this->request->data['reembolso'];
            $calculo['carencia'] = $this->request->data['carencia'];
            $calculo['informacao'] = $this->request->data['documento'];

            $this->loadModel('OdontoOperadoras');
            $op = $this->OdontoOperadoras->find('all', ['contain' => ['Imagens']])->toArray();
            foreach ($op as $o) {
                $odonto_operadoras[$o['id']] = $o;
            }


            //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
            $tipoPDF = $this->request->data("tipopdf");
            switch ($tipoPDF) {
                case "D":
                    $nomePDF = 'Calculo-Odonto-' . $calculo["tipo_pessoa"] . '-' . $calculo['id'] . '.pdf';
                    break;
                case "I":
                    $nomePDF = 'Calculo-Odonto-' . $calculo["tipo_pessoa"] . '-' . $calculo['id'] . '.pdf';
                    break;
                case 'F':
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Odonto-' . $calculo["tipo_pessoa"] . '-' . $calculo['id'] . '.pdf';
                    break;
                default:
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Odonto-' . $calculo["tipo_pessoa"] . '-' . $calculo['id'] . '.pdf';
                    break;
            }




            //            $this->set('simulacao', $simulacao);
            //            $this->set('_serialize', ['simulacao']);
            $this->set(compact('odonto_tabelas', 'tabelas', 'simulacao', 'calculo', 'tabelasOrdenadas', 'odonto_operadoras', 'sessao', 'nomePDF', 'tipoPDF'));
            $this->viewBuilder()->layout('pdf_odonto');
            //
        } else {
            $this->Flash->error(__('Selecione os cálculos que deseja imprimir.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function email($calculo = null, $emailCliente = null)
    {
        $dados = $this->OdontoCalculos->get($calculo);

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');


        if (isset($emailCliente) && $emailCliente <> '') {
            $destinatario = $emailCliente;
        } else {
            $destinatario = $sessao["email"];
        }

        $imagem = rawurlencode($session->read('Auth.User.imagem.nome'));
        $caminho = "uploads/imagens/usuarios/logos/";
        $imagem = $caminho . $imagem;
        if (empty($imagem)) {
            $imagem = 'img/logoCpEmail.png';
        }

        //            debug($sessao);die();
        $email = new Email('default');
        //        $email = new Email();
        $email->transport('calculos');
        $email->from('nao-responda@corretorparceiro.com.br', 'Tabela de Preços - Cálculos')
            ->to($destinatario)
            ->sender('nao-responda@corretorparceiro.com.br', 'Tabela de Preços - Cálculos')
            ->replyTo('nao-responda@corretorparceiro.com.br')
            ->emailFormat('html')
            ->attachments([
                'Calculo-Odonto-Numero-' . $dados['id'] . '.pdf' => [
                    'file' => $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Odonto-' . $dados["tipo_pessoa"] . "-" . $dados['id'] . '.pdf'
                ]
            ])
            ->subject('Calculo Odonto Nº ' . $dados['id'] . ' - Cliente: ' . $dados['nome'] . ' - Ferramentas de Apoio Corretor Parceiro')
            ->send('<html><img src="https://corretorparceiro.com.br/app/' . $imagem . '" width="230"/>'
                . '<br/>'
                . '<br/>'
                . 'À <b>' . $dados['nome'] . '</b>, A/C: <b>' . $dados['contato'] . '</b>.<br/>
            Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.<br/>
            Informamos que os valores, regras de comercialização e condições contratuais são determinadas pelas seguradoras/operadoras dos planos e podem ser alterados pelas mesmas a qualquer momento.<br/>
            Os preços são tabelados e estão sujeitos a confirmação dos valores no ato do fechamento do contrato.<br/>'
                . '<br/>'
                . '<br/>'
                . 'Atenciosamente, ' . $sessao['nome'] . ' ' . $sessao['sobrenome']
                . '<br/> <b>Email:</b> ' . $sessao['email']
                . '<br/> <b>Celular:</b> ' . $sessao['celular']
                . '<br/> <b>WhatsApp:</b> ' . $sessao['whatsapp']
                . '</html>');


        $redirect = strtolower($dados['tipo_pessoa']);

        unlink($_SERVER['DOCUMENT_ROOT'] . 'Calculo-Odonto-' . $dados["tipo_pessoa"] . "-" . $dados['id'] . '.pdf');
        $resposta = 'Cálculo enviado para o e-mail: ' . $destinatario;

        $this->set(compact('resposta'));
    }

    /* Metodo para listar pedf gerados a partir do Cálculo */

    public function listarpdfs($odonto_calculo_id = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        //        $userId = $sessao['id'];

        $calculo = $this->OdontoCalculos->get($odonto_calculo_id)->toArray();
        $this->loadModel('OdontoFiltros');
        $filtros = $this->OdontoFiltros->find('all')->where(['OdontoFiltros.odonto_calculo_id' => $odonto_calculo_id])->order(['OdontoFiltros.created' => 'DESC'])->toArray();
        //        debug($filtros);

        $this->set(compact('filtros', 'calculo'));
    }

    /**
     * Método para filtrar tabelasdentro de um cálculo já gerado
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtropdf($filtro_id = null)
    {
        $this->loadModel('OdontoFiltros');
        $filtro = $this->OdontoFiltros->get($filtro_id)->toArray();

        $this->loadModel('OdontoFiltrosTabelas');
        $tabelas_filtradas = $this->OdontoFiltrosTabelas->find('all')->where(['OdontoFiltrosTabelas.odonto_filtro_id' => $filtro_id])->toArray();

        //Tabelas selecionadas para o PDF
        foreach ($tabelas_filtradas as $tabela) {
            $tabelasIds[] = $tabela['odonto_tabela_id'];
        }

        //Get todas as tabelas do cálculo
        $this->loadModel('OdontoCalculosTabelas');
        $calculos_tabelas = $this->OdontoCalculosTabelas->find('all')
            ->where(['OdontoCalculosTabelas.odonto_calculo_id' => $filtro['odonto_calculo_id']])->toArray();

        $this->loadModel('OdontoTabelas');

        $odonto_tabelas = $this->OdontoTabelas->find('all', [
            'contain' => [
                'OdontoComercializacoes',
                'OdontoAtendimentos',
                'OdontoProdutos' => [

                    'OdontoOperadoras' => [
                        'Imagens', 'OdontoCarencias',
                        'OdontoDocumentos',
                        'OdontoRedes',
                        'OdontoDependentes',
                        'OdontoObservacaos',
                        'OdontoReembolsos'
                    ]
                ]
            ]
        ])
            ->order([
                'OdontoOperadoras.prioridade' => 'ASC',
                'OdontoTabelas.prioridade' => 'ASC'
            ])
            ->where([
                'OdontoTabelas.id IN' => $tabelasIds,
            ])->toArray();
        $totalTabelas = count($odonto_tabelas);
        $totalsemfiltro = count($calculos_tabelas);
        foreach ($odonto_tabelas as $tabela) {
            //            debug($tabela);
            //            die();
            $tabelasOrdenadas[$tabela['odonto_produto']['odonto_operadora']['id']][$tabela['odonto_produto']['nome']][$tabela['nome']] = $tabela;
        }
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');




        $this->loadModel('OdontoOperadoras');
        $operadoras = $this->OdontoOperadoras->find('all', ['contain' => ['Imagens']])->toArray();

        $odonto_calculo = $this->OdontoCalculos->get($filtro['odonto_calculo_id']);

        $this->set(compact('odonto_calculo', 'filtro', 'tabelasOrdenadas', 'operadoras', 'totalTabelas', 'totalsemfiltro'));
        //        $this->render('filtro');
    }

    /**
     *
     *
     *  Reenvia PDF identico ao salvo nos Filtros
     *
     *
     */
    public function reenviarpdf($filtro_id = null)
    {
        $this->loadModel('OdontoFiltros');
        $filtro = $this->OdontoFiltros->get($filtro_id);

        $this->loadModel('OdontoFiltrosTabelas');
        $tabelas_selecionadas = $this->OdontoFiltrosTabelas->find('all')
            ->where(['OdontoFiltrosTabelas.odonto_filtro_id' => $filtro_id])
            ->contain(['OdontoFiltros', 'OdontoTabelas'
            => ['OdontoProdutos'
            => [
                'OdontoOperadoras' => [
                    'Imagens', 'OdontoCarencias',
                    'OdontoReembolsos',
                    'OdontoRedes',
                    'OdontoDocumentos',
                    'OdontoDependentes',
                    'OdontoObservacaos'
                ]
            ], 'OdontoAtendimentos', 'OdontoComercializacoes', 'Estados'], 'OdontoFiltros'])
            ->toArray();
        //        $filtro = $this->OdontoFiltrosTabelas->find('all')
        //                        ->where(['OdontoFiltrosTabelas.odonto_filtro_id' => $filtro])
        //                        ->contain(['OdontoFiltros', 'OdontoTabelas'])->toArray();
        //        $this->OdontoFiltrosTabelas->find('all')



        foreach ($tabelas_selecionadas as $tab) {
            //            debug($tab);die();
            $tabelasIds[$tab['odonto_tabela_id']] = $tab['odonto_tabela'];
        }

        $this->loadModel('OdontoTabelas');

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $userId = $sessao['id'];

        $calculo = $this->OdontoCalculos->get($filtro['odonto_calculo_id']);


        $this->loadModel('OdontoTabelas');
        $odonto_tabelas = $this->OdontoTabelas->find('all')->contain([
            'Estados',
            'OdontoComercializacoes',
            'OdontoAtendimentos',
            'OdontoProdutos' => [
                'OdontoOperadoras' => [
                    'Imagens', 'OdontoCarencias',
                    'OdontoRedes',
                    'OdontoObservacaos',
                    'OdontoDocumentos',
                    'OdontoDependentes',
                    'OdontoReembolsos'
                ]
            ]
        ])
            ->where([
                'OdontoTabelas.id IN' => array_keys($tabelasIds)
            ])
            ->order(['OdontoOperadoras.prioridade' => 'ASC', 'OdontoTabelas.prioridade' => 'ASC'])->toArray();
        foreach ($odonto_tabelas as $tabela) {
            //            debug($tabela['nome']);
            $tabelasOrdenadas[$tabela['odonto_produto']['odonto_operadora']['id']][$tabela['odonto_produto']['nome']][$tabela['nome']] = $tabela;
        }

        //        debug($tabelasOrdenadas);
        //        die();

        $calculo['observaco'] = $filtro['odonto_observacao'];
        $calculo['rede'] = $filtro['odonto_rede'];
        $calculo['reembolso'] = $filtro['odonto_reembolso'];
        $calculo['carencia'] = $filtro['odonto_carencia'];
        $calculo['documento'] = $filtro['odonto_documento'];
        $calculo['dependente'] = $filtro['odonto_dependente'];
        //        debug($calculo);
        //        die();
        //NÃO UTILIZADOS
        //            $simulacao['info'] = 1;
        //            $simulacao['opcionais'] = 1;

        $this->loadModel('OdontoOperadoras');
        $op = $this->OdontoOperadoras->find('all', ['contain' => ['Imagens']])->toArray();
        foreach ($op as $o) {
            $odonto_operadoras[$o['id']] = $o;
        }


        //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
        $tipoPDF = 'F';
        //Nome do PDF
        $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Rapido-Numero-' . $calculo['id'] . '.pdf';

        //        $this->set(compact('odonto_tabelas', 'calculo', 'tabelasOrdenadas', 'odonto_operadoras', 'sessao', 'nomePDF', 'tipoPDF'));
        $this->set(compact('odonto_tabelas', 'calculo', 'tabelasOrdenadas', 'odonto_operadoras', 'sessao', 'nomePDF', 'tipoPDF'));
        $this->viewBuilder()->layout('pdf_odonto');
        $this->render('pdf');
    }

    /* Metodo para listar pedf gerados a partir do Cálculo */

    public function alterarStatus($calculo_id = null, $status_id = null)
    {
        $calculo = $this->OdontoCalculos->get($calculo_id);
        $calculo->status_id = $status_id;

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->Alertas->criarAlerta($sessao["id"], ($calculo["tipo_pessoa"] = "PF" ? "OPF" : "OPJ"), $calculo_id, $status_id);


        $this->OdontoCalculos->save($calculo);
    }

    public function salvaFiltros()
    {
        $data = $this->request->getData();
        $tabelasSelecionadas = $this->request->getData();

        foreach ($tabelasSelecionadas as $chave => $tabela) {
            if (
                $chave <> 'coparticipacao' &&
                $chave <> 'tipo_produto_id' &&
                $chave <> 'tipo_id' &&
                $chave <> 'abrangencia_id' &&
                $chave <> 'calculo_id' &&
                $chave <> 'observacao' &&
                $chave <> 'rede' &&
                $chave <> 'reembolso' &&
                $chave <> 'carencia' &&
                $chave <> 'documento' &&
                $chave <> 'informacao'
            ) {
                if (substr($chave, 0, 8) === 'checkAll') {
                    unset($tabelasSelecionadas[$chave]);
                } else {
                    if ($tabela == 0) {
                        unset($tabelasSelecionadas[$chave]);
                    } else {
                        // echo $chave . " - ";
                        $tabelasFiltradas[] = $tabela;
                    }
                }
            }
        }

        if (!empty($tabelasFiltradas)) {

            //* SALVAR FILTROS E TABELAS */
            $tabelasSelecionadas['user_id'] = $this->request->session()->read('Auth.User.id');
            $tabelasSelecionadas['odonto_calculo_id'] = $data['calculo_id'];
            $tabelasSelecionadas['odonto_observacao'] = $data['observacao'];
            $tabelasSelecionadas['odonto_carencia'] = $data['carencia'];
            $tabelasSelecionadas['odonto_reembolso'] = $data['reembolso'];
            $tabelasSelecionadas['odonto_rede'] = $data['rede'];
            $tabelasSelecionadas['odonto_documento'] = $data['documento'];
            if (isset($data['dependente'])) {
                $tabelasSelecionadas['odonto_dependente'] = $data['dependente'];
            }

            $this->loadModel('OdontoFiltros');
            $filtro = $this->OdontoFiltros->newEntity();
            $filtro = $this->OdontoFiltros->patchEntity($filtro, $tabelasSelecionadas);
            if ($this->OdontoFiltros->save($filtro)) {
                $this->loadModel('OdontoFiltrosTabelas');
                foreach ($tabelasFiltradas as $tabela_filtrada) {

                    $dadosTabelasFiltradas['odonto_filtro_id'] = $filtro->id;
                    $dadosTabelasFiltradas['odonto_tabela_id'] = $tabela_filtrada;

                    $tabelasfiltradas = $this->OdontoFiltrosTabelas->newEntity();
                    $tabelasfiltradas = $this->OdontoFiltrosTabelas->patchEntity($tabelasfiltradas, $dadosTabelasFiltradas);
                    $this->OdontoFiltrosTabelas->save($tabelasfiltradas);
                }
            }
            return $this->redirect(['action' => 'cotacao', $filtro->id]);
        } else {
            $this->request->session()->write('erro', 'S');
            //$this->Flash->error(__('Selecione os cálculos que deseja imprimir.'));
            return $this->redirect(['action' => 'view', $tabelasSelecionadas['calculo_id']]);
        }
    }

    public function cotacao($id)
    {
        try {
            $this->loadModel('odontoFiltros');
            $filtro = $this->odontoFiltros->get($id);
        } catch (\Exception $e) {
            return $this->render('cotacao_expirada');
        }
        $this->loadModel('OdontoFiltrosTabelas');
        $findTabelas = $this->OdontoFiltrosTabelas->find('list', ['keyField' => 'odonto_tabela_id', 'valueField' => 'odonto_tabela_id'])->where(['odonto_filtro_id' => $id])->toArray();
        $cotacao = $simulacao = $this->OdontoCalculos->get($filtro->odonto_calculo_id);
        $this->loadModel('OdontoTabelas');
        $tabelas = $this->OdontoTabelas
            ->find('all', [
                'contain' => [
                    'OdontoAtendimentos',
                    'OdontoFormasPagamentos',
                    'OdontoCarencias',
                    'OdontoRedes',
                    'OdontoReembolsos',
                    'OdontoDocumentos',
                    'OdontoDependentes',
                    'OdontoObservacaos',
                    'OdontoAreasComercializacoes' => [
                        'Metropoles',
                        'Municipios' => ['Estados'],
                        'OdontoOperadoras' => ['Imagens']
                    ]
                ]
            ])
            ->where(['OdontoTabelas.id IN' => $findTabelas])
            ->toArray();

        $tabelas_ordenadas = [];
        $area_adicionada = [];
        foreach ($tabelas as $tabela) {
            $operadora_id = $tabela->odonto_areas_comercializaco->odonto_operadora_id;
            $tabelas_ordenadas[$operadora_id]['operadora'] = $tabela->odonto_areas_comercializaco->odonto_operadora;

            @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['odonto_atendimento']['nome']] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_atendimento');

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_areas_comercializaco');
            if (!in_array($tabela['odonto_areas_comercializaco']['id'], $area_adicionada)) {
                foreach ($tabela['odonto_areas_comercializaco']['metropoles'] as $metropole) {
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                }
                foreach ($tabela['odonto_areas_comercializaco']['municipios'] as $municipio) {
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                }
                $area_adicionada[] = $tabela['odonto_areas_comercializaco']['id'];
            }
            unset($tabela['odonto_areas_comercializaco']['municipios']);
            unset($tabela['odonto_areas_comercializaco']['metropoles']);

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_observaco');
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observaco']['id']]['data'] =  nl2br($tabela['odonto_observaco']['descricao']);
            unset($tabela['odonto_observaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_rede');
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['data'] =  nl2br($tabela['odonto_rede']['descricao']);
            unset($tabela['odonto_rede']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'reembolsoEntity');
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
            unset($tabela['reembolsoEntity']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_carencia');
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['data'] =  nl2br($tabela['odonto_carencia']['descricao']);
            unset($tabela['odonto_carencia']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_dependente');
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['data'] =  nl2br($tabela['odonto_dependente']['descricao']);
            unset($tabela['odonto_dependente']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_documento');
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['data'] =  nl2br($tabela['odonto_documento']['descricao']);
            unset($tabela['odonto_documento']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_formas_pagamento');
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['data'] =  nl2br($tabela['odonto_formas_pagamento']['descricao']);
            unset($tabela['odonto_formas_pagamento']);

            @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
        }

        $tabelasOrdenadas = $tabelas_ordenadas;
        $user = $this->OdontoCalculos->Users->get($cotacao->user_id);
        if ($user['imagem_id'] != null) {
            $imagem = $this->OdontoCalculos->Users->Imagens->get($user['imagem_id']);
        }
        unset($user['password']);
        $sessao = $this->request->getSession()->read('Auth.User');

        $simulacao['carencia'] = $filtro['odonto_carencia'];
        $simulacao['reembolso'] = $filtro['odonto_reembolso'];
        $simulacao['rede'] = $filtro['odonto_rede'];
        $simulacao['documento'] = $filtro['odonto_documento'];
        $simulacao['observaco'] = $filtro['odonto_observacao'];
        $simulacao['dependente'] = $filtro['odonto_dependente'];

        $this->set(compact('tabelas', 'cotacao', 'simulacao', 'tabelasOrdenadas', 'tabelas_ordenadas', 'sessao', 'user', 'imagem', 'filtro', 'findTabelas'));
        if ($this->request->getData('tipo') && $this->request->getData('tipo') === "DI") {
            //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
            $tipoPDF = $this->request->getData("tipo");
            switch ($tipoPDF) {
                case "D":
                    $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                    break;
                case "I":
                    $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                    break;
                case 'F':
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                    break;
                default:
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                    break;
            }

            $nomePDF = transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove', $nomePDF);

            $this->viewBuilder()->setLayout('pdf');
            $this->render('pdf');
        }
    }

    public function pdfCotacao($id)
    {
        try {
            $this->loadModel('odontoFiltros');
            $filtro = $this->odontoFiltros->get($id);
        } catch (\Exception $e) {
            return $this->render('cotacao_expirada');
        }
        $this->loadModel('OdontoFiltrosTabelas');
        $findTabelas = $this->OdontoFiltrosTabelas->find('list', ['keyField' => 'odonto_tabela_id', 'valueField' => 'odonto_tabela_id'])->where(['odonto_filtro_id' => $id])->toArray();
        $cotacao = $simulacao = $this->OdontoCalculos->get($filtro->odonto_calculo_id);
        $this->loadModel('OdontoTabelas');
        $tabelas = $this->OdontoTabelas
            ->find('all', [
                'contain' => [
                    'OdontoAtendimentos',
                    'OdontoFormasPagamentos',
                    'OdontoCarencias',
                    'OdontoRedes',
                    'OdontoReembolsos',
                    'OdontoDocumentos',
                    'OdontoDependentes',
                    'OdontoObservacaos',
                    'OdontoAreasComercializacoes' => [
                        'Metropoles',
                        'Municipios' => ['Estados'],
                        'OdontoOperadoras' => ['Imagens']
                    ]
                ]
            ])
            ->where(['OdontoTabelas.id IN' => $findTabelas])
            ->toArray();

        $tabelas_ordenadas = [];
        $area_adicionada = [];
        foreach ($tabelas as $tabela) {
            $operadora_id = $tabela->odonto_areas_comercializaco->odonto_operadora_id;
            $tabelas_ordenadas[$operadora_id]['operadora'] = $tabela->odonto_areas_comercializaco->odonto_operadora;

            @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['odonto_atendimento']['nome']] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_atendimento');

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_areas_comercializaco');
            if (!in_array($tabela['odonto_areas_comercializaco']['id'], $area_adicionada)) {
                foreach ($tabela['odonto_areas_comercializaco']['metropoles'] as $metropole) {
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                }
                foreach ($tabela['odonto_areas_comercializaco']['municipios'] as $municipio) {
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                }
                $area_adicionada[] = $tabela['odonto_areas_comercializaco']['id'];
            }
            unset($tabela['odonto_areas_comercializaco']['municipios']);
            unset($tabela['odonto_areas_comercializaco']['metropoles']);

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_observaco');
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observaco']['id']]['data'] =  nl2br($tabela['odonto_observaco']['descricao']);
            unset($tabela['odonto_observaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_rede');
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['data'] =  nl2br($tabela['odonto_rede']['descricao']);
            unset($tabela['odonto_rede']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'reembolsoEntity');
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
            unset($tabela['reembolsoEntity']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_carencia');
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['data'] =  nl2br($tabela['odonto_carencia']['descricao']);
            unset($tabela['odonto_carencia']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_dependente');
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['data'] =  nl2br($tabela['odonto_dependente']['descricao']);
            unset($tabela['odonto_dependente']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_documento');
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['data'] =  nl2br($tabela['odonto_documento']['descricao']);
            unset($tabela['odonto_documento']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'odonto_formas_pagamento');
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['data'] =  nl2br($tabela['odonto_formas_pagamento']['descricao']);
            unset($tabela['odonto_formas_pagamento']);

            @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
        }

        $tabelasOrdenadas = $tabelas_ordenadas;
        $user = $this->OdontoCalculos->Users->get($cotacao->user_id);
        if ($user['imagem_id'] != null) {
            $imagem = $this->OdontoCalculos->Users->Imagens->get($user['imagem_id']);
        }
        unset($user['password']);
        $sessao = $this->request->getSession()->read('Auth.User');

        $simulacao['carencia'] = $filtro['odonto_carencia'];
        $simulacao['reembolso'] = $filtro['odonto_reembolso'];
        $simulacao['rede'] = $filtro['odonto_rede'];
        $simulacao['documento'] = $filtro['odonto_documento'];
        $simulacao['observaco'] = $filtro['odonto_observacao'];
        $simulacao['dependente'] = $filtro['odonto_dependente'];

        //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
        $tipoPDF = $this->request->getData("tipopdf");
        switch ($tipoPDF) {
            case "D":
                $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                break;
            case "I":
                $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                break;
            case 'F':
                $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                break;
            default:
                $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                break;
        }

        $nomePDF = transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove', $nomePDF);

        $this->viewBuilder()->layout('pdf');
        $this->set(compact('tabelas', 'cotacao', 'simulacao', 'tabelasOrdenadas', 'tabelas_ordenadas', 'sessao', 'user', 'imagem', 'filtro', 'findTabelas'));
        $this->set(compact('nomePDF', 'tipoPDF'));
    }

    public function visualizarCotacao($id)
    {
        try {
            $this->loadModel('odontoFiltros');
            $filtro = $this->odontoFiltros->get($id);
        } catch (\Exception $e) {
            return $this->render('cotacao_expirada');
        }

        $this->loadModel('OdontoFiltrosTabelas');
        $findTabelas = $this->OdontoFiltrosTabelas->find('list', ['keyField' => 'odonto_tabela_id', 'valueField' => 'odonto_tabela_id'])->where(['odonto_filtro_id' => $id])->toArray();

        $simulacao = $this->OdontoCalculos->get($filtro->odonto_calculo_id);

        $this->loadModel('OdontoTabelas');
        $tabelas = $this->OdontoTabelas->find('all')
            ->contain([
                'OdontoComercializacoes',
                'OdontoAtendimentos',
                'Estados',
                'OdontoAreasComercializacoes' => [
                    'Municipios' => ['Estados'],
                    'OdontoOperadoras' => [
                        'Imagens',
                        'OdontoFormasPagamentos',
                        'OdontoRedes',
                        'OdontoCarencias',
                        'OdontoReembolsos',
                        'OdontoDocumentos',
                        'OdontoDependentes',
                        'OdontoObservacaos',
                        'OdontoComercializacoes'
                    ],
                ]
            ])
            ->where([
                'OdontoTabelas.id IN' => $findTabelas
            ])->toArray();

        $tabelas_ordenadas = [];
        foreach ($tabelas as $tabela) {
            $tabelas_ordenadas[$tabela['odonto_areas_comercializaco']['odonto_operadora']['id']]['dados'] =  $tabela['odonto_areas_comercializaco']['odonto_operadora'];
            $tabelas_ordenadas[$tabela['odonto_areas_comercializaco']['odonto_operadora']['id']]['tabelas'][] = $tabela;
        }

        $this->loadModel('OdontoOperadoras');
        $odonto_operadoras = $this->OdontoOperadoras->find('all', ['contain' => ['Imagens']])->toArray();

        $user = $this->OdontoCalculos->Users->get($simulacao->user_id);
        if ($user['imagem_id'] != null) {
            $imagem = $this->OdontoCalculos->Users->Imagens->get($user['imagem_id']);
        }
        unset($user['password']);
        $sessao = $this->request->getSession()->read('Auth.User');

        $simulacao['carencia'] = $filtro['odonto_carencia'];
        $simulacao['reembolso'] = $filtro['odonto_reembolso'];
        $simulacao['rede'] = $filtro['odonto_rede'];
        $simulacao['documento'] = $filtro['odonto_documento'];
        $simulacao['observaco'] = $filtro['odonto_observacao'];
        $simulacao['dependente'] = $filtro['odonto_dependente'];

        //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
        $tipoPDF = $this->request->getData("tipopdf");
        if (!is_null($tipoPDF)) {

            switch ($tipoPDF) {
                case "D":
                    $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '.pdf';
                    break;
                case "I":
                    $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '.pdf';
                    break;
                case 'F':
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '.pdf';
                    break;
                default:
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '.pdf';
                    break;
            }
            $this->set(compact('tabelas', 'simulacao', 'tabelas_ordenadas', 'tabelasOrdenadas', 'operadoras', 'sessao', 'user', 'imagem', 'filtro', 'findTabelas'));
            $this->set(compact('nomePDF', 'tipoPDF'));

            $this->viewBuilder()->setClassName('CakePdf.Pdf');
            // $this->viewBuilder()->setLayout('pdf');
            // $this->render("pdf");
        } else {

            $this->set(compact('tabelas', 'simulacao', 'tabelas_ordenadas', 'odonto_operadoras', 'sessao', 'user', 'imagem', 'filtro', 'findTabelas'));
            $this->viewBuilder()->setLayout('link_tela');
        }
    }
}
