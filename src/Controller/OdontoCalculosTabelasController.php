<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoCalculosTabelas Controller
 *
 * @property \App\Model\Table\OdontoCalculosTabelasTable $OdontoCalculosTabelas
 */
class OdontoCalculosTabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OdontoCalculos', 'OdontoTabelas']
        ];
        $odontoCalculosTabelas = $this->paginate($this->OdontoCalculosTabelas);

        $this->set(compact('odontoCalculosTabelas'));
        $this->set('_serialize', ['odontoCalculosTabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Calculos Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoCalculosTabela = $this->OdontoCalculosTabelas->get($id, [
            'contain' => ['OdontoCalculos', 'OdontoTabelas']
        ]);

        $this->set('odontoCalculosTabela', $odontoCalculosTabela);
        $this->set('_serialize', ['odontoCalculosTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoCalculosTabela = $this->OdontoCalculosTabelas->newEntity();
        if ($this->request->is('post')) {
            $odontoCalculosTabela = $this->OdontoCalculosTabelas->patchEntity($odontoCalculosTabela, $this->request->data);
            if ($this->OdontoCalculosTabelas->save($odontoCalculosTabela)) {
                $this->Flash->success(__('The odonto calculos tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto calculos tabela could not be saved. Please, try again.'));
            }
        }
        $odontoCalculos = $this->OdontoCalculosTabelas->OdontoCalculos->find('list', ['limit' => 200]);
        $odontoTabelas = $this->OdontoCalculosTabelas->OdontoTabelas->find('list', ['limit' => 200]);
        $this->set(compact('odontoCalculosTabela', 'odontoCalculos', 'odontoTabelas'));
        $this->set('_serialize', ['odontoCalculosTabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Calculos Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoCalculosTabela = $this->OdontoCalculosTabelas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoCalculosTabela = $this->OdontoCalculosTabelas->patchEntity($odontoCalculosTabela, $this->request->data);
            if ($this->OdontoCalculosTabelas->save($odontoCalculosTabela)) {
                $this->Flash->success(__('The odonto calculos tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto calculos tabela could not be saved. Please, try again.'));
            }
        }
        $odontoCalculos = $this->OdontoCalculosTabelas->OdontoCalculos->find('list', ['limit' => 200]);
        $odontoTabelas = $this->OdontoCalculosTabelas->OdontoTabelas->find('list', ['limit' => 200]);
        $this->set(compact('odontoCalculosTabela', 'odontoCalculos', 'odontoTabelas'));
        $this->set('_serialize', ['odontoCalculosTabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Calculos Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoCalculosTabela = $this->OdontoCalculosTabelas->get($id);
        if ($this->OdontoCalculosTabelas->delete($odontoCalculosTabela)) {
            $this->Flash->success(__('The odonto calculos tabela has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto calculos tabela could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
