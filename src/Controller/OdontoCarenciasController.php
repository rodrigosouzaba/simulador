<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoCarencias Controller
 *
 * @property \App\Model\Table\OdontoCarenciasTable $OdontoCarencias
 */
class OdontoCarenciasController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Odonto');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $odonto_operadoras = $this->OdontoCarencias->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $estados = $this->OdontoCarencias->OdontoOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => '1 = 1 '])->toArray();
        $estados = $this->OdontoCarencias->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(['id IN' => $estados]);
        $this->set(compact('estados'));
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Carencia id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoCarencia = $this->OdontoCarencias->get($id, [
            'contain' => ['OdontoOperadoras', 'OdontoProdutos']
        ]);

        $this->set('odontoCarencia', $odontoCarencia);
        $this->set('_serialize', ['odontoCarencia']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoCarencia = $this->OdontoCarencias->newEntity();
        if ($this->request->is('post')) {
            $odontoCarencia = $this->OdontoCarencias->patchEntity($odontoCarencia, $this->request->data);

            if ($this->OdontoCarencias->save($odontoCarencia)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->matching('OdontoOperadoras', function ($q) {
            return $q;
        });

        $this->set(compact('odontoCarencia', 'estados'));
        $this->set('_serialize', ['odontoCarencia']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Carencia id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoCarencia = $this->OdontoCarencias->get($id, [
            'contain' => ['OdontoOperadoras']
        ]);
        $odontoCarencia->estado_id = $odontoCarencia->odonto_operadora->estado_id;
        $odontoCarencia->tipo_pessoa = $odontoCarencia->odonto_operadora->tipo_pessoa;
        $odontoCarencia->nova = $odontoCarencia->odonto_operadora->nova;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoCarencia = $this->OdontoCarencias->patchEntity($odontoCarencia, $this->request->data);
            if ($this->OdontoCarencias->save($odontoCarencia)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $estados = $this->OdontoCarencias->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoOperadoras = $this->OdontoCarencias->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['tipo_pessoa' => $odontoCarencia->tipo_pessoa])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $this->set(compact('odontoCarencia', 'odontoOperadoras', 'estados'));
        $this->set('_serialize', ['odontoCarencia']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Carencia id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoCarencia = $this->OdontoCarencias->get($id);
        if ($this->OdontoCarencias->delete($odontoCarencia)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $odonto_carencias = $this->OdontoCarencias->find('all', ['limit' => 200, 'valueField' => 'nome', 'conditions' => '1 = 1 '])->where(["odonto_operadora_id" => $id])->contain(["OdontoOperadoras"]);

        $this->set(compact('odonto_carencias'));
        $this->set('_serialize', ['odonto_carencias']);
    }

    public function filtroEstado()
    {

        $estado = $this->request->data()['estado_id'];

        $operadoras = $this->OdontoCarencias->OdontoOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            }, 'conditions' => '1 = 1 ',
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])
            ->where(['estado_id' => $estado]);

        $this->set(compact('operadoras'));
    }

    public function filtroIndex($nova = null, $tipo = null, $estado = null, $operadora = null)
    {
        $data = $this->Odonto->filtroIndex($nova, $tipo, $estado, $operadora);
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }

    public function getOperadoras($nova = null, $tipo = null)
    {
        $operadoras = $this->Odonto->getOperadoras($nova, $tipo);
        $this->set(compact('operadoras'));
    }


    public function getEstadosFromNova(int $nova)
    {
        $this->set('estados', $this->Odonto->getEstadosFromNova($nova));
    }
}
