<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoCoberturas Controller
 *
 * @property \App\Model\Table\OdontoCoberturasTable $OdontoCoberturas */
class OdontoCoberturasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $odontoCoberturas = $this->paginate($this->OdontoCoberturas);

        $this->set(compact('odontoCoberturas'));
        $this->set('_serialize', ['odontoCoberturas']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Cobertura id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoCobertura = $this->OdontoCoberturas->get($id, [
            'contain' => []
        ]);

        $this->set('odontoCobertura', $odontoCobertura);
        $this->set('_serialize', ['odontoCobertura']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoCobertura = $this->OdontoCoberturas->newEntity();
        if ($this->request->is('post')) {
            $odontoCobertura = $this->OdontoCoberturas->patchEntity($odontoCobertura, $this->request->data);
            if ($this->OdontoCoberturas->save($odontoCobertura)) {
                $this->Flash->success(__('The odonto cobertura has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto cobertura could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('odontoCobertura'));
        $this->set('_serialize', ['odontoCobertura']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Cobertura id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoCobertura = $this->OdontoCoberturas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoCobertura = $this->OdontoCoberturas->patchEntity($odontoCobertura, $this->request->data);
            if ($this->OdontoCoberturas->save($odontoCobertura)) {
                $this->Flash->success(__('The odonto cobertura has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto cobertura could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('odontoCobertura'));
        $this->set('_serialize', ['odontoCobertura']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Cobertura id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoCobertura = $this->OdontoCoberturas->get($id);
        if ($this->OdontoCoberturas->delete($odontoCobertura)) {
            $this->Flash->success(__('The odonto cobertura has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto cobertura could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
