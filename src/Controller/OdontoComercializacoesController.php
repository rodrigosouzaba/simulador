<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoComercializacoes Controller
 *
 * @property \App\Model\Table\OdontoComercializacoesTable $OdontoComercializacoes
 */
class OdontoComercializacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $odonto_operadoras = $this->OdontoComercializacoes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $estados = $this->OdontoComercializacoes->OdontoOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => '1 = 1 '])->toArray();
        $estados = $this->OdontoComercializacoes->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados'));
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Comercializaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoComercializaco = $this->OdontoComercializacoes->get($id, [
            'contain' => ['Estados', 'OdontoOperadoras']
        ]);

        $this->set('odontoComercializaco', $odontoComercializaco);
        $this->set('_serialize', ['odontoComercializaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoComercializaco = $this->OdontoComercializacoes->newEntity();
        if ($this->request->is('post')) {
            $odontoComercializaco = $this->OdontoComercializacoes->patchEntity($odontoComercializaco, $this->request->data);
            $escolhidos = $this->request->data['lista_municipios_escolhidos'];
            $municipios = array();
            foreach ($escolhidos as $municipio) {
                array_push($municipios, $this->OdontoComercializacoes->Municipios->get($municipio));
            }
            if ($this->OdontoComercializacoes->save($odontoComercializaco)) {
                $this->OdontoComercializacoes->Municipios->link($odontoComercializaco, $municipios);
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $municipios = "";
        $municipios_tabela = "";
        $estados = $this->OdontoComercializacoes->Estados->find('list', ['valueField' => 'nome']);
        $odontoOperadoras = $this->OdontoComercializacoes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $this->set(compact('odontoComercializaco', 'estados', 'odontoOperadoras', 'municipios', 'municipios_tabela'));
        $this->set('_serialize', ['odontoComercializaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Comercializaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoComercializaco = $this->OdontoComercializacoes->get($id, [
            'contain' => ['Municipios', 'OdontoOperadoras']
        ]);
        $odontoComercializaco->tipo_pessoa = $odontoComercializaco->odonto_operadora->tipo_pessoa;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoComercializaco = $this->OdontoComercializacoes->patchEntity($odontoComercializaco, $this->request->data);
            $escolhidos = $this->request->data['lista_municipios_escolhidos'];
            $municipios = array();
            //$this->loadModel('Municipios');
            foreach ($escolhidos as $municipio) {
                array_push($municipios, $this->OdontoComercializacoes->Municipios->get($municipio));
            }
            if ($this->OdontoComercializacoes->save($odontoComercializaco)) {
                $this->OdontoComercializacoes->Municipios->MunicipiosOdontoComercializacoes->deleteAll(['odonto_comercializacao_id' => $id]);
                $this->OdontoComercializacoes->Municipios->link($odontoComercializaco, $municipios);
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $estados = $this->OdontoComercializacoes->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoOperadoras = $this->OdontoComercializacoes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])
            ->where(['tipo_pessoa' => $odontoComercializaco->tipo_pessoa])
            ->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $municipios = $odontoComercializaco['municipios'];
        $municipios_tabela = array();
        foreach ($municipios as $i => $municipio) {
            $municipios_tabela[$municipio['id']] = $municipio['nome'];
        }
        $municipios = $this->OdontoComercializacoes->Municipios->find('all')->where(['estado_id' => $odontoComercializaco['estado_id']])->toArray();
        foreach ($municipios as  $municipio) {
            if (array_key_exists($municipio->id, $municipios_tabela)) {
                unset($municipios[$municipio->id]);
            }
        }
        $this->set(compact('odontoComercializaco', 'estados', 'odontoOperadoras', 'municipios', 'municipios_tabela'));
        $this->set('_serialize', ['odontoComercializaco']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Comercializaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoComercializaco = $this->OdontoComercializacoes->get($id);
        if ($this->OdontoComercializacoes->delete($odontoComercializaco)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }


    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $odonto_comercializacoes = $this->OdontoComercializacoes->find('all', ['contain' => ['Estados', 'OdontoOperadoras' => ['Imagens']]])
            ->where(['OdontoComercializacoes.odonto_operadora_id' => $id])
            ->order('OdontoComercializacoes.nome')->toArray();
        $odonto_operadoras = $this->OdontoComercializacoes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            }, 
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $this->set(compact('odonto_comercializacoes', 'odonto_operadoras'));
        $this->set('_serialize', ['odonto_comercializacoes']);
    }

    public function filtroEstado()
    {

        $estado = $this->request->data()['estado_id'];

        $operadoras = $this->OdontoComercializacoes->OdontoOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            }, 'conditions' => '1 = 1 ',
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])
            ->where(['estado_id' => $estado]);

        $this->set(compact('operadoras'));
    }

    public function getMunicipios($id = null)
    {
        $municipios_tabela = '';
        $municipios = $this->OdontoComercializacoes->Estados->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $id]);
        $this->set(compact('municipios', 'municipios_tabela'));
    }

    public function filtroIndex($estado = null, $tipo_pessoa = null)
    {
        $operadoras = $this->OdontoComercializacoes->OdontoOperadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'tipo_pessoa' => $tipo_pessoa, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'tipo_pessoa' => $tipo_pessoa, 'status IS' => null]
                ]
            ])
            ->orderASC('nome');
        $count = $operadoras->count();
        $operadoras = $operadoras->toArray();
        $this->set(compact("operadoras"));
    }

    public function filtroOperadoras($id = null, $tipo_pessoa = null)
    {
        $operadoras = $this->OdontoComercializacoes->OdontoOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            }, 'conditions' => '1 = 1 ',
            'order' => ['nome' => 'ASC']
        ])->where(['estado_id' => $id, 'tipo_pessoa' => $tipo_pessoa]);
        $this->set(compact('operadoras'));
    }
}
