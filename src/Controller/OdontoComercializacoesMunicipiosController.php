<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoComercializacoesMunicipios Controller
 *
 * @property \App\Model\Table\OdontoComercializacoesMunicipiosTable $OdontoComercializacoesMunicipios
 */
class OdontoComercializacoesMunicipiosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Municipios', 'OdontoTabelas']
        ];
        $odontoComercializacoesMunicipios = $this->paginate($this->OdontoComercializacoesMunicipios);

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);

        $this->set(compact('odontoComercializacoesMunicipios', 'estados'));
        $this->set('_serialize', ['odontoComercializacoesMunicipios']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Comercializacoes Municipio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoComercializacoesMunicipio = $this->OdontoComercializacoesMunicipios->get($id, [
            'contain' => ['Municipios', 'OdontoTabelas']
        ]);

        $this->set('odontoComercializacoesMunicipio', $odontoComercializacoesMunicipio);
        $this->set('_serialize', ['odontoComercializacoesMunicipio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoComercializacoesMunicipio = $this->OdontoComercializacoesMunicipios->newEntity();
        if ($this->request->is('post')) {
            $odontoComercializacoesMunicipio = $this->OdontoComercializacoesMunicipios->patchEntity($odontoComercializacoesMunicipio, $this->request->data);
            if ($this->OdontoComercializacoesMunicipios->save($odontoComercializacoesMunicipio)) {
                $this->Flash->success(__('The odonto comercializacoes municipio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto comercializacoes municipio could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->OdontoComercializacoesMunicipios->Municipios->find('list', ['limit' => 200]);
        $odontoTabelas = $this->OdontoComercializacoesMunicipios->OdontoTabelas->find('list', ['limit' => 200]);
        $this->set(compact('odontoComercializacoesMunicipio', 'municipios', 'odontoTabelas'));
        $this->set('_serialize', ['odontoComercializacoesMunicipio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Comercializacoes Municipio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($odontoTabela)
    {
        $odontoComercializacoesMunicipio = $this->OdontoComercializacoesMunicipios->newEntity();
        $odontoTabela = $this->OdontoComercializacoesMunicipios->OdontoTabelas->get($odontoTabela);


        $municipios_tabela = $this->OdontoComercializacoesMunicipios->Municipios->find('list', ['valueField' => 'nome']);
        $municipios_tabela->matching('OdontoTabelas', function ($q) use ($odontoTabela) {
            return $q->where(['OdontoTabelas.id' => $odontoTabela->id]);
        })->where(['1 = 1']);

        if ($this->request->is(['patch', 'post', 'put'])) {

            // Salva os municipios

            $data = $this->request->data;
            $erros = 0;
            foreach ($data['lista_municipios_escolhidos'] as $municipio) {
                // Se o municipio não estiver salvo, salva.
                if (!key_exists($municipio, $municipios_tabela->toArray())) {

                    $odontoComercializacoesMunicipio = $this->OdontoComercializacoesMunicipios->newEntity();

                    $dados = [
                        'odonto_tabela_id' => $data['odonto_tabela_id'],
                        'municipio_id' => $municipio
                    ];

                    $odontoComercializacoesMunicipio = $this->OdontoComercializacoesMunicipios->patchEntity($odontoComercializacoesMunicipio, $dados);

                    if (!$this->OdontoComercializacoesMunicipios->save($odontoComercializacoesMunicipio)) {
                        $erros++;
                    }
                }
            }

            // Remove municipios não selecionados

            $municipios_removidos = array_diff(array_keys($municipios_tabela->toArray()), $data['lista_municipios_escolhidos']);
            foreach ($municipios_removidos as $municipio) {
                $municipio = $this->OdontoComercializacoesMunicipios->find('list')->where(['odonto_tabela_id' => $odontoTabela->id, 'municipio_id' => $municipio]);

                foreach ($municipio as $registro) {
                    $registro = $this->OdontoComercializacoesMunicipios->get($registro);
                    if (!$this->OdontoComercializacoesMunicipios->delete($registro)) {
                        $erros++;
                    }
                }
            }

            if ($erros == 0) {
                $this->Flash->success(__('The pf comercializacoes municipio has been saved.'));
                return $this->redirect(['controller' => 'users', 'action' => 'configurar-odonto', 'destino' => 'OdontoComercializacoesMunicipios']);
            } else {
                $this->Flash->error(__('The pf comercializacoes municipio could not be saved. Please, try again.'));
            }
        }

        $municipios = $this->OdontoComercializacoesMunicipios->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $odontoTabela->estado_id]);
        $this->set(compact('odontoComercializacoesMunicipio', 'odontoTabela', 'municipios', 'municipios_tabela'));
        $this->set('_serialize', ['odontoComercializacoesMunicipio']);
    }


    /**
     * Delete method
     *
     * @param string|null $id Odonto Comercializacoes Municipio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoComercializacoesMunicipio = $this->OdontoComercializacoesMunicipios->get($id);
        if ($this->OdontoComercializacoesMunicipios->delete($odontoComercializacoesMunicipio)) {
            $this->Flash->success(__('The odonto comercializacoes municipio has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto comercializacoes municipio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function filtroEstado($estado)
    {
        if (!empty($estado)) {
            $this->loadModel('OdontoOperadoras');
            $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome'])
                ->where(['estado_id' => $estado, 'NOT' => ['status' => 'INATIVA']])
                ->orderASC('nome');
            $this->set(compact('operadoras'));
        }
    }

    public function filtroOperadora($operadora)
    {
        if (!empty($operadora)) {
            $this->loadModel('OdontoTabelas');
            $tabelas = $this->OdontoTabelas->find('all')
                ->contain(['Municipios'])
                ->where(['OdontoTabelas.odonto_operadora_id' => $operadora, 'validade' => 1]);
            $this->set(compact('tabelas'));
        }
    }
}
