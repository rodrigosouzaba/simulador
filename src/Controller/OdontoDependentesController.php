<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoDependentes Controller
 *
 * @property \App\Model\Table\OdontoDependentesTable $OdontoDependentes
 */
class OdontoDependentesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Odonto');
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $odonto_operadoras = $this->OdontoDependentes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);

        $estados = $this->OdontoDependentes->OdontoOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => '1 = 1 '])->toArray();
        $estados = $this->OdontoDependentes->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados'));
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Dependente id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoDependente = $this->OdontoDependentes->get($id, [
            'contain' => ['OdontoOperadoras', 'OdontoProdutos']
        ]);

        $this->set('odontoDependente', $odontoDependente);
        $this->set('_serialize', ['odontoDependente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoDependente = $this->OdontoDependentes->newEntity();
        if ($this->request->is('post')) {
            $odontoDependente = $this->OdontoDependentes->patchEntity($odontoDependente, $this->request->data);
            if ($this->OdontoDependentes->save($odontoDependente)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $estados = $this->OdontoDependentes->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->matching('OdontoOperadoras', function ($q) {
            return $q;
        });

        $odontoOperadoras = $this->OdontoDependentes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);

        $this->set(compact('odontoDependente', 'estados', 'odontoOperadoras'));
        $this->set('_serialize', ['odontoDependente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Dependente id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoDependente = $this->OdontoDependentes->get($id, [
            'contain' => ['OdontoOperadoras']
        ]);
        $odontoDependente->estado_id =  $odontoDependente->odonto_operadora->estado_id;
        $odontoDependente->tipo_pessoa = $odontoDependente->odonto_operadora->tipo_pessoa;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoDependente = $this->OdontoDependentes->patchEntity($odontoDependente, $this->request->data);
            if ($this->OdontoDependentes->save($odontoDependente)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $estados = $this->OdontoDependentes->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoOperadoras = $this->OdontoDependentes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            }, 'conditions' => '1 = 1 ',
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['tipo_pessoa' => $odontoDependente->tipo_pessoa])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $this->set(compact('odontoDependente', 'odontoOperadoras', 'estados'));
        $this->set('_serialize', ['odontoDependente']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Dependente id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoDependente = $this->OdontoDependentes->get($id);
        if ($this->OdontoDependentes->delete($odontoDependente)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente Novamente'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $odonto_dependentes = $this->OdontoDependentes->find('all', ['limit' => 200, 'valueField' => 'nome', 'conditions' => '1 = 1 '])->where(["odonto_operadora_id" => $id])->contain(["OdontoOperadoras"]);

        $this->set(compact('odonto_dependentes'));
        $this->set('_serialize', ['odonto_dependentes']);
    }

    public function filtroEstado()
    {

        $estado = $this->request->data()['estado_id'];

        $operadoras = $this->OdontoDependentes->OdontoOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])
            ->where(['estado_id' => $estado]);

        $this->set(compact('operadoras'));
    }

    public function filtroIndex($nova = null, $tipo = null, $estado = null, $operadora = null)
    {
        $data = $this->Odonto->filtroIndex($nova, $tipo, $estado, $operadora);
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }

    public function getOperadoras($nova = null, $tipo = null)
    {
        $operadoras = $this->Odonto->getOperadoras($nova, $tipo);
        $this->set(compact('operadoras'));
    }
}
