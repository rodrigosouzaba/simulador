<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoDocumentos Controller
 *
 * @property \App\Model\Table\OdontoDocumentosTable $OdontoDocumentos
 */
class OdontoDocumentosController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Odonto');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // $odonto_operadoras = $this->OdontoDocumentos->OdontoOperadoras->find('list', [
        //     'contain' => ["Estados"],
        //     'valueField' => function ($e) {
        //         return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
        //     },
        //     'order' => ['OdontoOperadoras.nome' => 'ASC']
        // ])->where(['OR' => ['nova is NULL', 'nova' => '']]);

        // $estados = $this->OdontoDocumentos->OdontoOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => '1 = 1 '])->toArray();
        // $estados = $this->OdontoDocumentos->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(['id IN' => $estados]);
        // $this->set(compact('odonto_operadoras', 'estados'));
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoDocumento = $this->OdontoDocumentos->get($id, [
            'contain' => ['OdontoOperadoras', 'OdontoProdutos']
        ]);

        $this->set('odontoDocumento', $odontoDocumento);
        $this->set('_serialize', ['odontoDocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoDocumento = $this->OdontoDocumentos->newEntity();
        if ($this->request->is('post')) {
            $odontoDocumento = $this->OdontoDocumentos->patchEntity($odontoDocumento, $this->request->data);
            if ($this->OdontoDocumentos->save($odontoDocumento)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $estados = $this->OdontoDocumentos->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->matching('OdontoOperadoras', function ($q) {
            return $q;
        });

        $odontoOperadoras = $this->OdontoDocumentos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);

        $this->set(compact('odontoDocumento', 'estados', 'odontoOperadoras'));
        $this->set('_serialize', ['odontoDocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoDocumento = $this->OdontoDocumentos->get($id, [
            'contain' => ['OdontoOperadoras']
        ]);
        $odontoDocumento->estado_id =  $odontoDocumento->odonto_operadora->estado_id;
        $odontoDocumento->tipo_pessoa = $odontoDocumento->odonto_operadora->tipo_pessoa;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoDocumento = $this->OdontoDocumentos->patchEntity($odontoDocumento, $this->request->data);
            if ($this->OdontoDocumentos->save($odontoDocumento)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $estados = $this->OdontoDocumentos->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);

        $odontoOperadoras = $this->OdontoDocumentos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['tipo_pessoa' => $odontoDocumento->tipo_pessoa])->where(['OR' => ['nova is NULL', 'nova' => '']]);

        $this->set(compact('odontoDocumento', 'odontoOperadoras', 'estados'));
        $this->set('_serialize', ['odontoDocumento']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoDocumento = $this->OdontoDocumentos->get($id);
        if ($this->OdontoDocumentos->delete($odontoDocumento)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $odonto_documentos = $this->OdontoDocumentos->find('all', ['limit' => 200, 'valueField' => 'nome', 'conditions' => '1 = 1 '])->where(["odonto_operadora_id" => $id])->contain(["OdontoOperadoras"]);

        $this->set(compact('odonto_documentos'));
        $this->set('_serialize', ['odonto_documentos']);
    }

    public function filtroEstado()
    {

        $estado = $this->request->data()['estado_id'];

        $operadoras = $this->OdontoDocumentos->OdontoOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            }, 'conditions' => '1 = 1 ',
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])
            ->where(['estado_id' => $estado]);

        $this->set(compact('operadoras'));
    }

    public function filtroIndex($nova = null, $tipo = null, $estado = null, $operadora = null)
    {
        $data = $this->Odonto->filtroIndex($nova, $tipo, $estado, $operadora);
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }

    public function getOperadoras($nova = null, $tipo = null)
    {
        $operadoras = $this->Odonto->getOperadoras($nova, $tipo);
        $this->set(compact('operadoras'));
    }
}
