<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoFiltros Controller
 *
 * @property \App\Model\Table\OdontoFiltrosTable $OdontoFiltros
 */
class OdontoFiltrosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OdontoCalculos', 'Users']
        ];
        $odontoFiltros = $this->paginate($this->OdontoFiltros);

        $this->set(compact('odontoFiltros'));
        $this->set('_serialize', ['odontoFiltros']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Filtro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoFiltro = $this->OdontoFiltros->get($id, [
            'contain' => ['OdontoCalculos', 'Users', 'Tabelas']
        ]);

        $this->set('odontoFiltro', $odontoFiltro);
        $this->set('_serialize', ['odontoFiltro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoFiltro = $this->OdontoFiltros->newEntity();
        if ($this->request->is('post')) {
            $odontoFiltro = $this->OdontoFiltros->patchEntity($odontoFiltro, $this->request->data);
            if ($this->OdontoFiltros->save($odontoFiltro)) {
                $this->Flash->success(__('The odonto filtro has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto filtro could not be saved. Please, try again.'));
            }
        }
        $odontoCalculos = $this->OdontoFiltros->OdontoCalculos->find('list', ['limit' => 200]);
        $users = $this->OdontoFiltros->Users->find('list', ['limit' => 200]);
        $tabelas = $this->OdontoFiltros->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('odontoFiltro', 'odontoCalculos', 'users', 'tabelas'));
        $this->set('_serialize', ['odontoFiltro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Filtro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoFiltro = $this->OdontoFiltros->get($id, [
            'contain' => ['Tabelas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoFiltro = $this->OdontoFiltros->patchEntity($odontoFiltro, $this->request->data);
            if ($this->OdontoFiltros->save($odontoFiltro)) {
                $this->Flash->success(__('The odonto filtro has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto filtro could not be saved. Please, try again.'));
            }
        }
        $odontoCalculos = $this->OdontoFiltros->OdontoCalculos->find('list', ['limit' => 200]);
        $users = $this->OdontoFiltros->Users->find('list', ['limit' => 200]);
        $tabelas = $this->OdontoFiltros->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('odontoFiltro', 'odontoCalculos', 'users', 'tabelas'));
        $this->set('_serialize', ['odontoFiltro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Filtro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoFiltro = $this->OdontoFiltros->get($id);
        if ($this->OdontoFiltros->delete($odontoFiltro)) {
            $this->Flash->success(__('The odonto filtro has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto filtro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
