<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoFiltrosTabelas Controller
 *
 * @property \App\Model\Table\OdontoFiltrosTabelasTable $OdontoFiltrosTabelas
 */
class OdontoFiltrosTabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OdontoFiltros', 'OdontoTabelas', 'Tabelas']
        ];
        $odontoFiltrosTabelas = $this->paginate($this->OdontoFiltrosTabelas);

        $this->set(compact('odontoFiltrosTabelas'));
        $this->set('_serialize', ['odontoFiltrosTabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Filtros Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoFiltrosTabela = $this->OdontoFiltrosTabelas->get($id, [
            'contain' => ['OdontoFiltros', 'OdontoTabelas', 'Tabelas']
        ]);

        $this->set('odontoFiltrosTabela', $odontoFiltrosTabela);
        $this->set('_serialize', ['odontoFiltrosTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoFiltrosTabela = $this->OdontoFiltrosTabelas->newEntity();
        if ($this->request->is('post')) {
            $odontoFiltrosTabela = $this->OdontoFiltrosTabelas->patchEntity($odontoFiltrosTabela, $this->request->data);
            if ($this->OdontoFiltrosTabelas->save($odontoFiltrosTabela)) {
                $this->Flash->success(__('The odonto filtros tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto filtros tabela could not be saved. Please, try again.'));
            }
        }
        $odontoFiltros = $this->OdontoFiltrosTabelas->OdontoFiltros->find('list', ['limit' => 200]);
        $odontoTabelas = $this->OdontoFiltrosTabelas->OdontoTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->OdontoFiltrosTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('odontoFiltrosTabela', 'odontoFiltros', 'odontoTabelas', 'tabelas'));
        $this->set('_serialize', ['odontoFiltrosTabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Filtros Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoFiltrosTabela = $this->OdontoFiltrosTabelas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoFiltrosTabela = $this->OdontoFiltrosTabelas->patchEntity($odontoFiltrosTabela, $this->request->data);
            if ($this->OdontoFiltrosTabelas->save($odontoFiltrosTabela)) {
                $this->Flash->success(__('The odonto filtros tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto filtros tabela could not be saved. Please, try again.'));
            }
        }
        $odontoFiltros = $this->OdontoFiltrosTabelas->OdontoFiltros->find('list', ['limit' => 200]);
        $odontoTabelas = $this->OdontoFiltrosTabelas->OdontoTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->OdontoFiltrosTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('odontoFiltrosTabela', 'odontoFiltros', 'odontoTabelas', 'tabelas'));
        $this->set('_serialize', ['odontoFiltrosTabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Filtros Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoFiltrosTabela = $this->OdontoFiltrosTabelas->get($id);
        if ($this->OdontoFiltrosTabelas->delete($odontoFiltrosTabela)) {
            $this->Flash->success(__('The odonto filtros tabela has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto filtros tabela could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
