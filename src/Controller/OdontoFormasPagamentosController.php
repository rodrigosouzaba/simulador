<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoFormasPagamentos Controller
 *
 * @property \App\Model\Table\OdontoFormasPagamentosTable $OdontoFormasPagamentos
 */
class OdontoFormasPagamentosController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Odonto');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OdontoOperadoras']
        ];
        $odontoFormasPagamentos = $this->paginate($this->OdontoFormasPagamentos);

        $odontoOperadoras = $this->OdontoFormasPagamentos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ]);
        $estados = $this->OdontoFormasPagamentos->OdontoOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => '1 = 1 '])->toArray();
        $estados = $this->OdontoFormasPagamentos->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(['id IN' => $estados]);
        $this->set(compact('estados'));
        $this->set(compact('odontoFormasPagamentos', 'odontoOperadoras'));
        $this->set('_serialize', ['odontoFormasPagamentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Formas Pagamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoFormasPagamento = $this->OdontoFormasPagamentos->get($id, [
            'contain' => ['OdontoOperadoras']
        ]);

        $this->set('odontoFormasPagamento', $odontoFormasPagamento);
        $this->set('_serialize', ['odontoFormasPagamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoFormasPagamento = $this->OdontoFormasPagamentos->newEntity();
        if ($this->request->is('post')) {
            $odontoFormasPagamento = $this->OdontoFormasPagamentos->patchEntity($odontoFormasPagamento, $this->request->data);
            if ($this->OdontoFormasPagamentos->save($odontoFormasPagamento)) {
                $this->Flash->success(__('The odonto formas pagamento has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The odonto formas pagamento could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->matching('OdontoOperadoras', function ($q) {
            return $q;
        });

        $odontoOperadoras = $this->OdontoFormasPagamentos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                if (!is_null($e->estado))
                    return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            }, 'conditions' => '1 = 1 ',
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ]);

        $this->set(compact('odontoFormasPagamento', 'estados', 'odontoOperadoras'));
        $this->set('_serialize', ['odontoFormasPagamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Formas Pagamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoFormasPagamento = $this->OdontoFormasPagamentos->get($id, [
            'contain' => ['OdontoOperadoras']
        ]);
        $odontoFormasPagamento->estado_id =  $odontoFormasPagamento->odonto_operadora->estado_id;
        $odontoFormasPagamento->tipo_pessoa = $odontoFormasPagamento->odonto_operadora->tipo_pessoa;


        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoFormasPagamento = $this->OdontoFormasPagamentos->patchEntity($odontoFormasPagamento, $this->request->data);
            if ($this->OdontoFormasPagamentos->save($odontoFormasPagamento)) {
                $this->Flash->success(__('The odonto formas pagamento has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The odonto formas pagamento could not be saved. Please, try again.'));
            }
        }

        $estados = $this->OdontoFormasPagamentos->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);


        $odontoOperadoras = $this->OdontoFormasPagamentos->OdontoOperadoras->find('list', ['limit' => 200, 'conditions' => '1 = 1 ', 'valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('detalhe');
        }])->order(['nome' => 'ASC'])->where(['tipo_pessoa' => $odontoFormasPagamento->tipo_pessoa]);

        $this->set(compact('odontoFormasPagamento', 'odontoOperadoras', 'estados'));
        $this->set('_serialize', ['odontoFormasPagamento']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Formas Pagamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoFormasPagamento = $this->OdontoFormasPagamentos->get($id);
        if ($this->OdontoFormasPagamentos->delete($odontoFormasPagamento)) {
            $this->Flash->success(__('The odonto formas pagamento has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto formas pagamento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }


    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $odonto_formas_pagamentos = $this->OdontoFormasPagamentos->find('all', ['limit' => 200, 'valueField' => 'nome', 'conditions' => '1 = 1 '])->where(["odonto_operadora_id" => $id])->contain(["OdontoOperadoras"]);

        $this->set(compact('odonto_formas_pagamentos'));
        $this->set('_serialize', ['odonto_formas_pagamentos']);
    }

    public function filtroEstado()
    {
        $estado = $this->request->data['estado_id'];
        $operadoras = $this->OdontoFormasPagamentos->OdontoOperadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
        }])
            ->where(["estado_id" => $estado])
            ->order(["OdontoOperadoras.nome" => "ASC"])
            ->toArray();
        $this->set(compact("operadoras"));
    }

    public function filtroIndex($nova = null, $tipo = null, $estado = null, $operadora = null)
    {
        $data = $this->Odonto->filtroIndex($nova, $tipo, $estado, $operadora);
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }

    public function getOperadoras($nova = null, $tipo = null)
    {
        $operadoras = $this->Odonto->getOperadoras($nova, $tipo);
        $this->set(compact('operadoras'));
    }
}
