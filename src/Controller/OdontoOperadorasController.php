<?php

namespace App\Controller;

use Cake\Event\Event;
use App\Controller\AppController;

/**
 * OdontoOperadoras Controller
 *
 * @property \App\Model\Table\OdontoOperadorasTable $OdontoOperadoras
 */
class OdontoOperadorasController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow('apiGetEstados');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'conditions' => [
                'NOT' => ['nova <=> 1']
            ],
            'contain' => ['Imagens', "Estados"],
            'order' => ['FIELD(OdontoOperadoras.status, "INATIVA"), estado_id, OdontoOperadoras.nome ASC']
        ];
        $operadoras = $this->paginate($this->OdontoOperadoras);
        $estados = $this->OdontoOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => '1 = 1'])->toArray();
        $estados = $this->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados'));
        $this->set('_serialize', ['odontoOperadoras']);
    }

    /**
     * New method
     *
     * @return \Cake\Network\Response|null
     */
    public function new()
    {
        $this->paginate = [
            'conditions' => 'nova <=>1',
            'contain' => ['Imagens'],
            'order' => ['FIELD(OdontoOperadoras.status, "INATIVA"), OdontoOperadoras.nome ASC']
        ];
        $operadoras = $this->paginate($this->OdontoOperadoras);

        $this->set(compact('operadoras'));
        $this->set('_serialize', ['odontoOperadoras']);
    }

    public function filtro(int $new = null)
    {

        $dados = $this->request->data;
        $conditions = [];

        $estados = $this->OdontoOperadoras->find("list", ["valueField" => "estado_id"])->toArray();
        $estados = $this->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);

        if (!empty($dados['estado']) && !$new) {
            array_push($conditions, ['estado_id' => $dados['estado']]);
        }
        if ($new) {
            array_push($conditions, ['nova' => 1]);
        }
        if (!empty($dados['status'])) {

            $status = '';

            if ($dados['status'] == 'ATIVA') {
                $status = [
                    'OR' => [
                        'OdontoOperadoras.status' => '',
                        'OdontoOperadoras.status is NULL'
                    ]
                ];
                array_push($conditions, $status);
            } else {
                $status = $dados['status'];
                array_push($conditions, ['OdontoOperadoras.status' => $status]);
            }
        }

        if (!empty($dados['search'])) {
            array_push($conditions, ['OdontoOperadoras.nome LIKE' => "%$dados[search]%"]);
        }

        if (!$new || ($new && empty($dados['estado']))) {

            $this->paginate = [
                'contain' => ['Imagens', "Estados"],
                'order' => ['estado_id, OdontoOperadoras.nome ASC'],
                'conditions' => $conditions
            ];
            $operadoras = $this->paginate($this->OdontoOperadoras);
        } else {
            $operadoras = $this->OdontoOperadoras->find('all')
                ->matching('OdontoAreasComercializacoes.Municipios', function ($q) use ($dados) {
                    return $q->where(['Municipios.estado_id' => $dados['estado']]);
                })
                ->where($conditions)
                ->group('PfOperadoras.id');

            $operadoras = $this->paginate($operadoras);
        }
        $this->set(compact('estados', 'operadoras', 'new'));
    }


    /**
     * View method
     *
     * @param string|null $id Odonto Operadora id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoOperadora = $this->OdontoOperadoras->get($id, [
            'contain' => ['Imagens', 'OdontoProdutos']
        ]);

        $this->set('odontoOperadora', $odontoOperadora);
        $this->set('_serialize', ['odontoOperadora']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoOperadora = $this->OdontoOperadoras->newEntity();
        if ($this->request->is('post')) {
            $odontoOperadora = $this->OdontoOperadoras->patchEntity($odontoOperadora, $this->request->data);
            if ($this->OdontoOperadoras->save($odontoOperadora)) {
                $this->Flash->success(__('The odonto operadora has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The odonto operadora could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);

        $imagens = $this->OdontoOperadoras->Imagens->find('list', ['limit' => 200]);
        $this->set(compact('odontoOperadora', 'imagens', 'estados'));
        $this->set('_serialize', ['odontoOperadora']);
    }

    public function newAdd()
    {
        $odontoOperadora = $this->OdontoOperadoras->newEntity();
        if ($this->request->is('post')) {
            $odontoOperadora = $this->OdontoOperadoras->patchEntity($odontoOperadora, $this->request->data);
            if ($this->OdontoOperadoras->save($odontoOperadora)) {
                $this->Flash->success(__('The odonto operadora has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The odonto operadora could not be saved. Please, try again.'));
            }
        }

        $imagens = $this->OdontoOperadoras->Imagens->find('list', ['limit' => 200]);
        $this->set(compact('odontoOperadora', 'imagens', 'estados'));
        $this->set('_serialize', ['odontoOperadora']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Operadora id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $new = 0)
    {
        $odontoOperadora = $this->OdontoOperadoras->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoOperadora = $this->OdontoOperadoras->patchEntity($odontoOperadora, $this->request->data);
            //        debug($this->request->data);
            //        debug($odontoOperadora);
            //        debug($this->OdontoOperadoras->save($odontoOperadora));
            //        die();
            if ($this->OdontoOperadoras->save($odontoOperadora)) {
                $this->Flash->success(__('The odonto operadora has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The odonto operadora could not be saved. Please, try again.'));
            }
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);
        $imagens = $this->OdontoOperadoras->Imagens->find('list', ['limit' => 200]);
        $this->set(compact('odontoOperadora', 'imagens', 'estados'));
        $this->set('_serialize', ['odontoOperadora']);
        if ($new == 0) {
            $this->render('add');
        } else {
            $this->render('new_add');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //EXCLUI ODONTOTABELAS
        $odontoTabelas = $this->OdontoOperadoras->OdontoTabelas->find('list')->where(["odonto_operadora_id" => $id])->toArray();
        if (!empty($odontoTabelas)) {

            foreach ($odontoTabelas as $odontoTabela) {

                //EXCLUI LINKS-TABELAS
                $this->loadModel("LinksTabelas");
                $linksTabelas = $this->LinksTabelas->find('list')->where(["odonto_tabela_id" => $odontoTabela])->toArray();
                if (!empty($linksTabelas)) {
                    foreach ($linksTabelas as $linksTabela) {
                        $linksTabela = $this->LinksTabelas->get($linksTabela);
                        $this->LinksTabelas->delete($linksTabela);
                    }
                }
                // FIM - EXLUI LINKSTABELAS

                $this->loadModel("OdontoCalculosTabelas");
                $odontoCalculosTabelas = $this->OdontoCalculosTabelas->find('list')->where(["odonto_tabela_id" => $odontoTabela])->toArray();
                if (!empty($odontoCalculosTabelas)) {
                    foreach ($odontoCalculosTabelas as $calculoTabela) {
                        $calculoTabela = $this->OdontoCalculosTabelas->get($calculoTabela);
                        $this->OdontoCalculosTabelas->delete($calculoTabela);
                    }
                }

                // EXCLUI ODONTO TABELAS
                $odontoTabela = $this->OdontoOperadoras->OdontoTabelas->get($odontoTabela);
                $this->OdontoOperadoras->OdontoTabelas->delete($odontoTabela);
            }
        }
        //FIM - EXCLUI ODONTOTABELAS

        //EXLUI ODONTOREDES
        $odontoRedes = $this->OdontoOperadoras->OdontoRedes->find('list')->where(["odonto_operadora_id" => $id])->toArray();
        if (!empty($odontoRedes)) {
            foreach ($odontoRedes as $odontoRede) {
                $odontoRede = $this->OdontoOperadoras->OdontoRedes->get($odontoRede);
                $this->OdontoOperadoras->OdontoRedes->delete($odontoRede);
            }
        }
        //FIM - EXLUI ODONTOREDES

        $odontoCarencias = $this->OdontoOperadoras->OdontoCarencias->find('list')->where(["odonto_operadora_id" => $id]);
        if (!empty($odontoCarencias)) {
            foreach ($odontoCarencias as $odontoCarencia) {
                $odontoCarencia = $this->OdontoOperadoras->OdontoCarencias->get($odontoCarencia);

                $this->OdontoOperadoras->OdontoCarencias->delete($odontoCarencia);
            }
        }

        $odontoComercializacoes = $this->OdontoOperadoras->OdontoComercializacoes->find('list')->where(['odonto_operadora_id' => $id]);
        if (!empty($odontoComercializacoes)) {
            foreach ($odontoComercializacoes as $comercializacao) {
                $comercializacao = $this->OdontoOperadoras->OdontoComercializacoes->get($comercializacao);
                $this->OdontoOperadoras->OdontoComercializacoes->delete($comercializacao);
            }
        }
        $odontoOperadora = $this->OdontoOperadoras->get($id);
        if ($this->OdontoOperadoras->delete($odontoOperadora)) {
            $this->Flash->success(__('Operadora Excluída com Sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir Operadora. Tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }

    /**
     * Método para alterar Prioridade de Operadoras
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function prioridade($id = null)
    {

        $operadora_id = $id;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $operadora = $this->OdontoOperadoras->get($this->request->data['operadora_id']);
            $operadora->prioridade = $this->request->data['prioridade'];
            if ($this->OdontoOperadoras->save($operadora)) {
                $this->redirect(['controller' => 'users', 'action' => 'configurarOdonto']);
            };
        }
        $this->set(compact('operadora_id'));
    }

    /**
     * Adicionar Imagem Operadora method
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function addImagem($idOperadora = null)
    {
        $this->set('title', 'Adicionar Logo a Operadora');

        $uploadData = '';
        if ($this->request->is(['patch', 'post', 'put'])) {
            $idOperadora = $this->request->data['idOperadora'];
            if (!empty($this->request->data['file'])) {
                $fileName = $this->request->data['file']['name'];
                $uploadPath = 'uploads/imagens/';
                $uploadFile = $uploadPath . $fileName;

                if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadFile)) {
                    $this->loadModel('Imagens');
                    $uploadData = $this->Imagens->newEntity();
                    $uploadData->nome = $fileName;
                    $uploadData->caminho = $uploadPath;
                    $uploadData->created = date("Y-m-d H:i:s");
                    $uploadData->modified = date("Y-m-d H:i:s");
                    if ($this->Imagens->save($uploadData)) {
                        $idImagem = $uploadData->id;


                        $operadora = $this->OdontoOperadoras->get($idOperadora);

                        $operadora->imagem_id = $idImagem;
                        $this->OdontoOperadoras->save($operadora);




                        $this->Flash->success(__('Arquivo enviado e Salvo com sucesso.'));
                    } else {
                        $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                    }
                } else {
                    $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                }
            } else {
                $this->Flash->error(__('Nenhum arquivo selecionado.'));
            }
        }

        $this->set('uploadData', $uploadData);

        //        $files = $this->Imagens->find('all', ['order' => ['Imagens.created' => 'DESC']]);
        //        $filesRowNum = $files->count();
        //        $this->set('files', $files);
        //        $this->set('filesRowNum', $filesRowNum);

        $this->set(compact('idOperadora'));
        $this->set('_serialize', ['operadora']);
    }

    public function ocultar($id = null)
    {
        if (isset($id)) {
            $operadora = $this->OdontoOperadoras->get($id);
            if ($operadora->status == "OCULTA") {
                $operadora->status = "";
                if ($this->OdontoOperadoras->save($operadora)) {
                    $this->loadModel("OdontoTabelas");
                    $tabelas = $this->OdontoTabelas->find("all")->where(["odonto_operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->OdontoTabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 1;
                        $this->OdontoTabelas->save($tabelaEnt);
                    }

                    $this->Flash->success(__('Operadora Reexibida com sucesso.'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
                } else {
                    $this->Flash->error(__('Erro ao Reexibir Operadora. Tente novamente.'));
                }
            } else {

                $operadora->status = "OCULTA";
                if ($this->OdontoOperadoras->save($operadora)) {

                    $this->loadModel("OdontoTabelas");
                    $tabelas = $this->OdontoTabelas->find("all")->where(["odonto_operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->OdontoTabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 0;
                        $this->OdontoTabelas->save($tabelaEnt);
                    }
                    $this->Flash->success(__('Operadora Oculta com sucesso.'));

                    return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
                } else {
                    $this->Flash->error(__('Erro ao Ocultar Operadora. Tente novamente.'));
                }
            }
        }
    }


    public function atualizacao($id = null)
    {
        if (isset($id)) {
            $operadora = $this->OdontoOperadoras->get($id);
            if ($operadora->status == "EM ATUALIZAÇÃO") {
                $operadora->status = "";
                if ($this->OdontoOperadoras->save($operadora)) {
                    $this->loadModel("OdontoTabelas");
                    $tabelas = $this->OdontoTabelas->find("all")->where(["odonto_operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->OdontoTabelas->get($tabela["id"]);
                        $tabelaEnt->atualizacao = 0;
                        $this->OdontoTabelas->save($tabelaEnt);
                    }

                    $this->Flash->success(__('Operadora REMOVIDA DE ATUALIZAÇÃO de Preços.'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            } else {
                # code...

                $operadora->status = "EM ATUALIZAÇÃO";
                if ($this->OdontoOperadoras->save($operadora)) {
                    $this->loadModel("OdontoTabelas");
                    $tabelas = $this->OdontoTabelas->find("all")->where(["odonto_operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->OdontoTabelas->get($tabela["id"]);
                        $tabelaEnt->atualizacao = 1;
                        $this->OdontoTabelas->save($tabelaEnt);
                    }
                    $this->Flash->success(__('Operadora EM ATUALIZAÇÃO DE Preços.'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            }
        }
    }

    public function ativacao($id = null)
    {
        if (isset($id)) {
            $operadora = $this->OdontoOperadoras->get($id);

            if ($operadora->status == "INATIVA") {

                $operadora->status = "";
                if ($this->OdontoOperadoras->save($operadora)) {
                    $this->loadModel("OdontoTabelas");
                    $tabelas = $this->OdontoTabelas->find("all")->where(["odonto_operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->OdontoTabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 1;
                        $this->OdontoTabelas->save($tabelaEnt);
                    }

                    $this->Flash->success(__('Operadora Ativa.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            } else {

                $operadora->status = "INATIVA";
                $operadora->prioridade = 0;
                if ($this->OdontoOperadoras->save($operadora)) {
                    $this->loadModel("OdontoTabelas");
                    $tabelas = $this->OdontoTabelas->find("all")->where(["odonto_operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->OdontoTabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 0;
                        $this->OdontoTabelas->save($tabelaEnt);
                    }

                    //EXCLUINDO AS TABELASGERADAS SALVAS
                    $this->loadModel("TabelasGeradas");
                    $tabelas_geradas = $this->TabelasGeradas->find("all")->where(["operadora" => $id, "ramo" => $operadora['tipo_pessoa'] == "PJ" ? "OPJ" : "OPF"]);
                    foreach ($tabelas_geradas as $tabela_gerada) {
                        $tabelaEnt = $this->TabelasGeradas->get($tabela_gerada["id"]);
                        $this->TabelasGeradas->delete($tabelaEnt);
                    }
                    //FIM -- EXCLUINDO AS TABELASGERADAS SALVAS

                    $this->Flash->success(__('Operadora Inativa.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            }
        }
    }

    /**
     * Método duplicação de Operadoras com todos os dados exceto rede credenciada
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function duplicar($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $origem = $this->OdontoOperadoras->find("all")->contain([
                "OdontoProdutos",
                "OdontoCarencias",
                "OdontoFormasPagamentos",
                "OdontoComercializacoes",
                "OdontoObservacaos",
                "Imagens",
                "OdontoDependentes",
                "OdontoReembolsos",
                "OdontoDocumentos",
                "OdontoTabelas" => ["OdontoProdutos"]
            ])
                ->where(["OdontoOperadoras.id" => $id])
                ->first();

            $this->request->data["imagem_id"] = $origem["imagem_id"];
            $novaOperadora = $this->OdontoOperadoras->newEntity();
            $novaOperadora = $this->OdontoOperadoras->patchEntity($novaOperadora, $this->request->data);
            if ($this->OdontoOperadoras->save($novaOperadora)) {
                $this->loadModel("OdontoProdutos");
                foreach ($origem["odonto_produtos"] as $produto) {
                    $dadosProduto = array();
                    $dadosProduto["nome"] = $produto["nome"];
                    $dadosProduto["descricao"] = $produto["descricao"];
                    $dadosProduto["odonto_operadora_id"] = $novaOperadora->id;
                    $dadosProduto["tipo_produto_id"] = $novaOperadora["tipo_produto_id"];
                    $produtoNovo = $this->OdontoProdutos->newEntity();
                    $produtoNovo = $this->OdontoProdutos->patchEntity($produtoNovo, $dadosProduto);
                    $this->OdontoProdutos->save($produtoNovo);
                }
                $this->loadModel("OdontoDocumentos");
                foreach ($origem["odonto_documentos"] as $documento) {
                    $dadosDocumentos = array();
                    $dadosDocumentos["descricao"] = $documento["descricao"];
                    $dadosDocumentos["nome"] =  $documento["nome"];
                    $dadosDocumentos["odonto_operadora_id"] = $novaOperadora->id;
                    $docNovo = $this->OdontoDocumentos->newEntity();
                    $docNovo = $this->OdontoDocumentos->patchEntity($docNovo, $dadosDocumentos);
                    $this->OdontoDocumentos->save($docNovo);
                }
                $this->loadModel("OdontoReembolsos");
                foreach ($origem["odonto_reembolsos"] as $reembolso) {
                    $dadosReembolso = array();
                    $dadosReembolso["descricao"] = $reembolso["descricao"];
                    $dadosReembolso["nome"] =  $reembolso["nome"];
                    $dadosReembolso["odonto_operadora_id"] = $novaOperadora->id;
                    $reembolsoNovo = $this->OdontoReembolsos->newEntity();
                    $reembolsoNovo = $this->OdontoReembolsos->patchEntity($reembolsoNovo, $dadosReembolso);
                    $this->OdontoReembolsos->save($reembolsoNovo);
                }
                $this->loadModel("OdontoDependentes");
                foreach ($origem["odonto_dependentes"] as $dependente) {
                    $dadosDependente = array();
                    $dadosDependente["descricao"] = $dependente["descricao"];
                    $dadosDependente["nome"] =  $dependente["nome"];
                    $dadosDependente["odonto_operadora_id"] = $novaOperadora->id;
                    $dependenteNovo = $this->OdontoDependentes->newEntity();
                    $dependenteNovo = $this->OdontoDependentes->patchEntity($dependenteNovo, $dadosDependente);
                    $this->OdontoDependentes->save($dependenteNovo);
                }
                $this->loadModel("OdontoObservacaos");
                foreach ($origem["odonto_observacaos"] as $obs) {
                    $dadosObs = array();
                    $dadosObs["descricao"] = $obs["descricao"];
                    $dadosObs["nome"] =  $obs["nome"];
                    $dadosObs["odonto_operadora_id"] = $novaOperadora->id;
                    $obsNovo = $this->OdontoObservacaos->newEntity();
                    $obsNovo = $this->OdontoObservacaos->patchEntity($obsNovo, $dadosObs);
                    $this->OdontoObservacaos->save($obsNovo);
                }
                $this->loadModel("OdontoComercializacoes");
                foreach ($origem["odonto_comercializacoes"] as $comercializacao) {
                    $dadosComercializacao = array();
                    $dadosComercializacao["estado_id"] = $this->request->data["estado_id"];
                    $dadosComercializacao["descricao"] = $comercializacao["descricao"];
                    $dadosComercializacao["nome"] =  $comercializacao["nome"];
                    $dadosComercializacao["odonto_operadora_id"] = $novaOperadora->id;
                    $comercializacaoNovo = $this->OdontoComercializacoes->newEntity();
                    $comercializacaoNovo = $this->OdontoComercializacoes->patchEntity($comercializacaoNovo, $dadosComercializacao);
                    $this->OdontoComercializacoes->save($comercializacaoNovo);
                }
                $this->loadModel("OdontoFormasPagamentos");
                foreach ($origem["odonto_formas_pagamentos"] as $formaPgto) {
                    $dadosFormaPgto = array();
                    $dadosFormaPgto["descricao"] = $formaPgto["descricao"];
                    $dadosFormaPgto["nome"] =  $formaPgto["nome"];
                    $dadosFormaPgto["odonto_operadora_id"] = $novaOperadora->id;
                    $pgtoNovo = $this->OdontoFormasPagamentos->newEntity();
                    $pgtoNovo = $this->OdontoFormasPagamentos->patchEntity($pgtoNovo, $dadosFormaPgto);
                    $this->OdontoFormasPagamentos->save($pgtoNovo);
                }
                $this->loadModel("OdontoCarencias");
                foreach ($origem["odonto_carencias"] as $carencia) {
                    $dadosCarencia = array();
                    $dadosCarencia["descricao"] = $carencia["descricao"];
                    $dadosCarencia["nome"] =  $carencia["nome"];
                    $dadosCarencia["odonto_operadora_id"] = $novaOperadora->id;
                    $carenciaNovo = $this->OdontoCarencias->newEntity();
                    $carenciaNovo = $this->OdontoCarencias->patchEntity($carenciaNovo, $dadosCarencia);
                    $this->OdontoCarencias->save($carenciaNovo);
                }
                $this->loadModel("OdontoTabelas");
                foreach ($origem["odonto_tabelas"] as $tabela) {
                    $dadosTabela = array();
                    $dadosTabela["nome"] =  $tabela["nome"];
                    $dadosTabela["descricao"] = $tabela["descricao"];
                    $dadosTabela["vigencia"] = $tabela["vigencia"];
                    $dadosTabela["preco_vida"] = $tabela["preco_vida"];
                    $dadosTabela["validade"] = 0;
                    $dadosTabela["minimo_vidas"] = $tabela["minimo_vidas"];
                    $dadosTabela["maximo_vidas"] = $tabela["maximo_vidas"];
                    $dadosTabela["titulares"] = $tabela["titulares"];
                    $dadosTabela["prioridade"] = $tabela["prioridade"];
                    $dadosTabela["reembolso"] = $tabela["reembolso"];
                    $dadosTabela["tipo_pessoa"] = $tabela["tipo_pessoa"];
                    $dadosTabela["estado_id"] = $novaOperadora->estado_id;
                    $dadosTabela["odonto_operadora_id"] = $novaOperadora->id;

                    $produtoNovo = $this->OdontoProdutos->find("all")->where([
                        "odonto_operadora_id" => $novaOperadora->id,
                        "nome" => $tabela["odonto_produto"]["nome"]
                    ])->first()->toArray();
                    $dadosTabela["odonto_produto_id"] = $produtoNovo["id"];

                    $tabelaNova = $this->OdontoTabelas->newEntity();
                    $tabelaNova = $this->OdontoTabelas->patchEntity($tabelaNova, $dadosTabela);

                    $this->OdontoTabelas->save($tabelaNova);
                }
                $this->Flash->success(__('Operadora duplicada com sucesso'));
                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            }
        }
        $estados = $this->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1']);
        $odontoOperadora = $this->OdontoOperadoras->get($id);

        $this->set(compact('odontoOperadora', 'estados'));
        $this->render("add");
    }

    public function validacaoPrioridade()
    {
        $dados = $this->request->data();
        $operadoras = ['result' => $this->OdontoOperadoras->find('list')->where(['prioridade' => $dados['prioridade']])->count() > 0 ? false : true];
        $this->response->type('json');
        $this->response->body(json_encode($operadoras));
        return $this->response;
        $this->autoRender = false;
    }

    public function apiGetEstados()
    {
        $operadoras_id = $this->OdontoOperadoras->OdontoTabelas->find('list', ['keyField' => 'odonto_operadora_id', 'valueField' => 'odonto_operadora_id'])->where(['OR' => ['validade' => 1, 'validade IS' => NULL]]);
        $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1'])->where(['id IN' => $operadoras_id])->toArray();
        $estados_id = $this->OdontoOperadoras->find('list', ['keyField' => 'id', 'valueField' => 'estado_id'])->where(['id IN' => $operadoras_id])->toArray();
        $estados = $this->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1'])->where(['id IN' => $estados_id])->toArray();

        foreach ($estados_id as $op_id => $es_id) {
            $result[$estados[$es_id]][$op_id] = $operadoras[$op_id];
        }

        $this->response->type('json');
        $this->response->body(json_encode($result));
        return $this->response;
    }
}
