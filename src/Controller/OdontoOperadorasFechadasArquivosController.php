<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoOperadorasFechadasArquivos Controller
 *
 * @property \App\Model\Table\OdontoOperadorasFechadasArquivosTable $OdontoOperadorasFechadasArquivos
 */
class OdontoOperadorasFechadasArquivosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OdontoOperadorasFechadas', 'Arquivos']
        ];
        $odontoOperadorasFechadasArquivos = $this->paginate($this->OdontoOperadorasFechadasArquivos);

        $this->set(compact('odontoOperadorasFechadasArquivos'));
        $this->set('_serialize', ['odontoOperadorasFechadasArquivos']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Operadoras Fechadas Arquivo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoOperadorasFechadasArquivo = $this->OdontoOperadorasFechadasArquivos->get($id, [
            'contain' => ['OdontoOperadorasFechadas', 'Arquivos']
        ]);

        $this->set('odontoOperadorasFechadasArquivo', $odontoOperadorasFechadasArquivo);
        $this->set('_serialize', ['odontoOperadorasFechadasArquivo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoOperadorasFechadasArquivo = $this->OdontoOperadorasFechadasArquivos->newEntity();
        if ($this->request->is('post')) {
            $odontoOperadorasFechadasArquivo = $this->OdontoOperadorasFechadasArquivos->patchEntity($odontoOperadorasFechadasArquivo, $this->request->data);
            if ($this->OdontoOperadorasFechadasArquivos->save($odontoOperadorasFechadasArquivo)) {
                $this->Flash->success(__('The odonto operadoras fechadas arquivo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto operadoras fechadas arquivo could not be saved. Please, try again.'));
            }
        }
        $odontoOperadorasFechadas = $this->OdontoOperadorasFechadasArquivos->OdontoOperadorasFechadas->find('list', ['limit' => 200]);
        $arquivos = $this->OdontoOperadorasFechadasArquivos->Arquivos->find('list', ['limit' => 200]);
        $this->set(compact('odontoOperadorasFechadasArquivo', 'odontoOperadorasFechadas', 'arquivos'));
        $this->set('_serialize', ['odontoOperadorasFechadasArquivo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Operadoras Fechadas Arquivo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoOperadorasFechadasArquivo = $this->OdontoOperadorasFechadasArquivos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoOperadorasFechadasArquivo = $this->OdontoOperadorasFechadasArquivos->patchEntity($odontoOperadorasFechadasArquivo, $this->request->data);
            if ($this->OdontoOperadorasFechadasArquivos->save($odontoOperadorasFechadasArquivo)) {
                $this->Flash->success(__('The odonto operadoras fechadas arquivo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto operadoras fechadas arquivo could not be saved. Please, try again.'));
            }
        }
        $odontoOperadorasFechadas = $this->OdontoOperadorasFechadasArquivos->OdontoOperadorasFechadas->find('list', ['limit' => 200]);
        $arquivos = $this->OdontoOperadorasFechadasArquivos->Arquivos->find('list', ['limit' => 200]);
        $this->set(compact('odontoOperadorasFechadasArquivo', 'odontoOperadorasFechadas', 'arquivos'));
        $this->set('_serialize', ['odontoOperadorasFechadasArquivo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Operadoras Fechadas Arquivo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoOperadorasFechadasArquivo = $this->OdontoOperadorasFechadasArquivos->get($id);
        if ($this->OdontoOperadorasFechadasArquivos->delete($odontoOperadorasFechadasArquivo)) {
            $this->Flash->success(__('The odonto operadoras fechadas arquivo has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto operadoras fechadas arquivo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
