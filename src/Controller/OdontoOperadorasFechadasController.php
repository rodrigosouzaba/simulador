<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoOperadorasFechadas Controller
 *
 * @property \App\Model\Table\OdontoOperadorasFechadasTable $OdontoOperadorasFechadas
 */
class OdontoOperadorasFechadasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $odontoOperadorasFechadas = $this->paginate($this->OdontoOperadorasFechadas);

        $this->set(compact('odontoOperadorasFechadas'));
        $this->set('_serialize', ['odontoOperadorasFechadas']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Operadoras Fechada id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoOperadorasFechada = $this->OdontoOperadorasFechadas->get($id, [
            'contain' => ['Arquivos']
        ]);

        $this->set('odontoOperadorasFechada', $odontoOperadorasFechada);
        $this->set('_serialize', ['odontoOperadorasFechada']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoOperadorasFechada = $this->OdontoOperadorasFechadas->newEntity();
        if ($this->request->is('post')) {
            $odontoOperadorasFechada = $this->OdontoOperadorasFechadas->patchEntity($odontoOperadorasFechada, $this->request->data);
            if ($this->OdontoOperadorasFechadas->save($odontoOperadorasFechada)) {
                $this->Flash->success(__('The odonto operadoras fechada has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto operadoras fechada could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('odontoOperadorasFechada'));
        $this->set('_serialize', ['odontoOperadorasFechada']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Operadoras Fechada id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoOperadorasFechada = $this->OdontoOperadorasFechadas->get($id, [
            'contain' => ['Arquivos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoOperadorasFechada = $this->OdontoOperadorasFechadas->patchEntity($odontoOperadorasFechada, $this->request->data);
            if ($this->OdontoOperadorasFechadas->save($odontoOperadorasFechada)) {
                $this->Flash->success(__('The odonto operadoras fechada has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The odonto operadoras fechada could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('odontoOperadorasFechada'));
        $this->set('_serialize', ['odontoOperadorasFechada']);
        $this->render("add");
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Operadoras Fechada id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoOperadorasFechada = $this->OdontoOperadorasFechadas->get($id);
        if ($this->OdontoOperadorasFechadas->delete($odontoOperadorasFechada)) {
            $this->Flash->success(__('The odonto operadoras fechada has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto operadoras fechada could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    /**
	 * Adicionar Arquivos a Operadora que serão enviados por email aos solicitantes
	 *
	 * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
	 */
	public function arquivos($operadoraFechadaId = null)
	{
		$uploadData = '';


		$odonto_operadora_fechada = $this->OdontoOperadorasFechadas
		->find('all')
		->where(["OdontoOperadorasFechadas.id" => $operadoraFechadaId])
		->contain(["Arquivos"])
		->first();
// 		debug();

		$this->set(compact('pf_operadora_fechada'));

		if ($this->request->is(['patch', 'post', 'put'])) {
/*
			debug($this->request->data);
			die();
*/
			$this->loadModel('Arquivos');
			$odonto_operadora_fechada_id = $this->request->data['odonto_operadora_fechada_id'];
			if (!empty($this->request->data['file'])) {
				if ($this->request->data['file']['type'] === 'image/png' ||
					$this->request->data['file']['type'] === 'image/jpeg' ||
					$this->request->data['file']['type'] === 'application/pdf' ||
					$this->request->data['file']['type'] === 'application/xls' ||
					$this->request->data['file']['type'] === 'application/xlsx' ||
					$this->request->data['file']['type'] === 'image/jpg' ||
					$this->request->data['file']['type'] === 'image/gif') {
					$fileName = $this->request->data['file']['name'];
					
					//SE NÃO HOUVER PASTA DA OPERADORA CRIA UMA
					if (!file_exists('uploads/odonto_operadoras_fechadas/'.$odonto_operadora_fechada['nome']."/")) {
					    mkdir('uploads/odonto_operadoras_fechadas/'.$odonto_operadora_fechada['nome']."/", 0777, true);
					}
					$uploadPath = 'uploads/odonto_operadoras_fechadas/'.$odonto_operadora_fechada['nome']."/";
					$uploadFile = $uploadPath . $fileName;

					if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadFile)) {
						$uploadData = $this->Arquivos->newEntity();

						$uploadData->nome = $fileName;
						$uploadData->caminho = $uploadPath;
						$uploadData->exibicao_nome = $this->request->data["exibicao_nome"];
						$uploadData->tipo = $this->request->data['tipo'];
						$uploadData->created = date("Y-m-d H:i:s");
						$uploadData->modified = date("Y-m-d H:i:s");
						if ($this->Arquivos->save($uploadData)) {
							$idArquivo = $uploadData->id;
							$this->loadModel('OdontoOperadorasFechadasArquivos');

							$dados = $this->OdontoOperadorasFechadasArquivos->newEntity();
							$dados->odonto_operadora_fechada_id = $odonto_operadora_fechada_id;
							$dados->arquivo_id = $uploadData->id;
							$this->OdontoOperadorasFechadasArquivos->save($dados);
						} else {
							$this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
						}
					} else {
						$this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
					}
				} else {
					$this->Flash->error(__('Formato de arquivo não permitido. Formatos permitidos são: PDF, XLS, PNG, JPG, JPEG ou GIF.'));
				}
			} else {
				$this->Flash->error(__('Nenhum arquivo selecionado.'));
			}
			return $this->redirect(['action' => 'arquivos', $odonto_operadora_fechada_id]);

		}
		$tipos = array("tabela" => 'Tabela', "rede" => "Rede", "formulario"=>"Formulário");
		$this->set(compact('odonto_operadora_fechada',"uploadData", "tipos"));
		$this->set('_serialize', ['operadora']);
	}

	public function removerArquivo($arquivoId = null){
		$this->request->allowMethod(['post', 'delete']);
		$this->loadModel('Arquivos');
		$arquivo = $this->Arquivos->get($arquivoId);

		//Remoção de associação do Arquivo a Operadora
		$this->loadModel("OdontoOperadorasFechadasArquivos");
		$rel = $this->OdontoOperadorasFechadasArquivos->find('all')->where(["arquivo_id" => $arquivoId])->first();
		$op = $this->OdontoOperadorasFechadasArquivos->get($rel['id']);
		$this->OdontoOperadorasFechadasArquivos->delete($op);

// 		unlink($_SERVER['DOCUMENT_ROOT'] . $arquivo['caminho'].$arquivo['nome']);
		unlink(WWW_ROOT . $arquivo['caminho'].$arquivo['nome']);

		if ($this->Arquivos->delete($arquivo)) {
			$this->Flash->success(__('Excluído com sucesso.'));
		} else {
			$this->Flash->error(__('Erro ao excluir. Tente novamente'));
		}
// 		return $this->redirect(['action' => 'arquivos', $rel['operadora_fechada_id']]);
	}
}
