<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoProdutos Controller
 *
 * @property \App\Model\Table\OdontoProdutosTable $OdontoProdutos
 */
class OdontoProdutosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $operadoras = $this->OdontoProdutos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            }, 'conditions' => '1 = 1 ',
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ]);
        $estados = $this->OdontoProdutos->OdontoOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => '1 = 1 '])->toArray();
        $estados = $this->OdontoProdutos->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados'));
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Produto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoProduto = $this->OdontoProdutos->get($id, [
            'contain' => ['OdontoOperadoras', 'OdontoOdontoCarencias', 'OdontoOdontoRedes', 'OdontoOdontoReembolsos', 'OdontoDependentes', 'OdontoDocumentos', 'OdontoObservacaos', 'OdontoTabelas']
        ]);

        $this->set('odontoProduto', $odontoProduto);
        $this->set('_serialize', ['odontoProduto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoProduto = $this->OdontoProdutos->newEntity();
        if ($this->request->is('post')) {
            $odontoProduto = $this->OdontoProdutos->patchEntity($odontoProduto, $this->request->data);
            if ($this->OdontoProdutos->save($odontoProduto)) {
                $this->Flash->success(__('The odonto produto has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The odonto produto could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->matching('OdontoOperadoras', function ($q) {
            return $q;
        });



        $odontoCarencias = $this->OdontoProdutos->OdontoCarencias->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoRedes = $this->OdontoProdutos->OdontoRedes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoReembolsos = $this->OdontoProdutos->OdontoReembolsos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoDependentes = $this->OdontoProdutos->OdontoDependentes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoDocumentos = $this->OdontoProdutos->OdontoDocumentos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoObservacoes = $this->OdontoProdutos->OdontoObservacaos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $this->set(compact('odontoProduto', 'odontoCarencias', 'odontoRedes', 'odontoReembolsos', 'odontoDependentes', 'odontoDocumentos', 'odontoObservacoes', 'estados'));
        $this->set('_serialize', ['odontoProduto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Produto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoProduto = $this->OdontoProdutos->get($id, [
            'contain' => ['OdontoOperadoras']
        ]);
        $odontoProduto->tipo_pessoa = $odontoProduto->odonto_operadora->tipo_pessoa;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoProduto = $this->OdontoProdutos->patchEntity($odontoProduto, $this->request->data);
            if ($this->OdontoProdutos->save($odontoProduto)) {
                $this->Flash->success(__('The odonto produto has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The odonto produto could not be saved. Please, try again.'));
            }
        }
        $odontoOperadoras = $this->OdontoProdutos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                if (isset($e->estado))
                    return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            }, 'conditions' => '1 = 1 ',
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ]);
        $odontoCarencias = $this->OdontoProdutos->OdontoCarencias->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoRedes = $this->OdontoProdutos->OdontoRedes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoReembolsos = $this->OdontoProdutos->OdontoReembolsos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoDependentes = $this->OdontoProdutos->OdontoDependentes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoDocumentos = $this->OdontoProdutos->OdontoDocumentos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoObservacoes = $this->OdontoProdutos->OdontoObservacaos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $this->set(compact('odontoProduto', 'odontoOperadoras', 'odontoCarencias', 'odontoRedes', 'odontoReembolsos', 'odontoDependentes', 'odontoDocumentos', 'odontoObservacoes'));
        $this->set('_serialize', ['odontoProduto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoProduto = $this->OdontoProdutos->get($id);
        if ($this->OdontoProdutos->delete($odontoProduto)) {
            $this->Flash->success(__('The odonto produto has been deleted.'));
        } else {
            $this->Flash->error(__('The odonto produto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $produtos = $this->OdontoProdutos->find('all', ['contain' => ['OdontoOperadoras' => ['Imagens']]])
            ->where(['OdontoProdutos.odonto_operadora_id' => $id])
            ->order('OdontoProdutos.nome')->toArray();

        $operadoras = $this->OdontoProdutos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                if (isset($e->estado))
                    return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            }, 'conditions' => '1 = 1 ',
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ]);

        $this->set(compact('produtos', 'operadoras'));
        $this->set('_serialize', ['produtos']);
    }

    /**
     * Método que retorna informações de Carências, Info, OdontoDependentes .... referente a operadora selecionada.
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function findInfo($id = null)
    {
        $this->loadModel('OdontoCarencias');
        $this->loadModel('OdontoReembolsos');
        $this->loadModel('OdontoRedes');
        $this->loadModel('OdontoDocumentos');
        $this->loadModel('OdontoDependentes');
        $this->loadModel('OdontoObservacaos');
        if ($id) {
            $odontoCarencias = $this->OdontoCarencias->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoCarencias.nome' => 'ASC']])
                ->where(['OdontoCarencias.odonto_operadora_id' => $id])->toArray();
            $odontoReembolsos = $this->OdontoReembolsos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoReembolsos.nome' => 'ASC']])
                ->where(['OdontoReembolsos.odonto_operadora_id' => $id])->toArray();
            $odontoRedes = $this->OdontoRedes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoRedes.nome' => 'ASC']])
                ->where(['OdontoRedes.odonto_operadora_id' => $id])->toArray();
            $odontoDocumentos = $this->OdontoDocumentos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoDocumentos.nome' => 'ASC']])
                ->where(['OdontoDocumentos.odonto_operadora_id' => $id])->toArray();
            $odontoDependentes = $this->OdontoDependentes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoDependentes.nome' => 'ASC']])
                ->where(['OdontoDependentes.odonto_operadora_id' => $id])->toArray();
            $odontoObservacoes = $this->OdontoObservacaos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoObservacaos.nome' => 'ASC']])
                ->where(['OdontoObservacaos.odonto_operadora_id' => $id])->toArray();
            //            debug($produtos);
        } else {
            $odontoCarencias = $this->OdontoCarencias->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoCarencias.nome' => 'ASC']])->toArray();
            $odontoReembolsos = $this->OdontoReembolsos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoReembolsos.nome' => 'ASC']])->toArray();
            $odontoRedes = $this->OdontoRedes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoRedes.nome' => 'ASC']])->toArray();
            $odontoDocumentos = $this->OdontoDocumentos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoDocumentos.nome' => 'ASC']])->toArray();
            $odontoDependentes = $this->OdontoDependentes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoDependentes.nome' => 'ASC']])->toArray();
            $odontoObservacoes = $this->OdontoObservacaos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoObservacaos.nome' => 'ASC']])->toArray();
            //            debug($produtos);
        }
        //        die();
        $this->set(compact('odontoCarencias', 'odontoReembolsos', 'odontoRedes', 'odontoDocumentos', 'odontoDependentes', 'odontoObservacoes'));

        $this->render('infoAdicional');
    }

    function filtroIndex($estado = null, $tipo_pessoa = null)
    {
        $operadoras = $this->OdontoProdutos->OdontoOperadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'tipo_pessoa' => $tipo_pessoa, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'tipo_pessoa' => $tipo_pessoa, 'status IS' => null]
                ]
            ])
            ->orderASC('nome');
        $count = $operadoras->count();
        $operadoras = $operadoras->toArray();
        $this->set(compact("operadoras"));
    }

    public function getOperadoras($estado = null, $tipo = null)
    {
        $operadoras = $this->OdontoProdutos->OdontoOperadoras
            ->find('list', ['valueField' => 'nome'])
            ->where(['estado_id' => $estado, 'tipo_pessoa' => $tipo])
            ->orderAsc('nome');
        $this->set(compact('operadoras'));
    }
}
