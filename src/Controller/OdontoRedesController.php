<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoRedes Controller
 *
 * @property \App\Model\Table\OdontoRedesTable $OdontoRedes
 */
class OdontoRedesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Odonto');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $odonto_operadoras = $this->OdontoRedes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $estados = $this->OdontoRedes->OdontoOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => '1 = 1 '])->toArray();
        $estados = $this->OdontoRedes->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(['id IN' => $estados]);
        $this->set(compact('odonto_operadoras', 'estados'));
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Rede id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoRede = $this->OdontoRedes->get($id, [
            'contain' => ['OdontoOperadoras', 'OdontoProdutos']
        ]);

        $this->set('odontoRede', $odontoRede);
        $this->set('_serialize', ['odontoRede']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoRede = $this->OdontoRedes->newEntity();
        if ($this->request->is('post')) {
            $odontoRede = $this->OdontoRedes->patchEntity($odontoRede, $this->request->data);
            if ($this->OdontoRedes->save($odontoRede)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->matching('OdontoOperadoras', function ($q) {
            return $q;
        });

        $odontoOperadoras = $this->OdontoRedes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);

        $this->set(compact('odontoRede', 'estados', 'odontoOperadoras'));
        $this->set('_serialize', ['odontoRede']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Rede id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoRede = $this->OdontoRedes->get($id, [
            'contain' => ['OdontoOperadoras']
        ]);
        $odontoRede->estado_id =  $odontoRede->odonto_operadora->estado_id;
        $odontoRede->tipo_pessoa = $odontoRede->odonto_operadora->tipo_pessoa;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoRede = $this->OdontoRedes->patchEntity($odontoRede, $this->request->data);
            if ($this->OdontoRedes->save($odontoRede)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $estados = $this->OdontoRedes->Estados->find('list', ['valueField' => 'nome']);

        $odontoOperadoras = $this->OdontoRedes->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['tipo_pessoa' => $odontoRede->tipo_pessoa])->where(['OR' => ['nova is NULL', 'nova' => '']]);

        $this->set(compact('odontoRede', 'odontoOperadoras', 'estados'));
        $this->set('_serialize', ['odontoRede']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Rede id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoRede = $this->OdontoRedes->get($id);
        if ($this->OdontoRedes->delete($odontoRede)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }


    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $odonto_redes = $this->OdontoRedes->find('all', ['limit' => 200, 'valueField' => 'nome', 'conditions' => '1 = 1 '])->where(["odonto_operadora_id" => $id])->contain(["OdontoOperadoras"]);

        $this->set(compact('odonto_redes'));
        $this->set('_serialize', ['odonto_redes']);
    }

    public function filtroEstado()
    {

        $estado = $this->request->data()['estado_id'];

        $operadoras = $this->OdontoRedes->OdontoOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])
            ->where(['estado_id' => $estado]);

        $this->set(compact('operadoras'));
    }

    public function filtroIndex($nova = null, $tipo = null, $estado = null, $operadora = null)
    {
        $data = $this->Odonto->filtroIndex($nova, $tipo, $estado, $operadora);
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }

    public function getOperadoras($nova = null, $tipo = null)
    {
        $operadoras = $this->Odonto->getOperadoras($nova, $tipo);
        $this->set(compact('operadoras'));
    }
}
