<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * OdontoReembolsos Controller
 *
 * @property \App\Model\Table\OdontoReembolsosTable $OdontoReembolsos
 */
class OdontoReembolsosController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Odonto');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $odonto_operadoras = $this->OdontoReembolsos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ]);
        $estados = $this->OdontoReembolsos->OdontoOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => '1 = 1 '])->toArray();
        $estados = $this->OdontoReembolsos->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados'));
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Reembolso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoReembolso = $this->OdontoReembolsos->get($id, [
            'contain' => ['OdontoOperadoras', 'OdontoProdutos']
        ]);

        $this->set('odontoReembolso', $odontoReembolso);
        $this->set('_serialize', ['odontoReembolso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $odontoReembolso = $this->OdontoReembolsos->newEntity();
        if ($this->request->is('post')) {
            $odontoReembolso = $this->OdontoReembolsos->patchEntity($odontoReembolso, $this->request->data);
            if ($this->OdontoReembolsos->save($odontoReembolso)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->matching('OdontoOperadoras', function ($q) {
            return $q;
        });

        $odontoOperadoras = $this->OdontoReembolsos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);

        $this->set(compact('odontoReembolso', 'estados', 'odontoOperadoras'));
        $this->set('_serialize', ['odontoReembolso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Reembolso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $odontoReembolso = $this->OdontoReembolsos->get($id, [
            'contain' => ['OdontoOperadoras']
        ]);
        $odontoReembolso->estado_id =  $odontoReembolso->odonto_operadora->estado_id;
        $odontoReembolso->tipo_pessoa = $odontoReembolso->odonto_operadora->tipo_pessoa;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $odontoReembolso = $this->OdontoReembolsos->patchEntity($odontoReembolso, $this->request->data);
            if ($this->OdontoReembolsos->save($odontoReembolso)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $estados = $this->OdontoReembolsos->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);

        $odontoOperadoras = $this->OdontoReembolsos->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])->where(['tipo_pessoa' => $odontoReembolso->tipo_pessoa])->where(['OR' => ['nova is NULL', 'nova' => '']]);

        $this->set(compact('odontoReembolso', 'odontoOperadoras', 'estados'));
        $this->set('_serialize', ['odontoReembolso']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Reembolso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoReembolso = $this->OdontoReembolsos->get($id);
        if ($this->OdontoReembolsos->delete($odontoReembolso)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }



    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $odonto_reembolsos = $this->OdontoReembolsos->find('all', ['limit' => 200, 'valueField' => 'nome', 'conditions' => '1 = 1 '])->where(["odonto_operadora_id" => $id])->contain(["OdontoOperadoras"]);

        $this->set(compact('odonto_reembolsos'));
        $this->set('_serialize', ['odonto_reembolsos']);
    }

    public function filtroEstado()
    {

        $estado = $this->request->data()['estado_id'];

        $operadoras = $this->OdontoReembolsos->OdontoOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa');
            },
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ])
            ->where(['estado_id' => $estado]);

        $this->set(compact('operadoras'));
    }

    public function filtroIndex($nova = null, $tipo = null, $estado = null, $operadora = null)
    {
        $data = $this->Odonto->filtroIndex($nova, $tipo, $estado, $operadora);
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }


    public function getOperadoras($nova = null, $tipo = null)
    {
        $operadoras = $this->Odonto->getOperadoras($nova, $tipo);
        $this->set(compact('operadoras'));
    }
}
