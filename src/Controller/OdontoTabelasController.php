<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Database\Type;
use Cake\View\ViewBuilder;
use Knp\Snappy\Pdf;
use App\Utility\PdfWriter;
use Cake\Mailer\Email;

/**
 * OdontoTabelas Controller
 *
 * @property \App\Model\Table\OdontoTabelasTable $OdontoTabelas
 */
class OdontoTabelasController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Odonto');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['inativaPorVigencia']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'OdontoComercializacoes',
                'OdontoAtendimentos',
                'OdontoProdutos' => [
                    'OdontoOperadoras'
                ], 'Estados'

            ],
            'conditions' => [
                'NOT' => [
                    'new <=> 1'
                ]
            ]
        ];

        $odonto_tabelas = $this->paginate($this->OdontoTabelas);

        $this->loadModel('OdontoOperadoras');
        $odonto_operadoras = $this->OdontoOperadoras->find('list', [
            'contain' => ["Estados"],
            'valueField' => function ($e) {
                return $e->get('nome') . ' - ' . $e->get('tipo_pessoa') . ' ' . $e->estado->get("nome");
            },
            'conditions' => ['NOT' => ['nova <=> 1']],
            'order' => ['OdontoOperadoras.nome' => 'ASC']
        ]);

        $this->loadModel('OdontoOperadoras');
        $estados = $this->OdontoOperadoras->find('list', ['valueField' => 'estado_id'])->toArray();
        $estados = $this->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);

        $this->set(compact('odonto_tabelas', 'odonto_operadoras', 'estados'));
        $this->set('_serialize', ['odonto_tabelas']);
    }

    /**
     * New method
     *
     * @return \Cake\Network\Response|null
     */
    public function new()
    {
        $this->paginate = [
            'contain' => [
                'OdontoAreasComercializacoes' => [
                    'OdontoOperadoras' => [
                        'Imagens'
                    ]
                ]
            ],
            'conditions' => [
                'new' => 1
            ]
        ];

        $odonto_tabelas = $this->paginate($this->OdontoTabelas);

        $this->set(compact('odonto_tabelas'));
        $this->set('_serialize', ['odonto_tabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Odonto Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $odontoTabela = $this->OdontoTabelas->get($id, [
            'contain' => ['OdontoComercializacoes', 'OdontoAtendimentos', 'OdontoProdutos', 'Estados']
        ]);

        $this->set('odontoTabela', $odontoTabela);
        $this->set('_serialize', ['odontoTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($new = null)
    {
        $odontoTabela = $this->OdontoTabelas->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['preco_vida'] = floatval($data['preco_vida']);
            $odontoTabela = $this->OdontoTabelas->patchEntity($odontoTabela, $data);
            if (!is_null($new)) {
                $odontoTabela->new = 1;
            }

            if ($this->OdontoTabelas->save($odontoTabela)) {
                $this->Flash->success(__('The odonto tabela has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The odonto tabela could not be saved. Please, try again.'));
            }
        }
        $odontoComercializacoes = $this->OdontoTabelas->OdontoComercializacoes->find('list', ['valueField' => 'nome']);
        $odontoAtendimentos = $this->OdontoTabelas->OdontoAtendimentos->find('list', ['valueField' => 'nome'])->orderAsc('prioridade');
        $odontoProdutos = $this->OdontoTabelas->OdontoProdutos->find('list', ['valueField' => 'nome']);
        $estados = $this->OdontoTabelas->Estados->find('list', ['valueField' => 'nome']);
        $areas_comercializacoes = $this->OdontoTabelas->OdontoAreasComercializacoes->find('list', [
            'contain' => ['OdontoOperadoras'],
            'valueField' => function ($q) {
                return $q->odonto_operadora->get('nome') . ' - ' . $q->odonto_operadora->get('tipo_pessoa') . ' - ' .  $q->get('nome');
            }
        ]);
        $this->loadModel('OdontoOperadoras');
        $odonto_operadoras = $this->OdontoOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('detalhe');
        }])->order(['nome' => 'ASC']);
        $this->set(compact('odontoTabela', 'odontoComercializacoes', 'odonto_operadoras', 'odontoAtendimentos', 'odontoProdutos', 'estados', 'areas_comercializacoes'));
        $this->set('_serialize', ['odontoTabela']);
        if (!is_null($new)) {
            $this->render('new_add');
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Odonto Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $new = 0)
    {
        if ($new) {
            $odontoTabela = $this->OdontoTabelas->get($id, [
                'contain' => ['OdontoAtendimentos', 'OdontoAreasComercializacoes' => ['OdontoOperadoras']]
            ]);
        } else {
            $odontoTabela = $this->OdontoTabelas->get($id, [
                'contain' => ['OdontoComercializacoes', 'OdontoAtendimentos', 'OdontoProdutos' => ['OdontoOperadoras'], 'Estados']
            ]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['preco_vida'] = floatval($this->request->data['preco_vida']);
            $odontoTabela = $this->OdontoTabelas->patchEntity($odontoTabela, $this->request->data);
            if ($this->OdontoTabelas->save($odontoTabela)) {
                $this->Flash->success(__('The odonto tabela has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The odonto tabela could not be saved. Please, try again.'));
            }
        }
        $this->loadModel('OdontoOperadoras');
        $odonto_operadoras = $this->OdontoOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('detalhe');
        }])->where(['tipo_pessoa' => $odontoTabela->tipo_pessoa, 'estado_id' => $odontoTabela->estado_id, "NOT (status <=> 'INATIVA')"])->order(['nome' => 'ASC']);
        $odontoComercializacoes = $this->OdontoTabelas->OdontoComercializacoes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1'])->where(['estado_id' => $odontoTabela->estado_id, 'odonto_operadora_id' => $odontoTabela->odonto_operadora_id]);
        $odontoAtendimentos = $this->OdontoTabelas->OdontoAtendimentos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $odontoProdutos = $this->OdontoTabelas->OdontoProdutos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(['odonto_operadora_id' => $odontoTabela->odonto_operadora_id]);
        $estados = $this->OdontoTabelas->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ']);
        $areas_comercializacoes = $this->OdontoTabelas->OdontoAreasComercializacoes->find('list', [
            'contain' => ["OdontoOperadoras"],
            'valueField' => function ($q) {
                return $q->odonto_operadora->get('nome') . ' - ' . $q->odonto_operadora->get('tipo_pessoa') . ' - ' .  $q->get('nome');
            }
        ]);
        $this->set(compact('areas_comercializacoes', 'odontoTabela', 'odonto_operadoras', 'odontoComercializacoes', 'odontoAtendimentos', 'odontoProdutos', 'estados'));
        $this->set('_serialize', ['odontoTabela']);
        if ($new == 1) {
            $operadora_id = $odontoTabela->odonto_areas_comercializaco->odonto_operadora_id;
            $odonto_redes = $this->OdontoTabelas->OdontoRedes->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $operadora_id]);
            $odonto_observacoes = $this->OdontoTabelas->OdontoObservacaos->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $operadora_id]);
            $odonto_reembolsos = $this->OdontoTabelas->OdontoReembolsos->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $operadora_id]);
            $odonto_carencias = $this->OdontoTabelas->OdontoCarencias->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $operadora_id]);
            $odonto_dependentes = $this->OdontoTabelas->OdontoDependentes->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $operadora_id]);
            $odonto_documentos = $this->OdontoTabelas->OdontoDocumentos->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $operadora_id]);
            $odonto_formas_pagamentos = $this->OdontoTabelas->OdontoFormasPagamentos->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $operadora_id]);
            $this->set(compact('odonto_redes', 'odonto_observacoes', 'odonto_reembolsos', 'odonto_carencias', 'odonto_dependentes', 'odonto_documentos', 'odonto_formas_pagamentos'));
            $this->render('new_add');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Odonto Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $odontoTabela = $this->OdontoTabelas->get($id);


        //EXCLUSAO DE TABELAS RELACIONADAS AS ODONTOTABELAS

        $this->loadModel('OdontoCalculosTabelas');
        $calculosTab = $this->OdontoCalculosTabelas->find('all')
            ->where(['OdontoCalculosTabelas.odonto_tabela_id' => $id])->toArray();
        if (!empty($calculosTab)) {
            foreach ($calculosTab as $calculosT) {
                $excluirCalculoTabela = $this->OdontoCalculosTabelas->get($calculosT['id']);
                $this->OdontoCalculosTabelas->delete($excluirCalculoTabela);
            }
        }
        $this->loadModel('OdontoFiltrosTabelas');
        $filtrosTab = $this->OdontoFiltrosTabelas->find('all')
            ->where(['OdontoFiltrosTabelas.odonto_tabela_id' => $id])->toArray();
        if (!empty($filtrosTab)) {
            foreach ($filtrosTab as $filtrosT) {
                $excluirFiltroTabela = $this->OdontoFiltrosTabelas->get($filtrosT['id']);
                $this->OdontoFiltrosTabelas->delete($excluirFiltroTabela);
            }
        }

        $this->loadModel('LinksTabelas');
        $linksTabelas = $this->LinksTabelas->find('list')->where(['odonto_tabela_id' => $id])->toArray();
        if (!empty($linksTabelas)) {
            foreach ($linksTabelas as $link) {
                $link = $this->LinksTabelas->get($link);
                $this->LinksTabelas->delete($link);
            }
        }

        // FINAL EXCLUSAO DE TABELAS RELACIONADAS

        // debug($odontoTabela);
        // die;
        if ($this->OdontoTabelas->delete($odontoTabela)) {
            $this->Flash->success(__('Excluída com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'OdontoTabelas', 'action' => 'index']);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $operadora = $this->OdontoTabelas->OdontoOperadoras->get($id);

        if ($operadora->nova == 1) {
            $odonto_tabelas = $this->OdontoTabelas
                ->find('all', ['limit' => 200, 'conditions' => '1 = 1 '])
                ->contain('OdontoAreasComercializacoes.OdontoOperadoras')
                ->matching('OdontoAreasComercializacoes.OdontoOperadoras', function ($q) use ($id) {
                    return $q->where(['OdontoOperadoras.id' => $id]);
                })
                ->order(['OdontoTabelas.validade' => 'DESC', 'OdontoTabelas.prioridade' => 'ASC']);
        } else {
            $odonto_tabelas = $this->OdontoTabelas->find('all')->contain(['OdontoProdutos'])->where(['OdontoTabelas.odonto_operadora_id' => $id]);
        }

        $this->set(compact('odonto_tabelas'));
        $this->set('_serialize', ['odonto_tabelas']);
    }

    /**
     * Busca de Produtos por Operadoras selecionadas
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function findProdutos($operadora_id = null)
    {
        $this->loadModel('OdontoProdutos');
        if ($operadora_id) {
            $odonto_produtos = $this->OdontoProdutos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])
                ->where(['OdontoProdutos.odonto_operadora_id' => $operadora_id])->toArray();
        } else {
            $odonto_produtos = $this->OdontoProdutos->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->toArray();
        }
        $this->set(compact('odonto_produtos'));

        $this->render('selectProduto');
    }

    /**
     * Busca de Areas de Comercializacao da Operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function findComercializacao($operadora_id = null)
    {
        $this->loadModel('OdontoComercializacoes');
        if ($operadora_id) {
            $odonto_comercializacoes = $this->OdontoComercializacoes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])
                ->where(['OdontoComercializacoes.odonto_operadora_id' => $operadora_id])->toArray();
        } else {
            $odonto_comercializacoes = $this->OdontoComercializacoes->find('list', ['valueField' => 'nome'])->toArray();
        }
        $this->set(compact('odonto_comercializacoes'));

        $this->render('selectComercializacao');
    }

    /**
     * Método para alterar Prioridade de Tabelas
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function prioridade($id = null)
    {
        if ($this->request->is(['get'])) {
            $odonto_tabela_id = $id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $odonto_tabela = $this->OdontoTabelas->get($this->request->data['odonto_tabela_id']);
            $odonto_tabela->prioridade = $this->request->data['prioridade'];
            $this->OdontoTabelas->save($odonto_tabela);
        }
        $this->set(compact('odonto_tabela_id'));
    }

    /**
     * Método de Validação de Tabela
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function validar($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tabela = $this->OdontoTabelas->get($id);

        $tabela->validade = 1;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->OdontoTabelas->save($tabela)) {
            $this->Flash->success(__('Tabela validada com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao validar tabela. Tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }

    /**
     * Método de Validação de Tabela
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function invalidar($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tabela = $this->OdontoTabelas->get($id);

        $tabela->validade = 0;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->OdontoTabelas->save($tabela)) {
            $this->Flash->success(__('Tabela Invalidada com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao Invalidar tabela. Tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }

    /**
     * Cadastrar valores percentuais de Tabelas:
     * Exemplo: Apartatamento de um Produto equivale a 15% mais caro que Enfermaria
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function valorPercentual()
    {
        $this->loadModel('OdontoOperadoras');
        $odonto_operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['nome' => 'ASC']]);
        $odonto_tabelas = $this->OdontoTabelas->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['nome' => 'ASC']]);

        $ids_estados = $this->OdontoOperadoras->find('list', ['keyValue' => 'estado_id', 'valueField' => 'estado_id'])->distinct(['estado_id'])->toArray();

        $estados = $this->OdontoOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 '])->where(["id IN" => $ids_estados]);
        $this->set(compact('odonto_operadoras', 'odonto_tabelas', 'estados'));
    }

    /**
     * Busca de Tabelas por Operadoras
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function buscaTabela($id = null)
    {
        $odonto_tabelas = $this->OdontoTabelas->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1 ', 'order' => ['OdontoTabelas.nome' => 'ASC'], 'contain' => [
            'OdontoProdutos' => ['OdontoOperadoras', 'OdontoCarencias', 'OdontoReembolsos', 'OdontoRedes', 'OdontoDocumentos'], 'OdontoComercializacoes', 'OdontoAtendimentos'
        ]])
            ->where(['OdontoProdutos.odonto_operadora_id' => $id])->toArray();
        //        debug($odonto_tabelas);
        //        die();
        $this->set(compact('odonto_tabelas'));
    }

    /**
     * Cadastrar todos os preços de uma Tabelas secundária com base em uma tabela matriz:
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editarPercentual()
    {
        //        debug($this->request->data);
        $dados = $this->request->data;
        //        die();
        $matriz = $this->OdontoTabelas->get($dados['tabelaMatriz_id']);
        $secundaria = $this->OdontoTabelas->get($dados['tabelasSecundaria_id']);
        //        debug($secundaria);
        //        die();
        $percentual = $dados['percentual'];
        //        debug($percentual);
        //        die();
        //
        ////        debug($matriz);die();
        //
        $secundaria->preco_vida = $matriz->preco_vida + $matriz->preco_vida * $percentual / 100;

        //        $secundaria->faixa1 = $matriz->faixa1 * $percentual/100;
        //        $i = 1;
        //        for ($i=1;$i=10;$i++){
        //            $secundaria->faixa.$i = $matriz->faixa.$i * ($percentual/100);
        //        }
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->OdontoTabelas->save($secundaria)) {
                $this->Flash->success(__('Preços ajustados com sucesso'));
                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao ajustar preço'));
            }
        }
    }

    /**
     * Delete de múltiplas tabelas de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteLote()
    {
        $tabelas = $this->request->data;
        foreach ($tabelas as $tabela) {
            if ($tabela <> 0 && $tabela <> '') {
                $id = $this->OdontoTabelas->get($tabela);
                $this->OdontoTabelas->delete($id);
            }
        }

        $this->Flash->success(__('Tabelas excluídas com sucesso'));
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
    }

    /**
     * Reajuste de múltiplas tabelas de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function reajusteLote()
    {
        $this->set('title', 'Reajustar Tabelas');
        $odonto_tabelas = $this->request->data;
        if (isset($this->request->data['percentual'])) {
            foreach ($odonto_tabelas as $chave => $tabela) {
                if ($chave <> 'percentual') {
                    if ($tabela <> 0) {
                        $id = $this->OdontoTabelas->get($tabela);
                        $id->$preco_vida = round($id->$faixa + (($odonto_tabelas['percentual'] / 100) * $id->$preco_vida), 2);
                        $id->validade = 0;
                        $this->OdontoTabelas->save($id);
                    }
                }
            }
            $this->Flash->success(__('Tabelas Reajustadas com sucesso.'));

            return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
        }
        $this->set(compact('odonto_tabelas'));
    }

    /**
     * Reajuste de Vigência tabelas de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function reajusteVigencia()
    {
        $this->set('title', 'Reajustar Tabelas');
        $tabelas = $this->request->data;

        if ($this->request->is(['post', 'put'])) {
            if (isset($this->request->data['vigencia'])) {
                $vigencia = $tabelas['vigencia'];
                unset($tabelas['vigencia']);
                foreach ($tabelas as $chave => $tabela) {
                    //                    debug($tabela);
                    //                    debug($chave);
                    //                    die();
                    if (!empty($tabela) && $tabela <> '' && $tabela <> 0) {
                        $id = $this->OdontoTabelas->get($tabela);
                        $id->vigencia = $vigencia['year'] . "-" . $vigencia['month'] . "-" . $vigencia['day'];
                        $this->OdontoTabelas->save($id);
                    }
                }
                $this->Flash->success(__('Vigência de Tabelas alteradas com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarOdonto', 'destino' =>  $this->request->controller]);
            }
        }
        $this->set(compact('tabelas'));
    }

    /*

        Filtrar por tipo de Tabelas ( PF ou PJ )

    */
    public function filtroTipoPessoa()
    {
        $this->loadModel("OdontoOperadoras");
        $ids = $this->OdontoTabelas->find("list", ["valueField" => "odonto_operadora_id", 'conditions' => '1 = 1 '])->where(["tipo_pessoa" => $this->request->data['tipo_pessoa']])->toArray();

        $odonto_operadoras = $this->OdontoOperadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }])
            ->where(["id IN" => $ids])
            ->order(["OdontoOperadoras.nome" => "ASC"])
            ->toArray();
        $this->set(compact("odonto_operadoras"));
    }

    public function filtroIndex($nova = null, $tipo = null, $estado = null, $operadora = null)
    {
        $data = $this->Odonto->filtroIndex($nova, $tipo, $estado, $operadora);
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
        $this->set('new', $nova);
    }

    public function getOperadora($estado = null, $tipo_pessoa = null)
    {
        $operadoras = $this->OdontoTabelas->OdontoOperadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'tipo_pessoa' => $tipo_pessoa, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'tipo_pessoa' => $tipo_pessoa, 'status IS' => null]
                ]
            ])
            ->orderASC('nome');
        $count = $operadoras->count();
        $operadoras = $operadoras->toArray();
        $this->set(compact("operadoras"));
    }

    public function duplicar($tabela)
    {
        if (isset($tabela)) {
            $tabela_base = $this->OdontoTabelas->get($tabela)->toArray();

            $tabela_duplicata = $this->OdontoTabelas->newEntity();
            $tabela_duplicata = $this->OdontoTabelas->patchEntity($tabela_duplicata, $tabela_base);
            $tabela_duplicata->validade = 0;
            $tabela_duplicata = $this->OdontoTabelas->save($tabela_duplicata);
            if ($tabela_duplicata) {
                $this->Flash->success('Tabela Duplicada com sucesso.');
            } else {
                $this->Flash->error('Erro ao duplicar a tabela');
            }
            $this->redirect(['action' => 'index']);
        }
    }

    public function inativaPorVigencia()
    {
        $tabelasInvalidas = $this->OdontoTabelas->find('list')->where('NOW() > vigencia AND atualizacao = 1');
        foreach ($tabelasInvalidas as $tabela) {
            $tabela = $this->OdontoTabelas->get($tabela);
            $tabela->validade = 0;
            $this->OdontoTabelas->save($tabela);
        }
        $this->autoRender = false;
    }

    /**
     * Método de colocar Tabela em Atualização
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function colocarAtlz($id = null)
    {
        $tabela = $this->OdontoTabelas->get($id);

        $tabela->atualizacao = 1;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->OdontoTabelas->save($tabela)) {
            $this->Flash->success(__('Tabela Invalidada com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao Invalidar tabela. Tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    /**
     * Método de remover Tabela em Atualização
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function removerAtlz($id = null)
    {
        $tabela = $this->OdontoTabelas->get($id);

        $tabela->atualizacao = 0;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->OdontoTabelas->save($tabela)) {
            $this->Flash->success(__('Tabela Invalidada com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao Invalidar tabela. Tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    public function filtroAssociacoes(int $area_comercializacao = null)
    {
        $odonto_operadora_id = $this->OdontoTabelas->OdontoAreasComercializacoes->get($area_comercializacao)->odonto_operadora_id;
        $odonto_formas_pagamentos = $this->OdontoTabelas->OdontoFormasPagamentos->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $odonto_operadora_id]);
        $odonto_documentos = $this->OdontoTabelas->OdontoDocumentos->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $odonto_operadora_id]);
        $odonto_dependentes = $this->OdontoTabelas->OdontoDependentes->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $odonto_operadora_id]);
        $odonto_carencias = $this->OdontoTabelas->OdontoCarencias->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $odonto_operadora_id]);
        $odonto_reembolsos = $this->OdontoTabelas->OdontoReembolsos->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $odonto_operadora_id]);
        $odonto_observacoes = $this->OdontoTabelas->OdontoObservacaos->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $odonto_operadora_id]);
        $odonto_redes = $this->OdontoTabelas->OdontoRedes->find('list', ['valueField' => 'nome'])->where(['odonto_operadora_id' => $odonto_operadora_id]);

        $this->set(compact(
            'odonto_formas_pagamentos',
            'odonto_documentos',
            'odonto_dependentes',
            'odonto_carencias',
            'odonto_reembolsos',
            'odonto_observacoes',
            'odonto_redes'
        ));
    }
}
