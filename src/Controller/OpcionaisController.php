<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Opcionais Controller
 *
 * @property \App\Model\Table\OpcionaisTable $Opcionais
 */
class OpcionaisController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            $conditions_operadoras = [
                'estado_id' => $estado,

            ];
            if ($nova) {
                $operadoras = $this->Opcionais->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            } else {
                $operadoras = $this->Opcionais->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'Operadoras.estado_id' => $estado, 'nova' => 0], 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['Operadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['Operadoras.nova'] =  1;
                $opcionais = $this->Opcionais
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->matching('Operadoras.AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('Opcionais.id')
                    ->orderAsc('Operadoras.prioridade');
            } else {
                $conditions['Operadoras.estado_id'] = $estado;
                $conditions['OR'] = ['Operadoras.nova' => 0, 'Operadoras.nova IS NULL'];
                $opcionais = $this->Opcionais
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->where($conditions)
                    ->orderAsc('Operadoras.prioridade');
            }
        } else {
            $opcionais = $this->Opcionais
                ->find('all')
                ->contain(['Operadoras'])
                ->orderAsc('Operadoras.prioridade');
        }

        $opcionais = $this->paginate($opcionais);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('AreasComercializacoes.Operadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'operadoras', 'alias' => 'Operadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = Operadoras.estado_id']]);
            }
        }
        $this->set(compact('opcionais', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }


    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        //        debug($id);
        $opcionais = $this->Opcionais->find('all', ['contain' => ['Operadoras' => ['Imagens']]])
            ->where(['Opcionais.operadora_id' => $id])
            ->order('Opcionais.nome')->toArray();

        $operadoras = $this->Opcionais->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);

        $this->set(compact('opcionais', 'operadoras'));
        $this->set('_serialize', ['opcionais']);
    }



    /**
     * View method
     *
     * @param string|null $id Opcionai id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $opcionai = $this->Opcionais->get($id, [
            'contain' => []
        ]);

        $this->set('opcionai', $opcionai);
        $this->set('_serialize', ['opcionai']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Novo Dependentes');
        $opcionai = $this->Opcionais->newEntity();
        if ($this->request->is('post')) {
            $opcionai = $this->Opcionais->patchEntity($opcionai, $this->request->data);
            if ($this->Opcionais->save($opcionai)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar Opcional. Tente Novamente.'));
            }
        }
        $operadoras = $this->Opcionais->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);

        $this->set(compact('opcionai', 'operadoras'));
        $this->set('_serialize', ['opcionai']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Opcionai id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Editar Dependentes');
        $opcionai = $this->Opcionais->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $opcionai = $this->Opcionais->patchEntity($opcionai, $this->request->data);
            if ($this->Opcionais->save($opcionai)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar Opcional. Tente Novamente.'));
            }
        }
        $operadoras = $this->Opcionais->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);

        $this->set(compact('opcionai', 'operadoras'));
        $this->set('_serialize', ['opcionai']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Opcionai id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $opcionai = $this->Opcionais->get($id);
        if ($this->Opcionais->delete($opcionai)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao Excluir. Tente Novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    public function filtroEstado()
    {
        $estado = $this->request->data()['estados'];
        $operadoras = $this->Opcionais->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact('operadoras'));
    }

    function filtroNova($nova)
    {
        $conditions = ['NOT' => ['status <=>' => 'inativa']];
        if ($nova == 1) {
            $conditions[] = ['Operadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['Operadoras.nova IS NULL', 'Operadoras.nova' => 0];
        }
        $this->loadModel('Operadoras');
        $operadoras = $this->Operadoras->find('list', [
            'valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }
}
