<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Operadoras Controller
 *
 * @property \App\Model\Table\OperadorasTable $Operadoras
 */
class OperadorasController extends AppController
{
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow('apiGetEstados', 'index', 'edit');
        parent::beforeFilter($event);

        $this->Auth->allow('apiGetEstados', 'index', 'edit');
    }


    public function initialize()
    {
        parent::initialize();

        // Load Files model
        $this->loadModel('Imagens');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'conditions' => [
                'OR' => [
                    'NOT' => ['Operadoras.nova' => 1],
                    'Operadoras.nova is NULL',
                    'Operadoras.nova' => 0
                ]
            ],
            'contain' => ['Imagens', "Estados"],
            'order' => ['FIELD(Operadoras.status, "INATIVA"), estado_id, Operadoras.nome ASC']
        ];
        $estados = $this->Operadoras->find("list", ["valueField" => "estado_id"])->toArray();
        $estados = $this->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);

        $operadoras = $this->paginate($this->Operadoras);

        $this->set(compact('operadoras', 'estados'));
        $this->set('_serialize', ['operadoras']);
    }

    /**
     * News method
     *
     * @return \Cake\Network\Response|null
     */
    public function new()
    {
        $this->paginate = [
            'conditions' => ['Operadoras.nova' => 1],
            'contain' => ['Imagens', "Estados"],
            'order' => ['FIELD(Operadoras.status, "INATIVA"), estado_id, Operadoras.nome ASC']
        ];
        $estados = $this->Operadoras->find("list", ["valueField" => "estado_id"])->toArray();
        $estados = $this->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);

        $operadoras = $this->paginate($this->Operadoras);

        $this->set(compact('operadoras', 'estados'));
        $this->set('_serialize', ['operadoras']);
        $this->render('index');
    }

    /**
     * View method
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $operadora = $this->Operadoras->get($id, [
            'contain' => ['Produtos']
        ]);

        $this->set('operadora', $operadora);
        $this->set('_serialize', ['operadora']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($new = null)
    {
        $operadora = $this->Operadoras->newEntity();
        if ($this->request->is('post')) {
            if ($new == 1) {
                $this->request->data['nova'] = 1;
            }
            $operadora = $this->Operadoras->patchEntity($operadora, $this->request->data);

            if ($this->Operadoras->save($operadora)) {
                $this->Flash->success(__('Operadora salva com Sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar Operadora. Tente Novamente'));
            }
        }
        $estados = $this->Operadoras->Estados->find('list');
        $this->set(compact('operadora', 'estados', 'new'));
        $this->set('_serialize', ['operadora']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $new = null)
    {
        $operadora = $this->Operadoras->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $operadora = $this->Operadoras->patchEntity($operadora, $this->request->data);
            if ($this->Operadoras->save($operadora)) {
                $this->Flash->success(__('Operadora Salva com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar Operadora. Tente novamente.'));
            }
        }
        $estados = $this->Operadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1']);
        $this->set(compact('operadora', 'estados', 'new'));
        $this->set('_serialize', ['operadora']);

        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $operadora = $this->Operadoras->get($id);
        if ($this->Operadoras->delete($operadora)) {
            $this->Flash->success(__('The operadora has been deleted.'));
        } else {
            $this->Flash->error(__('The operadora could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    /**
     * Adicionar Imagem Operadora method
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function addImagem($idOperadora = null)
    {
        $this->set('title', 'Adicionar Logo a Operadora');

        $uploadData = '';
        if ($this->request->is(['patch', 'post', 'put'])) {
            $idOperadora = $this->request->data['idOperadora'];
            if (!empty($this->request->data['file'])) {
                $fileName = $this->request->data['file']['name'];
                $uploadPath = 'uploads/imagens/';
                $uploadFile = $uploadPath . $fileName;

                if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadFile)) {
                    $uploadData = $this->Imagens->newEntity();
                    $uploadData->nome = $fileName;
                    $uploadData->caminho = $uploadPath;
                    $uploadData->created = date("Y-m-d H:i:s");
                    $uploadData->modified = date("Y-m-d H:i:s");
                    if ($this->Imagens->save($uploadData)) {
                        $idImagem = $uploadData->id;


                        $operadora = $this->Operadoras->get($idOperadora);

                        $operadora->imagem_id = $idImagem;
                        $this->Operadoras->save($operadora);




                        $this->Flash->success(__('Arquivo enviado e Salvo com sucesso.'));
                    } else {
                        $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                    }
                } else {
                    $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                }
            } else {
                $this->Flash->error(__('Nenhum arquivo selecionado.'));
            }
        }

        $this->set('uploadData', $uploadData);

        $files = $this->Imagens->find('all', ['order' => ['Imagens.created' => 'DESC']]);
        $filesRowNum = $files->count();
        $this->set('files', $files);
        $this->set('filesRowNum', $filesRowNum);

        $this->set(compact('files', 'filesRowNum', 'idOperadora'));
        $this->set('_serialize', ['operadora']);
    }

    /**
     * Método para alterar Prioridade de Operadoras
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function prioridade($id = null)
    {

        $operadora_id = $id;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $operadora = $this->Operadoras->get($this->request->data['operadora_id']);
            $operadora->prioridade = $this->request->data['prioridade'];
            if ($this->Operadoras->save($operadora)) {
                $this->redirect(['controller' => 'users', 'action' => 'configurarPme']);
            };
        }
        $this->set(compact('operadora_id'));
    }

    /**
     * Método duplicação de Operadoras com todos os dados exceto rede credenciada
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function duplicar($id = null)
    {
        $operadora = $this->Operadoras->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $origem = $this->Operadoras->find("all")->contain([
                "Produtos",
                "Carencias",
                "FormasPagamentos",
                "Regioes",
                "Observacoes",
                "Imagens",
                "Opcionais",
                "Reembolsos",
                "Informacoes",
                "Tabelas" => ["TabelasCnpjs", "Produtos"]
            ])
                ->where(["Operadoras.id" => $id])
                ->first();

            $operadora = $this->Operadoras->newEntity();
            $operadora = $this->Operadoras->patchEntity($operadora, $this->request->data);
            if ($this->Operadoras->save($operadora)) {
                $this->loadModel("Produtos");
                foreach ($origem["produtos"] as $produto) {
                    $dadosProduto = array();
                    $dadosProduto["nome"] = $produto["nome"];
                    $dadosProduto["descricao"] = $produto["descricao"];
                    $dadosProduto["operadora_id"] = $operadora->id;
                    $dadosProduto["tipo_produto_id"] = $produto["tipo_produto_id"];
                    $produtoNovo = $this->Produtos->newEntity();
                    $produtoNovo = $this->Produtos->patchEntity($produtoNovo, $dadosProduto);
                    $this->Produtos->save($produtoNovo);
                }
                $this->loadModel("Informacoes");
                foreach ($origem["informacoes"] as $documento) {
                    $dadosDocumentos = array();
                    $dadosDocumentos["descricao"] = $documento["descricao"];
                    $dadosDocumentos["nome"] =  $documento["nome"];
                    $dadosDocumentos["operadora_id"] = $operadora->id;
                    $docNovo = $this->Informacoes->newEntity();
                    $docNovo = $this->Informacoes->patchEntity($docNovo, $dadosDocumentos);
                    $this->Informacoes->save($docNovo);
                }
                $this->loadModel("Reembolsos");
                foreach ($origem["reembolsos"] as $reembolso) {
                    $dadosReembolso = array();
                    $dadosReembolso["descricao"] = $reembolso["descricao"];
                    $dadosReembolso["nome"] =  $reembolso["nome"];
                    $dadosReembolso["operadora_id"] = $operadora->id;
                    $reembolsoNovo = $this->Reembolsos->newEntity();
                    $reembolsoNovo = $this->Reembolsos->patchEntity($reembolsoNovo, $dadosReembolso);
                    $this->Reembolsos->save($reembolsoNovo);
                }
                $this->loadModel("Opcionais");
                foreach ($origem["opcionais"] as $dependente) {
                    $dadosDependente = array();
                    $dadosDependente["descricao"] = $dependente["descricao"];
                    $dadosDependente["nome"] =  $dependente["nome"];
                    $dadosDependente["operadora_id"] = $operadora->id;
                    $dependenteNovo = $this->Opcionais->newEntity();
                    $dependenteNovo = $this->Opcionais->patchEntity($dependenteNovo, $dadosDependente);
                    $this->Opcionais->save($dependenteNovo);
                }
                $this->loadModel("Observacoes");
                foreach ($origem["observacoes"] as $obs) {
                    $dadosObs = array();
                    $dadosObs["descricao"] = $obs["descricao"];
                    $dadosObs["nome"] =  $obs["nome"];
                    $dadosObs["operadora_id"] = $operadora->id;
                    $obsNovo = $this->Observacoes->newEntity();
                    $obsNovo = $this->Observacoes->patchEntity($obsNovo, $dadosObs);
                    $this->Observacoes->save($obsNovo);
                }
                $this->loadModel("Regioes");
                foreach ($origem["regioes"] as $comercializacao) {
                    $dadosComercializacao = array();
                    $dadosComercializacao["estado_id"] = $comercializacao["estado_id"];
                    $dadosComercializacao["descricao"] = $comercializacao["descricao"];
                    $dadosComercializacao["nome"] =  $comercializacao["nome"];
                    $dadosComercializacao["operadora_id"] = $operadora->id;
                    $comercializacaoNovo = $this->Regioes->newEntity();
                    $comercializacaoNovo = $this->Regioes->patchEntity($comercializacaoNovo, $dadosComercializacao);
                    $this->Regioes->save($comercializacaoNovo);
                }
                $this->loadModel("FormasPagamentos");
                foreach ($origem["formas_pagamentos"] as $formaPgto) {
                    $dadosFormaPgto = array();
                    $dadosFormaPgto["descricao"] = $formaPgto["descricao"];
                    $dadosFormaPgto["nome"] =  $formaPgto["nome"];
                    $dadosFormaPgto["operadora_id"] = $operadora->id;
                    $pgtoNovo = $this->FormasPagamentos->newEntity();
                    $pgtoNovo = $this->FormasPagamentos->patchEntity($pgtoNovo, $dadosFormaPgto);
                    $this->FormasPagamentos->save($pgtoNovo);
                }
                $this->loadModel("Carencias");
                foreach ($origem["carencias"] as $carencia) {
                    $dadosCarencia = array();
                    $dadosCarencia["descricao"] = $carencia["descricao"];
                    $dadosCarencia["nome"] =  $carencia["nome"];
                    $dadosCarencia["operadora_id"] = $operadora->id;
                    $carenciaNovo = $this->Carencias->newEntity();
                    $carenciaNovo = $this->Carencias->patchEntity($carenciaNovo, $dadosCarencia);
                    $this->Carencias->save($carenciaNovo);
                }
                $this->loadModel("Tabelas");
                $this->loadModel("TabelasCnpjs");
                foreach ($origem["tabelas"] as $tabela) {
                    $dadosTabela = array();
                    $dadosTabela["nome"] =  $tabela["nome"];
                    $dadosTabela["descricao"] = $tabela["descricao"];
                    $dadosTabela["vigencia"] = $tabela["vigencia"];
                    $dadosTabela["faixa1"] = $tabela["faixa1"];
                    $dadosTabela["faixa2"] = $tabela["faixa2"];
                    $dadosTabela["faixa3"] = $tabela["faixa3"];
                    $dadosTabela["faixa4"] = $tabela["faixa4"];
                    $dadosTabela["faixa5"] = $tabela["faixa5"];
                    $dadosTabela["faixa6"] = $tabela["faixa6"];
                    $dadosTabela["faixa7"] = $tabela["faixa7"];
                    $dadosTabela["faixa8"] = $tabela["faixa8"];
                    $dadosTabela["faixa9"] = $tabela["faixa9"];
                    $dadosTabela["faixa10"] = $tabela["faixa10"];
                    $dadosTabela["faixa11"] = $tabela["faixa11"];
                    $dadosTabela["faixa12"] = $tabela["faixa12"];
                    $dadosTabela["ramo_id"] = $tabela["ramo_id"];
                    $dadosTabela["abrangencia_id"] = $tabela["abrangencia_id"];
                    $dadosTabela["tipo_id"] = $tabela["tipo_id"];
                    $dadosTabela["modalidade_id"] = 3;
                    $dadosTabela["validade"] = 0;
                    $dadosTabela["estado_id"] = $operadora->estado_id;
                    $dadosTabela["operadora_id"] = $operadora->id;
                    $dadosTabela["regiao_id"] = $tabela["regiao_id"];
                    $dadosTabela["cod_ans"] = "";
                    $dadosTabela["minimo_vidas"] = $tabela["minimo_vidas"];
                    $dadosTabela["maximo_vidas"] = $tabela["maximo_vidas"];
                    $dadosTabela["titulares"] = $tabela["titulares"];
                    $dadosTabela["prioridade"] = $tabela["prioridade"];
                    $dadosTabela["coparticipacao"] = $tabela["coparticipacao"];
                    $dadosTabela["detalhe_coparticipacao"] = $tabela["detalhe_coparticipacao"];
                    $dadosTabela["tipo_contratacao"] = $tabela["tipo_contratacao"];
                    $dadosTabela["reembolso"] = $tabela["reembolso"];

                    $produtoNovo = $this->Produtos->find("all")->where([
                        "operadora_id" => $operadora->id,
                        "nome" => $tabela["produto"]["nome"]
                    ])->first()->toArray();
                    $dadosTabela["produto_id"] = $produtoNovo["id"];


                    $tabelaNova = $this->Tabelas->newEntity();
                    $tabelaNova = $this->Tabelas->patchEntity($tabelaNova, $dadosTabela);

                    if ($this->Tabelas->save($tabelaNova)) {
                        foreach ($tabela["tabelas_cnpjs"] as $cnpj) {
                            $dadosCnpj = array();
                            $dadosCnpj["tabela_id"] =  $tabelaNova->id;
                            $dadosCnpj["cnpj_id"] =  $cnpj["cnpj_id"];

                            $cnpjsTabelaNova = $this->TabelasCnpjs->newEntity();
                            $cnpjsTabelaNova = $this->TabelasCnpjs->patchEntity($cnpjsTabelaNova, $dadosCnpj);
                            $this->TabelasCnpjs->save($cnpjsTabelaNova);
                        }
                    }
                }
                $this->Flash->success(__('Operadora duplicada com sucesso'));
                return $this->redirect(['action' => 'index']);
            }
        }
        $estados = $this->Operadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1']);

        $this->set(compact('operadora', 'estados'));
    }

    public function ocultar($id = null)
    {
        if (isset($id)) {
            $operadora = $this->Operadoras->get($id);

            if ($operadora->status == "OCULTA") {

                $operadora->status = "";
                if ($this->Operadoras->save($operadora)) {
                    $this->loadModel("Tabelas");
                    $tabelas = $this->Tabelas->find("all")->where(["operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->Tabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 1;
                        $this->Tabelas->save($tabelaEnt);
                    }

                    $this->Flash->success(__('Operadora Reexibida com sucesso.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao Reexibir Operadora. Tente novamente.'));
                }
            } else {

                $operadora->status = "OCULTA";
                if ($this->Operadoras->save($operadora)) {
                    $this->loadModel("Tabelas");
                    $tabelas = $this->Tabelas->find("all")->where(["operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->Tabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 0;
                        $this->Tabelas->save($tabelaEnt);
                    }
                    $this->Flash->success(__('Operadora Oculta com sucesso.'));

                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao Ocultar Operadora. Tente novamente.'));
                }
            }
        }
    }

    public function atualizacao($id = null)
    {
        if (isset($id)) {
            $operadora = $this->Operadoras->get($id);
            if ($operadora->status == "EM ATUALIZAÇÃO") {

                $operadora->status = "";
                if ($this->Operadoras->save($operadora)) {
                    $this->loadModel("Tabelas");

                    if ($operadora->nova != 1) {
                        $condition = ["operadora_id" => $id];
                    } else {
                        $this->loadModel('AreasComercializacoes');
                        $areas = $this->AreasComercializacoes->find('list')->where(['operadora_id' => $operadora->id])->toArray();
                        $condition = ["area_comercializacao_id IN" => $areas];
                    }

                    $tabelas = $this->Tabelas->find("all")->where($condition);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->Tabelas->get($tabela["id"]);
                        $tabelaEnt->atualizacao = 0;
                        $this->Tabelas->save($tabelaEnt);
                    }

                    $this->Flash->success(__('Operadora Removida de ATUALIZAÇÃO de Preços.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            } else {

                $operadora->status = "EM ATUALIZAÇÃO";
                if ($this->Operadoras->save($operadora)) {
                    $this->loadModel("Tabelas");

                    if ($operadora->nova != 1) {
                        $condition = ["operadora_id" => $id];
                    } else {
                        $this->loadModel('AreasComercializacoes');
                        $areas = $this->AreasComercializacoes->find('list')->where(['operadora_id' => $operadora->id])->toArray();
                        $condition = ["area_comercializacao_id IN" => $areas];
                    }

                    $tabelas = $this->Tabelas->find("all")->where($condition);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->Tabelas->get($tabela["id"]);
                        $tabelaEnt->atualizacao = 1;
                        $this->Tabelas->save($tabelaEnt);
                    }
                    $this->Flash->success(__('Operadora EM ATUALIZAÇÃO DE Preços.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            }
        }
    }

    public function ativacao($id = null)
    {
        if (isset($id)) {
            $operadora = $this->Operadoras->get($id);
            if ($operadora->status == "INATIVA") {

                $operadora->status = "";
                if ($this->Operadoras->save($operadora)) {


                    //ATIVANDO AS TABELAS DESSA OPERADORA
                    $this->loadModel("Tabelas");
                    $tabelas = $this->Tabelas->find("all")->where(["operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->Tabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 1;
                        $this->Tabelas->save($tabelaEnt);
                    }
                    //FIM -- ATIVANDO AS TABELAS DESSA OPERADORA

                    $this->Flash->success(__('Operadora Ativa.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            } else {

                $operadora->status = "INATIVA";
                $operadora->prioridade = 0;
                if ($this->Operadoras->save($operadora)) {

                    //INATIVANDO AS TABELAS DESSA OPERADORA
                    $this->loadModel("Tabelas");
                    $tabelas = $this->Tabelas->find("all")->where(["operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->Tabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 0;
                        $this->Tabelas->save($tabelaEnt);
                    }
                    //FIM -- INATIVANDO AS TABELAS DESSA OPERADORA

                    //EXCLUINDO AS TABELASGERADAS SALVAS
                    $this->loadModel("TabelasGeradas");
                    $tabelas_geradas = $this->TabelasGeradas->find("all")->where(["operadora" => $id, "ramo" => "SPJ"]);
                    foreach ($tabelas_geradas as $tabela_gerada) {
                        $tabelaEnt = $this->TabelasGeradas->get($tabela_gerada["id"]);
                        $this->TabelasGeradas->delete($tabelaEnt);
                    }
                    //FIM -- EXCLUINDO AS TABELASGERADAS SALVAS

                    $this->Flash->success(__('Operadora Inativa.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            }
        }
    }

    public function filtro()
    {

        $dados = $this->request->data;
        $conditions = [];

        $estados = $this->Operadoras->find("list", ["valueField" => "estado_id"])->toArray();
        $estados = $this->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);

        if (!empty($dados['estado'])) {
            array_push($conditions, ['estado_id' => $dados['estado']]);
        }

        if (!empty($dados['status'])) {

            $status = '';

            if ($dados['status'] == 'ATIVA') {
                $status = [
                    'OR' => [
                        'Operadoras.status' => '',
                        'Operadoras.status is NULL'
                    ]
                ];
                array_push($conditions, $status);
            } else {
                $status = $dados['status'];
                array_push($conditions, ['Operadoras.status' => $status]);
            }
        }

        if (!empty($dados['search'])) {
            array_push($conditions, ['Operadoras.nome LIKE' => "%$dados[search]%"]);
        }

        array_push($conditions, ['nova' => '1']);

        $this->paginate = [
            'contain' => ['Imagens', "Estados"],
            'order' => ['estado_id, Operadoras.nome ASC'],
            'conditions' => $conditions
        ];

        $operadoras = $this->paginate($this->Operadoras);

        $this->set(compact('estados', 'operadoras'));
    }

    public function validacaoPrioridade()
    {
        $dados = $this->request->data();
        $operadoras = ['result' => $this->Operadoras->find('list')->where(['prioridade' => $dados['prioridade']])->count() > 0 ? false : true];
        $this->response->type('json');
        $this->response->body(json_encode($operadoras));
        return $this->response;
        $this->autoRender = false;
    }

    public function apiGetEstados()
    {
        $operadoras_id = $this->Operadoras->Tabelas->find('list', ['keyField' => 'operadora_id', 'valueField' => 'operadora_id'])->where(['OR' => ['validade' => 1, 'validade IS' => NULL]]);
        $operadoras = $this->Operadoras->find('list', ['valueField' => 'nome'])->where(['id IN' => $operadoras_id])->toArray();
        $estados_id = $this->Operadoras->find('list', ['keyField' => 'id', 'valueField' => 'estado_id'])->where(['id IN' => $operadoras_id])->toArray();
        $estados = $this->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados_id])->toArray();

        foreach ($estados_id as $op_id => $es_id) {
            $result[$estados[$es_id]][$op_id] = $operadoras[$op_id];
        }

        $this->response->type('json');
        $this->response->body(json_encode($result));
        return $this->response;
    }
}
