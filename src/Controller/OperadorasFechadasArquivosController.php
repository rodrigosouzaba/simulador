<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OperadorasFechadasArquivos Controller
 *
 * @property \App\Model\Table\OperadorasFechadasArquivosTable $OperadorasFechadasArquivos
 */
class OperadorasFechadasArquivosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OperadorasFechadas', 'Arquivos']
        ];
        $operadorasFechadasArquivos = $this->paginate($this->OperadorasFechadasArquivos);

        $this->set(compact('operadorasFechadasArquivos'));
        $this->set('_serialize', ['operadorasFechadasArquivos']);
    }

    /**
     * View method
     *
     * @param string|null $id Operadoras Fechadas Arquivo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $operadorasFechadasArquivo = $this->OperadorasFechadasArquivos->get($id, [
            'contain' => ['OperadorasFechadas', 'Arquivos']
        ]);

        $this->set('operadorasFechadasArquivo', $operadorasFechadasArquivo);
        $this->set('_serialize', ['operadorasFechadasArquivo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $operadorasFechadasArquivo = $this->OperadorasFechadasArquivos->newEntity();
        if ($this->request->is('post')) {
            $operadorasFechadasArquivo = $this->OperadorasFechadasArquivos->patchEntity($operadorasFechadasArquivo, $this->request->data);
            if ($this->OperadorasFechadasArquivos->save($operadorasFechadasArquivo)) {
                $this->Flash->success(__('The operadoras fechadas arquivo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The operadoras fechadas arquivo could not be saved. Please, try again.'));
            }
        }
        $operadorasFechadas = $this->OperadorasFechadasArquivos->OperadorasFechadas->find('list', ['limit' => 200]);
        $arquivos = $this->OperadorasFechadasArquivos->Arquivos->find('list', ['limit' => 200]);
        $this->set(compact('operadorasFechadasArquivo', 'operadorasFechadas', 'arquivos'));
        $this->set('_serialize', ['operadorasFechadasArquivo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Operadoras Fechadas Arquivo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $operadorasFechadasArquivo = $this->OperadorasFechadasArquivos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $operadorasFechadasArquivo = $this->OperadorasFechadasArquivos->patchEntity($operadorasFechadasArquivo, $this->request->data);
            if ($this->OperadorasFechadasArquivos->save($operadorasFechadasArquivo)) {
                $this->Flash->success(__('The operadoras fechadas arquivo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The operadoras fechadas arquivo could not be saved. Please, try again.'));
            }
        }
        $operadorasFechadas = $this->OperadorasFechadasArquivos->OperadorasFechadas->find('list', ['limit' => 200]);
        $arquivos = $this->OperadorasFechadasArquivos->Arquivos->find('list', ['limit' => 200]);
        $this->set(compact('operadorasFechadasArquivo', 'operadorasFechadas', 'arquivos'));
        $this->set('_serialize', ['operadorasFechadasArquivo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Operadoras Fechadas Arquivo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $operadorasFechadasArquivo = $this->OperadorasFechadasArquivos->get($id);
        if ($this->OperadorasFechadasArquivos->delete($operadorasFechadasArquivo)) {
            $this->Flash->success(__('The operadoras fechadas arquivo has been deleted.'));
        } else {
            $this->Flash->error(__('The operadoras fechadas arquivo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
