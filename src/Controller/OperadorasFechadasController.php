<?php
namespace App\Controller;

use Cake\Network\Http\Client;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\I18n\Time;
/**
 * OperadorasFechadas Controller
 *
 * @property \App\Model\Table\OperadorasFechadasTable $OperadorasFechadas
 */
class OperadorasFechadasController extends AppController
{

	/**
	 * Index method
	 *
	 * @return \Cake\Network\Response|null
	 */
	public function index()
	{
		$operadorasFechadas = $this->paginate($this->OperadorasFechadas);

		$this->set(compact('operadorasFechadas'));
		$this->set('_serialize', ['operadorasFechadas']);
	}

	/**
	 * View method
	 *
	 * @param string|null $id Operadoras Fechada id.
	 * @return \Cake\Network\Response|null
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function view($id = null)
	{
		$operadorasFechada = $this->OperadorasFechadas->get($id, [
			'contain' => ['Arquivos']
			]);

		$this->set('operadorasFechada', $operadorasFechada);
		$this->set('_serialize', ['operadorasFechada']);
	}

	/**
	 * Add method
	 *
	 * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
	 */
	public function add()
	{
		$operadorasFechada = $this->OperadorasFechadas->newEntity();
		if ($this->request->is('post')) {
			$operadorasFechada = $this->OperadorasFechadas->patchEntity($operadorasFechada, $this->request->data);
			debug($this->OperadorasFechadas->save($operadorasFechada));
			//            die();
			if ($this->OperadorasFechadas->save($operadorasFechada)) {
				$this->Flash->success(__('The operadoras fechada has been saved.'));

				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The operadoras fechada could not be saved. Please, try again.'));
			}
		}
		$arquivos = $this->OperadorasFechadas->Arquivos->find('list', ['limit' => 200]);
		$this->set(compact('operadorasFechada', 'arquivos'));
		$this->set('_serialize', ['operadorasFechada']);
	}

	/**
	 * Edit method
	 *
	 * @param string|null $id Operadoras Fechada id.
	 * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
	 * @throws \Cake\Network\Exception\NotFoundException When record not found.
	 */
	public function edit($id = null)
	{
		$operadorasFechada = $this->OperadorasFechadas->get($id, [
			'contain' => ['Arquivos']
			]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			$operadorasFechada = $this->OperadorasFechadas->patchEntity($operadorasFechada, $this->request->data);
			if ($this->OperadorasFechadas->save($operadorasFechada)) {
				$this->Flash->success(__('The operadoras fechada has been saved.'));

				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error(__('The operadoras fechada could not be saved. Please, try again.'));
			}
		}
		$arquivos = $this->OperadorasFechadas->Arquivos->find('list', ['valueField' => 'nome','limit' => 200])->toArray();
		$this->set(compact('operadorasFechada', 'arquivos'));
		$this->set('_serialize', ['operadorasFechada']);
	}

	/**
	 * Delete method
	 *
	 * @param string|null $id Operadoras Fechada id.
	 * @return \Cake\Network\Response|null Redirects to index.
	 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
	 */
	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$operadorasFechada = $this->OperadorasFechadas->get($id);
		if ($this->OperadorasFechadas->delete($operadorasFechada)) {
			$this->Flash->success(__('The operadoras fechada has been deleted.'));
		} else {
			$this->Flash->error(__('The operadoras fechada could not be deleted. Please, try again.'));
		}

		return $this->redirect(['action' => 'index']);
	}

	/**
	 * Adicionar Arquivos a Operadora que serão enviados por email aos solicitantes
	 *
	 * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
	 */
	public function arquivos($operadoraFechadaId = null)
	{
		$uploadData = '';


		$operadora_fechada = $this->OperadorasFechadas
		->find('all')
		->where(["OperadorasFechadas.id" => $operadoraFechadaId])
		->contain(["Arquivos"])
		->first();
// 		debug();

		$this->set(compact('operadora_fechada'));

		if ($this->request->is(['patch', 'post', 'put'])) {
/*
			debug($this->request->data);
			die();
*/
			$this->loadModel('Arquivos');
			$operadora_id = $this->request->data['operadora_fechada_id'];
			if (!empty($this->request->data['file'])) {
				if ($this->request->data['file']['type'] === 'image/png' ||
					$this->request->data['file']['type'] === 'image/jpeg' ||
					$this->request->data['file']['type'] === 'application/pdf' ||
					$this->request->data['file']['type'] === 'application/xls' ||
					$this->request->data['file']['type'] === 'application/xlsx' ||
					$this->request->data['file']['type'] === 'image/jpg' ||
					$this->request->data['file']['type'] === 'image/gif') {
					$fileName = $this->request->data['file']['name'];
					
					//SE NÃO HOUVER PASTA DA OPERADORA CRIA UMA
					if (!file_exists('uploads/operadoras_fechadas_pme/'.$operadora_fechada['nome']."/")) {
					    mkdir('uploads/operadoras_fechadas_pme/'.$operadora_fechada['nome']."/", 0777, true);
					}
					$uploadPath = 'uploads/operadoras_fechadas_pme/'.$operadora_fechada['nome']."/";
					$uploadFile = $uploadPath . $fileName;

					if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadFile)) {
						$uploadData = $this->Arquivos->newEntity();

						$uploadData->nome = $fileName;
						$uploadData->caminho = $uploadPath;
						$uploadData->exibicao_nome = $this->request->data["exibicao_nome"];
						$uploadData->tipo = $this->request->data['tipo'];
						$uploadData->created = date("Y-m-d H:i:s");
						$uploadData->modified = date("Y-m-d H:i:s");
						if ($this->Arquivos->save($uploadData)) {
							$idArquivo = $uploadData->id;
							$this->loadModel('OperadorasFechadasArquivos');

							$dados = $this->OperadorasFechadasArquivos->newEntity();
							$dados->operadora_fechada_id = $operadora_id;
							$dados->arquivo_id = $uploadData->id;
							$this->OperadorasFechadasArquivos->save($dados);
						} else {
							$this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
						}
					} else {
						$this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
					}
				} else {
					$this->Flash->error(__('Formato de arquivo não permitido. Formatos permitidos são: PDF, XLS, PNG, JPG, JPEG ou GIF.'));
				}
			} else {
				$this->Flash->error(__('Nenhum arquivo selecionado.'));
			}
			return $this->redirect(['action' => 'arquivos', $operadora_id]);

		}
		$tipos = array("tabela" => 'Tabela', "rede" => "Rede", "formulario"=>"Formulário");
		$this->set(compact('operadora_fechada',"uploadData", "tipos"));
		$this->set('_serialize', ['operadora']);
	}

	public function removerArquivo($arquivoId = null){
/*
		debug($_SERVER['DOCUMENT_ROOT']);
		debug(WWW_ROOT);
		die();
*/
		$this->request->allowMethod(['post', 'delete']);
		$this->loadModel('Arquivos');
		$arquivo = $this->Arquivos->get($arquivoId);

		//Remoção de associação do Arquivo a Operadora
		$this->loadModel("OperadorasFechadasArquivos");
		$rel = $this->OperadorasFechadasArquivos->find('all')->where(["arquivo_id" => $arquivoId])->first();
		$op = $this->OperadorasFechadasArquivos->get($rel['id']);
		$this->OperadorasFechadasArquivos->delete($op);

// 		unlink($_SERVER['DOCUMENT_ROOT'] . $arquivo['caminho'].$arquivo['nome']);
		unlink(WWW_ROOT . $arquivo['caminho'].$arquivo['nome']);

		if ($this->Arquivos->delete($arquivo)) {
			$this->Flash->success(__('Excluído com sucesso.'));
		} else {
			$this->Flash->error(__('Erro ao excluir. Tente novamente'));
		}
// 		return $this->redirect(['action' => 'arquivos', $rel['operadora_fechada_id']]);
	}


}
