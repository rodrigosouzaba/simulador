<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use Illuminate\Http\Request;
use Cake\Event\Event;
use Rest\Controller\RestController;
use Cake\Http\Response;
use Cake\Http\Client;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Menu');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow(['allEstados', 'estados', 'getMenu', 'index', 'menu', 'header']);
    }

    public function index()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        if ($sessao != null) {
            $this->redirect(['controller' => 'users', 'action' => 'central']);
        } else {
            // $this->redirect('https://corretorparceiro.com.br');
        }
    }

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Carrega o menu do sistema
     *
     */
    public function menu($opcaoMenu = 1)
    {

        $this->autoRender = false;
        $dados = [];
        if ($opcaoMenu == 1 || $opcaoMenu == 2) {
            $this->loadModel("Operadoras");
            $dados = $this->Operadoras->getMenu($opcaoMenu);
        } elseif ($opcaoMenu == 3 || $opcaoMenu == 4) {
            $this->loadModel("OdontoOperadoras");
            $dados = $this->OdontoOperadoras->getMenu($opcaoMenu);
        }

        $this->response->type('json');
        $this->response->body(json_encode($dados));
    }

    /**
     * Carrega o menu do sistema
     *
     */
    public function estados($model, $tipoPessoa, $json = true)
    {

        $dados = [];
        if ($model == 'saude') {
            $this->loadModel("Operadoras");
            $dados = $this->Operadoras->getEstados($tipoPessoa);
        } elseif ($model == 'odonto') {
            $this->loadModel("OdontoOperadoras");
            $dados = $this->OdontoOperadoras->getEstados($tipoPessoa);
        }
        if ($json) {
            $this->autoRender = false;
            $this->response->type('json');
            $this->response->body(json_encode($dados));
        } else {
            return $dados;
        }
    }

    public function allEstados($menu = 0, $return = 0)
    {

        $dados = [];
        $this->loadModel("Operadoras");
        $this->loadModel("OdontoOperadoras");

        $dados[0]['submenu'] = 0;
        $dados[0]['link'] = WEB_ROOT;
        $dados[0]['image'] = WEB_ROOT . '/app/img/menu/casa.png';
        $dados[0]['label'] = 'Inicio';
        $dados[0]['logado'] = 0;

        $dados[1]['dados'] = $this->Operadoras->getEstados();
        if (!empty($dados[1]['dados'])) {
            $dados[1]['submenu'] = 1;
            $dados[1]['link'] = '#';
            $dados[1]['hrefDestino'] = WEB_ROOT . '/index.php/planos-de-saude-estados/estado/?estado-cod={{id-estado}}&estado-name={{nome-estado}}';
            $dados[1]['image'] = WEB_ROOT . '/app/img/menu/estetoscopio.png';
            $dados[1]['label'] = 'Planos de Saúde';
            $dados[1]['saude'] = 1;
        }

        $dados[2]['dados'] = $this->OdontoOperadoras->getEstados();
        if (!empty($dados[2]['dados'])) {
            $dados[2]['submenu'] = 1;
            $dados[2]['link'] = '#';
            $dados[2]['hrefDestino'] = WEB_ROOT . '/index.php/planos-odontologicos?estado-cod={{id-estado}}&estado-name={{nome-estado}}';
            $dados[2]['image'] = WEB_ROOT . '/app/img/menu/dente.png';
            $dados[2]['odonto'] = 1;
            $dados[2]['label'] = 'Planos Odontológicos';
        }

        $dados[3]['label'] = 'Seguros';
        $dados[3]['link'] = WEB_ROOT . '/index.php/pagina-em-atualizacao/';
        $dados[3]['image'] = WEB_ROOT . '/app/img/menu/guardachuva.png';
        $dados[3]['submenu'] = 0;

        $dados[4]['label'] = 'Consórcios';
        $dados[4]['link'] = WEB_ROOT . '/index.php/pagina-em-atualizacao/';
        $dados[4]['image'] = WEB_ROOT . '/app/img/menu/chave.png';
        $dados[4]['submenu'] = 0;

        $dados[5]['label'] = 'Empréstimos';
        $dados[5]['link'] = WEB_ROOT . '/index.php/pagina-em-atualizacao/';
        $dados[5]['image'] = WEB_ROOT . '/app/img/menu/cifrao.png';
        $dados[5]['submenu'] = 0;

        $dados[6]['label'] = 'Beneficios';
        $dados[6]['link'] = WEB_ROOT . '/index.php/pagina-em-atualizacao/';
        $dados[6]['image'] = WEB_ROOT . '/app/img/menu/talheres.png';
        $dados[6]['submenu'] = 0;

        if ($return == 0) {
            $this->autoRender = false;
            $this->response->type('json');
            $this->response->body(json_encode($dados));
        } else {

            return json_decode(json_encode($dados, true));
        }
    }

    public function getMenu($resJson = 0)
    {

        $session = $this->request->session();
        $dadosSessao = $session->read('Auth.User');
        $dados['dadosSessao'] = $dadosSessao;
        $this->loadModel("Operadoras");
        $this->loadModel("VendasOnlines");

        $dados['saude']['pf'] = $this->estados('saude', 'pf', false);
        $dados['saude']['pj'] = $this->estados('saude', 'pj', false);

        $dados['odonto']['pf'] = $this->estados('odonto', 'pf', false);
        $dados['odonto']['pj'] = $this->estados('odonto', 'pj', false);

        $dados['vendasOnline'][] = $this->VendasOnlines->find('all')
            ->select(['id', 'icone', 'menu_linha1', 'menu_linha2', 'exibir_vendas_online'])
            ->where(['exibir_menu' => 1])
            ->order(['ordem' => 'ASC']);

        if ($resJson == 0) {
            $this->set(compact('dadosSessao', 'dados'));
            $this->viewBuilder()->layout('menu_externo');
        } else {
            $this->autoRender = false;
            $this->response->type('json');
            $this->response->body(json_encode($dados));
        }
    }

    public function iframe($link)
    {
        switch ($link) {
            case 'auto':
                $link = "https://villa.segfy.com:443/Publico/Segurados/Orcamentos/SolicitarCotacao?e=ej7LcHNjuCs%3D";
                break;

            case 'residencial':
                $link = "https://villa.segfy.com:443/Publico/Segurados/Orcamentos/SolicitarCotacaoResidencial?e=ej7LcHNjuCs%3D";
                break;
        }
        $this->set('link', $link);
    }

    public function header($dadosUser = null)
    {

        $token = $this->request->getHeaderLine('Authorization');
        $userApiCtlr = new \App\Controller\Api\UsersController;
        $userCtlr = new \App\Controller\UsersController;

        if ($this->Auth->identify()) {
            $dado = 'Bearer ' . $this->request->getSession()->read('Auth.token');
            $menu = $this->Menu->dadosMenu($dado);
            $sessao = $userApiCtlr->resUser($dado);

            $this->set(compact('sessao', 'menu'));
        } else {
            if (!empty($token)) {
                $http = new Client([
                    'headers' =>  ['Authorization' => $token]
                ]);
                $sessao = $http->get('https://corretorparceiro.com.br/app/api/users/resUser');
                $sessao = $sessao->getJson();

                $menu = $this->Menu->dadosMenu($token);
                $menu = $userCtlr->menuAdicional($menu);
                ksort($menu['linha'][1]);
                $this->set(compact('sessao', 'menu'));
            } else {
                $menu = $this->Menu->dadosMenu();
                $menu = $userCtlr->menuAdicional($menu);
                ksort($menu['linha'][1]);
                $this->set(compact('menu'));
            }
        }
        $this->viewBuilder()->setLayout('header');
    }
}
