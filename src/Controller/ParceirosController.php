<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Parceiros Controller
 *
 * @property \App\Model\Table\ParceirosTable $Parceiros
 */
class ParceirosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $parceiros = $this->paginate($this->Parceiros);

        $this->set(compact('parceiros'));
        $this->set('_serialize', ['parceiros']);
    }

    /**
     * View method
     *
     * @param string|null $id Parceiro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $parceiro = $this->Parceiros->get($id, [
            'contain' => []
        ]);

        $this->set('parceiro', $parceiro);
        $this->set('_serialize', ['parceiro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $parceiro = $this->Parceiros->newEntity();
        if ($this->request->is('post')) {
            $parceiro = $this->Parceiros->patchEntity($parceiro, $this->request->data);
            if ($this->Parceiros->save($parceiro)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $estados = array(
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins'
        );
        $this->set(compact('parceiro', 'estados'));
        $this->set('_serialize', ['parceiro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Parceiro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $parceiro = $this->Parceiros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $parceiro = $this->Parceiros->patchEntity($parceiro, $this->request->data);
            if ($this->Parceiros->save($parceiro)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $estados = array(
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins'
        );
        $this->set(compact('parceiro', 'estados'));
        $this->set('_serialize', ['parceiro']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Parceiro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $parceiro = $this->Parceiros->get($id);
        if ($this->Parceiros->delete($parceiro)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método que exporta TXT com informações dos parceiros no formato exigido pela CEF
     *
     * @param string|null $id Parceiro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function txt($id = null) {
        $parceiros = $this->Parceiros->find('all')->toArray();

        $this->set(compact('parceiros'));
        $this->set('_serialize', ['parceiros']);
        $this->response->type('Content-Type: text/csv');
        $this->response->download('FEG_CONSORCIOS_' . date('dmY') . '.txt');
        $this->viewBuilder()->layout('txt');
//        return $this->redirect(['action' => 'index']);
    }

}
