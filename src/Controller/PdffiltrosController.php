<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pdffiltros Controller
 *
 * @property \App\Model\Table\PdffiltrosTable $Pdffiltros
 */
class PdffiltrosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Abrangencias', 'Tipos', 'TiposProdutos']
        ];
        $pdffiltros = $this->paginate($this->Pdffiltros);

        $this->set(compact('pdffiltros'));
        $this->set('_serialize', ['pdffiltros']);
    }

    /**
     * View method
     *
     * @param string|null $id Pdffiltro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pdffiltro = $this->Pdffiltros->get($id, [
            'contain' => ['Abrangencias', 'Tipos', 'TiposProdutos', 'Simulacoes']
        ]);

        $this->set('pdffiltro', $pdffiltro);
        $this->set('_serialize', ['pdffiltro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pdffiltro = $this->Pdffiltros->newEntity();
        if ($this->request->is('post')) {
            $pdffiltro = $this->Pdffiltros->patchEntity($pdffiltro, $this->request->data);
            if ($this->Pdffiltros->save($pdffiltro)) {
                $this->Flash->success(__('The pdffiltro has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pdffiltro could not be saved. Please, try again.'));
            }
        }
        $abrangencias = $this->Pdffiltros->Abrangencias->find('list', ['limit' => 200]);
        $tipos = $this->Pdffiltros->Tipos->find('list', ['limit' => 200]);
        $tiposProdutos = $this->Pdffiltros->TiposProdutos->find('list', ['limit' => 200]);
        $simulacoes = $this->Pdffiltros->Simulacoes->find('list', ['limit' => 200]);
        $this->set(compact('pdffiltro', 'abrangencias', 'tipos', 'tiposProdutos', 'simulacoes'));
        $this->set('_serialize', ['pdffiltro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pdffiltro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pdffiltro = $this->Pdffiltros->get($id, [
            'contain' => ['Simulacoes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pdffiltro = $this->Pdffiltros->patchEntity($pdffiltro, $this->request->data);
            if ($this->Pdffiltros->save($pdffiltro)) {
                $this->Flash->success(__('The pdffiltro has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pdffiltro could not be saved. Please, try again.'));
            }
        }
        $abrangencias = $this->Pdffiltros->Abrangencias->find('list', ['limit' => 200]);
        $tipos = $this->Pdffiltros->Tipos->find('list', ['limit' => 200]);
        $tiposProdutos = $this->Pdffiltros->TiposProdutos->find('list', ['limit' => 200]);
        $simulacoes = $this->Pdffiltros->Simulacoes->find('list', ['limit' => 200]);
        $this->set(compact('pdffiltro', 'abrangencias', 'tipos', 'tiposProdutos', 'simulacoes'));
        $this->set('_serialize', ['pdffiltro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pdffiltro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pdffiltro = $this->Pdffiltros->get($id);
        if ($this->Pdffiltros->delete($pdffiltro)) {
            $this->Flash->success(__('The pdffiltro has been deleted.'));
        } else {
            $this->Flash->error(__('The pdffiltro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
