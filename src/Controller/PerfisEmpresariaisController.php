<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Database\Type;

Type::build('datetime')->useLocaleParser();

/**
 * PerfisEmpresariais Controller
 *
 * @property \App\Model\Table\PerfisEmpresariaisTable $PerfisEmpresariais
 */
class PerfisEmpresariaisController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
//        
        $query = $this->PerfisEmpresariais->find()->where(['user_id' => $sessao['id']])->contain(['DetalhesPerfisEmpresariais']);
        $this->set('perfisEmpresariais', $this->paginate($query));
//        
//        $this->loadModel('DetalhesPerfisEmpresariais');
//        $perfisEmpresariais = $this->PerfisEmpresariais->find('all')
//                ->contain(['DetalhesPerfisEmpresariais'])
//                ->where(['PerfisEmpresariais.user_id' => $sessao['id']])->toArray();
//        debug($perfisEmpresariais);
//        $this->paginate = [
//            'where' => ['PerfisEmpresariais.user_id' => $sessao['id']]
//        ];
//        $perfisEmpresariais = $this->paginate($this->PerfisEmpresariais);

        $this->set(compact('perfisEmpresariais'));
        $this->set('_serialize', ['perfisEmpresariais']);
    }

    /**
     * View method
     *
     * @param string|null $id Perfis Empresariai id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $perfisEmpresariai = $this->PerfisEmpresariais->get($id, [
            'contain' => ['Users']
        ]);
        $this->loadModel('DetalhesPerfisEmpresariais');
        $perfis = $this->DetalhesPerfisEmpresariais->find('all')
                        ->where(['DetalhesPerfisEmpresariais.perfil_empresarial_id' => $id])->toArray();
// $geral
// $geralPorFaixa 
        $geralPorFaixa = array();
        $geralPorFaixa['Titulares']['m'] = null;
        $geralPorFaixa['Titulares']['f'] = null;
        $geralPorFaixa['Dependentes']['m'] = null;
        $geralPorFaixa['Dependentes']['f'] = null;
        $geralPorFaixa['Agregados']['m'] = null;
        $geralPorFaixa['Agregados']['f'] = null;
        $geralPorFaixa['Afastados']['m'] = null;
        $geralPorFaixa['Afastados']['f'] = null;
        $titularesM = 0;
        $titularesF = 0;
        $dependentesM = 0;
        $dependentesF = 0;
        $agregadosM = 0;
        $agregadosF = 0;
        $afastadosM = 0;
        $afastadosF = 0;
        foreach ($perfis as $perfil) {
            $geralPorFaixa[$perfil['tipo']][$perfil['sexo']]['faixa' . $perfil['faixa']] = $geralPorFaixa[$perfil['tipo']][$perfil['sexo']]['faixa' . $perfil['faixa']] + 1;
            switch ($perfil['tipo']) {
                case 'Titulares':
                    switch ($perfil['sexo']) {
                        case 'm':
                            $titularesM = $titularesM + 1;
                            break;
                        case 'f':
                            $titularesF = $titularesF + 1;
                            break;
                    }
                    break;
                case 'Dependentes':
                    switch ($perfil['sexo']) {
                        case 'm':
                            $dependentesM = $dependentesM + 1;
                            break;
                        case 'f':
                            $dependentesF = $dependentesF + 1;
                            break;
                    }
                    break;
                case 'Agregados':
                    switch ($perfil['sexo']) {
                        case 'm':
                            $agregadosM = $agregadosM + 1;
                            break;
                        case 'f':
                            $agregadosF = $agregadosF + 1;
                            break;
                    }
                    break;
                case 'Afastados':
                    switch ($perfil['sexo']) {
                        case 'm':
                            $afastadosM = $afastadosM + 1;
                            break;
                        case 'f':
                            $afastadosF = $afastadosF + 1;
                            break;
                    }
                    break;
            }
        }
//        $geralPorFaixa[$tipo]['f']['faixa' . $titF['faixa']] = (isset($geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']] : 0) + 1;
        $geral = $perfis;
//        debug($perfis);
//        debug($geralPorFaixa);
//        die();
        $this->set(compact('perfisEmpresariai', 'geral', 'perfis', 'geralPorFaixa', 'afastadosM', 'afastadosF', 'titularesM', 'titularesF', 'dependentesM', 'dependentesF', 'agregadosM', 'agregadosF'));
        $this->set('_serialize', ['perfisEmpresariai']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $perfisEmpresariai = $this->PerfisEmpresariais->newEntity();
        if ($this->request->is('post')) {
            $todas = $this->request->data['Vidas'];
            $geral = array();
            $geral['Titulares']['m'] = null;
            $geral['Titulares']['f'] = null;
            $geral['Dependentes']['m'] = null;
            $geral['Dependentes']['f'] = null;
            $geral['Agregados']['m'] = null;
            $geral['Agregados']['f'] = null;
            $geral['Afastados']['m'] = null;
            $geral['Afastados']['f'] = null;

            foreach ($todas as $tipo => $valor) {
                foreach ($valor as $sexo => $todas_datas) {
                    $datas = explode(PHP_EOL, $todas_datas);
                    foreach ($datas as $data) {
                        $data = str_replace(array("\r", "\n"), '', $data);
                        if (!empty($data)) {
                            // Separa em dia, mês e ano
                            list($dia, $mes, $ano) = explode('/', $data);
                            // Descobre que dia é hoje e retorna a unix timestamp
                            $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                            // Descobre a unix timestamp da data de nascimento do fulano
                            $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                            // Depois apenas fazemos o cálculo já citado :)
                            $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                            switch ($idade) {
                                case ($idade <= 18 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 1;
                                    break;
                                case ($idade >= 19 && $idade <= 23 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 2;
                                    break;
                                case ($idade >= 24 && $idade <= 28 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 3;
                                    break;
                                case ($idade >= 29 && $idade <= 33 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 4;
                                    break;
                                case ($idade >= 34 && $idade <= 38 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 5;
                                    break;
                                case ($idade >= 39 && $idade <= 43 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 6;
                                    break;
                                case ($idade >= 44 && $idade <= 48 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 7;
                                    break;
                                case ($idade >= 49 && $idade <= 53 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 8;
                                    break;
                                case ($idade >= 54 && $idade <= 58 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 9;
                                    break;
                                case ($idade >= 59 ? $idade : !$idade):
                                    $m = count($geral[$tipo][$sexo]) + 1;
                                    $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                    $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                    $geral[$tipo][$sexo][$m]['faixa'] = 10;
                                    break;
                            }
                        }
                    }
                }
            }
            $geralPorFaixa = array();
            $geralPorFaixa['Titulares']['m'] = null;
            $geralPorFaixa['Titulares']['f'] = null;
            $geralPorFaixa['Dependentes']['m'] = null;
            $geralPorFaixa['Dependentes']['f'] = null;
            $geralPorFaixa['Agregados']['m'] = null;
            $geralPorFaixa['Agregados']['f'] = null;
            $geralPorFaixa['Afastados']['m'] = null;
            $geralPorFaixa['Afastados']['f'] = null;
            foreach ($geral as $tipo => $dados) {
                switch ($tipo) {
                    case 'Titulares':
                        $titularesM = count($dados['m']);
                        $titularesF = count($dados['f']);
                        if (isset($dados['m'])) {
                            foreach ($dados['m'] as $sexo => $titM) {
                                $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] = (isset($geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] : 0) + 1;
                            }
                        }
                        if (isset($dados['f'])) {
                            foreach ($dados['f'] as $titF) {
                                $geralPorFaixa[$tipo]['f']['faixa' . $titF['faixa']] = (isset($geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']] : 0) + 1;
                            }
                        }
                        break;
                    case 'Dependentes':
                        $dependentesM = count($dados['m']);
                        $dependentesF = count($dados['f']);
                        if (isset($dados['m']))
                            foreach ($dados['m'] as $sexo => $titM) {
                                $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] = (isset($geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] : 0) + 1;
                            }
                        if (isset($dados['f']))
                            foreach ($dados['f'] as $titF) {
                                $geralPorFaixa[$tipo]['f']['faixa' . $titF['faixa']] = (isset($geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']] : 0) + 1;
                            }
                        break;
                    case 'Agregados':
                        $agregadosM = count($dados['m']);
                        $agregadosF = count($dados['f']);
                        if (isset($dados['m']))
                            foreach ($dados['m'] as $sexo => $titM) {
                                $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] = (isset($geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] : 0) + 1;
                            }
                        if (isset($dados['f']))
                            foreach ($dados['f'] as $titF) {
                                $geralPorFaixa[$tipo]['f']['faixa' . $titF['faixa']] = (isset($geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']] : 0) + 1;
                            }
                        break;
                    case 'Afastados':
                        $afastadosM = count($dados['m']);
                        $afastadosF = count($dados['f']);
                        if (isset($dados['m']))
                            foreach ($dados['m'] as $sexo => $titM) {
                                $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] = (isset($geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] : 0) + 1;
                            }
                        if (isset($dados['f']))
                            foreach ($dados['f'] as $titF) {
                                $geralPorFaixa[$tipo]['f']['faixa' . $titF['faixa']] = (isset($geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']] : 0) + 1;
                            }
                        break;
                }
            }
//            debug($geral);
//            die();

            $perfisEmpresariai = $this->PerfisEmpresariais->patchEntity($perfisEmpresariai, $this->request->data);
//            debug($perfisEmpresariai);
            if ($this->PerfisEmpresariais->save($perfisEmpresariai)) {
                foreach ($geral as $chave => $perfil) {
//                    debug($chave);
//                    debug($perfil);
                    foreach ($perfil as $sexo => $data) {
//                        debug($sexo);
//                        debug($data);
////                    die();
                        if ($data <> null || $data <> '') {
                            foreach ($data as $data) {
//                                debug($data);
//                            die();
                                $dados['tipo'] = $chave;
                                $dados['sexo'] = $sexo;
                                $dados['perfil_empresarial_id'] = $perfisEmpresariai->id;
                                $dados['data_nascimento'] = $data['data_nascimento'];
                                $dados['idade'] = $data['idade'];
                                $dados['faixa'] = $data['faixa'];
                                $this->loadModel('DetalhesPerfisEmpresariais');
                                $detalhes = $this->DetalhesPerfisEmpresariais->newEntity();
                                $detalhes = $this->DetalhesPerfisEmpresariais->patchEntity($detalhes, $dados);
                                $this->DetalhesPerfisEmpresariais->save($detalhes);
                            }
                        }
//                die();
                    }
                }
                $this->Flash->success(__('Perfil salvo com sucesso'));

                return $this->redirect(['action' => 'view', $perfisEmpresariai->id]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $this->set(compact('geral', 'geralPorFaixa', 'afastadosM', 'afastadosF', 'titularesM', 'titularesF', 'dependentesM', 'dependentesF', 'agregadosM', 'agregadosF'));
        $this->set('_serialize', ['perfisEmpresariai']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Perfis Empresariai id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $perfisEmpresariai = $this->PerfisEmpresariais->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $perfisEmpresariai = $this->PerfisEmpresariais->patchEntity($perfisEmpresariai, $this->request->data);
            if ($this->PerfisEmpresariais->save($perfisEmpresariai)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $users = $this->PerfisEmpresariais->Users->find('list', ['limit' => 200]);
        $this->set(compact('perfisEmpresariai', 'users'));
        $this->set('_serialize', ['perfisEmpresariai']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Perfis Empresariai id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $perfisEmpresariai = $this->PerfisEmpresariais->get($id);
        $this->loadModel('DetalhesPerfisEmpresariais');
        $detalhes = $this->DetalhesPerfisEmpresariais->find('all')->where(['DetalhesPerfisEmpresariais.perfil_empresarial_id' => $id]);

        foreach ($detalhes as $detalhe) {
            $entidadeDetalhes = $this->DetalhesPerfisEmpresariais->get($detalhe['id']);
            $this->DetalhesPerfisEmpresariais->delete($entidadeDetalhes);
        }
        if ($this->PerfisEmpresariais->delete($perfisEmpresariai)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
