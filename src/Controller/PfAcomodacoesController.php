<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfAcomodacoes Controller
 *
 * @property \App\Model\Table\PfAcomodacoesTable $PfAcomodacoes
 */
class PfAcomodacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pfAcomodacoes = $this->paginate($this->PfAcomodacoes);

        $this->set(compact('pfAcomodacoes'));
        $this->set('_serialize', ['pfAcomodacoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Acomodaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfAcomodaco = $this->PfAcomodacoes->get($id, [
            'contain' => []
        ]);

        $this->set('pfAcomodaco', $pfAcomodaco);
        $this->set('_serialize', ['pfAcomodaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfAcomodaco = $this->PfAcomodacoes->newEntity();
        if ($this->request->is('post')) {
            $pfAcomodaco = $this->PfAcomodacoes->patchEntity($pfAcomodaco, $this->request->data);
            if ($this->PfAcomodacoes->save($pfAcomodaco)) {
                $this->Flash->success(__('The pf acomodaco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf acomodaco could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pfAcomodaco'));
        $this->set('_serialize', ['pfAcomodaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Acomodaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfAcomodaco = $this->PfAcomodacoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfAcomodaco = $this->PfAcomodacoes->patchEntity($pfAcomodaco, $this->request->data);
            if ($this->PfAcomodacoes->save($pfAcomodaco)) {
                $this->Flash->success(__('The pf acomodaco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf acomodaco could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pfAcomodaco'));
        $this->set('_serialize', ['pfAcomodaco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Acomodaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfAcomodaco = $this->PfAcomodacoes->get($id);
        if ($this->PfAcomodacoes->delete($pfAcomodaco)) {
            $this->Flash->success(__('The pf acomodaco has been deleted.'));
        } else {
            $this->Flash->error(__('The pf acomodaco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
