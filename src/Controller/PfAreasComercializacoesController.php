<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity;

/**
 * PfAreasComercializacoes Controller
 *
 * @property \App\Model\Table\PfAreasComercializacoesTable $PfAreasComercializacoes
 *
 * @method \App\Model\Entity\PfAreasComercializaco[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PfAreasComercializacoesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfOperadoras'],
        ];
        $pfAreasComercializacoes = $this->paginate($this->PfAreasComercializacoes);

        $this->set(compact('pfAreasComercializacoes'));
    }

    /**
     * View method
     *
     * @param string|null $id Pf Areas Comercializaco id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfAreasComercializaco = $this->PfAreasComercializacoes->get($id, [
            'contain' => ['PfOperadoras', 'PfAtendimentos'],
        ]);

        $this->set('pfAreasComercializaco', $pfAreasComercializaco);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfAreasComercializaco = $this->PfAreasComercializacoes->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $metropoles = $data['metropoles'];
            unset($data['metropoles']);
            $municipios = $this->getPostMunicipios();

            // ORDER METROPOLES
            foreach ($metropoles as $estado => $metropoles) {
                foreach ($metropoles as $metropole) {
                    $data['metropoles'][] = [
                        'id' => $metropole,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }

            // ORDER MUNICIPIOS
            foreach ($municipios as $estado => $municipios) {
                foreach ($municipios as $municipio) {
                    $data['municipios'][] = [
                        'id' => $municipio,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }
            $pfAreasComercializaco = $this->PfAreasComercializacoes->patchEntity($pfAreasComercializaco, $data);
            $pfAreasComercializaco = $this->PfAreasComercializacoes->save($pfAreasComercializaco);
            if ($pfAreasComercializaco) {
                $this->Flash->success(__('Area de Comercialização salva com sucesso!'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Area de Comercialização não pode ser salva. Tente Novamente.'));
        }

        $pfOperadoras = $this->PfAreasComercializacoes->PfOperadoras
            ->find('list', ['valueField' => function ($e) {
                return $e->nome . ' - ' . $e->detalhe;
            }])
            ->orderAsc('nome')
            ->where([
                'OR' => [
                    'status IS NOT' => 'INATIVA',
                    'status IS' => null
                ],
                'nova' => 1
            ]);

        $this->loadModel('Estados');
        $estados = $this->Estados->find('all')->contain(['Municipios', 'Metropoles']);

        $this->set(compact('estados', 'pfAreasComercializaco', 'pfOperadoras', 'pfAtendimentos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Areas Comercializaco id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfAreasComercializaco = $this->PfAreasComercializacoes->get($id, [
            'contain' => ['Municipios', 'Metropoles'],
        ]);
        $locaisSalvos['municipios'] = [];
        $locaisSalvos['metropoles'] = [];
        foreach ($pfAreasComercializaco->municipios as $municipio) {
            $locaisSalvos['municipios'][$municipio->id] = $municipio->estado_id;
        }
        foreach ($pfAreasComercializaco->metropoles as $metropole) {
            $locaisSalvos['metropoles'][$metropole->id] = $metropole->estado_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            unset($data['municipios']);
            $metropoles = $data['metropoles'];
            unset($data['metropoles']);
            $municipios = $this->getPostMunicipios();
            $data['municipios'] = [];
            $data['metropoles'] = [];
            // ORDER METROPOLES
            foreach ($metropoles as $estado => $metropoles) {
                foreach ($metropoles as $metropole) {
                    $data['metropoles'][] = [
                        'id' => $metropole,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }

            // ORDER MUNICIPIOS
            foreach ($municipios as $estado => $municipios) {
                foreach ($municipios as $municipio) {
                    $data['municipios'][] = [
                        'id' => $municipio,
                        '_joinData' => [
                            'estado_id' => $estado
                        ]
                    ];
                }
            }

            $pfAreasComercializaco = $this->PfAreasComercializacoes->patchEntity($pfAreasComercializaco, $data);
            if ($this->PfAreasComercializacoes->save($pfAreasComercializaco)) {
                $this->Flash->success(__('The areas comercializaco has been saved.'));
                return $this->redirect('/pfAreasComercializacoes/index');
            } else {
                $this->Flash->error(__('The pf areas comercializaco could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('Estados');
        $estados = $this->Estados->find('all', ['valueField' => 'nome'])->contain(['Municipios', 'Metropoles']);

        $municipios = $this->PfAreasComercializacoes->Municipios->find('all')->orderAsc('nome');

        $pfOperadoras = $this->PfAreasComercializacoes->PfOperadoras
            ->find('list', ['valueField' => function ($e) {
                return $e->nome . ' - ' . $e->detalhe;
            }])
            ->orderAsc('nome')
            ->where([
                'OR' => [
                    'status IS NOT' => 'INATIVA',
                    'status IS' => null
                ],
                'nova' => 1
            ]);
        $this->set(compact('estados', 'municipios', 'pfAreasComercializaco', 'pfOperadoras', 'pfAtendimentos', 'locaisSalvos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Areas Comercializaco id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfAreasComercializaco = $this->PfAreasComercializacoes->get($id);
        if ($this->PfAreasComercializacoes->delete($pfAreasComercializaco)) {
            $this->Flash->success(__('The pf areas comercializaco has been deleted.'));
        } else {
            $this->Flash->error(__('The pf areas comercializaco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function duplicar($id = null)
    {
        $this->request->allowMethod(['post']);
        $pfAreasComercializaco = $this->PfAreasComercializacoes->get($id, ['contain' => ['Municipios']]);
        $data = clone $pfAreasComercializaco;
        unset($data->id);
        $data->new = true;
        $data->created = null;
        $data->modified = null;
        $new_municipios = [];
        foreach ($data->municipios as $municipio) {
            array_push($new_municipios, [
                'municipio_id' => $municipio->id,
                'estado_id' => $municipio->estado_id
            ]);
        }
        $data->municipios = array_values($new_municipios);
        $data = $data->toArray();

        $new_area = $this->PfAreasComercializacoes->newEntity();
        $new_area = $this->PfAreasComercializacoes->patchEntity($new_area, $data, ['associated' => ['Municipios']]);

        if ($this->PfAreasComercializacoes->save($new_area)) {
            $this->Flash->success(__('The pf areas comercializaco has been deleted.'));
        } else {
            $this->Flash->error(__('The pf areas comercializaco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getPostMunicipios()
    {
        $f = file_get_contents("php://input");
        $municipios = [];
        foreach (explode('&', urldecode($f)) as $item) {
            if (substr($item, 0, 10) == 'municipios') {
                (int) $posicao = str_replace(']', '', substr($item, 11, 2));
                if (intval($posicao)) {
                    $municipios[$posicao] = substr($item, strpos($item, '=') + 1);
                } else {
                    $municipios[] = substr($item, strpos($item, '=') + 1);
                }
            } elseif (substr($item, 0, 9) == 'municipio') {
                (int) $posicao = str_replace(']', '', substr($item, 10, 2));
                if (intval($posicao)) {
                    $municipios[$posicao][] = substr($item, strpos($item, '=') + 1);
                } else {
                    $municipios[] = substr($item, strpos($item, '=') + 1);
                }
            }
        };
        return $municipios;
    }
}
