<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfAtendimentos Controller
 *
 * @property \App\Model\Table\PfAtendimentosTable $PfAtendimentos
 */
class PfAtendimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pfAtendimentos = $this->paginate($this->PfAtendimentos);

        $this->set(compact('pfAtendimentos'));
        $this->set('_serialize', ['pfAtendimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Atendimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfAtendimento = $this->PfAtendimentos->get($id, [
            'contain' => ['PfTabelas']
        ]);

        $this->set('pfAtendimento', $pfAtendimento);
        $this->set('_serialize', ['pfAtendimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfAtendimento = $this->PfAtendimentos->newEntity();
        if ($this->request->is('post')) {
            $pfAtendimento = $this->PfAtendimentos->patchEntity($pfAtendimento, $this->request->data);
            if ($this->PfAtendimentos->save($pfAtendimento)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $this->set(compact('pfAtendimento'));
        $this->set('_serialize', ['pfAtendimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Atendimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfAtendimento = $this->PfAtendimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfAtendimento = $this->PfAtendimentos->patchEntity($pfAtendimento, $this->request->data);
            if ($this->PfAtendimentos->save($pfAtendimento)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $this->set(compact('pfAtendimento'));
        $this->set('_serialize', ['pfAtendimento']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Atendimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfAtendimento = $this->PfAtendimentos->get($id);
        if ($this->PfAtendimentos->delete($pfAtendimento)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
