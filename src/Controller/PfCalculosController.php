<?php

namespace App\Controller;

use App\Controller\AppController;
//use App\Utility\PdfWriter;
use Cake\I18n\Number;
use Cake\View\ViewBuilder;
use Knp\Snappy\Pdf;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Event\Event;
use App\Model\Entity\PfFiltrosTabela;

/**
 * PfCalculos Controller
 *
 * @property \App\Model\Table\PfCalculosTable $PfCalculos
 */
class PfCalculosController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.

        $this->Auth->allow(['cotacao']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        //        $this->paginate = [
        //            'contain' => ['PfProfissoes', 'Users']
        //        ];
        //        $pfCalculos = $this->paginate($this->PfCalculos);
        //
        //        $this->set(compact('pfCalculos'));
        //        $this->set('_serialize', ['pfCalculos']);
        //
        //
        //
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $userId = $sessao['id'];


        $query = $this->PfCalculos->find('all')->where(['user_id' => $userId])->order(['created' => 'DESC'])->contain(['PfCalculosDependentes', 'PfProfissoes']);
        $this->set('pfCalculos', $this->paginate($query));

        $this->loadModel('PfFiltros');
        $filtros = $this->PfFiltros->find('all')->where(['user_id' => $userId])->order(['created' => 'DESC'])->toArray();

        $this->loadModel('Status');
        $status = $this->Status->find('list', ['valueField' => 'nome'])->toArray();

        //        $simulacoes = $this->paginate($this->Simulacoes);
        $this->set('title', 'Cálculos Salvos');

        $this->set(compact('pfCalculos', 'filtros', 'status'));
        $this->set('_serialize', ['simulacoes']);
    }


    /**
     * 
     */
    public function visualizarCotacao(int $id)
    {
        $is_filtro = false;
        $cotacao = $this->PfCalculos->find("all")->contain(["PfCalculosDependentes"])->where(["PfCalculos.id" => $id])->first();

        $sessao = $this->request->getSession()->read('Auth.User');
        $ids_vitalmed = [92, 103];
        if ($cotacao['user_id'] == $sessao['id'] || $sessao['role'] == 'admin') {

            $total_vidas = 0;
            for ($i = 1; $i <= 10; $i++) {
                $total_vidas = $total_vidas + (int) $cotacao['faixa' . $i];
            }

            /**
             * ENTIDADES QUE ESTAO ASSOCIADAS A PROFISSAO
             **/
            $this->loadModel('PfEntidadesProfissoes');
            $this->loadModel('PfEntidadesGrupos');
            $entidades_grupos = [];

            //O ID 138 CORRESPONDE A PROFISSÃO "TODAS AS PROFISSÕES"
            if ($cotacao['pf_profissao_id'] == 138) {
                $entidades = $this->PfEntidadesProfissoes->find('all')
                    ->group('pf_entidade_id')
                    ->extract("pf_entidade_id")
                    ->toArray();
                $entidades_grupos = $this->PfEntidadesGrupos
                    ->find('list')
                    ->toArray();
            } else {
                $entidades = $this->PfEntidadesProfissoes->find('all')
                    ->where(['PfEntidadesProfissoes.pf_profissao_id' => $cotacao['pf_profissao_id']])
                    ->group('pf_entidade_id')
                    ->extract("pf_entidade_id")
                    ->toArray();
                $entidades_grupos = $this->PfEntidadesGrupos
                    ->find('list')
                    ->matching('PfEntidades.PfProfissoes', function ($q) use ($cotacao) {
                        return $q->where(['PfProfissoes.id' => $cotacao['pf_profissao_id']]);
                    })
                    ->toArray();
            }
            if (count($entidades) >= 1) {
                $this->loadModel('PfEntidadesTabelas');
                $tabelas_ids = $this->PfEntidadesTabelas->find('list', ['valueField' => 'pf_tabela_id'])->where(['pf_entidade_id IN' => $entidades])->group('pf_tabela_id')->toArray();
                if (empty($tabelas_ids)) {
                    $entidades = [
                        'PfTabelas.modalidade' => 'INDIVIDUAL'
                    ];
                } else {
                    $entidades = [
                        'OR' => [
                            ['PfTabelas.id IN' => $tabelas_ids],
                            ['PfTabelas.modalidade' => 'INDIVIDUAL']
                        ]
                    ];
                }
            } else {
                $entidades = [
                    'PfTabelas.modalidade' => 'INDIVIDUAL'
                ];
            }

            if (!empty($entidades_grupos)) {
                $entidades['OR'][] = ['pf_entidades_grupo_id IN' => $entidades_grupos];
            }

            if ($this->request->is('POST')) {
                $is_filtro = true;
                $data = $this->request->getData();
                $conditions = [];


                if (isset($data['pf_atendimento_id']) && !empty($data['pf_atendimento_id']) && $data['pf_atendimento_id'] <> 21) {
                    $array = ["PfTabelas.pf_atendimento_id" => $data['pf_atendimento_id']];
                    array_push($conditions, $array);
                }

                if (isset($data['pf_acomodacao_id']) && !empty($data['pf_acomodacao_id']) && $data['pf_acomodacao_id'] <> 4) {
                    $array = ["PfTabelas.pf_acomodacao_id" => $data['pf_acomodacao_id']];
                    array_push($conditions, $array);
                }
                if (isset($data['pf_cobertura_id']) && !empty($data['pf_cobertura_id']) && $data['pf_cobertura_id'] <> 99) {
                    $this->loadModel('PfCoberturas');
                    $array = ["PfTabelas.pf_cobertura_id" => $data['pf_cobertura_id']];
                    array_push($conditions, $array);
                }
                if (isset($data['abrangencia_id']) && !empty($data['abrangencia_id']) && $data['abrangencia_id'] <> 6) {
                    $array = ['PfTabelas.pf_atendimento_id' => $data['abrangencia_id']];
                    array_push($conditions, $array);
                }

                if ($data['filtroreembolso'] === '99') {
                    $data['filtroreembolso'] = '';
                }
                if (isset($data['filtroreembolso']) && !empty($data['filtroreembolso'])) {
                    $array = ['PfTabelas.reembolso' => $data['filtroreembolso']];
                    array_push($conditions, $array);
                }

                if ($data['coparticipacao'] === '99') {
                    $data['coparticipacao'] = '';
                }
                if (isset($data['coparticipacao']) && !empty($data['coparticipacao'])) {
                    $s = $data['coparticipacao'];
                    $array = ["PfTabelas.coparticipacao" => "$s"];
                    array_push($conditions, $array);
                }
                if (isset($data['municipios']) && !empty($data['municipios'])) {
                    $array = ['Municipios.id' => $data['municipios']];
                    array_push($conditions, $array);
                }
            }

            $this->loadModel('PfTabelas');
            ini_set('memory_limit', '-1');
            $tabelas_municipios = $this->PfTabelas->find('all')
                ->matching('PfAreasComercializacoes.Municipios', function ($q) use ($cotacao) {
                    return $q->where(['Municipios.estado_id' => $cotacao['estado_id']]);
                })
                ->contain([
                    'PfAtendimentos',
                    'PfAcomodacoes',
                    'PfCoberturas',
                    'PfFormasPagamentos',
                    'PfCarencias',
                    'PfRedes',
                    'PfReembolsos',
                    'PfDocumentos',
                    'PfDependentes',
                    'PfComercializacoes',
                    'PfObservacoes',
                    'PfProdutosGrupos',
                    'PfAreasComercializacoes' => [
                        'Metropoles',
                        'Municipios' => ['Estados'],
                        'PfOperadoras' => [
                            'Imagens',
                        ]
                    ],
                    'PfEntidadesGrupos' => [
                        'PfEntidades' => [
                            'PfProfissoes' =>  function ($q) {
                                return $q->order('nome');
                            }
                        ],
                    ],
                    'PfEntidades' => [
                        'PfProfissoes' =>  function ($q) {
                            return $q->order('nome');
                        }
                    ],
                ])
                ->where([
                    'PfTabelas.new' => 1,
                    'PfTabelas.validade' => 1,
                    'PfTabelas.minimo_vidas <=' => $total_vidas,
                    'PfTabelas.maximo_vidas >=' => $total_vidas,
                    $entidades
                ])
                ->group('PfTabelas.id');

            $tabelas_metropoles = $this->PfTabelas->find('all')
                ->matching('PfAreasComercializacoes.Metropoles.Municipios', function ($q) use ($cotacao) {
                    return $q->where(['Municipios.estado_id' => $cotacao['estado_id']]);
                })
                ->contain([
                    'PfAtendimentos',
                    'PfAcomodacoes',
                    'PfCoberturas',
                    'PfFormasPagamentos',
                    'PfCarencias',
                    'PfRedes',
                    'PfReembolsos',
                    'PfDocumentos',
                    'PfDependentes',
                    'PfComercializacoes',
                    'PfObservacoes',
                    'PfProdutosGrupos',
                    'PfAreasComercializacoes' => [
                        'Metropoles',
                        'Municipios' => ['Estados'],
                        'PfOperadoras' => [
                            'Imagens',
                        ]
                    ],
                    'PfEntidadesGrupos' => [
                        'PfEntidades' => [
                            'PfProfissoes' =>  function ($q) {
                                return $q->order('nome');
                            }
                        ],
                    ],
                    'PfEntidades' => [
                        'PfProfissoes' =>  function ($q) {
                            return $q->order('nome');
                        }
                    ],
                ])
                ->where([
                    'PfTabelas.new' => 1,
                    'PfTabelas.validade' => 1,
                    'PfTabelas.minimo_vidas <=' => $total_vidas,
                    'PfTabelas.maximo_vidas >=' => $total_vidas,
                    $entidades
                ])
                ->group('PfTabelas.id');

            if ($is_filtro && !empty($conditions)) {
                $tabelas_municipios->where($conditions);
                $tabelas_metropoles->where($conditions);
            }

            $tabelas = array_merge($tabelas_municipios->toArray(), $tabelas_metropoles->toArray());

            $total_tabelas = count($tabelas);
            $precos = $tabelas;

            array_multisort(array_column($precos, 'faixa1'), SORT_ASC, SORT_NUMERIC, $precos);
            foreach ($precos as $a) {

                if (
                    $a['faixa1'] +
                    $a['faixa2'] +
                    $a['faixa3'] +
                    $a['faixa4'] +
                    $a['faixa5'] +
                    $a['faixa6'] +
                    $a['faixa7'] +
                    $a['faixa8'] +
                    $a['faixa9'] +
                    $a['faixa10'] +
                    $a['faixa11'] +
                    $a['faixa12'] > 0
                ) {
                    $menor_preco = $a;
                    break;
                }
            }

            $intermediario = $precos[count($precos) / 2];
            $maior_preco = end($precos);

            $tabelas_ordenadas = [];
            $area_adicionada = [];
            $grupo_produtos_adicionados = [];

            $i = 0;
            foreach ($tabelas as $tabela) {
                $operadora_id = $tabela['pf_areas_comercializaco']['pf_operadora']['id'];


                @$tabelas_ordenadas[$operadora_id]['operadora'] = $tabela['pf_areas_comercializaco']['pf_operadora'];

                @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['pf_atendimento']['nome']] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_atendimento');

                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_areas_comercializaco');
                if (!in_array($tabela['pf_areas_comercializaco']['id'], $area_adicionada)) {
                    foreach ($tabela['pf_areas_comercializaco']['metropoles'] as $metropole) {
                        @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                    }
                    foreach ($tabela['pf_areas_comercializaco']['municipios'] as $municipio) {
                        @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                    }
                    $area_adicionada[] = $tabela['pf_areas_comercializaco']['id'];
                }
                unset($tabela['pf_areas_comercializaco']['municipios']);
                unset($tabela['pf_areas_comercializaco']['metropoles']);

                // NÃO REPETIÇÃO DOS GRUPOS DE ENTIDADES NAS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['tabelas'] .= $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_entidades_grupo');
                if (!empty($tabela['pf_entidades_grupo']) && !in_array($tabela['pf_entidades_grupo']['id'], $grupo_produtos_adicionados)) {
                    foreach ($tabela['pf_entidades_grupo']['pf_entidades'] as $entidade) {
                        @$tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] .=  $entidade['nome'] . ':';
                        foreach ($entidade['pf_profissoes'] as $profissao) {
                            $tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] .= $profissao['nome'] . ', ';
                        }
                        $tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] = substr($tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'], 0, -2) . '; ';
                    }
                    $grupo_produtos_adicionados[] = $tabela['pf_entidades_grupo']['id'];
                }
                unset($tabela['pf_entidades_grupo']);

                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_observaco');
                @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['data'] =  nl2br($tabela['pf_observaco']['descricao']);
                unset($tabela['pf_observaco']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_rede');
                @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['data'] =  nl2br($tabela['pf_rede']['descricao']);
                unset($tabela['pf_rede']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'reembolsoEntity');
                @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
                unset($tabela['pf_reembolsoEntity']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_carencia');
                @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['data'] =  nl2br($tabela['pf_carencia']['descricao']);
                unset($tabela['pf_carencia']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_dependente');
                @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['data'] =  nl2br($tabela['pf_dependente']['descricao']);
                unset($tabela['pf_dependentes']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_documento');
                @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['data'] =  nl2br($tabela['pf_documento']['descricao']);
                unset($tabela['pf_documentos']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_formas_pagamento');
                @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['data'] =  nl2br($tabela['pf_formas_pagamento']['descricao']);
                unset($tabela['pf_formas_pagamento']);

                $tabela['cobertura'] = $tabela['pf_cobertura'];
                $tabela['acomodacao'] = $tabela['pf_acomodaco'];

                @$tabelas_ordenadas[$operadora_id]['tabelas'][$tabela['id']] = $tabela;
            }
            $tabelasOrdenadas = $tabelas_ordenadas;

            $this->loadModel('PfOperadoras');
            $operadoras = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->where(['NOT(PfOperadoras.status <=> \'INATIVA\')', 'nova' => 1])->toArray();

            // DADOS PARA FILTRO
            $simulacao = $cotacao; //ALIAS ELEMENT FILTRO
            $totalTabelas = $total_tabelas; //ALIAS ELEMENT FILTRO
            $this->loadModel('PfAcomodacoes');
            $pfAcomodacoes = $this->PfAcomodacoes->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

            $this->loadModel('PfAtendimentos');
            $pfAtendimentos = $this->PfAtendimentos->find('list', ['valueField' => 'nome', 'order' => ['prioridade' => 'ASC']])->toArray();

            $this->loadModel('PfCoberturas');
            $pfCoberturas = $this->PfCoberturas->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

            $this->loadModel('Municipios');
            $municipios = $this->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $simulacao['estado_id']])->toArray();

            $simulacao['rede'] = 1;
            $simulacao['carencia'] = 1;
            $simulacao['reembolso'] = 1;
            $simulacao['info'] = 1;
            $simulacao['opcionais'] = 1;
            $simulacao['observaco'] = 1;

            $this->set(compact('ids_vitalmed', 'cotacao', 'simulacao', 'tabelas_ordenadas', 'total_tabelas', 'totalTabelas', 'menor_preco', 'intermediario', 'maior_preco', 'total_vidas', 'operadoras', 'pfAcomodacoes', 'pfAtendimentos', 'pfCoberturas', 'municipios', 'is_filtro'));
        } else {
            $this->Flash->error(__('Sem permissão de visualizar este Cálculo. Cálculo pertence a outro usuário.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function auxGetNomeProduto(&$grupo_adicionado, $produto, $campo)
    {
        $grupo_id = $produto->pf_produtos_grupo_id;

        if (is_null($grupo_id) || empty($grupo_id)) {
            $nome = $produto->nome . ' • ';
        } else {
            if (in_array($produto->$campo->id, $grupo_adicionado[$grupo_id][$campo])) {
                $nome = "";
            } else {
                $nome = $produto->pf_produtos_grupo->nome . ' • ';
                $grupo_adicionado[$grupo_id][$campo][] = $produto->$campo->id;
            }
        }
        return $nome;
    }

    /**
     * View method
     *
     * @param string|null $id Pf Calculo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('PfOperadoras');
        $operadoras = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->where(['NOT(PfOperadoras.status <=> \'INATIVA\')', 'nova' => 1])->toArray();
        $simulacao = $this->PfCalculos->find("all")->contain(["PfCalculosDependentes"])->where(["PfCalculos.id" => $id])->first();

        $this->loadModel("PfPdffiltros");
        $filtro = $this->PfPdffiltros->find("all")->where(["pf_calculo_id" => $id])->order(["created" => "DESC"])->first();

        if ($simulacao['user_id'] == $sessao['id'] || $sessao['role'] == 'admin') {

            $session->delete('Auth.User.pf_tabelas');
            $session->delete('Auth.User.pf_operadoras');
            $session->renew();

            $totalVidas = 0;
            for ($i = 1; $i <= 10; $i++) {
                $totalVidas = $totalVidas + (int) $simulacao['faixa' . $i];
            }

            //ENTIDADES QUE ESTAO ASSOCIADAS A PROFISSAO
            $this->loadModel('PfEntidadesProfissoes');

            //O ID 138 CORRESPONDE A PROFISSÃO "TODAS AS PROFISSÕES"
            if ($simulacao['pf_profissao_id'] == 138) {
                $entidadesIds = $this->PfEntidadesProfissoes->find('all')->extract("pf_entidade_id")->toArray();
            } else {
                $entidadesIds = $this->PfEntidadesProfissoes->find('all')->where(['PfEntidadesProfissoes.pf_profissao_id' => $simulacao['pf_profissao_id']])->extract("pf_entidade_id")->toArray();
            }
            if (count($entidadesIds) >= 1) {

                $this->loadModel('PfEntidadesTabelas');
                $tabelas_ids = $this->PfEntidadesTabelas->find('list', ['valueField' => 'pf_tabela_id'])->where(['pf_entidade_id IN' => $entidadesIds])->group('pf_tabela_id')->toArray();
                $entidades = [
                    'OR' => [
                        ['PfTabelas.id IN' => $tabelas_ids],
                        ['PfTabelas.modalidade' => 'INDIVIDUAL']
                    ]
                ];
            } else {
                // $this->loadModel('PfEntidadesPfOperadoras');
                // $this->loadModel('PfOperadoras');
                // $operadorasEnt = $this->PfEntidadesPfOperadoras->find('all')->extract("pf_operadora_id")->toArray();
                // $operadorasIds = $this->PfOperadoras->find('list')->where(['id NOT IN ' => $operadorasEnt])->toArray();
                $entidades = [
                    'PfTabelas.modalidade' => 'INDIVIDUAL'
                ];
            }

            $this->loadModel('PfTabelas');
            $tabelas = $this->PfTabelas->find('all')
                ->matching('PfAreasComercializacoes.Municipios', function ($q) use ($simulacao) {
                    return $q->where(['Municipios.estado_id' => $simulacao['estado_id']]);
                })
                ->contain([
                    'PfAtendimentos',
                    'PfAcomodacoes',
                    'PfCoberturas',
                    'PfAreasComercializacoes' => [
                        'PfOperadoras' => [
                            'Imagens',
                            'PfFormasPagamentos',
                            'PfCarencias',
                            'PfRedes',
                            'PfReembolsos',
                            'PfDocumentos',
                            'PfDependentes',
                            'PfComercializacoes',
                            'PfObservacoes'
                        ]
                    ]
                ])
                ->where([
                    'PfTabelas.new' => 1,
                    'PfTabelas.validade' => 1,
                    'PfTabelas.minimo_vidas <=' => $totalVidas,
                    'PfTabelas.maximo_vidas >=' => $totalVidas,
                    $entidades
                ])
                ->group('PfTabelas.id')
                ->toArray();
            $totalTabelas = count($tabelas);

            foreach ($tabelas as $tabela) {
                $idsSessao[$tabela["id"]] = $tabela["id"];
            }

            $sessao = $session->read('Auth.User');
            $tabelaspreco = $tabelas;
            foreach ($tabelaspreco as $key => $row) {
                $tt[$key] = $row['faixa1'];
            }

            array_multisort($tt, SORT_ASC, $tabelaspreco);

            foreach ($tabelaspreco as $a) {

                if (
                    $a['faixa1'] +
                    $a['faixa2'] +
                    $a['faixa3'] +
                    $a['faixa4'] +
                    $a['faixa5'] +
                    $a['faixa6'] +
                    $a['faixa7'] +
                    $a['faixa8'] +
                    $a['faixa9'] +
                    $a['faixa10'] +
                    $a['faixa11'] +
                    $a['faixa12'] > 0
                ) {
                    $maisbarato = $a;
                    break;
                }
            }
            $intermediario = $tabelaspreco[count($tabelas) / 2];
            $maiscaro = end($tabelaspreco);

            foreach ($tabelas as $tabela) {
                if ($tabela['coparticipacao'] == 'n') {
                    $coparticipacao = 'S/COPARTICIPAÇÃO';
                } else {
                    $coparticipacao = 'C/COPARTICIPAÇÃO';
                }

                $tabelasOrdenadas[$tabela['pf_areas_comercializaco']['pf_operadora']['id']][$tabela['nome']][$tabela['nome'] . " - " . $tabela['pf_acomodaco']['nome'] . " - " . $coparticipacao] = $tabela;
            }
        } else {
            $this->Flash->error(__('Sem permissão de visualizar este Cálculo. Cálculo pertence a outro usuário.'));
            return $this->redirect(['action' => 'index']);
        }

        $this->loadModel('PfAcomodacoes');
        $pfAcomodacoes = $this->PfAcomodacoes->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('PfAtendimentos');
        $pfAtendimentos = $this->PfAtendimentos->find('list', ['valueField' => 'nome', 'order' => ['prioridade' => 'ASC']])->toArray();

        $this->loadModel('PfCoberturas');
        $pfCoberturas = $this->PfCoberturas->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $simulacao['rede'] = 1;
        $simulacao['carencia'] = 1;
        $simulacao['reembolso'] = 1;
        $simulacao['info'] = 1;
        $simulacao['opcionais'] = 1;
        $simulacao['observaco'] = 1;
        $this->set(compact("filtro", "maisbarato", "intermediario", "maiscaro", 'profissoesPf', 'ordenada', 'simulacao', 'pfAtendimentos', 'pfAcomodacoes', 'pfCoberturas', 'tabelasOrdenadas', 'operadoras', 'totalTabelas'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfCalculo = $this->PfCalculos->newEntity();
        if ($this->request->is('post')) {

            $calculo = $this->request->data;

            //Adicionar titular por faixa
            switch ($calculo['idade_titular']) {
                case ($calculo['idade_titular'] <= 18 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa1'] = (int) $calculo['faixa1'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 19 && $calculo['idade_titular'] <= 23 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa2'] = (int) $calculo['faixa2'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 24 && $calculo['idade_titular'] <= 28 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa3'] = (int) $calculo['faixa3'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 29 && $calculo['idade_titular'] <= 33 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa4'] = (int) $calculo['faixa4'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 34 && $calculo['idade_titular'] <= 38 ? $calculo['idade_titular'] : !$calculo['idade_titular']):

                    $calculo['faixa5'] = (int) $calculo['faixa5'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 39 && $calculo['idade_titular'] <= 43 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa6'] = (int) $calculo['faixa6'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 44 && $calculo['idade_titular'] <= 48 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa7'] = (int) $calculo['faixa7'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 49 && $calculo['idade_titular'] <= 53 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa8'] = (int) $calculo['faixa8'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 54 && $calculo['idade_titular'] <= 58 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa9'] = (int) $calculo['faixa9'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 59 && $calculo['idade_titular'] <= 64 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa10'] = (int) $calculo['faixa10'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 65 && $calculo['idade_titular'] <= 80 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa11'] = (int) $calculo['faixa11'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 81 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa12'] = (int) $calculo['faixa12'] + 1;
                    break;
            }
            //Adiciona dependentes as faixas etárias
            if (isset($calculo['Dependentes'])) {
                foreach ($calculo['Dependentes'] as $dependente) {
                    switch ($dependente) {
                        case ($dependente <= 18 ? $dependente : !$dependente):
                            $calculo['faixa1'] = (int) $calculo['faixa1'] + 1;
                            break;
                        case ($dependente >= 19 && $dependente <= 23 ? $dependente : !$dependente):
                            $calculo['faixa2'] = (int) $calculo['faixa2'] + 1;
                            break;
                        case ($dependente >= 24 && $dependente <= 28 ? $dependente : !$dependente):
                            $calculo['faixa3'] = (int) $calculo['faixa3'] + 1;
                            break;
                        case ($dependente >= 29 && $dependente <= 33 ? $dependente : !$dependente):
                            $calculo['faixa4'] = (int) $calculo['faixa4'] + 1;
                            break;
                        case ($dependente >= 34 && $dependente <= 38 ? $dependente : !$dependente):
                            $calculo['faixa5'] = (int) $calculo['faixa5'] + 1;
                            break;
                        case ($dependente >= 39 && $dependente <= 43 ? $dependente : !$dependente):
                            $calculo['faixa6'] = (int) $calculo['faixa6'] + 1;
                            break;
                        case ($dependente >= 44 && $dependente <= 48 ? $dependente : !$dependente):
                            $calculo['faixa7'] = (int) $calculo['faixa7'] + 1;
                            break;
                        case ($dependente >= 49 && $dependente <= 53 ? $dependente : !$dependente):
                            $calculo['faixa8'] = (int) $calculo['faixa8'] + 1;
                            break;
                        case ($dependente >= 54 && $dependente <= 58 ? $dependente : !$dependente):
                            $calculo['faixa9'] = (int) $calculo['faixa9'] + 1;
                            break;
                        case ($dependente >= 59 && $dependente <= 64 ? $dependente : !$dependente):
                            $calculo['faixa10'] = (int) $calculo['faixa10'] + 1;
                            break;
                        case ($dependente >= 65 && $dependente <= 80 ? $dependente : !$dependente):
                            $calculo['faixa11'] = (int) $calculo['faixa11'] + 1;
                            break;
                        case ($dependente >= 81 ? $dependente : !$dependente):
                            $calculo['faixa12'] = (int) $calculo['faixa12'] + 1;
                            break;
                    }
                }
            }

            $i = 1;
            $totalVidas = 0;
            for ($i = 1; $i <= 12; $i++) {
                $totalVidas = $totalVidas + (int) $calculo['faixa' . $i];
            }
            $this->loadModel('PfTabelas');
            if ($calculo['modalidade'] === 'ADESAO') {
                $this->loadModel('PfProfissoesTabelas');
                $pf_tabelas = $this->PfProfissoesTabelas->find('all', ['contain' => [
                    'PfTabelas' => ['PfComercializacoes', 'Estados', 'PfAtendimentos', 'PfAcomodacoes', 'PfProdutos' => [
                        'PfOperadoras' => ['Imagens', 'PfCarencias', 'PfRedes', 'PfReembolsos', 'PfDocumentos', 'PfDependentes', 'PfObservacoes'], 'TiposProdutos'
                    ]]
                ]])
                    ->where([
                        'PfProfissoesTabelas.pf_profissao_id' => $calculo['pf_profissao_id'],
                        'PfTabelas.validade' => 1,
                        'PfTabelas.estado_id' => $calculo['estado_id'],
                        'PfTabelas.minimo_vidas <=' => $totalVidas,
                        'PfTabelas.maximo_vidas >=' => $totalVidas
                    ])->toArray();

                foreach ($pf_tabelas as $tabela) {
                    switch ($tabela['pf_tabela']['coparticipacao']) {
                        case 'n':
                            $coparticipacao = 'S/ COPARTICIPACAO';
                            break;
                        case 's':
                            $coparticipacao = 'C/ COPARTICIPACAO - ' . $tabela['pf_tabela']['detalhe_coparticipacao'];
                            break;
                    }
                    $tabelasOrdenadas[$tabela['pf_tabela']['pf_produto']['pf_operadora']['id']][$tabela['pf_tabela']['pf_produto']['nome']][$tabela['pf_tabela']['nome'] . " - " . $tabela['pf_tabela']['pf_acomodaco']['nome'] . " - " . $coparticipacao] = $tabela['pf_tabela'];
                }
            } else {
                $pf_tabelas = $this->PfTabelas->find('all', ['contain' => [
                    'PfComercializacoes', 'Estados', 'PfAtendimentos', 'PfAcomodacoes', 'PfProdutos' => [
                        'PfOperadoras' => ['PfCarencias', 'PfRedes', 'PfReembolsos', 'PfDocumentos', 'PfDependentes', 'PfObservacoes'], 'TiposProdutos'
                    ]
                ]])
                    ->where([
                        'PfTabelas.validade' => 1,
                        'PfTabelas.estado_id' => $calculo['estado_id'],
                        'PfTabelas.minimo_vidas <=' => $totalVidas,
                        'PfTabelas.maximo_vidas >=' => $totalVidas
                    ])->toArray();
                foreach ($pf_tabelas as $tabela) {
                    switch ($tabela['coparticipacao']) {
                        case 'n':
                            $coparticipacao = 'S/ COPARTICIPACAO';
                            break;
                        case 's':
                            $coparticipacao = 'C/ COPARTICIPACAO - ' . $tabela['pf_tabela']['detalhe_coparticipacao'];
                            break;
                    }
                    $tabelasOrdenadas[$tabela['pf_produto']['pf_operadora']['id']][$tabela['pf_produto']['nome']][$tabela['nome'] . " - " . $tabela['pf_acomodaco']['nome'] . " - " . $coparticipacao] = $tabela;
                }
            }

            $pfCalculo = $this->PfCalculos->patchEntity($pfCalculo, $this->request->data);
            // debug($pfCalculo);
            // die;
            if ($this->PfCalculos->save($pfCalculo)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $this->loadModel('Estados');
        $estados = $this->Estados
            ->find('list', ['valueField' => 'nome'])
            ->matching('PfAreasComercializacoes.PfTabelas', function ($q) {
                return $q->where(['validade' => 1]);
            })
            ->order(['Estados.nome' => 'asc']);

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $pfProfissoes = $this->PfCalculos->PfProfissoes->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => '1 = 1'])->order(['PfProfissoes.nome' => 'ASC'])->toArray();
        /*  $array = array(8888 => "TODAS AS PROFISSÕES", 9999 => "SEM PROFISSÃO");
        $pfProfissoes = $array + $pfProfissoes; */
        $users = $this->PfCalculos->Users->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => '1 = 1']);
        //         $tabelas = $this->PfCalculos->PfTabelas->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $this->set(compact('estados', 'sessao', 'pfCalculo', 'pfProfissoes', 'tabelasOrdenadas', 'users'));
        $this->set('_serialize', ['pfCalculo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Calculo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfCalculo = $this->PfCalculos->get($id, ['contain' => ['PfCalculosDependentes']]);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        if ($this->request->is(['patch', 'post', 'put'])) {

            $userId = $sessao['id'];
            $calculo = $this->request->data;

            //Se houver tabelas anteriormente associadas ao Cálculo removo - MODEL PFCALCULOSTABELAS
            $this->loadModel('PfCalculosTabelas');
            $pf_calculos_tabelas = $this->PfCalculosTabelas->find('list')->where(['PfCalculosTabelas.pf_calculo_id' => $id]);
            if (count($pf_calculos_tabelas) > 0) {
                foreach ($pf_calculos_tabelas as $pfcalctab) {
                    $delCalcTabela = $this->PfCalculosTabelas->get($pfcalctab);
                    $this->PfCalculosTabelas->delete($delCalcTabela);
                }
            }
            //Se houver tabelas anteriormente associadas ao Cálculo removo - MODEL PFCALCULOSDEPENDENTES
            $this->loadModel('PfCalculosDependentes');
            $pf_calculos_dependentes = $this->PfCalculosDependentes->find('list')->where(['PfCalculosDependentes.pf_calculo_id' => $id]);
            if (count($pf_calculos_dependentes) > 0) {
                foreach ($pf_calculos_dependentes as $pfcalcdep) {
                    $delCalcDependente = $this->PfCalculosDependentes->get($pfcalcdep);
                    $this->PfCalculosDependentes->delete($delCalcDependente);
                }
            }
            $i = 1;
            for ($i = 1; $i <= 12; $i++) {
                $calculo['faixa' . $i] = null;
            }
            switch ($calculo['idade_titular']) {
                case ($calculo['idade_titular'] <= 18 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa1'] = (int) $calculo['faixa1'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 19 && $calculo['idade_titular'] <= 23 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa2'] = (int) $calculo['faixa2'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 24 && $calculo['idade_titular'] <= 28 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa3'] = (int) $calculo['faixa3'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 29 && $calculo['idade_titular'] <= 33 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa4'] = (int) $calculo['faixa4'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 34 && $calculo['idade_titular'] <= 38 ? $calculo['idade_titular'] : !$calculo['idade_titular']):

                    $calculo['faixa5'] = (int) $calculo['faixa5'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 39 && $calculo['idade_titular'] <= 43 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa6'] = (int) $calculo['faixa6'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 44 && $calculo['idade_titular'] <= 48 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa7'] = (int) $calculo['faixa7'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 49 && $calculo['idade_titular'] <= 53 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa8'] = (int) $calculo['faixa8'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 54 && $calculo['idade_titular'] <= 58 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa9'] = (int) $calculo['faixa9'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 59 && $calculo['idade_titular'] <= 64 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa10'] = (int) $calculo['faixa10'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 65 && $calculo['idade_titular'] <= 80 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa11'] = (int) $calculo['faixa11'] + 1;
                    break;
                case ($calculo['idade_titular'] >= 81 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                    $calculo['faixa12'] = (int) $calculo['faixa12'] + 1;
                    break;
            }

            //Adiciona dependentes as faixas etárias
            if (isset($calculo['Dependentes'])) {
                foreach ($calculo['Dependentes'] as $dependente) {
                    switch ($dependente) {
                        case ($dependente <= 18 ? $dependente : !$dependente):
                            $calculo['faixa1'] = (int) $calculo['faixa1'] + 1;
                            break;
                        case ($dependente >= 19 && $dependente <= 23 ? $dependente : !$dependente):
                            $calculo['faixa2'] = (int) $calculo['faixa2'] + 1;
                            break;
                        case ($dependente >= 24 && $dependente <= 28 ? $dependente : !$dependente):
                            $calculo['faixa3'] = (int) $calculo['faixa3'] + 1;
                            break;
                        case ($dependente >= 29 && $dependente <= 33 ? $dependente : !$dependente):
                            $calculo['faixa4'] = (int) $calculo['faixa4'] + 1;
                            break;
                        case ($dependente >= 34 && $dependente <= 38 ? $dependente : !$dependente):
                            $calculo['faixa5'] = (int) $calculo['faixa5'] + 1;
                            break;
                        case ($dependente >= 39 && $dependente <= 43 ? $dependente : !$dependente):
                            $calculo['faixa6'] = (int) $calculo['faixa6'] + 1;
                            break;
                        case ($dependente >= 44 && $dependente <= 48 ? $dependente : !$dependente):
                            $calculo['faixa7'] = (int) $calculo['faixa7'] + 1;
                            break;
                        case ($dependente >= 49 && $dependente <= 53 ? $dependente : !$dependente):
                            $calculo['faixa8'] = (int) $calculo['faixa8'] + 1;
                            break;
                        case ($dependente >= 54 && $dependente <= 58 ? $dependente : !$dependente):
                            $calculo['faixa9'] = (int) $calculo['faixa9'] + 1;
                            break;
                        case ($dependente >= 59 && $dependente <= 64 ? $dependente : !$dependente):
                            $calculo['faixa10'] = (int) $calculo['faixa10'] + 1;
                            break;
                        case ($dependente >= 65 && $dependente <= 80 ? $dependente : !$dependente):
                            $calculo['faixa11'] = (int) $calculo['faixa11'] + 1;
                            break;
                        case ($dependente >= 81 ? $dependente : !$dependente):
                            $calculo['faixa12'] = (int) $calculo['faixa12'] + 1;
                            break;
                    }
                }
            }

            $i = 1;
            $totalVidas = 0;
            for ($i = 1; $i <= 12; $i++) {
                $totalVidas = $totalVidas + (int) $calculo['faixa' . $i];
            }
            $this->loadModel('PfTabelas');
            $pf_tabelas = $this->PfTabelas->find('all', ['contain' => [
                'PfComercializacoes', 'Estados', 'PfAtendimentos', 'PfAcomodacoes', 'PfProdutos' => [
                    'PfOperadoras' => ['Imagens', 'PfCarencias', 'PfRedes', 'PfReembolsos', 'PfDocumentos', 'PfDependentes', 'PfObservacoes'], 'TiposProdutos'
                ]
            ]])
                ->where([
                    'PfTabelas.validade' => 1,
                    'PfTabelas.modalidade' => 'INDIVIDUAL',
                    'PfTabelas.estado_id' => $calculo['estado_id'],
                    'PfTabelas.minimo_vidas <=' => $totalVidas,
                    'PfTabelas.maximo_vidas >=' => $totalVidas
                ])->toArray();
            foreach ($pf_tabelas as $tabela) {
                switch ($tabela['coparticipacao']) {
                    case 'n':
                        $coparticipacao = 'S/ COPARTICIPACAO';
                        break;
                    case 's':
                        $coparticipacao = 'C/ COPARTICIPACAO - ' . $tabela['pf_tabela']['detalhe_coparticipacao'];
                        break;
                }
                $tabelasOrdenadas[$tabela['pf_produto']['pf_operadora']['id']][$tabela['pf_produto']['nome']][$tabela['nome'] . " - " . $tabela['pf_acomodaco']['nome'] . " - " . $coparticipacao] = $tabela;
            }
            //            }
            if (isset($calculo['Dependentes'])) {
                $dependentes = $calculo['Dependentes'];
                //                unset($calculo['Dependentes']);
                //                unset($calculo['dependentes']);
            }


            $calculo['user_id'] = $userId;
            $calculo['id'] = $id;

            //            $pfCalculo = $this->PfCalculos->newEntity();
            $pfCalculo = $this->PfCalculos->patchEntity($pfCalculo, $calculo);
            //            debug($pfCalculo);
            //            debug($this->PfCalculos->save($pfCalculo));
            //            die();


            if ($this->PfCalculos->save($pfCalculo)) {

                $this->loadModel('Users');
                $userLogado = $this->Users->get($userId);

                $userLogado->ultimocalculo = Time::now();
                $userLogado->dirty('ultimocalculo', true);
                $this->Users->save($userLogado);

                $this->loadModel('Tabelas');

                if (isset($dependentes)) {
                    $this->loadModel('PfCalculosDependentes');
                    foreach ($dependentes as $chave => $dependente) {
                        $dados['pf_calculo_id'] = $id;
                        $dados['idade_dependente'] = $dependente;

                        $calculosDependentes = $this->PfCalculosDependentes->newEntity();
                        $calculosDependentes = $this->PfCalculosDependentes->patchEntity($calculosDependentes, $dados);
                        $this->PfCalculosDependentes->save($calculosDependentes);
                    }
                }

                $this->loadModel('PfCalculosTabelas');
                //                    debug($pf_tabelas);
                foreach ($pf_tabelas as $idTabela) {
                    //                    debug($idTabela);

                    $dadosCalculo['pf_calculo_id'] = $id;
                    //                    if ($calculo['modalidade'] == 'ADESAO') {
                    //                        $dadosCalculo['pf_tabela_id'] = $idTabela['pf_tabela']['id'];
                    //                    } else {
                    $dadosCalculo['pf_tabela_id'] = $idTabela['id'];
                    //                    }
                    //                    debug($dadosCalculo);
                    $calculosTabelas = $this->PfCalculosTabelas->newEntity();
                    $calculosTabelas = $this->PfCalculosTabelas->patchEntity($calculosTabelas, $dadosCalculo);
                    //                    debug($calculosTabelas);
                    //                    debug($this->PfCalculosTabelas->save($calculosTabelas));
                    //                     die();
                    $this->PfCalculosTabelas->save($calculosTabelas);
                }
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
                return $this->redirect(['action' => 'add']);
            }
        }
        $this->loadModel('PfProfissoes');
        $pfProfissoes = $this->PfProfissoes->find('list', ['valueField' => 'nome']);
        $users = $this->PfCalculos->Users->find('list', ['valueField' => 'nome']);


        $this->loadModel('PfTabelas');
        $estadosContados = $this->PfTabelas
            ->find('all', ['empty' => 'SELECIONE'])
            ->select(['estado_id'])
            ->distinct(['estado_id'])
            ->where(['validade' => 1])
            ->toArray();
        $array = array();
        foreach ($estadosContados as $estadosContados) {
            $array[] = $estadosContados['estado_id'];
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC'], 'empty' => 'SELECIONE'])->where(['id IN' => $array]);

        $this->set(compact('pfCalculo', 'estados', 'pfProfissoes', 'users', 'sessao'));
        $this->set('_serialize', ['pfCalculo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Calculo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfCalculo = $this->PfCalculos->get($id);
        if ($this->PfCalculos->delete($pfCalculo)) {
            $this->Flash->success(__('O Cálculo foi Deletado.'));
        } else {
            $this->Flash->error(__('O Cálculo não foi Deletado. Por favor, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Renderiza o total de dependentes e exibe campos para digitar idades
     *
     * @param string|null $id Pf Calculo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function dependentes($qtde = null)
    {
        $this->set(compact('qtde'));
    }

    /**
     * Renderiza o total de dependentes e exibe campos para digitar idades
     *
     * @param string|null $id Pf Calculo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function encontrarTabelas($data = null)
    {
        $calculo = $this->request->data;

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $userId = $sessao['id'];

        $calculo['faixa1'] = 0;
        $calculo['faixa2'] = 0;
        $calculo['faixa3'] = 0;
        $calculo['faixa4'] = 0;
        $calculo['faixa5'] = 0;
        $calculo['faixa6'] = 0;
        $calculo['faixa7'] = 0;
        $calculo['faixa8'] = 0;
        $calculo['faixa9'] = 0;
        $calculo['faixa10'] = 0;

        //Adicionar titular por faixa
        switch ($calculo['idade_titular']) {
            case ($calculo['idade_titular'] <= 18 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                $calculo['faixa1'] = (int) $calculo['faixa1'] + 1;
                break;
            case ($calculo['idade_titular'] >= 19 && $calculo['idade_titular'] <= 23 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                $calculo['faixa2'] = (int) $calculo['faixa2'] + 1;
                break;
            case ($calculo['idade_titular'] >= 24 && $calculo['idade_titular'] <= 28 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                $calculo['faixa3'] = (int) $calculo['faixa3'] + 1;
                break;
            case ($calculo['idade_titular'] >= 29 && $calculo['idade_titular'] <= 33 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                $calculo['faixa4'] = (int) $calculo['faixa4'] + 1;
                break;
            case ($calculo['idade_titular'] >= 34 && $calculo['idade_titular'] <= 38 ? $calculo['idade_titular'] : !$calculo['idade_titular']):

                $calculo['faixa5'] = (int) $calculo['faixa5'] + 1;
                break;
            case ($calculo['idade_titular'] >= 39 && $calculo['idade_titular'] <= 43 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                $calculo['faixa6'] = (int) $calculo['faixa6'] + 1;
                break;
            case ($calculo['idade_titular'] >= 44 && $calculo['idade_titular'] <= 48 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                $calculo['faixa7'] = (int) $calculo['faixa7'] + 1;
                break;
            case ($calculo['idade_titular'] >= 49 && $calculo['idade_titular'] <= 53 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                $calculo['faixa8'] = (int) $calculo['faixa8'] + 1;
                break;
            case ($calculo['idade_titular'] >= 54 && $calculo['idade_titular'] <= 58 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                $calculo['faixa9'] = (int) $calculo['faixa9'] + 1;
                break;
            case ($calculo['idade_titular'] >= 59 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
                $calculo['faixa10'] = (int) $calculo['faixa10'] + 1;
                break;
                /*
              case ($calculo['idade_titular'] >= 65 && $calculo['idade_titular'] <= 80 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
              $calculo['faixa11'] = (int) $calculo['faixa11'] + 1;
              break;
              case ($calculo['idade_titular'] >= 81 ? $calculo['idade_titular'] : !$calculo['idade_titular']):
              $calculo['faixa12'] = (int) $calculo['faixa12'] + 1;
              break;
             */
        }

        //Adiciona dependentes as faixas etárias
        if (isset($calculo['Dependentes'])) {
            foreach ($calculo['Dependentes'] as $dependente) {
                if (isset($dependente) && $dependente != '') {
                    if ($dependente <= 18) {
                        $calculo['faixa1'] = $calculo['faixa1'] + 1;
                    }
                    if ($dependente >= 19 && $dependente <= 23) {
                        $calculo['faixa2'] = $calculo['faixa2'] + 1;
                    }
                    if ($dependente >= 24 && $dependente <= 28) {
                        $calculo['faixa3'] = $calculo['faixa3'] + 1;
                    }
                    if ($dependente >= 29 && $dependente <= 33) {
                        $calculo['faixa4'] = $calculo['faixa4'] + 1;
                    }
                    if ($dependente >= 34 && $dependente <= 38) {
                        $calculo['faixa5'] = $calculo['faixa5'] + 1;
                    }
                    if ($dependente >= 39 && $dependente <= 43) {
                        $calculo['faixa6'] = $calculo['faixa6'] + 1;
                    }
                    if ($dependente >= 44 && $dependente <= 48) {
                        $calculo['faixa7'] = $calculo['faixa7'] + 1;
                    }
                    if ($dependente >= 49 && $dependente <= 53) {
                        $calculo['faixa8'] = $calculo['faixa8'] + 1;
                    }
                    if ($dependente >= 54 && $dependente <= 58) {
                        $calculo['faixa9'] = $calculo['faixa9'] + 1;
                    }
                    if ($dependente >= 59) {
                        $calculo['faixa10'] = $calculo['faixa10'] + 1;
                    }
                }
            }
        }
        $i = 1;
        $totalVidas = 0;
        for ($i = 1; $i <= 10; $i++) {
            $totalVidas = $totalVidas + (int) $calculo['faixa' . $i];
        }

        //ENTIDADES QUE ESTAO ASSOCIADAS A PROFISSAO
        $this->loadModel('PfEntidadesProfissoes');
        $entidadesProfissoes = $this->PfEntidadesProfissoes->find('all')->where(['PfEntidadesProfissoes.pf_profissao_id' => $calculo['pf_profissao_id']])->toArray();

        if (count($entidadesProfissoes) >= 1) {
            foreach ($entidadesProfissoes as $entidadesProfissoes) {
                $entidadesIds[] = $entidadesProfissoes['pf_entidade_id'];
            }

            //OPERADORAS ASSOCIADAS AS ENTIDADES
            $this->loadModel('PfEntidadesPfOperadoras');
            $entidadesOperadoras = $this->PfEntidadesPfOperadoras->find('all')->where(['PfEntidadesPfOperadoras.pf_entidade_id IN' => $entidadesIds])->toArray();
            foreach ($entidadesOperadoras as $entidadesOperadora) {
                $operadorasIds[] = $entidadesOperadora['pf_operadora_id'];
            }

            $conditions = array('OR' => [['PfTabelas.pf_operadora_id IN' => $operadorasIds], ['PfTabelas.modalidade' => 'INDIVIDUAL']]);
        } else {
            $conditions = array('OR' => [['PfTabelas.pf_operadora_id <>' => ''], ['PfTabelas.modalidade' => 'INDIVIDUAL']]);
        }

        $this->loadModel('PfTabelas');
        $pf_tabelas = $this->PfTabelas->find('all')
            ->contain([
                'PfComercializacoes',
                'Estados',
                'PfAtendimentos',
                'PfAcomodacoes',
                'PfProdutos' => [
                    'PfOperadoras' => [
                        'Imagens',
                        'PfCarencias',
                        'PfRedes',
                        'PfReembolsos',
                        'PfDocumentos',
                        'PfDependentes',
                        'PfObservacoes'
                    ],
                    'TiposProdutos'
                ]
            ])
            ->where([
                $conditions,
                'PfTabelas.validade' => 1,
                'PfTabelas.estado_id' => $calculo['estado_id'],
                'PfTabelas.minimo_vidas <=' => $totalVidas,
                'PfTabelas.maximo_vidas >=' => $totalVidas
            ])
            ->toArray();

        foreach ($pf_tabelas as $tabela) {
            switch ($tabela['coparticipacao']) {
                case 'n':
                    $coparticipacao = 'S/ COPARTICIPACAO';
                    break;
                case 's':
                    $coparticipacao = 'C/ COPARTICIPACAO - ' . $tabela['pf_tabela']['detalhe_coparticipacao'];
                    break;
            }
            $tabelasOrdenadas[$tabela['pf_produto']['pf_operadora']['id']][$tabela['pf_produto']['nome']][$tabela['nome'] . " - " . $tabela['pf_acomodaco']['nome'] . " - " . $coparticipacao] = $tabela;
        }

        if (isset($calculo['Dependentes'])) {
            $dependentes = $calculo['Dependentes'];
            unset($calculo['Dependentes']);
            unset($calculo['dependentes']);
        }


        $calculo['user_id'] = $userId;

        $pfCalculo = $this->PfCalculos->newEntity();
        $pfCalculo = $this->PfCalculos->patchEntity($pfCalculo, $calculo);


        if ($this->PfCalculos->save($pfCalculo)) {

            $this->loadModel('Users');
            $userLogado = $this->Users->get($userId);

            $userLogado->ultimocalculo = Time::now();
            $userLogado->dirty('ultimocalculo', true);
            $this->Users->save($userLogado);


            if (isset($dependentes)) {
                $this->loadModel('PfCalculosDependentes');
                foreach ($dependentes as $chave => $dependente) {
                    $dados['pf_calculo_id'] = $pfCalculo->id;
                    $dados['idade_dependente'] = $dependente;

                    $calculosDependentes = $this->PfCalculosDependentes->newEntity();
                    $calculosDependentes = $this->PfCalculosDependentes->patchEntity($calculosDependentes, $dados);
                    $this->PfCalculosDependentes->save($calculosDependentes);
                }
            }

            /*
              $this->loadModel('PfCalculosTabelas');
              foreach ($pf_tabelas as $idTabela) {
              $dadosCalculo['pf_calculo_id'] = $pfCalculo->id;

              $dadosCalculo['pf_tabela_id'] = $idTabela['id'];

              $calculosTabelas = $this->PfCalculosTabelas->newEntity();
              $calculosTabelas = $this->PfCalculosTabelas->patchEntity($calculosTabelas, $dadosCalculo);
              $this->PfCalculosTabelas->save($calculosTabelas);
              }
             */

            $dados['id'] = $pfCalculo->id;
            $dados['msg'] = 'Registro salvo com sucesso.';
            $dados['ok'] = true;
        } else {
            $dados['msg'] = 'Erro ao salvar. Tente novamente.';
            $dados['ok'] = false;
        }

        $this->autoRender = false;
        $this->response->type('json');
        $this->response->body(json_encode($dados));
    }

    public function cotar()
    {
        $data = $this->request->getData();
        $user_id = $this->request->getSession()->read('Auth.User.id');

        //Adicionar titular por faixa
        $this->auxPreencheFaixas($data);

        if (isset($calculo['Dependentes'])) {
            $dependentes = $calculo['Dependentes'];
            unset($calculo['Dependentes']);
            unset($calculo['dependentes']);
        }
        $data['user_id'] = $user_id;

        $pfCalculo = $this->PfCalculos->newEntity();
        $pfCalculo = $this->PfCalculos->patchEntity($pfCalculo, $data);
        if ($this->PfCalculos->save($pfCalculo)) {

            $this->loadModel('Users');
            $userLogado = $this->Users->get($user_id);

            $userLogado->ultimocalculo = Time::now();
            $userLogado->dirty('ultimocalculo', true);
            $this->Users->save($userLogado);

            if (isset($dependentes)) {
                $this->loadModel('PfCalculosDependentes');
                foreach ($dependentes as $chave => $dependente) {
                    $dados['pf_calculo_id'] = $pfCalculo->id;
                    $dados['idade_dependente'] = $dependente;

                    $calculosDependentes = $this->PfCalculosDependentes->newEntity();
                    $calculosDependentes = $this->PfCalculosDependentes->patchEntity($calculosDependentes, $dados);
                    $this->PfCalculosDependentes->save($calculosDependentes);
                }
            }

            $dados['id'] = $pfCalculo->id;
            $dados['msg'] = 'Registro salvo com sucesso.';
            $dados['ok'] = true;
        } else {
            $dados['msg'] = 'Erro ao salvar. Tente novamente.';
            $dados['ok'] = false;
        }

        $this->autoRender = false;
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($dados));
    }

    /**
     * Funçao Auxiliar - não renderiza 
     */
    public function auxPreencheFaixas(&$data)
    {
        // Faixas Padrão
        $data['faixa1'] = 0;
        $data['faixa2'] = 0;
        $data['faixa3'] = 0;
        $data['faixa4'] = 0;
        $data['faixa5'] = 0;
        $data['faixa6'] = 0;
        $data['faixa7'] = 0;
        $data['faixa8'] = 0;
        $data['faixa9'] = 0;
        // Faixas extras vitalmed
        $data['faixa_extra_1'] = 0;
        $data['faixa_extra_2'] = 0;
        $data['faixa_extra_3'] = 0;
        $data['faixa_extra_4'] = 0;
        $data['faixa_extra_5'] = 0;
        $data['faixa_extra_6'] = 0;
        // 
        $data['faixa_promo_1'] = 0;
        $data['faixa_promo_2'] = 0;
        $data['faixa_promo_3'] = 0;

        // Adiciona Titular nas faixas padrão
        switch ($data['idade_titular']) {
            case ($data['idade_titular'] <= 18 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa1'] = (int) $data['faixa1'] + 1;
                break;
            case ($data['idade_titular'] >= 19 && $data['idade_titular'] <= 23 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa2'] = (int) $data['faixa2'] + 1;
                break;
            case ($data['idade_titular'] >= 24 && $data['idade_titular'] <= 28 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa3'] = (int) $data['faixa3'] + 1;
                break;
            case ($data['idade_titular'] >= 29 && $data['idade_titular'] <= 33 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa4'] = (int) $data['faixa4'] + 1;
                break;
            case ($data['idade_titular'] >= 34 && $data['idade_titular'] <= 38 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa5'] = (int) $data['faixa5'] + 1;
                break;
            case ($data['idade_titular'] >= 39 && $data['idade_titular'] <= 43 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa6'] = (int) $data['faixa6'] + 1;
                break;
            case ($data['idade_titular'] >= 44 && $data['idade_titular'] <= 48 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa7'] = (int) $data['faixa7'] + 1;
                break;
            case ($data['idade_titular'] >= 49 && $data['idade_titular'] <= 53 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa8'] = (int) $data['faixa8'] + 1;
                break;
            case ($data['idade_titular'] >= 54 && $data['idade_titular'] <= 58 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa9'] = (int) $data['faixa9'] + 1;
                break;
            case ($data['idade_titular'] >= 59 ? $data['idade_titular'] : !$data['idade_titular']):
                $data['faixa10'] = (int) $data['faixa10'] + 1;
                break;
        }

        // Adiciona Titular nas faixas extras
        switch ($data['idade_titular']) {
            case ($data['idade_titular'] <= 14):
                $data['faixa_extra_1'] = $data['faixa_extra_1'] + 1;
                break;
            case ($data['idade_titular'] >= 15 && $data['idade_titular'] <= 29):
                $data['faixa_extra_2'] = $data['faixa_extra_2'] + 1;
                break;
            case ($data['idade_titular'] >= 30 && $data['idade_titular'] <= 39):
                $data['faixa_extra_3'] = $data['faixa_extra_3'] + 1;
                break;
            case ($data['idade_titular'] >= 40 && $data['idade_titular'] <= 49):
                $data['faixa_extra_4'] = $data['faixa_extra_4'] + 1;
                break;
            case ($data['idade_titular'] >= 50 && $data['idade_titular'] <= 58):
                $data['faixa_extra_5'] = $data['faixa_extra_5'] + 1;
                break;
            case ($data['idade_titular'] >= 59):
                $data['faixa_extra_6'] = $data['faixa_extra_6'] + 1;
                break;
        }

        // 18 - 39, 40 - 58, acima de 59
        switch ($data['idade_titular']) {
            case ($data['idade_titular'] >= 18 && $data['idade_titular'] <= 39):
                $data['faixa_promo_1'] = $data['faixa_promo_1'] + 1;
                break;
            case ($data['idade_titular'] >= 40 && $data['idade_titular'] <= 58):
                $data['faixa_promo_2'] = $data['faixa_promo_2'] + 1;
                break;
            case ($data['idade_titular'] >= 59):
                $data['faixa_promo_3'] = $data['faixa_promo_3'] + 1;
                break;
        }

        if (isset($data['Dependentes'])) {
            foreach ($data['Dependentes'] as $dependente) {
                if (isset($dependente) && $dependente != '') {

                    //Adiciona dependentes as faixas etárias
                    if ($dependente <= 18) {
                        $data['faixa1'] = $data['faixa1'] + 1;
                    }
                    if ($dependente >= 19 && $dependente <= 23) {
                        $data['faixa2'] = $data['faixa2'] + 1;
                    }
                    if ($dependente >= 24 && $dependente <= 28) {
                        $data['faixa3'] = $data['faixa3'] + 1;
                    }
                    if ($dependente >= 29 && $dependente <= 33) {
                        $data['faixa4'] = $data['faixa4'] + 1;
                    }
                    if ($dependente >= 34 && $dependente <= 38) {
                        $data['faixa5'] = $data['faixa5'] + 1;
                    }
                    if ($dependente >= 39 && $dependente <= 43) {
                        $data['faixa6'] = $data['faixa6'] + 1;
                    }
                    if ($dependente >= 44 && $dependente <= 48) {
                        $data['faixa7'] = $data['faixa7'] + 1;
                    }
                    if ($dependente >= 49 && $dependente <= 53) {
                        $data['faixa8'] = $data['faixa8'] + 1;
                    }
                    if ($dependente >= 54 && $dependente <= 58) {
                        $data['faixa9'] = $data['faixa9'] + 1;
                    }
                    if ($dependente >= 59) {
                        $data['faixa10'] = $data['faixa10'] + 1;
                    }

                    // Adiciona dependentes as faixas extras
                    if ($dependente <= 14) {
                        $data['faixa_extra_1'] = $data['faixa_extra_1'] + 1;
                    }
                    if ($dependente >= 15 && $dependente <= 29) {
                        $data['faixa_extra_2'] = $data['faixa_extra_2'] + 1;
                    }
                    if ($dependente >= 30 && $dependente <= 39) {
                        $data['faixa_extra_3'] = $data['faixa_extra_3'] + 1;
                    }
                    if ($dependente >= 40 && $dependente <= 49) {
                        $data['faixa_extra_4'] = $data['faixa_extra_4'] + 1;
                    }
                    if ($dependente >= 50 && $dependente <= 58) {
                        $data['faixa_extra_5'] = $data['faixa_extra_5'] + 1;
                    }
                    if ($dependente >= 59) {
                        $data['faixa_extra_6'] = $data['faixa_extra_6'] + 1;
                    }

                    // Queima
                    if ($dependente >= 18 && $dependente <= 39) {
                        $data['faixa_promo_1'] = $data['faixa_promo_1'] + 1;
                    }
                    if ($dependente >= 40 && $dependente <= 58) {
                        $data['faixa_promo_2'] = $data['faixa_promo_2'] + 1;
                    }
                    if ($dependente >= 59) {
                        $data['faixa_promo_3'] = $data['faixa_promo_3'] + 1;
                    }
                }
            }
        }
    }


    /**
     *
     *
     *  Salva filtros do PDF incluind tabelas selecionadas e Gera PDF
     *
     *
     */
    public function pdf()
    {
        //debug($this->request->data);die();
        $id = $this->request->data['simulacao_id'];
        $tabelasSelecionadas = $this->request->data;

        //SALVAR OS FILTROS CAS ESTEJAM SELECIONADOS
        if (!empty($tabelasSelecionadas["pf_atendimento_id"]) || !empty($tabelasSelecionadas["pf_acomodacao_id"]) || !empty($tabelasSelecionadas["tipo_produto_id"]) || !empty($tabelasSelecionadas["coparticipacao"]) || !empty($tabelasSelecionadas["filtroreembolso"])) {
            $this->loadModel("PfPdffiltros");

            $dadosFiltros["pf_calculo_id"] = $tabelasSelecionadas["simulacao_id"];
            $dadosFiltros["pf_atendimento_id"] = $tabelasSelecionadas["pf_atendimento_id"];
            $dadosFiltros["pf_acomodacao_id"] = $tabelasSelecionadas["pf_acomodacao_id"];
            $dadosFiltros["tipo_produto_id"] = $tabelasSelecionadas["tipo_produto_id"];
            $dadosFiltros["coparticipacao"] = $tabelasSelecionadas["coparticipacao"];
            $dadosFiltros["filtroreembolso"] = $tabelasSelecionadas["filtroreembolso"];

            $entidadeFiltros = $this->PfPdffiltros->newEntity();
            $entidadeFiltros = $this->PfPdffiltros->patchEntity($entidadeFiltros, $dadosFiltros);
            $this->PfPdffiltros->save($entidadeFiltros);
        }
        //         die();

        foreach ($tabelasSelecionadas as $chave => $tabela) {
            if (
                $chave <> 'coparticipacao' &&
                $chave <> 'tipo_produto_id' &&
                $chave <> 'pf_acomodacao_id' &&
                $chave <> 'pf_atendimento_id' &&
                $chave <> 'simulacao_id' &&
                $chave <> 'observacao' &&
                $chave <> 'rede' &&
                $chave <> 'reembolso' &&
                $chave <> 'carencia' &&
                $chave <> 'documento'
            ) {
                if (substr($chave, 0, 8) === 'checkAll') {
                    unset($tabelasSelecionadas[$chave]);
                } else {
                    if ($tabela == 0) {
                        unset($tabelasSelecionadas[$chave]);
                    } else {
                        $tabelasFiltradas[] = $tabela;
                        $findTabelas[] = $tabela;
                    }
                }
            }
        }

        $tabelasSelecionadas['pf_calculo_id'] = $this->request->data['simulacao_id'];
        if (!empty($tabelasFiltradas)) {


            //* SALVAR FILTROS E TABELAS */
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $userId = $sessao['id'];
            $tabelasSelecionadas['user_id'] = $userId;

            $this->loadModel('PfFiltros');
            $filtro = $this->PfFiltros->newEntity();
            $filtro = $this->PfFiltros->patchEntity($filtro, $tabelasSelecionadas);
            if ($this->PfFiltros->save($filtro)) {
                $this->loadModel('PfFiltrosTabelas');
                foreach ($tabelasFiltradas as $chave => $tabelas_filtradas) {
                    $dadosTabelasFiltradas['pf_filtro_id'] = $filtro->id;
                    $dadosTabelasFiltradas['pf_tabela_id'] = $tabelas_filtradas;


                    $tabelasfiltradas = $this->PfFiltrosTabelas->newEntity();
                    $tabelasfiltradas = $this->PfFiltrosTabelas->patchEntity($tabelasfiltradas, $dadosTabelasFiltradas);
                    $this->PfFiltrosTabelas->save($tabelasfiltradas);
                }
            }

            $simulacao = $this->PfCalculos->get($id);

            $this->loadModel('PfTabelas');
            $tabelas = $this->PfTabelas->find('all', [
                'contain' => [
                    'Estados',
                    'PfAtendimentos',
                    'PfAcomodacoes',
                    'PfProdutos' => [
                        'PfOperadoras' => [
                            'Imagens',
                            'PfFormasPagamentos',
                            'PfCarencias',
                            'PfRedes',
                            'PfReembolsos',
                            'PfDocumentos',
                            'PfDependentes',
                            'PfComercializacoes',
                            'PfObservacoes'
                        ],
                        'TiposProdutos'
                    ]
                ]
            ])
                ->order([
                    'PfOperadoras.prioridade' => 'ASC',
                    'PfTabelas.prioridade' => 'ASC'
                ])
                ->where([
                    'PfTabelas.id IN' => $findTabelas
                ])->toArray();
            $operadorasIds = array();
            foreach ($tabelas as $tabela) {
                $operadorasIds[$tabela["pf_operadora_id"]] = $tabela["pf_operadora_id"];

                if ($tabela['coparticipacao'] == 'n') {
                    $coparticipacao = 'S/COPARTICIPAÇÃO';
                } else {
                    $coparticipacao = 'C/COPARTICIPAÇÃO';
                }
                $tabelasOrdenadas[$tabela['pf_produto']['pf_operadora']['id']][$tabela['pf_produto']['nome']][$tabela['nome'] . " - " . $tabela['pf_acomodaco']['nome'] . " - " . $coparticipacao] = $tabela;
            }

            $simulacao['observaco'] = $this->request->data['observacao'];
            $simulacao['rede'] = $this->request->data['rede'];
            $simulacao['reembolso'] = $this->request->data['reembolso'];
            $simulacao['carencia'] = $this->request->data['carencia'];
            $simulacao['documento'] = $this->request->data['documento'];

            //NÃO UTILIZADOS
            //$simulacao['info'] = 1;
            //$simulacao['opcionais'] = 1;

            $this->loadModel('PfOperadoras');
            $op = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->toArray();
            foreach ($op as $o) {
                $operadoras[$o['id']] = $o;
            }


            //ENCONTRAR ENTIDADES QUE POSSUEM ASSOCIAÇÃO COM A PROFISSAO DO CALCULO
            $this->loadModel("PfEntidadesPfOperadoras");
            $arvore = $this->PfEntidadesPfOperadoras->find("all")
                ->where(["pf_operadora_id IN" => $operadorasIds])
                ->contain(["PfEntidades" => ["PfEntidadesProfissoes" => ["PfProfissoes"]]])
                ->toArray();

            foreach ($arvore as $dados) {
                foreach ($dados["pf_entidade"]["pf_entidades_profissoes"] as $info) {
                    $entidadesOrdenadas[$dados["pf_operadora_id"]][$dados["pf_entidade"]["nome"]][] = $info["pf_profisso"]["nome"];
                }
            }


            $session = $this->request->session();
            $sessao = $session->read('Auth.User');


            //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
            $tipoPDF = $this->request->data("tipopdf");
            switch ($tipoPDF) {
                case "D":
                    $nomePDF = 'Calculo-Saude-PF-' . $simulacao['id'] . '.pdf';
                    break;
                case "I":
                    $nomePDF = 'Calculo-Saude-PF-' . $simulacao['id'] . '.pdf';
                    break;
                case 'F':
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PF-' . $simulacao['id'] . '.pdf';
                    break;
                default:
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PF-' . $simulacao['id'] . '.pdf';
                    break;
            }



            $this->set(compact('tabelas', 'entidadesOrdenadas', 'simulacao', 'tabelasOrdenadas', 'operadoras', 'sessao', 'nomePDF', 'tipoPDF'));
            $this->viewBuilder()->layout('pdf_pf');
        } else {
            $this->Flash->error(__('Selecione os cálculos que deseja imprimir.'));
            return $this->redirect(['action' => 'view/' . $this->request->data['simulacao_id']]);
        }
    }

    public function email($simulacao = null, $emailCliente = null)
    {

        $dados = $this->PfCalculos->get($simulacao);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (isset($emailCliente) && $emailCliente <> '') {
            $destinatario = $emailCliente;
        } else {
            $destinatario = $sessao["email"];
        }

        $imagem = rawurlencode($session->read('Auth.User.imagem.nome'));
        $caminho = "uploads/imagens/usuarios/logos/";
        $imagem = $caminho . $imagem;
        if (empty($imagem)) {
            $imagem = 'img/logoCpEmail.png';
        }

        $email = new Email('default');

        $email->transport('calculos');
        $email->from('nao-responda@corretorparceiro.com.br', 'Tabela de Preços - Cálculos')
            ->to($destinatario)
            ->sender('nao-responda@corretorparceiro.com.br', 'Tabela de Preços - Cálculos')
            ->replyTo('nao-responda@corretorparceiro.com.br')
            ->emailFormat('html')
            ->attachments([
                'Calculo-PF-' . $dados['id'] . '.pdf' => [
                    'file' => $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PF-' . $dados['id'] . '.pdf'
                ]
            ])
            ->subject('Cálculo PF Nº ' . $dados['id'] . ' - Cliente: ' . $dados['nome'] . ' - Plataforma Digital Corretor Parceiro')
            ->send('<html><img src="https://corretorparceiro.com.br/app/' . $imagem . '" width="230"/>'
                . '<br/>'
                . '<br/>'
                . 'À <b>' . $dados['nome'] . '</b>, A/C: <b>' . $dados['contato'] . '</b>.<br/>
        Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.<br/>
        Informamos que os valores, regras de comercialização e condições contratuais são determinadas pelas seguradoras/operadoras dos planos e podem ser alterados pelas mesmas a qualquer momento.<br/>
        Os preços são tabelados e estão sujeitos a confirmação dos valores no ato do fechamento do contrato.<br/>'
                . '<br/>'
                . '<br/>'
                . 'Atenciosamente, ' . $sessao['nome'] . ' ' . $sessao['sobrenome']
                . '<br/> <b>Email:</b> ' . $sessao['email']
                . '<br/> <b>Celular:</b> ' . $sessao['celular']
                . '<br/> <b>WhatsApp:</b> ' . $sessao['whatsapp']
                . '</html>');

        unlink($_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PF-' . $dados['id'] . '.pdf');

        $resposta = 'Cálculo enviado para o e-mail: ' . $destinatario;

        $this->set(compact('resposta'));
        //        return $this->redirect(['action' => 'view/'. $simulacao]);
    }

    /* Metodo para listar pedf gerados a partir do Cálculo */

    public function listarpdfs($simulacao_id = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        //        $userId = $sessao['id'];

        $simulacao = $this->PfCalculos->get($simulacao_id)->toArray();
        $this->loadModel('PfFiltros');
        $filtros = $this->PfFiltros->find('all')->where(['PfFiltros.pf_calculo_id' => $simulacao_id])->order(['created' => 'DESC'])->toArray();
        //        debug($filtros);

        $this->set(compact('filtros', 'simulacao'));
    }

    /**
     *
     *
     *  Salva filtros do PDF incluind tabelas selecionadas e Gera PDF
     *
     *
     */
    public function reenviarpdf($filtro = null)
    {

        $this->loadModel('PfFiltros');
        $filtro = $this->PfFiltros->get($filtro, [
            'contain' => ['PfFiltrosTabelas' => ['PfTabelas']]
        ])->toArray();

        foreach ($filtro['pf_filtros_tabelas'] as $tab) {
            $tabelasIds[] = $tab['pf_tabela_id'];
        }

        $this->loadModel('PfTabelas');

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $userId = $sessao['id'];

        $simulacao = $this->PfCalculos->get($filtro['pf_calculo_id']);
        //        debug($filtro);
        //        debug($simulacao);
        //        die();

        $this->loadModel('PfTabelas');
        $tabelas = $this->PfTabelas->find('all', [
            'contain' => [
                'PfComercializacoes', 'Estados', 'PfAtendimentos', 'PfAcomodacoes', 'PfProdutos' => [
                    'PfOperadoras' => ['Imagens', 'PfCarencias', 'PfRedes', 'PfReembolsos', 'PfDocumentos', 'PfDependentes', 'PfObservacoes'], 'TiposProdutos'
                ]
            ]
        ], [
            'order' => ['PfOperadoras.nome' => 'ASC']
        ])
            ->where([
                'PfTabelas.id IN' => $tabelasIds
            ])->toArray();

        foreach ($tabelas as $tabela) {
            if ($tabela['coparticipacao'] == 'n') {
                $coparticipacao = 'S/COPARTICIPAÇÃO';
            } else {
                $coparticipacao = 'C/COPARTICIPAÇÃO';
            }
            $tabelasOrdenadas[$tabela['pf_produto']['pf_operadora']['id']][$tabela['pf_produto']['nome']][$tabela['nome'] . " - " . $tabela['pf_acomodaco']['nome'] . " - " . $coparticipacao] = $tabela;
        }


        $simulacao['observaco'] = $filtro['observacao'];
        $simulacao['rede'] = $filtro['rede'];
        $simulacao['reembolso'] = $filtro['reembolso'];
        $simulacao['carencia'] = $filtro['carencia'];
        $simulacao['documento'] = $filtro['documento'];

        //NÃO UTILIZADOS
        //            $simulacao['info'] = 1;
        //            $simulacao['opcionais'] = 1;

        $this->loadModel('PfOperadoras');
        $op = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->toArray();
        foreach ($op as $o) {
            $operadoras[$o['id']] = $o;
        }


        //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
        $tipoPDF = 'F';
        //Nome do PDF
        $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-PF-' . $simulacao['id'] . '.pdf';

        $this->set('simulacao', $simulacao);
        $this->set('_serialize', ['simulacao']);
        $this->set(compact('tabelas', 'simulacao', 'tabelasOrdenadas', 'operadoras', 'sessao', 'nomePDF', 'tipoPDF'));
        $this->viewBuilder()->layout('pdf_pf');
        $this->render('pdf');
        //
    }

    /**
     * Método para filtrar tabelasdentro de um cálculo já gerado
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtropdf($filtro = null)
    {

        $this->loadModel('PfFiltros');
        $filtro = $this->PfFiltros->get($filtro, [
            'contain' => ['PfFiltrosTabelas' => ['PfTabelas' => ['PfProdutos' => ['PfOperadoras' => ['Imagens', 'PfCarencias', 'PfRedes', 'PfReembolsos', 'PfDocumentos', 'PfDependentes', 'PfObservacoes'], 'TiposProdutos']]]]
        ])->toArray();


        foreach ($filtro['pf_filtros_tabelas'] as $tab) {
            $tabelasIds[] = $tab['pf_tabela_id'];
        }

        $this->loadModel('PfCalculosTabelas');
        $simulacoesTabelas = $this->PfCalculosTabelas->find('all')
            ->where(['PfCalculosTabelas.pf_calculo_id' => $filtro['pf_calculo_id']])->toArray();
        $simulacao = $this->PfCalculos->get($filtro['pf_calculo_id']);


        $this->loadModel('PfTabelas');
        $tabelas = $this->PfTabelas->find('all', [
            'contain' => [
                'PfComercializacoes', 'Estados', 'PfAtendimentos', 'PfAcomodacoes', 'PfProdutos' => [
                    'PfOperadoras' => ['Imagens', 'PfCarencias', 'PfRedes', 'PfReembolsos', 'PfDocumentos', 'PfDependentes', 'PfObservacoes'], 'TiposProdutos'
                ]
            ]
        ])
            ->order([
                'PfOperadoras.prioridade' => 'ASC',
                'PfTabelas.prioridade' => 'ASC'
            ])
            ->where([
                'PfTabelas.id IN' => $tabelasIds,
            ])->toArray();
        $totalTabelas = count($tabelas);
        $totalsemfiltro = count($simulacoesTabelas);
        foreach ($tabelas as $tabela) {
            if ($tabela['coparticipacao'] == 'n') {
                $coparticipacao = 'S/COPARTICIPAÇÃO';
            } else {
                $coparticipacao = 'C/COPARTICIPAÇÃO';
            }
            $tabelasOrdenadas[$tabela['pf_produto']['pf_operadora']['id']][$tabela['pf_produto']['nome']][$tabela['nome'] . " - " . $tabela['pf_acomodaco']['nome'] . " - " . $coparticipacao] = $tabela;
        }
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('Tipos');
        $tipos = $this->Tipos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('Abrangencias');
        $abrangencias = $this->Abrangencias->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('PfOperadoras');
        $operadoras = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->toArray();

        $this->loadModel('TiposProdutos');
        $tipos_produtos = $this->TiposProdutos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        //        $simulacao = $this->Simulacoes->get($filtro['simulacao_id']);
        $btnCancelar = ['action' => 'index'];

        $this->set(compact('simulacao', 'filtro', 'tipos_produtos', 'tabelasOrdenadas', 'abrangencias', 'tipos', 'btnCancelar', 'operadoras', 'totalTabelas', 'totalsemfiltro'));
        //        $this->render('filtro');
    }

    /**
     * Método para filtrar tabelas dentro de um cálculo já gerado
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($filtro = null)
    {
        $data = $this->request->data;
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        foreach ($data as $chave => $valor) {
            if (ctype_digit($chave)) {
                $ids[$chave] = $chave;
            }
        }

        $conditions = array();
        $this->loadModel('PfTabelas');

        if (isset($data['pf_atendimento_id']) && !empty($data['pf_atendimento_id']) && $data['pf_atendimento_id'] <> 21) {
            $array = array("PfTabelas.pf_atendimento_id" => $data['pf_atendimento_id']);
            array_push($conditions, $array);
        }

        if (isset($data['pf_acomodacao_id']) && !empty($data['pf_acomodacao_id']) && $data['pf_acomodacao_id'] <> 4) {
            $array = array("PfTabelas.pf_acomodacao_id" => $data['pf_acomodacao_id']);
            array_push($conditions, $array);
        }
        if (isset($data['pf_cobertura_id']) && !empty($data['pf_cobertura_id']) && $data['pf_cobertura_id'] <> 99) {
            $this->loadModel('PfCoberturas');
            $array = array("PfTabelas.pf_cobertura_id" => $data['pf_cobertura_id']);
            array_push($conditions, $array);
        }
        if (isset($data['abrangencia_id']) && !empty($data['abrangencia_id']) && $data['abrangencia_id'] <> 6) {
            $array = array('PfTabelas.pf_atendimento_id' => $data['abrangencia_id']);
            array_push($conditions, $array);
        }

        if ($data['filtroreembolso'] === '99') {
            $data['filtroreembolso'] = '';
        }
        if (isset($data['filtroreembolso']) && !empty($data['filtroreembolso'])) {
            $array = array('PfTabelas.reembolso' => $data['filtroreembolso']);
            array_push($conditions, $array);
        }

        if ($data['coparticipacao'] === '99') {
            $data['coparticipacao'] = '';
        }
        if (isset($data['coparticipacao']) && !empty($data['coparticipacao'])) {
            $s = $data['coparticipacao'];
            $array = array("PfTabelas.coparticipacao" => "$s");
            array_push($conditions, $array);
        }

        $this->loadModel('PfOperadoras');
        $operadoras = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->where(['NOT(PfOperadoras.status <=> \'INATIVA\')', 'nova' => 1])->toArray();
        $cotacao = $this->PfCalculos->find("all")->contain(["PfCalculosDependentes"])->where(["PfCalculos.id" => $data['simulacao_id']])->first();
        $simulacao = $cotacao;

        $this->loadModel("PfPdffiltros");
        $filtro = $this->PfPdffiltros->find("all")->where(["pf_calculo_id" => $data['simulacao_id']])->order(["created" => "DESC"])->first();

        if ($simulacao['user_id'] == $sessao['id'] || $sessao['role'] == 'admin') {

            $totalVidas = 0;
            for ($i = 1; $i <= 10; $i++) {
                $totalVidas = $totalVidas + (int) $simulacao['faixa' . $i];
            }
            $total_vidas = $totalVidas;

            //ENTIDADES QUE ESTAO ASSOCIADAS A PROFISSAO
            $this->loadModel('PfEntidadesProfissoes');

            //O ID 138 CORRESPONDE A PROFISSÃO "TODAS AS PROFISSÕES"
            if ($cotacao['pf_profissao_id'] == 138) {
                $entidades = $this->PfEntidadesProfissoes->find('all')
                    ->group('pf_entidade_id')
                    ->extract("pf_entidade_id")
                    ->toArray();
            } else {
                $entidades = $this->PfEntidadesProfissoes->find('all')
                    ->where(['PfEntidadesProfissoes.pf_profissao_id' => $cotacao['pf_profissao_id']])
                    ->group('pf_entidade_id')
                    ->extract("pf_entidade_id")
                    ->toArray();
            }

            if (count($entidades) >= 1) {

                $this->loadModel('PfEntidadesTabelas');
                $tabelas_ids = $this->PfEntidadesTabelas->find('list', ['valueField' => 'pf_tabela_id'])->where(['pf_entidade_id IN' => $entidades])->group('pf_tabela_id')->toArray();
                $entidades = [
                    'OR' => [
                        ['PfTabelas.id IN' => $tabelas_ids],
                        ['PfTabelas.modalidade' => 'INDIVIDUAL']
                    ]
                ];
            } else {
                $entidades = [
                    'PfTabelas.modalidade' => 'INDIVIDUAL'
                ];
            }

            $this->loadModel('PfTabelas');
            // ini_set('memory_limit', '-1');
            // set_time_limit(0);
            $tabelas_municipios = $this->PfTabelas->find('all')
                ->matching('PfAreasComercializacoes.Municipios', function ($q) use ($cotacao, $data) {
                    if (isset($data['municipios']) && !empty($data['municipios'])) {
                        return $q->where(['Municipios.id' => $data['municipios']]);
                    } else {
                        return $q->where(['Municipios.estado_id' => $cotacao['estado_id']]);
                    }
                })
                ->contain([
                    'PfAtendimentos',
                    'PfAcomodacoes',
                    'PfCoberturas',
                    'PfEntidades' => [
                        'PfProfissoes' =>  function ($q) {
                            return $q->order('nome');
                        }
                    ],
                    'PfAreasComercializacoes' => [
                        'Municipios' => ['Estados'],
                        'PfOperadoras' => [
                            'Imagens',
                            'PfFormasPagamentos',
                            'PfCarencias',
                            'PfRedes',
                            'PfReembolsos',
                            'PfDocumentos',
                            'PfDependentes',
                            'PfComercializacoes',
                            'PfObservacoes'
                        ]
                    ]
                ])
                ->where([
                    'PfTabelas.new' => 1,
                    'PfTabelas.validade' => 1,
                    'PfTabelas.minimo_vidas <=' => $total_vidas,
                    'PfTabelas.maximo_vidas >=' => $total_vidas,
                    $entidades,
                    $conditions
                ])
                ->group('PfTabelas.id')->toArray();

            $tabelas_metropoles = $this->PfTabelas->find('all')
                ->matching('PfAreasComercializacoes.Metropoles.Municipios', function ($q) use ($cotacao) {
                    if (isset($data['municipios']) && !empty($data['municipios'])) {
                        return $q->where(['Municipios.id' => $data['municipios']]);
                    } else {
                        return $q->where(['Municipios.estado_id' => $cotacao['estado_id']]);
                    }
                })
                ->contain([
                    'PfAtendimentos',
                    'PfAcomodacoes',
                    'PfCoberturas',
                    'PfFormasPagamentos',
                    'PfCarencias',
                    'PfRedes',
                    'PfReembolsos',
                    'PfDocumentos',
                    'PfDependentes',
                    'PfComercializacoes',
                    'PfObservacoes',
                    'PfAreasComercializacoes' => [
                        'Metropoles',
                        'Municipios' => ['Estados'],
                        'PfOperadoras' => [
                            'Imagens',
                        ]
                    ],
                    'PfEntidades' => [
                        'PfProfissoes' =>  function ($q) {
                            return $q->order('nome');
                        }
                    ],
                ])
                ->where([
                    'PfTabelas.new' => 1,
                    'PfTabelas.validade' => 1,
                    'PfTabelas.minimo_vidas <=' => $total_vidas,
                    'PfTabelas.maximo_vidas >=' => $total_vidas,
                    $entidades,
                    $conditions
                ])
                ->group('PfTabelas.id')
                ->toArray();

            $tabelas = array_merge($tabelas_municipios, $tabelas_metropoles);
            $total_tabelas = count($tabelas);
            $precos = $tabelas;

            array_multisort(array_column($precos, 'faixa1'), SORT_ASC, SORT_NUMERIC, $precos);
            foreach ($precos as $a) {

                if (
                    $a['faixa1'] +
                    $a['faixa2'] +
                    $a['faixa3'] +
                    $a['faixa4'] +
                    $a['faixa5'] +
                    $a['faixa6'] +
                    $a['faixa7'] +
                    $a['faixa8'] +
                    $a['faixa9'] +
                    $a['faixa10'] +
                    $a['faixa11'] +
                    $a['faixa12'] > 0
                ) {
                    $menor_preco = $a;
                    break;
                }
            }

            $intermediario = $precos[count($precos) / 2];
            $maior_preco = end($precos);

            $tabelas_ordenadas = [];
            $area_adicionada = [];
            foreach ($tabelas as $tabela) {

                $operadora_id = $tabela['pf_areas_comercializaco']['pf_operadora']['id'];

                @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['pf_areas_comercializaco']['pf_operadora'];

                @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['pf_atendimento']['nome']] .=  $tabela['nome'] . ' • ';

                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                if (!in_array($tabela['pf_areas_comercializaco']['id'], $area_adicionada)) {
                    if (!empty($tabela['pf_areas_comercializaco']['metropoles'])) {
                        foreach ($tabela['pf_areas_comercializaco']['metropoles'] as $metropole) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                        }
                    }
                    if (!empty($tabela['pf_areas_comercializaco']['municipios'])) {
                        foreach ($tabela['pf_areas_comercializaco']['municipios'] as $municipio) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                        }
                    }
                    $area_adicionada[] = $tabela['pf_areas_comercializaco']['id'];
                }
                unset($tabela['pf_areas_comercializaco']);

                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['data'] =  $tabela['pf_observaco']['descricao'];
                unset($tabela['pf_observaco']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['data'] =  $tabela['pf_rede']['descricao'];
                unset($tabela['pf_rede']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  $tabela['reembolsoEntity']['descricao'];
                unset($tabela['pf_reembolsoEntity']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['data'] =  $tabela['pf_carencia']['descricao'];
                unset($tabela['pf_carencia']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['data'] =  $tabela['pf_dependente']['descricao'];
                unset($tabela['pf_dependentes']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['data'] =  $tabela['pf_documento']['descricao'];
                unset($tabela['pf_documentos']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['data'] =  $tabela['pf_formas_pagamento']['descricao'];
                unset($tabela['pf_formas_pagamento']);

                $tabela['cobertura'] = $tabela['pf_cobertura'];
                $tabela['acomodacao'] = $tabela['pf_acomodaco'];

                @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
            }

            $this->loadModel('PfOperadoras');
            $operadoras = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->where(['NOT(PfOperadoras.status <=> \'INATIVA\')', 'nova' => 1])->toArray();

            // DADOS PARA FILTRO
            $simulacao = $cotacao; //ALIAS ELEMENT FILTRO
            $totalTabelas = $total_tabelas; //ALIAS ELEMENT FILTRO
            $this->loadModel('PfAcomodacoes');
            $pfAcomodacoes = $this->PfAcomodacoes->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

            $this->loadModel('PfAtendimentos');
            $pfAtendimentos = $this->PfAtendimentos->find('list', ['valueField' => 'nome', 'order' => ['prioridade' => 'ASC']])->toArray();

            $this->loadModel('PfCoberturas');
            $pfCoberturas = $this->PfCoberturas->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

            $simulacao['rede'] = 1;
            $simulacao['carencia'] = 1;
            $simulacao['reembolso'] = 1;
            $simulacao['info'] = 1;
            $simulacao['opcionais'] = 1;
            $simulacao['observaco'] = 1;

            $this->set(compact('cotacao', 'simulacao', 'tabelas_ordenadas', 'total_tabelas', 'totalTabelas', 'menor_preco', 'intermediario', 'maior_preco', 'total_vidas', 'operadoras', 'pfAcomodacoes', 'pfAtendimentos', 'pfCoberturas'));
        } else {
            $this->Flash->error(__('Sem permissão de visualizar este Cálculo. Cálculo pertence a outro usuário.'));
            return $this->redirect(['action' => 'index']);
        }



        $this->loadModel('PfProfissoes');
        $profissoes = $this->PfProfissoes->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('PfAcomodacoes');
        $pfAcomodacoes = $this->PfAcomodacoes->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('PfAtendimentos');
        $pfAtendimentos = $this->PfAtendimentos->find('list', ['valueField' => 'nome', 'order' => ['prioridade' => 'ASC']])->toArray();

        $this->loadModel('PfOperadoras');
        $operadoras = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->where('NOT(PfOperadoras.status <=> \'INATIVA\')')->toArray();

        $this->loadModel('PfCoberturas');
        $pfCoberturas = $this->PfCoberturas->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);

        $simulacao = $this->PfCalculos->get($data['simulacao_id']);
        $btnCancelar = ['action' => 'index'];



        // $this->loadModel('PfEntidadesPfOperadoras');
        // $entidadesOperadoras = $this->PfEntidadesPfOperadoras->find('all')
        //     ->where(['pf_operadora_id IN' => $sessao["pf_operadoras"]])
        //     ->contain(['PfEntidades'])
        //     ->toArray();


        // if (count($entidadesOperadoras) > 0) {
        //     $this->loadModel('PfEntidadesProfissoes');
        //     foreach ($entidadesOperadoras as $entidadesOperadora) {
        //         $entidadesIds[] = $entidadesOperadora['pf_entidade_id'];
        //     }
        //     $profissoesPf = $this->PfEntidadesProfissoes->find('all')
        //         ->where(['PfEntidadesProfissoes.pf_entidade_id IN' => $entidadesIds])
        //         ->contain(['PfProfissoes', 'PfEntidades'])->toArray();

        //     foreach ($entidadesOperadoras as $ent) {
        //         foreach ($profissoesPf as $p) {
        //             if ($ent['pf_entidade_id'] == $p['pf_entidade_id']) {

        //                 $ordenada[$ent['pf_operadora_id']][$ent['pf_entidade']['nome']][$p['pf_profisso']['nome']] = $p;
        //             }
        //         }
        //     }
        // }



        $this->set(compact("ordenada", "maisbarato", 'intermediario', "maiscaro", 'profissoes', 'simulacao', 'btnCancelar', 'pfCoberturas', 'pfAtendimentos', 'totalsemfiltro', 'pfAcomodacoes', 'tipos_produtos', 'tabelasOrdenadas', 'abrangencias', 'tipos', 'btnCancelar', 'operadoras', 'tabelas', 'totalTabelas'));
    }

    /* Metodo para listar pedf gerados a partir do Cálculo */

    public function alterarStatus($pfcalculo_id = null, $status_id = null)
    {
        $pfcalculo = $this->PfCalculos->get($pfcalculo_id);

        $pfcalculo->status_id = $status_id;
        $this->loadModel("Alertas");

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->Alertas->criarAlerta($sessao["id"], "SPF", $pfcalculo_id, $status_id);

        $this->PfCalculos->save($pfcalculo);
    }

    public function salvaFiltros(int $new = 0)
    {
        $id = $this->request->data['simulacao_id'];
        $tabelasSelecionadas = $this->request->data;

        //SALVAR OS FILTROS DOS PDF CASOS ESTEJAM SELECIONADOS
        if (!empty($tabelasSelecionadas["pf_atendimento_id"]) || !empty($tabelasSelecionadas["pf_acomodacao_id"]) || !empty($tabelasSelecionadas["tipo_produto_id"]) || !empty($tabelasSelecionadas["coparticipacao"]) || !empty($tabelasSelecionadas["filtroreembolso"])) {
            $this->loadModel("PfPdffiltros");

            $dadosFiltros["pf_calculo_id"] = $tabelasSelecionadas["simulacao_id"];
            $dadosFiltros["pf_atendimento_id"] = $tabelasSelecionadas["pf_atendimento_id"];
            $dadosFiltros["pf_acomodacao_id"] = $tabelasSelecionadas["pf_acomodacao_id"];
            $dadosFiltros["tipo_produto_id"] = $tabelasSelecionadas["tipo_produto_id"];
            $dadosFiltros["coparticipacao"] = $tabelasSelecionadas["coparticipacao"];
            $dadosFiltros["filtroreembolso"] = $tabelasSelecionadas["filtroreembolso"];

            $entidadeFiltros = $this->PfPdffiltros->newEntity();
            $entidadeFiltros = $this->PfPdffiltros->patchEntity($entidadeFiltros, $dadosFiltros);
            $pdf_id = $this->PfPdffiltros->save($entidadeFiltros);
            $pdf_id = $pdf_id['id'];
        }

        //SALVA TABELAS EM UM ARRAY
        foreach ($tabelasSelecionadas as $chave => $tabela) {
            if (
                $chave <> 'coparticipacao' &&
                $chave <> 'tipo_produto_id' &&
                $chave <> 'pf_acomodacao_id' &&
                $chave <> 'pf_atendimento_id' &&
                $chave <> 'simulacao_id' &&
                $chave <> 'observacao' &&
                $chave <> 'rede' &&
                $chave <> 'reembolso' &&
                $chave <> 'carencia' &&
                $chave <> 'documento'
            ) {
                if (substr($chave, 0, 8) === 'checkAll') {
                    unset($tabelasSelecionadas[$chave]);
                } else {
                    if ($tabela == 0) {
                        unset($tabelasSelecionadas[$chave]);
                    } else {
                        $tabelasFiltradas[] = $tabela;
                        $findTabelas[] = $tabela;
                    }
                }
            }
        }

        //SALVA OS FILTROS
        $tabelasSelecionadas['pf_calculo_id'] = $id;
        if (!empty($tabelasFiltradas)) {

            //* SALVAR FILTROS E TABELAS */
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $userId = $sessao['id'];
            $tabelasSelecionadas['user_id'] = $userId;
            if (isset($pdf_id)) {
                $tabelasSelecionadas['filtro_pdf_id'] = $pdf_id;
            }

            $this->loadModel('PfFiltros');
            $filtro = $this->PfFiltros->newEntity();
            $filtro = $this->PfFiltros->patchEntity($filtro, $tabelasSelecionadas);
            $idFiltro = $this->PfFiltros->save($filtro);
            if ($idFiltro) {
                $this->loadModel('PfFiltrosTabelas');
                foreach ($tabelasFiltradas as $chave => $tabelas_filtradas) {
                    $dadosTabelasFiltradas['pf_filtro_id'] = $filtro->id;
                    $dadosTabelasFiltradas['pf_tabela_id'] = $tabelas_filtradas;


                    $tabelasfiltradas = $this->PfFiltrosTabelas->newEntity();
                    $tabelasfiltradas = $this->PfFiltrosTabelas->patchEntity($tabelasfiltradas, $dadosTabelasFiltradas);
                    $this->PfFiltrosTabelas->save($tabelasfiltradas);
                }
            }
            if ($new) {
                return $this->redirect(['action' => 'newCotacao/', $idFiltro['id']]);
            } else {
                return $this->redirect(['action' => 'cotacao/', $idFiltro['id']]);
            }
        } else {
            $error = "erro";
            $this->request->session()->write('erro', 'S');
            if ($new) {
                return $this->redirect(['action' => 'visualizarCotacao/' . $id]);
            } else {
                return $this->redirect(['action' => 'view/' . $id]);
            }
        }
    }

    public function cotacao($id = null)
    {
        $this->loadModel('PfFiltros');
        try {
            $filtro = $this->PfFiltros->get($id);
        } catch (\Exception $e) {
            $this->viewBuilder()->layout('link_tela');
            $this->render("cotacao_expirada");
            return;
        }

        if ($filtro['filtro_pdf_id'] != null && $filtro['filtro_pdf_id'] != '') {
            $this->loadModel('PfPdffiltros');
            $filtroPDF = $this->PfPdffiltros->get($filtro['filtro_pdf_id'])->toArray();
        }

        $simulacao = $this->PfCalculos->get($filtro['pf_calculo_id']);

        $this->loadModel('PfFiltrosTabelas');
        $findTabelas = $this->PfFiltrosTabelas->find('list', ['keyField' => 'pf_tabela_id', 'valueField' => 'pf_tabela_id'])->where(['pf_filtro_id' => $id])->toArray();

        $informacoes = [
            'Imagens',
            'PfFormasPagamentos',
            'PfReembolsos',
            'PfDependentes',
            'PfComercializacoes',
            'PfObservacoes',
            'PfRedes',
            'PfCarencias',
        ];

        $filtro['carencia'] == 1 ? array_merge($informacoes, ['PfCarencias']) : [];
        $filtro['rede'] == 1 ? array_merge($informacoes, ['PfRedes']) : [];
        $filtro['documento'] == 1 ? array_merge($informacoes, ['PfDocumentos']) : [];
        $filtro['observacao'] == 1 ? array_merge($informacoes, ['PfObservacoes']) : [];

        $this->loadModel('PfTabelas');
        $tabelas = $this->PfTabelas->find('all', ['contain' => [
            'Estados',
            'PfAtendimentos',
            'PfAcomodacoes',
            'PfCoberturas',
            'PfProdutos' => [
                'PfOperadoras' => $informacoes,
                'TiposProdutos'
            ]
        ]])
            ->order([
                'PfOperadoras.prioridade' => 'ASC',
                'PfTabelas.prioridade' => 'ASC'
            ])
            ->where(['PfTabelas.id IN' => $findTabelas])->toArray();

        $operadorasIds = array();

        foreach ($tabelas as $tabela) {
            $operadorasIds[$tabela["pf_operadora_id"]] = $tabela["pf_operadora_id"];

            if ($tabela['coparticipacao'] == 'n') {
                $coparticipacao = 'S/COPARTICIPAÇÃO';
            } else {
                $coparticipacao = 'C/COPARTICIPAÇÃO';
            }
            $tabelasOrdenadas[$tabela['pf_produto']['pf_operadora']['id']][$tabela['pf_produto']['nome']][$tabela['nome'] . " - " . $tabela['pf_acomodaco']['nome'] . " - " . $coparticipacao] = $tabela;
        }

        $simulacao['observaco'] = $filtro['observacao'];
        $simulacao['rede'] = $filtro['rede'];
        $simulacao['reembolso'] = $filtro['reembolso'];
        $simulacao['carencia'] = $filtro['carencia'];
        $simulacao['documento'] = $filtro['documento'];

        //NÃO UTILIZADOS
        $this->loadModel('PfOperadoras');
        $op = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->toArray();
        foreach ($op as $o) {
            $operadoras[$o['id']] = $o;
        }


        //ENCONTRAR ENTIDADES QUE POSSUEM ASSOCIAÇÃO COM A PROFISSAO DO CALCULO
        $this->loadModel("PfEntidadesPfOperadoras");
        $arvore = $this->PfEntidadesPfOperadoras->find("all")
            ->where(["pf_operadora_id IN" => $operadorasIds])
            ->contain(["PfEntidades" => ["PfEntidadesProfissoes" => ["PfProfissoes"]]])
            ->toArray();
        /*  debug($arvore);
        die; */
        foreach ($arvore as $dados) {
            foreach ($dados["pf_entidade"]["pf_entidades_profissoes"] as $info) {
                $entidadesOrdenadas[$dados["pf_operadora_id"]][$dados["pf_entidade"]["nome"]][] = $info["pf_profisso"]["nome"];
            }
        }


        //PEGANDO OS DADOS DO USUÁRIO
        $user = $this->PfCalculos->Users->get($filtro['user_id']);
        if ($user['imagem_id'] != null) {
            $imagem = $this->PfCalculos->Users->Imagens->get($user['imagem_id']);
        }
        unset($user['password']);

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->set(compact('tabelas', 'entidadesOrdenadas', 'simulacao', 'tabelasOrdenadas', 'operadoras', 'sessao', 'user', 'imagem', 'findTabelas', 'filtroPDF', 'filtro'));
        $this->viewBuilder()->layout('link_tela');
    }

    public function newCotacao($id)
    {
        $this->loadModel('PfFiltros');
        try {
            $filtro = $this->PfFiltros->get($id);
        } catch (\Exception $e) {
            $this->viewBuilder()->layout('link_tela');
            $this->render("cotacao_expirada");
            return;
        }

        $ids_vitalmed = [92, 103];
        $simulacao = $this->PfCalculos->get($filtro['pf_calculo_id']);

        $this->loadModel('PfFiltrosTabelas');
        $findTabelas = $this->PfFiltrosTabelas->find('list', ['keyField' => 'pf_tabela_id', 'valueField' => 'pf_tabela_id'])->where(['pf_filtro_id' => $id])->toArray();

        $informacoes = [
            'Imagens',
            'PfFormasPagamentos',
            'PfReembolsos',
            'PfDependentes',
            'PfComercializacoes',
            'PfObservacoes',
            'PfRedes',
            'PfCarencias',
        ];

        $filtro['carencia'] == 1 ? array_merge($informacoes, ['PfCarencias']) : [];
        $filtro['rede'] == 1 ? array_merge($informacoes, ['PfRedes']) : [];
        $filtro['documento'] == 1 ? array_merge($informacoes, ['PfDocumentos']) : [];
        $filtro['observacao'] == 1 ? array_merge($informacoes, ['PfObservacoes']) : [];
        $this->loadModel('PfTabelas');
        $tabelas = $this->PfTabelas->find('all')
            ->contain([
                'PfAtendimentos',
                'PfAcomodacoes',
                'PfCoberturas',
                'PfFormasPagamentos',
                'PfCarencias',
                'PfRedes',
                'PfReembolsos',
                'PfDocumentos',
                'PfDependentes',
                'PfComercializacoes',
                'PfObservacoes',
                'PfProdutosGrupos',
                'PfEntidadesGrupos' => [
                    'PfEntidades' => [
                        'PfProfissoes' =>  function ($q) {
                            return $q->order('nome');
                        }
                    ],
                ],
                'PfEntidades' => [
                    'PfProfissoes' =>  function ($q) {
                        return $q->order('nome');
                    }
                ],
                'PfAreasComercializacoes' => [
                    'Metropoles',
                    'Municipios' => ['Estados'],
                    'PfOperadoras' => ['Imagens']
                ]
            ])
            ->where(['PfTabelas.id IN' => $findTabelas])
            ->toArray();

        $operadorasIds = array();
        $tabelas_ordenadas = [];
        $area_adicionada = [];
        $grupo_produtos_adicionados = [];

        foreach ($tabelas as $tabela) {
            $operadora_id = $tabela['pf_areas_comercializaco']['pf_operadora']['id'];
            @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['pf_areas_comercializaco']['pf_operadora'];

            @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['pf_atendimento']['nome']] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_atendimento');

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_areas_comercializaco');
            if (!in_array($tabela['pf_areas_comercializaco']['id'], $area_adicionada)) {
                foreach ($tabela['pf_areas_comercializaco']['metropoles'] as $metropole) {
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                }
                foreach ($tabela['pf_areas_comercializaco']['municipios'] as $municipio) {
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                }
                $area_adicionada[] = $tabela['pf_areas_comercializaco']['id'];
            }
            unset($tabela['pf_areas_comercializaco']['municipios']);
            unset($tabela['pf_areas_comercializaco']['metropoles']);

            // NÃO REPETIÇÃO DOS GRUPOS DE ENTIDADES NAS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['tabelas'] .= $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_entidades_grupo');
            if (!empty($tabela['pf_entidades_grupo']) && !in_array($tabela['pf_entidades_grupo']['id'], $grupo_produtos_adicionados)) {
                foreach ($tabela['pf_entidades_grupo']['pf_entidades'] as $entidade) {
                    @$tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] .= '<strong>' . $entidade['nome'] . ':</strong> ';
                    foreach ($entidade['pf_profissoes'] as $profissao) {
                        $tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] .= $profissao['nome'] . ', ';
                    }
                    $tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] = substr($tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'], 0, -2) . '; ';
                }
                $grupo_produtos_adicionados[] = $tabela['pf_entidades_grupo']['id'];
            }
            unset($tabela['pf_entidades_grupo']);

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_observaco');
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['data'] =  nl2br($tabela['pf_observaco']['descricao']);
            unset($tabela['pf_observaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_rede');
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['data'] =  nl2br($tabela['pf_rede']['descricao']);
            unset($tabela['pf_rede']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'reembolsoEntity');
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
            unset($tabela['reembolsoEntity']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_carencia');
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['data'] =  nl2br($tabela['pf_carencia']['descricao']);
            unset($tabela['pf_carencia']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_dependente');
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['data'] =  nl2br($tabela['pf_dependente']['descricao']);
            unset($tabela['pf_dependente']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_documento');
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['data'] =  nl2br($tabela['pf_documento']['descricao']);
            unset($tabela['pf_documento']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['tabelas'] .=  $this->auxGetNomeProduto($grupo_produtos_adicionados, $tabela, 'pf_formas_pagamento');
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['data'] =  nl2br($tabela['pf_formas_pagamento']['descricao']);
            unset($tabela['pf_formas_pagamento']);

            @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
        }

        $simulacao['observaco'] = $filtro['observacao'];
        $simulacao['rede'] = $filtro['rede'];
        $simulacao['reembolso'] = $filtro['reembolso'];
        $simulacao['carencia'] = $filtro['carencia'];
        $simulacao['documento'] = $filtro['documento'];

        //NÃO UTILIZADOS
        $this->loadModel('PfOperadoras');
        $op = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->toArray();
        foreach ($op as $o) {
            $operadoras[$o['id']] = $o;
        }

        $user = $this->request->session()->read('Auth.User');
        $imagem = $user['imagem'];
        $sessao = $this->request->getSession()->read('Auth.User');

        $this->set(compact('ids_vitalmed', 'tabelas_ordenadas', 'simulacao', 'user', 'filtro', 'findTabelas', 'imagem', 'tabelas', 'operadoras', 'sessao'));
        $this->viewBuilder()->layout('link_tela');
    }


    public function pdfNewCotacao($id)
    {
        $this->loadModel('PfFiltros');
        try {
            $filtro = $this->PfFiltros->get($id);
        } catch (\Exception $e) {
            $this->viewBuilder()->layout('link_tela');
            $this->render("cotacao_expirada");
            return;
        }

        $simulacao = $this->PfCalculos->get($filtro['pf_calculo_id']);

        $this->loadModel('PfFiltrosTabelas');
        $findTabelas = $this->PfFiltrosTabelas->find('list', ['keyField' => 'pf_tabela_id', 'valueField' => 'pf_tabela_id'])->where(['pf_filtro_id' => $id])->toArray();

        $informacoes = [
            'Imagens',
            'PfFormasPagamentos',
            'PfReembolsos',
            'PfDependentes',
            'PfComercializacoes',
            'PfObservacoes',
            'PfRedes',
            'PfCarencias',
        ];

        $filtro['carencia'] == 1 ? array_merge($informacoes, ['PfCarencias']) : [];
        $filtro['rede'] == 1 ? array_merge($informacoes, ['PfRedes']) : [];
        $filtro['documento'] == 1 ? array_merge($informacoes, ['PfDocumentos']) : [];
        $filtro['observacao'] == 1 ? array_merge($informacoes, ['PfObservacoes']) : [];
        $this->loadModel('PfTabelas');
        $tabelas = $this->PfTabelas->find('all')
            ->contain([
                'PfAtendimentos',
                'PfAcomodacoes',
                'PfCoberturas',
                'PfFormasPagamentos',
                'PfCarencias',
                'PfRedes',
                'PfReembolsos',
                'PfDocumentos',
                'PfDependentes',
                'PfComercializacoes',
                'PfObservacoes',
                'PfEntidades' => [
                    'PfProfissoes' =>  function ($q) {
                        return $q->order('nome');
                    }
                ],
                'PfAreasComercializacoes' => [
                    'Municipios' => ['Estados'],
                    'PfOperadoras' => ['Imagens']
                ]
            ])
            ->where(['PfTabelas.id IN' => $findTabelas])
            ->toArray();

        $operadorasIds = array();
        $tabelas_ordenadas = [];

        foreach ($tabelas as $tabela) {
            $operadora_id = $tabela['pf_areas_comercializaco']['pf_operadora']['id'];
            @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['pf_areas_comercializaco']['pf_operadora'];

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            $tabela['area_comercializacao'] = $tabela['pf_areas_comercializaco']['nome'];
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] =  $tabela['pf_areas_comercializaco']['nome'] . '<strong>* Vide Anexo.</strong>';
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['municipios'] =  $tabela['pf_areas_comercializaco']['municipios'];
            unset($tabela['pf_areas_comercializaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['data'] =  $tabela['pf_observaco']['descricao'];
            unset($tabela['pf_observaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['data'] =  $tabela['pf_rede']['descricao'];
            unset($tabela['pf_rede']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  $tabela['reembolsoEntity']['descricao'];
            unset($tabela['reembolsoEntity']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['data'] =  $tabela['pf_carencia']['descricao'];
            unset($tabela['pf_carencia']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['data'] =  $tabela['pf_dependente']['descricao'];
            unset($tabela['pf_dependente']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['data'] =  $tabela['pf_documento']['descricao'];
            unset($tabela['pf_documento']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['data'] =  $tabela['pf_formas_pagamento']['descricao'];
            unset($tabela['pf_formas_pagamento']);

            @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
        }

        $simulacao['observaco'] = $filtro['observacao'];
        $simulacao['rede'] = $filtro['rede'];
        $simulacao['reembolso'] = $filtro['reembolso'];
        $simulacao['carencia'] = $filtro['carencia'];
        $simulacao['documento'] = $filtro['documento'];

        //NÃO UTILIZADOS
        $this->loadModel('PfOperadoras');
        $op = $this->PfOperadoras->find('all', ['contain' => ['Imagens']])->toArray();
        foreach ($op as $o) {
            $operadoras[$o['id']] = $o;
        }

        $user = $this->request->session()->read('Auth.User');
        $imagem = $user['imagem'];
        $sessao = $this->request->getSession()->read('Auth.User');


        //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
        $tipoPDF = $this->request->getData("tipopdf");
        switch ($tipoPDF) {
            case "D":
                $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                break;
            case "I":
                $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                break;
            case 'F':
                $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                break;
            default:
                $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '-' . date('d-m-Y_H:i:s') . '.pdf';
                break;
        }

        $nomePDF = transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove', $nomePDF);

        $this->viewBuilder()->layout('pdf');

        $this->set(compact('tabelas', 'simulacao', 'tabelas_ordenadas', 'operadoras', 'sessao', 'user', 'imagem', 'filtro', 'findTabelas'));
        $this->set(compact('nomePDF', 'tipoPDF'));
    }
}
