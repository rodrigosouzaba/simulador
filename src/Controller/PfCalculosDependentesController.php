<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfCalculosDependentes Controller
 *
 * @property \App\Model\Table\PfCalculosDependentesTable $PfCalculosDependentes
 */
class PfCalculosDependentesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfCalculos']
        ];
        $pfCalculosDependentes = $this->paginate($this->PfCalculosDependentes);

        $this->set(compact('pfCalculosDependentes'));
        $this->set('_serialize', ['pfCalculosDependentes']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Calculos Dependente id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfCalculosDependente = $this->PfCalculosDependentes->get($id, [
            'contain' => ['PfCalculos']
        ]);

        $this->set('pfCalculosDependente', $pfCalculosDependente);
        $this->set('_serialize', ['pfCalculosDependente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfCalculosDependente = $this->PfCalculosDependentes->newEntity();
        if ($this->request->is('post')) {
            $pfCalculosDependente = $this->PfCalculosDependentes->patchEntity($pfCalculosDependente, $this->request->data);
            if ($this->PfCalculosDependentes->save($pfCalculosDependente)) {
                $this->Flash->success(__('The pf calculos dependente has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf calculos dependente could not be saved. Please, try again.'));
            }
        }
        $pfCalculos = $this->PfCalculosDependentes->PfCalculos->find('list', ['limit' => 200]);
        $this->set(compact('pfCalculosDependente', 'pfCalculos'));
        $this->set('_serialize', ['pfCalculosDependente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Calculos Dependente id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfCalculosDependente = $this->PfCalculosDependentes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfCalculosDependente = $this->PfCalculosDependentes->patchEntity($pfCalculosDependente, $this->request->data);
            if ($this->PfCalculosDependentes->save($pfCalculosDependente)) {
                $this->Flash->success(__('The pf calculos dependente has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf calculos dependente could not be saved. Please, try again.'));
            }
        }
        $pfCalculos = $this->PfCalculosDependentes->PfCalculos->find('list', ['limit' => 200]);
        $this->set(compact('pfCalculosDependente', 'pfCalculos'));
        $this->set('_serialize', ['pfCalculosDependente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Calculos Dependente id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfCalculosDependente = $this->PfCalculosDependentes->get($id);
        if ($this->PfCalculosDependentes->delete($pfCalculosDependente)) {
            $this->Flash->success(__('The pf calculos dependente has been deleted.'));
        } else {
            $this->Flash->error(__('The pf calculos dependente could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
