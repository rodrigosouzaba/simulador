<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfCalculosTabelas Controller
 *
 * @property \App\Model\Table\PfCalculosTabelasTable $PfCalculosTabelas
 */
class PfCalculosTabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfCalculos', 'PfTabelas', 'Tabelas']
        ];
        $pfCalculosTabelas = $this->paginate($this->PfCalculosTabelas);

        $this->set(compact('pfCalculosTabelas'));
        $this->set('_serialize', ['pfCalculosTabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Calculos Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfCalculosTabela = $this->PfCalculosTabelas->get($id, [
            'contain' => ['PfCalculos', 'PfTabelas', 'Tabelas']
        ]);

        $this->set('pfCalculosTabela', $pfCalculosTabela);
        $this->set('_serialize', ['pfCalculosTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfCalculosTabela = $this->PfCalculosTabelas->newEntity();
        if ($this->request->is('post')) {
            $pfCalculosTabela = $this->PfCalculosTabelas->patchEntity($pfCalculosTabela, $this->request->data);
            if ($this->PfCalculosTabelas->save($pfCalculosTabela)) {
                $this->Flash->success(__('The pf calculos tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf calculos tabela could not be saved. Please, try again.'));
            }
        }
        $pfCalculos = $this->PfCalculosTabelas->PfCalculos->find('list', ['limit' => 200]);
        $pfTabelas = $this->PfCalculosTabelas->PfTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->PfCalculosTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfCalculosTabela', 'pfCalculos', 'pfTabelas', 'tabelas'));
        $this->set('_serialize', ['pfCalculosTabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Calculos Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfCalculosTabela = $this->PfCalculosTabelas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfCalculosTabela = $this->PfCalculosTabelas->patchEntity($pfCalculosTabela, $this->request->data);
            if ($this->PfCalculosTabelas->save($pfCalculosTabela)) {
                $this->Flash->success(__('The pf calculos tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf calculos tabela could not be saved. Please, try again.'));
            }
        }
        $pfCalculos = $this->PfCalculosTabelas->PfCalculos->find('list', ['limit' => 200]);
        $pfTabelas = $this->PfCalculosTabelas->PfTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->PfCalculosTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfCalculosTabela', 'pfCalculos', 'pfTabelas', 'tabelas'));
        $this->set('_serialize', ['pfCalculosTabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Calculos Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfCalculosTabela = $this->PfCalculosTabelas->get($id);
        if ($this->PfCalculosTabelas->delete($pfCalculosTabela)) {
            $this->Flash->success(__('The pf calculos tabela has been deleted.'));
        } else {
            $this->Flash->error(__('The pf calculos tabela could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
