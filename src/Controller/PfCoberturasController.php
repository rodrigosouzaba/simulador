<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfCoberturas Controller
 *
 * @property \App\Model\Table\PfCoberturasTable $PfCoberturas */
class PfCoberturasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pfCoberturas = $this->paginate($this->PfCoberturas);

        $this->set(compact('pfCoberturas'));
        $this->set('_serialize', ['pfCoberturas']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Cobertura id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfCobertura = $this->PfCoberturas->get($id, [
            'contain' => []
        ]);

        $this->set('pfCobertura', $pfCobertura);
        $this->set('_serialize', ['pfCobertura']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfCobertura = $this->PfCoberturas->newEntity();
        if ($this->request->is('post')) {
            $pfCobertura = $this->PfCoberturas->patchEntity($pfCobertura, $this->request->data);
            if ($this->PfCoberturas->save($pfCobertura)) {
                $this->Flash->success(__('The pf cobertura has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf cobertura could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pfCobertura'));
        $this->set('_serialize', ['pfCobertura']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Cobertura id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfCobertura = $this->PfCoberturas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfCobertura = $this->PfCoberturas->patchEntity($pfCobertura, $this->request->data);
            if ($this->PfCoberturas->save($pfCobertura)) {
                $this->Flash->success(__('The pf cobertura has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf cobertura could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pfCobertura'));
        $this->set('_serialize', ['pfCobertura']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Cobertura id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfCobertura = $this->PfCoberturas->get($id);
        if ($this->PfCoberturas->delete($pfCobertura)) {
            $this->Flash->success(__('The pf cobertura has been deleted.'));
        } else {
            $this->Flash->error(__('The pf cobertura could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
