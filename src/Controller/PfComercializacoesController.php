<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfComercializacoes Controller
 *
 * @property \App\Model\Table\PfComercializacoesTable $PfComercializacoes
 */
class PfComercializacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $pfComercializacoes = $this->PfComercializacoes->find('all', ['contain' => ['Estados', 'PfOperadoras']]);

        $ids_estados = $this->PfComercializacoes->find('list', ['keyValue' => 'estado_id', 'valueField' => 'estado_id'])->toArray();
        $estados = $this->PfComercializacoes->Estados->find('list', ['valueField' => 'nome'])->where(["id IN" => $ids_estados]);
        $this->set(compact('pfComercializacoes', 'estados'));
        // debug($pfComercializacoes->toArray());
    }

    /**
     * View method
     *
     * @param string|null $id Pf Comercializaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfComercializaco = $this->PfComercializacoes->get($id, [
            'contain' => ['Estados', 'PfOperadoras']
        ]);

        $this->set('pfComercializaco', $pfComercializaco);
        $this->set('_serialize', ['pfComercializaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfComercializaco = $this->PfComercializacoes->newEntity();
        if ($this->request->is('post')) {
            $pfComercializaco = $this->PfComercializacoes->patchEntity($pfComercializaco, $this->request->data);
            $escolhidos = $this->request->data['lista_municipios_escolhidos'];
            $municipios = array();
            //$this->loadModel('Municipios');
            foreach ($escolhidos as $municipio) {
                array_push($municipios, $this->PfComercializacoes->Municipios->get($municipio));
            }
            $pfComercializaco = $this->PfComercializacoes->save($pfComercializaco);

            if ($pfComercializaco) {
                $this->PfComercializacoes->Municipios->link($pfComercializaco, $municipios);
                $this->Flash->success(__('Salvo com sucesso.'));
                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $municipios = "";
        $municipios_tabela = "";
        $estados = $this->PfComercializacoes->Estados->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $pfOperadoras = $this->PfComercializacoes->PfOperadoras->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $this->set(compact('pfComercializaco', 'estados', 'pfOperadoras', 'municipios_tabela', 'municipios'));
        $this->set('_serialize', ['pfComercializaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Comercializaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfComercializaco = $this->PfComercializacoes->get($id, ['contain' => ['Municipios']]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfComercializaco = $this->PfComercializacoes->patchEntity($pfComercializaco, $this->request->data);
            $municipios = [];


            foreach ($this->request->data['municipios']['_ids'] as $municipio) {
                array_push($municipios, $this->PfComercializacoes->Municipios->get($municipio));
            }
            if ($this->PfComercializacoes->save($pfComercializaco)) {
                $this->PfComercializacoes->Municipios->MunicipiosPfComercializacoes->deleteAll(['pf_comercializacao_id' => $id]);
                $this->PfComercializacoes->Municipios->link($pfComercializaco, $municipios);
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome'])->where('1 = 1');
        $pfOperadoras = $this->PfComercializacoes->PfOperadoras->find('list', ['limit' => 200, 'valueField' => 'nome'])->where('1 = 1');
        $municipios = $pfComercializaco['municipios'];

        $this->loadModel('Municipios');
        $municipios = $this->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $pfComercializaco['estado_id']])->toArray();

        $this->set(compact('pfComercializaco', 'estados', 'pfOperadoras', 'municipios', 'municipios_tabela'));
        $this->set('_serialize', ['pfComercializaco']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Comercializaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfComercializaco = $this->PfComercializacoes->get($id);
        if ($this->PfComercializacoes->delete($pfComercializaco)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    public function filtroEstado($dados = null)
    {
        if (!empty($dados)) {
            $pfComercializacoes = $this->PfComercializacoes->find()->contain(['Estados', 'PfOperadoras'])->where(['PfComercializacoes.estado_id' => $dados]);
            $this->set(compact('pfComercializacoes'));
        }
    }

    public function getMunicipios($id = null)
    {
        $municipios_tabela = '';
        $municipios = $this->PfComercializacoes->Estados->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $id]);
        $this->set(compact('municipios', 'municipios_tabela'));
    }

    public function filtroOperadoras($estado = null)
    {
        $operadoras = $this->PfComercializacoes->PfOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['PfOperadoras.nome' => 'ASC']
        ])->where([
            'OR' => [
                ['estado_id' => $estado, 'status' => ''],
                ['estado_id' => $estado, 'status IS' => null]
            ]
        ]);
        $this->set(compact('operadoras'));
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {
        $operadoras = $this->PfComercializacoes->PfOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }, 'conditions' => ['1 = 1']])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status' => ''],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('PfOperadoras.nome')
            ->toArray();

        if ($operadora != null) {

            $filtradas = $this->PfComercializacoes->find('all', ['contain' => ['Municipios', 'PfOperadoras' => ['Imagens']]])
                ->where(["PfComercializacoes.pf_operadora_id" => $operadora])
                ->order('PfComercializacoes.nome')->toArray();

            $pf_operadoras = $this->PfComercializacoes->PfOperadoras->find('list', ['limit' => 200, 'valueField' => 'nome']);

            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $this->set(compact('sessao', 'filtradas', 'pf_operadoras'));
        }

        $estados = $this->PfComercializacoes->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfComercializacoes->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados', 'operadora', 'estado'));
    }
}
