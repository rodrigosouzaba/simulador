<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfComercializacoesMunicipios Controller
 *
 * @property \App\Model\Table\PfComercializacoesMunicipiosTable $PfComercializacoesMunicipios
 */
class PfComercializacoesMunicipiosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfTabelas', 'Municipios']
        ];
        $pfComercializacoesMunicipios = $this->paginate($this->PfComercializacoesMunicipios);

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);

        $this->set(compact('pfComercializacoesMunicipios', 'estados'));
        $this->set('_serialize', ['pfComercializacoesMunicipios']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Comercializacoes Municipio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfComercializacoesMunicipio = $this->PfComercializacoesMunicipios->get($id, [
            'contain' => ['PfTabelas', 'Municipios']
        ]);

        $this->set('pfComercializacoesMunicipio', $pfComercializacoesMunicipio);
        $this->set('_serialize', ['pfComercializacoesMunicipio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfComercializacoesMunicipio = $this->PfComercializacoesMunicipios->newEntity();
        if ($this->request->is('post')) {
            $pfComercializacoesMunicipio = $this->PfComercializacoesMunicipios->patchEntity($pfComercializacoesMunicipio, $this->request->data);
            if ($this->PfComercializacoesMunicipios->save($pfComercializacoesMunicipio)) {
                $this->Flash->success(__('The pf comercializacoes municipio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf comercializacoes municipio could not be saved. Please, try again.'));
            }
        }
        $pfTabelas = $this->PfComercializacoesMunicipios->PfTabelas->find('list', ['limit' => 200]);
        $municipios = $this->PfComercializacoesMunicipios->Municipios->find('list', ['limit' => 200]);
        $this->set(compact('pfComercializacoesMunicipio', 'pfTabelas', 'municipios'));
        $this->set('_serialize', ['pfComercializacoesMunicipio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Comercializacoes Municipio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($pfTabela)
    {
        $pfComercializacoesMunicipio = $this->PfComercializacoesMunicipios->newEntity();
        $pfTabela = $this->PfComercializacoesMunicipios->PfTabelas->get($pfTabela);


        $municipios_tabela = $this->PfComercializacoesMunicipios->Municipios->find('list', ['valueField' => 'nome']);
        $municipios_tabela->matching('PfTabelas', function ($q) use ($pfTabela) {
            return $q->where(['PfTabelas.id' => $pfTabela->id]);
        })->where(['1 = 1']);

        if ($this->request->is(['patch', 'post', 'put'])) {

            // Salva os municipios

            $data = $this->request->data;
            $erros = 0;
            foreach ($data['lista_municipios_escolhidos'] as $municipio) {
                // Se o municipio não estiver salvo, salva.
                if (!key_exists($municipio, $municipios_tabela->toArray())) {

                    $pfComercializacoesMunicipio = $this->PfComercializacoesMunicipios->newEntity();

                    $dados = [
                        'pf_tabela_id' => $data['pf_tabela_id'],
                        'municipio_id' => $municipio
                    ];

                    $pfComercializacoesMunicipio = $this->PfComercializacoesMunicipios->patchEntity($pfComercializacoesMunicipio, $dados);

                    if (!$this->PfComercializacoesMunicipios->save($pfComercializacoesMunicipio)) {
                        $erros++;
                    }
                }
            }

            // Remove municipios não selecionados

            $municipios_removidos = array_diff(array_keys($municipios_tabela->toArray()), $data['lista_municipios_escolhidos']);
            foreach ($municipios_removidos as $municipio) {
                $municipio = $this->PfComercializacoesMunicipios->find('list')->where(['pf_tabela_id' => $pfTabela->id, 'municipio_id' => $municipio]);

                foreach ($municipio as $registro) {
                    $registro = $this->PfComercializacoesMunicipios->get($registro);
                    if (!$this->PfComercializacoesMunicipios->delete($registro)) {
                        $erros++;
                    }
                }
            }

            if ($erros == 0) {
                $this->Flash->success(__('The pf comercializacoes municipio has been saved.'));
                return $this->redirect(['controller' => 'users', 'action' => 'configurar-pf', 'destino' => 'PfComercializacoesMunicipios']);
            } else {
                $this->Flash->error(__('The pf comercializacoes municipio could not be saved. Please, try again.'));
            }
        }

        $municipios = $this->PfComercializacoesMunicipios->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $pfTabela->estado_id]);
        $this->set(compact('pfComercializacoesMunicipio', 'pfTabela', 'municipios', 'municipios_tabela'));
        $this->set('_serialize', ['pfComercializacoesMunicipio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Comercializacoes Municipio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfComercializacoesMunicipio = $this->PfComercializacoesMunicipios->get($id);
        if ($this->PfComercializacoesMunicipios->delete($pfComercializacoesMunicipio)) {
            $this->Flash->success(__('The pf comercializacoes municipio has been deleted.'));
        } else {
            $this->Flash->error(__('The pf comercializacoes municipio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function filtroEstado($estado)
    {
        if (!empty($estado)) {
            $this->loadModel('PfOperadoras');
            $operadoras = $this->PfOperadoras->find('list', ['valueField' => 'nome'])->where(['estado_id' => $estado, 'NOT' => ['status' => 'INATIVA']]);
            $this->set(compact('operadoras'));
        }
    }

    public function filtroOperadora($operadora)
    {
        if (!empty($operadora)) {
            $this->loadModel('PfTabelas');
            $tabelas = $this->PfTabelas->find('all')->contain(['PfOperadoras', 'Municipios'])->where(['pf_operadora_id' => $operadora, 'validade' => 1]);
            $this->set(compact('tabelas'));
        }
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {
        $this->loadModel('PfOperadoras');
        $operadoras = $this->PfOperadoras->find('list', ['valueField' => 'nome'])
            ->orderAsc('nome')
            ->where(['estado_id' => $estado, 'NOT' => ['status' => 'INATIVA']]);
        $this->set(compact('operadoras'));

        if ($operadora != null) {

            $this->loadModel('PfTabelas');
            $tabelas = $this->PfTabelas->find('all')->contain(['PfOperadoras', 'Municipios'])

                ->where(['pf_operadora_id' => $operadora, 'validade' => 1]);
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $this->set(compact('sessao', 'tabelas', 'operadora'));
        }

        $estados = $this->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $this->loadModel('Estados');

        $estados = $this->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados', 'operadora', 'estado'));
    }
}
