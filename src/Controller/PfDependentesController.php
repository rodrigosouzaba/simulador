<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfDependentes Controller
 *
 * @property \App\Model\Table\PfDependentesTable $PfDependentes
 */
class PfDependentesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            $conditions_operadoras = [
                'estado_id' => $estado,

            ];
            if ($nova) {
                $operadoras = $this->PfDependentes->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            } else {
                $operadoras = $this->PfDependentes->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'nova' => 0], 'PfOperadoras.estado_id' => $estado, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['PfOperadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['PfOperadoras.nova'] =  1;
                $dependentes = $this->PfDependentes
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->matching('PfOperadoras.PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('PfDependentes.id')
                    ->orderAsc('PfOperadoras.prioridade');
            } else {
                $conditions['PfOperadoras.estado_id'] = $estado;
                $conditions['OR'] = ['PfOperadoras.nova' => 0, 'PfOperadoras.nova IS NULL'];
                $dependentes = $this->PfDependentes
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->where($conditions)
                    ->orderAsc('PfOperadoras.prioridade');
            }
        } else {
            $dependentes = $this->PfDependentes
                ->find('all')
                ->contain(['PfOperadoras'])
                ->orderAsc('PfOperadoras.prioridade');
        }

        $dependentes = $this->paginate($dependentes);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('PfAreasComercializacoes.PfOperadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'pf_operadoras', 'alias' => 'PfOperadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = PfOperadoras.estado_id']]);
            }
        }
        $this->set(compact('dependentes', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }


    /**
     * View method
     *
     * @param string|null $id Pf Dependente id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfDependente = $this->PfDependentes->get($id, [
            'contain' => ['PfOperadoras', 'PfProdutos']
        ]);

        $this->set('pfDependente', $pfDependente);
        $this->set('_serialize', ['pfDependente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfDependente = $this->PfDependentes->newEntity();
        if ($this->request->is('post')) {
            $pfDependente = $this->PfDependentes->patchEntity($pfDependente, $this->request->data);
            if ($this->PfDependentes->save($pfDependente)) {
                $this->Flash->success(__('The pf dependente has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The pf dependente could not be saved. Please, try again.'));
            }
        }
        $pfOperadoras = $this->PfDependentes->PfOperadoras->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('pfDependente', 'pfOperadoras'));
        $this->set('_serialize', ['pfDependente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Dependente id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfDependente = $this->PfDependentes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfDependente = $this->PfDependentes->patchEntity($pfDependente, $this->request->data);
            if ($this->PfDependentes->save($pfDependente)) {
                $this->Flash->success(__('The pf dependente has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The pf dependente could not be saved. Please, try again.'));
            }
        }
        $pfOperadoras = $this->PfDependentes->PfOperadoras->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('pfDependente', 'pfOperadoras'));
        $this->set('_serialize', ['pfDependente']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Dependente id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfDependente = $this->PfDependentes->get($id);
        if ($this->PfDependentes->delete($pfDependente)) {
            $this->Flash->success(__('The pf dependente has been deleted.'));
        } else {
            $this->Flash->error(__('The pf dependente could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $filtradas = $this->Global->filtroOperadora($id, 'pf_operadora_id', 'PfOperadoras');
        //        debug($dependentes);
        //        die();
        $pf_operadoras = $this->PfDependentes->PfOperadoras->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('filtradas', 'pf_operadoras'));
    }


    public function filtroEstado()
    {
        $estado = $this->request->data()['estados'];
        $operadoras = $this->PfDependentes->PfOperadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }, 'conditions' => ['1 = 1']])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact('operadoras'));
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $operadoras = $this->PfDependentes->PfOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }, 'conditions' => ['1 = 1']])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status' => ''],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('PfOperadoras.nome')
            ->toArray();

        $conditions[] = ['PfOperadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['PfOperadoras.id' => $operadora];
        }

        $this->paginate = [
            'valueField' => 'nome', 'order' => ['PfDependentes.prioridade' => 'ASC'],
            'contain' => [
                'PfOperadoras'
            ],
            'order' => [
                'PfDependentes.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $dependentes = $this->paginate($this->PfDependentes);

        $estados = $this->PfDependentes->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfDependentes->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'dependentes', 'estados', 'operadora', 'estado', 'sessao'));
    }

    function filtroNova($nova)
    {
        $conditions = [];
        if ($nova == 1) {
            $conditions[] = ['PfOperadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['PfOperadoras.nova IS NULL', 'PfOperadoras.nova' => 0];
        }
        $this->loadModel('PfOperadoras');
        $operadoras = $this->PfOperadoras->find('list', [
            'valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }
}
