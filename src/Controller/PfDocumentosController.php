<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfDocumentos Controller
 *
 * @property \App\Model\Table\PfDocumentosTable $PfDocumentos
 */
class PfDocumentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            $conditions_operadoras = [
                'estado_id' => $estado,

            ];
            if ($nova) {
                $operadoras = $this->PfDocumentos->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            } else {
                $operadoras = $this->PfDocumentos->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'nova' => 0], 'PfOperadoras.estado_id' => $estado, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['PfOperadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['PfOperadoras.nova'] =  1;
                $documentos = $this->PfDocumentos
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->matching('PfOperadoras.PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('PfDocumentos.id')
                    ->orderAsc('PfOperadoras.prioridade');
            } else {
                $conditions['PfOperadoras.estado_id'] = $estado;
                $conditions['OR'] = ['PfOperadoras.nova' => 0, 'PfOperadoras.nova IS NULL'];
                $documentos = $this->PfDocumentos
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->where($conditions)
                    ->orderAsc('PfOperadoras.prioridade');
            }
        } else {
            $documentos = $this->PfDocumentos
                ->find('all')
                ->contain(['PfOperadoras'])
                ->orderAsc('PfOperadoras.prioridade');
        }

        $documentos = $this->paginate($documentos);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('PfAreasComercializacoes.PfOperadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'pf_operadoras', 'alias' => 'PfOperadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = PfOperadoras.estado_id']]);
            }
        }
        $this->set(compact('documentos', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }

    /**
     * View method
     *
     * @param string|null $id Pf Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfDocumento = $this->PfDocumentos->get($id, [
            'contain' => ['PfOperadoras', 'PfProdutos']
        ]);

        $this->set('pfDocumento', $pfDocumento);
        $this->set('_serialize', ['pfDocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfDocumento = $this->PfDocumentos->newEntity();
        if ($this->request->is('post')) {
            $pfDocumento = $this->PfDocumentos->patchEntity($pfDocumento, $this->request->data);
            if ($this->PfDocumentos->save($pfDocumento)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $pfOperadoras = $this->PfDocumentos->PfOperadoras->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('pfDocumento', 'pfOperadoras'));
        $this->set('_serialize', ['pfDocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfDocumento = $this->PfDocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfDocumento = $this->PfDocumentos->patchEntity($pfDocumento, $this->request->data);
            if ($this->PfDocumentos->save($pfDocumento)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $pfOperadoras = $this->PfDocumentos->PfOperadoras->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('pfDocumento', 'pfOperadoras'));
        $this->set('_serialize', ['pfDocumento']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfDocumento = $this->PfDocumentos->get($id);
        if ($this->PfDocumentos->delete($pfDocumento)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $filtradas = $this->Global->filtroOperadora($id, 'pf_operadora_id', 'PfOperadoras');
        $pf_operadoras = $this->PfDocumentos->PfOperadoras->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('filtradas', 'pf_operadoras'));
    }
    public function filtroEstado()
    {
        $estado = $this->request->data['estados'];
        $operadoras = $this->PfDocumentos->PfOperadoras->find("list", [
            'valueField' => function ($e) {
                return $e->get('nome') . '  ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1']
        ])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact("operadoras"));
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $operadoras = $this->PfDocumentos->PfOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }, 'conditions' => ['1 = 1']])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status' => ''],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('PfOperadoras.nome')
            ->toArray();

        $conditions[] = ['PfOperadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['PfOperadoras.id' => $operadora];
        }

        $this->paginate = [
            'valueField' => 'nome', 'order' => ['PfDocumentos.prioridade' => 'ASC'],
            'contain' => [
                'PfOperadoras'
            ],
            'order' => [
                'PfDocumentos.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $documentos = $this->paginate($this->PfDocumentos);

        $estados = $this->PfDocumentos->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfDocumentos->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'documentos', 'estados', 'operadora', 'estado', 'sessao'));
    }

    function filtroNova($nova)
    {
        $conditions = [];
        if ($nova == 1) {
            $conditions[] = ['PfOperadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['PfOperadoras.nova IS NULL', 'PfOperadoras.nova' => 0];
        }
        $this->loadModel('PfOperadoras');
        $operadoras = $this->PfOperadoras->find('list', [
            'valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }
}
