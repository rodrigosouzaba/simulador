<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfEntidades Controller
 *
 * @property \App\Model\Table\PfEntidadesTable $PfEntidades
 */
class PfEntidadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {


        $pfEntidades = $this->paginate($this->PfEntidades->find()->order(['nome' => 'ASC']));

        $this->set(compact('pfEntidades'));
        $this->set('_serialize', ['pfEntidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Entidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfEntidade = $this->PfEntidades->get($id, [
            'contain' => ['PfEntidadesProfissoes' => ['PfProfissoes']]
        ]);

        $this->set('pfEntidade', $pfEntidade);
        $this->set('_serialize', ['pfEntidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('PfProfissoes');
        $pfEntidade = $this->PfEntidades->newEntity();
        if ($this->request->is('post')) {

            $pfEntidade = $this->PfEntidades->patchEntity($pfEntidade, $this->request->data);

            if ($this->PfEntidades->save($pfEntidade)) {
                $this->loadModel('PfEntidadesProfissoes');


                $dados['pf_entidade_id'] = $pfEntidade->id;


                foreach ($this->request->data['lista_profissoes_escolhidas'] as $profissao) {
                    $dados['pf_profissao_id'] = $profissao;
                    $pfEntidadesProfissoes = $this->PfEntidadesProfissoes->newEntity();
                    $pfEntidadesProfissoes = $this->PfEntidadesProfissoes->patchEntity($pfEntidadesProfissoes, $dados);
                    $this->PfEntidadesProfissoes->save($pfEntidadesProfissoes);
                }
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $profissoes_tabela = '';
        $profissoes = $this->PfProfissoes->find('list', ['valueField' => 'nome', 'order' => 'PfProfissoes.nome']);

        $tabelas = $this->PfEntidades->Tabelas->find('list', ['limit' => 200]);

        $this->set(compact('pfEntidade', 'tabelas', 'profissoes', 'profissoes_tabela'));
        $this->set('_serialize', ['pfEntidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Entidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfEntidade = $this->PfEntidades->get($id, [
            'contain' => ['PfEntidadesProfissoes' => ['PfProfissoes']]
        ]);
        //        debug($pfEntidade);
        $this->loadModel('PfProfissoes');
        $profissoes = $this->PfProfissoes->find('list', ['valueField' => 'nome'])->toArray();

        if (count($pfEntidade['pf_entidades_profissoes']) > 0) {
            foreach ($pfEntidade['pf_entidades_profissoes'] as $prof) {
                $profissoes_tabela[$prof['pf_profissao_id']] = $prof['pf_profisso']['nome'];
            }
        } else {
            $profissoes_tabela = '';
        }


        if ($this->request->is(['patch', 'post', 'put'])) {




            $pfEntidade = $this->PfEntidades->patchEntity($pfEntidade, $this->request->data);
            if ($this->PfEntidades->save($pfEntidade)) {


                $this->loadModel('PfEntidadesProfissoes');


                $excluir = $this->PfEntidadesProfissoes->find('all')->where(['PfEntidadesProfissoes.pf_entidade_id' => $id]);
                if (count($excluir) > 0) {
                    foreach ($excluir as $excluir) {
                        $entprof = $this->PfEntidadesProfissoes->get($excluir['id']);
                        $this->PfEntidadesProfissoes->delete($entprof);
                    }
                }


                $dados['pf_entidade_id'] = $pfEntidade->id;
                foreach ($this->request->data['lista_profissoes_escolhidas'] as $profissao) {
                    $dados['pf_profissao_id'] = $profissao;
                    $pfEntidadesProfissoes = $this->PfEntidadesProfissoes->newEntity();
                    $pfEntidadesProfissoes = $this->PfEntidadesProfissoes->patchEntity($pfEntidadesProfissoes, $dados);
                    $this->PfEntidadesProfissoes->save($pfEntidadesProfissoes);
                }
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $tabelas = $this->PfEntidades->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfEntidade', 'profissoes', 'profissoes_tabela', 'tabelas'));
        $this->set('_serialize', ['pfEntidade']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Entidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfEntidade = $this->PfEntidades->get($id);

        //Desvincula ENTIDADE da OPERADORA PF
        $this->loadModel("PfEntidadesPfOperadoras");
        $pfEntidadesOperadoras = $this->PfEntidadesPfOperadoras->find("list")->where(["pf_entidade_id" => $id])->toArray();

        if (isset($pfEntidadesOperadoras) && !empty($pfEntidadesOperadoras)) {
            foreach ($pfEntidadesOperadoras as $apagar) {
                $entidadeApagar = $this->PfEntidadesPfOperadoras->get($apagar);
                $this->PfEntidadesPfOperadoras->delete($entidadeApagar);
            }
        }

        //Desvincula ENTIDADE das PROFISSOES
        $this->loadModel("PfEntidadesProfissoes");
        $pfEntidadesProfissoes = $this->PfEntidadesProfissoes->find("list")->where(["pf_entidade_id" => $id])->toArray();

        if (isset($pfEntidadesProfissoes) && !empty($pfEntidadesProfissoes)) {
            foreach ($pfEntidadesProfissoes as $apagar) {
                $entidadeApagar = $this->PfEntidadesProfissoes->get($apagar);
                $this->PfEntidadesProfissoes->delete($entidadeApagar);
            }
        }


        if ($this->PfEntidades->delete($pfEntidade)) {
            $this->Flash->success(__('Entidade excluída com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir entidade. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    public function search()
    {
        // $palavra = $this->request->query['q'];
        $sugestao = $this->PfEntidades->find('list', [
            'keyField' => 'id', 'valueField' => 'nome'
            // , 'conditions' => ['nome LIKE' => $palavra."%"]
        ])->toArray();
        $this->set(compact('sugestao'));
    }
}
