<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfEntidadesGrupos Controller
 *
 * @property \App\Model\Table\PfEntidadesGruposTable $PfEntidadesGrupos
 *
 * @method \App\Model\Entity\PfEntidadesGrupo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PfEntidadesGruposController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfOperadoras'],
        ];
        $pfEntidadesGrupos = $this->paginate($this->PfEntidadesGrupos);

        $this->set(compact('pfEntidadesGrupos'));
    }

    /**
     * View method
     *
     * @param string|null $id Pf Entidades Grupo id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfEntidadesGrupo = $this->PfEntidadesGrupos->get($id, [
            'contain' => ['PfOperadoras', 'PfEntidades'],
        ]);

        $this->set('pfEntidadesGrupo', $pfEntidadesGrupo);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfEntidadesGrupo = $this->PfEntidadesGrupos->newEntity();
        if ($this->request->is('post')) {
            $pfEntidadesGrupo = $this->PfEntidadesGrupos->patchEntity($pfEntidadesGrupo, $this->request->getData());
            if ($this->PfEntidadesGrupos->save($pfEntidadesGrupo)) {
                $this->Flash->success(__('The pf entidades grupo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pf entidades grupo could not be saved. Please, try again.'));
        }
        $pfOperadoras = $this->PfEntidadesGrupos->PfOperadoras->find('list', ['valueField' => function ($q) {
            return $q->get('nome') . ' - ' . $q->get('detalhe');
        }, 'limit' => 200])->where(['nova' => 1])->orderAsc('nome');
        $pfEntidades = $this->PfEntidadesGrupos->PfEntidades->find('list', ['valueField' => 'nome', 'limit' => 200])->orderAsc('nome');
        $this->set(compact('pfEntidadesGrupo', 'pfOperadoras', 'pfEntidades'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Entidades Grupo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfEntidadesGrupo = $this->PfEntidadesGrupos->get($id, [
            'contain' => ['PfEntidades'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfEntidadesGrupo = $this->PfEntidadesGrupos->patchEntity($pfEntidadesGrupo, $this->request->getData());
            if ($this->PfEntidadesGrupos->save($pfEntidadesGrupo)) {
                $this->Flash->success(__('The pf entidades grupo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pf entidades grupo could not be saved. Please, try again.'));
        }
        $pfOperadoras = $this->PfEntidadesGrupos->PfOperadoras->find('list', ['valueField' => function ($q) {
            return $q->get('nome') . ' - ' . $q->get('detalhe');
        }, 'limit' => 200])->where(['nova' => 1])->orderAsc('nome');
        $pfEntidades = $this->PfEntidadesGrupos->PfEntidades->find('list', ['valueField' => 'nome', 'limit' => 200])->orderAsc('nome');
        $this->set(compact('pfEntidadesGrupo', 'pfOperadoras', 'pfEntidades'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Entidades Grupo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfEntidadesGrupo = $this->PfEntidadesGrupos->get($id);
        if ($this->PfEntidadesGrupos->delete($pfEntidadesGrupo)) {
            $this->Flash->success(__('The pf entidades grupo has been deleted.'));
        } else {
            $this->Flash->error(__('The pf entidades grupo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
