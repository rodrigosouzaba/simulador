<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfEntidadesPfOperadoras Controller
 *
 * @property \App\Model\Table\PfEntidadesPfOperadorasTable $PfEntidadesPfOperadoras
 */
class PfEntidadesPfOperadorasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfEntidades', 'PfOperadoras']
        ];
        $pfEntidadesPfOperadoras = $this->paginate($this->PfEntidadesPfOperadoras);

        $this->set(compact('pfEntidadesPfOperadoras'));
        $this->set('_serialize', ['pfEntidadesPfOperadoras']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Entidades Pf Operadora id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfEntidadesPfOperadora = $this->PfEntidadesPfOperadoras->get($id, [
            'contain' => ['PfEntidades', 'PfOperadoras']
        ]);

        $this->set('pfEntidadesPfOperadora', $pfEntidadesPfOperadora);
        $this->set('_serialize', ['pfEntidadesPfOperadora']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfEntidadesPfOperadora = $this->PfEntidadesPfOperadoras->newEntity();
        if ($this->request->is('post')) {
            $pfEntidadesPfOperadora = $this->PfEntidadesPfOperadoras->patchEntity($pfEntidadesPfOperadora, $this->request->data);
            if ($this->PfEntidadesPfOperadoras->save($pfEntidadesPfOperadora)) {
                $this->Flash->success(__('The pf entidades pf operadora has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf entidades pf operadora could not be saved. Please, try again.'));
            }
        }
        $pfEntidades = $this->PfEntidadesPfOperadoras->PfEntidades->find('list', ['limit' => 200]);
        $pfOperadoras = $this->PfEntidadesPfOperadoras->PfOperadoras->find('list', ['limit' => 200]);
        $this->set(compact('pfEntidadesPfOperadora', 'pfEntidades', 'pfOperadoras'));
        $this->set('_serialize', ['pfEntidadesPfOperadora']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Entidades Pf Operadora id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfEntidadesPfOperadora = $this->PfEntidadesPfOperadoras->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfEntidadesPfOperadora = $this->PfEntidadesPfOperadoras->patchEntity($pfEntidadesPfOperadora, $this->request->data);
            if ($this->PfEntidadesPfOperadoras->save($pfEntidadesPfOperadora)) {
                $this->Flash->success(__('The pf entidades pf operadora has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf entidades pf operadora could not be saved. Please, try again.'));
            }
        }
        $pfEntidades = $this->PfEntidadesPfOperadoras->PfEntidades->find('list', ['limit' => 200]);
        $pfOperadoras = $this->PfEntidadesPfOperadoras->PfOperadoras->find('list', ['limit' => 200]);
        $this->set(compact('pfEntidadesPfOperadora', 'pfEntidades', 'pfOperadoras'));
        $this->set('_serialize', ['pfEntidadesPfOperadora']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Entidades Pf Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfEntidadesPfOperadora = $this->PfEntidadesPfOperadoras->get($id);
        if ($this->PfEntidadesPfOperadoras->delete($pfEntidadesPfOperadora)) {
            $this->Flash->success(__('The pf entidades pf operadora has been deleted.'));
        } else {
            $this->Flash->error(__('The pf entidades pf operadora could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
