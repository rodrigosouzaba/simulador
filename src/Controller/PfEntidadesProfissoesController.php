<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfEntidadesProfissoes Controller
 *
 * @property \App\Model\Table\PfEntidadesProfissoesTable $PfEntidadesProfissoes
 */
class PfEntidadesProfissoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfEntidades', 'PfProfissoes']
        ];
        $pfEntidadesProfissoes = $this->paginate($this->PfEntidadesProfissoes);

        $this->set(compact('pfEntidadesProfissoes'));
        $this->set('_serialize', ['pfEntidadesProfissoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Entidades Profisso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfEntidadesProfisso = $this->PfEntidadesProfissoes->get($id, [
            'contain' => ['PfEntidades', 'PfProfissoes']
        ]);

        $this->set('pfEntidadesProfisso', $pfEntidadesProfisso);
        $this->set('_serialize', ['pfEntidadesProfisso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfEntidadesProfisso = $this->PfEntidadesProfissoes->newEntity();
        if ($this->request->is('post')) {
            $pfEntidadesProfisso = $this->PfEntidadesProfissoes->patchEntity($pfEntidadesProfisso, $this->request->data);
            if ($this->PfEntidadesProfissoes->save($pfEntidadesProfisso)) {
                $this->Flash->success(__('The pf entidades profisso has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf entidades profisso could not be saved. Please, try again.'));
            }
        }
        $pfEntidades = $this->PfEntidadesProfissoes->PfEntidades->find('list', ['limit' => 200]);
        $pfProfissoes = $this->PfEntidadesProfissoes->PfProfissoes->find('list', ['limit' => 200]);
        $this->set(compact('pfEntidadesProfisso', 'pfEntidades', 'pfProfissoes'));
        $this->set('_serialize', ['pfEntidadesProfisso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Entidades Profisso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfEntidadesProfisso = $this->PfEntidadesProfissoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfEntidadesProfisso = $this->PfEntidadesProfissoes->patchEntity($pfEntidadesProfisso, $this->request->data);
            if ($this->PfEntidadesProfissoes->save($pfEntidadesProfisso)) {
                $this->Flash->success(__('The pf entidades profisso has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf entidades profisso could not be saved. Please, try again.'));
            }
        }
        $pfEntidades = $this->PfEntidadesProfissoes->PfEntidades->find('list', ['limit' => 200]);
        $pfProfissoes = $this->PfEntidadesProfissoes->PfProfissoes->find('list', ['limit' => 200]);
        $this->set(compact('pfEntidadesProfisso', 'pfEntidades', 'pfProfissoes'));
        $this->set('_serialize', ['pfEntidadesProfisso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Entidades Profisso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfEntidadesProfisso = $this->PfEntidadesProfissoes->get($id);
        if ($this->PfEntidadesProfissoes->delete($pfEntidadesProfisso)) {
            $this->Flash->success(__('The pf entidades profisso has been deleted.'));
        } else {
            $this->Flash->error(__('The pf entidades profisso could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
