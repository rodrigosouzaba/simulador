<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfEntidadesTabelas Controller
 *
 * @property \App\Model\Table\PfEntidadesTabelasTable $PfEntidadesTabelas
 */
class PfEntidadesTabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfEntidades', 'PfTabelas', 'Tabelas']
        ];
        $pfEntidadesTabelas = $this->paginate($this->PfEntidadesTabelas);

        $this->set(compact('pfEntidadesTabelas'));
        $this->set('_serialize', ['pfEntidadesTabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Entidades Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfEntidadesTabela = $this->PfEntidadesTabelas->get($id, [
            'contain' => ['PfEntidades', 'PfTabelas', 'Tabelas']
        ]);

        $this->set('pfEntidadesTabela', $pfEntidadesTabela);
        $this->set('_serialize', ['pfEntidadesTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfEntidadesTabela = $this->PfEntidadesTabelas->newEntity();
        if ($this->request->is('post')) {
            $pfEntidadesTabela = $this->PfEntidadesTabelas->patchEntity($pfEntidadesTabela, $this->request->data);
            if ($this->PfEntidadesTabelas->save($pfEntidadesTabela)) {
                $this->Flash->success(__('The pf entidades tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf entidades tabela could not be saved. Please, try again.'));
            }
        }
        $pfEntidades = $this->PfEntidadesTabelas->PfEntidades->find('list', ['limit' => 200]);
        $pfTabelas = $this->PfEntidadesTabelas->PfTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->PfEntidadesTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfEntidadesTabela', 'pfEntidades', 'pfTabelas', 'tabelas'));
        $this->set('_serialize', ['pfEntidadesTabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Entidades Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfEntidadesTabela = $this->PfEntidadesTabelas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfEntidadesTabela = $this->PfEntidadesTabelas->patchEntity($pfEntidadesTabela, $this->request->data);
            if ($this->PfEntidadesTabelas->save($pfEntidadesTabela)) {
                $this->Flash->success(__('The pf entidades tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf entidades tabela could not be saved. Please, try again.'));
            }
        }
        $pfEntidades = $this->PfEntidadesTabelas->PfEntidades->find('list', ['limit' => 200]);
        $pfTabelas = $this->PfEntidadesTabelas->PfTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->PfEntidadesTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfEntidadesTabela', 'pfEntidades', 'pfTabelas', 'tabelas'));
        $this->set('_serialize', ['pfEntidadesTabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Entidades Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfEntidadesTabela = $this->PfEntidadesTabelas->get($id);
        if ($this->PfEntidadesTabelas->delete($pfEntidadesTabela)) {
            $this->Flash->success(__('The pf entidades tabela has been deleted.'));
        } else {
            $this->Flash->error(__('The pf entidades tabela could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
