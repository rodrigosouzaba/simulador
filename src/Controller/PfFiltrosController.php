<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfFiltros Controller
 *
 * @property \App\Model\Table\PfFiltrosTable $PfFiltros
 */
class PfFiltrosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfCalculos', 'PfAtendimentos', 'PfAcomodacoes', 'TiposProdutos', 'Users']
        ];
        $pfFiltros = $this->paginate($this->PfFiltros);

        $this->set(compact('pfFiltros'));
        $this->set('_serialize', ['pfFiltros']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Filtro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfFiltro = $this->PfFiltros->get($id, [
            'contain' => ['PfCalculos', 'PfAtendimentos', 'PfAcomodacoes', 'TiposProdutos', 'Users', 'Tabelas']
        ]);

        $this->set('pfFiltro', $pfFiltro);
        $this->set('_serialize', ['pfFiltro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfFiltro = $this->PfFiltros->newEntity();
        if ($this->request->is('post')) {
            $pfFiltro = $this->PfFiltros->patchEntity($pfFiltro, $this->request->data);
            if ($this->PfFiltros->save($pfFiltro)) {
                $this->Flash->success(__('The pf filtro has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf filtro could not be saved. Please, try again.'));
            }
        }
        $pfCalculos = $this->PfFiltros->PfCalculos->find('list', ['limit' => 200]);
        $pfAtendimentos = $this->PfFiltros->PfAtendimentos->find('list', ['limit' => 200]);
        $pfAcomodacoes = $this->PfFiltros->PfAcomodacoes->find('list', ['limit' => 200]);
        $tiposProdutos = $this->PfFiltros->TiposProdutos->find('list', ['limit' => 200]);
        $users = $this->PfFiltros->Users->find('list', ['limit' => 200]);
        $tabelas = $this->PfFiltros->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfFiltro', 'pfCalculos', 'pfAtendimentos', 'pfAcomodacoes', 'tiposProdutos', 'users', 'tabelas'));
        $this->set('_serialize', ['pfFiltro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Filtro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfFiltro = $this->PfFiltros->get($id, [
            'contain' => ['Tabelas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfFiltro = $this->PfFiltros->patchEntity($pfFiltro, $this->request->data);
            if ($this->PfFiltros->save($pfFiltro)) {
                $this->Flash->success(__('The pf filtro has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf filtro could not be saved. Please, try again.'));
            }
        }
        $pfCalculos = $this->PfFiltros->PfCalculos->find('list', ['limit' => 200]);
        $pfAtendimentos = $this->PfFiltros->PfAtendimentos->find('list', ['limit' => 200]);
        $pfAcomodacoes = $this->PfFiltros->PfAcomodacoes->find('list', ['limit' => 200]);
        $tiposProdutos = $this->PfFiltros->TiposProdutos->find('list', ['limit' => 200]);
        $users = $this->PfFiltros->Users->find('list', ['limit' => 200]);
        $tabelas = $this->PfFiltros->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfFiltro', 'pfCalculos', 'pfAtendimentos', 'pfAcomodacoes', 'tiposProdutos', 'users', 'tabelas'));
        $this->set('_serialize', ['pfFiltro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Filtro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfFiltro = $this->PfFiltros->get($id);
        if ($this->PfFiltros->delete($pfFiltro)) {
            $this->Flash->success(__('The pf filtro has been deleted.'));
        } else {
            $this->Flash->error(__('The pf filtro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
