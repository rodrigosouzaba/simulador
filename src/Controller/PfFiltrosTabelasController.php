<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfFiltrosTabelas Controller
 *
 * @property \App\Model\Table\PfFiltrosTabelasTable $PfFiltrosTabelas
 */
class PfFiltrosTabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfFiltros', 'PfTabelas', 'Tabelas']
        ];
        $pfFiltrosTabelas = $this->paginate($this->PfFiltrosTabelas);

        $this->set(compact('pfFiltrosTabelas'));
        $this->set('_serialize', ['pfFiltrosTabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Filtros Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfFiltrosTabela = $this->PfFiltrosTabelas->get($id, [
            'contain' => ['PfFiltros', 'PfTabelas', 'Tabelas']
        ]);

        $this->set('pfFiltrosTabela', $pfFiltrosTabela);
        $this->set('_serialize', ['pfFiltrosTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfFiltrosTabela = $this->PfFiltrosTabelas->newEntity();
        if ($this->request->is('post')) {
            $pfFiltrosTabela = $this->PfFiltrosTabelas->patchEntity($pfFiltrosTabela, $this->request->data);
            if ($this->PfFiltrosTabelas->save($pfFiltrosTabela)) {
                $this->Flash->success(__('The pf filtros tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf filtros tabela could not be saved. Please, try again.'));
            }
        }
        $pfFiltros = $this->PfFiltrosTabelas->PfFiltros->find('list', ['limit' => 200]);
        $pfTabelas = $this->PfFiltrosTabelas->PfTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->PfFiltrosTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfFiltrosTabela', 'pfFiltros', 'pfTabelas', 'tabelas'));
        $this->set('_serialize', ['pfFiltrosTabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Filtros Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfFiltrosTabela = $this->PfFiltrosTabelas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfFiltrosTabela = $this->PfFiltrosTabelas->patchEntity($pfFiltrosTabela, $this->request->data);
            if ($this->PfFiltrosTabelas->save($pfFiltrosTabela)) {
                $this->Flash->success(__('The pf filtros tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf filtros tabela could not be saved. Please, try again.'));
            }
        }
        $pfFiltros = $this->PfFiltrosTabelas->PfFiltros->find('list', ['limit' => 200]);
        $pfTabelas = $this->PfFiltrosTabelas->PfTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->PfFiltrosTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfFiltrosTabela', 'pfFiltros', 'pfTabelas', 'tabelas'));
        $this->set('_serialize', ['pfFiltrosTabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Filtros Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfFiltrosTabela = $this->PfFiltrosTabelas->get($id);
        if ($this->PfFiltrosTabelas->delete($pfFiltrosTabela)) {
            $this->Flash->success(__('The pf filtros tabela has been deleted.'));
        } else {
            $this->Flash->error(__('The pf filtros tabela could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
