<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfFormasPagamentos Controller
 *
 * @property \App\Model\Table\PfFormasPagamentosTable $PfFormasPagamentos
 */
class PfFormasPagamentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            if ($nova) {
                $operadoras = $this->PfFormasPagamentos->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            } else {
                $operadoras = $this->PfFormasPagamentos->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'nova' => 0], 'PfOperadoras.estado_id' => $estado, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['PfOperadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['PfOperadoras.nova'] =  1;
                $formasPagamentos = $this->PfFormasPagamentos
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->matching('PfOperadoras.PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('PfFormasPagamentos.id')
                    ->orderAsc('PfOperadoras.prioridade');
            } else {
                $conditions['PfOperadoras.estado_id'] = $estado;
                $conditions['OR'] = ['PfOperadoras.nova' => 0, 'PfOperadoras.nova IS NULL'];
                $formasPagamentos = $this->PfFormasPagamentos
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->where($conditions)
                    ->orderAsc('PfOperadoras.prioridade');
            }
        } else {
            $formasPagamentos = $this->PfFormasPagamentos
                ->find('all')
                ->contain(['PfOperadoras'])
                ->orderAsc('PfOperadoras.prioridade');
        }

        $formasPagamentos = $this->paginate($formasPagamentos);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('PfAreasComercializacoes.PfOperadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'pf_operadoras', 'alias' => 'PfOperadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = PfOperadoras.estado_id']]);
            }
        }
        $this->set(compact('formasPagamentos', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }

    /**
     * View method
     *
     * @param string|null $id Pf Formas Pagamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfFormasPagamento = $this->PfFormasPagamentos->get($id, [
            'contain' => ['PfOperadoras']
        ]);

        $this->set('pfFormasPagamento', $pfFormasPagamento);
        $this->set('_serialize', ['pfFormasPagamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfFormasPagamento = $this->PfFormasPagamentos->newEntity();
        if ($this->request->is('post')) {
            $pfFormasPagamento = $this->PfFormasPagamentos->patchEntity($pfFormasPagamento, $this->request->data);
            if ($this->PfFormasPagamentos->save($pfFormasPagamento)) {
                $this->Flash->success(__('The pf formas pagamento has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The pf formas pagamento could not be saved. Please, try again.'));
            }
        }
        $pfOperadoras = $this->PfFormasPagamentos->PfOperadoras->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('pfFormasPagamento', 'pfOperadoras'));
        $this->set('_serialize', ['pfFormasPagamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Formas Pagamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfFormasPagamento = $this->PfFormasPagamentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfFormasPagamento = $this->PfFormasPagamentos->patchEntity($pfFormasPagamento, $this->request->data);
            if ($this->PfFormasPagamentos->save($pfFormasPagamento)) {
                $this->Flash->success(__('The pf formas pagamento has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('The pf formas pagamento could not be saved. Please, try again.'));
            }
        }
        $pfOperadoras = $this->PfFormasPagamentos->PfOperadoras->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);

        $this->set(compact('pfFormasPagamento', 'pfOperadoras'));
        $this->set('_serialize', ['pfFormasPagamento']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Formas Pagamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfFormasPagamento = $this->PfFormasPagamentos->get($id);
        if ($this->PfFormasPagamentos->delete($pfFormasPagamento)) {
            $this->Flash->success(__('The pf formas pagamento has been deleted.'));
        } else {
            $this->Flash->error(__('The pf formas pagamento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {

        $pfFormasPagamentos = $this->PfFormasPagamentos->find('all', ['contain' => ['PfOperadoras' => ['Imagens']]])
            ->where(['PfFormasPagamentos.pf_operadora_id' => $id])
            ->order('PfFormasPagamentos.nome')->toArray();
        $pfOperadoras = $this->PfFormasPagamentos->PfOperadoras->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('pfFormasPagamentos', 'pfOperadoras'));
    }

    public function filtroEstado()
    {
        $estado = $this->request->data()['estados'];
        $operadoras = $this->PfFormasPagamentos->PfOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1']
        ])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact('operadoras'));
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $operadoras = $this->PfFormasPagamentos->PfOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }, 'conditions' => ['1 = 1']])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status' => ''],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('PfOperadoras.nome')
            ->toArray();

        $conditions[] = ['PfOperadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['PfOperadoras.id' => $operadora];
        }

        $this->paginate = [
            'valueField' => 'nome', 'order' => ['PfFormasPagamentos.prioridade' => 'ASC'],
            'contain' => [
                'PfOperadoras'
            ],
            'order' => [
                'PfFormasPagamentos.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $formasPagamentos = $this->paginate($this->PfFormasPagamentos);

        $estados = $this->PfFormasPagamentos->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfFormasPagamentos->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'formasPagamentos', 'estados', 'operadora', 'estado', 'sessao'));
    }

    function filtroNova($nova)
    {
        $conditions = [];
        if ($nova == 1) {
            $conditions[] = ['PfOperadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['PfOperadoras.nova IS NULL', 'PfOperadoras.nova' => 0];
        }
        $this->loadModel('PfOperadoras');
        $operadoras = $this->PfOperadoras->find('list', [
            'valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }
}
