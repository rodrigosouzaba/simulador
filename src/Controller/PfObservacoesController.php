<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfObservacoes Controller
 *
 * @property \App\Model\Table\PfObservacoesTable $PfObservacoes
 */
class PfObservacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            $conditions_operadoras = [
                'estado_id' => $estado,

            ];
            if ($nova) {
                $operadoras = $this->PfObservacoes->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            } else {
                $operadoras = $this->PfObservacoes->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'nova' => 0], 'PfOperadoras.estado_id' => $estado, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['PfOperadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['PfOperadoras.nova'] =  1;
                $observacoes = $this->PfObservacoes
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->matching('PfOperadoras.PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('PfObservacoes.id')
                    ->orderAsc('PfOperadoras.prioridade');
            } else {
                $conditions['PfOperadoras.estado_id'] = $estado;
                $conditions['OR'] = ['PfOperadoras.nova' => 0, 'PfOperadoras.nova IS NULL'];
                $observacoes = $this->PfObservacoes
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->where($conditions)
                    ->orderAsc('PfOperadoras.prioridade');
            }
        } else {
            $observacoes = $this->PfObservacoes
                ->find('all')
                ->contain(['PfOperadoras'])
                ->orderAsc('PfOperadoras.prioridade');
        }

        $observacoes = $this->paginate($observacoes);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('PfAreasComercializacoes.PfOperadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'pf_operadoras', 'alias' => 'PfOperadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = PfOperadoras.estado_id']]);
            }
        }
        $this->set(compact('observacoes', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }

    /**
     * View method
     *
     * @param string|null $id Pf Observaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfObservaco = $this->PfObservacoes->get($id, [
            'contain' => ['PfOperadoras']
        ]);

        $this->set('pfObservaco', $pfObservaco);
        $this->set('_serialize', ['pfObservaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfObservaco = $this->PfObservacoes->newEntity();
        if ($this->request->is('post')) {
            $pfObservaco = $this->PfObservacoes->patchEntity($pfObservaco, $this->request->data);
            if ($this->PfObservacoes->save($pfObservaco)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $pfOperadoras = $this->PfObservacoes->PfOperadoras->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('pfObservaco', 'pfOperadoras'));
        $this->set('_serialize', ['pfObservaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Observaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfObservaco = $this->PfObservacoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfObservaco = $this->PfObservacoes->patchEntity($pfObservaco, $this->request->data);
            if ($this->PfObservacoes->save($pfObservaco)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $pfOperadoras = $this->PfObservacoes->PfOperadoras->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1'],]);
        $this->set(compact('pfObservaco', 'pfOperadoras'));
        $this->set('_serialize', ['pfObservaco']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Observaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfObservaco = $this->PfObservacoes->get($id);
        if ($this->PfObservacoes->delete($pfObservaco)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $filtradas = $this->Global->filtroOperadora($id, 'pf_operadora_id', 'PfOperadoras');
        $pf_operadoras = $this->PfObservacoes->PfOperadoras->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('filtradas', 'pf_operadoras'));
    }

    public function filtroEstado()
    {
        $estado = $this->request->data()['estado_id'];
        $operadoras = $this->PfObservacoes->PfOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            }
        ])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact('operadoras'));
    }

    function filtroNova($nova)
    {
        $conditions = [];
        if ($nova == 1) {
            $conditions[] = ['PfOperadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['PfOperadoras.nova IS NULL', 'PfOperadoras.nova' => 0];
        }
        $operadoras = $this->PfObservacoes->PfOperadoras->find('list', [
            'valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $nova = null, $operadora = null)
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $conditions_operadoras = [
            'estado_id' => $estado,
            'NOT' => 'status <=> "INATIVA"'
        ];
        if (!is_null($nova) && $nova == 1) {
            $conditions_operadoras[] = ['nova' => 1];
        } else {
            $conditions_operadoras['OR'] = ['nova IS NULL', 'nova' => 0];
        }
        $operadoras = $this->PfObservacoes->PfOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }])
            ->where($conditions_operadoras)
            ->orderASC('PfOperadoras.nome');

        $conditions[] = ['PfOperadoras.estado_id' => $estado];
        if ($nova != null && $nova == 1) {
            $conditions[] = ['PfOperadoras.nova' => 1];
        } else {
            $conditions[] = ['OR' => ['PfOperadoras.nova' => 0, 'PfOperadoras.nova IS NULL']];
        }

        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['PfOperadoras.id' => $operadora];
        }

        $this->paginate = [
            'valueField' => 'nome', 'order' => ['PfTabelas.prioridade' => 'ASC'],
            'contain' => [
                'PfOperadoras'
            ],
            'order' => [
                'PfTabelas.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $observacoes = $this->paginate($this->PfObservacoes);

        $estados = $this->PfObservacoes->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfObservacoes->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('estado', 'estados', 'observacoes', 'operadora', 'operadoras', 'sessao', 'nova'));
    }
}
