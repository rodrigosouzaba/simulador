<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\Event\Event;

/**
 * PfOperadoras Controller
 *
 * @property \App\Model\Table\PfOperadorasTable $PfOperadoras
 */
class PfOperadorasController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow('apiGetEstados');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'conditions' => [
                'OR' => [
                    'NOT' => ['PfOperadoras.nova' => 1],
                    'PfOperadoras.nova is NULL',
                    'PfOperadoras.nova' => 0
                ]
            ],
            'contain' => ['Imagens', "Estados"],
            'order' => ['FIELD(PfOperadoras.status, "INATIVA"), estado_id, PfOperadoras.nome ASC']
        ];
        $pfOperadoras = $this->paginate($this->PfOperadoras);
        $estados = $this->PfOperadoras->find("list", ["valueField" => "estado_id"])->toArray();
        $estados = $this->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);

        $this->set(compact('pfOperadoras', 'estados'));
        $this->set('_serialize', ['pfOperadoras']);
    }

    /**
     * New method
     *
     * @return \Cake\Network\Response|null
     */
    public function new()
    {
        $this->paginate = [
            'conditions' => ['PfOperadoras.nova' => 1],
            'contain' => ['Imagens', "Estados"],
            'order' => ['FIELD(PfOperadoras.status, "INATIVA"), estado_id, PfOperadoras.nome ASC']
        ];
        $pfOperadoras = $this->paginate($this->PfOperadoras);
        $estados = $this->PfOperadoras->find("list", ["valueField" => "estado_id"])->toArray();
        $estados = $this->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('pfOperadoras', 'estados'));
        $this->set('_serialize', ['pfOperadoras']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Operadora id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfOperadora = $this->PfOperadoras->get($id, [
            'contain' => ['Imagens', 'PfCarencias', 'PfComercializacoes', 'PfDependentes', 'PfDocumentos', 'PfObservacoes', 'PfProdutos', 'PfRedes', 'PfReembolsos', 'PfTabelas']
        ]);

        $this->set('pfOperadora', $pfOperadora);
        $this->set('_serialize', ['pfOperadora']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($new = null)
    {
        $pfOperadora = $this->PfOperadoras->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['nova'] = (int)$this->request->data['nova'];
            $pfOperadora = $this->PfOperadoras->patchEntity($pfOperadora, $this->request->data);
            if ($this->PfOperadoras->save($pfOperadora)) {

                //Salva entidades relacionadas a Operadora
                $this->loadModel('PfEntidadesPfOperadoras');
                if (!empty($this->request->data['lista_entidades_escolhidas'])) {
                    foreach ($this->request->data['lista_entidades_escolhidas'] as $entidade) {
                        $dados['pf_operadora_id'] = $pfOperadora->id;
                        $dados['pf_entidade_id'] = $entidade;
                        //                    debug($dados);
                        $entidadeTabela = $this->PfEntidadesPfOperadoras->newEntity();
                        $entidadeTabela = $this->PfEntidadesPfOperadoras->patchEntity($entidadeTabela, $dados);
                        $this->PfEntidadesPfOperadoras->save($entidadeTabela);
                    }
                }

                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente Novamente.'));
            }
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);
        //        debug($estados);
        $this->loadModel('PfEntidades');
        $pf_entidades = $this->PfEntidades->find('list', ['limit' => 200, 'valueField' => 'nome', 'order' => 'PfEntidades.nome']);

        $imagens = $this->PfOperadoras->Imagens->find('list', ['limit' => 200]);

        $this->set(compact('pfOperadora', 'imagens', 'estados', "pf_entidades"));
        $this->set('_serialize', ['pfOperadora']);

        if ($new == 1) {
            $this->render('add_new');
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Operadora id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $new = null)
    {
        $pfOperadora = $this->PfOperadoras->get($id, [
            'contain' => []
        ]);

        $this->loadModel('PfEntidadesPfOperadoras');
        if ($this->request->is(['patch', 'post', 'put'])) {
            // 	        				debug($this->request->data);die();
            $pfOperadora = $this->PfOperadoras->patchEntity($pfOperadora, $this->request->data);
            if ($this->PfOperadoras->save($pfOperadora)) {

                //Exclue todas as entidades e vincula as selecionadas
                $excluir = $this->PfEntidadesPfOperadoras->find('all')->where(['PfEntidadesPfOperadoras.pf_operadora_id' => $id]);
                if ($excluir->count() > 0) {
                    foreach ($excluir as $excluir) {
                        $enttab = $this->PfEntidadesPfOperadoras->get($excluir['id']);
                        $this->PfEntidadesPfOperadoras->delete($enttab);
                    }
                }

                foreach ($this->request->data['lista_entidades_escolhidas'] as $entidade) {

                    $dados['pf_operadora_id'] = $pfOperadora->id;
                    $dados['pf_entidade_id'] = $entidade;
                    //                    debug($dados);
                    $entidadeTabela = $this->PfEntidadesPfOperadoras->newEntity();
                    $entidadeTabela = $this->PfEntidadesPfOperadoras->patchEntity($entidadeTabela, $dados);
                    $this->PfEntidadesPfOperadoras->save($entidadeTabela);
                }


                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }

        $entidades_vinculadas = $this->PfEntidadesPfOperadoras->find("all")->where(["pf_operadora_id" => $id])->contain(["PfEntidades"])->toArray();
        $idsEntidades = array();
        foreach ($entidades_vinculadas as $entidade_viculada) {
            $idsEntidades[] = $entidade_viculada['pf_entidade_id'];
        }
        if (count($idsEntidades) > 0) {
            $conditionsSelecionadas = array("PfEntidades.id IN" => $idsEntidades);
        } else {
            $conditionsSelecionadas = array("PfEntidades.id" => 0);
        }

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);

        $imagens = $this->PfOperadoras->Imagens->find('list', ['limit' => 200]);
        $this->loadModel('PfEntidades');

        $pf_entidades = $this->PfEntidades->find('list', ['valueField' => 'nome', 'order' => 'PfEntidades.nome']);
        $pf_entidades_associadas = $this->PfEntidades->find('list', ['order' => 'PfEntidades.nome'])->where([$conditionsSelecionadas]);


        $this->set(compact('pfOperadora', 'imagens', 'estados', 'pf_entidades', "entidades_vinculadas", "pf_entidades_associadas"));
        $this->set('_serialize', ['pfOperadora']);

        if ($new == 1) {
            $this->render('add_new');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //EXCLUI AS PFTABELAS
        $pfTabelas = $this->PfOperadoras->PfTabelas->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfTabelas)) {
            foreach ($pfTabelas as $pfTabela) {
                $pfTabela = $this->PfOperadoras->PfTabelas->get($pfTabela);
                $this->PfOperadoras->PfTabelas->delete($pfTabela);
            }
        }
        //FIM - EXCLUI AS PFTABELAS

        //EXCLUI AS PFPRODUTOS
        $pfProdutos = $this->PfOperadoras->PfProdutos->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfProdutos)) {
            foreach ($pfProdutos as $pfProduto) {
                $pfProduto = $this->PfOperadoras->PfProdutos->get($pfProduto);
                $this->PfOperadoras->PfProdutos->delete($pfProduto);
            }
        }

        //EXCLUI AS PFCARENCIAS
        $pfCarencias = $this->PfOperadoras->PfCarencias->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfCarencias)) {
            foreach ($pfCarencias as $pfCarencia) {
                $pfCarencia = $this->PfOperadoras->PfCarencias->get($pfCarencia);
                $this->PfOperadoras->PfCarencias->delete($pfCarencia);
            }
        }
        //FIM - EXCLUI AS PFCARENCIAS

        //EXCLUI AS PFFORMASPAGAMENTOS
        $pfPagamentos = $this->PfOperadoras->PfFormasPagamentos->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfPagamentos)) {
            foreach ($pfPagamentos as $pfPagamento) {
                $pfPagamento = $this->PfOperadoras->PfFormasPagamentos->get($pfPagamento);
                $this->PfOperadoras->PfFormasPagamentos->delete($pfPagamento);
            }
        }
        //FIM - EXCLUI AS PFFORMASPAGAMENTOS

        //EXCLUI AS PFCOMERIALIZACOES
        $pfComercializacoes = $this->PfOperadoras->PfComercializacoes->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfComercializacoes)) {
            foreach ($pfComercializacoes as $pfComercializacao) {
                $pfComercializacao = $this->PfOperadoras->PfComercializacoes->get($pfComercializacao);
                $this->PfOperadoras->PfComercializacoes->delete($pfComercializacao);
            }
        }
        //FIM - EXCLUI AS PFCOMERIALIZACOES

        //EXCLUI AS PFDEPENDENTES
        $pfDependentes = $this->PfOperadoras->PfDependentes->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfDependentes)) {
            foreach ($pfDependentes as $pfDependente) {
                $pfDependente = $this->PfOperadoras->PfDependentes->get($pfDependente);
                $this->PfOperadoras->PfDependentes->delete($pfDependente);
            }
        }
        //FIM - EXCLUI AS PFDEPENDENTES

        //EXCLUI AS PFDOCUMENTOS
        $pfDocumentos = $this->PfOperadoras->PfDocumentos->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfDocumentos)) {
            foreach ($pfDocumentos as $pfDocumento) {
                $pfDocumento = $this->PfOperadoras->PfDocumentos->get($pfDocumento);
                $this->PfOperadoras->PfDocumentos->delete($pfDocumento);
            }
        }
        //FIM - EXCLUI AS PFDOCUMENTOS

        //EXCLUI AS PFOBSERVACOES
        $pfObservacoes = $this->PfOperadoras->PfObservacoes->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfObservacoes)) {
            foreach ($pfObservacoes as $pfObservacao) {
                $pfObservacao = $this->PfOperadoras->PfObservacoes->get($pfObservacao);
                $this->PfOperadoras->PfObservacoes->delete($pfObservacao);
            }
        }
        //FIM - EXCLUI AS PFOBSERVACOES

        //EXCLUI AS PFOBSERVACOES
        $pfObservacoes = $this->PfOperadoras->PfObservacoes->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfObservacoes)) {
            foreach ($pfObservacoes as $pfObservacao) {
                $pfObservacao = $this->PfOperadoras->PfObservacoes->get($pfObservacao);
                $this->PfOperadoras->PfObservacoes->delete($pfObservacao);
            }
        }
        //FIM - EXCLUI AS PFOBSERVACOES

        //EXCLUI AS PFREDES
        $pfRedes = $this->PfOperadoras->PfRedes->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfRedes)) {
            foreach ($pfRedes as $pfRede) {
                $pfRede = $this->PfOperadoras->PfRedes->get($pfRede);
                $this->PfOperadoras->PfRedes->delete($pfRede);
            }
        }
        //FIM - EXCLUI AS PFREDES

        //EXCLUI AS PFREEMBOLSOS
        $pfReembolsos = $this->PfOperadoras->PfReembolsos->find('list')->where(["pf_operadora_id" => $id])->toArray();
        if (!empty($pfReembolsos)) {
            foreach ($pfReembolsos as $pfReembolso) {
                $pfReembolso = $this->PfOperadoras->PfReembolsos->get($pfReembolso);
                $this->PfOperadoras->PfReembolsos->delete($pfReembolso);
            }
        }
        //FIM - EXCLUI AS PFREEMBOLSOS

        $this->request->allowMethod(['post', 'delete']);
        $pfOperadora = $this->PfOperadoras->get($id);
        if ($this->PfOperadoras->delete($pfOperadora)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    /**
     * Adicionar Imagem Operadora method
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function addImagem($idOperadora = null)
    {
        $this->set('title', 'Adicionar Logo a Operadora');

        $uploadData = '';
        $this->loadModel('Imagens');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $idOperadora = $this->request->data['idOperadora'];
            if (!empty($this->request->data['file'])) {
                $fileName = $this->request->data['file']['name'];
                $uploadPath = 'uploads/imagens/';
                $uploadFile = $uploadPath . $fileName;

                if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadFile)) {
                    $uploadData = $this->Imagens->newEntity();
                    $uploadData->nome = $fileName;
                    $uploadData->caminho = $uploadPath;
                    $uploadData->created = date("Y-m-d H:i:s");
                    $uploadData->modified = date("Y-m-d H:i:s");
                    if ($this->Imagens->save($uploadData)) {
                        $idImagem = $uploadData->id;


                        $operadora = $this->PfOperadoras->get($idOperadora);

                        $operadora->imagem_id = $idImagem;
                        $this->PfOperadoras->save($operadora);




                        $this->Flash->success(__('Arquivo enviado e Salvo com sucesso.'));
                    } else {
                        $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                    }
                } else {
                    $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                }
            } else {
                $this->Flash->error(__('Nenhum arquivo selecionado.'));
            }
        }

        $this->set('uploadData', $uploadData);

        $files = $this->Imagens->find('all', ['order' => ['Imagens.created' => 'DESC']]);
        $filesRowNum = $files->count();
        $this->set('files', $files);
        $this->set('filesRowNum', $filesRowNum);

        $this->set(compact('files', 'filesRowNum', 'idOperadora'));
        $this->set('_serialize', ['operadora']);
    }

    /**
     * Método para alterar Prioridade de Operadoras
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function prioridade($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $operadora = $this->PfOperadoras->get($this->request->data['operadora_id']);
            $operadora->prioridade = $this->request->data['prioridade'];
            if ($this->PfOperadoras->save($operadora)) {
                $this->redirect(['controller' => 'users', 'action' => 'configurarPf']);
            };
        }
        $operadora_id = $id;
        $this->set(compact('operadora_id'));
    }


    /**
     * Método duplicação de Operadoras com todos os dados exceto rede credenciada
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function duplicar($id = null)
    {
        $operadora = $this->PfOperadoras->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $origem = $this->PfOperadoras->find("all")->contain([
                "PfProdutos",
                "PfCarencias",
                "PfFormasPagamentos",
                "PfComercializacoes",
                "PfObservacoes",
                "Imagens",
                "PfDependentes",
                "PfReembolsos",
                "PfDocumentos",
                "PfTabelas" => ["PfProdutos"]
            ])
                ->where(["PfOperadoras.id" => $id])
                ->first();

            $novaOperadora = $this->PfOperadoras->newEntity();
            $novaOperadora = $this->PfOperadoras->patchEntity($novaOperadora, $this->request->data);

            if ($this->PfOperadoras->save($novaOperadora)) {
                $this->loadModel("PfProdutos");
                foreach ($origem["pf_produtos"] as $produto) {
                    $dadosProduto = array();
                    $dadosProduto["nome"] = $produto["nome"];
                    $dadosProduto["descricao"] = $produto["descricao"];
                    $dadosProduto["pf_operadora_id"] = $novaOperadora->id;
                    $dadosProduto["tipo_produto_id"] = $novaOperadora["tipo_produto_id"];
                    $produtoNovo = $this->PfProdutos->newEntity();
                    $produtoNovo = $this->PfProdutos->patchEntity($produtoNovo, $dadosProduto);
                    $this->PfProdutos->save($produtoNovo);
                }
                $this->loadModel("PfDocumentos");
                foreach ($origem["pf_documentos"] as $documento) {
                    $dadosDocumentos = array();
                    $dadosDocumentos["descricao"] = $documento["descricao"];
                    $dadosDocumentos["nome"] =  $documento["nome"];
                    $dadosDocumentos["pf_operadora_id"] = $operadora->id;
                    $docNovo = $this->PfDocumentos->newEntity();
                    $docNovo = $this->PfDocumentos->patchEntity($docNovo, $dadosDocumentos);
                    $this->PfDocumentos->save($docNovo);
                }
                $this->loadModel("PfReembolsos");
                foreach ($origem["pf_reembolsos"] as $reembolso) {
                    $dadosReembolso = array();
                    $dadosReembolso["descricao"] = $reembolso["descricao"];
                    $dadosReembolso["nome"] =  $reembolso["nome"];
                    $dadosReembolso["pf_operadora_id"] = $operadora->id;
                    $reembolsoNovo = $this->PfReembolsos->newEntity();
                    $reembolsoNovo = $this->PfReembolsos->patchEntity($reembolsoNovo, $dadosReembolso);
                    $this->PfReembolsos->save($reembolsoNovo);
                }
                $this->loadModel("PfDependentes");
                foreach ($origem["pf_dependentes"] as $dependente) {
                    $dadosDependente = array();
                    $dadosDependente["descricao"] = $dependente["descricao"];
                    $dadosDependente["nome"] =  $dependente["nome"];
                    $dadosDependente["pf_operadora_id"] = $operadora->id;
                    $dependenteNovo = $this->PfDependentes->newEntity();
                    $dependenteNovo = $this->PfDependentes->patchEntity($dependenteNovo, $dadosDependente);
                    $this->PfDependentes->save($dependenteNovo);
                }
                $this->loadModel("PfObservacoes");
                foreach ($origem["pf_observacoes"] as $obs) {
                    $dadosObs = array();
                    $dadosObs["descricao"] = $obs["descricao"];
                    $dadosObs["nome"] =  $obs["nome"];
                    $dadosObs["pf_operadora_id"] = $operadora->id;
                    $obsNovo = $this->PfObservacoes->newEntity();
                    $obsNovo = $this->PfObservacoes->patchEntity($obsNovo, $dadosObs);
                    $this->PfObservacoes->save($obsNovo);
                }
                $this->loadModel("PfComercializacoes");
                foreach ($origem["pf_comercializacoes"] as $comercializacao) {
                    $dadosComercializacao = array();
                    $dadosComercializacao["estado_id"] = $this->request->data["estado_id"];
                    $dadosComercializacao["descricao"] = $comercializacao["descricao"];
                    $dadosComercializacao["nome"] =  $comercializacao["nome"];
                    $dadosComercializacao["pf_operadora_id"] = $operadora->id;
                    $comercializacaoNovo = $this->PfComercializacoes->newEntity();
                    $comercializacaoNovo = $this->PfComercializacoes->patchEntity($comercializacaoNovo, $dadosComercializacao);
                    $this->PfComercializacoes->save($comercializacaoNovo);
                }
                $this->loadModel("PfFormasPagamentos");
                foreach ($origem["pf_formas_pagamentos"] as $formaPgto) {
                    $dadosFormaPgto = array();
                    $dadosFormaPgto["descricao"] = $formaPgto["descricao"];
                    $dadosFormaPgto["nome"] =  $formaPgto["nome"];
                    $dadosFormaPgto["pf_operadora_id"] = $operadora->id;
                    $pgtoNovo = $this->PfFormasPagamentos->newEntity();
                    $pgtoNovo = $this->PfFormasPagamentos->patchEntity($pgtoNovo, $dadosFormaPgto);
                    $this->PfFormasPagamentos->save($pgtoNovo);
                }
                $this->loadModel("PfCarencias");
                foreach ($origem["pf_carencias"] as $carencia) {
                    $dadosCarencia = array();
                    $dadosCarencia["descricao"] = $carencia["descricao"];
                    $dadosCarencia["nome"] =  $carencia["nome"];
                    $dadosCarencia["pf_operadora_id"] = $operadora->id;
                    $carenciaNovo = $this->PfCarencias->newEntity();
                    $carenciaNovo = $this->PfCarencias->patchEntity($carenciaNovo, $dadosCarencia);
                    $this->PfCarencias->save($carenciaNovo);
                }
                $this->loadModel("PfTabelas");
                foreach ($origem["pf_tabelas"] as $tabela) {
                    $dadosTabela = array();
                    $dadosTabela["nome"] =  $tabela["nome"];
                    $dadosTabela["descricao"] = $tabela["descricao"];
                    $dadosTabela["vigencia"] = $tabela["vigencia"];
                    $dadosTabela["faixa1"] = $tabela["faixa1"];
                    $dadosTabela["faixa2"] = $tabela["faixa2"];
                    $dadosTabela["faixa3"] = $tabela["faixa3"];
                    $dadosTabela["faixa4"] = $tabela["faixa4"];
                    $dadosTabela["faixa5"] = $tabela["faixa5"];
                    $dadosTabela["faixa6"] = $tabela["faixa6"];
                    $dadosTabela["faixa7"] = $tabela["faixa7"];
                    $dadosTabela["faixa8"] = $tabela["faixa8"];
                    $dadosTabela["faixa9"] = $tabela["faixa9"];
                    $dadosTabela["faixa10"] = $tabela["faixa10"];
                    $dadosTabela["faixa11"] = $tabela["faixa11"];
                    $dadosTabela["faixa12"] = $tabela["faixa12"];
                    $dadosTabela["imagem_id"] = $tabela["imagem_id"];
                    $dadosTabela["pf_atendimento_id"] = $tabela["pf_atendimento_id"];
                    $dadosTabela["pf_acomodacao_id"] = $tabela["pf_acomodacao_id"];
                    $dadosTabela["estado_id"] = $novaOperadora->estado_id;
                    $dadosTabela["pf_operadora_id"] = $novaOperadora->id;
                    $dadosTabela["pf_comercializacao_id"] = $tabela["pf_comercializacao_id"];
                    $dadosTabela["validade"] = 0;
                    $dadosTabela["cod_ans"] = $tabela["cod_ans"];
                    $dadosTabela["minimo_vidas"] = $tabela["minimo_vidas"];
                    $dadosTabela["maximo_vidas"] = $tabela["maximo_vidas"];
                    $dadosTabela["prioridade"] = $tabela["prioridade"];
                    $dadosTabela["coparticipacao"] = $tabela["coparticipacao"];
                    $dadosTabela["detalhe_coparticipacao"] = $tabela["detalhe_coparticipacao"];
                    $dadosTabela["modalidade"] = $tabela["modalidade"];
                    $dadosTabela["reembolso"] = $tabela["reembolso"];


                    $produtoNovo = $this->PfProdutos->find("all")->where([
                        "pf_operadora_id" => $novaOperadora['id'],
                        "nome" => $tabela["pf_produto"]["nome"]
                    ])->first()->toArray();
                    $dadosTabela["pf_produto_id"] = $produtoNovo["id"];

                    $tabelaNova = $this->PfTabelas->newEntity();
                    $tabelaNova = $this->PfTabelas->patchEntity($tabelaNova, $dadosTabela);

                    $this->PfTabelas->save($tabelaNova);
                }
                $this->Flash->success(__('Operadora duplicada com sucesso'));
                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            }
        }
        $estados = $this->PfOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);

        $this->set(compact('operadora', 'estados'));
    }

    public function filtro(int $new = null)
    {

        $dados = $this->request->data;
        $conditions = [];

        $estados = $this->PfOperadoras->find("list", ["valueField" => "estado_id", 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfOperadoras->Estados->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']])->where(['id IN' => $estados]);

        if (!empty($dados['estado']) && !$new) {
            array_push($conditions, ['estado_id' => $dados['estado']]);
        }
        if ($new) {
            array_push($conditions, ['nova' => 1]);
        }
        if (!empty($dados['status'])) {

            $status = '';

            if ($dados['status'] == 'ATIVA') {
                $status = [
                    'OR' => [
                        'PfOperadoras.status' => '',
                        'PfOperadoras.status is NULL'
                    ]
                ];
                array_push($conditions, $status);
            } else {
                $status = $dados['status'];
                array_push($conditions, ['PfOperadoras.status' => $status]);
            }
        }

        if (!empty($dados['search'])) {
            array_push($conditions, ['PfOperadoras.nome LIKE' => "%$dados[search]%"]);
        }

        if (!$new || ($new && empty($dados['estado']))) {
            $this->paginate = [
                'contain' => ['Imagens', "Estados"],
                'order' => ['estado_id, PfOperadoras.nome ASC'],
                'conditions' => $conditions
            ];
            $pfOperadoras = $this->paginate($this->PfOperadoras);
        } else {
            $pfOperadoras = $this->PfOperadoras->find('all')
                ->matching('PfAreasComercializacoes.Municipios', function ($q) use ($dados) {
                    return $q->where(['Municipios.estado_id' => $dados['estado']]);
                })
                ->where($conditions)
                ->group('PfOperadoras.id');

            $pfOperadoras = $this->paginate($pfOperadoras);
        }
        // $pfOperadoras = $this->PfOperadoras->find('all')->where($conditions);

        $this->set(compact('estados', 'pfOperadoras', 'new'));
        // $this->render('index');
    }

    public function ocultar($id = null)
    {
        if (isset($id)) {
            $operadora = $this->PfOperadoras->get($id);
            if ($operadora->status == "OCULTA") {
                $operadora->status = "";
                if ($this->PfOperadoras->save($operadora)) {
                    $this->loadModel("PfTabelas");
                    $tabelas = $this->PfTabelas->find("all")->where(["pf_operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->PfTabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 1;
                        $this->PfTabelas->save($tabelaEnt);
                    }

                    $this->Flash->success(__('Operadora Reexibida com sucesso.'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
                } else {
                    $this->Flash->error(__('Erro ao Reexibir Operadora. Tente novamente.'));
                }
            } else {


                $operadora->status = "OCULTA";
                if ($this->PfOperadoras->save($operadora)) {
                    $this->loadModel("PfTabelas");
                    $tabelas = $this->PfTabelas->find("all")->where(["pf_operadora_id" => $id]);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->PfTabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 0;
                        $this->PfTabelas->save($tabelaEnt);
                    }
                    $this->Flash->success(__('Operadora Oculta com sucesso.'));

                    return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
                } else {
                    $this->Flash->error(__('Erro ao Ocultar Operadora. Tente novamente.'));
                }
            }
        }
    }

    public function atualizacao($id = null)
    {
        if (isset($id)) {
            $operadora = $this->PfOperadoras->get($id);
            if ($operadora->status == "EM ATUALIZAÇÃO") {
                $operadora->status = "";
                if ($this->PfOperadoras->save($operadora)) {

                    $this->loadModel("PfTabelas");
                    if ($operadora->nova != 1) {
                        $condition = ["pf_operadora_id" => $id];
                    } else {
                        $this->loadModel('PfAreasComercializacoes');
                        $areas = $this->PfAreasComercializacoes->find('list')->where(['pf_operadora_id' => $operadora->id])->toArray();
                        $condition = ["pf_area_comercializacao_id IN" => $areas];
                    }

                    $tabelas = $this->PfTabelas->find("all")->where($condition);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->PfTabelas->get($tabela["id"]);
                        $tabelaEnt->atualizacao = 0;
                        $this->PfTabelas->save($tabelaEnt);
                    }

                    $this->Flash->success(__('Operadora Removida de ATUALIZAÇÃO de Preços.'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            } else {
                $operadora->status = "EM ATUALIZAÇÃO";
                if ($this->PfOperadoras->save($operadora)) {

                    $this->loadModel("PfTabelas");
                    if ($operadora->nova != 1) {
                        $condition = ["pf_operadora_id" => $id];
                    } else {
                        $this->loadModel('PfAreasComercializacoes');
                        $areas = $this->PfAreasComercializacoes->find('list')->where(['pf_operadora_id' => $operadora->id])->toArray();
                        $condition = ["pf_area_comercializacao_id IN" => $areas];
                    }

                    $tabelas = $this->PfTabelas->find("all")->where($condition);
                    foreach ($tabelas as $tabela) {
                        $tabelaEnt = $this->PfTabelas->get($tabela["id"]);
                        $tabelaEnt->atualizacao = 1;
                        $this->PfTabelas->save($tabelaEnt);
                    }
                    $this->Flash->success(__('Operadora EM ATUALIZAÇÃO DE Preços.'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            }
        }
    }

    public function removeratualizacao($id = null)
    {
        if (isset($id)) {
            $operadora = $this->PfOperadoras->get($id);
            $operadora->status = "";
            if ($this->PfOperadoras->save($operadora)) {
                $this->loadModel("PfTabelas");
                $tabelas = $this->PfTabelas->find("all")->where(["pf_operadora_id" => $id]);
                foreach ($tabelas as $tabela) {
                    $tabelaEnt = $this->PfTabelas->get($tabela["id"]);
                    $tabelaEnt->validade = 1;
                    $this->PfTabelas->save($tabelaEnt);
                }

                $this->Flash->success(__('Operadora Removida de ATUALIZAÇÃO de Preços.'));
                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
            }
        }
    }


    public function ativacao($id = null)
    {
        if (isset($id)) {
            $operadora = $this->PfOperadoras->get($id);
            if ($operadora->status == "INATIVA") {

                $operadora->status = "";
                if ($this->PfOperadoras->save($operadora)) {
                    $this->loadModel("PfTabelas");
                    $Pftabelas = $this->PfTabelas->find("all")->where(["pf_operadora_id" => $id]);
                    foreach ($Pftabelas as $tabela) {
                        $tabelaEnt = $this->PfTabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 1;
                        $this->PfTabelas->save($tabelaEnt);
                    }

                    $this->Flash->success(__('Operadora Ativa.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            } else {

                $operadora->status = "INATIVA";
                $operadora->prioridade = 0;
                if ($this->PfOperadoras->save($operadora)) {
                    $this->loadModel("PfTabelas");
                    $Pftabelas = $this->PfTabelas->find("all")->where(["pf_operadora_id" => $id]);
                    foreach ($Pftabelas as $tabela) {
                        $tabelaEnt = $this->PfTabelas->get($tabela["id"]);
                        $tabelaEnt->validade = 0;
                        $this->PfTabelas->save($tabelaEnt);
                    }

                    //EXCLUINDO AS TABELASGERADAS SALVAS
                    $this->loadModel("TabelasGeradas");
                    $tabelas_geradas = $this->TabelasGeradas->find("all")->where(["operadora" => $id, "ramo" => "SPF"]);
                    foreach ($tabelas_geradas as $tabela_gerada) {
                        $tabelaEnt = $this->TabelasGeradas->get($tabela_gerada["id"]);
                        $this->TabelasGeradas->delete($tabelaEnt);
                    }
                    //FIM -- EXCLUINDO AS TABELASGERADAS SALVAS

                    $this->Flash->success(__('Operadora Inativa.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Erro ao alterar STATUS da Operadora. Tente novamente.'));
                }
            }
        }
    }

    public function validacaoPrioridade()
    {
        $dados = $this->request->data();
        $operadoras = ['result' => $this->PfOperadoras->find('list')->where(['prioridade' => $dados['prioridade']])->count() > 0 ? false : true];
        $this->response->type('json');
        $this->response->body(json_encode($operadoras));
        return $this->response;
        $this->autoRender = false;
    }

    public function apiGetEstados()
    {
        $operadoras_id = $this->PfOperadoras->PfTabelas->find('list', ['keyField' => 'pf_operadora_id', 'valueField' => 'pf_operadora_id'])->where(['OR' => ['validade' => 1, 'validade IS' => NULL]]);
        $operadoras = $this->PfOperadoras->find('list', ['valueField' => 'nome'])->where(['id IN' => $operadoras_id])->toArray();
        $estados_id = $this->PfOperadoras->find('list', ['keyField' => 'id', 'valueField' => 'estado_id'])->where(['id IN' => $operadoras_id])->toArray();
        $estados = $this->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados_id])->toArray();

        foreach ($estados_id as $op_id => $es_id) {
            $result[$estados[$es_id]][$op_id] = $operadoras[$op_id];
        }

        $this->response->type('json');
        $this->response->body(json_encode($result));
        return $this->response;
    }
}
