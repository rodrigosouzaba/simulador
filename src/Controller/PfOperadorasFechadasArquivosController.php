<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfOperadorasFechadasArquivos Controller
 *
 * @property \App\Model\Table\PfOperadorasFechadasArquivosTable $PfOperadorasFechadasArquivos
 */
class PfOperadorasFechadasArquivosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfOperadorasFechadas', 'Arquivos']
        ];
        $pfOperadorasFechadasArquivos = $this->paginate($this->PfOperadorasFechadasArquivos);

        $this->set(compact('pfOperadorasFechadasArquivos'));
        $this->set('_serialize', ['pfOperadorasFechadasArquivos']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Operadoras Fechadas Arquivo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfOperadorasFechadasArquivo = $this->PfOperadorasFechadasArquivos->get($id, [
            'contain' => ['PfOperadorasFechadas', 'Arquivos']
        ]);

        $this->set('pfOperadorasFechadasArquivo', $pfOperadorasFechadasArquivo);
        $this->set('_serialize', ['pfOperadorasFechadasArquivo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfOperadorasFechadasArquivo = $this->PfOperadorasFechadasArquivos->newEntity();
        if ($this->request->is('post')) {
            $pfOperadorasFechadasArquivo = $this->PfOperadorasFechadasArquivos->patchEntity($pfOperadorasFechadasArquivo, $this->request->data);
            if ($this->PfOperadorasFechadasArquivos->save($pfOperadorasFechadasArquivo)) {
                $this->Flash->success(__('The pf operadoras fechadas arquivo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf operadoras fechadas arquivo could not be saved. Please, try again.'));
            }
        }
        $pfOperadorasFechadas = $this->PfOperadorasFechadasArquivos->PfOperadorasFechadas->find('list', ['limit' => 200]);
        $arquivos = $this->PfOperadorasFechadasArquivos->Arquivos->find('list', ['limit' => 200]);
        $this->set(compact('pfOperadorasFechadasArquivo', 'pfOperadorasFechadas', 'arquivos'));
        $this->set('_serialize', ['pfOperadorasFechadasArquivo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Operadoras Fechadas Arquivo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfOperadorasFechadasArquivo = $this->PfOperadorasFechadasArquivos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfOperadorasFechadasArquivo = $this->PfOperadorasFechadasArquivos->patchEntity($pfOperadorasFechadasArquivo, $this->request->data);
            if ($this->PfOperadorasFechadasArquivos->save($pfOperadorasFechadasArquivo)) {
                $this->Flash->success(__('The pf operadoras fechadas arquivo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf operadoras fechadas arquivo could not be saved. Please, try again.'));
            }
        }
        $pfOperadorasFechadas = $this->PfOperadorasFechadasArquivos->PfOperadorasFechadas->find('list', ['limit' => 200]);
        $arquivos = $this->PfOperadorasFechadasArquivos->Arquivos->find('list', ['limit' => 200]);
        $this->set(compact('pfOperadorasFechadasArquivo', 'pfOperadorasFechadas', 'arquivos'));
        $this->set('_serialize', ['pfOperadorasFechadasArquivo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Operadoras Fechadas Arquivo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfOperadorasFechadasArquivo = $this->PfOperadorasFechadasArquivos->get($id);
        if ($this->PfOperadorasFechadasArquivos->delete($pfOperadorasFechadasArquivo)) {
            $this->Flash->success(__('The pf operadoras fechadas arquivo has been deleted.'));
        } else {
            $this->Flash->error(__('The pf operadoras fechadas arquivo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
