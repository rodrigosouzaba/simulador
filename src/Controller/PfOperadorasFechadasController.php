<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfOperadorasFechadas Controller
 *
 * @property \App\Model\Table\PfOperadorasFechadasTable $PfOperadorasFechadas
 */
class PfOperadorasFechadasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pfOperadorasFechadas = $this->paginate($this->PfOperadorasFechadas);

        $this->set(compact('pfOperadorasFechadas'));
        $this->set('_serialize', ['pfOperadorasFechadas']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Operadoras Fechada id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfOperadorasFechada = $this->PfOperadorasFechadas->get($id, [
            'contain' => ['Arquivos']
        ]);

        $this->set('pfOperadorasFechada', $pfOperadorasFechada);
        $this->set('_serialize', ['pfOperadorasFechada']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfOperadorasFechada = $this->PfOperadorasFechadas->newEntity();
        if ($this->request->is('post')) {
            $pfOperadorasFechada = $this->PfOperadorasFechadas->patchEntity($pfOperadorasFechada, $this->request->data);
            if ($this->PfOperadorasFechadas->save($pfOperadorasFechada)) {
                $this->Flash->success(__('The pf operadoras fechada has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf operadoras fechada could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pfOperadorasFechada'));
        $this->set('_serialize', ['pfOperadorasFechada']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Operadoras Fechada id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfOperadorasFechada = $this->PfOperadorasFechadas->get($id, [
            'contain' => ['Arquivos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfOperadorasFechada = $this->PfOperadorasFechadas->patchEntity($pfOperadorasFechada, $this->request->data);
            if ($this->PfOperadorasFechadas->save($pfOperadorasFechada)) {
                $this->Flash->success(__('The pf operadoras fechada has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf operadoras fechada could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pfOperadorasFechada'));
        $this->set('_serialize', ['pfOperadorasFechada']);
        $this->render("add");
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Operadoras Fechada id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfOperadorasFechada = $this->PfOperadorasFechadas->get($id);
        if ($this->PfOperadorasFechadas->delete($pfOperadorasFechada)) {
            $this->Flash->success(__('The pf operadoras fechada has been deleted.'));
        } else {
            $this->Flash->error(__('The pf operadoras fechada could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    /**
	 * Adicionar Arquivos a Operadora que serão enviados por email aos solicitantes
	 *
	 * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
	 */
	public function arquivos($operadoraFechadaId = null)
	{
		$uploadData = '';


		$pf_operadora_fechada = $this->PfOperadorasFechadas
		->find('all')
		->where(["PfOperadorasFechadas.id" => $operadoraFechadaId])
		->contain(["Arquivos"])
		->first();
// 		debug();

		$this->set(compact('pf_operadora_fechada'));

		if ($this->request->is(['patch', 'post', 'put'])) {
/*
			debug($this->request->data);
			die();
*/
			$this->loadModel('Arquivos');
			$pf_operadora_fechada_id = $this->request->data['pf_operadora_fechada_id'];
			if (!empty($this->request->data['file'])) {
				if ($this->request->data['file']['type'] === 'image/png' ||
					$this->request->data['file']['type'] === 'image/jpeg' ||
					$this->request->data['file']['type'] === 'application/pdf' ||
					$this->request->data['file']['type'] === 'application/xls' ||
					$this->request->data['file']['type'] === 'application/xlsx' ||
					$this->request->data['file']['type'] === 'image/jpg' ||
					$this->request->data['file']['type'] === 'image/gif') {
					$fileName = $this->request->data['file']['name'];
					
					//SE NÃO HOUVER PASTA DA OPERADORA CRIA UMA
					if (!file_exists('uploads/pf_operadoras_fechadas/'.$pf_operadora_fechada['nome']."/")) {
					    mkdir('uploads/pf_operadoras_fechadas/'.$pf_operadora_fechada['nome']."/", 0777, true);
					}
					$uploadPath = 'uploads/pf_operadoras_fechadas/'.$pf_operadora_fechada['nome']."/";
					$uploadFile = $uploadPath . $fileName;

					if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadFile)) {
						$uploadData = $this->Arquivos->newEntity();

						$uploadData->nome = $fileName;
						$uploadData->caminho = $uploadPath;
						$uploadData->exibicao_nome = $this->request->data["exibicao_nome"];
						$uploadData->tipo = $this->request->data['tipo'];
						$uploadData->created = date("Y-m-d H:i:s");
						$uploadData->modified = date("Y-m-d H:i:s");
						if ($this->Arquivos->save($uploadData)) {
							$idArquivo = $uploadData->id;
							$this->loadModel('PfOperadorasFechadasArquivos');

							$dados = $this->PfOperadorasFechadasArquivos->newEntity();
							$dados->pf_operadora_fechada_id = $pf_operadora_fechada_id;
							$dados->arquivo_id = $uploadData->id;
							$this->PfOperadorasFechadasArquivos->save($dados);
						} else {
							$this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
						}
					} else {
						$this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
					}
				} else {
					$this->Flash->error(__('Formato de arquivo não permitido. Formatos permitidos são: PDF, XLS, PNG, JPG, JPEG ou GIF.'));
				}
			} else {
				$this->Flash->error(__('Nenhum arquivo selecionado.'));
			}
			return $this->redirect(['action' => 'arquivos', $pf_operadora_fechada_id]);

		}
		$tipos = array("tabela" => 'Tabela', "rede" => "Rede", "formulario"=>"Formulário");
		$this->set(compact('pf_operadora_fechada',"uploadData", "tipos"));
		$this->set('_serialize', ['operadora']);
	}

	public function removerArquivo($arquivoId = null){
/*
		debug($_SERVER['DOCUMENT_ROOT']);
		debug(WWW_ROOT);
		die();
*/
		$this->request->allowMethod(['post', 'delete']);
		$this->loadModel('Arquivos');
		$arquivo = $this->Arquivos->get($arquivoId);

		//Remoção de associação do Arquivo a Operadora
		$this->loadModel("PfOperadorasFechadasArquivos");
		$rel = $this->PfOperadorasFechadasArquivos->find('all')->where(["arquivo_id" => $arquivoId])->first();
		$op = $this->PfOperadorasFechadasArquivos->get($rel['id']);
		$this->PfOperadorasFechadasArquivos->delete($op);

// 		unlink($_SERVER['DOCUMENT_ROOT'] . $arquivo['caminho'].$arquivo['nome']);
		unlink(WWW_ROOT . $arquivo['caminho'].$arquivo['nome']);

		if ($this->Arquivos->delete($arquivo)) {
			$this->Flash->success(__('Excluído com sucesso.'));
		} else {
			$this->Flash->error(__('Erro ao excluir. Tente novamente'));
		}
// 		return $this->redirect(['action' => 'arquivos', $rel['operadora_fechada_id']]);
	}
}
