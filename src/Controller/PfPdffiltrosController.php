<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfPdffiltros Controller
 *
 * @property \App\Model\Table\PfPdffiltrosTable $PfPdffiltros
 */
class PfPdffiltrosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfCalculos', 'PfAtendimentos', 'PfAcomodacaos', 'TipoProdutos']
        ];
        $pfPdffiltros = $this->paginate($this->PfPdffiltros);

        $this->set(compact('pfPdffiltros'));
        $this->set('_serialize', ['pfPdffiltros']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Pdffiltro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfPdffiltro = $this->PfPdffiltros->get($id, [
            'contain' => ['PfCalculos', 'PfAtendimentos', 'PfAcomodacaos', 'TipoProdutos']
        ]);

        $this->set('pfPdffiltro', $pfPdffiltro);
        $this->set('_serialize', ['pfPdffiltro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfPdffiltro = $this->PfPdffiltros->newEntity();
        if ($this->request->is('post')) {
            $pfPdffiltro = $this->PfPdffiltros->patchEntity($pfPdffiltro, $this->request->data);
            if ($this->PfPdffiltros->save($pfPdffiltro)) {
                $this->Flash->success(__('The pf pdffiltro has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf pdffiltro could not be saved. Please, try again.'));
            }
        }
        $pfCalculos = $this->PfPdffiltros->PfCalculos->find('list', ['limit' => 200]);
        $pfAtendimentos = $this->PfPdffiltros->PfAtendimentos->find('list', ['limit' => 200]);
        $pfAcomodacaos = $this->PfPdffiltros->PfAcomodacaos->find('list', ['limit' => 200]);
        $tipoProdutos = $this->PfPdffiltros->TipoProdutos->find('list', ['limit' => 200]);
        $this->set(compact('pfPdffiltro', 'pfCalculos', 'pfAtendimentos', 'pfAcomodacaos', 'tipoProdutos'));
        $this->set('_serialize', ['pfPdffiltro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Pdffiltro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfPdffiltro = $this->PfPdffiltros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfPdffiltro = $this->PfPdffiltros->patchEntity($pfPdffiltro, $this->request->data);
            if ($this->PfPdffiltros->save($pfPdffiltro)) {
                $this->Flash->success(__('The pf pdffiltro has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf pdffiltro could not be saved. Please, try again.'));
            }
        }
        $pfCalculos = $this->PfPdffiltros->PfCalculos->find('list', ['limit' => 200]);
        $pfAtendimentos = $this->PfPdffiltros->PfAtendimentos->find('list', ['limit' => 200]);
        $pfAcomodacaos = $this->PfPdffiltros->PfAcomodacaos->find('list', ['limit' => 200]);
        $tipoProdutos = $this->PfPdffiltros->TipoProdutos->find('list', ['limit' => 200]);
        $this->set(compact('pfPdffiltro', 'pfCalculos', 'pfAtendimentos', 'pfAcomodacaos', 'tipoProdutos'));
        $this->set('_serialize', ['pfPdffiltro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Pdffiltro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfPdffiltro = $this->PfPdffiltros->get($id);
        if ($this->PfPdffiltros->delete($pfPdffiltro)) {
            $this->Flash->success(__('The pf pdffiltro has been deleted.'));
        } else {
            $this->Flash->error(__('The pf pdffiltro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
