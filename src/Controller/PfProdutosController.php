<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfProdutos Controller
 *
 * @property \App\Model\Table\PfProdutosTable $PfProdutos
 */
class PfProdutosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->paginate = [
            'contain' => ['PfOperadoras' => ['Imagens']]
        ];
        $produtos = $this->paginate($this->PfProdutos);

        $operadoras = $this->PfProdutos->PfOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['PfOperadoras.nome' => 'ASC']
        ]);
        $estados = $this->PfProdutos->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfProdutos->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);


        $this->set(compact('operadoras', 'estados', 'produtos', 'sessao'));
    }

    /**
     * View method
     *
     * @param string|null $id Pf Produto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfProduto = $this->PfProdutos->get($id, [
            'contain' => ['PfOperadoras', 'TiposProdutos', 'PfTabelas']
        ]);

        $this->set('pfProduto', $pfProduto);
        $this->set('_serialize', ['pfProduto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfProduto = $this->PfProdutos->newEntity();
        if ($this->request->is('post')) {
            $pfProduto = $this->PfProdutos->patchEntity($pfProduto, $this->request->data);
            //            debug($pfProduto);
            //            die();
            if ($this->PfProdutos->save($pfProduto)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $pfOperadoras = $this->PfProdutos->PfOperadoras->find('list', [
            'valueField' => function ($e) {
                if (!is_null($e->estado)) {
                    return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
                } else {
                    return $e->get('nome') . ' ' . $e->get('detalhe');
                }
            },

            'order' => ['PfOperadoras.nome' => 'ASC'],
            'conditions' => 'NOT (PfOperadoras.status <=> \'INATIVA\') and 1 = 1 ',
            'contain' => ['Estados']
        ]);

        $tiposProdutos = $this->PfProdutos->TiposProdutos->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $this->set(compact('pfProduto', 'pfOperadoras', 'tiposProdutos'));
        $this->set('_serialize', ['pfProduto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Produto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfProduto = $this->PfProdutos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfProduto = $this->PfProdutos->patchEntity($pfProduto, $this->request->data);
            //            debug($pfProduto);
            //            die();
            if ($this->PfProdutos->save($pfProduto)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        //         $pfOperadoras = $this->PfProdutos->PfOperadoras->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $pfOperadoras = $this->PfProdutos->PfOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['PfOperadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $tiposProdutos = $this->PfProdutos->TiposProdutos->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $this->set(compact('pfProduto', 'pfOperadoras', 'tiposProdutos'));
        $this->set('_serialize', ['pfProduto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfProduto = $this->PfProdutos->get($id);
        if ($this->PfProdutos->delete($pfProduto)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }


    /**
     * Método que retorna informações de Carências, Info, PfDependentes .... referente a operadora selecionada.
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function findInfo($id = null)
    {
        $this->loadModel('PfCarencias');
        $this->loadModel('PfReembolsos');
        $this->loadModel('PfRedes');
        $this->loadModel('PfDocumentos');
        $this->loadModel('PfDependentes');
        $this->loadModel('PfObservacoes');
        //        if ($id) {
        $pfCarencias = $this->PfCarencias->find('list', ['valueField' => 'nome', 'order' => ['PfCarencias.nome' => 'ASC']])
            ->where(['PfCarencias.pf_operadora_id' => $id])->toArray();
        $pfReembolsos = $this->PfReembolsos->find('list', ['valueField' => 'nome', 'order' => ['PfReembolsos.nome' => 'ASC']])
            ->where(['PfReembolsos.pf_operadora_id' => $id])->toArray();
        $pfRedes = $this->PfRedes->find('list', ['valueField' => 'nome', 'order' => ['PfRedes.nome' => 'ASC']])
            ->where(['PfRedes.pf_operadora_id' => $id])->toArray();
        $pfDocumentos = $this->PfDocumentos->find('list', ['valueField' => 'nome', 'order' => ['PfDocumentos.nome' => 'ASC']])
            ->where(['PfDocumentos.pf_operadora_id' => $id])->toArray();
        $pfDependentes = $this->PfDependentes->find('list', ['valueField' => 'nome', 'order' => ['PfDependentes.nome' => 'ASC']])
            ->where(['PfDependentes.pf_operadora_id' => $id])->toArray();
        $pfObservacoes = $this->PfObservacoes->find('list', ['valueField' => 'nome', 'order' => ['PfObservacoes.nome' => 'ASC']])
            ->where(['PfObservacoes.pf_operadora_id' => $id])->toArray();
        //            debug($produtos);
        //        } else {
        //            $pfCarencias = $this->PfCarencias->find('list', ['valueField' => 'nome', 'order' => ['PfCarencias.nome' => 'ASC']])->toArray();
        //            $pfReembolsos = $this->PfReembolsos->find('list', ['valueField' => 'nome', 'order' => ['PfReembolsos.nome' => 'ASC']])->toArray();
        //            $pfRedes = $this->PfRedes->find('list', ['valueField' => 'nome', 'order' => ['PfRedes.nome' => 'ASC']])->toArray();
        //            $pfDocumentos = $this->PfDocumentos->find('list', ['valueField' => 'nome', 'order' => ['PfDocumentos.nome' => 'ASC']])->toArray();
        //            $pfDependentes = $this->PfDependentes->find('list', ['valueField' => 'nome', 'order' => ['PfDependentes.nome' => 'ASC']])->toArray();
        //            $pfObservacoes = $this->PfObservacoes->find('list', ['valueField' => 'nome', 'order' => ['PfObservacoes.nome' => 'ASC']])->toArray();
        ////            debug($produtos);
        //        }
        //        die();
        $this->set(compact('pfCarencias', 'pfReembolsos', 'pfRedes', 'pfDocumentos', 'pfDependentes', 'pfObservacoes'));

        $this->render('infoAdicional');
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {
        // Operadoras para filtro
        $operadoras = $this->PfProdutos->PfOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }])
            ->where([
                'estado_id' => $estado,
                'NOT' => 'status <=> "INATIVA"'
            ])
            ->orderASC('PfOperadoras.nome')
            ->toArray();

        // Produtos Filtrados
        $conditions[] = ['PfOperadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['PfOperadoras.id' => $operadora];
        }

        $this->paginate = [
            'contain' => ['PfOperadoras' => ['Imagens']],
            'conditions' => $conditions
        ];

        $filtradas = $this->paginate($this->PfProdutos);

        $estados = $this->PfProdutos->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfProdutos->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados', 'filtradas', 'operadora', 'estado'));
    }
}
