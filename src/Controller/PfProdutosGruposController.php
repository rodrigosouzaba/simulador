<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfProdutosGrupos Controller
 *
 * @property \App\Model\Table\PfProdutosGruposTable $PfProdutosGrupos
 *
 * @method \App\Model\Entity\PfProdutosGrupo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PfProdutosGruposController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfOperadoras'],
        ];
        $pfProdutosGrupos = $this->paginate($this->PfProdutosGrupos);

        $this->set(compact('pfProdutosGrupos'));
    }

    /**
     * View method
     *
     * @param string|null $id Pf Produtos Grupo id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfProdutosGrupo = $this->PfProdutosGrupos->get($id, [
            'contain' => ['PfOperadoras'],
        ]);

        $this->set('pfProdutosGrupo', $pfProdutosGrupo);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfProdutosGrupo = $this->PfProdutosGrupos->newEntity();
        if ($this->request->is('post')) {
            $pfProdutosGrupo = $this->PfProdutosGrupos->patchEntity($pfProdutosGrupo, $this->request->getData());
            if ($this->PfProdutosGrupos->save($pfProdutosGrupo)) {
                $this->Flash->success(__('The pf produtos grupo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pf produtos grupo could not be saved. Please, try again.'));
        }
        $pfOperadoras = $this->PfProdutosGrupos->PfOperadoras
            ->find('list', ['valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }])
            ->where(['nova' => 1])->orderAsc('nome');
        $this->set(compact('pfProdutosGrupo', 'pfOperadoras'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Produtos Grupo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfProdutosGrupo = $this->PfProdutosGrupos->get($id, [
            'contain' => ['PfTabelas'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfProdutosGrupo = $this->PfProdutosGrupos->patchEntity($pfProdutosGrupo, $this->request->getData());
            if ($this->PfProdutosGrupos->save($pfProdutosGrupo)) {
                $this->Flash->success(__('The pf produtos grupo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pf produtos grupo could not be saved. Please, try again.'));
        }
        $pfOperadoras = $this->PfProdutosGrupos->PfOperadoras->find('list', ['valueField' => 'nome']);
        $pfTabelas = $this->PfProdutosGrupos->PfTabelas->find('list', ['valueField' => function ($q) {
            return $q->get('nome') . ' - ' . $q->get('detalhe');
        }])
            ->matching('PfAreasComercializacoes', function ($q) use ($pfProdutosGrupo) {
                return $q->where(['PfAreasComercializacoes.pf_operadora_id' => $pfProdutosGrupo->pf_operadora_id]);
            })
            ->where(['new' => 1])
            ->order(['PfAreasComercializacoes.nome' => 'ASC']);
        $this->set(compact('pfProdutosGrupo', 'pfOperadoras', 'pfTabelas'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Produtos Grupo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfProdutosGrupo = $this->PfProdutosGrupos->get($id);
        if ($this->PfProdutosGrupos->delete($pfProdutosGrupo)) {
            $this->Flash->success(__('The pf produtos grupo has been deleted.'));
        } else {
            $this->Flash->error(__('The pf produtos grupo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getProdutos($operadora)
    {
        $this->loadModel('PfTabelas');
        $produtos = $this->PfTabelas
            ->find('list', ['valueField' => function ($q) use ($operadora) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }])
            ->matching('PfAreasComercializacoes', function ($q) use ($operadora) {
                return $q->where(['PfAreasComercializacoes.pf_operadora_id' => $operadora]);
            })
            ->where(['new' => 1])
            ->order(['PfAreasComercializacoes.nome' => 'ASC']);

        $this->set(compact('produtos'));
    }
}
