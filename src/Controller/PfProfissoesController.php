<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfProfissoes Controller
 *
 * @property \App\Model\Table\PfProfissoesTable $PfProfissoes
 */
class PfProfissoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pfProfissoes = $this->paginate($this->PfProfissoes);

        $this->set(compact('pfProfissoes'));
        $this->set('_serialize', ['pfProfissoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Profisso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfProfisso = $this->PfProfissoes->get($id, [
            'contain' => ['Tabelas']
        ]);

        $this->set('pfProfisso', $pfProfisso);
        $this->set('_serialize', ['pfProfisso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfProfisso = $this->PfProfissoes->newEntity();
        if ($this->request->is('post')) {
            $pfProfisso = $this->PfProfissoes->patchEntity($pfProfisso, $this->request->data, ['associated' => ['pf_profissoes_alias']]);

            if ($this->PfProfissoes->save($pfProfisso)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $this->set(compact('pfProfisso'));
        $this->set('_serialize', ['pfProfisso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Profisso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfProfisso = $this->PfProfissoes->get($id, ['contain' => ['pf_profissoes_alias']]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfProfisso = $this->PfProfissoes->patchEntity($pfProfisso, $this->request->data, ['associated' => ['pf_profissoes_alias']]);
            try {
                $save = $this->PfProfissoes->save($pfProfisso);
                $this->Flash->success(__('Salvo com sucesso.'));
                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } catch (\PDOException $e) {
                if ($e->getCode() == '23000') {
                    $this->Flash->error(__('A Profissão tem apelidos repetidos. Tente novamente.'));
                } else {
                    $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
                }
            }
        }
        $entidades = $this->PfProfissoes->PfEntidades->find('list', ['valueField' => 'nome'])->orderAsc('nome');
        $entidades_assoc = $this->PfProfissoes->PfEntidadesProfissoes->find('list', ['keyField' => 'pf_entidade_id', 'valueField' => 'pf_entidade_id'])->where(['pf_profissao_id' => $pfProfisso->id])->toArray();
        $this->set(compact('pfProfisso', 'entidades', 'entidades_assoc'));
        $this->set('_serialize', ['pfProfisso']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Profisso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfProfisso = $this->PfProfissoes->get($id);
        if ($this->PfProfissoes->delete($pfProfisso)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }
}
