<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PfProfissoesTabelas Controller
 *
 * @property \App\Model\Table\PfProfissoesTabelasTable $PfProfissoesTabelas
 */
class PfProfissoesTabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PfProfissoes', 'PfTabelas', 'Tabelas']
        ];
        $pfProfissoesTabelas = $this->paginate($this->PfProfissoesTabelas);

        $this->set(compact('pfProfissoesTabelas'));
        $this->set('_serialize', ['pfProfissoesTabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Pf Profissoes Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfProfissoesTabela = $this->PfProfissoesTabelas->get($id, [
            'contain' => ['PfProfissoes', 'PfTabelas', 'Tabelas']
        ]);

        $this->set('pfProfissoesTabela', $pfProfissoesTabela);
        $this->set('_serialize', ['pfProfissoesTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfProfissoesTabela = $this->PfProfissoesTabelas->newEntity();
        if ($this->request->is('post')) {
            $pfProfissoesTabela = $this->PfProfissoesTabelas->patchEntity($pfProfissoesTabela, $this->request->data);
            if ($this->PfProfissoesTabelas->save($pfProfissoesTabela)) {
                $this->Flash->success(__('The pf profissoes tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf profissoes tabela could not be saved. Please, try again.'));
            }
        }
        $pfProfissoes = $this->PfProfissoesTabelas->PfProfissoes->find('list', ['limit' => 200]);
        $pfTabelas = $this->PfProfissoesTabelas->PfTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->PfProfissoesTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfProfissoesTabela', 'pfProfissoes', 'pfTabelas', 'tabelas'));
        $this->set('_serialize', ['pfProfissoesTabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Profissoes Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfProfissoesTabela = $this->PfProfissoesTabelas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfProfissoesTabela = $this->PfProfissoesTabelas->patchEntity($pfProfissoesTabela, $this->request->data);
            if ($this->PfProfissoesTabelas->save($pfProfissoesTabela)) {
                $this->Flash->success(__('The pf profissoes tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pf profissoes tabela could not be saved. Please, try again.'));
            }
        }
        $pfProfissoes = $this->PfProfissoesTabelas->PfProfissoes->find('list', ['limit' => 200]);
        $pfTabelas = $this->PfProfissoesTabelas->PfTabelas->find('list', ['limit' => 200]);
        $tabelas = $this->PfProfissoesTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('pfProfissoesTabela', 'pfProfissoes', 'pfTabelas', 'tabelas'));
        $this->set('_serialize', ['pfProfissoesTabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Profissoes Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfProfissoesTabela = $this->PfProfissoesTabelas->get($id);
        if ($this->PfProfissoesTabelas->delete($pfProfissoesTabela)) {
            $this->Flash->success(__('The pf profissoes tabela has been deleted.'));
        } else {
            $this->Flash->error(__('The pf profissoes tabela could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
