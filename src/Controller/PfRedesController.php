<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PfRedes Controller
 *
 * @property \App\Model\Table\PfRedesTable $PfRedes
 */
class PfRedesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            $conditions_operadoras = [
                'estado_id' => $estado,

            ];
            if ($nova) {
                $operadoras = $this->PfRedes->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            } else {
                $operadoras = $this->PfRedes->PfOperadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'nova' => 0], 'PfOperadoras.estado_id' => $estado, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('PfOperadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['PfOperadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['PfOperadoras.nova'] =  1;
                $redes = $this->PfRedes
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->matching('PfOperadoras.PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('PfRedes.id')
                    ->orderAsc('PfOperadoras.prioridade');
            } else {
                $conditions['PfOperadoras.estado_id'] = $estado;
                $conditions['OR'] = ['PfOperadoras.nova' => 0, 'PfOperadoras.nova IS NULL'];
                $redes = $this->PfRedes
                    ->find('all')
                    ->contain(['PfOperadoras'])
                    ->where($conditions)
                    ->orderAsc('PfOperadoras.prioridade');
            }
        } else {
            $redes = $this->PfRedes
                ->find('all')
                ->contain(['PfOperadoras'])
                ->orderAsc('PfOperadoras.prioridade');
        }

        $redes = $this->paginate($redes);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('PfAreasComercializacoes.PfOperadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'pf_operadoras', 'alias' => 'PfOperadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = PfOperadoras.estado_id']]);
            }
        }
        $this->set(compact('redes', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }

    /**
     * View method
     *
     * @param string|null $id Pf Rede id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfRede = $this->PfRedes->get($id, [
            'contain' => ['PfOperadoras', 'PfProdutos']
        ]);

        $this->set('pfRede', $pfRede);
        $this->set('_serialize', ['pfRede']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pfRede = $this->PfRedes->newEntity();
        if ($this->request->is('post')) {
            $pfRede = $this->PfRedes->patchEntity($pfRede, $this->request->data);
            if ($this->PfRedes->save($pfRede)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $pfOperadoras = $this->PfRedes->PfOperadoras->find('list', ['valueField' => 'nome'])
            ->where(['OR' => ['nova is NULL', 'nova' => '']])
            ->orderAsc('nome');
        $this->set(compact('pfRede', 'pfOperadoras'));
        $this->set('_serialize', ['pfRede']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Rede id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pfRede = $this->PfRedes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pfRede = $this->PfRedes->patchEntity($pfRede, $this->request->data);
            if ($this->PfRedes->save($pfRede)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $pfOperadoras = $this->PfRedes->PfOperadoras->find('list', ['valueField' => 'nome'])
            ->where(['OR' => ['nova is NULL', 'nova' => '']])
            ->orderAsc('nome');
        $this->set(compact('pfRede', 'pfOperadoras'));
        $this->set('_serialize', ['pfRede']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Rede id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pfRede = $this->PfRedes->get($id);
        if ($this->PfRedes->delete($pfRede)) {
            $this->Flash->success(__('The pf rede has been deleted.'));
        } else {
            $this->Flash->error(__('The pf rede could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }


    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        $filtradas = $this->Global->filtroOperadora($id, 'pf_operadora_id', 'PfOperadoras');
        $pf_operadoras = $this->PfRedes->PfOperadoras->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $this->set(compact('filtradas', 'pf_operadoras'));
    }

    public function filtroEstado()
    {
        $estado = $this->request->data['estados'];
        $operadoras = $this->PfRedes->PfOperadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }, 'conditions' => ['1 = 1']])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact("operadoras"));
    }
    public function pesquisar($palavra, $estado)
    {
        if ($this->request->isGET()) {
            $palavra = preg_replace('/( ){2,}/', ' ', $palavra);
            $redes = $this->PfRedes->find('all')->where(['descricao LIKE' => '%' . $palavra . '%', "PfOperadoras.estado_id" => $estado, "PfOperadoras.status <>" => "INATIVA"])->contain(['PfOperadoras' => ['Imagens']]);
            $this->set(compact('redes', 'palavra'));
        }
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $operadoras = $this->PfRedes->PfOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }])
            ->where([
                'estado_id' => $estado,
                'NOT' => 'status <=> "INATIVA"'
            ])
            ->orderASC('PfOperadoras.nome')
            ->toArray();

        // Produtos Filtrados
        $conditions[] = ['PfOperadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['PfOperadoras.id' => $operadora];
        }

        $this->paginate = [
            'valueField' => 'nome', 'order' => ['PfTabelas.prioridade' => 'ASC'],
            'contain' => [
                'PfOperadoras'
            ],
            'order' => [
                'PfTabelas.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $redes = $this->paginate($this->PfRedes);

        $estados = $this->PfRedes->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfRedes->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados', 'operadora', 'redes', 'sessao', 'estado'));
    }

    function filtroNova($nova)
    {
        $conditions = [];
        if ($nova == 1) {
            $conditions[] = ['PfOperadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['PfOperadoras.nova IS NULL', 'PfOperadoras.nova' => 0];
        }
        $this->loadModel('PfOperadoras');
        $operadoras = $this->PfOperadoras->find('list', [
            'valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }
}
