<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Database\Type;
use Cake\View\ViewBuilder;
use Knp\Snappy\Pdf;
use App\Utility\PdfWriter;
use Cake\Collection\Collection;
use Cake\Mailer\Email;

// Habilita o parseamento de datas localizadas
Type::build('date')
    ->useLocaleParser()
    ->setLocaleFormat('dd/MM/yyyy');
Type::build('datetime')
    ->useLocaleParser()
    ->setLocaleFormat('dd/MM/yyyy HH:mm:ss');
Type::build('timestamp')
    ->useLocaleParser()
    ->setLocaleFormat('dd/MM/yyyy HH:mm:ss');

// Habilita o parseamento de decimal localizaddos
Type::build('decimal')
    ->useLocaleParser();
Type::build('float')
    ->useLocaleParser();

/**
 * PfPfTabelas Controller
 *
 * @property \App\Model\Table\PfTabelasTable $PfTabelas
 */
class PfTabelasController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['inativaPorVigencia', 'edit']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $conditions = $conditionsInicial = ['1' => '1'];
        if (!empty($this->getRequest()->getData('estados') || !empty($this->getRequest()->getQuery('estados')))) {
            $conditionsInicial[] = ['PfOperadoras.estado_id' => !empty($this->getRequest()->getData('estados')) ? $this->getRequest()->getData('estados') : $this->getRequest()->getQuery('estados'), 'NOT' => 'status <=> "INATIVA"'];
            $conditions[] = ['PfOperadoras.estado_id' => !empty($this->getRequest()->getData('estados')) ? $this->getRequest()->getData('estados') : $this->getRequest()->getQuery('estados')];
        }
        if (!empty($this->getRequest()->getData('fakeOperadora') || !empty($this->getRequest()->getQuery('fakeOperadora')))) {
            $conditionsInicial[] = ['PfOperadoras.id' => !empty($this->getRequest()->getData('fakeOperadora')) ? $this->getRequest()->getData('fakeOperadora') : $this->getRequest()->getQuery('fakeOperadora')];
            $conditions[] = ['PfOperadoras.id' => !empty($this->getRequest()->getData('fakeOperadora')) ? $this->getRequest()->getData('fakeOperadora') : $this->getRequest()->getQuery('fakeOperadora')];
        }

        $conditions[] = ['new' => 0];

        $operadoras = $this->PfTabelas->PfProdutos->PfOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['PfOperadoras.nome' => 'ASC'],
            'conditions' => $conditionsInicial,
            'contain' => ['Estados']
        ]);

        $this->paginate = [
            'valueField' => 'nome', 'order' => ['PfTabelas.prioridade' => 'ASC'],
            'contain' => [
                'PfCoberturas',
                'PfProdutos' =>
                ['PfOperadoras' => ['Imagens', 'PfCarencias', 'PfRedes', 'PfReembolsos', 'PfDocumentos', 'PfDependentes', 'PfObservacoes']],
                'Estados', 'PfAtendimentos', 'PfAcomodacoes', 'PfComercializacoes'
            ],
            'order' => [
                'PfTabelas.validade' => 'DESC',
                'PfTabelas.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $tabelas = $this->paginate($this->PfTabelas);

        $estados = $this->PfTabelas->PfOperadoras->find('list', ['valueField' => 'estado_id',  'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfTabelas->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('tabelas', 'operadoras', 'estados', 'sessao'));
    }

    /**
     * New method
     *
     * @return \Cake\Network\Response|null
     */
    public function new(int $estado = null, $operadora = null, $comercializacao = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $conditionsContain = [];
        $conditionsContain = ['PfTabelas.new' => 1];
        $operadoras = null;
        if ($estado) {
            $operadoras = $this->PfTabelas->PfOperadoras
                ->find('list', ['valueField' => function ($e) {
                    return $e->get('nome') . " " . $e->get('detalhe');
                }])
                ->matching('PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                    return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                })
                ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                ->orderASC('PfOperadoras.nome');
        }
        $comercializacoes = [];
        if ($operadora) {
            $comercializacoes = $this->PfTabelas->PfOperadoras->PfAreasComercializacoes->find('list', ['valueField' => 'nome'])->where(['pf_operadora_id' => $operadora]);
        }

        $tabelas = $this->PfTabelas->find('all');
        if ((!is_null($estado) && !empty($estado)) && is_null($operadora)) {
            $tabelas->matching('PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
            });
        }

        if (!is_null($operadora) && is_null($comercializacao)) {
            $tabelas->matching('PfAreasComercializacoes', function ($q) use ($operadora) {
                return $q->where(['PfAreasComercializacoes.pf_operadora_id' => $operadora]);
            });
        }

        if (!is_null($comercializacao)) {
            $tabelas->matching('PfAreasComercializacoes', function ($q) use ($comercializacao) {
                return $q->where(['PfAreasComercializacoes.id' => $comercializacao]);
            });
        }

        $tabelas->contain([
            'PfCoberturas',
            'PfAcomodacoes',
            'PfAtendimentos',
            'PfAreasComercializacoes' => ['PfAreasComercializacoesEstadosMunicipios'],
            'PfAreasComercializacoes' => ['PfOperadoras' => ['Imagens']]
        ])
            ->where($conditionsContain)
            ->order([
                'PfTabelas.validade' => 'DESC',
                'PfTabelas.prioridade' => 'ASC'
            ])
            ->group('PfTabelas.id');

        $tabelas = $this->paginate($tabelas);
        $estados = $this->PfTabelas->PfAreasComercializacoes->Estados->find('list', ['valueField' => 'nome'])->toArray();

        $this->set(compact('tabelas', 'operadoras', 'estados', 'sessao', 'comercializacoes', 'estado', 'operadora', 'comercializacao'));
    }

    /**
     * View method
     *
     * @param string|null $id Pf Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pfTabela = $this->PfTabelas->get($id, [
            'contain' => ['PfProdutos', 'PfOperadoras', 'PfAtendimentos', 'PfAcomodacoes', 'Estados', 'Regioes', 'PfProfissoesTabelas']
        ]);

        $this->set('pfTabela', $pfTabela);
        $this->set('_serialize', ['pfTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($new = null)
    {
        $pfTabela = $this->PfTabelas->newEntity();
        if ($this->request->is('post')) {
            for ($i = 1; $i <= 12; $i++) {
                if (!isset($this->request->data['faixa' . $i])) {
                    $this->request->data['faixa' . $i] = 0;
                }
                $this->request->data['faixa' . $i] = str_replace('.', ',', $this->request->data['faixa' . $i]);
            }
            $pfTabela = $this->PfTabelas->patchEntity($pfTabela, $this->request->data);
            $pfTabela->new = $new == 1 ? 1 : 0;
            // debug($pfTabela);
            // die;
            if ($this->PfTabelas->save($pfTabela)) {

                $this->Flash->success(__('Salvo com Sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $this->loadModel('PfEntidades');
        $entidades = $this->PfEntidades->find('list', ['limit' => 200, 'valueField' => 'nome', 'order' => 'PfEntidades.nome']);

        $pfProdutos = $this->PfTabelas->PfProdutos->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $pfOperadoras = $this->PfTabelas->PfOperadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['PfOperadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $pf_areas_comercializacoes = $this->PfTabelas->PfAreasComercializacoes
            ->find('list', ['valueField' => function ($q) {
                return  $q->get('nome') . ' | ' .  $q->pf_operadora->get('nome') . ' - ' . $q->pf_operadora->get('detalhe');
            }])
            ->contain(['PfOperadoras'])
            ->where(['PfOperadoras.nova' => 1]);

        $pfAtendimentos = $this->PfTabelas->PfAtendimentos->find('list', ['limit' => 200, 'valueField' => 'nome'])->orderAsc('prioridade');
        $pfAcomodacoes = $this->PfTabelas->PfAcomodacoes->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $estados = $this->PfTabelas->Estados->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $pfComercializacoes = $this->PfTabelas->PfComercializacoes->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $tiposProdutos = $this->PfTabelas->TiposProdutos->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $pfCoberturas = $this->PfTabelas->PfCoberturas->find('list', ['limit' => 200, 'valueField' => 'nome']);
        // $pf_entidades_grupos = $this->PfTabelas->PfEntidadesGrupos->find('list', ['valueField' => 'nome']);

        $this->set(compact('entidades', 'pfTabela', 'pfProdutos', 'pfOperadoras', 'pfAtendimentos', 'pfAcomodacoes', 'estados', 'pfComercializacoes', 'tiposProdutos',  'pfCoberturas', 'pf_areas_comercializacoes', 'pf_entidades_grupos'));
        $this->set('_serialize', ['pfTabela']);

        $this->loadModel('OdontoOperadoras');
        $odonto_operadoras = $this->OdontoOperadoras->find("list", ['valueField' => 'nome'])->where(['tipo_pessoa' => 'PF']);
        $this->set(compact('odonto_operadoras'));

        if ($new == 1) {
            $this->render('new_add');
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Pf Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $new = null)
    {

        if ($new) {
            $pfTabelasContain = [
                'PfEntidades',
                'PfAreasComercializacoes' => ['PfOperadoras']
            ];
        } else {
            $pfTabelasContain = ['PfEntidades'];
        }
        $pfTabela = $this->PfTabelas->get($id, [
            'contain' => $pfTabelasContain
        ]);

        $ids_vitalmed = [92, 103];
        if (in_array($pfTabela->pf_areas_comercializaco->pf_operadora_id, $ids_vitalmed)) {
            $is_vitalmed = true;
        }
        $this->loadModel('PfEntidades');

        $entidades = $this->PfEntidades->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => '1 = 1', 'order' => 'PfEntidades.nome'])->toArray();
        if ($pfTabela['modalidade'] == 'ADESAO' && !empty($pfTabela['pf_entidades_tabelas'])) {
            foreach ($pfTabela['pf_entidades_tabelas'] as $prof) {
                $entidades_tabela[$prof['pf_entidade']['id']] = $prof['pf_entidade']['nome'];
                unset($entidades[$prof['pf_entidade']['id']]);
                $this->set(compact('entidades_tabela'));
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            (new Collection($this->request->data))->each(function ($value, $key) {
                if (str_starts_with($key, 'faixa')) {
                    $this->request->data[$key] = str_replace('.', ',',  $value);
                }
            });

            if (!empty($this->request->getData('odonto_valor'))) {
                $this->request->data['odonto_valor'] = str_replace('.', ',', $this->request->data['odonto_valor']);
            }
            if ($this->request->data['modalidade'] == 'ADESAO') {
                $this->loadModel('PfEntidadesTabelas');

                $excluir = $this->PfEntidadesTabelas->find('all')->where(['PfEntidadesTabelas.pf_tabela_id' => $id])->toArray();
                if (count($excluir) > 0) {
                    foreach ($excluir as $excluir) {
                        $enttab = $this->PfEntidadesTabelas->get($excluir['id']);
                        $this->PfEntidadesTabelas->delete($enttab);
                    }
                }
            }
            $pfTabela = $this->PfTabelas->patchEntity($pfTabela, $this->request->data);

            if ($this->PfTabelas->save($pfTabela, ['associated' => ['PfEntidades']])) {
                $this->Flash->success('Registro salvo com sucesso.');
                if ($new) {
                    return $this->redirect(['action' => 'new']);
                }
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        } else {
            if (@count($pfTabela['pf_entidades_tabelas']) == 0) {
                $entidades_tabela = '';
            }

            $pfProdutos = $this->PfTabelas->PfProdutos->find('list', ['limit' => 200, 'conditions' => '1 = 1', 'valueField' => 'nome'])->where(['pf_operadora_id' => $pfTabela['pf_operadora_id']]);
            $pfOperadoras = $this->PfTabelas->PfOperadoras->find('list', [
                'valueField' => function ($e) {
                    return $e->get('nome') . ' ' . $e->get('detalhe');
                },
                'conditions' => ['1 = 1'],
                'order' => ['PfOperadoras.nome' => 'ASC'],
                'contain' => ['Estados']
            ]);

            $pf_areas_comercializacoes = $this->PfTabelas->PfAreasComercializacoes
                ->find('list', ['valueField' => function ($q) {
                    return  $q->get('nome') . ' | ' .  $q->pf_operadora->get('nome') . ' - ' . $q->pf_operadora->get('detalhe');
                }])
                ->contain(['PfOperadoras']);
            $pfAtendimentos = $this->PfTabelas->PfAtendimentos->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']])->orderAsc('prioridade');
            $pfAcomodacoes = $this->PfTabelas->PfAcomodacoes->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']]);
            $estados = $this->PfTabelas->Estados->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']]);
            $pfComercializacoes = $this->PfTabelas->PfComercializacoes->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']])->where(['pf_operadora_id' => $pfTabela['pf_operadora_id']]);
            $tiposProdutos = $this->PfTabelas->TiposProdutos->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']]);
            $pfCoberturas = $this->PfTabelas->PfCoberturas->find('list', ['limit' => 200, 'valueField' => 'nome', 'conditions' => ['1 = 1']]);
            $this->set(compact('is_vitalmed', 'pfTabela', 'entidades_tabela', 'entidades', 'pfProdutos', 'pfOperadoras', 'pfAtendimentos', 'pfComercializacoes', 'pfAcomodacoes', 'estados', 'tiposProdutos', 'pfCoberturas', 'pf_areas_comercializacoes', 'ids_vitalmed'));
            $this->set('_serialize', ['pfTabela']);
            $this->loadModel('OdontoOperadoras');
            $odonto_operadoras = $this->OdontoOperadoras->find("list", ['valueField' => 'nome'])->where(['tipo_pessoa' => 'PF']);
            $this->set(compact('odonto_operadoras'));
            // debug($pfTabela);
            // die;

            if ($new == 1) {
                $operadora_id = $pfTabela->pf_areas_comercializaco->pf_operadora->id;
                $pf_formas_pagamentos = $this->PfTabelas->PfFormasPagamentos->find('list', ['valueField' => 'nome'])->where(['pf_operadora_id' => $operadora_id]);
                $pf_documentos = $this->PfTabelas->PfDocumentos->find('list', ['valueField' => 'nome'])->where(['pf_operadora_id' => $operadora_id]);
                $pf_dependentes = $this->PfTabelas->PfDependentes->find('list', ['valueField' => 'nome'])->where(['pf_operadora_id' => $operadora_id]);
                $pf_carencias = $this->PfTabelas->PfCarencias->find('list', ['valueField' => 'nome'])->where(['pf_operadora_id' => $operadora_id]);
                $pf_reembolsos = $this->PfTabelas->PfReembolsos->find('list', ['valueField' => 'nome'])->where(['pf_operadora_id' => $operadora_id]);
                $pf_observacoes = $this->PfTabelas->PfObservacoes->find('list', ['valueField' => 'nome'])->where(['pf_operadora_id' => $operadora_id]);
                $pf_redes = $this->PfTabelas->PfRedes->find('list', ['valueField' => 'nome'])->where(['pf_operadora_id' => $operadora_id]);
                $pf_entidades_grupos = $this->PfTabelas->PfEntidadesGrupos->find('list', ['valueField' => 'nome'])->where(['pf_operadora_id' => $operadora_id]);

                $this->set(compact('pf_formas_pagamentos', 'pf_documentos', 'pf_dependentes', 'pf_carencias', 'pf_reembolsos', 'pf_observacoes', 'pf_redes', 'pf_entidades_grupos'));
                $this->render('new_add');
            } else {
                if ($this->request->is(['ajax']))
                    $this->render('edit_ajax');
                else
                    $this->render('add');
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Pf Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $pfTabela = $this->PfTabelas->get($id);

        $this->loadModel('LinksTabelas');

        $this->LinksTabelas->deleteAll(["pf_tabela_id" => $id]);


        if ($this->PfTabelas->delete($pfTabela)) {
            $this->Flash->success(__('A tabela foi excluida.'));
        } else {
            $this->Flash->error(__('A tabela não foi excluida. Por favor, Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        if (isset($this->request->data['tabelas'])) {
            $ids = $this->request->data();

            //Removendo tabelas não selecionadas
            foreach ($ids as $chave => $valor) {
                if ($valor == 0) {
                    unset($ids[$chave]);
                } else {
                    $t[$valor] = $valor;
                }
            }

            $tabelas = $this->PfTabelas->find('all')
                ->contain([
                    'PfProdutos' => [
                        'PfOperadoras' => [
                            'PfCarencias',
                            'PfRedes',
                            'PfReembolsos',
                            'PfDocumentos',
                            'PfDependentes',
                            'PfObservacoes'
                        ]
                    ],
                    'Estados',
                    'PfAtendimentos',
                    'PfAcomodacoes',
                    'PfComercializacoes'
                ])
                ->order(['PfTabelas.prioridade' => 'ASC'])
                ->where(['PfTabelas.id IN' => $t])
                ->where(['PfTabelas.new' => 1])->toArray();

            $total = count($tabelas->toArray());

            foreach ($tabelas as $tabela) {
                if ($tabela['cod_ans'] != '') {
                    $tabelasOrdenadas[$tabela['produto']['nome']][$tabela['tipo']['nome']][$tabela['cod_ans']] = $tabela;
                } else {
                    $tabelasOrdenadas[$tabela['produto']['nome']][$tabela['tipo']['nome']][] = $tabela;
                }
            }

            $tabelas = $tabelasOrdenadas;


            foreach ($tabelas as $t) {
                foreach ($t as $dados) {
                    foreach ($dados as $info) {
                        $validade = $info['vigencia'];
                        $regiao = $info['regio']['estado']['nome'] . " / " . $info['regio']['nome'];
                        $titulo = $info['operadora']['nome'] . " - PLANO " . $info['modalidade']['nome'] . " " . $info['minimo_vidas'] . " À " . $info['maximo_vidas'] . " VIDAS";
                        $imagem = $info['operadora']['imagen']['caminho'] . $info['operadora']['imagen']['nome'];
                        break;
                    }
                }
            }

            //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
            $tipoPDF = 'D';
            //Nome do PDF
            $nomePDF = $titulo . ".pdf";

            $btnCancelar = ['controller' => 'tabelas', 'action' => 'index'];
            $this->set('tabela', $tabelas);

            $this->set('_serialize', ['tabelas']);

            $this->set(compact('tabelas', 'btnCancelar', 'total', 'nomePDF', 'tipoPDF', 'titulo'));
            $this->viewBuilder()->layout('tabela');
            $this->render('gerar_tabela');
        }

        $tabelas = $this->PfTabelas->find('all', [
            'valueField' => 'nome', 'order' => ['PfTabelas.prioridade' => 'ASC'],
            'contain' => [
                'TiposProdutos',
                'PfProdutos' =>
                ['PfOperadoras' => ['PfCarencias', 'PfRedes', 'PfReembolsos', 'PfDocumentos', 'PfDependentes', 'PfObservacoes']],
                'Estados', 'PfAtendimentos', 'PfAcomodacoes', 'PfComercializacoes'
            ]
        ])
            ->order(['PfTabelas.prioridade' => 'ASC'])
            ->where(['PfTabelas.pf_operadora_id' => $id]);

        $this->set('tabela', $tabelas);
        $this->set(compact('tabelas'));
    }

    /**
     * Método de Validação de Tabela
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function validar($id = null)
    {
        //        debug($id);
        $tabela = $this->PfTabelas->get($id);

        $tabela->validade = 1;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->PfTabelas->save($tabela)) {
            $this->Flash->success(__('Tabela validada com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao validar tabela. Tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    /**
     * Método de Validação de Tabela
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function invalidar($id = null)
    {
        //        debug($this->request->data);
        //        die();
        $tabela = $this->PfTabelas->get($id);

        $tabela->validade = 0;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->PfTabelas->save($tabela)) {
            $this->Flash->success(__('Tabela Invalidada com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao Invalidar tabela. Tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    public function findProdutos($id = null)
    {
        $this->loadModel('PfProdutos');
        if ($id) {
            $pf_produtos = $this->PfProdutos->find('list', ['valueField' => 'nome'])
                ->where(['PfProdutos.pf_operadora_id' => $id])->order(['nome' => 'ASC'])->toArray();
            //            debug($produtos);
        } else {
            $pf_produtos = $this->PfProdutos->find('list', ['valueField' => 'nome'])->order(['nome' => 'ASC'])->toArray();
        }
        //            debug($pf_produtos);
        //        die();
        $this->set(compact('pf_produtos'));

        $this->render('selectProduto');
    }

    public function findRegioes($id = null, $estado = null)
    {
        $this->loadModel('PfComercializacoes');
        if ($id <> '') {
            $pf_comercializacoes = $this->PfComercializacoes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1', 'order' => ['PfComercializacoes.nome' => 'ASC']])
                ->where([
                    'PfComercializacoes.pf_operadora_id' => $id,
                    'PfComercializacoes.estado_id' => $estado
                ])->toArray();
        } else {
            $pf_comercializacoes = $this->PfComercializacoes->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1', 'order' => ['PfComercializacoes.nome' => 'ASC']])->toArray();
        }
        //        die();
        $this->set(compact('pf_comercializacoes'));

        $this->render('selectRegioes');
    }

    /**
     * Delete de múltiplas tabelas de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteLote()
    {
        $tabelas = $this->request->data;
        //        debug($tabelas);
        //        die();
        foreach ($tabelas as $tabela) {
            if ($tabela <> 0) {
                $idEntidade = $this->PfTabelas->get($tabela);
                $id = $this->PfTabelas->get($tabela);
                //                debug($id->id);
                //                die();
                $this->loadModel('PfProfissoesTabelas');
                $prof_tabelas = $this->PfProfissoesTabelas->find('all')->where(['PfProfissoesTabelas.pf_tabela_id' => $id->id])->toArray();
                if (count($prof_tabelas > 0)) {
                    foreach ($prof_tabelas as $prof_tab) {
                        //                        debug($prof_tab);die();
                        $this->PfProfissoesTabelas->delete($prof_tab);
                    }
                }
                $this->loadModel('PfCalculosTabelas');
                $calc_tabelas = $this->PfCalculosTabelas->find('all')->where(['PfCalculosTabelas.pf_tabela_id' => $id->id])->toArray();
                if (count($calc_tabelas > 0)) {
                    foreach ($calc_tabelas as $calc_tab) {
                        $this->PfCalculosTabelas->delete($calc_tab);
                    }
                }
                $this->PfTabelas->delete($idEntidade);
            }
        }

        $this->Flash->success(__('Tabelas excluídas com sucesso'));
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
    }

    /**
     * Método para alterar Prioridade de Tabelas
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function prioridade($id = null)
    {

        if ($this->request->is(['patch', 'post', 'put'])) {
            $tabela = $this->PfTabelas->get($this->request->data['tabela_id']);
            $tabela->prioridade = $this->request->data['prioridade'];
            if ($this->PfTabelas->save($tabela))
                $this->Flash->success('Registro salvo com sucesso.');
            else
                $this->Flash->error(__('Não foi possível salvar o registro. Favor tentar novamente.'));
        } else {
            $tabela_id = $id;
        }
        $this->set(compact('tabela_id'));
    }

    /**
     * Reajuste de múltiplas tabelas de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function reajusteLote()
    {

        $this->set('title', 'Reajustar Tabelas');
        $tabelas = $this->request->data;
        if (isset($this->request->data['percentual'])) {
            foreach ($tabelas as $chave => $tabela) {
                if ($chave <> 'percentual') {
                    if ($tabela <> 0) {
                        $id = $this->PfTabelas->get($tabela);
                        $i = 1;
                        for ($i = 1; $i <= 12; $i++) {
                            $faixa = 'faixa' . $i;
                            $id->$faixa = round($id->$faixa + (($tabelas['percentual'] / 100) * $id->$faixa), 2);
                        }
                        $id->validade = 0;
                        $this->PfTabelas->save($id);
                    }
                }
            }
            $this->Flash->success(__('Tabelas Reajustadas com sucesso.'));

            return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
        }
        $this->set(compact('tabelas'));
    }

    /**
     * Reajuste de Vigência tabelas de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function reajusteVigencia()
    {

        $this->set('title', 'Reajustar Tabelas');

        //        die();
        $tabelas = $this->request->data;
        if ($this->request->is(['post', 'put'])) {

            if (isset($this->request->data['vigencia'])) {
                $vigencia = $tabelas['vigencia'];
                unset($tabelas['vigencia']);
                foreach ($tabelas as $chave => $tabela) {
                    if (!empty($tabela) && $tabela <> '') {
                        //                debug($tabela);
                        $id = $this->PfTabelas->get($tabela);
                        $id->vigencia = $vigencia['year'] . "-" . $vigencia['month'] . "-" . $vigencia['day'];

                        $this->PfTabelas->save($id);
                    }
                }
                $this->Flash->success(__('Vigência de Tabelas alteradas com sucesso.'));


                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPf', 'destino' =>  $this->request->controller]);
            }
        }
        $this->set(compact('tabelas'));
    }

    public function filtroEstado()
    {
        $this->autoRender = false;
        $this->loadModel("PfOperadoras");
        $estado = $this->request->data('estado');

        $operadoras = $this->PfOperadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        //         $this->set(compact("operadoras"));
        $this->response->type('json');
        $this->response->body(json_encode($operadoras));
    }

    public function valorPercentual()
    {
        $this->loadModel('PfOperadoras');
        $ids_estados = $this->PfOperadoras->find('list', ['keyValue' => 'estado_id', 'valueField' => 'estado_id', 'conditions' => '1 = 1'])->distinct(['estado_id'])->toArray();

        $estados = $this->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $ids_estados])->toArray();

        $pfoperadoras = $this->PfOperadoras->find(
            'list',
            [
                'valueField' => function ($e) {
                    return $e->get('nome') . ' ' . $e->get('detalhe');
                },
                'conditions' => ['1 = 1'],
                'order' => ['nome' => 'ASC']
            ]
        );
        $pftabelas = $this->PfTabelas->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('minimo_vidas') . ' a ' . $e->get('maximo_vidas');
        }, 'conditions' => ['1 = 1'], 'order' => ['nome' => 'ASC']]);
        $pfacomodacoes = $this->PfTabelas->PfAcomodacoes->find('list', ['valueField' => 'nome']);
        // debug($pfacomodacoes);die;
        $this->set(compact('pfoperadoras', 'pftabelas', 'estados', 'pfacomodacoes'));
    }

    public function buscaTabela()
    {
        $dados = $this->request->data();
        $pftabelas = $this->PfTabelas->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('minimo_vidas') . ' a ' . $e->get('maximo_vidas') . ' vidas';
        }, 'conditions' => ['1 = 1'], 'order' => ['PfTabelas.nome' => 'ASC'], 'contain' => ['PfProdutos', 'PfOperadoras', 'PfAtendimentos', 'PfAcomodacoes']])
            ->where([
                'PfTabelas.pf_operadora_id' => !empty($dados['pf_operadora_id']) ? $dados['pf_operadora_id'] : $dados['operadora_id'],
                'PfTabelas.pf_acomodacao_id' => $dados['pf_acomodacao_id'],
                'PfTabelas.minimo_vidas >=' => $dados['minimo_vidas'],
                'PfTabelas.maximo_vidas <=' => $dados['maximo_vidas'],
                'PfTabelas.coparticipacao' => $dados['coparticipacao'],
            ])->toArray();
        //            debug($tabelas);
        $this->set(compact('pftabelas'));
    }

    public function editarPercentual()
    {
        $dados = $this->request->data;
        $percentual = $dados['percentual'];
        foreach ($dados['Tabelas'] as $secundaria) {
            if ($secundaria !== 'false') {

                $pf_tabela_secundaria = $this->PfTabelas->get($secundaria);
                $percentual = str_replace(",", ".", $percentual);
                $dados['faixa1'] = str_replace(",", ".", $dados['faixa1']);
                $dados['faixa2'] = str_replace(",", ".", $dados['faixa2']);
                $dados['faixa3'] = str_replace(",", ".", $dados['faixa3']);
                $dados['faixa4'] = str_replace(",", ".", $dados['faixa4']);
                $dados['faixa5'] = str_replace(",", ".", $dados['faixa5']);
                $dados['faixa6'] = str_replace(",", ".", $dados['faixa6']);
                $dados['faixa7'] = str_replace(",", ".", $dados['faixa7']);
                $dados['faixa8'] = str_replace(",", ".", $dados['faixa8']);
                $dados['faixa9'] = str_replace(",", ".", $dados['faixa9']);
                $dados['faixa10'] = str_replace(",", ".", $dados['faixa10']);
                $dados['faixa11'] = str_replace(",", ".", $dados['faixa11']);
                $dados['faixa12'] = str_replace(",", ".", $dados['faixa12']);

                $pf_tabela_secundaria->faixa1 = round($pf_tabela_secundaria->faixa1 + ($pf_tabela_secundaria->faixa1 * ($percentual / 100)), 2);

                $pf_tabela_secundaria->faixa2 = round($pf_tabela_secundaria->faixa1 + ($pf_tabela_secundaria->faixa1 * ($dados['faixa2'] / 100)), 2);

                $pf_tabela_secundaria->faixa3 = round($pf_tabela_secundaria->faixa2 + ($pf_tabela_secundaria->faixa2 * ($dados['faixa3']) / 100), 2);

                $pf_tabela_secundaria->faixa4 = round($pf_tabela_secundaria->faixa3 + ($pf_tabela_secundaria->faixa3 * ($dados['faixa4']) / 100), 2);

                $pf_tabela_secundaria->faixa5 = round($pf_tabela_secundaria->faixa4 + ($pf_tabela_secundaria->faixa4 * ($dados['faixa5']) / 100), 2);

                $pf_tabela_secundaria->faixa6 = round($pf_tabela_secundaria->faixa5 + ($pf_tabela_secundaria->faixa5 * ($dados['faixa6']) / 100), 2);

                $pf_tabela_secundaria->faixa7 = round($pf_tabela_secundaria->faixa6 + ($pf_tabela_secundaria->faixa6 * ($dados['faixa7']) / 100), 2);

                $pf_tabela_secundaria->faixa8 = round($pf_tabela_secundaria->faixa7 + ($pf_tabela_secundaria->faixa7 * ($dados['faixa8']) / 100), 2);

                $pf_tabela_secundaria->faixa9 = round($pf_tabela_secundaria->faixa8 + ($pf_tabela_secundaria->faixa8 * ($dados['faixa9']) / 100), 2);

                $pf_tabela_secundaria->faixa10 = round($pf_tabela_secundaria->faixa9 + ($pf_tabela_secundaria->faixa9 * ($dados['faixa10']) / 100), 2);

                $pf_tabela_secundaria->faixa11 = round($pf_tabela_secundaria->faixa9 + ($pf_tabela_secundaria->faixa9 * ($dados['faixa10']) / 100), 2);

                $pf_tabela_secundaria->faixa12 = round($pf_tabela_secundaria->faixa9 + ($pf_tabela_secundaria->faixa9 * ($dados['faixa10']) / 100), 2);

                $this->PfTabelas->save($pf_tabela_secundaria);

                $this->set(compact('pf_tabela_secundaria'));
            }
        }
    }
    public function selectOperadoras()
    {
        $this->autoRender = false;
        $this->loadModel("PfOperadoras");
        $estado = $this->request->data['estado'];
        $operadoras = $this->PfOperadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }])
            ->matching('PfAreasComercializacoes.PfAreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                return $q->where(['PfAreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
            })
            ->where(["nova" => 1])
            ->order(["PfOperadoras.nome" => "ASC"])
            ->toArray();
        $this->response->type('json');
        $this->response->body(json_encode($operadoras));
    }

    public function duplicar($tabela)
    {
        if (isset($tabela)) {
            $tabela_base = $this->PfTabelas->get($tabela)->toArray();

            $tabela_duplicata = $this->PfTabelas->newEntity();
            $tabela_duplicata = $this->PfTabelas->patchEntity($tabela_duplicata, $tabela_base);
            $tabela_duplicata->validade = 0;
            $tabela_duplicata->new = 1;
            $tabela_duplicata = $this->PfTabelas->save($tabela_duplicata);
            if ($tabela_duplicata) {
                $this->Flash->success('Tabela Duplicada com sucesso.');
            } else {
                $this->Flash->error('Erro ao duplicar a tabela');
            }
            $this->redirect(['action' => 'index']);
        }
    }

    public function inativaPorVigencia()
    {
        $tabelasInvalidas = $this->PfTabelas->find('list')->where('NOW() > vigencia AND atualizacao = 1');
        foreach ($tabelasInvalidas as $tabela) {
            $tabela = $this->PfTabelas->get($tabela);
            $tabela->validade = 0;
            $this->PfTabelas->save($tabela);
        }
        $this->autoRender = false;
    }

    /**
     * Método de colocar Tabela em Atualização
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function colocarAtlz($id = null)
    {
        $tabela = $this->PfTabelas->get($id);

        $tabela->atualizacao = 1;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->PfTabelas->save($tabela)) {
            $this->Flash->success(__('Tabela Invalidada com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao Invalidar tabela. Tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    /**
     * Método de remover Tabela em Atualização
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function removerAtlz($id = null)
    {
        $tabela = $this->PfTabelas->get($id);

        $tabela->atualizacao = 0;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->PfTabelas->save($tabela)) {
            $this->Flash->success(__('Tabela Invalidada com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao Invalidar tabela. Tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $operadoras = $this->PfTabelas->PfOperadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }])
            ->where([
                'estado_id' => $estado,
                'NOT' => 'status <=> "INATIVA"'
            ])
            ->orderASC('PfOperadoras.nome')
            ->toArray();

        // Produtos Filtrados
        $conditions[] = ['PfOperadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['PfOperadoras.id' => $operadora];
        }

        $this->paginate = [
            'valueField' => 'nome', 'order' => ['PfTabelas.prioridade' => 'ASC'],
            'contain' => [
                'PfCoberturas',
                'PfProdutos' =>
                ['PfOperadoras' => ['Imagens', 'PfCarencias', 'PfRedes', 'PfReembolsos', 'PfDocumentos', 'PfDependentes', 'PfObservacoes']],
                'Estados', 'PfAtendimentos', 'PfAcomodacoes', 'PfComercializacoes'
            ],
            'order' => [
                'PfTabelas.validade' => 'DESC',
                'PfTabelas.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $tabelas = $this->paginate($this->PfTabelas);

        $estados = $this->PfTabelas->PfOperadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->PfTabelas->PfOperadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);

        $this->set(compact('operadoras', 'estados', 'operadora', 'estado', 'tabelas', 'sessao'));
    }
    public function filtroAssociacoes(int $area_comercializacao = null)
    {
        $this->loadModel('PfAreasComercializacoes');
        $area_comercializacao = $this->PfAreasComercializacoes->get($area_comercializacao);
        $this->loadModel('PfOperadoras');
        $operadora = $this->PfOperadoras->get($area_comercializacao->pf_operadora_id, [
            'contain' => ['PfRedes', 'PfObservacoes', 'PfReembolsos', 'PfCarencias', 'PfDependentes', 'PfDocumentos', 'PfFormasPagamentos', 'PfEntidadesGrupos']
        ]);
        $pf_formas_pagamentos = (new \Cake\Collection\Collection($operadora->pf_formas_pagamentos))->combine('id', 'nome');
        $pf_documentos = (new \Cake\Collection\Collection($operadora->pf_documentos))->combine('id', 'nome');
        $pf_dependentes = (new \Cake\Collection\Collection($operadora->pf_dependentes))->combine('id', 'nome');
        $pf_carencias = (new \Cake\Collection\Collection($operadora->pf_carencias))->combine('id', 'nome');
        $pf_reembolsos = (new \Cake\Collection\Collection($operadora->pf_reembolsos))->combine('id', 'nome');
        $pf_observacoes = (new \Cake\Collection\Collection($operadora->pf_observacoes))->combine('id', 'nome');
        $pf_redes = (new \Cake\Collection\Collection($operadora->pf_redes))->combine('id', 'nome');
        $pf_entidades_grupos = (new \Cake\Collection\Collection($operadora->pf_entidades_grupos))->combine('id', 'nome');

        $this->set(compact('pf_formas_pagamentos', 'pf_documentos', 'pf_dependentes', 'pf_carencias', 'pf_reembolsos', 'pf_observacoes', 'pf_redes', 'pf_entidades_grupos'));
    }

    public function getFaixas(int $area)
    {
        $area = $this->PfTabelas->PfAreasComercializacoes->get($area)->pf_operadora_id;
        $is_vitalmed = false;

        // ID ESPECÍFICO DA VITALMED
        $ids_vitalmed = [92, 103];
        if (in_array($area, $ids_vitalmed)) {
            $is_vitalmed = true;
        }

        $this->set('is_vitalmed', $is_vitalmed);
    }
}
