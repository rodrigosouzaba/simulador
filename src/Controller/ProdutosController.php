<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Produtos Controller
 *
 * @property \App\Model\Table\ProdutosTable $Produtos
 */
class ProdutosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $operadoras = $this->Produtos->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $ids_estados = $this->Produtos->Operadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->Produtos->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $ids_estados]);
        $this->set(compact('operadoras', 'estados'));
    }

    /**
     * View method
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Detalhes do Produto');
        $produto = $this->Produtos->get($id, [
            'contain' => ['Operadoras', 'Tabelas']
        ]);

        $this->set('produto', $produto);
        $this->set('_serialize', ['produto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $produto = $this->Produtos->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['nome'] = rtrim($this->request->data['nome']);

            $testProduto = $this->Produtos->find('list')
                ->where([
                    'Produtos.nome' => $this->request->data['nome'],
                    'Produtos.operadora_id' => $this->request->data['operadora_id']
                ])->toArray();

            if (!$testProduto) {
                $produto = $this->Produtos->patchEntity($produto, $this->request->data);
                if ($this->Produtos->save($produto)) {
                    $this->Flash->success(__('Produto salvo com sucesso.'));

                    return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
                } else {
                    $this->Flash->error(__('Erro ao salvar o produto. Tente novamente.'));
                }
            } else {

                $this->Flash->error(__('Já existe um PRODUTO cadastrado com este nome.'));
            }
        }

        //        $carencias = $this->Produtos->Carencias->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);
        //        $reembolsos = $this->Produtos->Reembolsos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);
        //        $redes = $this->Produtos->Redes->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);
        //        $opcionais = $this->Produtos->Opcionais->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);
        //        $informacoes = $this->Produtos->Informacoes->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);
        //        $observacoes = $this->Produtos->Observacoes->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);
        $operadoras = $this->Produtos->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'conditions' => '1 = 1 and NOT (Operadoras.status <=> \'INATIVA\')',
            'contain' => ['Estados']
        ]);

        $this->set(compact('produto', 'operadoras', 'tipos_produtos'));
        $this->set('_serialize', ['produto']);
    }

    /**
     * Método que retorna informações de Carências, Info, Opcionais .... referente a operadora selecionada.
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function findInfo($id = null)
    {
        $this->loadModel('Carencias');
        $this->loadModel('Reembolsos');
        $this->loadModel('Redes');
        $this->loadModel('Informacoes');
        $this->loadModel('Opcionais');
        $this->loadModel('Observacoes');
        if ($id) {
            $carencias = $this->Carencias->find('list', ['valueField' => 'nome', 'order' => ['Carencias.nome' => 'ASC']])
                ->where(['Carencias.operadora_id' => $id])->toArray();
            $reembolsos = $this->Reembolsos->find('list', ['valueField' => 'nome', 'order' => ['Reembolsos.nome' => 'ASC']])
                ->where(['Reembolsos.operadora_id' => $id])->toArray();
            $redes = $this->Redes->find('list', ['valueField' => 'nome', 'order' => ['Redes.nome' => 'ASC']])
                ->where(['Redes.operadora_id' => $id])->toArray();
            $informacoes = $this->Informacoes->find('list', ['valueField' => 'nome', 'order' => ['Informacoes.nome' => 'ASC']])
                ->where(['Informacoes.operadora_id' => $id])->toArray();
            $opcionais = $this->Opcionais->find('list', ['valueField' => 'nome', 'order' => ['Opcionais.nome' => 'ASC']])
                ->where(['Opcionais.operadora_id' => $id])->toArray();
            $observacoes = $this->Observacoes->find('list', ['valueField' => 'nome', 'order' => ['Observacoes.nome' => 'ASC']])
                ->where(['Observacoes.operadora_id' => $id])->toArray();
            //            debug($produtos);
        } else {
            $carencias = $this->Carencias->find('list', ['valueField' => 'nome', 'order' => ['Carencias.nome' => 'ASC']])->toArray();
            $reembolsos = $this->Reembolsos->find('list', ['valueField' => 'nome', 'order' => ['Reembolsos.nome' => 'ASC']])->toArray();
            $redes = $this->Redes->find('list', ['valueField' => 'nome', 'order' => ['Redes.nome' => 'ASC']])->toArray();
            $informacoes = $this->Informacoes->find('list', ['valueField' => 'nome', 'order' => ['Informacoes.nome' => 'ASC']])->toArray();
            $opcionais = $this->Opcionais->find('list', ['valueField' => 'nome', 'order' => ['Opcionais.nome' => 'ASC']])->toArray();
            $observacoes = $this->Observacoes->find('list', ['valueField' => 'nome', 'order' => ['Observacoes.nome' => 'ASC']])->toArray();
            //            debug($produtos);
        }
        //        die();
        $this->set(compact('carencias', 'reembolsos', 'redes', 'informacoes', 'opcionais', 'observacoes'));

        $this->render('infoAdicional');
    }

    /**
     * Edit method
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $produto = $this->Produtos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $produto = $this->Produtos->patchEntity($produto, $this->request->data);
            if ($this->Produtos->save($produto)) {
                $this->Flash->success(__('Produto salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao salvar o produto. Tente novamente.'));
            }
        }

        $tipos_produtos = $this->Produtos->TiposProdutos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);

        $operadoras = $this->Produtos->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $this->set(compact('produto', 'tipos_produtos', 'operadoras'));
        $this->set('_serialize', ['produto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->loadModel('Tabelas');
        $tabelas = $this->Tabelas->find('list')->where(['Tabelas.produto_id' => $id]);

        if (empty($tabelas->toArray())) {
            $this->request->allowMethod(['post', 'delete']);
            $produto = $this->Produtos->get($id);
            if ($this->Produtos->delete($produto)) {
                $this->Flash->success(__('Produto Excluído com sucesso.'));
            } else {
                $this->Flash->error(__('Erro ao excluir o Produto. Tente Novamente.'));
            }
        } else {
            $this->Flash->error(__('Existem Tabelas vinculadas a este Produto. Desvincule as Tabelas ao produto e tente Novamente.'));
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        //        debug($id);
        $produtos = $this->Produtos->find('all', ['contain' => ['Operadoras' => ['Imagens'], 'TiposProdutos']])
            ->where(['Produtos.operadora_id' => $id])
            ->order('Produtos.nome')->toArray();
        $operadoras = $this->Produtos->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $this->set(compact('produtos', 'operadoras'));
        $this->set('_serialize', ['produtos']);
    }

    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function updateProdutos()
    {
        //        debug($id);
        $this->loadModel('Tabelas');

        $dados = $this->Tabelas->find()->select(['produto_id'])
            ->toArray();
        $produto = $this->Produtos->newEntity();
        foreach ($dados as $dado) {
            $produto['id'] = $dado['produto_id'];

            $this->Produtos->save($produto);
        }

        $produtos = $this->Produtos->find('all', ['contain' => ['Operadoras' => ['Imagens']]])
            ->where(['Produtos.operadora_id' => $id])
            ->order('Produtos.nome')->toArray();
        //        debug($produtos);die();
        $operadoras = $this->Produtos->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $this->set(compact('produtos', 'operadoras'));
        $this->set('_serialize', ['produtos']);
    }

    /**
     * Delete de múltiplos produtos de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteLote()
    {
        $produtos = $this->request->data;
        foreach ($produtos as $chave => $valor) {
            if ($valor == 0) {
                unset($produtos[$chave]);
            }
        }
        $this->Produtos->deleteAll(['id IN' => $produtos]);

        $this->Flash->success(__(count($produtos) . ' Produtos excluídos com sucesso'));
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    public function filtroEstado()
    {
        $estado = $this->request->data()['estados'];
        $operadoras = $this->Produtos->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->order(['nome' => 'ASC']);
        $this->set(compact('operadoras'));
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {
        $operadoras = $this->Produtos->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status' => ''],
                    ['estado_id' => $estado, 'status IS' => null]
                ],
                'NOT' => 'status <=> "INATIVA"',
                '1 = 1'
            ])
            ->orderASC('Operadoras.nome')
            ->toArray();

        if ($operadora != null) {
            $filtradas = $this->Global->filtroOperadora($operadora, 'operadora_id', 'Operadoras', 'Produtos');
            $pf_operadoras = $this->Produtos->Operadoras->find('list', ['limit' => 200, 'valueField' => 'nome']);
            $this->set(compact('filtradas', 'operadoras'));
        }

        $estados = $this->Produtos->Operadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->Produtos->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'estados', 'operadora', 'estado'));
    }
}
