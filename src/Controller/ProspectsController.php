<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Prospects Controller
 *
 * @property \App\Model\Table\ProspectsTable $Prospects
 */
class ProspectsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $prospects = $this->paginate($this->Prospects);

        $this->set(compact('prospects'));
        $this->set('_serialize', ['prospects']);
    }

    /**
     * View method
     *
     * @param string|null $id Prospect id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $prospect = $this->Prospects->get($id, [
            'contain' => []
        ]);

        $this->set('prospect', $prospect);
        $this->set('_serialize', ['prospect']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $prospect = $this->Prospects->newEntity();
        if ($this->request->is('post')) {
            $prospect = $this->Prospects->patchEntity($prospect, $this->request->data);
            if ($this->Prospects->save($prospect)) {
                $this->Flash->success(__('The prospect has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The prospect could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('prospect'));
        $this->set('_serialize', ['prospect']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Prospect id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $prospect = $this->Prospects->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $prospect = $this->Prospects->patchEntity($prospect, $this->request->data);
            if ($this->Prospects->save($prospect)) {
                $this->Flash->success(__('The prospect has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The prospect could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('prospect'));
        $this->set('_serialize', ['prospect']);
        $this->render("add");
    }

    /**
     * Delete method
     *
     * @param string|null $id Prospect id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $prospect = $this->Prospects->get($id);
        if ($this->Prospects->delete($prospect)) {
            $this->Flash->success(__('The prospect has been deleted.'));
        } else {
            $this->Flash->error(__('The prospect could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
