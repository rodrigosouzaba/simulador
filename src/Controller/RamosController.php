<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ramos Controller
 *
 * @property \App\Model\Table\RamosTable $Ramos
 */
class RamosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $ramos = $this->paginate($this->Ramos);

        $this->set(compact('ramos'));
        $this->set('_serialize', ['ramos']);
    }

    /**
     * View method
     *
     * @param string|null $id Ramo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ramo = $this->Ramos->get($id, [
            'contain' => ['Tabelas']
        ]);

        $this->set('ramo', $ramo);
        $this->set('_serialize', ['ramo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ramo = $this->Ramos->newEntity();
        if ($this->request->is('post')) {
            
            $ramo = $this->Ramos->patchEntity($ramo, $this->request->data);
         
            if ($this->Ramos->save($ramo)) {
                $this->Flash->success(__('Ramo salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar Ramo. Tente novamente.'));
            }
        }
        $this->set(compact('ramo'));
        $this->set('_serialize', ['ramo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ramo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ramo = $this->Ramos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ramo = $this->Ramos->patchEntity($ramo, $this->request->data);
            if ($this->Ramos->save($ramo)) {
                $this->Flash->success(__('Ramo salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar Ramo. Tente novamente.'));
            }
        }
        $this->set(compact('ramo'));
        $this->set('_serialize', ['ramo']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Ramo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ramo = $this->Ramos->get($id);
        if ($this->Ramos->delete($ramo)) {
            $this->Flash->success(__('Ramo excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir Ramo. Tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
