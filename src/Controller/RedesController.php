<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Redes Controller
 *
 * @property \App\Model\Table\RedesTable $Redes
 */
class RedesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            $conditions_operadoras = [
                'estado_id' => $estado,

            ];
            if ($nova) {
                $operadoras = $this->Redes->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            } else {
                $operadoras = $this->Redes->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'Operadoras.estado_id' => $estado, 'nova' => 0], 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['Operadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['Operadoras.nova'] =  1;
                $redes = $this->Redes
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->matching('Operadoras.AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('Redes.id')
                    ->orderAsc('Operadoras.prioridade');
            } else {
                $conditions['Operadoras.estado_id'] = $estado;
                $conditions['OR'] = ['Operadoras.nova' => 0, 'Operadoras.nova IS NULL'];
                $redes = $this->Redes
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->where($conditions)
                    ->orderAsc('Operadoras.prioridade');
            }
        } else {
            $redes = $this->Redes
                ->find('all')
                ->contain(['Operadoras'])
                ->orderAsc('Operadoras.prioridade');
        }

        $redes = $this->paginate($redes);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('AreasComercializacoes.Operadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'operadoras', 'alias' => 'Operadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = Operadoras.estado_id']]);
            }
        }
        $this->set(compact('redes', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }


    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        //        debug($id);
        $redes = $this->Redes->find('all', ['contain' => ['Operadoras' => ['Imagens']]])
            ->where(['Redes.operadora_id' => $id])
            ->order('Redes.nome')->toArray();
        //        debug($produtos);die();
        $operadoras = $this->Redes->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ])->where(['OR' => ['nova is NULL', 'nova' => '']]);
        $this->set(compact('redes', 'operadoras'));
        $this->set('_serialize', ['redes']);
    }

    /**
     * View method
     *
     * @param string|null $id Rede id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rede = $this->Redes->get($id, [
            'contain' => ['Tabelas']
        ]);

        $this->set('rede', $rede);
        $this->set('_serialize', ['rede']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nova Rede Credenciada');
        $rede = $this->Redes->newEntity();
        if ($this->request->is('post')) {
            $rede = $this->Redes->patchEntity($rede, $this->request->data);
            if ($this->Redes->save($rede)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente.'));
            }
        }
        $operadoras = $this->Redes->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ])->where(['OR' => ['nova is NULL', 'nova' => ''], 'NOT' => ['status <=>' => 'INATIVA']]);

        $this->set(compact('rede', 'operadoras'));
        $this->set('_serialize', ['rede']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rede id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Editar Rede Credenciada');

        $rede = $this->Redes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rede = $this->Redes->patchEntity($rede, $this->request->data);
            if ($this->Redes->save($rede)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente.'));
            }
        }
        $operadoras = $this->Redes->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ])->where(['OR' => ['nova is NULL', 'nova' => ''], 'NOT' => ['status <=>' => 'INATIVA']]);

        $this->set(compact('rede', 'operadoras'));
        $this->set('_serialize', ['rede']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Rede id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rede = $this->Redes->get($id);
        if ($this->Redes->delete($rede)) {
            $this->Flash->success(__('Excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao Excluir. Tente Novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    public function filtroEstado($estado = null)
    {
        $operadoras = $this->Redes->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact('operadoras'));
    }

    public function pesquisar($palavra, $estado)
    {
        if ($this->request->isGET()) {
            $palavra = preg_replace('/( ){2,}/', ' ', $palavra);
            $redes = $this->Redes->find('all')->where(['descricao LIKE' => '%' . $palavra . '%', "Operadoras.estado_id" => $estado, "Operadoras.status" <> "INATIVA"])->contain(['Operadoras' => ['Imagens']]);
            $this->set(compact('redes', 'palavra'));
        }
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado, $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $operadoras = $this->Redes->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status' => ''],
                    ['estado_id' => $estado, 'status IS' => null]
                ],
                'NOT' => 'status <=> "INATIVA"',
                '1 = 1'
            ])
            ->orderASC('Operadoras.nome')
            ->toArray();

        $conditions[] = ['Operadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['Operadoras.id' => $operadora];
        }
        $this->paginate = [
            'valueField' => 'nome', 'order' => ['Tabelas.prioridade' => 'ASC'],
            'contain' => [
                'Operadoras'
            ],
            'order' => [
                'Tabelas.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $redes = $this->paginate($this->Redes);

        $estados = $this->Redes->Operadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->Redes->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('redes', 'operadoras', 'estados', 'operadora', 'sessao', 'estado'));
    }

    function filtroNova($nova)
    {
        $conditions = ['NOT' => ['status <=>' => 'inativa']];
        if ($nova == 1) {
            $conditions[] = ['Operadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['Operadoras.nova IS NULL', 'Operadoras.nova' => 0];
        }
        $this->loadModel('Operadoras');
        $operadoras = $this->Operadoras->find('list', [
            'valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }
}
