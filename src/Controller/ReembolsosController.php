<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Reembolsos Controller
 *
 * @property \App\Model\Table\ReembolsosTable $Reembolsos
 */
class ReembolsosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($nova = null, $estado = null,  $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if (!is_null($estado) && !is_null($nova)) {
            $conditions_operadoras = [
                'estado_id' => $estado,

            ];
            if ($nova) {
                $operadoras = $this->Reembolsos->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->matching('AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            } else {
                $operadoras = $this->Reembolsos->Operadoras
                    ->find('list', ['valueField' => function ($e) {
                        return $e->get('nome') . " " . $e->get('detalhe');
                    }])
                    ->where(['OR' => ['nova IS NULL', 'Operadoras.estado_id' => $estado, 'nova' => 0], 'NOT' => 'status <=> "INATIVA"'])
                    ->orderASC('Operadoras.nome');
            }
        }

        if (!is_null($operadora)) {
            $conditions['Operadoras.id'] = $operadora;
        }
        if (!is_null($nova) && !is_null($estado)) {
            if ($nova) {
                $conditions['Operadoras.nova'] =  1;
                $reembolsos = $this->Reembolsos
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->matching('Operadoras.AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                        return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                    })
                    ->where($conditions)
                    ->group('Reembolsos.id')
                    ->orderAsc('Operadoras.prioridade');
            } else {
                $conditions['Operadoras.estado_id'] = $estado;
                $conditions['OR'] = ['Operadoras.nova' => 0, 'Operadoras.nova IS NULL'];
                $reembolsos = $this->Reembolsos
                    ->find('all')
                    ->contain(['Operadoras'])
                    ->where($conditions)
                    ->orderAsc('Operadoras.prioridade');
            }
        } else {
            $reembolsos = $this->Reembolsos
                ->find('all')
                ->contain(['Operadoras'])
                ->orderAsc('Operadoras.prioridade');
        }

        $reembolsos = $this->paginate($reembolsos);
        if (!is_null($nova)) {
            $this->loadModel('Estados');
            if ($nova) {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('AreasComercializacoes.Operadoras');
            } else {
                $estados = $this->Estados
                    ->find('list', ['valueField' => 'nome'])
                    ->join(['table' => 'operadoras', 'alias' => 'Operadoras', 'type' => 'INNER', 'conditions' => ['Estados.id = Operadoras.estado_id']]);
            }
        }
        $this->set(compact('reembolsos', 'operadoras', 'estados', 'sessao', 'estado', 'nova', 'operadora'));
    }


    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {
        //        debug($id);
        $reembolsos = $this->Reembolsos->find('all', ['contain' => ['Operadoras' => ['Imagens']]])
            ->where(['Reembolsos.operadora_id' => $id])
            ->order('Reembolsos.nome')->toArray();
        //        debug($produtos);die();
        $operadoras = $this->Reembolsos->Operadoras->find('list', [
            'valueField' => function ($e) {
                if (!is_null($e->estado))
                    return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $this->set(compact('reembolsos', 'operadoras'));
        $this->set('_serialize', ['reembolsos']);
    }

    /**
     * View method
     *
     * @param string|null $id Reembolso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reembolso = $this->Reembolsos->get($id, [
            'contain' => ['Tabelas']
        ]);

        $this->set('reembolso', $reembolso);
        $this->set('_serialize', ['reembolso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Novo Reembolso');
        $reembolso = $this->Reembolsos->newEntity();
        if ($this->request->is('post')) {
            $reembolso = $this->Reembolsos->patchEntity($reembolso, $this->request->data);
            if ($this->Reembolsos->save($reembolso)) {
                $this->Flash->success(__('Salvo com Sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente'));
            }
        }
        $operadoras = $this->Reembolsos->Operadoras->find('list', [
            'valueField' => function ($e) {
                if (!is_null($e->estado))
                    return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['NOT' => ['status <=>' => 'inativa']],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);

        $this->set(compact('reembolso', 'operadoras'));
        $this->set('_serialize', ['reembolso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Reembolso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Editar Reembolso');
        $reembolso = $this->Reembolsos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reembolso = $this->Reembolsos->patchEntity($reembolso, $this->request->data);
            if ($this->Reembolsos->save($reembolso)) {
                $this->Flash->success(__('Alterado com Sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao alterar. Tente Novamente'));
            }
        }
        $operadoras = $this->Reembolsos->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);

        $this->set(compact('reembolso', 'operadoras'));
        $this->set('_serialize', ['reembolso']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Reembolso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reembolso = $this->Reembolsos->get($id);
        if ($this->Reembolsos->delete($reembolso)) {
            $this->Flash->success(__('Excluído com Sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao Excluir. Tente Novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }
    public function filtroEstado()
    {
        $estado = $this->request->data()['estados'];
        $operadoras = $this->Reembolsos->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                    ['estado_id' => $estado, 'status IS' => null]
                ]
            ])
            ->orderASC('nome')
            ->toArray();
        $this->set(compact('operadoras'));
    }

    public function filtro($estado, $operadora = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $operadoras = $this->Reembolsos->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status' => ''],
                    ['estado_id' => $estado, 'status IS' => null]
                ],
                'NOT' => 'status <=> "INATIVA"',
                '1 = 1'
            ])
            ->orderASC('Operadoras.nome')
            ->toArray();

        $conditions[] = ['Operadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['Operadoras.id' => $operadora];
        }
        $this->paginate = [
            'valueField' => 'nome', 'order' => ['Tabelas.prioridade' => 'ASC'],
            'contain' => [
                'Operadoras'
            ],
            'order' => [
                'Tabelas.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $reembolsos = $this->paginate($this->Reembolsos);

        $estados = $this->Reembolsos->Operadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->Reembolsos->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('reembolsos', 'operadoras', 'estados', 'operadora', 'sessao', 'estado'));
    }

    function filtroNova($nova)
    {
        $conditions = ['NOT' => ['status <=>' => 'inativa']];
        if ($nova == 1) {
            $conditions[] = ['Operadoras.nova' => 1];
        } else {
            $conditions[]['OR'] = ['Operadoras.nova IS NULL', 'Operadoras.nova' => 0];
        }
        $this->loadModel('Operadoras');
        $operadoras = $this->Operadoras->find('list', [
            'valueField' => function ($q) {
                return $q->get('nome') . ' - ' . $q->get('detalhe');
            }
        ])->where($conditions)->orderAsc('nome');

        $this->set('operadoras', $operadoras);
    }
}
