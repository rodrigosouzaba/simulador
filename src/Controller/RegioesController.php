<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Regioes Controller
 *
 * @property \App\Model\Table\RegioesTable $Regioes
 */
class RegioesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $this->set('title', 'Áreas de Comercialização');
        $conditions = [];

        if ($this->request->is('ajax')) {
            if (!empty($this->request->data('estados')) || !empty($this->request->query('estados'))) {
                $estado = !empty($this->request->data('estados')) ? $this->request->data('estados') : $this->request->query('estados');
                $conditions[] = ['Regioes.estado_id' => $estado];
            }
        }

        $regioes = $this->Regioes->find('all')
            ->contain(['Estados', 'Operadoras'])
            ->where($conditions);

        $regioes = $this->paginate($regioes);

        $ids_estados = $this->Regioes->find('list', ['valueField' => 'estado_id'])->toArray();
        $estados = $this->Regioes->Estados->find('list', ['valueField' => 'nome'])->where(["id IN" => $ids_estados]);

        $this->set(compact('regioes', 'estados'));
        $this->set('_serialize', ['regioes']);
    }

    /**
     * View method
     *
     * @param string|null $id Regio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $regio = $this->Regioes->get($id, [
            'contain' => ['Estados', 'Operadoras']
        ]);

        $this->set('regio', $regio);
        $this->set('_serialize', ['regio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nova Área de Comercialização');

        $regio = $this->Regioes->newEntity();
        if ($this->request->is('post')) {
            $regio = $this->Regioes->patchEntity($regio, $this->request->data);
            $escolhidos = $this->request->data['lista_municipios_escolhidos'];
            $municipios = array();
            foreach ($escolhidos as $municipio) {
                array_push($municipios, $this->Regioes->Municipios->get($municipio));
            }
            if ($this->Regioes->save($regio)) {
                $this->Flash->success(__('Salvo com sucesso.'));
                $this->Regioes->Municipios->link($regio, $municipios);
                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente.'));
            }
        }
        $municipios = "";
        $municipios_tabela = "";
        $estados = $this->Regioes->Estados->find('list', ['limit' => 200, 'valueField' => 'nome'])->where('1 = 1');
        $operadoras = $this->Regioes->Operadoras->find('list', [
            'valueField' => function ($e) {
                if (isset($e->estado))
                    return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $this->set(compact('regio', 'estados', 'operadoras', 'municipios_tabela', 'municipios'));
        $this->set('_serialize', ['regio']);
    }


    /**
     * Edit method
     *
     * @param string|null $id Regio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Editar Área de Comercialização');
        $regio = $this->Regioes->get($id, [
            'contain' => ['Estados', 'Operadoras', 'Municipios']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $regio = $this->Regioes->patchEntity($regio, $this->request->data);
            $escolhidos = $this->request->data['lista_municipios_escolhidos'];
            $municipios = array();
            foreach ($escolhidos as $municipio) {
                array_push($municipios, $this->Regioes->Municipios->get($municipio));
            }
            if ($this->Regioes->save($regio)) {
                $this->Regioes->Municipios->MunicipiosRegioes->deleteAll(['regiao_id' => $id]);
                $this->Regioes->Municipios->link($regio, $municipios);
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente.'));
            }
        }
        $estados = $this->Regioes->Estados->find('list', ['limit' => 200, 'empty' => 'SELECIONE', 'valueField' => 'nome'])->where('1 = 1');
        $operadoras = $this->Regioes->Operadoras->find('list', [
            'valueField' => function ($e) {
                if (isset($e->estado))
                    return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ])->where(['nova' => null]);

        $municipios = $regio['municipios'];
        $municipios_tabela = array();
        foreach ($municipios as $i => $municipio) {
            $municipios_tabela[$municipio['id']] = $municipio['nome'];
        }

        $this->loadModel('Municipios');
        $municipios = $this->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $regio['estado_id']])->toArray();
        foreach ($municipios as $chave => $municipio) {
            if (array_key_exists($chave, $municipios_tabela)) {
                unset($municipios[$chave]);
            }
        }
        $this->set(compact('regio', 'estados', 'operadoras', 'municipios', 'municipios_tabela'));
        $this->set('_serialize', ['regio']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Regio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $regio = $this->Regioes->get($id);
        if ($this->Regioes->delete($regio)) {
            $this->Flash->success(__('Excluído com Sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    public function filtroEstado($dados = null)
    {
        if (!empty($dados)) {
            $regioes = $this->paginate($this->Regioes->find()->contain(['Estados', 'Operadoras'])->where(['Regioes.estado_id' => $dados]));
            $this->set(compact('regioes'));
        }
    }

    public function getMunicipios($id = null)
    {
        $municipios_tabela = '';
        $municipios = $this->Regioes->Estados->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $id]);
        $this->set(compact('municipios', 'municipios_tabela'));
    }

    public function filtroOperadoras($id = null)
    {
        $operadoras = $this->Regioes->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['Operadoras.nome' => 'ASC']
        ])->where(['estado_id' => $id]);
        $this->set(compact('operadoras'));
    }
}
