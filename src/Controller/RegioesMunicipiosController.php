<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * RegioesMunicipios Controller
 *
 * @property \App\Model\Table\RegioesMunicipiosTable $RegioesMunicipios
 */
class RegioesMunicipiosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Municipios', 'Tabelas']
        ];
        $regioesMunicipios = $this->paginate($this->RegioesMunicipios);

        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);

        $this->set(compact('regioesMunicipios', 'estados'));
        $this->set('_serialize', ['regioesMunicipios']);
    }

    /**
     * View method
     *
     * @param string|null $id Regioes Municipio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $regioesMunicipio = $this->RegioesMunicipios->get($id, [
            'contain' => ['Municipios', 'Tabelas']
        ]);

        $this->set('regioesMunicipio', $regioesMunicipio);
        $this->set('_serialize', ['regioesMunicipio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $regioesMunicipio = $this->RegioesMunicipios->newEntity();
        if ($this->request->is('post')) {
            $regioesMunicipio = $this->RegioesMunicipios->patchEntity($regioesMunicipio, $this->request->data);
            if ($this->RegioesMunicipios->save($regioesMunicipio)) {
                $this->Flash->success(__('The regioes municipio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The regioes municipio could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->RegioesMunicipios->Municipios->find('list', ['limit' => 200]);
        $tabelas = $this->RegioesMunicipios->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('regioesMunicipio', 'municipios', 'tabelas'));
        $this->set('_serialize', ['regioesMunicipio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Regioes Municipio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($tabela)
    {
        $regioesMunicipio = $this->RegioesMunicipios->newEntity();
        $tabela = $this->RegioesMunicipios->Tabelas->get($tabela);


        $municipios_tabela = $this->RegioesMunicipios->Municipios->find('list', ['valueField' => 'nome']);
        $municipios_tabela->matching('Tabelas', function ($q) use ($tabela) {
            return $q->where(['Tabelas.id' => $tabela->id]);
        })->where(['1 = 1']);

        if ($this->request->is(['patch', 'post', 'put'])) {

            // Salva os municipios

            $data = $this->request->data;
            $erros = 0;
            foreach ($data['lista_municipios_escolhidos'] as $municipio) {
                // Se o municipio não estiver salvo, salva.
                if (!key_exists($municipio, $municipios_tabela->toArray())) {

                    $regioesMunicipio = $this->RegioesMunicipios->newEntity();

                    $dados = [
                        'tabela_id' => $data['pf_tabela_id'],
                        'municipio_id' => $municipio
                    ];

                    $regioesMunicipio = $this->RegioesMunicipios->patchEntity($regioesMunicipio, $dados);

                    if (!$this->RegioesMunicipios->save($regioesMunicipio)) {
                        $erros++;
                    }
                }
            }

            // Remove municipios não selecionados
            $municipios_removidos = array_diff(array_keys($municipios_tabela->toArray()), $data['lista_municipios_escolhidos']);
            foreach ($municipios_removidos as $municipio) {
                $municipio = $this->RegioesMunicipios->find('list')->where(['tabela_id' => $tabela->id, 'municipio_id' => $municipio]);

                foreach ($municipio as $registro) {
                    $registro = $this->RegioesMunicipios->get($registro);
                    if (!$this->RegioesMunicipios->delete($registro)) {
                        $erros++;
                    }
                }
            }

            if ($erros == 0) {
                $this->Flash->success(__('The pf comercializacoes municipio has been saved.'));
                return $this->redirect(['controller' => 'Users', 'action' => 'configurar-pme', 'destino' => 'RegioesMunicipios']);
            } else {
                $this->Flash->error(__('The pf comercializacoes municipio could not be saved. Please, try again.'));
            }
        }

        $municipios = $this->RegioesMunicipios->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $tabela->estado_id]);

        $this->set(compact('regioesMunicipio', 'tabela', 'municipios', 'municipios_tabela'));
        $this->set('_serialize', ['regioesMunicipio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Regioes Municipio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $regioesMunicipio = $this->RegioesMunicipios->get($id);
        if ($this->RegioesMunicipios->delete($regioesMunicipio)) {
            $this->Flash->success(__('The regioes municipio has been deleted.'));
        } else {
            $this->Flash->error(__('The regioes municipio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function filtroEstado($estado)
    {
        if (!empty($estado)) {
            $this->loadModel('Operadoras');
            $operadoras = $this->Operadoras->find('list', ['valueField' => 'nome'])
                ->where(['estado_id' => $estado, 'NOT' => ['status' => 'INATIVA']])
                ->orderASC('nome');
            $this->set(compact('operadoras'));
        }
    }

    public function filtroOperadora($operadora)
    {
        if (!empty($operadora)) {
            $this->loadModel('Tabelas');
            $tabelas = $this->Tabelas->find('all')
                ->contain(['Operadoras', 'Municipios'])->where(['operadora_id' => $operadora, 'validade' => 1]);
            $this->set(compact('tabelas'));
        }
    }
}
