<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SimulacoesComentarios Controller
 *
 * @property \App\Model\Table\SimulacoesComentariosTable $SimulacoesComentarios
 */
class SimulacoesComentariosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($simulacao_id = null)
    {
    
        $simulacoesComentarios = $this->SimulacoesComentarios->find('all')->where(['SimulacoesComentarios.simulacao_id' => $simulacao_id])->order(['SimulacoesComentarios.data_alerta' => 'ASC'])->toArray();

        $this->set(compact('simulacoesComentarios','simulacao_id'));
        $this->set('_serialize', ['simulacoesComentarios']);
    }

    /**
     * View method
     *
     * @param string|null $id Simulacoes Comentario id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $simulacoesComentario = $this->SimulacoesComentarios->get($id, [
            'contain' => ['Simulacoes']
        ]);

        $this->set('simulacoesComentario', $simulacoesComentario);
        $this->set('_serialize', ['simulacoesComentario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($simulacao_id = null)
    {
        $simulacoesComentario = $this->SimulacoesComentarios->newEntity();
        if ($this->request->is('post')) {
            $simulacoesComentario = $this->SimulacoesComentarios->patchEntity($simulacoesComentario, $this->request->data);
            if ($this->SimulacoesComentarios->save($simulacoesComentario)) {
                $this->Flash->success(__('Salvo com sucesso'));
                return $this->redirect(['controller' => 'simulacoes','action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $simulacoes = $this->SimulacoesComentarios->Simulacoes->find('list', ['limit' => 200]);
        $this->set(compact('simulacoesComentario', 'simulacoes' ,'simulacao_id'));
        $this->set('_serialize', ['simulacoesComentario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Simulacoes Comentario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $simulacoesComentario = $this->SimulacoesComentarios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $simulacoesComentario = $this->SimulacoesComentarios->patchEntity($simulacoesComentario, $this->request->data);
            if ($this->SimulacoesComentarios->save($simulacoesComentario)) {
                $this->Flash->success(__('Salvo com sucesso'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao salvar. Tente novamente.'));
            }
        }
        $simulacoes = $this->SimulacoesComentarios->Simulacoes->find('list', ['limit' => 200]);
        $this->set(compact('simulacoesComentario', 'simulacoes'));
        $this->set('_serialize', ['simulacoesComentario']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Simulacoes Comentario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $simulacoesComentario = $this->SimulacoesComentarios->get($id);
        if ($this->SimulacoesComentarios->delete($simulacoesComentario)) {
            $this->Flash->success(__('Excluído com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao excluir. Tente novamente'));
        }

        return $this->redirect(['controller' => 'simulacoes','action' => 'index']);
    }
}
