<?php

namespace App\Controller;

use App\Controller\AppController;
//use App\Utility\PdfWriter;
use Cake\I18n\Number;
use Cake\View\ViewBuilder;
use Knp\Snappy\Pdf;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Event\Event;

/**
 * Simulacoes Controller
 *
 * @property \App\Model\Table\SimulacoesTable $Simulacoes
 */
class SimulacoesController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.

        $this->Auth->allow(['cotacao', 'visualizarCotacao', 'pdfVisualizarCotacao']);
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public $paginate = [
        // Other keys here.
        'maxLimit' => 50
    ];

    /**
     *
     *
     *  Salva filtros do PDF incluind tabelas selecionadas e Gera PDF
     *
     *
     */
    public function reenviarpdf($filtro = null)
    {
        //        debug($this->request->data);
        //        die();
        $this->loadModel('Filtros');
        $filtro = $this->Filtros->get($filtro, [
            'contain' => ['TabelasFiltradas' => ['Tabelas']]
        ])->toArray();

        //
        //        debug($filtro);
        //        die();

        foreach ($filtro['tabelas_filtradas'] as $tab) {
            //            debug($tabela);die();
            $tabelasIds[] = $tab['tabela_id'];
        }

        $this->loadModel('Tabelas');

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $userId = $sessao['id'];

        $simulacao = $this->Simulacoes->get($filtro['simulacao_id']);

        $this->loadModel('Tabelas');
        $tabelas = $this->Tabelas->find('all', [
            'contain' => [
                'Abrangencias',
                'Tipos',
                'Estados',
                'Produtos',
                'TiposProdutos',
                'Operadoras' => [
                    'Imagens',
                    'Carencias',
                    'Redes',
                    'Observacoes',
                    'Informacoes',
                    'Opcionais',
                    'Reembolsos'
                ]
            ]
        ], [
            'order' => ['Operadoras.nome' => 'ASC']
        ], ['group' => ['Tabelas.operadora_id']])
            ->where([
                'Tabelas.id IN' => $tabelasIds
            ])->toArray();

        foreach ($tabelas as $tabela) {
            if ($tabela['coparticipacao'] == 'n') {
                $coparticipacao = 'S/COPARTICIPAÇÃO';
            } else {
                $coparticipacao = 'C/COPARTICIPAÇÃO';
            }
            $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;
        }


        $simulacao['observaco'] = $filtro['observacao'];
        $simulacao['rede'] = $filtro['rede'];
        $simulacao['reembolso'] = $filtro['reembolso'];
        $simulacao['carencia'] = $filtro['carencia'];
        $simulacao['informacao'] = $filtro['informacao'];

        //NÃO UTILIZADOS
        //            $simulacao['info'] = 1;
        //            $simulacao['opcionais'] = 1;

        $this->loadModel('Operadoras');
        $op = $this->Operadoras->find('all', ['contain' => ['Imagens']])->toArray();
        foreach ($op as $o) {
            $operadoras[$o['id']] = $o;
        }


        //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
        $tipoPDF = 'F';
        //Nome do PDF
        $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Numero-' . $simulacao['id'] . '.pdf';

        $this->set('simulacao', $simulacao);
        $this->set('_serialize', ['simulacao']);
        $this->set(compact('tabelas', 'simulacao', 'tabelasOrdenadas', 'operadoras', 'sessao', 'nomePDF', 'tipoPDF'));
        $this->viewBuilder()->layout('pdf');
        $this->render('pdf');
        //
    }

    /**
     *
     *
     *  Salva filtros do PDF incluind tabelas selecionadas e Gera PDF
     *
     *
     */
    public function pdf()
    {

        $id = $this->request->data['simulacao_id'];
        $tabelasSelecionadas = $this->request->data;

        //SALVAR OS FILTROS CAS ESTEJAM SELECIONADOS
        if (!empty($tabelasSelecionadas["abrangencia_id"]) || !empty($tabelasSelecionadas["tipo_id"]) || !empty($tabelasSelecionadas["tipo_produto_id"]) || !empty($tabelasSelecionadas["coparticipacao"]) || !empty($tabelasSelecionadas["tipo_contratacao"]) || !empty($tabelasSelecionadas["filtroreembolso"])) {
            $this->loadModel("Pdffiltros");
            $dadosFiltros["simulacao_id"] = $tabelasSelecionadas["simulacao_id"];
            $dadosFiltros["abrangencia_id"] = $tabelasSelecionadas["abrangencia_id"];
            $dadosFiltros["tipo_id"] = $tabelasSelecionadas["tipo_id"];
            $dadosFiltros["tipo_produto_id"] = $tabelasSelecionadas["tipo_produto_id"];
            $dadosFiltros["coparticipacao"] = $tabelasSelecionadas["coparticipacao"];
            $dadosFiltros["tipo_contratacao"] = $tabelasSelecionadas["tipo_contratacao"];
            $dadosFiltros["filtroreembolso"] = $tabelasSelecionadas["filtroreembolso"];

            $entidadeFiltros = $this->Pdffiltros->newEntity();
            $entidadeFiltros = $this->Pdffiltros->patchEntity($entidadeFiltros, $dadosFiltros);
            $this->Pdffiltros->save($entidadeFiltros);
        }

        foreach ($tabelasSelecionadas as $chave => $tabela) {
            //            debug(substr($chave, 0, 8));
            if (
                $chave <> 'coparticipacao' &&
                $chave <> 'tipo_produto_id' &&
                $chave <> 'tipo_id' &&
                $chave <> 'abrangencia_id' &&
                $chave <> 'simulacao_id' &&
                $chave <> 'observacao' &&
                $chave <> 'rede' &&
                $chave <> 'reembolso' &&
                $chave <> 'carencia' &&
                $chave <> 'informacao'
            ) {
                if (substr($chave, 0, 8) === 'checkAll') {
                    unset($tabelasSelecionadas[$chave]);
                } else {
                    if ($tabela == 0) {
                        unset($tabelasSelecionadas[$chave]);
                    } else {
                        $tabelasFiltradas[] = $tabela;
                        $findTabelas[] = $tabela;
                    }
                }
            }
        }
        if (!empty($tabelasFiltradas)) {

            //* SALVAR FILTROS E TABELAS */
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $userId = $sessao['id'];
            $tabelasSelecionadas['user_id'] = $userId;

            $simulacao = $this->Simulacoes->get($id);

            $this->loadModel('Tabelas');
            $tabelas = $this->Tabelas->find('all', [
                'contain' => [
                    'Abrangencias',
                    'Tipos',
                    'Estados',
                    "TabelasCnpjs" => ["Cnpjs"],
                    'Produtos',
                    'TiposProdutos',
                    'Operadoras' => [
                        'Imagens',
                        'FormasPagamentos',
                        'Carencias',
                        'Redes',
                        'Observacoes',
                        'Informacoes',
                        'Opcionais',
                        'Reembolsos',
                        'Regioes'
                    ]
                ]
            ], [
                'order' => ['Operadoras.nome' => 'ASC']
            ], ['group' => ['Tabelas.operadora_id']])
                ->where([
                    'Tabelas.id IN' => $findTabelas
                ])->toArray();

            foreach ($tabelas as $tabela) {
                if ($tabela['coparticipacao'] == 'n') {
                    $coparticipacao = 'S/COPARTICIPAÇÃO';
                } else {
                    $coparticipacao = 'C/COPARTICIPAÇÃO';
                }
                $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;
            }

            $simulacao['observaco'] = $this->request->data['observacao'];
            $simulacao['rede'] = $this->request->data['rede'];
            $simulacao['reembolso'] = $this->request->data['reembolso'];
            $simulacao['carencia'] = $this->request->data['carencia'];
            $simulacao['informacao'] = $this->request->data['documento'];

            //NÃO UTILIZADOS
            //            $simulacao['info'] = 1;
            //            $simulacao['opcionais'] = 1;

            $this->loadModel('Operadoras');
            $op = $this->Operadoras->find('all', ['contain' => ['Imagens']])->toArray();
            foreach ($op as $o) {
                $operadoras[$o['id']] = $o;
            }

            //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
            $tipoPDF = $this->request->data("tipopdf");
            switch ($tipoPDF) {
                case "D":
                    $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '.pdf';
                    break;
                case "I":
                    $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . '.pdf';
                    break;
                case 'F':
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '.pdf';
                    break;
                default:
                    $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . '.pdf';
                    break;
            }

            $this->set('simulacao', $simulacao);
            $this->set('_serialize', ['simulacao']);
            $this->set(compact('tabelas', 'simulacao', 'tabelasOrdenadas', 'operadoras', 'sessao', 'nomePDF', 'tipoPDF'));
            $this->viewBuilder()->layout('pdf');

            //
        } else {
            $this->Flash->error(__('Selecione os cálculos que deseja imprimir.'));
            return $this->redirect(['action' => 'view/' . $this->request->data['simulacao_id']]);
        }
    }

    public function email($simulacao = null, $emailCliente = null)
    {

        $dados = $this->Simulacoes->get($simulacao);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        if (isset($emailCliente) && $emailCliente <> '') {
            $destinatario = $emailCliente;
        } else {
            $destinatario = $sessao["email"];
        }
        $imagem = rawurlencode($session->read('Auth.User.imagem.nome'));
        $caminho = "uploads/imagens/usuarios/logos/";
        $imagem = $caminho . $imagem;
        if (empty($imagem)) {
            $imagem = 'img/logoCpEmail.png';
        }
        $email = new Email('default');
        $email->transport('calculos');
        $email->from('nao-responda@corretorparceiro.com.br', 'Tabela de Preços - Cálculos')
            ->to($destinatario)
            ->sender('nao-responda@corretorparceiro.com.br', 'Tabela de Preços - Cálculos')
            ->replyTo('nao-responda@corretorparceiro.com.br')
            ->emailFormat('html')
            ->attachments([
                'Calculo-Numero-' . $dados['id'] . '.pdf' => [
                    'file' => $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $dados['id'] . '.pdf'
                ]
            ])
            ->subject('Calculo Nº ' . $dados['id'] . ' - Cliente: ' . $dados['nome'] . ' - Ferramentas de Apoio Corretor Parceiro')
            ->send('<html><img src="https://corretorparceiro.com.br/app/' . $imagem . '" width="230"/>'
                . '<br/>'
                . '<br/>'
                . 'À <b>' . $dados['nome'] . '</b>, A/C: <b>' . $dados['contato'] . '</b>.<br/>
        Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.<br/>
        Informamos que os valores, regras de comercialização e condições contratuais são determinadas<br/>
        pelas seguradoras/operadoras e podem ser alterados pelas mesmas a qualquer momento.<br/>
        Os preços e condições estão sujeitos a confirmação no ato do fechamento do contrato.<br/>'
                . '<br/>'
                . '<br/>'
                . 'Atenciosamente, ' . $sessao['nome'] . ' ' . $sessao['sobrenome']
                . '<br/> <b>Email:</b> ' . $sessao['email']
                . '<br/> <b>Celular:</b> ' . $sessao['celular']
                . '<br/> <b>WhatsApp:</b> ' . $sessao['whatsapp']
                . '</html>');



        unlink($_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $dados['id'] . '.pdf');
        $resposta = 'Cálculo enviado para o e-mail: ' . $destinatario;

        $this->set(compact('resposta'));
        //            }
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        return $this->redirect(['controller' => 'Users', 'action' => 'Calculos']);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $userId = $sessao['id'];
        //
        //        $this->paginate = [
        //            'finder' => [
        //                'user_id' => $userId
        //            ]
        //        ];

        $query = $this->Simulacoes->find('all')->contain(['Status'])->where(['user_id' => $userId])->order(['data' => 'DESC']);
        $this->set('simulacoes', $this->paginate($query));

        $this->loadModel('Filtros');
        $filtros = $this->Filtros->find('all')->where(['user_id' => $userId])->order(['created' => 'DESC'])->contain(['TabelasFiltradas' => ['Tabelas' => ['Operadoras', 'Tipos']]])->toArray();

        $this->loadModel('Status');
        $status = $this->Status->find('list', ['valueField' => 'nome'])->toArray();

        //        $simulacoes = $this->paginate($this->Simulacoes);
        $this->set('title', 'Cálculos Salvos');

        $this->set(compact('simulacoes', 'filtros', 'status'));
        $this->set('_serialize', ['simulacoes']);
    }

    /* Metodo para listar pedf gerados a partir do Cálculo */

    public function listarpdfs($simulacao_id = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        //        $userId = $sessao['id'];

        $simulacao = $this->Simulacoes->get($simulacao_id)->toArray();
        $this->loadModel('Filtros');
        $filtros = $this->Filtros->find('all')->where(['Filtros.simulacao_id' => $simulacao_id])->order(['created' => 'DESC'])->contain(['TabelasFiltradas' => ['Tabelas' => ['Operadoras', 'Tipos']]])->toArray();
        //        debug($filtros);

        $this->set(compact('filtros', 'simulacao'));
        $resposta = 'Cálculo enviado para o e-mail: ' . $destinatario;

        $this->set(compact('resposta'));
    }

    /* Metodo para listar pedf gerados a partir do Cálculo */

    public function alterarStatus($simulacao_id = null, $status_id = null)
    {
        $simulacao = $this->Simulacoes->get($simulacao_id);
        $simulacao->status_id = $status_id;

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel("Alertas");
        $this->Alertas->criarAlerta($sessao["id"], "SPJ", $simulacao_id, $status_id);

        $this->Simulacoes->save($simulacao);
    }


    public function tabelas(int $id = null)
    {
        // ID PODE CHEGAR VIA GET, OU VIA POST CASO FILTRO
        $id = is_null($id) ? $this->request->getData('simulacao_id') : $id;
        $is_filtro = $this->request->is('POST') ? true : false;
        $simulacao = $this->Simulacoes->get($id);
        $sessao = $this->request->getSession()->read('Auth.User');


        $simulacao = $this->Simulacoes->get($id);
        $this->loadModel('Pdffiltros');
        $filtro = $this->Pdffiltros->find("all")->where(["simulacao_id" => $id])->order(["created" => "DESC"])->first();

        if (empty($simulacao['tipo_cnpj_id'])) {
            $this->Flash->success(__('Confirme o campo TIPO DE CNPJ.'));
            return $this->redirect(['action' => 'edit', $id]);
        }

        if ($simulacao['user_id'] == $sessao['id'] || $sessao['role'] == 'admin') {

            $totalVidas = 0;
            for ($i = 1; $i <= 10; $i++) {
                $totalVidas = $totalVidas + (int) $simulacao['faixa' . $i];
                if ($simulacao['faixa' . $i] === 0) {
                    $simulacao['faixa' . $i] = null;
                }
            }

            $this->loadModel('TabelasCnpjs');
            $tabelas_cnpjs = $this->TabelasCnpjs
                ->find("all")
                ->where(["cnpj_id" => $simulacao['tipo_cnpj_id']])
                ->toArray();

            $tabelas_ids = [];
            foreach ($tabelas_cnpjs as $row) {
                $tabelas_ids[] = $row["tabela_id"];
            }

            $filtro = "";
            if ($this->request->is('POST')) {
                $filtro = $this->auxAddFiltros($this->request->getData());
            }

            $this->loadModel('Tabelas');
            $tabelas_municipios = $this->Tabelas->find('all')
                ->matching('AreasComercializacoes.Municipios', function ($q) use ($simulacao) {
                    if (!is_null($this->request->getData('municipios')) && !empty($this->request->getData('municipios'))) {
                        return $q->where(['Municipios.id' => $this->request->getData('municipios')]);
                    } else {
                        return $q->where(['Municipios.estado_id' => $simulacao['estado_id']]);
                    }
                })
                ->contain([
                    'Abrangencias',
                    'Tipos',
                    'Coberturas',
                    'Carencias',
                    'Redes',
                    'Observacoes',
                    'Informacoes',
                    'Opcionais',
                    'Reembolsos',
                    'FormasPagamentos',
                    "TabelasCnpjs" => [
                        "Cnpjs"
                    ],
                    'AreasComercializacoes' => [
                        'Metropoles',
                        'Municipios' => ['Estados'],
                        'Operadoras' => ['Imagens']
                    ]
                ])
                ->where([
                    "Tabelas.new" => 1,
                    "Tabelas.id IN" => $tabelas_ids,
                    'Tabelas.validade' => 1,
                    'Tabelas.minimo_vidas <=' => $totalVidas,
                    'Tabelas.maximo_vidas >=' => $totalVidas,
                ])
                ->group('Tabelas.id');
            if ($is_filtro) {
                $tabelas_municipios->where($filtro);
            }
            $tabelas_metropoles = $this->Tabelas->find('all')
                ->matching('AreasComercializacoes.Metropoles.Municipios', function ($q) use ($simulacao) {
                    if (!is_null($this->request->getData('municipios')) && !empty($this->request->getData('municipios'))) {
                        return $q->where(['Municipios.id' => $this->request->getData('municipios')]);
                    } else {
                        return $q->where(['Municipios.estado_id' => $simulacao['estado_id']]);
                    }
                })
                ->contain([
                    'Abrangencias',
                    'Tipos',
                    'Coberturas',
                    'Carencias',
                    'Redes',
                    'Observacoes',
                    'Informacoes',
                    'Opcionais',
                    'Reembolsos',
                    'FormasPagamentos',
                    "TabelasCnpjs" => [
                        "Cnpjs"
                    ],
                    'AreasComercializacoes' => [
                        'Metropoles',
                        'Municipios' => ['Estados'],
                        'Operadoras' => ['Imagens']
                    ]
                ])
                ->where([
                    "Tabelas.new" => 1,
                    "Tabelas.id IN" => $tabelas_ids,
                    'Tabelas.validade' => 1,
                    'Tabelas.minimo_vidas <=' => $totalVidas,
                    'Tabelas.maximo_vidas >=' => $totalVidas,
                ])
                ->group('Tabelas.id');
            if ($is_filtro) {
                $tabelas_metropoles->where($filtro);
            }
            $tabelas = array_merge($tabelas_municipios->toArray(), $tabelas_metropoles->toArray());
            $total_tabelas = count($tabelas);

            $precos = $tabelas;

            array_multisort(array_column($precos, 'faixa1'), SORT_ASC, SORT_NUMERIC, $precos);
            foreach ($precos as $a) {

                if (
                    $a['faixa1'] +
                    $a['faixa2'] +
                    $a['faixa3'] +
                    $a['faixa4'] +
                    $a['faixa5'] +
                    $a['faixa6'] +
                    $a['faixa7'] +
                    $a['faixa8'] +
                    $a['faixa9'] +
                    $a['faixa10'] +
                    $a['faixa11'] +
                    $a['faixa12'] > 0
                ) {
                    $menor_preco = $a;
                    break;
                }
            }
            if (!isset($menor_preco)) {
                $menor_preco = $precos[0];
            }

            $intermediario = $total_tabelas > 1 ? $precos[count($precos) / 2] : $precos[0];
            $maior_preco = end($precos);

            $tabelas_ordenadas = [];
            $area_adicionada = [];
            foreach ($tabelas as $tabela) {
                $operadora_id = $tabela['areas_comercializaco']['operadora']['id'];
                @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['areas_comercializaco']['operadora'];

                @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['abrangencia']['nome']] .=  $tabela['nome'] . ' • ';


                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';

                if (!in_array($tabela['areas_comercializaco']['id'], $area_adicionada)) {
                    if (!empty($tabela['areas_comercializaco']['metropoles'])) {
                        foreach ($tabela['areas_comercializaco']['metropoles'] as $metropoles) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['data'] .= $metropoles['nome'] . ', ';
                        }
                    }
                    if (!empty($tabela['areas_comercializaco']['municipios'])) {
                        foreach ($tabela['areas_comercializaco']['municipios'] as $municipio) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                        }
                    }
                    $area_adicionada[] = $tabela['areas_comercializaco']['id'];
                }
                unset($tabela['areas_comercializaco']['municipios']);
                unset($tabela['areas_comercializaco']['metropoles']);

                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['data'] =  nl2br($tabela['observaco']['descricao']);
                unset($tabela['observaco']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['data'] =  nl2br($tabela['rede']['descricao']);
                unset($tabela['rede']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
                unset($tabela['reembolsoEntity']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['data'] =  nl2br($tabela['carencia']['descricao']);
                unset($tabela['carencia']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['data'] =  nl2br($tabela['opcionai']['descricao']);
                unset($tabela['opcionai']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['data'] =  nl2br($tabela['informaco']['descricao']);
                unset($tabela['informaco']);
                // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['data'] =  nl2br($tabela['formas_pagamento']['descricao']);
                unset($tabela['formas_pagamento']);

                $tabela['acomodacao'] = $tabela['tipo'];

                @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
            }
            $tabelasOrdenadas = $tabelas_ordenadas;


            $this->loadModel('Operadoras');
            $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']])
                ->where(['nova' => 1, 'OR' => ['NOT' => ['Operadoras.status <=>' => 'OCULTA'], 'NOT' => ['Operadoras.status <=>' => 'INATIVA']]])->toArray();

            // DADOS PARA FILTRO
            $totalTabelas = $total_tabelas; //ALIAS ELEMENT FILTRO
            $this->loadModel('Tipos'); // TIPOS É ACOMODAÇÃO
            $acomodacoes = $this->Tipos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

            $this->loadModel('Abrangencias');
            $abrangencias = $this->Abrangencias->find('list', ['valueField' => 'nome', 'order' => ['prioridade' => 'ASC']])->toArray();

            $this->loadModel('Coberturas');
            $coberturas = $this->Coberturas->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();


            $this->loadModel('Municipios');
            $municipios = $this->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $simulacao['estado_id']])->toArray();


            $simulacao['rede'] = 1;
            $simulacao['carencia'] = 1;
            $simulacao['reembolso'] = 1;
            $simulacao['info'] = 1;
            $simulacao['opcionais'] = 1;
            $simulacao['observaco'] = 1;

            // ALIAS
            $total_vidas = $totalVidas;

            $this->set(compact('cotacao', 'simulacao', 'tabelas_ordenadas', 'total_tabelas', 'totalTabelas', 'menor_preco', 'intermediario', 'maior_preco', 'total_vidas', 'operadoras', 'acomodacoes', 'abrangencias', 'coberturas', 'municipios', 'is_filtro'));
        } else {
            $this->Flash->error(__('Sem permissão de visualizar este Cálculo. Cálculo pertence a outro usuário.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function auxAddFiltros(array $filtro)
    {

        $conditions = [];

        if (!empty($filtro)) {

            // ENFERMARIA - AMBULATORIAL - APARTAMENTO - HOSPITALAR
            if (isset($filtro['tipo_id']) && !empty($filtro['tipo_id']) && $filtro['tipo_id'] <> 6) {
                $array = array("Tabelas.tipo_id" => $filtro['tipo_id']);
                array_push($conditions, $array);
            }

            // COMPLETO - HOSPITALAR - AMBULATORIAL
            if (isset($filtro['cobertura_id']) && !empty($filtro['cobertura_id']) && $filtro['cobertura_id'] <> 99) {
                $this->loadModel('TiposProdutos');

                $array = array("Coberturas.id" => $filtro['cobertura_id']);
                array_push($conditions, $array);
            }

            // NACIONAL - ESTADUAL - MUNICIPAL ...
            if (isset($filtro['abrangencia_id']) && !empty($filtro['abrangencia_id']) && $filtro['abrangencia_id'] <> 6) {
                switch ($filtro['abrangencia_id']) {
                    case "3":
                        $array = array("Tabelas.abrangencia_id IN" => array("1", "2", "3", "4", "5"));
                        break;
                    case "4":
                        $array = array("Tabelas.abrangencia_id IN" => array("1", "2", "4", "5"));
                        break;
                    case "2":
                        $array = array("Tabelas.abrangencia_id IN" => array("1", "2", "5"));
                        break;
                    case "5":
                        $array = array("Tabelas.abrangencia_id IN" => array("1", "5"));
                        break;
                    case "1":
                        $array = array("Tabelas.abrangencia_id IN" => array("1"));
                        break;
                }
                array_push($conditions, $array);
            }

            // OPCIONAL - COMPULSÓRIO 
            if (isset($filtro['tipo_contratacao']) && !empty($filtro['tipo_contratacao']) && $filtro['tipo_contratacao'] !== '99') {
                if ($filtro['tipo_contratacao'] === '2') {
                    $filtro['tipo_contratacao'] = '0';
                }
                $variaveis = array(2, $filtro['tipo_contratacao']);
                $array = array('Tabelas.tipo_contratacao IN' => $variaveis);
                array_push($conditions, $array);
            }

            // COM OU SEM REEMBOLSO
            if (isset($filtro['filtroreembolso']) && !empty($filtro['filtroreembolso']) && $filtro['filtroreembolso'] !== '99') {
                $array = array('Tabelas.reembolso' => $filtro['filtroreembolso']);
                array_push($conditions, $array);
            }

            if ($filtro['coparticipacao'] === '99') {
                $filtro['coparticipacao'] = '';
            }
            // COM OU SEM COPARTICIPAÇÃO
            if (isset($filtro['coparticipacao']) && !empty($filtro['coparticipacao']) && $filtro['coparticipacao'] !== '99') {
                $s = $filtro['coparticipacao'];
                $array = array("Tabelas.coparticipacao" => "$s");
                array_push($conditions, $array);
            }
        }

        return $conditions;
    }


    /**
     * View method
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {


        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('Operadoras');
        $this->loadModel('Pdffiltros');
        $this->loadModel('Tabelas');
        $this->loadModel("TabelasCnpjs");

        $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']])->where(['OR' => [['Operadoras.status <>' => 'OCULTA'], ['Operadoras.status <>' => 'INATIVA']]])->toArray();
        $simulacao = $this->Simulacoes->get($id);
        $filtro = $this->Pdffiltros->find("all")->where(["simulacao_id" => $id])->order(["created" => "DESC"])->first();

        if (empty($simulacao['tipo_cnpj_id'])) {
            $this->Flash->success(__('Confirme o campo TIPO DE CNPJ.'));
            return $this->redirect(['action' => 'edit', $id]);
        }
        if ($simulacao['user_id'] == $sessao['id'] || $sessao['role'] == 'admin') {

            //SET TODAS AS FAIXAS COM 0 VIDAS COMO NULL
            $i = 1;
            $totalVidas = 0;
            for ($i = 1; $i <= 10; $i++) {
                $totalVidas = $totalVidas + (int) $simulacao['faixa' . $i];
                if ($simulacao['faixa' . $i] === 0) {
                    $simulacao['faixa' . $i] = null;
                }
            }

            $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']])->where(['NOT(Operadoras.status <=> \'INATIVA\')']);

            $relacional = $this->TabelasCnpjs->find("all")
                ->where(["cnpj_id" => $simulacao['tipo_cnpj_id']])
                ->toArray();
            foreach ($relacional as $relacional) {
                $ids[] = $relacional["tabela_id"];
            }

            $tabelas = $this->Tabelas->find('all', [
                'contain' => [
                    'Abrangencias',
                    'Tipos',
                    'Produtos',
                    'Produtos',
                    'Coberturas',
                    "TabelasCnpjs" => ["Cnpjs"],
                    'Operadoras' => [
                        'Carencias', 'Redes', 'Regioes', 'Observacoes', 'Informacoes', 'Opcionais', 'Reembolsos', 'FormasPagamentos'
                    ]
                ]
            ], [
                'order' => [
                    'Operadoras.nome' => 'ASC'
                ]
            ], ['group' => ['Tabelas.operadora_id']])
                ->order([
                    'Operadoras.prioridade' => 'ASC',
                    'Tabelas.prioridade' => 'ASC'
                ])
                ->where([
                    "Tabelas.id IN" => $ids,
                    'Tabelas.validade' => 1,
                    'Tabelas.estado_id' => $simulacao['estado_id'],
                    'Tabelas.minimo_vidas <=' => $totalVidas,
                    'Tabelas.maximo_vidas >=' => $totalVidas
                ])->toArray();

            $session->delete('Auth.User.tabelas');
            //REMOÇÃO DE TABELAS BRADESCO DE ACORDO COM SELEÇÃO DE GRUPO FAMILIAR OU NÃO

            $bradesco = array(1, 28, 29, 30, 31);
            if ($simulacao['familia'] == 1) {
                foreach ($tabelas as $chave => $tabela) {
                    if (in_array($tabela['operadora_id'], $bradesco) && $tabela['minimo_vidas'] == 4 && $tabela['maximo_vidas'] == 29) {
                        unset($tabelas[$chave]);
                    }
                }
            } else {
                if ($totalVidas >= 4) {
                    foreach ($tabelas as $chave => $tabela) {
                        if (in_array($tabela['operadora_id'], $bradesco) && $tabela['minimo_vidas'] == 3 && $tabela['maximo_vidas'] == 29) {
                            unset($tabelas[$chave]);
                        }
                    }
                }
            }

            foreach ($tabelas as $tabela) {
                $idsSessao[$tabela["id"]] = $tabela["id"];
            }
            $session->write([
                'Auth.User.tabelas' => $idsSessao
            ]);
            // 	                    $session->renew();
            $sessao = $session->read('Auth.User');



            $tabelaspreco = $this->Tabelas->find('all', [
                'contain' => [
                    'Abrangencias',
                    'Tipos',
                    'Estados',
                    'Produtos',
                    'Produtos',
                    'TiposProdutos',
                    'Operadoras' => [
                        'Imagens',
                        'Carencias',
                        'Redes',
                        'Observacoes',
                        'Informacoes',
                        'Opcionais',
                        'Reembolsos',
                        'FormasPagamentos'
                    ]
                ]
            ])
                ->order([
                    'Tabelas.faixa1' => 'ASC'
                ])
                ->where([
                    'Tabelas.id IN' => $idsSessao,
                    'Tabelas.validade' => 1
                ])->toArray();
            foreach ($tabelaspreco as $a) {

                if (
                    $a['faixa1'] +
                    $a['faixa2'] +
                    $a['faixa3'] +
                    $a['faixa4'] +
                    $a['faixa5'] +
                    $a['faixa6'] +
                    $a['faixa7'] +
                    $a['faixa8'] +
                    $a['faixa9'] +
                    $a['faixa10'] > 0
                ) {
                    $maisbarato = $a;
                    break;
                }
            }
            $intermediario = $tabelaspreco[count($tabelas) / 2];
            $maiscaro = end($tabelaspreco);

            $totalTabelas = count($tabelas);
            foreach ($tabelas as $tabela) {
                if ($tabela['coparticipacao'] == 'n') {
                    $coparticipacao = 'S/COPARTICIPAÇÃO';
                } else {
                    $coparticipacao = 'C/COPARTICIPAÇÃO ' . $tabela["detalhe_coparticipacao"];
                }
                $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;
            }
        } else {
            $this->Flash->error(__('Sem permissão de visualizar este Cálculo. Cálculo pertence a outro usuário.'));
            return $this->redirect(['action' => 'index']);
        }

        $this->loadModel('Tipos');
        $tipos = $acomodacoes = $this->Tipos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('Abrangencias');
        $abrangencias = $this->Abrangencias->find('list', ['valueField' => 'nome', 'order' => ['prioridade' => 'ASC']])->toArray();


        $this->loadModel('Coberturas');
        $coberturas = $this->Coberturas->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();


        $simulacao['rede'] = 1;
        $simulacao['carencia'] = 1;
        $simulacao['reembolso'] = 1;
        $simulacao['info'] = 1;
        $simulacao['opcionais'] = 1;
        $simulacao['observaco'] = 1;

        $btnCancelar = ['controller' => 'simulacoes', 'action' => 'index'];
        $this->set(compact('filtro', 'maisbarato', 'intermediario', 'maiscaro', 'simulacao', 'coberturas', 'tabelasOrdenadas', 'abrangencias', 'tipos', 'acomodacoes', 'btnCancelar', 'operadoras', 'totalTabelas'));
        $this->render('simulacao');
    }

    /**
     * View method
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewGrupo($id = null)
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->set('title', 'Simulação Salva');
        $this->loadModel('Operadoras');
        $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']])->toArray();
        $simulacao = $this->Simulacoes->get($id);
        if ($simulacao['id_primario'] == '') {
            $todas_simulacoes = $this->Simulacoes->find('all')->where(['OR' => ['Simulacoes.id_primario' => $id, 'Simulacoes.id' => $id]])->toArray();
        } else {
            $todas_simulacoes = $this->Simulacoes->find('all')
                ->where([
                    'OR' => [
                        'Simulacoes.id' => $simulacao['id_primario'],
                        'Simulacoes.id_primario' => $simulacao['id_primario'],
                    ]
                ])->toArray();
        }

        if ($simulacao['user_id'] == $sessao['id'] || $sessao['role'] == 'admin') {
            $this->loadModel('SimulacoesTabelas');
            foreach ($todas_simulacoes as $grupo) {
                //                debug($grupo);die();
                $simulacoesTabelas[$grupo['id']] = $this->SimulacoesTabelas->find('all')
                    ->where(['SimulacoesTabelas.simulacao_id' => $grupo['id']])->toArray();
                foreach ($simulacoesTabelas[$grupo['id']] as $tabela) {
                    $tabelasIds[$grupo['id']][] = $tabela['tabela_id'];
                }

                $this->loadModel('Tabelas');
                $tabelas[$grupo['id']] = $this->Tabelas->find('all', [
                    'contain' => [
                        'Abrangencias',
                        'Tipos',
                        'Regioes',
                        'Estados',
                        'Produtos',
                        'TiposProdutos',
                        'Operadoras' => [
                            'Imagens',
                            'Carencias',
                            'Redes',
                            'Observacoes',
                            'Informacoes',
                            'Opcionais',
                            'Reembolsos'
                        ]
                    ]
                ])
                    ->order([
                        'Tabelas.faixa1' => 'ASC'
                    ])
                    ->where([
                        'Tabelas.id IN' => $tabelasIds[$grupo['id']],
                        'Tabelas.validade' => 1
                    ])->toArray();
                //                debug();

                $maisbarato[$grupo['id']] = $tabelas[$grupo['id']][0];
                $intermediario[$grupo['id']] = $tabelas[$grupo['id']][count($tabelas[$grupo['id']]) / 2];
                $maiscaro[$grupo['id']] = end($tabelas[$grupo['id']]);


                //                debug($intermediario);
                //                die();
                //                debug($tabelas[$grupo['id']][count($tabelas[$grupo['id']])-1]);
                //                debug($totalTabelas);
                //                $totalTabelas[$grupo['id']] = count($tabelas[$grupo['id']]);
                //                die();
                //                foreach ($tabelas[$grupo['id']] as $tabela) {
                //                    if ($tabela['coparticipacao'] == 'n') {
                //                        $coparticipacao = 'S/COPARTICIPAÇÃO';
                //                    } else {
                //                        $coparticipacao = 'C/COPARTICIPAÇÃO';
                //                    }
                //                    $tabelasOrdenadas[$grupo['id']][$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;
                //                }
            }
            //            debug($tabelasOrdenadas);
            //            die();
        } else {
            $this->Flash->error(__('Sem permissão de visualizar este Cálculo. Cálculo pertence a outro usuário.'));
            return $this->redirect(['action' => 'index']);
        }

        $this->loadModel('Tipos');
        $tipos = $this->Tipos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('Abrangencias');
        $abrangencias = $this->Abrangencias->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();


        $this->loadModel('TiposProdutos');
        $tipos_produtos = $this->TiposProdutos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();


        $btnCancelar = ['controller' => 'simulacoes', 'action' => 'index'];
        $this->set(compact('maisbarato', 'intermediario', 'maiscaro', 'todas_simulacoes'));
        $this->render('view_grupo');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nova Simulação');
        $simulacao = $this->Simulacoes->newEntity();
        if ($this->request->is('post')) {


            $simulacao = $this->Simulacoes->patchEntity($simulacao, $this->request->data);
            if ($this->Simulacoes->save($simulacao)) {
                $this->Flash->success(__('Simulação Salva com Sucesso'));

                $idSimulacao = $simulacao->id;

                $this->loadModel('Tabelas');
                $tabelasIds = $this->Tabelas->find('all', ['fields' => 'id'])
                    ->where([
                        'Tabelas.validade' => 1,
                        'Tabelas.abrangencia_id' => $simulacao['abrangencia_id']
                    ])->toArray();

                $this->loadModel('SimulacoesTabelas');
                foreach ($tabelasIds as $idTabela) {
                    $dados['simulacao_id'] = $idSimulacao;
                    $dados['tabela_id'] = $idTabela['id'];
                    //                    debug($dados);
                    $simulacoesTabelas = $this->SimulacoesTabelas->newEntity();
                    $simulacoesTabelas = $this->SimulacoesTabelas->patchEntity($simulacoesTabelas, $dados);
                    $this->SimulacoesTabelas->save($simulacoesTabelas);
                }
                //                    die();

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao Salvar Simulação. Tente Novamente'));
            }
        }

        $this->loadModel('Modalidades');
        $modalidades = $this->Modalidades->find('list', ['valueField' => 'nome', 'order' => ['id' => 'ASC']]);

        $this->loadModel('Ramos');
        $ramos = $this->Ramos->find('list', ['valueField' => 'nome', 'order' => ['id' => 'ASC']]);

        $this->loadModel('Abrangencias');
        $abrangencias = $this->Abrangencias->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);

        $this->loadModel('Cnpjs');
        $cnpjs = $this->Cnpjs->find('list', ['valueField' => 'nome', 'order' => ['id' => 'ASC']]);

        $this->loadModel('Operadoras');
        $operadoras = $this->Operadoras->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC'], 'empty' => 'SELECIONE']);


        $this->loadModel('Estados');
        // $estados = $this->Estados
        //     ->find('list', ['valueField' => 'nome'])
        //     ->order(['Estados.nome' => 'asc']);
        $estados = $this->Estados
            ->getConnection()
            ->execute("SELECT Estados.id AS `id`, Estados.nome AS `nome` FROM estados Estados 
                INNER JOIN areas_comercializacoes_estados_municipios AreasComercializacoesEstadosMunicipios ON Estados.id = (AreasComercializacoesEstadosMunicipios.estado_id) 
                INNER JOIN areas_comercializacoes AreasComercializacoes ON AreasComercializacoes.id = (AreasComercializacoesEstadosMunicipios.area_comercializacao_id) 
                INNER JOIN tabelas Tabelas ON (validade = 1 AND AreasComercializacoes.id = (Tabelas.area_comercializacao_id)) 
                GROUP BY id")
            ->fetchAll(\PDO::FETCH_KEY_PAIR);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');


        $tabelasRegioes = $this->Simulacoes->TabelasRegioes->find('list', ['limit' => 200]);
        $this->set(compact('simulacao', "cnpjs", 'sessao', 'tabelasRegioes', 'estados', 'operadoras', 'modalidades', 'ramos', 'abrangencias'));
        $this->set('_serialize', ['simulaco']);
        //        $this->viewBuilder()->layout('simulacao');
    }

    /**
     * Edit method
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (empty($this->request->data)) {
            $this->set('title', 'Editar Simulação');
            $simulacao = $this->Simulacoes->get($id, [
                'contain' => []
            ]);
        }
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('Cnpjs');
        $cnpjs = $this->Cnpjs->find('list', ['valueField' => 'nome', 'order' => ['id' => 'ASC']]);

        $this->loadModel('Tabelas');
        $estadosContados = $this->Tabelas
            ->find('all', ['empty' => 'SELECIONE'])
            ->select(['estado_id'])
            ->distinct(['estado_id'])
            ->where(['validade' => 1])
            ->toArray();
        $array = array();
        foreach ($estadosContados as $estadosContados) {
            $array[] = $estadosContados['estado_id'];
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC'], 'empty' => 'SELECIONE'])->where(['id IN' => $array]);

        $this->set(compact("cnpjs", 'simulacao', 'sessao', 'estados'));
        $this->set('_serialize', ['simulaco']);
        $this->render("add");
    }

    /**
     * Delete method
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $simulacao = $this->Simulacoes->get($id);

        $this->loadModel('SimulacoesTabelas');
        $simulacoesTabelas = $this->SimulacoesTabelas->find('list')->where(['SimulacoesTabelas.simulacao_id' => $id]);
        //        debug($simulacoesTabelas->toArray());
        //        die();
        foreach ($simulacoesTabelas as $simulacaoTabela) {
            //            $this->request->allowMethod(['post', 'delete']);
            $entidade = $this->SimulacoesTabelas->get($simulacaoTabela);
            $this->SimulacoesTabelas->delete($entidade);
        }

        if ($this->Simulacoes->delete($simulacao)) {
            $this->Flash->success(__('Cálculo excluído com Sucesso'));
        } else {
            $this->Flash->error(__('Erro ao Excluir Cálculo. Tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método para popular Select
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function preencheSelect($tipo = null, $id = null)
    {
        $model = null;
        switch ($tipo) {
            case 'p':
                $model = 'Tabelas';
                $nome = 'produto_id';
                $label = 'Tabelas';
                $field = 'produto_id';
                break;
            case 't':
                $model = 'Tabelas';
                $nome = 'tabela_id';
                $label = 'Tabelas';
                $field = 'id';
                break;
            case 'r':
                $model = 'Regioes';
                $nome = 'regiao_id';
                $label = 'Regiões';
                $field = 'estado_id';
                break;
            case 'tr':
                $model = 'TabelasRegioes';
                $nome = 'tabelas_regioes_id';
                $label = 'Produtos Disponíveis';
                $field = 'regiao_id';
                break;
            case 'o':
                $model = 'Produtos';
                $nome = 'produto_id';
                $label = 'Produtos';
                $field = 'operadora_id';
                break;
        }

        $this->loadModel($model);
        $options = $this->$model->find('list', ['valueField' => 'nome', 'empty' => 'SELECIONE'])->where([$model . '.' . $field => $id]);

        $this->set(compact('options', 'nome', 'label'));
        $this->render('selectPopulado');
    }

    /**
     * Método para popular Multiplo Select
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function multiploSelect($tipo = null, $id = null)
    {

        switch ($tipo) {
            case 't':
                $model = 'Tabelas';
                $nome = 'produto_id';
                $label = 'Tabelas';
                $field = 'produto_id';
                break;
        }

        $this->loadModel($model);
        $options = $this->$model->find('list', ['valueField' => 'nome', 'empty' => 'SELECIONE'])->where([$model . '.' . $field => $id]);

        $this->set(compact('options', 'nome', 'label'));
        $this->render('mutiploSelect');
    }

    /**
     * Método para popular Multiplo Select
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function grupofamiliar()
    {

        //        $this->render('mutiploSelect');
    }

    /**
     * Método para popular Multiplo Select
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function grupototal()
    {
    }

    /**
     * Método para emitir Simulações
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function encontrarTabelas($data = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $userId = $sessao['id'];
        $simulacao = $this->request->data();
        if (!empty($this->request->data['id'])) {
            $simulacoes = $this->Simulacoes->get($this->request->data['id']);
        }
        if (isset($simulacoes) && $simulacoes->edicoes < 10) {
            $simulacoes->edicoes = $simulacoes->edicoes + 1;
            $simulacoes = $this->Simulacoes->patchEntity($simulacoes, $this->request->data);
            $this->Simulacoes->save($simulacoes);

            $dados['id'] = $simulacao['id'];
            $dados['msg'] = 'Registro salvo com sucesso.';
            $dados['ok'] = true;
            // return $this->redirect(['action' => 'view', $simulacao['id']]);
            $this->Flash->success('Registro salvo com sucesso.');
            $this->set(compact('simulacoes', 'dados'));
            $this->render('simulacao', 'ajax');
        } else {

            //        Transformando 0 vidas em null para armazenar no Banco;
            if (!isset($simulacao['grupofamiliar'])) {
                $i = 1;
                for ($i = 1; $i <= 10; $i++) {
                    if ($simulacao['faixa' . $i] === 0) {
                        $simulacao['faixa' . $i] = null;
                    }
                }
                // 			debug($simulacao);die();

                if (empty($simulacao['tipo_cnpj_id'])) {

                    if ($simulacao['tipo_cnpj_id'] == null) {
                        $campoVazio = $campoVazio . 'Tipo de CNPJ';
                    }
                    $this->Flash->error('Campo ' . $campoVazio . ' é Obrigatório.');
                    $this->render('add', 'ajax');
                    // return $this->redirect(['action' => 'add']);
                } else {
                    $this->loadModel('Tabelas');
                    $i = 1;
                    $totalVidas = 0;
                    for ($i = 1; $i <= 10; $i++) {
                        $totalVidas = $totalVidas + (int) $simulacao['faixa' . $i];
                    }

                    $this->loadModel('Operadoras');
                    $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']]);

                    $this->loadModel("TabelasCnpjs");
                    $relacional = $this->TabelasCnpjs->find("all")
                        ->where(["cnpj_id" => $simulacao['tipo_cnpj_id']])
                        ->toArray();
                    foreach ($relacional as $relacional) {
                        $ids[] = $relacional["tabela_id"];
                    }

                    $tabelas = $this->Tabelas->find('all', [

                        'contain' => [
                            'Abrangencias',
                            'Tipos',
                            "TiposProdutos",
                            'Produtos' => ["TiposProdutos"],
                            "TabelasCnpjs" => ["Cnpjs"],
                            'Operadoras' => [
                                'Carencias', 'Redes', 'Observacoes', 'Informacoes', 'Opcionais', 'Reembolsos'
                            ]
                        ]
                    ], [
                        'order' => [
                            'Operadoras.nome' => 'ASC'
                        ]
                    ], ['group' => ['Tabelas.operadora_id']])
                        ->order([
                            'Operadoras.prioridade' => 'ASC',
                            'Tabelas.prioridade' => 'ASC'
                        ])
                        ->where([
                            "Tabelas.id IN" => $ids,
                            'Tabelas.validade' => 1,
                            'Tabelas.estado_id' => $simulacao['estado_id'],
                            'Tabelas.minimo_vidas <=' => $totalVidas,
                            'Tabelas.maximo_vidas >=' => $totalVidas
                        ])->toArray();


                    if ($tabelas) {

                        $simulacao = $this->Simulacoes->newEntity();
                        $simulacao = $this->Simulacoes->patchEntity($simulacao, $this->request->data);
                        $simulacao['user_id'] = $userId;

                        if ($this->Simulacoes->save($simulacao)) {
                            $this->loadModel('Users');
                            $userLogado = $this->Users->get($simulacao['user_id']);

                            $userLogado->ultimocalculo = Time::now();
                            $userLogado->dirty('ultimocalculo', true);
                            $this->Users->save($userLogado);

                            $idSimulacao = $simulacao->id;
                            $this->loadModel('Tabelas');

                            //REMOÇÃO DE TABELAS BRADESCO DE ACORDO COM SELEÇÃO DE GRUPO FAMILIAR OU NÃO
                            $bradesco = array(1, 28, 29, 30, 31);
                            if ($simulacao['familia'] == 1) {
                                foreach ($tabelas as $chave => $tabela) {
                                    if (in_array($tabela['operadora_id'], $bradesco) && $tabela['minimo_vidas'] == 4 && $tabela['maximo_vidas'] == 29) {
                                        unset($tabelas[$chave]);
                                    }
                                }
                            } else {
                                if ($totalVidas >= 4) {
                                    foreach ($tabelas as $chave => $tabela) {
                                        if (in_array($tabela['operadora_id'], $bradesco) && $tabela['minimo_vidas'] == 3 && $tabela['maximo_vidas'] == 29) {
                                            unset($tabelas[$chave]);
                                        }
                                    }
                                }
                            }

                            $dados['id'] = $simulacao->id;
                            $dados['msg'] = 'Registro salvo com sucesso.';
                            $dados['ok'] = true;

                            $this->autoRender = false;
                            $this->response->type('json');
                            $this->response->body(json_encode($dados));
                        } else {
                            // $this->Flash->error(__(''));
                            $dados['msg'] = 'Erro ao Salvar Simulação. Tente Novamente.';
                            $dados['ok'] = false;

                            $this->autoRender = false;
                            $this->response->type('json');
                            $this->response->body(json_encode($dados));
                        }
                    }

                    foreach ($tabelas as $tabela) {
                        if ($tabela['coparticipacao'] == 'n') {
                            $coparticipacao = 'S/COPARTICIPAÇÃO';
                        } else {
                            $coparticipacao = 'C/COPARTICIPAÇÃO';
                        }
                        $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;
                    }
                }
            } else {
                $idPrimario = null;

                foreach ($simulacao['Grupos'] as $grupos) {

                    $i = 1;
                    for ($i = 1; $i <= 10; $i++) {
                        if ($grupos['faixa' . $i] === 0) {
                            $grupos['faixa' . $i] = null;
                        }
                    }


                    $this->loadModel('Tabelas');
                    $i = 1;
                    $totalVidas = 0;
                    for ($i = 1; $i <= 10; $i++) {
                        $totalVidas = $totalVidas + (int) $grupos['faixa' . $i];
                    }


                    $this->loadModel('Operadoras');
                    $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']]);

                    $tabelas = $this->Tabelas->find('all', ['contain' => [
                        'Abrangencias',
                        'Tipos',
                        "TiposProdutos",
                        'Produtos' => ["TiposProdutos"],
                        'Operadoras' => ['Carencias', 'Redes', 'Observacoes', 'Informacoes', 'Opcionais', 'Reembolsos']
                    ]], ['order' => ['Operadoras.nome' => 'ASC']], ['group' => ['Tabelas.operadora_id']])
                        ->order([
                            'Operadoras.prioridade' => 'ASC',
                            'Tabelas.prioridade' => 'ASC'
                        ])
                        ->where([
                            'Tabelas.validade' => 1,
                            'Tabelas.estado_id' => $simulacao['estado_id'],
                            'Tabelas.minimo_vidas <=' => $totalVidas,
                            'Tabelas.tipo_cnpj IN' => array($simulacao['tipo_cnpj_id'], '3'),
                            'Tabelas.maximo_vidas >=' => $totalVidas
                        ])->toArray();

                    if ($tabelas) {
                        //                    debug($simulacao);
                        //                    debug($this->request->data);
                        $simulacao['nome_titular'] = $grupos['nome_titular'];
                        $simulacao['id_primario'] = $idPrimario;
                        $simulacao['faixa1'] = $grupos['faixa1'];
                        $simulacao['faixa2'] = $grupos['faixa2'];
                        $simulacao['faixa3'] = $grupos['faixa3'];
                        $simulacao['faixa4'] = $grupos['faixa4'];
                        $simulacao['faixa5'] = $grupos['faixa5'];
                        $simulacao['faixa6'] = $grupos['faixa6'];
                        $simulacao['faixa7'] = $grupos['faixa7'];
                        $simulacao['faixa8'] = $grupos['faixa8'];
                        $simulacao['faixa9'] = $grupos['faixa9'];
                        $simulacao['faixa10'] = $grupos['faixa10'];
                        //                    die();
                        $simulacaoEntidade = $this->Simulacoes->newEntity();
                        $simulacaoEntidade = $this->Simulacoes->patchEntity($simulacaoEntidade, $simulacao);
                        $simulacaoEntidade['user_id'] = $userId;

                        //                    debug($simulacao);
                        //                    debug($this->Simulacoes->save($simulacaoEntidade));

                        if ($this->Simulacoes->save($simulacaoEntidade)) {
                            //                        debug($simulacaoEntidade);
                            if ($idPrimario === null) {
                                $idPrimario = $simulacaoEntidade->id;
                            }
                            $this->loadModel('Users');
                            $userLogado = $this->Users->get($simulacaoEntidade['user_id']);

                            $userLogado->ultimocalculo = Time::now();
                            $userLogado->dirty('ultimocalculo', true);
                            $this->Users->save($userLogado);

                            $idSimulacao = $simulacaoEntidade->id;
                            $this->loadModel('Tabelas');

                            //REMOÇÃO DE TABELAS BRADESCO DE ACORDO COM SELEÇÃO DE GRUPO FAMILIAR OU NÃO
                            $bradesco = array(1, 28, 29, 30, 31);
                            if ($simulacao['familia'] == 1) {
                                foreach ($tabelas as $chave => $tabela) {
                                    if (in_array($tabela['operadora_id'], $bradesco) && $tabela['minimo_vidas'] == 4 && $tabela['maximo_vidas'] == 29) {
                                        unset($tabelas[$chave]);
                                    }
                                }
                            } else {
                                if ($totalVidas >= 4) {
                                    foreach ($tabelas as $chave => $tabela) {
                                        if (in_array($tabela['operadora_id'], $bradesco) && $tabela['minimo_vidas'] == 3 && $tabela['maximo_vidas'] == 29) {
                                            unset($tabelas[$chave]);
                                        }
                                    }
                                }
                            }
                            //REMOÇÃO DE TABELAS SULAMÉRICA DE ACORDO COM TIPO DE CNPJ SELECIONADO
                            foreach ($tabelas as $chave => $tabela) {
                                if ($tabela['operadora_id'] == 16) {
                                    if ($tabela['tipo_cnpj'] <> $simulacao['tipo_cnpj_id']) {
                                        unset($tabelas[$chave]);
                                    }
                                }
                            }

                            $this->loadModel('SimulacoesTabelas');
                            foreach ($tabelas as $idTabela) {
                                $dados['simulacao_id'] = $idSimulacao;
                                $dados['tabela_id'] = $idTabela['id'];
                                $this->loadModel('SimulacoesTabelas');
                                $simulacoesTabelas = $this->SimulacoesTabelas->newEntity();
                                $simulacoesTabelas = $this->SimulacoesTabelas->patchEntity($simulacoesTabelas, $dados);
                                $this->SimulacoesTabelas->save($simulacoesTabelas);
                            }
                        }
                    }

                    foreach ($tabelas as $tabela) {
                        if ($tabela['coparticipacao'] == 'n') {
                            $coparticipacao = 'S/COPARTICIPAÇÃO';
                        } else {
                            $coparticipacao = 'C/COPARTICIPAÇÃO';
                        }
                        $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;
                    }
                }
                if (isset($simulacao['grupofamiliar'])) {
                    return $this->redirect(['action' => 'viewGrupo', $simulacaoEntidade->id]);
                } else {
                    // return $this->redirect(['action' => 'view', $simulacaoEntidade->id]);
                    $dados['id'] = $simulacaoEntidade->id;
                    $dados['msg'] = 'Registro salvo com sucesso.';
                    $dados['ok'] = true;

                    $this->autoRender = false;
                    $this->response->type('json');
                    $this->response->body(json_encode($dados));
                }
            }

            $btnCancelar = '#';
            $this->set(compact('dados', 'tabelas', 'simulacao', 'tabelasOrdenadas', 'btnCancelar', 'operadoras'));
            $this->render('simulacao');
        }
    }

    public function cotar()
    {
        $user_id = $this->request->getSession()->read('Auth.User.id');
        $cotacao = $this->request->getData();

        for ($i = 1; $i <= 10; $i++) {
            if ($cotacao['faixa' . $i] === 0) {
                $cotacao['faixa' . $i] = null;
            }
        }

        $cotacao['user_id'] = $user_id;

        $simulacao = $this->Simulacoes->newEntity();
        $simulacao = $this->Simulacoes->patchEntity($simulacao, $cotacao);

        // SALVA A COTAÇÃO 
        if ($this->Simulacoes->save($simulacao)) {

            // ALTERA DATA DA ULTIMA COTAÇÃO DO USUÁRIO
            $this->loadModel('Users');
            $userLogado = $this->Users->get($simulacao['user_id']);

            $userLogado->ultimocalculo = Time::now();
            $userLogado->dirty('ultimocalculo', true);
            $this->Users->save($userLogado);

            $dados['id'] = $simulacao->id;
            $dados['msg'] = 'Registro salvo com sucesso.';
            $dados['ok'] = true;
        } else {
            $dados['msg'] = 'Erro ao Salvar Simulação. Tente Novamente.';
            $dados['ok'] = false;
        }

        $this->autoRender = false;
        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($dados));
    }

    /**
     * Checar Regioes que as tabelas estao disponiveis
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function regioesDisponiveis($tabelas = null)
    {
        $tabelas = $this->request->data();
        $this->loadModel('TabelasRegioes');

        foreach ($tabelas['tabelas']['_ids'] as $tabela) {
            //            debug($tabela);
            $tabelasRegioes[] = $this->TabelasRegioes->find('all')->where(['TabelasRegioes.tabela_id' => $tabela])->toArray();
        }

        if ($tabelasRegioes) {
            $this->loadModel('Regioes');
            foreach ($tabelasRegioes as $id) {
                foreach ($id as $id) {
                    $regioes[] = $this->Regioes->find('all', ['contain' => 'Estados'])
                        ->where(['Regioes.id' => $id['regiao_id']])->toArray();
                }
            }
        }

        foreach ($regioes as $regiao) {

            $estados[$regiao[0]['estado']['id']] = $regiao[0]['estado']['nome'];
        }
        //        debug($estados);
        asort($estados);
        $this->set(compact('estados'));
        $this->render('regioesDisponiveis');
    }

    /**
     * Operadoras selecionadas para Impressão
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function selecionarImpressao($idSimulacao = null)
    {
        $operadorasSelecionadas = $this->request->data;
        $simulacao = $this->Simulacoes->get($idSimulacao);
        unset($operadorasSelecionadas['simulacaoId']);

        if (isset($operadorasSelecionadas)) {
            $idsOperadoras = array();
            foreach ($operadorasSelecionadas as $operadora) {

                if ($operadora <> 0) {
                    $idsOperadoras[] = $operadora;
                }
            }
            $this->loadModel('SimulacoesTabelas');

            $tabelasSelecionadas = $this->SimulacoesTabelas->find('all', [
                'contain' => [
                    'Tabelas' => ['Operadoras']
                ]
            ])
                ->where([
                    'SimulacoesTabelas.simulacao_id' => $idSimulacao,
                    'Operadoras.id IN' => $idsOperadoras
                ])
                ->select(['SimulacoesTabelas.tabela_id'])->toArray();

            $idsTabelas = null;
            foreach ($tabelasSelecionadas as $tabelasSelecionadas) {
                $idsTabelas[] = $tabelasSelecionadas['tabela_id'];
            }
            $this->loadModel('Tabelas');
            $tabelas = $this->Tabelas->find('all', [
                'contain' => [
                    'Abrangencias',
                    'Tipos',
                    'Produtos',
                    "TiposProdutos",
                    'Operadoras' => [
                        'Imagens',
                        'Carencias',
                        'Redes',
                        'Observacoes',
                        'Informacoes',
                        'Opcionais',
                        'Reembolsos'
                    ]
                ]
            ], [
                'order' => ['Operadoras.nome' => 'ASC']
            ], ['group' => ['Tabelas.operadora_id']])
                ->where([
                    'Tabelas.id IN' => $idsTabelas
                ])->toArray();

            foreach ($tabelas as $tabela) {
                $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;

                //                $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome'] . " - " . $tabela['tipo']['nome']] = $tabela;
            }

            $simulacaoOriginal = $idSimulacao;
            $this->viewBuilder()->options([
                'pdfConfig' => [
                    'orientation' => 'portrait',
                    'filename' => 'Simulacao_' . $idSimulacao . '.pdf'
                ]
            ]);

            $this->set(compact('simulacao', 'tabelasOrdenadas'));
            $this->viewBuilder()->layout('pdf');
        }
        $this->loadModel('Operadoras');
        $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']])->toArray();

        $simulacao['rede'] = 1;
        $simulacao['carencia'] = 1;
        $simulacao['reembolso'] = 1;
        $simulacao['info'] = 1;
        $simulacao['opcionais'] = 1;
        $simulacao['observaco'] = 1;

        $btnCancelar = ['controller' => 'simulacoes', 'action' => 'index'];
        $this->set('simulacao', $simulacao);
        $this->set(compact('tabelas', 'simulacao', 'tabelasOrdenadas', 'btnCancelar', 'operadoras', 'simulacaoOriginal'));
        $this->render('simulacao');
    }

    /**
     * Método para buscar calculo por parametros de busca
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function encontrarCalculo()
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        //        debug($sessao['id']);
        //        die();
        if (!empty($this->request->data['info_busca'])) {
            switch ($this->request->data['tipo_busca']) {
                case 'c':
                    $conditions = 'Simulacoes.id = ' . $this->request->data['info_busca'] . ' AND Simulacoes.user_id = ' . $sessao['id'];
                    break;
                case 'n':
                    $conditions = 'Simulacoes.nome LIKE "%' . $this->request->data['info_busca'] . '%" AND Simulacoes.user_id = ' . $sessao['id'];
                    break;
                case 'e':
                    $conditions = 'Simulacoes.email LIKE "%' . $this->request->data['info_busca'] . '%" AND Simulacoes.user_id = ' . $sessao['id'];
                    break;
            }
            $simulacoes = $this->Simulacoes->find('all')
                ->where([
                    $conditions
                ]);
            $this->set(compact('simulacoes'));
        }
    }

    /**
     * Método para filtrar tabelas dentro de um cálculo já gerado
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($filtro = null)
    {
        $filtro = $this->request->data;

        foreach ($filtro as $chave => $valor) {
            if (ctype_digit($chave)) {
                $ids[$chave] = $chave;
            }
        }
        $sessao = $this->request->getSession()->read('Auth.User');

        $conditions = array();
        $this->loadModel('Tabelas');
        if (isset($filtro['tipo_id']) && !empty($filtro['tipo_id']) && $filtro['tipo_id'] <> 6) {
            $array = array("Tabelas.tipo_id" => $filtro['tipo_id']);
            array_push($conditions, $array);
        }
        if (isset($filtro['cobertura_id']) && !empty($filtro['cobertura_id']) && $filtro['cobertura_id'] <> 99) {
            $this->loadModel('TiposProdutos');

            $array = array("Coberturas.id" => $filtro['cobertura_id']);
            array_push($conditions, $array);
        }
        if (isset($filtro['abrangencia_id']) && !empty($filtro['abrangencia_id']) && $filtro['abrangencia_id'] <> 6) {
            switch ($filtro['abrangencia_id']) {
                case "3":
                    $array = array("Tabelas.abrangencia_id IN" => array("1", "2", "3", "4", "5"));
                    break;
                case "4":
                    $array = array("Tabelas.abrangencia_id IN" => array("1", "2", "4", "5"));
                    break;
                case "2":
                    $array = array("Tabelas.abrangencia_id IN" => array("1", "2", "5"));
                    break;
                case "5":
                    $array = array("Tabelas.abrangencia_id IN" => array("1", "5"));
                    break;
                case "1":
                    $array = array("Tabelas.abrangencia_id IN" => array("1"));
                    break;
            }
            array_push($conditions, $array);
        }

        if ($filtro['tipo_contratacao'] === '99') {
            $filtro['tipo_contratacao'] = '';
        }
        if (isset($filtro['tipo_contratacao']) && !empty($filtro['tipo_contratacao'])) {
            if ($filtro['tipo_contratacao'] === '2') {
                $filtro['tipo_contratacao'] = '0';
            }
            $variaveis = array(2, $filtro['tipo_contratacao']);
            $array = array('Tabelas.tipo_contratacao IN' => $variaveis);
            array_push($conditions, $array);
        }

        if ($filtro['filtroreembolso'] === '99') {
            $filtro['filtroreembolso'] = '';
        }
        if (isset($filtro['filtroreembolso']) && !empty($filtro['filtroreembolso'])) {
            $array = array('Tabelas.reembolso' => $filtro['filtroreembolso']);
            array_push($conditions, $array);
        }

        if ($filtro['coparticipacao'] === '99') {
            $filtro['coparticipacao'] = '';
        }
        if (isset($filtro['coparticipacao']) && !empty($filtro['coparticipacao'])) {
            $s = $filtro['coparticipacao'];
            $array = array("Tabelas.coparticipacao" => "$s");
            array_push($conditions, $array);
        }

        $tabelas = $this->Tabelas->find('all', [
            'contain' => [
                'Abrangencias',
                'Tipos',
                "TabelasCnpjs" => ["Cnpjs"],
                'Produtos',
                'Coberturas',
                'Operadoras' => [
                    'Imagens',
                    'Carencias',
                    'Redes',
                    'Regioes',
                    'FormasPagamentos',
                    'Observacoes',
                    'Informacoes',
                    'Opcionais',
                    'Reembolsos'
                ]
            ]
        ])
            ->order([
                'Operadoras.prioridade' => 'ASC',
                'Tabelas.prioridade' => 'ASC'
            ])
            ->where([
                'Tabelas.id IN' => $sessao['tabelas'],
                $conditions
            ]);

        $totalTabelas = count($tabelas->toArray());

        $tabelaspreco = $this->Tabelas->find('all', [
            'contain' => [
                'Abrangencias',
                'Tipos',
                'Estados',
                'Produtos',
                'Coberturas',
                'Operadoras' => [
                    'Imagens',
                    'Carencias',
                    'Redes',
                    'Observacoes',
                    'Informacoes',
                    'Opcionais',
                    'Reembolsos'
                ]
            ]
        ])
            ->order([
                'Tabelas.faixa1' => 'ASC'
            ])
            ->where([
                'Tabelas.id IN' => $sessao['tabelas'],
                $conditions
            ])->toArray();

        if (count($tabelaspreco) > 0) {
            $maisbarato = $tabelaspreco[0];
            $intermediario = $tabelaspreco[count($tabelaspreco) / 2];
            $maiscaro = end($tabelaspreco);
        } else {
            $maisbarato = null;
            $intermediario = null;
            $maiscaro = null;
        }
        $totalsemfiltro = count($ids);
        foreach ($tabelas as $tabela) {
            if ($tabela['coparticipacao'] == 'n') {
                $coparticipacao = 'S/COPARTICIPAÇÃO';
            } else {
                $coparticipacao = 'C/COPARTICIPAÇÃO';
            }
            $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;
        }
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('Tipos');
        $tipos = $this->Tipos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('Abrangencias');
        $abrangencias = $this->Abrangencias->find('list', ['valueField' => 'nome', 'order' => ['prioridade' => 'ASC']])->toArray();

        $this->loadModel('Operadoras');
        //         $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']])->where(["Operadoras.status <>" => "OCULTA"])->toArray();
        $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']])->where('NOT(Operadoras.status <=> \'INATIVA\')')->toArray();

        $this->loadModel('Coberturas');
        $coberturas = $this->Coberturas->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);

        $simulacao = $this->Simulacoes->get($filtro['simulacao_id']);
        $btnCancelar = ['controller' => 'simulacoes', 'action' => 'index'];

        $this->set(compact('simulacao', 'maisbarato', 'intermediario', 'maiscaro', 'coberturas', 'tabelasOrdenadas', 'abrangencias', 'tipos', 'btnCancelar', 'operadoras', 'totalTabelas', 'totalsemfiltro'));
        //        $this->render('simulacao');
    }

    /**
     * Método para filtrar tabelasdentro de um cálculo já gerado
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtropdf($filtro = null)
    {

        $this->loadModel('Filtros');
        $filtro = $this->Filtros->get($filtro, [
            'contain' => ['TabelasFiltradas' => ['Tabelas']]
        ])->toArray();

        foreach ($filtro['tabelas_filtradas'] as $tabela) {
            $tabelasIds[] = $tabela['tabela_id'];
        }
        $this->loadModel('SimulacoesTabelas');
        $simulacoesTabelas = $this->SimulacoesTabelas->find('all')
            ->where(['SimulacoesTabelas.simulacao_id' => $filtro['simulacao_id']])->toArray();

        $this->loadModel('Tabelas');
        $tabelas = $this->Tabelas->find('all', [
            'contain' => [
                'Abrangencias',
                'Tipos',
                'Regioes',
                'Produtos',
                'TiposProdutos',
                'Operadoras' => [
                    'Imagens',
                    'Carencias',
                    'Redes',
                    'Observacoes',
                    'Informacoes',
                    'Opcionais',
                    'Reembolsos'
                ]
            ]
        ])
            ->order([
                'Operadoras.prioridade' => 'ASC',
                'Tabelas.prioridade' => 'ASC'
            ])
            ->where([
                'Tabelas.id IN' => $tabelasIds,
            ])->toArray();
        $totalTabelas = count($tabelas);
        $totalsemfiltro = count($simulacoesTabelas);
        foreach ($tabelas as $tabela) {
            if ($tabela['coparticipacao'] == 'n') {
                $coparticipacao = 'S/COPARTICIPAÇÃO';
            } else {
                $coparticipacao = 'C/COPARTICIPAÇÃO';
            }
            $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;

            //            $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome']] = $tabela;
        }
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('Tipos');
        $tipos = $this->Tipos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('Abrangencias');
        $abrangencias = $this->Abrangencias->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $this->loadModel('Operadoras');
        $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']])->toArray();

        $this->loadModel('TiposProdutos');
        $tipos_produtos = $this->TiposProdutos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']])->toArray();

        $simulacao = $this->Simulacoes->get($filtro['simulacao_id']);
        $btnCancelar = ['controller' => 'simulacoes', 'action' => 'index'];

        $this->set(compact('simulacao', 'filtro', 'tipos_produtos', 'tabelasOrdenadas', 'abrangencias', 'tipos', 'btnCancelar', 'operadoras', 'totalTabelas', 'totalsemfiltro'));
        //        $this->render('filtro');
    }

    public function perfiletario()
    {
        if ($this->request->is(['post'])) {
            $detalhamento = $this->request->data();
            $this->set(compact('detalhamento'));
            $this->render('totalvidas');
        }
    }

    public function colarArquivo()
    {
        $todas = $this->request->data();
        $geral = array();
        $geral['Titulares']['m'] = null;
        $geral['Titulares']['f'] = null;
        $geral['Dependentes']['m'] = null;
        $geral['Dependentes']['f'] = null;
        $geral['Agregados']['m'] = null;
        $geral['Agregados']['f'] = null;
        $geral['Afastados']['m'] = null;
        $geral['Afastados']['f'] = null;

        foreach ($todas as $tipo => $valor) {
            foreach ($valor as $sexo => $todas_datas) {
                $datas = explode(PHP_EOL, $todas_datas);
                foreach ($datas as $data) {
                    $data = str_replace(array("\r", "\n"), '', $data);
                    if (!empty($data)) {
                        // Separa em dia, mês e ano
                        list($dia, $mes, $ano) = explode('/', $data);
                        // Descobre que dia é hoje e retorna a unix timestamp
                        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                        // Descobre a unix timestamp da data de nascimento do fulano
                        $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                        // Depois apenas fazemos o cálculo já citado :)
                        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                        switch ($idade) {
                            case ($idade <= 18 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 1;
                                break;
                            case ($idade >= 19 && $idade <= 23 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 2;
                                break;
                            case ($idade >= 24 && $idade <= 28 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 3;
                                break;
                            case ($idade >= 29 && $idade <= 33 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 4;
                                break;
                            case ($idade >= 34 && $idade <= 38 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 5;
                                break;
                            case ($idade >= 39 && $idade <= 43 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 6;
                                break;
                            case ($idade >= 44 && $idade <= 48 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 7;
                                break;
                            case ($idade >= 49 && $idade <= 53 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 8;
                                break;
                            case ($idade >= 54 && $idade <= 58 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 9;
                                break;
                            case ($idade >= 59 ? $idade : !$idade):
                                $m = count($geral[$tipo][$sexo]) + 1;
                                $geral[$tipo][$sexo][$m]['idade'] = (int) $idade;
                                $geral[$tipo][$sexo][$m]['data_nascimento'] = $data;
                                $geral[$tipo][$sexo][$m]['faixa'] = 10;
                                break;
                        }
                    }
                }
            }
        }
        $geralPorFaixa = array();
        $geralPorFaixa['Titulares']['m'] = null;
        $geralPorFaixa['Titulares']['f'] = null;
        $geralPorFaixa['Dependentes']['m'] = null;
        $geralPorFaixa['Dependentes']['f'] = null;
        $geralPorFaixa['Agregados']['m'] = null;
        $geralPorFaixa['Agregados']['f'] = null;
        $geralPorFaixa['Afastados']['m'] = null;
        $geralPorFaixa['Afastados']['f'] = null;
        foreach ($geral as $tipo => $dados) {
            switch ($tipo) {
                case 'Titulares':
                    $titularesM = count($dados['m']);
                    $titularesF = count($dados['f']);
                    if (isset($dados['m'])) {
                        foreach ($dados['m'] as $sexo => $titM) {
                            $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] = (isset($geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] : 0) + 1;
                        }
                    }
                    if (isset($dados['f'])) {
                        foreach ($dados['f'] as $titF) {
                            $geralPorFaixa[$tipo]['f']['faixa' . $titF['faixa']] = (isset($geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']] : 0) + 1;
                        }
                    }
                    break;
                case 'Dependentes':
                    $dependentesM = count($dados['m']);
                    $dependentesF = count($dados['f']);
                    if (isset($dados['m']))
                        foreach ($dados['m'] as $sexo => $titM) {
                            $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] = (isset($geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] : 0) + 1;
                        }
                    if (isset($dados['f']))
                        foreach ($dados['f'] as $titF) {
                            $geralPorFaixa[$tipo]['f']['faixa' . $titF['faixa']] = (isset($geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']] : 0) + 1;
                        }
                    break;
                case 'Agregados':
                    $agregadosM = count($dados['m']);
                    $agregadosF = count($dados['f']);
                    if (isset($dados['m']))
                        foreach ($dados['m'] as $sexo => $titM) {
                            $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] = (isset($geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] : 0) + 1;
                        }
                    if (isset($dados['f']))
                        foreach ($dados['f'] as $titF) {
                            $geralPorFaixa[$tipo]['f']['faixa' . $titF['faixa']] = (isset($geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']] : 0) + 1;
                        }
                    break;
                case 'Afastados':
                    $afastadosM = count($dados['m']);
                    $afastadosF = count($dados['f']);
                    if (isset($dados['m']))
                        foreach ($dados['m'] as $sexo => $titM) {
                            $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] = (isset($geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['m']['faixa' . $titM['faixa']] : 0) + 1;
                        }
                    if (isset($dados['f']))
                        foreach ($dados['f'] as $titF) {
                            $geralPorFaixa[$tipo]['f']['faixa' . $titF['faixa']] = (isset($geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']]) ? $geralPorFaixa[$tipo]['f']['faixa' . $titM['faixa']] : 0) + 1;
                        }
                    break;
            }
        }
        //        debug($geralPorFaixa);
        //        die();

        $this->set(compact('geral', 'geralPorFaixa', 'afastadosM', 'afastadosF', 'titularesM', 'titularesF', 'dependentesM', 'dependentesF', 'agregadosM', 'agregadosF'));
    }

    public function totalvidas()
    {
        //        debug($qtde);
        if ($this->request->is(['post'])) {
            $todas = $this->request->data();

            $i = 1;
            $titularesM = 0;
            $titularesF = 0;
            $dependentesM = 0;
            $dependentesF = 0;
            $agregadosM = 0;
            $agregadosF = 0;
            $afastadosM = 0;
            $afastadosF = 0;
            $geral = array();
            $titulares = array();
            foreach ($todas as $chave => $grupos) {
                /* CHAVES :
                 * Titulares
                 * Dependentes
                 * Agregados
                 * Afastados
                 */
                foreach ($grupos as $sexo => $grupo) {

                    foreach ($grupo as $data) {
                        if ($data != '') {
                            switch ($chave) {
                                case 'Titulares': //Titulares
                                    switch ($sexo) {
                                        case 'm':
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    break;
                                case 'Dependentes': //Dependentes
                                    switch ($sexo) {
                                        case 'm':
                                            $dependentesM = $dependentesM + 1;
                                            break;
                                        case 'f':
                                            $dependentesF = $dependentesF + 1;
                                            break;
                                    }
                                    break;
                                case 'Agregados': //Agregados

                                    switch ($sexo) {
                                        case 'm':
                                            $agregadosM = $agregadosM + 1;
                                            break;
                                        case 'f':
                                            $agregadosF = $agregadosF + 1;
                                            break;
                                    }
                                    break;
                                case 'Afastados': //Afastados

                                    switch ($sexo) {
                                        case 'm':
                                            $afastadosM = $afastadosM + 1;
                                            break;
                                        case 'f':
                                            $afastadosF = $afastadosF + 1;
                                            break;
                                    }
                                    break;
                            }



                            // Separa em dia, mês e ano
                            list($dia, $mes, $ano) = explode('/', $data);

                            // Descobre que dia é hoje e retorna a unix timestamp
                            $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                            // Descobre a unix timestamp da data de nascimento do fulano
                            $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);

                            // Depois apenas fazemos o cálculo já citado :)
                            $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                            //Verificando faixa de Idade
                            switch ($idade) {
                                case ($idade <= 18 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 1;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 1;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;


                                case ($idade >= 19 && $idade <= 23 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 2;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 2;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 24 && $idade <= 28 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 3;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 3;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 29 && $idade <= 33 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 4;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 4;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 34 && $idade <= 38 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 5;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 5;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 39 && $idade <= 43 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 6;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 6;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 44 && $idade <= 48 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 7;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 7;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 49 && $idade <= 53 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 8;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 8;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 54 && $idade <= 58 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 9;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 9;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 59 && $idade <= 64 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 10;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 10;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 65 && $idade <= 80 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 11;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 11;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                                case ($idade >= 81 ? $idade : !$idade):
                                    switch ($sexo) {
                                        case 'm':
                                            $m = count($geral[$chave]['masc']) + 1;
                                            $geral[$chave]['masc'][$m]['idade'] = (int) $idade;
                                            $geral[$chave]['masc'][$m]['data_nascimento'] = $data;
                                            $geral[$chave]['masc'][$m]['faixa'] = 12;
                                            $titularesM = $titularesM + 1;
                                            break;
                                        case 'f':
                                            $f = count($geral[$chave]['fem']) + 1;
                                            $geral[$chave]['fem'][$f]['idade'] = (int) $idade;
                                            $geral[$chave]['fem'][$f]['data_nascimento'] = $data;
                                            $geral[$chave]['fem'][$f]['faixa'] = 12;
                                            $titularesF = $titularesF + 1;
                                            break;
                                    }
                                    $titulares['total'] = $titulares['total'] + 1;
                                    break;
                            }
                        }
                    }
                }
            }
            debug($geral);
            debug($titularesM);
            debug($titularesF);
            debug($dependentesM);
            debug($dependentesF);
            debug($agregadosM);
            debug($agregadosF);
            debug($afastadosM);
            debug($afastadosF);

            die();
        }
    }

    public function salvaFiltros()
    {
        $id = $this->request->data['simulacao_id'];
        $tabelasSelecionadas = $this->request->data;
        $tabelasSelecionadas['informacao'] = $tabelasSelecionadas['documento'];

        //SALVAR OS FILTROS CAS ESTEJAM SELECIONADOS
        if (!empty($tabelasSelecionadas["abrangencia_id"]) || !empty($tabelasSelecionadas["tipo_id"]) || !empty($tabelasSelecionadas["tipo_produto_id"]) || !empty($tabelasSelecionadas["coparticipacao"]) || !empty($tabelasSelecionadas["tipo_contratacao"]) || !empty($tabelasSelecionadas["filtroreembolso"])) {
            $this->loadModel("Pdffiltros");
            $dadosFiltros["simulacao_id"] = $tabelasSelecionadas["simulacao_id"];
            $dadosFiltros["abrangencia_id"] = $tabelasSelecionadas["abrangencia_id"];
            $dadosFiltros["tipo_id"] = $tabelasSelecionadas["tipo_id"];
            $dadosFiltros["cobertura_id"] = $tabelasSelecionadas["cobertura_id"];
            $dadosFiltros["coparticipacao"] = $tabelasSelecionadas["coparticipacao"];
            $dadosFiltros["tipo_contratacao"] = $tabelasSelecionadas["tipo_contratacao"];
            $dadosFiltros["filtroreembolso"] = $tabelasSelecionadas["filtroreembolso"];

            $entidadeFiltros = $this->Pdffiltros->newEntity();
            $entidadeFiltros = $this->Pdffiltros->patchEntity($entidadeFiltros, $dadosFiltros);
            $pdf_id = $this->Pdffiltros->save($entidadeFiltros);
        }

        foreach ($tabelasSelecionadas as $chave => $tabela) {
            if (
                $chave <> 'coparticipacao' &&
                $chave <> 'tipo_produto_id' &&
                $chave <> 'tipo_id' &&
                $chave <> 'abrangencia_id' &&
                $chave <> 'simulacao_id' &&
                $chave <> 'observacao' &&
                $chave <> 'rede' &&
                $chave <> 'reembolso' &&
                $chave <> 'carencia' &&
                $chave <> 'informacao'
            ) {
                if (substr($chave, 0, 8) === 'checkAll') {
                    unset($tabelasSelecionadas[$chave]);
                } else {
                    if ($tabela == 0 || $tabela == 1) {
                        unset($tabelasSelecionadas[$chave]);
                    } else {
                        $tabelasFiltradas[] = $tabela;
                    }
                }
            }
        }

        if (!empty($tabelasFiltradas)) {

            //* SALVAR FILTROS E TABELAS */
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $userId = $sessao['id'];
            $tabelasSelecionadas['user_id'] = $userId;

            $tabelasSelecionadas['user_id'] = $userId;
            if (isset($pdf_id['id'])) {
                $tabelasSelecionadas['filtro_pdf_id'] = $pdf_id;
            }
            $this->loadModel('Filtros');

            $filtro = $this->Filtros->newEntity();
            $filtro = $this->Filtros->patchEntity($filtro, $tabelasSelecionadas);
            $newFiltro = $this->Filtros->save($filtro);
            //SE SALVAR OS FILTROS SALVE AS TABELAS DOS FILTROS
            if ($newFiltro) {
                $this->loadModel('TabelasFiltradas');
                foreach ($tabelasFiltradas as $chave => $tabelas_filtradas) {

                    $dadosTabelasFiltradas['filtro_id'] = $filtro->id;
                    $dadosTabelasFiltradas['tabela_id'] = $tabelas_filtradas;

                    $tabelasfiltradas = $this->TabelasFiltradas->newEntity();
                    $tabelasfiltradas = $this->TabelasFiltradas->patchEntity($tabelasfiltradas, $dadosTabelasFiltradas);
                    $this->TabelasFiltradas->save($tabelasfiltradas);
                }
            }
            $this->redirect(['action' => 'visualizarCotacao', $newFiltro->id]);
        } else {
            $this->request->session()->write('erro', 'S');
            //$this->Flash->error(__('Selecione os cálculos que deseja imprimir.'));
            return $this->redirect(['action' => 'view/' . $this->request->data['simulacao_id']]);
        }
    }

    public function cotacao($id = null)
    {
        $this->loadModel('Filtros');
        try {
            $filtro = $this->Filtros->get($id);
        } catch (\Exception $e) {
            $this->viewBuilder()->layout('link_tela');
            $this->render("cotacao_expirada");
            return;
        }

        $simulacao = $this->Simulacoes->get($filtro['simulacao_id']);
        $this->loadModel('TabelasFiltradas');
        $findTabelas = $this->TabelasFiltradas->find('list', ['keyField' => 'tabela_id', 'valueField' => 'tabela_id'])->where(['filtro_id' => $id])->toArray();

        $this->loadModel('Tabelas');
        $tabelas = $this->Tabelas->find('all', [
            'contain' => [
                'Abrangencias',
                'Tipos',
                'Estados',
                "TabelasCnpjs" => ["Cnpjs"],
                'Produtos',
                'Coberturas',
                'Operadoras' => [
                    'Imagens',
                    'FormasPagamentos',
                    'Carencias',
                    'Redes',
                    'Observacoes',
                    'Informacoes',
                    'Opcionais',
                    'Reembolsos',
                    'Regioes'
                ]
            ]
        ], [
            'order' => ['Operadoras.nome' => 'ASC']
        ], ['group' => ['Tabelas.operadora_id']])
            ->where([
                'Tabelas.id IN' => $findTabelas
            ])->toArray();

        $simulacao['observaco'] = $filtro['observacao'];
        $simulacao['rede'] = $filtro['rede'];
        $simulacao['reembolso'] = $filtro['reembolso'];
        $simulacao['carencia'] = $filtro['carencia'];
        $simulacao['informacao'] = $filtro['informacao'];


        foreach ($tabelas as $tabela) {
            if ($tabela['coparticipacao'] == 'n') {
                $coparticipacao = 'S/COPARTICIPAÇÃO';
            } else {
                $coparticipacao = 'C/COPARTICIPAÇÃO';
            }
            $tabelasOrdenadas[$tabela['operadora']['id']][$tabela['produto']['nome']][$tabela['nome'] . " - " . $tabela['tipo']['nome'] . " - " . $coparticipacao] = $tabela;
        }

        $this->loadModel('Operadoras');
        $op = $this->Operadoras->find('all', ['contain' => ['Imagens']])->toArray();
        foreach ($op as $o) {
            $operadoras[$o['id']] = $o;
        }

        $user = $this->Simulacoes->Users->get($filtro['user_id']);
        if ($user['imagem_id'] != null) {
            $imagem = $this->Simulacoes->Users->Imagens->get($user['imagem_id']);
        }
        unset($user['password']);

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->set('simulacao', $simulacao);
        $this->set('_serialize', ['simulacao']);
        $this->set(compact('tabelas', 'simulacao', 'tabelasOrdenadas', 'operadoras', 'sessao', 'user', 'imagem', 'filtro', 'findTabelas'));
        $this->viewBuilder()->layout('link_tela');
        $this->render("cotacao");
    }


    public function visualizarCotacao($id = null)
    {
        $this->loadModel('Filtros');
        try {
            $filtro = $this->Filtros->get($id);
        } catch (\Exception $e) {
            $this->viewBuilder()->layout('link_tela');
            $this->render("cotacao_expirada");
            return;
        }

        $simulacao = $this->Simulacoes->get($filtro['simulacao_id']);

        $this->loadModel('TabelasFiltradas');
        $findTabelas = $this->TabelasFiltradas->find('list', ['keyField' => 'tabela_id', 'valueField' => 'tabela_id'])->where(['filtro_id' => $id])->toArray();

        $this->loadModel('Tabelas');
        $tabelas = $this->Tabelas
            ->find('all')
            ->contain([
                'Abrangencias',
                'Tipos',
                'Coberturas',
                'Carencias',
                'Redes',
                'Observacoes',
                'Informacoes',
                'Opcionais',
                'Reembolsos',
                'FormasPagamentos',
                "TabelasCnpjs" => [
                    "Cnpjs"
                ],
                'AreasComercializacoes' => [
                    'Metropoles',
                    'Municipios' => ['Estados'],
                    'Operadoras' => ['Imagens']
                ]
            ])
            ->where(['Tabelas.id IN' => $findTabelas])
            ->toArray();
        $simulacao['observaco'] = $filtro['observacao'];
        $simulacao['rede'] = $filtro['rede'];
        $simulacao['reembolso'] = $filtro['reembolso'];
        $simulacao['carencia'] = $filtro['carencia'];
        $simulacao['informacao'] = $filtro['informacao'];

        $tabelas_ordenadas = [];
        $area_adicionada = [];
        foreach ($tabelas as $tabela) {
            $operadora_id = $tabela['areas_comercializaco']['operadora']['id'];
            @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['areas_comercializaco']['operadora'];

            @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['abrangencia']['nome']] .=  $tabela['nome'] . ' • ';

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            if (!in_array($tabela['areas_comercializaco']['id'], $area_adicionada)) {
                foreach ($tabela['areas_comercializaco']['metropoles'] as $metropoles) {
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['data'] .= $metropoles['nome'] . ', ';
                }
                foreach ($tabela['areas_comercializaco']['municipios'] as $municipio) {
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                }
                $area_adicionada[] = $tabela['areas_comercializaco']['id'];
            }
            unset($tabela['areas_comercializaco']['municipios']);
            unset($tabela['areas_comercializaco']['metropoles']);

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['data'] =  nl2br($tabela['observaco']['descricao']);
            unset($tabela['observaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['data'] =  nl2br($tabela['rede']['descricao']);
            unset($tabela['rede']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
            unset($tabela['reembolsoEntity']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['data'] =  nl2br($tabela['carencia']['descricao']);
            unset($tabela['carencia']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['data'] =  nl2br($tabela['opcionai']['descricao']);
            unset($tabela['opcionai']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['data'] =  nl2br($tabela['informaco']['descricao']);
            unset($tabela['informaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['data'] =  nl2br($tabela['formas_pagamento']['descricao']);
            unset($tabela['formas_pagamento']);

            @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
        }
        $tabelasOrdenadas = $tabelas_ordenadas;
        $this->loadModel('Operadoras');
        $op = $this->Operadoras->find('all', ['contain' => ['Imagens']])->toArray();
        foreach ($op as $o) {
            $operadoras[$o['id']] = $o;
        }

        $user = $this->Simulacoes->Users->get($filtro['user_id']);
        if ($user['imagem_id'] != null) {
            $imagem = $this->Simulacoes->Users->Imagens->get($user['imagem_id']);
        }
        unset($user['password']);

        $session = $this->request->getSession();
        $sessao = $session->read('Auth.User');

        $this->set(compact('tabelas', 'simulacao', 'tabelas_ordenadas', 'tabelasOrdenadas', 'operadoras', 'sessao', 'user', 'imagem', 'filtro', 'findTabelas'));
        $this->viewBuilder()->setLayout('link_tela');
    }
    public function pdfVisualizarCotacao($id = null)
    {
        $this->loadModel('Filtros');
        try {
            $filtro = $this->Filtros->get($id);
        } catch (\Exception $e) {
            $this->viewBuilder()->layout('link_tela');
            $this->render("cotacao_expirada");
            return;
        }

        $simulacao = $this->Simulacoes->get($filtro['simulacao_id']);

        $this->loadModel('TabelasFiltradas');
        $findTabelas = $this->TabelasFiltradas->find('list', ['keyField' => 'tabela_id', 'valueField' => 'tabela_id'])->where(['filtro_id' => $id])->toArray();

        $this->loadModel('Tabelas');
        $tabelas = $this->Tabelas
            ->find('all')
            ->contain([
                'Abrangencias',
                'Tipos',
                'Coberturas',
                'Carencias',
                'Redes',
                'Observacoes',
                'Informacoes',
                'Opcionais',
                'Reembolsos',
                'FormasPagamentos',
                "TabelasCnpjs" => [
                    "Cnpjs"
                ],
                'AreasComercializacoes' => [
                    'Municipios' => ['Estados'],
                    'Operadoras' => ['Imagens']
                ]
            ])
            ->where(['Tabelas.id IN' => $findTabelas])
            ->toArray();
        $simulacao['observaco'] = $filtro['observacao'];
        $simulacao['rede'] = $filtro['rede'];
        $simulacao['reembolso'] = $filtro['reembolso'];
        $simulacao['carencia'] = $filtro['carencia'];
        $simulacao['informacao'] = $filtro['informacao'];

        $tabelas_ordenadas = [];

        foreach ($tabelas as $tabela) {
            $operadora_id = $tabela['areas_comercializaco']['operadora']['id'];
            @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['areas_comercializaco']['operadora'];

            @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['abrangencia']['nome']] .=  $tabela['nome'] . ' • ';

            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['data'] =  $tabela['areas_comercializaco']['nome'] . '* Vide Anexo.';
            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['municipios'] =  $tabela['areas_comercializaco']['municipios'];
            unset($tabela['areas_comercializaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['data'] =  $tabela['observaco']['descricao'];
            unset($tabela['observaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['data'] =  $tabela['rede']['descricao'];
            unset($tabela['rede']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  $tabela['reembolsoEntity']['descricao'];
            unset($tabela['reembolsoEntity']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['data'] =  $tabela['carencia']['descricao'];
            unset($tabela['carencia']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['data'] =  $tabela['opcionai']['descricao'];
            unset($tabela['opcionai']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['data'] =  $tabela['informaco']['descricao'];
            unset($tabela['informaco']);
            // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
            @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['data'] =  $tabela['formas_pagamento']['descricao'];
            unset($tabela['formas_pagamento']);

            @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
        }
        $tabelasOrdenadas = $tabelas_ordenadas;

        $this->loadModel('Operadoras');
        $op = $this->Operadoras->find('all', ['contain' => ['Imagens']])->toArray();
        foreach ($op as $o) {
            $operadoras[$o['id']] = $o;
        }

        $user = $this->Simulacoes->Users->get($filtro['user_id']);
        if ($user['imagem_id'] != null) {
            $imagem = $this->Simulacoes->Users->Imagens->get($user['imagem_id']);
        }
        unset($user['password']);

        $session = $this->request->getSession();
        $sessao = $session->read('Auth.User');


        //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
        $tipoPDF = $this->request->getData("tipopdf");
        switch ($tipoPDF) {
            case "D":
                $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . date('d-m-Y_H:i:s') . '.pdf';
                break;
            case "I":
                $nomePDF = 'Calculo-Saude-PME-' . $simulacao['id'] . date('d-m-Y_H:i:s') . '.pdf';
                break;
            case 'F':
                $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . date('d-m-Y_H:i:s') . '.pdf';
                break;
            default:
                $nomePDF = $_SERVER['DOCUMENT_ROOT'] . 'Calculo-Saude-PME-' . $simulacao['id'] . date('d-m-Y_H:i:s') . '.pdf';
                break;
        }

        $this->viewBuilder()->setLayout('pdf');

        $this->set(compact('tabelas', 'simulacao', 'tabelas_ordenadas', 'tabelasOrdenadas', 'operadoras', 'sessao', 'user', 'imagem', 'filtro', 'findTabelas'));
        $this->set(compact('nomePDF', 'tipoPDF'));
    }
}
