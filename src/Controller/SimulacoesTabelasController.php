<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SimulacoesTabelas Controller
 *
 * @property \App\Model\Table\SimulacoesTabelasTable $SimulacoesTabelas
 */
class SimulacoesTabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Simulacoes', 'Tabelas']
        ];
        $simulacoesTabelas = $this->paginate($this->SimulacoesTabelas);

        $this->set(compact('simulacoesTabelas'));
        $this->set('_serialize', ['simulacoesTabelas']);
    }

    /**
     * View method
     *
     * @param string|null $id Simulacoes Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $simulacoesTabela = $this->SimulacoesTabelas->get($id, [
            'contain' => ['Simulacoes', 'Tabelas']
        ]);

        $this->set('simulacoesTabela', $simulacoesTabela);
        $this->set('_serialize', ['simulacoesTabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $simulacoesTabela = $this->SimulacoesTabelas->newEntity();
        if ($this->request->is('post')) {
            $simulacoesTabela = $this->SimulacoesTabelas->patchEntity($simulacoesTabela, $this->request->data);
            if ($this->SimulacoesTabelas->save($simulacoesTabela)) {
                $this->Flash->success(__('The simulacoes tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The simulacoes tabela could not be saved. Please, try again.'));
            }
        }
        $simulacoes = $this->SimulacoesTabelas->Simulacoes->find('list', ['limit' => 200]);
        $tabelas = $this->SimulacoesTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('simulacoesTabela', 'simulacoes', 'tabelas'));
        $this->set('_serialize', ['simulacoesTabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Simulacoes Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $simulacoesTabela = $this->SimulacoesTabelas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $simulacoesTabela = $this->SimulacoesTabelas->patchEntity($simulacoesTabela, $this->request->data);
            if ($this->SimulacoesTabelas->save($simulacoesTabela)) {
                $this->Flash->success(__('The simulacoes tabela has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The simulacoes tabela could not be saved. Please, try again.'));
            }
        }
        $simulacoes = $this->SimulacoesTabelas->Simulacoes->find('list', ['limit' => 200]);
        $tabelas = $this->SimulacoesTabelas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('simulacoesTabela', 'simulacoes', 'tabelas'));
        $this->set('_serialize', ['simulacoesTabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Simulacoes Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $simulacoesTabela = $this->SimulacoesTabelas->get($id);
        if ($this->SimulacoesTabelas->delete($simulacoesTabela)) {
            $this->Flash->success(__('The simulacoes tabela has been deleted.'));
        } else {
            $this->Flash->error(__('The simulacoes tabela could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
