<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Controller\AppController;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Routing\DispatcherFilter;

use Illuminate\Http\Request;
use Cake\Event\Event;
use Rest\Controller\RestController;
use Cake\Http\Response;

 use App\Controller\SisPermissoesController;

// use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SisConfiguracoesController extends AppController
{

    public function index()
    {
        
    }

    public function permissions($modal = 0)
    {
        // PEGA PERMISSOES
        $sisPermissionsCtlr = new \App\Controller\SisPermissoesController;
        $dadosPermissions = $sisPermissionsCtlr->index();

        $this->loadModel('Users');
        $users = $this->Users->find('all')->toArray();

        $totalUsers = COUNT($users);
        $usersAtivos = $usersInativos = 0;
        foreach($users as $v){
            if ($v->ativo != 'Sim') {
                $usersInativos++;
            } else {
                $usersAtivos++;
            }
        }

        // PEGA GRUPOS
        $this->loadModel('SisGrupos');
        $dadosGrupos = $this->SisGrupos->find('all')->toArray();
        $gruposAtivos = $gruposInativos = 0;
        foreach($dadosGrupos as $v){
            if ($v->ativo != 'Sim') {
                $gruposInativos++;
            } else {
                $gruposAtivos++;
            }
        }

        // PEGA FUNCIONLIDADES
        $this->loadModel('SisFuncionalidades');
        $dadosFuncionalidades =  $this->SisFuncionalidades->find('all')->toArray();
        $funcionalidadesAtivos = $funcionalidadesInativos = $funcionalidadesSemPermissao =  0;
        foreach($dadosFuncionalidades as $v){
            if ($v->ativo != 'Sim') {
                $funcionalidadesInativos++;
            } else {
                $funcionalidadesAtivos++;
            }
            if ($v->countPermissionsAssociated == 0) {
                $funcionalidadesSemPermissao++;
            } 
        }

        // PEGA MÓDULOS
        $this->loadModel('SisModulos');
        $dadosModulos = $this->SisModulos->find('all')->toArray();
        $modulosAtivos = $modulosInativos = $moduloSemFuncionalidade = 0;
        foreach($dadosModulos as $v){
            if ($v->ativo != 'Sim') {
                $modulosInativos++;
            } else {
                $modulosAtivos++;
            }
            if ($v->funcionalidadesAssociadas == 0) {
                $moduloSemFuncionalidade++;
            } 
        }
        // PEGA USUÁRIO
        $dadosUser = $this->request->session()->read('Auth.User');
        
        // ENVIA DADOS PARA A VIEW
        $this->set(compact('modal', 'dadosFuncionalidades','funcionalidadesAtivos', 'funcionalidadesInativos', 'funcionalidadesSemPermissao', 'dadosPermissions', 'dadosModulos', 'dadosGrupos', 'dadosUser'));
        $this->set(compact('dadosModulos', 'modulosAtivos', 'modulosInativos', 'moduloSemFuncionalidade'));
        $this->set(compact('users', 'usersAtivos', 'usersInativos', 'totalUsers'));
        $this->set(compact('gruposAtivos', 'gruposInativos'));
    }


}
