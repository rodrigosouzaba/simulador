<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use Illuminate\Http\Request;
use Cake\Event\Event;
use Rest\Controller\RestController;
use Cake\Http\Response;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SisFuncionalidadesController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'permissions']);
    }

    public function index($grid = 0) {
        // CARREGA FUNÇÕES BÁSICAS DE PESQUISA E ORDENAÇÃO
        $options = parent::_index();

        // CONFIGURA O MODEL
        // $this->Funcionalidade->recursive = 2;
        // $this->Funcionalidade->Modulo->unbindModel(array('hasMany' => array('Funcionalidade')));
        // $this->Funcionalidade->Group->unbindModel(array('hasMany' => array('User')));

        // PEGA FUNCIONALIDADES CADASTRADAS
       $dados = $this->SisFuncionalidades->find('all')->toArray();
       
        // INICIALIZA VARIÁVEIS
        // $semPermissao = $ativo = $inativo = 0;

        // // ESTILIZA LINHA DA GRID
        // foreach ($dados as $k => $v) {
        //     if ($v->ativo != 'Sim' || $v->totalPermissoes == 0) {
        //         $dados[$k]->style = 'background-color: #F2C9CA';
        //         $inativo++;
        //     } else {
        //         $ativo++;
        //     }

        //     if ($v->totalPermissoes == 0) {
        //         $semPermissao++;
        //     }
        // }

        // PERMISSÕES NÃO VINCULADAS
        $permissoesNaoVinculadas = $this->SisFuncionalidade->findPermissoesNaoVinculadas();

        // PREPARA DADOS
        foreach ($dados as $k => $v) {
            $v['Funcionalidade']['modulo_active'] = $v['Modulo']['active'];
            $records[$v['Funcionalidade']['modulo']][] = $v['Funcionalidade'];
            unset($v['Funcionalidade']['modulo']);
        }

        // CALCULA O TOTAL DE REGISTROS
        $total = count($dados);

        // RETORNA DADOS
        return compact('permissoesNaoVinculadas', 'total', 'records', 'ativo', 'inativo', 'semPermissao');
    }

    public function add() {
        // CONFIGURA LAYOUT
        $this->layout = 'ajax';

        
        if ($this->request->is('post')) {
            $funcionalidade = $this->SisFuncionalidades->newEntity();

            $funcionalidade = $this->SisFuncionalidades->patchEntity($funcionalidade, $this->request->data);
            if ($this->SisFuncionalidades->save($funcionalidade)) {
                $this->Flash->success('Registro salvo com sucesso.', [
                    'params' => [
                        'class' => 'alert-success',
                    ]
                ]);
            } else {
                $this->Flash->error(__('Não foi possível salvar o registro. Favor tentar novamente.'));
            }
        }

        $this->loadModel('SisModulos');
        $optionsModulos = $this->SisModulos->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();

        $this->loadModel('SisPermissoes');
        $optionsPermissions = $this->SisPermissoes->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();

        $this->loadModel('SisGrupos');
        $optionsGrupos = $this->SisGrupos->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();
        
        $selectedGrupos = [];
        $this->set(compact('selectedGrupos', 'grupos', 'optionsPermissions', 'optionsGrupos', 'optionsModulos'));
        $this->set('_serialize', ['funcionalidade']);
    }

    public function edit($id = null) {
        // CONFIGURA LAYOUT
        $this->layout = 'ajax';

        if (!$this->SisFuncionalidades->exists(['SisFuncionalidades.id' => $id])) {
            $this->Flash->error(__('Registro inexistente'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            
            $funcionalidade = $this->SisFuncionalidades->get($id);
            $funcionalidade = $this->SisFuncionalidades->patchEntity($funcionalidade, $this->request->data);

            if ($this->SisFuncionalidades->save($funcionalidade)) {
                $this->Flash->success('Registro salvo com sucesso.', [
                    'class' => 'alert-success',                    
                ] );
            } else {
                $this->Flash->error(__('Não foi possível editar o registro. Favor tentar novamente.'));
            }
        }else{

            // CONFIGURA LISTA INICIAL DE PERMISSÕES
            $this->request->data = $this->SisFuncionalidades->get($id)->toArray();
            //  debug($this->request->data);
            $dadosPermissoes = $this->SisFuncionalidades->SisFuncionalidadesSisPermissoes->find('all')->where(['SisFuncionalidadesSisPermissoes.sis_funcionalidade_id' => $id])->toArray();
            
            $aux = [];
            foreach ($dadosPermissoes as $v)
                $aux[] = $v->sis_permissao_id;
             //   debug($aux);
            $this->set('selectedPermissions', $aux);

            $this->loadModel('SisPermissoes');
            $this->set('optionsPermissions', $this->SisPermissoes->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray());

            // CONFIGURA LISTA INICIAL DE GRUPOS
            $dadosGrupos = $this->SisFuncionalidades->SisFuncionalidadesSisGrupos->find('all')->where(['sis_funcionalidade_id' => $id])->toArray();
            foreach ($dadosGrupos as $v)
                $aux[] = $v->sis_grupo_id;
            $this->set('selectedGrupos', $aux);

            // PEGA LISTA DE GRUPOS
            $this->loadModel('SisGrupos');
            $this->set('optionsGrupos', $this->SisGrupos->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray());

            // PEGA LISTA DE MÓDULOS
            $this->loadModel('SisModulos');
            $this->set('optionsModulos', $this->SisModulos->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray());
        }
    }

    public function delete($id = null) {
        parent::_delete($id);
    }


}
