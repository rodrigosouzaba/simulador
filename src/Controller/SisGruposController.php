<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use Illuminate\Http\Request;
use Cake\Event\Event;
use Rest\Controller\RestController;
use Cake\Http\Response;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SisGruposController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    public function index($modal = 0) {
        // CARREGA FUNÇÕES BÁSICAS DE PESQUISA E ORDENAÇÃO
        #$options = parent::_index();

        // PEGA FUNCIONALIDADES CADASTRADAS
        $dados = $this->SisGrupos->find('all')->toArray();
        // debug($dados);
        // INICIALIZA VARIÁVEIS
        $semFuncionalidades = $semUsuarios = $ativo = $inativo = 0;

        // ESTILIZA LINHA DA GRID
        foreach ($dados as $v) {
            if ($v->active == 0){
                $ativo++;
            }else{
                $inativo++;
            }
        }

        $this->set(compact('total', 'dados', 'ativo', 'inativo', 'semFuncionalidades', 'semUsuarios','modal'));

    }

    public function add() {
        // CONFIGURA LAYOUT
        $this->layout = 'ajax';

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['name_wp'] = str_replace(' ', '-', strtolower($this->request->data['name']));
            
            $sisGrupos = $this->SisGrupos->newEntity();
            $sisGrupos = $this->SisGrupos->patchEntity($sisGrupos, $this->request->data);

            if ($this->SisGrupos->save($sisGrupos)) {
                $this->Flash->success('Registro salvo com sucesso.');
            } else {
                $this->Flash->error(__('Não foi possível salvar o registro. Favor tentar novamente.'));
            }
        }

        // PEGA LISTA DE FUNCIONALIDADES
        $this->loadModel('SisFuncionalidades');
        $optionsFuncionalidades = $this->SisFuncionalidades->find('list');

        $this->set(compact('optionsFuncionalidades'));
    }

    public function edit($id = null) {
        // CONFIGURA LAYOUT
        $this->layout = 'ajax';

        $this->loadModel('SisGrupos');
        if (!$this->SisGrupos->exists(['SisGrupos.id' => $id])) {
            $this->Flash->error(__('Registro inexistente'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            $grupo = $this->SisGrupos->get($id);
            $grupo = $this->SisGrupos->patchEntity($grupo, $this->request->data);

            if ($this->SisGrupos->save($grupo)) {
                $this->Flash->success('Registro salvo com sucesso.');
            } else {
                $this->Flash->error(__('Não foi possível salvar o registro. Favor tentar novamente.'));
            }
        }
        
        $this->request->data = $this->SisGrupos->get($id)->toArray();
        
        $this->loadModel('SisFuncionalidadesSisGrupos');
        $funcGrupos = $this->SisFuncionalidadesSisGrupos->find('all',[
                'fields' => ['SisFuncionalidadesSisGrupos.sis_funcionalidade_id'],
                'conditions' => ['SisFuncionalidadesSisGrupos.sis_grupo_id' => $id]
            ])->toArray();

        $aux = [];
        foreach ($funcGrupos as $v) {
            $aux[] = $v->sis_funcionalidade_id;
        }
        $this->set('selectedFuncionalidades', $aux);

        $this->loadModel('UsersSisGrupos');
        $arrUsersSisGrupos = $this->UsersSisGrupos->find('all',[
                'fields' => ['user_id'],
                'conditions' => ['sis_grupo_id' => $id]
        ])->toArray();

        $auxArr = [];
        foreach ($arrUsersSisGrupos as $v) {
            $auxArr[] = $v->user_id;
        }

        $this->set('selectedUsers', $auxArr);

        // PEGA LISTA DE FUNCIONALIDADES
        $this->loadModel('SisFuncionalidades');
        $optionsFuncionalidades = $this->SisFuncionalidades->find('list');

        $this->loadModel('Users');
        $optionsUsers = $this->Users->find('list',[
                    'keyField' => 'id',
                    'valueField' => 'fullname'
        ]);
        // var_dump($optionsUsers);
        $this->set(compact('optionsFuncionalidades', 'optionsUsers'));
    }

    public function delete($id = null) {
        parent::_delete($id, true);
    }

   


}
