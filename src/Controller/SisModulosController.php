<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use Illuminate\Http\Request;
use Cake\Event\Event;
use Rest\Controller\RestController;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SisModulosController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'permissions']);
    }

    public function index($modal = 0) {   
        // CARREGA FUNÇÕES BÁSICAS DE PESQUISA E ORDENAÇÃO
        # $options = parent::_index();
        
        // CONFIGURA O MODEL
        # $this->SisModulos->recursive = 1;
        
        // PEGA MÓDULOS CADASTRADOS
        $dados = $this->SisModulos->find('all')->toArray();
        
        // INICIALIZA VARIÁVEIS
        $semFuncionalidade = $ativo = $inativo = 0;

        // ESTILIZA LINHA DA GRID
        foreach($dados as $k => $v){
            if ($v->ativo != 'Sim') {
                $dados[$k]['SisModulos']['style'] = 'background-color: #F2C9CA';
                $inativo++;
            } else {
                $ativo++;
            }

            if ($v['SisModulos']['totalFuncionalidades'] == 0){
                $semFuncionalidade++;
            }
        }

        // CALCULA O TOTAL DE REGISTROS
        $total = count($dados);
        
        // ENVIA DADOS PARA A VIEW
        if($modal == 0){
            return compact('total','dados','ativo','inativo','semFuncionalidade');
        } else {
            $this->set(compact('total','dados','ativo','inativo','semFuncionalidade','modal'));
        }
    }
    
    public function add() {
        // CONFIGURA LAYOUT
        $this->layout = 'ajax';

        if ($this->request->is('post') || $this->request->is('put')) {

            $modulo = $this->SisModulos->newEntity();
            $modulo = $this->SisModulos->patchEntity($modulo, $this->request->data);

            if ($this->SisModulos->save($modulo)) {
                $this->Flash->success('Registro salvo com sucesso.');
            } else {
                $this->Flash->error(__('Não foi possível salvar o registro. Favor tentar novamente.'));
            }
        }
        
        // PEGA LISTA DE FUNCIONALIDADES
        $this->loadModel('SisFuncionalidades');
        $this->set('optionsFuncionalidades', $this->SisFuncionalidades->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['SisFuncionalidades.active'=>1])->toArray() );
    }
    
    public function edit($id = null) {
        // CONFIGURA LAYOUT
        $this->layout = 'ajax';

        // $sisModulos = TableRegistry::get('SisModulos');
        $this->loadModel('SisModulos');
        if (!$this->SisModulos->exists(['SisModulos.id' => $id])) {
            $this->Flash->error(__('Registro inexistente'));
        }
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $modulos = $this->SisModulos->get($id);

            $modulos = $this->SisModulos->patchEntity($modulos, $this->request->data);
            if ($this->SisModulos->save($modulos)) {
                $this->Flash->success('Registro salvo com sucesso.');
            } else {
                $this->Flash->error(__('Não foi possível salvar o registro. Favor tentar novamente.'));
            }
        }
        
        $this->request->data = $this->SisModulos->get($id)->toArray(); 

        $this->loadModel('SisFuncionalidades');
        $funcionalidades = $this->SisFuncionalidades->find('all')->where(['SisFuncionalidades.sis_modulo_id' => $id])->toArray();
        
        $aux = [];
        foreach($funcionalidades as $v){
             $aux[] = $v->id;
        }
        $this->set('selectedFuncionalidades', $aux);

        // PEGA LISTA DE FUNCIONALIDADES
        $this->loadModel('SisFuncionalidades');
        $this->set('optionsFuncionalidades', $this->SisFuncionalidades->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['SisFuncionalidades.active'=>1])->toArray() );
    }
    
    public function delete($id = null) {
        parent::_delete($id);
    }


}
