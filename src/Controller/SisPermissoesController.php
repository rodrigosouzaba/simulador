<?php

namespace App\Controller;

use App\Controller\AppController;

use Cake\View\ViewBuilder;
use Cake\I18n\Time;
use Cake\Event\Event;
// use App\Model\Entity\PermissionsTabela;

use Cake\ORM\TableRegistry;

/**
 * CakePHP Permissions
 * @author Anderson
 */
class SisPermissoesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('ControllerList');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow(['add', 'index']);
    }

    public function index()
    {

        // INICIALIZA VARIÁVEIS
        $semVinculoComFuncionalidade = [];
        $adicionar = [];
        $remover = [];

        // PEGA PERMISSÕES CADASTRADAS
        $database_permissions = $this->SisPermissoes->find('all')->toArray();

        // CALCULA O TOTAL DE PERMISSÕES CADASTRADAS
        $total = count($database_permissions);

        // PEGA PERMISSÕES DISPONÍVEIS
        $controller_permissions = $this->ControllerList->get();

        // PREPARA PERMISSÕES DISPONÍVEIS
        $aux = [];
        foreach ($controller_permissions as $k => $v) {
            $nameController = array_keys($v)[0];
            foreach ($v as $m) {
                foreach ($m as $a)
                    $aux[] = lcfirst($nameController) . '.' . $a;
            }
        }

        $controller_permissions = $aux;
        // VERIFICA SE A PERMISSÃO EXISTE NO CONTROLLER
        foreach ($database_permissions as $k => $v) {
            if (is_array($v['Permission']) && $v['Permission']['totalFuncionalidades'] == 0) {
                $semVinculoComFuncionalidade[] = $database_permissions[$k]->name;
            }
        }

        // PREPARA PERMISSÕES CADASTRADAS
        $aux = [];

        foreach ($database_permissions as $k => $v) {
            $aux[] = $v->name;
        }
        $database_permissions = $aux;

        $remover = array_diff($database_permissions, $controller_permissions);
        $adicionar = array_diff($controller_permissions, $database_permissions);

        // ENVIA PARA A VIEW O TOTAL DE PERMISSÕES A ADICIONAR
        return compact('adicionar', 'remover', 'semVinculoComFuncionalidade', 'total');
    }

    public function add()
    {
        // PEGA DADOS
        $dados = $this->index();

        // INICIARLIZA VARIÁVEIS
        $adicionar = $dados['adicionar'];
        $remover = $dados['remover'];


        // VERFICA SE EXISTEM PERMISSÕES A CADASTRAR
        if (count($adicionar)) {
            $permissions = [];
            foreach ($adicionar as $v) {
                $permissionTable = TableRegistry::getTableLocator()->get('SisPermissoes');
                $permission = $permissionTable->newEntity();

                $permission->name = $v;
                $permissionTable->save($permission);
            };
        }

        // VERFICA SE EXISTEM PERMISSÕES A DELETAR
        if (count($remover)) {

            $permissionTable = TableRegistry::getTableLocator()->get('SisPermissoes');

            foreach ($remover as $v) {
                $res['Permission.name'] = $v;
                $permissionTable->deleteAll($res);
            }

            // $this->Permission->deleteAll(['name'=>$remover]);
        }
        // ENVIA DADOS PARA A VIEW 
        $this->set(compact('adicionar', 'remover'));
    }
}
