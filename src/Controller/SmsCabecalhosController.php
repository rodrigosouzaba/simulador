<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * SmsCabecalhos Controller
 *
 * @property \App\Model\Table\SmsCabecalhosTable $SmsCabecalhos
 */
class SmsCabecalhosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $smsCabecalhos = $this->paginate($this->SmsCabecalhos);

        $this->set(compact('smsCabecalhos'));
        $this->set('_serialize', ['smsCabecalhos']);
    }

    /**
     * View method
     *
     * @param string|null $id Sms Cabecalho id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smsCabecalho = $this->SmsCabecalhos->get($id, [
            'contain' => []
        ]);

        $this->set('smsCabecalho', $smsCabecalho);
        $this->set('_serialize', ['smsCabecalho']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $smsCabecalho = $this->SmsCabecalhos->newEntity();
        if ($this->request->is('post')) {
            $smsCabecalho = $this->SmsCabecalhos->patchEntity($smsCabecalho, $this->request->data);
            if ($this->SmsCabecalhos->save($smsCabecalho)) {
                $this->Flash->success(__('The sms cabecalho has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sms cabecalho could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('smsCabecalho'));
        $this->set('_serialize', ['smsCabecalho']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sms Cabecalho id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smsCabecalho = $this->SmsCabecalhos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smsCabecalho = $this->SmsCabecalhos->patchEntity($smsCabecalho, $this->request->data);
            if ($this->SmsCabecalhos->save($smsCabecalho)) {
                $this->Flash->success(__('Cabeçalho editado com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'Smsmarketing']);
            } else {
                $this->Flash->error(__('O cabeçalho não foi salvo. Tente novamente.'));
            }
            return $this->redirect(['controller' => 'Users', 'action' => 'Smsmarketing']);
        }
        $this->set(compact('smsCabecalho'));
        $this->set('_serialize', ['smsCabecalho']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sms Cabecalho id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smsCabecalho = $this->SmsCabecalhos->get($id);
        if ($this->SmsCabecalhos->delete($smsCabecalho)) {
            $this->Flash->success(__('The sms cabecalho has been deleted.'));
        } else {
            $this->Flash->error(__('The sms cabecalho could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
