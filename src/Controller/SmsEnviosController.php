<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Http\Client;
use Cake\I18n\FrozenTime;

/**
 * SmsEnvios Controller
 *
 * @property \App\Model\Table\SmsEnviosTable $SmsEnvios
 */
class SmsEnviosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        /*
        $this->paginate = [
            'contain' => ['SmsListas', 'Users', 'SmsSituacoes']
        ];
        $smsEnvios = $this->paginate($this->SmsEnvios);

        $this->set(compact('smsEnvios'));
        $this->set('_serialize', ['smsEnvios']);
*/

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');


        $envios = $this->SmsEnvios->find('all')->where(['sms_situacao_id' => 2])->contain(['SisGrupos', 'Users', 'SmsSituacoes'])->order(['SmsEnvios.data_agendamento' => 'DESC']);

        $envios_finalizados = $this->SmsEnvios->find('all')->where(['sms_situacao_id' => 1])->contain(['SisGrupos', 'Users', 'SmsSituacoes'])->order(['SmsEnvios.data_agendamento' => 'DESC']);

        $this->set(compact('envios', 'envios_finalizados', 'sessao'));
        $this->set('_serialize', ['envios']);
    }

    /**
     * View method
     *
     * @param string|null $id Sms Envio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smsEnvio = $this->SmsEnvios->get($id, [
            'contain' => ['SmsListas', 'Users', 'SmsSituacoes', 'SmsRespostasEnvios']
        ]);

        $this->set('smsEnvio', $smsEnvio);
        $this->set('_serialize', ['smsEnvio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $smsEnvio = $this->SmsEnvios->newEntity();
        if ($this->request->is('post')) {
            $smsEnvio = $this->SmsEnvios->patchEntity($smsEnvio, $this->request->data);
            if ($this->SmsEnvios->save($smsEnvio)) {
                $this->Flash->success(__('The sms envio has been saved.'));

                return $this->redirect(['controller' => 'SmsEnvios', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The sms envio could not be saved. Please, try again.'));
            }
        }
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $grupos = $this->SmsEnvios->SisGrupos->find('list', ['valueField' => 'name']);
        // $smsListas = $this->SmsEnvios->SmsListas->find('list', ['limit' => 200, "valueField" => "descricao"]);
        $smsCabecalhos = $this->SmsEnvios->SmsCabecalhos->find('list', ['limit' => 200, "valueField" => "nome"]);
        $users = $this->SmsEnvios->Users->find('list', ['limit' => 200]);
        $smsSituacoes = $this->SmsEnvios->SmsSituacoes->find('list', ['limit' => 200]);
        $this->set(compact('sessao', 'smsEnvio', 'users', 'smsSituacoes', 'smsCabecalhos', 'grupos'));
        $this->set('_serialize', ['smsEnvio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sms Envio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smsEnvio = $this->SmsEnvios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smsEnvio = $this->SmsEnvios->patchEntity($smsEnvio, $this->request->data);
            if ($this->SmsEnvios->save($smsEnvio)) {
                $this->Flash->success(__('A campanha foi alterada com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'smsmarketing']);
            } else {
                $this->Flash->error(__('The sms envio could not be saved. Please, try again.'));
            }
        }
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $user_id = $session->read('Auth.User.id');
        $smsCabecalhos = $this->SmsEnvios->SmsCabecalhos->find('list', ['limit' => 200, "valueField" => "nome"]);
        $grupos = $this->SmsEnvios->SisGrupos->find('list', ['valueField' => 'name']);
        $smsSituacoes = $this->SmsEnvios->SmsSituacoes->find('list', ['valueField' => 'nome']);
        $this->set(compact('smsEnvio', 'grupos', 'smsSituacoes', 'user_id', 'smsCabecalhos', 'sessao'));
        $this->set('_serialize', ['smsEnvio']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Sms Envio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smsEnvio = $this->SmsEnvios->get($id);
        $this->SmsEnvios->delete($smsEnvio);

        return $this->redirect(['action' => 'index']);
    }


    /**
     * Autorizar method
     *
     * @param string|null $id Envio id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function autorizar()
    {
        $id = $this->request->data['id'];
        $tipo_envio = $this->request->data['tipo_envio'];

        $envio = $this->SmsEnvios->get($id, [
            'contain' => []
        ]);
        $envio->autorizado = 1;
        $envio->tipo_envio = $tipo_envio;
        $envio = $this->SmsEnvios->save($envio);
        debug($envio);
    }


    /*

	Método de envio via ZENVIA

	*/

    public function zenvia($id = null)
    {
        $dados = $this->SmsEnvios->get($id, ['contain' => ['SisGrupos' => ['Users']]]);

        if ($dados) {

            $caracteres = array("(", ")", " ", "-");

            // $lista = file(WWW_ROOT . $dados['sms_lista']['caminho'] . $dados['sms_lista']['nome']);
            $lista = $dados->sis_grupo->users;

            $i = 0;
            $msg = $dados['mensagem'];
            $envio = array();
            $envio["sendSmsMultiRequest"]["aggregateId"] = "32807";
            $this->loadModel("SmsRespostasEnvios");
            foreach ($lista as $envio) {

                $destinatario = str_replace($caracteres, '', $envio->celular);
                $dest = '55' . trim($destinatario);
                $dadosEnvio[$i]["to"] = $dest;
                $dadosEnvio[$i]["msg"] = $dados["mensagem"];

                $resposta["sms_envio_id"] = $dados["id"];
                $resposta["celular"] = $dest;
                $resposta["data"] = date("Y-m-d H:i:s");
                $resposta["resultado"] = "zenvia";

                $resposta_envio_entidade = $this->SmsRespostasEnvios->newEntity();
                $resposta_envio_entidade = $this->SmsRespostasEnvios->patchEntity($resposta_envio_entidade, $resposta);
                $this->SmsRespostasEnvios->save($resposta_envio_entidade);

                $dadosEnvio[$i]["id"] = $resposta_envio_entidade->id;


                $data = new FrozenTime($dados['data_agendamento']);

                $hora = $data->format("H:i:s");
                $data = $data->format("Y-m-d");
                $dados['data_agendamento'] = $data . "T" . $hora;

                $dadosEnvio[$i]["schedule"] = $dados['data_agendamento'];

                $i++;
            }

            $json = json_encode([
                "sendSmsMultiRequest" => [
                    "aggregateId" => "32807",
                    "sendSmsRequestList" =>
                    $dadosEnvio
                ]
            ]);

            $http = new Client();
            $response = $http->post('https://api-rest.zenvia.com/services/send-sms-multiple', $json, [
                'headers' => [
                    'Content-Type' => ' application/json',
                    'Authorization' => ' Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==',
                    'Accept' => 'application/json'
                ]
            ]);

            //ALTERAR STATUS E SALVA DATA DO ENVIO
            $envioSucesso = $this->SmsEnvios->get($dados["id"]);
            $envioSucesso->data_envio = date("Y-m-d H:i:s");
            $envioSucesso->sms_situacao_id = 1;
            $envioSucesso->tipo_envio = "zenvia";
            $this->SmsEnvios->save($envioSucesso);
        } else {
            $this->Flash->error(__('Campanha Não encontrada'));
            return $this->redirect(['action' => 'index']);
        }
    }

    /*

	Método de resposta do envio ZENVIA

	*/

    public function zenviaResposta($smsEnvioId = null)
    {

        $dados = $this->SmsEnvios->find("all")->contain(['SmsRespostasEnvios'])->where(["SmsEnvios.id" => $smsEnvioId])->first()->toArray();
        foreach ($dados["sms_respostas_envios"] as $respostas) {
            $ch = curl_init("https://api-rest.zenvia.com/services/get-sms-status/" . $respostas["id"]);

            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==', 'Accept:application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $result = json_decode($result);
            debug($result);
        }
        die();
    }

    public function voxmundi()
    {

        $dados = $this->SmsEnvios->find('all')->contain(['SmsListas'])->where([
            'SmsEnvios.autorizado' => 1,
            'SmsEnvios.tipo_envio' => "modem",
            'SmsEnvios.sms_situacao_id <>' => 1
        ])->toArray();
        //die;
        if ($dados) {
            foreach ($dados as $dados) {

                $caracteres = array("(", ")", " ", "-");

                $lista = file(WWW_ROOT . $dados['sms_lista']['caminho'] . $dados['sms_lista']['nome']);

                $i = 0;
                $msg = $dados['mensagem'];
                $envio = array();
                $envio["sendSmsMultiRequest"]["aggregateId"] = "32807";
                $this->loadModel("SmsRespostasEnvios");
                foreach ($lista as $envio) {

                    $detalhes = explode(";", $envio);
                    /*
		             * $detalhes[0] = celular
		             * $detalhes[1] = apelido
		             * $detalhes[2] = data de nascimento
		             *
                     */

                    $destinatario = str_replace('"', '', $detalhes[0]);
                    $destinatario = str_replace(" ", '', $destinatario);
                    $destinatario = str_replace("(", '', $destinatario);
                    $destinatario = str_replace(")", '', $destinatario);
                    $destinatario = str_replace("-", '', $destinatario);
                    $destinatario = str_replace(";", '', $destinatario);
                    $dest = trim($destinatario);

                    $dadosEnvio[$i]["to"] = $dest;
                    $dadosEnvio[$i]["msg"] = $dados["mensagem"];

                    $resposta["sms_envio_id"] = $dados["id"];
                    $resposta["celular"] = $dest;
                    $resposta["data"] = date("Y-m-d H:i:s");
                    $resposta["resultado"] = "zenvia";

                    $resposta_envio_entidade = $this->SmsRespostasEnvios->newEntity();
                    $resposta_envio_entidade = $this->SmsRespostasEnvios->patchEntity($resposta_envio_entidade, $resposta);
                    $this->SmsRespostasEnvios->save($resposta_envio_entidade);

                    $dadosEnvio[$i]["id"] = $resposta_envio_entidade->id;


                    $data = new FrozenTime($dados['data_agendamento']);

                    $hora = $data->format("H:i:s");
                    $data = $data->format("Y-m-d");
                    $dados['data_agendamento'] = $data . "T" . $hora;

                    $dadosEnvio[$i]["schedule"] = $dados['data_agendamento'];
                    $http = new Client();
                    $response = $http->get("http://187.44.189.118:5038/sendsms?username=admin&password=admin&phonenumber=" . $dest . '&message=' . urlencode($dados['mensagem']), [
                        'headers' => [
                            'Accept' => 'application/json'
                        ]
                    ]);
                    $i++;
                    debug($response);
                }
                //ALTERAR STATUS E SALVA DATA DO ENVIO
                $envioSucesso = $this->SmsEnvios->get($dados["id"]);
                $envioSucesso->data_envio = date("Y-m-d H:i:s");
                $envioSucesso->sms_situacao_id = 1;
                $envioSucesso->tipo_envio = "modem";
                $this->SmsEnvios->save($envioSucesso);
            }
        }
        return $this->redirect(['controller' => 'Users', 'action' => 'smsmarketing']);
    }
    public function cancelaEnvio()
    {
        $id = $this->request->data['id'];
        $campanha = $this->SmsEnvios->get($id);
        $campanha->sms_situacao_id = 2;
        $campanha->autorizado = null;
        $this->SmsEnvios->save($campanha);

        $mensagens = $this->SmsEnvios->SmsRespostasEnvios->find('list')->where(['sms_envio_id' => $id])->toArray();
        foreach ($mensagens as $mensagem) {

            $http = new Client();
            $response = $http->post('https://api-rest.zenvia.com/services/cancel-sms/' . $mensagem, [
                'headers' => [
                    'Content-Type' => ' application/json',
                    'Authorization' => ' Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ=='
                ]
            ]);
            debug($response);
        }
        die;
    }
}
