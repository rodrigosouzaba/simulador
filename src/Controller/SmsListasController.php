<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * SmsListas Controller
 *
 * @property \App\Model\Table\SmsListasTable $SmsListas
 */
class SmsListasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $smsListas = $this->paginate($this->SmsListas);

        $this->set(compact('smsListas'));
        $this->set('_serialize', ['smsListas']);
    }

    /**
     * View method
     *
     * @param string|null $id Sms Lista id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smsLista = $this->SmsListas->get($id, [
            'contain' => ['Envios', 'SmsEnvios']
        ]);

        $this->set('smsLista', $smsLista);
        $this->set('_serialize', ['smsLista']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dados = $this->request->data();

        $smsLista = $this->SmsListas->newEntity();
        if ($this->request->is('post')) {
            if (!empty($dados['nome'])) {
                if ($dados['nome']['type'] === "text/plain") {
                    $fileName = substr($dados['nome']['name'], 0, -4) . date('dmYHis') . substr($dados['nome']['name'], -4);
                    $uploadPath = 'uploads/listas/';
                    $uploadFile = $uploadPath . $fileName;

                    if (move_uploaded_file($dados['nome']['tmp_name'], $uploadFile)) {
                        $smsLista = $this->SmsListas->patchEntity($smsLista, $dados);
                        $smsLista->nome = $fileName;
                        $smsLista->caminho = $uploadPath;
                        $this->SmsListas->save($smsLista);

                        $this->Flash->success(__('Arquivo enviado e Salvo com sucesso.'));
                        return $this->redirect(["controller" => "Users", 'action' => 'smsmarketing']);
                    } else {
                        $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                    }
                } else {
                    $this->Flash->error(__('Formato de arquivo não permitido. Formato permitido: TXT'));
                }
            } else {
                $this->Flash->error(__('Nenhum arquivo selecionado.'));
            }
        }
        $this->set(compact('smsLista'));
        $this->set('_serialize', ['smsLista']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sms Lista id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smsLista = $this->SmsListas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smsLista = $this->SmsListas->patchEntity($smsLista, $this->request->data);
            if ($this->SmsListas->save($smsLista)) {
                $this->Flash->success(__('The sms lista has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'smsmarketing']);
            } else {
                $this->Flash->error(__('The sms lista could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('smsLista'));
        $this->set('_serialize', ['smsLista']);
        $this->render("add");
    }

    /**
     * Delete method
     *
     * @param string|null $id Sms Lista id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smsLista = $this->SmsListas->get($id);
        if ($this->SmsListas->delete($smsLista)) {
            $this->Flash->success(__('The sms lista has been deleted.'));
        } else {
            $this->Flash->error(__('The sms lista could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Adicionar Arquivo TXT com lista de destinatários
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function addArquivo($lista_id = null)
    {
        $uploadData = '';
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!empty($this->request->data['file']['name']) && !empty($this->request->data['descricao'] && !empty($this->request->data['lista_id']))) {
                if ($this->request->data['file']['type'] === "text/plain") {
                    $fileName = substr($this->request->data['nome']['name'], 0, -4) . date('dmYHis') . substr($this->request->data['nome']['name'], -4);
                    $uploadPath = 'uploads/listas/';
                    $uploadFile = $uploadPath . $fileName;

                    if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadFile)) {
                        $uploadData = $this->SmsListas->get($this->request->data['lista_id']);
                        $uploadData->nome = $fileName;
                        $uploadData->descricao = $this->request->data['descricao'];
                        $uploadData->caminho = $uploadPath;

                        $this->SmsListas->save($uploadData);

                        $this->Flash->success(__('Arquivo enviado e Salvo com sucesso.'));
                        return $this->redirect(["controller" => "Users", 'action' => 'smsmarketing']);
                    } else {
                        $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                    }
                } else {
                    $this->Flash->error(__('Formato de arquivo não permitido. Formato permitido: TXT'));
                }
            } else {
                $this->Flash->error(__('Nenhum arquivo selecionado.'));
            }
            return $this->redirect(["controller" => "Users", 'action' => 'smsmarketing']);
        }

        $this->set('uploadData', $uploadData);

        $files = $this->SmsListas->find('all', ['order' => ['SmsListas.created' => 'DESC']]);
        $filesRowNum = $files->count();
        $this->set('files', $files);
        $this->set('filesRowNum', $filesRowNum);

        $this->set(compact('files', 'filesRowNum', 'lista_id'));
    }
}
