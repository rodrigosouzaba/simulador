<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SmsRespostasEnvios Controller
 *
 * @property \App\Model\Table\SmsRespostasEnviosTable $SmsRespostasEnvios
 */
class SmsRespostasEnviosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SmsEnvios']
        ];
        $smsRespostasEnvios = $this->paginate($this->SmsRespostasEnvios);

        $this->set(compact('smsRespostasEnvios'));
        $this->set('_serialize', ['smsRespostasEnvios']);
    }

    /**
     * View method
     *
     * @param string|null $id Sms Respostas Envio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smsRespostasEnvio = $this->SmsRespostasEnvios->get($id, [
            'contain' => ['SmsEnvios']
        ]);

        $this->set('smsRespostasEnvio', $smsRespostasEnvio);
        $this->set('_serialize', ['smsRespostasEnvio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $smsRespostasEnvio = $this->SmsRespostasEnvios->newEntity();
        if ($this->request->is('post')) {
            $smsRespostasEnvio = $this->SmsRespostasEnvios->patchEntity($smsRespostasEnvio, $this->request->data);
            if ($this->SmsRespostasEnvios->save($smsRespostasEnvio)) {
                $this->Flash->success(__('The sms respostas envio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sms respostas envio could not be saved. Please, try again.'));
            }
        }
        $smsEnvios = $this->SmsRespostasEnvios->SmsEnvios->find('list', ['limit' => 200]);
        $this->set(compact('smsRespostasEnvio', 'smsEnvios'));
        $this->set('_serialize', ['smsRespostasEnvio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sms Respostas Envio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smsRespostasEnvio = $this->SmsRespostasEnvios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smsRespostasEnvio = $this->SmsRespostasEnvios->patchEntity($smsRespostasEnvio, $this->request->data);
            if ($this->SmsRespostasEnvios->save($smsRespostasEnvio)) {
                $this->Flash->success(__('The sms respostas envio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sms respostas envio could not be saved. Please, try again.'));
            }
        }
        $smsEnvios = $this->SmsRespostasEnvios->SmsEnvios->find('list', ['limit' => 200]);
        $this->set(compact('smsRespostasEnvio', 'smsEnvios'));
        $this->set('_serialize', ['smsRespostasEnvio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sms Respostas Envio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smsRespostasEnvio = $this->SmsRespostasEnvios->get($id);
        if ($this->SmsRespostasEnvios->delete($smsRespostasEnvio)) {
            $this->Flash->success(__('The sms respostas envio has been deleted.'));
        } else {
            $this->Flash->error(__('The sms respostas envio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
