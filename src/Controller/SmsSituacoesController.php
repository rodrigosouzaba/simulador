<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SmsSituacoes Controller
 *
 * @property \App\Model\Table\SmsSituacoesTable $SmsSituacoes
 */
class SmsSituacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $smsSituacoes = $this->paginate($this->SmsSituacoes);

        $this->set(compact('smsSituacoes'));
        $this->set('_serialize', ['smsSituacoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Sms Situaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smsSituaco = $this->SmsSituacoes->get($id, [
            'contain' => []
        ]);

        $this->set('smsSituaco', $smsSituaco);
        $this->set('_serialize', ['smsSituaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $smsSituaco = $this->SmsSituacoes->newEntity();
        if ($this->request->is('post')) {
            $smsSituaco = $this->SmsSituacoes->patchEntity($smsSituaco, $this->request->data);
            if ($this->SmsSituacoes->save($smsSituaco)) {
                $this->Flash->success(__('The sms situaco has been saved.'));

//                 return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sms situaco could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('smsSituaco'));
        $this->set('_serialize', ['smsSituaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sms Situaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smsSituaco = $this->SmsSituacoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smsSituaco = $this->SmsSituacoes->patchEntity($smsSituaco, $this->request->data);
            if ($this->SmsSituacoes->save($smsSituaco)) {
                $this->Flash->success(__('The sms situaco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sms situaco could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('smsSituaco'));
        $this->set('_serialize', ['smsSituaco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sms Situaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smsSituaco = $this->SmsSituacoes->get($id);
        $this->SmsSituacoes->delete($smsSituaco);

        return $this->redirect(['action' => 'index']);
    }
}
