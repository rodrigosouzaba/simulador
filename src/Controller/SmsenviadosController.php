<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Smsenviados Controller
 *
 * @property \App\Model\Table\SmsenviadosTable $Smsenviados
 */
class SmsenviadosController extends AppController
{
    
    public $paginate = [
        'limit' => 100,
        'order' => [
            'VwSms.created' => 'DESC'
        ]
    ];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
	    $this->loadModel('VwSms');
        $this->paginate = array(
            'order' => array(// sets a default order to sort by
                'VwSms.created' => 'DESC'
            )
        );
//         $users = $this->paginate($this->VwSms);
        $smsenviados = $this->paginate($this->VwSms);

        $this->set(compact('smsenviados'));
        $this->set('_serialize', ['smsenviados']);
    }

    /**
     * View method
     *
     * @param string|null $id Smsenviado id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $smsenviado = $this->Smsenviados->get($id, [
            'contain' => []
        ]);

        $this->set('smsenviado', $smsenviado);
        $this->set('_serialize', ['smsenviado']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $smsenviado = $this->Smsenviados->newEntity();
        if ($this->request->is('post')) {
            $smsenviado = $this->Smsenviados->patchEntity($smsenviado, $this->request->data);
            if ($this->Smsenviados->save($smsenviado)) {
                $this->Flash->success(__('The smsenviado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The smsenviado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('smsenviado'));
        $this->set('_serialize', ['smsenviado']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Smsenviado id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $smsenviado = $this->Smsenviados->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $smsenviado = $this->Smsenviados->patchEntity($smsenviado, $this->request->data);
            if ($this->Smsenviados->save($smsenviado)) {
                $this->Flash->success(__('The smsenviado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The smsenviado could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('smsenviado'));
        $this->set('_serialize', ['smsenviado']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Smsenviado id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $smsenviado = $this->Smsenviados->get($id);
        if ($this->Smsenviados->delete($smsenviado)) {
            $this->Flash->success(__('The smsenviado has been deleted.'));
        } else {
            $this->Flash->error(__('The smsenviado could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
