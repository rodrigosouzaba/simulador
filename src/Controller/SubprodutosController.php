<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Subprodutos Controller
 *
 * @property \App\Model\Table\SubprodutosTable $Subprodutos
 */
class SubprodutosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tipos']
        ];
        $subprodutos = $this->paginate($this->Subprodutos);

        $this->set(compact('subprodutos'));
        $this->set('_serialize', ['subprodutos']);
    }

    /**
     * View method
     *
     * @param string|null $id Subproduto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subproduto = $this->Subprodutos->get($id, [
            'contain' => ['Tipos']
        ]);

        $this->set('subproduto', $subproduto);
        $this->set('_serialize', ['subproduto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subproduto = $this->Subprodutos->newEntity();
        if ($this->request->is('post')) {
            $subproduto = $this->Subprodutos->patchEntity($subproduto, $this->request->data);
            if ($this->Subprodutos->save($subproduto)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente'));
            }
        }
        $tipos = $this->Subprodutos->Tipos->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $this->set(compact('subproduto', 'tipos'));
        $this->set('_serialize', ['subproduto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Subproduto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subproduto = $this->Subprodutos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subproduto = $this->Subprodutos->patchEntity($subproduto, $this->request->data);
            if ($this->Subprodutos->save($subproduto)) {
                $this->Flash->success(__('Salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao Salvar. Tente Novamente'));
            }
        }
        $tipos = $this->Subprodutos->Tipos->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $this->set(compact('subproduto', 'tipos'));
        $this->set('_serialize', ['subproduto']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Subproduto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subproduto = $this->Subprodutos->get($id);
        if ($this->Subprodutos->delete($subproduto)) {
            $this->Flash->success(__('The subproduto has been deleted.'));
        } else {
            $this->Flash->error(__('The subproduto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
