<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TabelasCnpjs Controller
 *
 * @property \App\Model\Table\TabelasCnpjsTable $TabelasCnpjs
 */
class TabelasCnpjsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tabelas', 'Cnpjs']
        ];
        $tabelasCnpjs = $this->paginate($this->TabelasCnpjs);

        $this->set(compact('tabelasCnpjs'));
        $this->set('_serialize', ['tabelasCnpjs']);
    }

    /**
     * View method
     *
     * @param string|null $id Tabelas Cnpj id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tabelasCnpj = $this->TabelasCnpjs->get($id, [
            'contain' => ['Tabelas', 'Cnpjs']
        ]);

        $this->set('tabelasCnpj', $tabelasCnpj);
        $this->set('_serialize', ['tabelasCnpj']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tabelasCnpj = $this->TabelasCnpjs->newEntity();
        if ($this->request->is('post')) {
            $tabelasCnpj = $this->TabelasCnpjs->patchEntity($tabelasCnpj, $this->request->data);
            if ($this->TabelasCnpjs->save($tabelasCnpj)) {
                $this->Flash->success(__('The tabelas cnpj has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tabelas cnpj could not be saved. Please, try again.'));
            }
        }
        $tabelas = $this->TabelasCnpjs->Tabelas->find('list', ['limit' => 200]);
        $cnpjs = $this->TabelasCnpjs->Cnpjs->find('list', ['limit' => 200]);
        $this->set(compact('tabelasCnpj', 'tabelas', 'cnpjs'));
        $this->set('_serialize', ['tabelasCnpj']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tabelas Cnpj id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tabelasCnpj = $this->TabelasCnpjs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tabelasCnpj = $this->TabelasCnpjs->patchEntity($tabelasCnpj, $this->request->data);
            if ($this->TabelasCnpjs->save($tabelasCnpj)) {
                $this->Flash->success(__('The tabelas cnpj has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tabelas cnpj could not be saved. Please, try again.'));
            }
        }
        $tabelas = $this->TabelasCnpjs->Tabelas->find('list', ['limit' => 200]);
        $cnpjs = $this->TabelasCnpjs->Cnpjs->find('list', ['limit' => 200]);
        $this->set(compact('tabelasCnpj', 'tabelas', 'cnpjs'));
        $this->set('_serialize', ['tabelasCnpj']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tabelas Cnpj id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tabelasCnpj = $this->TabelasCnpjs->get($id);
        if ($this->TabelasCnpjs->delete($tabelasCnpj)) {
            $this->Flash->success(__('The tabelas cnpj has been deleted.'));
        } else {
            $this->Flash->error(__('The tabelas cnpj could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
