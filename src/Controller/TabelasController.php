<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Database\Type;
use Cake\View\ViewBuilder;
use Knp\Snappy\Pdf;
use App\Utility\PdfWriter;
use Cake\Mailer\Email;

// Habilita o parseamento de datas localizadas
Type::build('date')
    ->useLocaleParser()
    ->setLocaleFormat('dd/MM/yyyy');
Type::build('datetime')
    ->useLocaleParser()
    ->setLocaleFormat('dd/MM/yyyy HH:mm:ss');
Type::build('timestamp')
    ->useLocaleParser()
    ->setLocaleFormat('dd/MM/yyyy HH:mm:ss');

// Habilita o parseamento de decimal localizaddos
Type::build('decimal')
    ->useLocaleParser();
Type::build('float')
    ->useLocaleParser();

/**
 * Tabelas Controller
 *
 * @property \App\Model\Table\TabelasTable $Tabelas
 */
class TabelasController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['inativaPorVigencia', 'edit']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $conditions = [];
        $conditions = ['Tabelas.new' => 0];
        if (!empty($this->getRequest()->getData('estados')) || $this->getRequest()->getQuery('estados')) {
            $estado = $operadora = !empty($this->getRequest()->getData('estados')) ? $this->getRequest()->getData('estados') : $this->getRequest()->getQuery('estados');
            $conditions[] = ['Operadoras.estado_id' => $estado];
        }
        if (!empty($this->getRequest()->getData('operadora_id')) || $this->getRequest()->getQuery('operadora_id')) {
            $operadora = $operadora = !empty($this->getRequest()->getData('operadora_id')) ? $this->getRequest()->getData('operadora_id') : $this->getRequest()->getQuery('operadora_id');
            $conditions[] = ['Operadoras.id' => $operadora];
        }

        $this->paginate = [
            'valueField' => 'nome', 'order' => ['Tabelas.prioridade' => 'ASC'],
            'contain' => [
                'Estados',
                'Produtos',
                'TiposProdutos',
                'Operadoras' => ['Imagens'],
                'Abrangencias',
                'Tipos',
                "TabelasCnpjs" => [
                    "Cnpjs"
                ]
            ],
            'order' => [
                'Tabelas.validade' => 'DESC',
                'Tabelas.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $tabelas = $this->paginate($this->Tabelas);

        $operadoras = $this->Tabelas->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $estados = $this->Tabelas->Operadoras->find('list', ['valueField' => 'estado_id',  'conditions' => ['1 = 1']])->toArray();
        $estados = $this->Tabelas->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);

        $this->set(compact('sessao', 'tabelas', 'operadoras', 'estados'));
        $this->set('_serialize', ['tabelas']);
    }

    /**
     * New method
     *
     * @return \Cake\Network\Response|null
     */
    public function new(int $estado = null, $operadora = null, $comercializacao = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $conditionsContain = [];
        $conditionsContain = ['Tabelas.new' => 1];
        $operadoras = null;
        if ($estado) {
            $operadoras = $this->Tabelas->Operadoras
                ->find('list', ['valueField' => function ($e) {
                    return $e->get('nome') . " " . $e->get('detalhe');
                }])
                ->matching('AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                    return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
                })
                ->where(['nova' => 1, 'NOT' => 'status <=> "INATIVA"'])
                ->orderASC('Operadoras.nome');
        }

        if ($operadora) {
            $comercializacoes = $this->Tabelas->Operadoras->AreasComercializacoes->find('list', ['valueField' => 'nome'])->where(['operadora_id' => $operadora]);
        }

        $tabelas = $this->Tabelas->find('all');
        if ((!is_null($estado) && !empty($estado)) && is_null($operadora)) {
            $tabelas->matching('AreasComercializacoes.AreasComercializacoesEstadosMunicipios', function ($q) use ($estado) {
                return $q->where(['AreasComercializacoesEstadosMunicipios.estado_id' => $estado]);
            });
        }

        if (!is_null($operadora) && is_null($comercializacao)) {
            $tabelas->matching('AreasComercializacoes', function ($q) use ($operadora) {
                return $q->where(['AreasComercializacoes.operadora_id' => $operadora]);
            });
        }

        if (!is_null($comercializacao)) {
            $tabelas->matching('AreasComercializacoes', function ($q) use ($comercializacao) {
                return $q->where(['AreasComercializacoes.id' => $comercializacao]);
            });
        }

        $tabelas->contain([
            'Coberturas',
            'AreasComercializacoes' => ['Operadoras' => ['Imagens']],
            'Estados',
            'Produtos',
            'TiposProdutos',
            'Operadoras' => ['Imagens'],
            'Abrangencias',
            'Tipos',
            'Cnpjs',
            "TabelasCnpjs" => [
                "Cnpjs"
            ]
        ])
            ->where($conditionsContain)
            ->order([
                'Tabelas.validade' => 'DESC',
                'Tabelas.prioridade' => 'ASC'
            ])
            ->group('Tabelas.id');

        $tabelas = $this->paginate($tabelas);

        $estados = $this->Tabelas->AreasComercializacoes->Estados->find('list', ['valueField' => 'nome'])->toArray();
        // debug($tabelas);
        $this->set(compact('tabelas', 'operadoras', 'estados', 'sessao', 'comercializacoes', 'estado', 'operadora', 'comercializacao'));
    }

    /**
     * View method
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tabela = $this->Tabelas->get($id, [
            'contain' => ['Produtos', 'Regioes']
        ]);

        $this->set('tabela', $tabela);
        $this->set('_serialize', ['tabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($new = null)
    {

        $this->set('title', 'Nova Tabela');

        $tabela = $this->Tabelas->newEntity();
        if ($this->request->is('post')) {

            for ($i = 1; $i <= 12; $i++) {
                if (!isset($this->request->data['faixa' . $i])) {
                    $this->request->data['faixa' . $i] = 0;
                }
                $this->request->data['faixa' . $i] = str_replace('.', ',', $this->request->data['faixa' . $i]);
            }

            $tabela = $this->Tabelas->patchEntity($tabela, $this->request->data);

            $tabela->new = $new == 1 ? 1 : 0;
            if ($this->Tabelas->save($tabela)) {
                //Salva os CNPJS que a tabela pode ser COtada
                $this->loadModel("TabelasCnpjs");
                foreach ($this->request->data["cnpjs"] as $cnpj) {
                    $dadosCnpj["tabela_id"] = $tabela->id;
                    $dadosCnpj["cnpj_id"] = $cnpj;

                    $entidadeRelacional = $this->TabelasCnpjs->newEntity();
                    $entidadeRelacional = $this->TabelasCnpjs->patchEntity($entidadeRelacional, $dadosCnpj);
                    $this->TabelasCnpjs->save($entidadeRelacional);
                }

                $this->Flash->success(__('Tabela Criada com Sucesso.'));

                $idTabela = $tabela->id;

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao Salvar a tabela. Tente Novamente ou contate a TI.'));
            }
        }
        $produtos = $this->Tabelas->Produtos->find('list', ['valueField' => 'nome']);
        $operadoras = $this->Tabelas->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $areas_comercializacoes = $this->Tabelas->AreasComercializacoes->find('list', ['valueField' => function ($q) {
            return  $q->get('nome') . ' | ' .  $q->operadora->get('nome') . ' - ' . $q->operadora->get('detalhe');
        }])
            ->contain('Operadoras')
            ->where(['Operadoras.nova' => 1]);
        $abrangencias = $this->Tabelas->Abrangencias->find('list', ['valueField' => 'nome'])->orderAsc(' prioridade');
        $tipos = $this->Tabelas->Tipos->find('list', ['valueField' => 'nome']);

        $coparticipacao = array('s' => 'Sim', 'n' => 'Não');
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);

        $this->loadModel('Cnpjs');
        $cnpjs = $this->Cnpjs->find('list', ['valueField' => 'nome', 'order' => ['id' => 'ASC']])->toArray();

        $coberturas = $this->Tabelas->Coberturas->find('list', ['limit' => 200, 'valueField' => 'nome']);

        $this->set(compact('estados'));


        $this->set(compact('tabela', 'coparticipacao', "cnpjs", 'produtos', 'operadoras', 'abrangencias', 'tipos', 'coberturas', 'areas_comercializacoes'));
        $this->set('_serialize', ['tabela']);

        $this->loadModel('OdontoOperadoras');
        $odonto_operadoras = $this->OdontoOperadoras->find("list", ['valueField' => 'nome'])->where(['tipo_pessoa' => 'PJ']);
        $this->set(compact('odonto_operadoras'));

        if ($new == 1) {
            $this->render('new_add');
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $new = null)
    {
        if ($new) {
            $containTabela = ["TabelasCnpjs", 'Operadoras', 'Estados', 'Produtos', 'Abrangencias', 'Tipos', 'AreasComercializacoes' => ['Operadoras']];
        } else {
            $containTabela = ["TabelasCnpjs", 'Operadoras', 'Estados', 'Produtos', 'Abrangencias', 'Tipos'];
        }
        $tabela = $this->Tabelas->get($id, ['contain' => $containTabela]);
        $this->set('title', 'Inserir / Alterar Preços');

        $ids_vitalmed = [224, 225];
        if (in_array($tabela->areas_comercializaco->operadora_id, $ids_vitalmed)) {
            $is_vitalmed = true;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            for ($i = 1; $i <= 12; $i++) {
                if (!isset($this->request->data['faixa' . $i])) {
                    $this->request->data['faixa' . $i] = 0;
                }
                $this->request->data['faixa' . $i] = str_replace('.', ',', $this->request->data['faixa' . $i]);
            }
            if (!empty($this->request->getData('odonto_valor'))) {
                $this->request->data['odonto_valor'] = str_replace('.', ',', $this->request->data['odonto_valor']);
            }
            $tabela->tipo_contratacao = $this->request->data['tipo_contratacao'];
            //$tabela->tipo_produto_id = $this->request->data['tipo_produto_id'];
            $tabela = $this->Tabelas->patchEntity($tabela, $this->request->data);

            if ($this->Tabelas->save($tabela)) {

                //Salva os CNPJS que a tabela pode ser COtada
                $this->loadModel("TabelasCnpjs");
                $apagar = $this->TabelasCnpjs->find("all")->where(["tabela_id" => $id]);

                if (!empty($apagar) && isset($apagar)) {
                    foreach ($apagar as $apagar) {
                        $excluirCnpjTabela = $this->TabelasCnpjs->get($apagar['id']);
                        $this->TabelasCnpjs->delete($excluirCnpjTabela);
                    }
                }

                foreach ($this->request->data["cnpjs"] as $cnpj) {
                    $dadosCnpj["tabela_id"] = $tabela->id;
                    $dadosCnpj["cnpj_id"] = $cnpj;

                    $entidadeRelacional = $this->TabelasCnpjs->newEntity();
                    $entidadeRelacional = $this->TabelasCnpjs->patchEntity($entidadeRelacional, $dadosCnpj);
                    $this->TabelasCnpjs->save($entidadeRelacional);
                }

                $this->Flash->success(__('Tabela atualizada com Sucesso'));

                return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            } else {
                $this->Flash->error(__('Erro ao atualizar Tabela. Tente Novamente.'));
            }
        }
        $produtos = $this->Tabelas->Produtos->find('list', ['valueField' => 'nome'])->where(["Produtos.operadora_id" => $tabela->operadora_id]);
        $operadoras = $this->Tabelas->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ])->where(['1 = 1', "Operadoras.estado_id" => $tabela->estado_id]);

        $areas_comercializacoes = $this->Tabelas->AreasComercializacoes->find('list', ['valueField' => function ($q) {
            return  $q->get('nome') . ' | ' .  $q->operadora->get('nome') . ' - ' . $q->operadora->get('detalhe');
        }])->contain('Operadoras');

        $abrangencias = $this->Tabelas->Abrangencias->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']])->orderAsc('prioridade');
        $tipos = $this->Tabelas->Tipos->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $estados = $this->Tabelas->Estados->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $coparticipacao = array('s' => 'Sim', 'n' => 'Não');
        $this->loadModel('Cnpjs');
        $cnpjs = $this->Cnpjs->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1'], 'order' => ['id' => 'ASC']])->toArray();
        $this->loadModel('Regioes');
        $regioes = $this->Regioes->find('list', ['valueField' => 'nome']);
        $coberturas = $this->Tabelas->Coberturas->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']]);
        $this->set(compact('ids_vitalmed', 'is_vitalmed', 'tabela', "cnpjs", 'coparticipacao', 'produtos', 'operadoras', 'areas_comercializacoes', 'abrangencias', 'tipos', 'estados', 'coberturas', 'regioes'));
        $this->set('_serialize', ['tabela']);
        $this->loadModel('OdontoOperadoras');
        $odonto_operadoras = $this->OdontoOperadoras->find("list", ['valueField' => 'nome'])->where(['tipo_pessoa' => 'PJ']);
        $this->set(compact('odonto_operadoras'));

        if ($new == 1) {

            $operadora_id = $tabela->areas_comercializaco->operadora->id;
            $formas_pagamentos = $this->Tabelas->FormasPagamentos->find('list', ['valueField' => 'nome'])->where(['operadora_id' => $operadora_id]);
            $informacoes = $this->Tabelas->Informacoes->find('list', ['valueField' => 'nome'])->where(['operadora_id' => $operadora_id]);
            $opcionais = $this->Tabelas->Opcionais->find('list', ['valueField' => 'nome'])->where(['operadora_id' => $operadora_id]);
            $carencias = $this->Tabelas->Carencias->find('list', ['valueField' => 'nome'])->where(['operadora_id' => $operadora_id]);
            $reembolsos = $this->Tabelas->Reembolsos->find('list', ['valueField' => 'nome'])->where(['operadora_id' => $operadora_id]);
            $observacoes = $this->Tabelas->Observacoes->find('list', ['valueField' => 'nome'])->where(['operadora_id' => $operadora_id]);
            $redes = $this->Tabelas->Redes->find('list', ['valueField' => 'nome'])->where(['operadora_id' => $operadora_id]);
            // debug($observacoes->toArray());
            // die;
            $this->set(compact('formas_pagamentos', 'informacoes', 'opcionais', 'carencias', 'reembolsos', 'observacoes', 'redes'));
            try {
                $this->render('new_add');
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete', 'get']);
        $tabela = $this->Tabelas->get($id);

        $this->loadModel('TabelasFiltradas');
        $this->loadModel('AgendamentosTabelas');
        $this->loadModel('SimulacoesTabelas');
        $this->loadModel('TabelasRegioes');
        $this->loadModel('TabelasCnpjs');
        $this->loadModel('LinksTabelas');

        $this->TabelasFiltradas->deleteAll(["tabela_id" => $id]);
        $this->AgendamentosTabelas->deleteAll(["tabela_id" => $id]);
        $this->SimulacoesTabelas->deleteAll(["tabela_id" => $id]);
        $this->TabelasRegioes->deleteAll(["tabela_id" => $id]);
        $this->TabelasCnpjs->deleteAll(["tabela_id" => $id]);
        $this->LinksTabelas->deleteAll(["tabela_id" => $id]);
        $this->Tabelas->deleteAll(["id" => $id]);

        if ($this->Tabelas->delete($tabela)) {
            $this->Flash->success(__('Tabela excluída com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao excluir tabela. Tente Novamente.'));
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    /**
     * Método de Validação de Tabela
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function validar($id = null)
    {

        $this->autoRender = false;
        $tabela = $this->Tabelas->get($id);

        $tabela->validade = 1;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->Tabelas->save($tabela)) {
            $res['ok'] = true;
            $res['msg'] = 'Registro atualizado com sucesso';
        } else {
            $res['ok'] = false;
            $res['msg'] = 'Erro ao atualizar oregistro. Tente Novamente';
        }
        $this->response->type('json');
        $this->response->body(json_encode($res));
    }

    /**
     * Método de Validação de Tabela
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function invalidar($id = null)
    {
        $this->autoRender = false;
        $tabela = $this->Tabelas->get($id);

        $tabela->validade = 0;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->Tabelas->save($tabela)) {
            $res['ok'] = true;
            $res['msg'] = 'Registro atualizado com sucesso';
        } else {
            $res['ok'] = false;
            $res['msg'] = 'Erro ao atualizar oregistro. Tente Novamente';
        }
        $this->response->type('json');
        $this->response->body(json_encode($res));
    }

    public function findProdutos($id = null)
    {
        $this->loadModel('Produtos');
        if ($id) {
            $produtos = $this->Produtos->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']])
                ->where(['Produtos.operadora_id' => $id])->toArray();
            //            debug($produtos);
        } else {
            $produtos = $this->Produtos->find('list', ['valueField' => 'nome', 'conditions' => ['1 = 1']])->toArray();
            //            debug($produtos);
        }
        //        die();
        $this->set(compact('produtos'));

        $this->render('selectProduto');
    }



    /**
     * Filtro por operadora
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroOperadora($id = null)
    {

        $tabelas = $this->Tabelas->find('all', [
            'valueField' => 'nome',
            'conditions' => ['1 = 1'],
            'order' => ['Tabelas.prioridade' => 'ASC'],
            'contain' => ['Estados', 'Produtos', 'TiposProdutos', 'Operadoras', 'Abrangencias', 'Tipos', "TabelasCnpjs" => ["Cnpjs"]]
        ])
            ->order(['Tabelas.prioridade' => 'ASC'])
            ->where(['Tabelas.operadora_id' => $id])->toArray();

        $operadoras = $this->Tabelas->Operadoras->find('list', [
            'valueField' => function ($e) {
                return $e->get('nome') . ' ' . $e->get('detalhe') . ' ' . $e->estado->get('nome');
            },
            'conditions' => ['1 = 1'],
            'order' => ['Operadoras.nome' => 'ASC'],
            'contain' => ['Estados']
        ]);
        $this->set(compact('tabelas'));
    }

    /**
     * Busca de Tabelas por Operadoras
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function buscaTabela()
    {
        $dados = $this->request->data();
        $tabelas = $this->Tabelas->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('minimo_vidas') . ' a ' . $e->get('maximo_vidas') . ' vidas';
        }, 'conditions' => ['1 = 1'], 'order' => ['Tabelas.nome' => 'ASC'], 'contain' => ['Produtos', 'Operadoras', 'Abrangencias', 'Tipos']])
            ->where([
                'Tabelas.operadora_id' => $dados['operadora_id'],
                'Tabelas.tipo_id' => $dados['tipo_id'],
                'Tabelas.minimo_vidas >=' => $dados['minimo_vidas'],
                'Tabelas.maximo_vidas <=' => $dados['maximo_vidas'],
                'Tabelas.coparticipacao' => $dados['coparticipacao'],
            ])->toArray();
        //            debug($tabelas);
        $this->set(compact('tabelas'));
    }

    /**
     * Delete de múltiplas tabelas de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteLote()
    {
        $tabelas = $this->request->data;
        $this->loadModel('TabelasFiltradas');
        $this->loadModel('AgendamentosTabelas');
        $this->loadModel('SimulacoesTabelas');
        $this->loadModel('TabelasRegioes');
        $this->loadModel('TabelasCnpjs');
        $this->loadModel('LinksTabelas');
        // 		debug($tabelas);
        foreach ($tabelas as $chave => $tabela) {
            if ($tabela == 0) {
                unset($tabelas[$chave]);
            }
        }
        $this->TabelasFiltradas->deleteAll(["tabela_id IN" => $tabelas]);
        $this->AgendamentosTabelas->deleteAll(["tabela_id IN" => $tabelas]);
        $this->SimulacoesTabelas->deleteAll(["tabela_id IN" => $tabelas]);
        $this->TabelasRegioes->deleteAll(["tabela_id IN" => $tabelas]);
        $this->TabelasCnpjs->deleteAll(["tabela_id IN" => $tabelas]);
        $this->LinksTabelas->deleteAll(["tabela_id IN" => $tabelas]);
        $this->Tabelas->deleteAll(["id IN" => $tabelas]);

        $this->Flash->success(__('Tabelas excluídas com sucesso'));
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
    }

    /**
     * Reajuste de múltiplas tabelas de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function reajusteLote()
    {

        $this->set('title', 'Reajustar Tabelas');
        $tabelas = $this->request->data;
        if (isset($this->request->data['percentual'])) {
            foreach ($tabelas as $chave => $tabela) {
                if ($chave <> 'percentual') {
                    if ($tabela <> 0) {
                        $id = $this->Tabelas->get($tabela);
                        for ($i = 1; $i <= 12; $i++) {
                            $faixa = 'faixa' . $i;
                            $id->$faixa = round($id->$faixa + (($tabelas['percentual'] / 100) * $id->$faixa), 2);
                        }
                        $id->validade = 0;
                        $this->Tabelas->save($id);
                    }
                }
            }
            $this->Flash->success(__('Tabelas Reajustadas com sucesso.'));

            return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
        }
        $this->set(compact('tabelas'));
    }

    /**
     * Reajuste de Vigência tabelas de Uma só vez
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function reajusteVigencia()
    {

        $this->set('title', 'Reajustar Tabelas');

        $tabelas = $this->request->data;
        if ($this->request->is(['post', 'put'])) {

            if (isset($this->request->data['vigencia'])) {
                $vigencia = $tabelas['vigencia'];
                unset($tabelas['vigencia']);
                foreach ($tabelas as $chave => $tabela) {

                    if (!empty($tabela) && $tabela <> '') {
                        //                debug($tabela);
                        $id = $this->Tabelas->get($tabela);
                        $id->vigencia = $vigencia['year'] . "-" . $vigencia['month'] . "-" . $vigencia['day'];

                        $this->Tabelas->save($id);
                    }
                }
                $this->Flash->success(__('Vigência de Tabelas alteradas com sucesso.'));


                // return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
            }
        }
        $this->set(compact('tabelas'));
    }

    /**
     * Cadastrar valores percentuais de Tabelas:
     * Exemplo: Apartatamento de um Produto equivale a 15% mais caro que Enfermaria
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function valorPercentual()
    {
        $this->loadModel('Operadoras');
        $ids_estados = $this->Operadoras->find('list', ['keyValue' => 'estado_id', 'valueField' => 'estado_id'])->distinct(['estado_id'])->toArray();

        $estados = $this->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $ids_estados])->toArray();

        $operadoras = $this->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('detalhe');
        }, 'order' => ['nome' => 'ASC']]);
        $tabelas = $this->Tabelas->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . ' ' . $e->get('minimo_vidas') . ' a ' . $e->get('maximo_vidas');
        }, 'order' => ['nome' => 'ASC']]);
        $tipos = $this->Tabelas->Tipos->find('list', ['valueField' => 'nome']);
        $this->set(compact('operadoras', 'tabelas', 'tipos', 'estados'));
    }

    /**
     * Cadastrar todos os preços de uma Tabelas secundária com base em uma tabela matriz:
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editarPercentual()
    {
        $dados = $this->request->data;
        //        debug($dados);
        //        die();
        //        $matriz = $this->Tabelas->get($dados['tabelaMatriz_id']);
        $percentual = $dados['percentual'];
        foreach ($dados['Tabelas'] as $secundaria) {
            if ($secundaria !== 'false') {
                $tabela_secundaria = $this->Tabelas->get($secundaria);
                $percentual = str_replace(",", ".", $percentual);
                $dados['faixa1'] = str_replace(",", ".", $dados['faixa1']);
                $dados['faixa2'] = str_replace(",", ".", $dados['faixa2']);
                $dados['faixa3'] = str_replace(",", ".", $dados['faixa3']);
                $dados['faixa4'] = str_replace(",", ".", $dados['faixa4']);
                $dados['faixa5'] = str_replace(",", ".", $dados['faixa5']);
                $dados['faixa6'] = str_replace(",", ".", $dados['faixa6']);
                $dados['faixa7'] = str_replace(",", ".", $dados['faixa7']);
                $dados['faixa8'] = str_replace(",", ".", $dados['faixa8']);
                $dados['faixa9'] = str_replace(",", ".", $dados['faixa9']);
                $dados['faixa10'] = str_replace(",", ".", $dados['faixa10']);
                $dados['faixa11'] = str_replace(",", ".", $dados['faixa11']);
                $dados['faixa12'] = str_replace(",", ".", $dados['faixa12']);
                $tabela_secundaria->faixa1 = round($tabela_secundaria->faixa1 + $tabela_secundaria->faixa1 * $percentual / 100, 2);
                $tabela_secundaria->faixa2 = round($tabela_secundaria->faixa1 + $tabela_secundaria->faixa1 * $dados['faixa2'] / 100, 2);
                $tabela_secundaria->faixa3 = round($tabela_secundaria->faixa2 + $tabela_secundaria->faixa2 * $dados['faixa3'] / 100, 2);
                $tabela_secundaria->faixa4 = round($tabela_secundaria->faixa3 + $tabela_secundaria->faixa3 * $dados['faixa4'] / 100, 2);
                $tabela_secundaria->faixa5 = round($tabela_secundaria->faixa4 + $tabela_secundaria->faixa4 * $dados['faixa5'] / 100, 2);
                $tabela_secundaria->faixa6 = round($tabela_secundaria->faixa5 + $tabela_secundaria->faixa5 * $dados['faixa6'] / 100, 2);
                $tabela_secundaria->faixa7 = round($tabela_secundaria->faixa6 + $tabela_secundaria->faixa6 * $dados['faixa7'] / 100, 2);
                $tabela_secundaria->faixa8 = round($tabela_secundaria->faixa7 + $tabela_secundaria->faixa7 * $dados['faixa8'] / 100, 2);
                $tabela_secundaria->faixa9 = round($tabela_secundaria->faixa8 + $tabela_secundaria->faixa8 * $dados['faixa9'] / 100, 2);
                $tabela_secundaria->faixa10 = round($tabela_secundaria->faixa9 + $tabela_secundaria->faixa9 * $dados['faixa10'] / 100, 2);
                $tabela_secundaria->faixa11 = round($tabela_secundaria->faixa9 + $tabela_secundaria->faixa9 * $dados['faixa10'] / 100, 2);
                $tabela_secundaria->faixa12 = round($tabela_secundaria->faixa9 + $tabela_secundaria->faixa9 * $dados['faixa10'] / 100, 2);
                //                debug( $tabela_secundaria);
                //                die();
                $this->Tabelas->save($this->Tabelas->save($tabela_secundaria));
            }
        }
        //        die();
        //
        ////        debug($matriz);die();
        //
        //        $secundaria->faixa1 = $matriz->faixa1 * $percentual/100;
        //        $i = 1;
        //        for ($i=1;$i=10;$i++){
        //            $secundaria->faixa.$i = $matriz->faixa.$i * ($percentual/100);
        //        }
        // $user->email= abc@gmail.com; // other fields if necessary
        //        if ($this->request->is(['patch', 'post', 'put'])) {
        //            if ($this->Tabelas->save($secundaria)) {
        //                $this->Flash->success(__('Preços ajustados com sucesso'));
        //                return $this->redirect(['controller' => 'Users','action' => 'configurarPme', 'destino' =>  $this->request->controller]);
        //            } else {
        //                $this->Flash->error(__('Erro ao ajustar preço'));
        //            }
        //        }
    }

    /**
     *
     *
     *  Gerar Tabela Padrão NATUSEG
     *
     *
     */
    public function gerarTabela()
    {

        $ids = $this->request->data();

        //Removendo tabelas não selecionadas
        foreach ($ids as $chave => $valor) {
            if ($valor == 0) {
                unset($ids[$chave]);
            } else {
                $t[$valor] = $valor;
            }
        }

        $this->set('title', 'Tabela');
        $tabelas = $this->Tabelas->find('all')
            ->contain([
                'Regioes' => ['Estados'],
                'Abrangencias',
                'Tipos',
                'Produtos',
                'Operadoras' => ['Carencias', 'Redes', 'Observacoes', 'Informacoes', 'Opcionais', 'Reembolsos', 'Imagens']
            ])
            ->where(['Tabelas.id IN' => $t]);
        $total = count($tabelas->toArray());

        foreach ($tabelas as $tabela) {
            if ($tabela['cod_ans'] != '') {
                $tabelasOrdenadas[$tabela['produto']['nome']][$tabela['tipo']['nome']][$tabela['cod_ans']] = $tabela;
            } else {
                $tabelasOrdenadas[$tabela['produto']['nome']][$tabela['tipo']['nome']][] = $tabela;
            }
        }

        $tabelas = $tabelasOrdenadas;


        foreach ($tabelas as $t) {
            foreach ($t as $dados) {
                foreach ($dados as $info) {
                    $validade = $info['vigencia'];
                    $regiao = $info['regio']['estado']['nome'] . " / " . $info['regio']['nome'];
                    $titulo = $info['operadora']['nome'] . " - PLANO " . $info['modalidade']['nome'] . " " . $info['minimo_vidas'] . " À " . $info['maximo_vidas'] . " VIDAS";
                    $imagem = $info['operadora']['imagen']['caminho'] . $info['operadora']['imagen']['nome'];
                    break;
                }
            }
        }

        //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
        $tipoPDF = 'F';
        //Nome do PDF
        $nomePDF = $_SERVER['DOCUMENT_ROOT'] . $titulo . ".pdf";

        $btnCancelar = ['controller' => 'tabelas', 'action' => 'index'];
        $this->set('tabela', $tabelas);

        $this->set('_serialize', ['tabelas']);

        $this->set(compact('tabelas', 'btnCancelar', 'total', 'nomePDF', 'tipoPDF', 'titulo'));
        $this->viewBuilder()->layout('tabela');
        return $this->redirect(['action' => 'email', $titulo]);
    }

    public function email($nomePDF = string)
    {
        $aquivoNome = $nomePDF . ".pdf"; // nome do arquivo que será enviado p/ download
        $arquivoLocal = $_SERVER['DOCUMENT_ROOT'] . $aquivoNome; // caminho absoluto do arquivo
        // Verifica se o arquivo não existe
        if (!file_exists($arquivoLocal)) {
            // Exiba uma mensagem de erro caso ele não exista
            exit;
        }
        // Aqui você pode aumentar o contador de downloads
        // Definimos o novo nome do arquivo
        $novoNome = $nomePDF . ".pdf";
        // Configuramos os headers que serão enviados para o browser
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . $novoNome . '"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($aquivoNome));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');
        // Envia o arquivo para o cliente
        readfile($aquivoNome);
        //        $dados = $this->Simulacoes->get($simulacao);
        //        unlink($_SERVER['DOCUMENT_ROOT'] . $nomePDF . '.pdf');
        //        unlink($_SERVER['DOCUMENT_ROOT'] . 'Calculo-Rapido-Numero-' . $dados['id'] . '.pdf');


        $this->Flash->error(__('Cálculo enviado para o e-mail: ' . $sessao['email']));
        return $this->redirect(['controller' => 'Users', 'action' => 'configurarPme', 'destino' =>  $this->request->controller]);
        //            }
    }

    /**
     * Método para alterar Prioridade de Tabelas
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function prioridade($id = null, $prioridade = null)
    {

        $this->layout = 'ajax';

        if ($this->request->is(['get'])) {
            $tabela_id = $id;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $tabela = $this->Tabelas->get($this->request->data['tabela_id']);
            $tabela->prioridade = $this->request->data['prioridade'];
            if ($this->Tabelas->save($tabela)) {
                $this->Flash->success('Registro salvo com sucesso.');
            } else {
                $this->Flash->error(__('Não foi possível salvar o registro. Favor tentar novamente.'));
            }
        }

        $this->set(compact('tabela_id'));
    }


    /*
Método Gerador de tabelas personalizadas

*/

    public function gerador()
    {
        $estadosContados = $this->Tabelas
            ->find('all', ['empty' => 'SELECIONE'])
            ->select(['estado_id'])
            ->distinct(['estado_id'])
            ->where(['validade' => 1])
            ->toArray();
        $array = array();
        foreach ($estadosContados as $estadosContados) {
            $array[] = $estadosContados['estado_id'];
        }

        $this->loadModel("Estados");
        $estados = $this->Estados->find('list', ["valueField" => "nome"])->where(["id IN" => $array])->toArray();

        if ($this->request->is(['post'])) {
            $ids = $this->request->data();

            //Removendo tabelas não selecionadas
            foreach ($ids as $chave => $valor) {
                if ($valor == 0) {
                    unset($ids[$chave]);
                } else {
                    $t[$valor] = $valor;
                }
            }

            $this->set('title', 'Tabela');
            $tabelas = $this->Tabelas->find('all')
                ->contain([
                    'Regioes',
                    'Estados',
                    'Abrangencias',
                    'Tipos',
                    'Produtos',
                    'TiposProdutos',
                    'Operadoras' => ['Imagens', 'Carencias', 'Redes', 'Observacoes', 'Informacoes', 'Opcionais', 'Reembolsos']
                ])
                ->where(['Tabelas.id IN' => $t]);
            $total = count($tabelas->toArray());

            foreach ($tabelas as $tabela) {
                if ($tabela['cod_ans'] != '') {
                    $tabelasOrdenadas[$tabela['produto']['nome']][$tabela['tipo']['nome']][$tabela['cod_ans']] = $tabela;
                } else {
                    $tabelasOrdenadas[$tabela['produto']['nome']][$tabela['tipo']['nome']][] = $tabela;
                }
            }

            $tabelas = $tabelasOrdenadas;


            foreach ($tabelas as $t) {
                foreach ($t as $dados) {
                    foreach ($dados as $info) {
                        $validade = $info['vigencia'];
                        $regiao = $info['regio']['estado']['nome'] . " / " . $info['regio']['nome'];
                        $titulo = $info['operadora']['nome'] . " - PLANO " . $info['modalidade']['nome'] . " " . $info['minimo_vidas'] . " À " . $info['maximo_vidas'] . " VIDAS";
                        $imagem = $info['operadora']['imagen']['caminho'] . $info['operadora']['imagen']['nome'];
                        break;
                    }
                }
            }
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $userId = $sessao['id'];


            //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
            $tipoPDF = 'D';
            //Nome do PDF
            $nomePDF = $titulo . ".pdf";

            $btnCancelar = ['controller' => 'tabelas', 'action' => 'index'];
            $this->set('tabela', $tabelas);

            $this->set('_serialize', ['tabelas']);

            $this->set(compact('tabelas', 'btnCancelar', 'sessao', 'total', 'nomePDF', 'tipoPDF', 'titulo'));
            $this->viewBuilder()->layout('tabela');
            $this->render('gerar_tabela');
        }

        $this->set(compact('estados'));
    }

    public function findOperadoras($idOperadora = null)
    {
        $this->loadModel("Operadoras");
        $operadoras = $this->Operadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }])
            ->where(["Operadoras.estado_id" => $idOperadora])
            ->order(["Operadoras.nome" => "ASC"]);
        $this->set(compact("operadoras"));
    }


    public function geradorProdutos($id = null)
    {
        $dados = $this->request->data();

        $this->loadModel('Produtos');

        $produtos = $this->Produtos->find('list', ['valueField' => 'nome'])
            ->where([
                'operadora_id' => $dados["operadora_id"]
            ])->toArray();
        $this->set(compact('produtos'));
    }


    public function geradorVidas($id = null)
    {
        if ($id) {
            $tabelas = $this->Tabelas->find('all')
                ->where(['operadora_id' => $id])
                ->order(["minimo_vidas" => "ASC"])
                ->toArray();
        }
        if ($tabelas) {
            $vidas = array();
            foreach ($tabelas as $tabela) {
                $vidas[$tabela["minimo_vidas"] . "-" . $tabela["maximo_vidas"]] = $tabela["minimo_vidas"] . " a " . $tabela["maximo_vidas"];
            }
        }
        $this->set(compact('vidas'));
    }


    public function tabela()
    {
        $dados = $this->request->data();
        //        debug(explode("-",$dados["vidas"]));die();

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        /*
		$this->loadModel("Operadoras");
		$operadora = $this->Operadoras->find("all")->where(["Operadoras.id" => $dados["operadora_id"]])->contain(["Estados", "Imagens"])->first();
*/
        $vidas = explode("-", $dados["vidas"]);
        if ($dados["coparticipacao"] == 99) {
            $coparticipacao = "";
        } else {
            $coparticipacao = array("Tabelas.coparticipacao" => $dados["coparticipacao"]);
        }
        foreach ($dados["Produtos"] as $id => $produto) {
            if ($produto == '0') {
                unset($dados["Produtos"][$id]);
            }
        }

        $tabelas = $this->Tabelas
            ->find("all")
            ->contain([
                'Tipos',
                'Produtos',
                "Abrangencias",
                "Operadoras"
            ])
            ->where([
                "Tabelas.estado_id" => $dados["estado_id"],
                "Tabelas.operadora_id" => $dados["operadora_id"],
                "Tabelas.produto_id IN " => $dados["Produtos"],
                "Tabelas.minimo_vidas <=" => $vidas[0],
                "Tabelas.maximo_vidas >=" => $vidas[1],
                $coparticipacao,
            ])
            ->order([
                "Produtos.nome" => "ASC",
                "Tabelas.prioridade" => "ASC"
            ])
            ->toArray();
        // 		debug($tabelas);die();

        $totalTabelas = count($tabelas);
        // 	   	debug($totalTabelas);

        //Divisão do ARRAY por 8 tabelas no máximo
        $tabelasDivididas = array_chunk($tabelas, 8, true);
        // 	   	debug($tabelasDivididas);


        foreach ($tabelasDivididas as $chave => $tabelasD) {
            // 	       	debug($chave);
            // 	       	debug(count($tabelasD));
            // 	       	debug($tabelasD);
            // die();
            /*

	       	debug(count($tabelasD));
*/
            // 	       	die();

            foreach ($tabelasD as $tabela) {
                /*
				debug($chave);
				debug($tabela);
*/

                switch ($tabela["coparticipacao"]) {
                    case "s":
                        $co = "COPARTICIPAÇÃO ";
                        break;
                    case "n":
                        $co = "SEM COPARTICIPAÇÃO";
                        break;
                    default:
                        $co = "SEM COPARTICIPAÇÃO";
                        break;
                }
                $tabelasOrdenadas[$chave][$tabela["produto"]["nome"]][$tabela["tipo"]["nome"]][$tabela["id"] . "-" . $co . $tabela["detalhe_coparticipacao"]] = $tabela;
            }
        }
        // 		debug($tabelasOrdenadas);
        // 		die();
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel("Operadoras");
        $operadora = $this->Operadoras
            ->find("all")
            ->contain([
                'Imagens',
                'Carencias',
                'Regioes',
                'Observacoes',
                'Informacoes',
                'Reembolsos',
                'Redes',
                'Estados',
                'Opcionais',
                'FormasPagamentos'
            ])
            ->where(["Operadoras.id" => $dados["operadora_id"]])
            ->first();

        $this->set(compact('operadora', 'sessao', "totalTabelas", "tabelasOrdenadas", "dadosOperadoras", "tabelas"));
    }

    public function filtroEstado()
    {
        $this->autoRender = false;

        $this->loadModel("Operadoras");
        $estado = $this->getRequest()->getData('estado');
        $operadoras = $this->Operadoras->find("list", ['valueField' => function ($e) {
            return $e->get('nome') . '  ' . $e->get('detalhe');
        }])->where([
            'OR' => [
                ['estado_id' => $estado, 'status <>' => 'INATIVA'],
                ['estado_id' => $estado, 'status IS' => null]
            ]
        ])
            ->order(["Operadoras.nome" => "ASC"])
            ->toArray();
        // debug($operadoras);
        $this->response->type('json');
        $this->response->body(json_encode($operadoras));
    }

    public function duplicar($tabela)
    {
        if (isset($tabela)) {
            $tabela_base = $this->Tabelas->get($tabela)->toArray();

            $tabela_duplicata = $this->Tabelas->newEntity();
            $tabela_duplicata = $this->Tabelas->patchEntity($tabela_duplicata, $tabela_base);
            $tabela_duplicata->validade = 0;
            $tabela_duplicata->new = 1;
            if ($this->Tabelas->save($tabela_duplicata)) {

                //tabelas_cnpj precisa ser duplicado junto com a tabela
                $cnpjs_base = $this->Tabelas->TabelasCnpjs->find('all')->where(['tabela_id' => $tabela]);
                foreach ($cnpjs_base as $cnpjs_base_individual) {
                    $cnpj_duplicata = $this->Tabelas->TabelasCnpjs->newEntity();
                    $cnpj_duplicata = $this->Tabelas->TabelasCnpjs->patchEntity($cnpj_duplicata, $cnpjs_base_individual->toArray());
                    $cnpj_duplicata->tabela_id = $tabela_duplicata->id;
                    if (!$this->Tabelas->TabelasCnpjs->save($cnpj_duplicata)) {
                        $this->Flash->error('Erro ao adicionar CNPJ\'s a tabela.');
                    }

                    //tabelas_cnpj precisa ser duplicado junto com a tabela
                    $regioes_base = $this->Tabelas->Tabelasregioes->find('all')->where(['tabela_id' => $tabela]);
                    foreach ($regioes_base as $regiao) {
                        $regioes_duplicata = $this->Tabelas->Tabelasregioes->newEntity();
                        $regioes_duplicata = $this->Tabelas->Tabelasregioes->patchEntity($regioes_duplicata, $regiao->toArray());
                        $regioes_duplicata->tabela_id = $tabela_duplicata->id;
                        if (!$this->Tabelas->Tabelasregioes->save($regioes_duplicata)) {
                            $this->Flash->error('Erro ao adicionar regiões a tabela.');
                        }
                    }
                }
                $this->Flash->success('Tabela Duplicada com sucesso.');
            }
        } else {
            $this->Flash->error('Erro ao duplicar tabela.');
        }
        $this->redirect(['action' => 'index']);
    }


    public function inativaPorVigencia()
    {
        $tabelasInvalidas = $this->Tabelas->find('list')->where('NOW() > vigencia AND atualizacao = 1');
        foreach ($tabelasInvalidas as $tabela) {
            $tabela = $this->Tabelas->get($tabela);
            $tabela->validade = 0;
            $this->Tabelas->save($tabela);
        }
        $this->autoRender = false;
    }

    public function findComercializacoes($operadora)
    {
        $this->loadModel('Regioes');
        $regioes = $this->Regioes->find('list', ['valueField' => 'nome'])->where(['operadora_id' => $operadora]);
        $this->set(compact('regioes'));
    }

    /**
     * Método de colocar Tabela em Atualização
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function colocarAtlz($id = null)
    {
        $this->autoRender = false;
        $tabela = $this->Tabelas->get($id);
        $tabela->atualizacao = 1;

        if ($this->Tabelas->save($tabela)) {
            $res['ok'] = true;
            $res['msg'] = 'Tabela Invalidada com sucesso';
        } else {
            $res['ok'] = true;
            $res['msg'] = 'Erro ao Invalidada com sucesso. Tente Novamente';
        }

        $this->response->type('json');
        $this->response->body(json_encode($res));
    }

    /**
     * Método de remover Tabela em Atualização
     *
     * @param string|null $id Tabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function removerAtlz($id = null)
    {
        $this->autoRender = false;
        $tabela = $this->Tabelas->get($id);

        $tabela->atualizacao = 0;
        // $user->email= abc@gmail.com; // other fields if necessary
        if ($this->Tabelas->save($tabela)) {
            $res['ok'] = true;
            $res['msg'] = 'Tabela Invalidada com sucesso';
        } else {
            $res['ok'] = false;
            $res['msg'] = 'Erro ao Invalidada com sucesso. Tente Novamente';
        }
        $this->response->type('json');
        $this->response->body(json_encode($res));
    }

    /**
     * Filtro por estado e operadora
     *
     * @param string $estado Estado id.
     * @param string|null $operadora Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtro($estado = null, $operadora = null)
    {
        if ($this->request->is('post')) {
            $estado = $this->request->data('estados');
            $operadora = $this->request->data('operadora_id');
        }

        $operadoras = $this->Tabelas->Operadoras->find('list', ['valueField' => function ($e) {
            return $e->get('nome') . " " . $e->get('detalhe');
        }])
            ->where([
                'OR' => [
                    ['estado_id' => $estado, 'status' => ''],
                    ['estado_id' => $estado, 'status IS' => null]
                ],
                'NOT' => 'status <=> "INATIVA"',
                '1 = 1'
            ])
            ->orderASC('Operadoras.nome')
            ->toArray();

        $conditions[] = ['Operadoras.estado_id' => $estado];
        if ($operadora != null && $operadora > 0) {
            $conditions[] = ['Operadoras.id' => $operadora];
        }

        $this->paginate = [
            'valueField' => 'nome', 'order' => ['Tabelas.prioridade' => 'ASC'],
            'contain' => [
                'Estados',
                'Produtos',
                'TiposProdutos',
                'Operadoras' => ['Imagens'],
                'Abrangencias',
                'Tipos',
                "TabelasCnpjs" => [
                    "Cnpjs"
                ]
            ],
            'order' => [
                'Tabelas.validade' => 'DESC',
                'Tabelas.prioridade' => 'ASC'
            ],
            'conditions' => $conditions
        ];
        $tabelas = $this->paginate($this->Tabelas);
        $operadoras = $this->Tabelas->Operadoras->find('list', ['limit' => 200, 'valueField' => 'nome']);
        $estados = $this->Tabelas->Operadoras->find('list', ['valueField' => 'estado_id', 'conditions' => ['1 = 1']])->toArray();
        $estados = $this->Tabelas->Operadoras->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estados]);
        $this->set(compact('operadoras', 'tabelas', 'estados', 'operadora', 'estado'));
    }

    public function filtroAssociacoes(int $area_comercializacao = null)
    {
        $this->loadModel('AreasComercializacoes');
        $area_comercializacao = $this->AreasComercializacoes->get($area_comercializacao);
        $this->loadModel('Operadoras');
        $operadora = $this->Operadoras->get($area_comercializacao->operadora_id, [
            'contain' => ['Redes', 'Observacoes', 'Reembolsos', 'Carencias', 'Opcionais', 'Informacoes', 'FormasPagamentos']
        ]);
        $formas_pagamentos = (new \Cake\Collection\Collection($operadora->formas_pagamentos))->combine('id', 'nome');
        $informacoes = (new \Cake\Collection\Collection($operadora->informacoes))->combine('id', 'nome');
        $opcionais = (new \Cake\Collection\Collection($operadora->opcionais))->combine('id', 'nome');
        $carencias = (new \Cake\Collection\Collection($operadora->carencias))->combine('id', 'nome');
        $reembolsos = (new \Cake\Collection\Collection($operadora->reembolsos))->combine('id', 'nome');
        $observacoes = (new \Cake\Collection\Collection($operadora->observacoes))->combine('id', 'nome');
        $redes = (new \Cake\Collection\Collection($operadora->redes))->combine('id', 'nome');

        $this->set(compact('formas_pagamentos', 'informacoes', 'opcionais', 'carencias', 'reembolsos', 'observacoes', 'redes'));
    }

    public function getFaixas(int $area)
    {
        $area = $this->Tabelas->AreasComercializacoes->get($area)->operadora_id;
        $is_vitalmed = false;

        // ID ESPECÍFICO DA VITALMED
        $ids_vitalmed = [228];
        if (in_array($area, $ids_vitalmed)) {
            $is_vitalmed = true;
        }

        $this->set('is_vitalmed', $is_vitalmed);
    }
}
