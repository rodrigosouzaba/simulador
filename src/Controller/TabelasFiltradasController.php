<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TabelasFiltradas Controller
 *
 * @property \App\Model\Table\TabelasFiltradasTable $TabelasFiltradas
 */
class TabelasFiltradasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Filtros', 'Tabelas']
        ];
        $tabelasFiltradas = $this->paginate($this->TabelasFiltradas);

        $this->set(compact('tabelasFiltradas'));
        $this->set('_serialize', ['tabelasFiltradas']);
    }

    /**
     * View method
     *
     * @param string|null $id Tabelas Filtrada id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tabelasFiltrada = $this->TabelasFiltradas->get($id, [
            'contain' => ['Filtros', 'Tabelas']
        ]);

        $this->set('tabelasFiltrada', $tabelasFiltrada);
        $this->set('_serialize', ['tabelasFiltrada']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tabelasFiltrada = $this->TabelasFiltradas->newEntity();
        if ($this->request->is('post')) {
            $tabelasFiltrada = $this->TabelasFiltradas->patchEntity($tabelasFiltrada, $this->request->data);
            if ($this->TabelasFiltradas->save($tabelasFiltrada)) {
                $this->Flash->success(__('The tabelas filtrada has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tabelas filtrada could not be saved. Please, try again.'));
            }
        }
        $filtros = $this->TabelasFiltradas->Filtros->find('list', ['limit' => 200]);
        $tabelas = $this->TabelasFiltradas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('tabelasFiltrada', 'filtros', 'tabelas'));
        $this->set('_serialize', ['tabelasFiltrada']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tabelas Filtrada id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tabelasFiltrada = $this->TabelasFiltradas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tabelasFiltrada = $this->TabelasFiltradas->patchEntity($tabelasFiltrada, $this->request->data);
            if ($this->TabelasFiltradas->save($tabelasFiltrada)) {
                $this->Flash->success(__('The tabelas filtrada has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tabelas filtrada could not be saved. Please, try again.'));
            }
        }
        $filtros = $this->TabelasFiltradas->Filtros->find('list', ['limit' => 200]);
        $tabelas = $this->TabelasFiltradas->Tabelas->find('list', ['limit' => 200]);
        $this->set(compact('tabelasFiltrada', 'filtros', 'tabelas'));
        $this->set('_serialize', ['tabelasFiltrada']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tabelas Filtrada id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tabelasFiltrada = $this->TabelasFiltradas->get($id);
        if ($this->TabelasFiltradas->delete($tabelasFiltrada)) {
            $this->Flash->success(__('The tabelas filtrada has been deleted.'));
        } else {
            $this->Flash->error(__('The tabelas filtrada could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
