<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\AreasComercializaco;
use App\Utility\PdfWriter;
use Cake\I18n\Number;
use Cake\View\ViewBuilder;
use Knp\Snappy\Pdf;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Event\Event;

/**
 * TabelasGeradas Controller
 *
 * @property \App\Model\Table\TabelasGeradasTable $TabelasGeradas
 */
class TabelasGeradasController extends AppController
{

    /**
     * Permissões para usuário NÃO LOGADO devem estar aqui
     *
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.

        $this->Auth->allow([
            "findPfOperadoras",
            "coparticipacao",
            'geradorAuto',
            "email",
            "findRamos",
            "findOperadoras",
            "geradorVidas",
            "pdf",
            "geradorProdutos",
            "tabela",
            "shared",
            "gerador",
            "filterAreasComercializacoes",
            "filterContratacao"
        ]);
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Estados']
        ];
        $tabelasGeradas = $this->paginate($this->TabelasGeradas);

        $this->set(compact('tabelasGeradas'));
        $this->set('_serialize', ['tabelasGeradas']);
    }

    /**
     * View method
     *
     * @param string|null $id Tabelas Gerada id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dadossalvos = $this->TabelasGeradas->get($id, [
            'contain' => ['Users', 'Estados', 'TabelasGeradasProdutos']
        ]);
        /*
          debug($dadossalvos);
          $this->set('tabelasGerada', $tabelasGerada);
          $this->set('_serialize', ['tabelasGerada']);
         */
        $this->set(compact('dadossalvos'));
    }

    /**
     * Método para checar coparticipação das Operadoras
     *
     * @param string|null $id Tabelas Gerada id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function coparticipacao()
    {

        $estado = $this->request->getData('estado_id');
        $ramo = $this->request->getData('ramo_id');
        $operadora = $this->request->getData('operadora_id');
        $comercializacao = $this->request->getData('area_comercializacao_id');

        switch ($ramo) {
            case "SPF":
                $this->loadModel("PfTabelas");
                $coparticipacao = $this->PfTabelas
                    ->find("list", [
                        'keyField' => function ($q) {
                            return $q->get('coparticipacao');
                        },
                        'valueField' => function ($q) {
                            if ($q->get('coparticipacao') == 's') {
                                return 'COM COPARTICIPAÇÃO';
                            }
                            return 'SEM COPARTICIPAÇÃO';
                        }
                    ])
                    ->matching('PfAreasComercializacoes.PfOperadoras', function ($q) use ($operadora) {
                        return $q->where(['PfOperadoras.id' => $operadora]);
                    })
                    ->group('PfTabelas.coparticipacao')
                    ->toArray();
                break;
            case "SPJ":
                $this->loadModel("Tabelas");
                $coparticipacao = $this->Tabelas
                    ->find("list", [
                        'keyField' => function ($q) {
                            return $q->get('coparticipacao');
                        },
                        'valueField' => function ($q) {
                            if ($q->get('coparticipacao') == 's') {
                                return 'COM COPARTICIPAÇÃO';
                            }
                            return 'SEM COPARTICIPAÇÃO';
                        }
                    ])
                    ->matching('AreasComercializacoes.Operadoras', function ($q) use ($operadora) {
                        return $q->where(['Operadoras.id' => $operadora]);
                    })
                    ->where(['Tabelas.tipo_contratacao' => $this->request->getData('contratacao_id')])
                    ->group('Tabelas.coparticipacao')
                    ->toArray();
                break;
            case "OPF":
                $this->loadModel("OdontoTabelas");
                $tabelas = $this->OdontoTabelas
                    ->find("all")
                    ->where(["estado_id" => $estado, "odonto_operadora_id" => $operadora, "tipo_pessoa" => "PF"]);
                break;
            case "OPJ":
                $this->loadModel("OdontoTabelas");
                $tabelas = $this->OdontoTabelas
                    ->find("all")
                    ->where(["estado_id" => $estado, "odonto_operadora_id" => $operadora, "tipo_pessoa" => "PJ"]);
                break;
        }
        if (isset($tabelas)) {
            foreach ($tabelas as $tabela) {
                $coparticipacao[$tabela["coparticipacao"]] = $tabela["coparticipacao"] == 's' ? 'COM COPARTICIPAÇÃO' : 'SEM COPARTICIPAÇÃO';
            }
        }

        // $coparticipacao[99] = "TODAS AS OPÇÕES";
        array_unshift($coparticipacao, "TODAS AS OPÇÕES");
        $this->set(compact('coparticipacao'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel("TabelasGeradasProdutos");

        $dados = $this->request->data();
        $estados = $this->TabelasGeradas->Estados->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1'])->toArray();

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        foreach ($dados["Produtos"] as $produto) {
            if ($produto <> 0) {
                $salvarProdutos[] = $produto;
            }
        }

        $coparticipacao = "";

        switch ($dados["coparticipacao"]) {
            case 's':
                $coparticipacao = "COM COPARTICIPAÇÃO";
                break;
            case 'n':
                $coparticipacao = "SEM COPARTICIPAÇÃO";
                break;
            case '99':
                $coparticipacao = "";
                break;
        }

        switch ($dados["ramo_id"]) {
            case "SPF":
                $ramo = "Saúde_Pessoa_Física";
                $this->loadModel("PfOperadoras");
                $operadora = $this->PfOperadoras->find('all')->where(["id" => $dados["operadora_id"]])->first()->toArray();
                $nomeOperadora = $operadora["nome"] . "-" . $operadora["detalhe"];
                $vidas = "";
                break;
            case "SPJ":
                $ramo = "Saúde_Pessoa_Jurídica";
                $this->loadModel("Operadoras");
                $operadora = $this->Operadoras->find('all')->where(["id" => $dados["operadora_id"]])->first()->toArray();
                $nomeOperadora = $operadora["nome"] . "-" . $operadora["detalhe"];
                $vidas = "-" . $dados["vidas"] . "_vidas_";
                break;
            case "OPF":
                $ramo = "Odonto_Pessoa_Física";
                $this->loadModel("OdontoOperadoras");
                $operadora = $this->OdontoOperadoras->find('all')->where(["id" => $dados["operadora_id"]])->first()->toArray();
                $nomeOperadora = $operadora["nome"];
                $vidas = "";
                break;
            case "OPJ":
                $ramo = "Odonto_Pessoa_Jurídica";
                $this->loadModel("OdontoOperadoras");
                $operadora = $this->OdontoOperadoras->find('all')->where(["id" => $dados["operadora_id"]])->first()->toArray();
                $nomeOperadora = $operadora["nome"];
                $vidas = "-" . $dados["vidas"] . "_vidas_";
                break;
        }

        $tabelasGerada["nome"] = $nomeOperadora . $coparticipacao . $vidas . $ramo . "-" . $estados[$dados["estado_id"]];
        $tabelasGerada["ramo"] = $dados["ramo_id"];
        $tabelasGerada["operadora"] = $dados["operadora_id"];
        $tabelasGerada["user_id"] = $sessao["id"];
        $tabelasGerada["estado_id"] = $dados["estado_id"];
        $tabelasGerada["coparticipacao"] = $dados["coparticipacao"];
        $tabelasGerada["profissao_id"] = $dados["profissao_id"];
        $tabelasGerada["compartilhada"] = $dados["compartilhada"];

        if ($dados["ramo_id"] != 'SPF' || $dados["ramo_id"] != "OPF") {
            $tabelasGerada["vidas"] = $dados["vidas"];
        } else {
            $tabelasGerada["vidas"] = "N/A";
        }

        // Verifica se já existe essa tabela salva
        $countExistente = $this->TabelasGeradas->find('list')->where(['ramo' => $tabelasGerada['ramo'], 'operadora' => $tabelasGerada['operadora'], 'user_id' => $tabelasGerada['user_id'], 'estado_id' => $tabelasGerada['estado_id'], 'coparticipacao' => $tabelasGerada['coparticipacao'], 'profissao_id' => $tabelasGerada['profissao_id']])->count();
        $firstExistente = $this->TabelasGeradas->find('list')->where(['ramo' => $tabelasGerada['ramo'], 'operadora' => $tabelasGerada['operadora'], 'user_id' => $tabelasGerada['user_id'], 'estado_id' => $tabelasGerada['estado_id'], 'coparticipacao' => $tabelasGerada['coparticipacao'], 'profissao_id' => $tabelasGerada['profissao_id']])->first();

        if ($firstExistente !== null) {
            $produtosExistentes = $this->TabelasGeradasProdutos->find('list', ['valueField' => 'produto_id'])->where(['tabela_gerada_id' => $firstExistente])->toArray();
            $comparaProdutos = $this->array_equal($produtosExistentes, $salvarProdutos);
        }
        // debug($produtosExistentes);
        // debug($salvarProdutos);
        if ($countExistente == 0 || ($countExistente > 0 && $comparaProdutos == false)) {

            $tabelasGeradaEnt = $this->TabelasGeradas->newEntity();
            $tabelasGeradaEnt = $this->TabelasGeradas->patchEntity($tabelasGeradaEnt, $tabelasGerada);

            if ($this->TabelasGeradas->save($tabelasGeradaEnt)) {
                foreach ($salvarProdutos as $salvarProduto) {
                    $dadosProdutos["tabela_gerada_id"] = $tabelasGeradaEnt->id;
                    $dadosProdutos["produto_id"] = $salvarProduto;
                    $tabelasGeradaProdutosEnt = $this->TabelasGeradasProdutos->newEntity();
                    $tabelasGeradaProdutosEnt = $this->TabelasGeradasProdutos->patchEntity($tabelasGeradaProdutosEnt, $dadosProdutos);
                    $this->TabelasGeradasProdutos->save($tabelasGeradaProdutosEnt);
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Tabelas Gerada id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tabelasGerada = $this->TabelasGeradas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tabelasGerada = $this->TabelasGeradas->patchEntity($tabelasGerada, $this->request->data);
            if ($this->TabelasGeradas->save($tabelasGerada)) {
                $this->Flash->success(__('The tabelas gerada has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tabelas gerada could not be saved. Please, try again.'));
            }
        }
        $users = $this->TabelasGeradas->Users->find('list', ['limit' => 200]);
        $estados = $this->TabelasGeradas->Estados->find('list', ['limit' => 200]);
        $this->set(compact('tabelasGerada', 'users', 'estados'));
        $this->set('_serialize', ['tabelasGerada']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tabelas Gerada id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tabelasGerada = $this->TabelasGeradas->get($id);
        if ($this->TabelasGeradas->delete($tabelasGerada)) {
            $this->Flash->success(__('The tabelas gerada has been deleted.'));
        } else {
            $this->Flash->error(__('The tabelas gerada could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /*     * *********************************** GERAÇÃO DE TABELAS PERSONALIZADAS ************************************* */

    /*

      Método Gerador de tabelas personalizadas

     */

    public function gerador()
    {
        $this->loadModel('Estados');
        $estadosPME = $this->Estados
            ->find('list')
            ->join([
                'table' => 'areas_comercializacoes_estados_municipios',
                'alias' => 'acem',
                'type' => 'INNER',
                'conditions' => 'Estados.id = acem.estado_id'
            ])
            ->join([
                'table' => 'areas_comercializacoes',
                'alias' => 'ac',
                'type' => 'INNER',
                'conditions' => 'acem.area_comercializacao_id = ac.id'
            ])
            ->join([
                'table' => 'tabelas',
                'alias' => 't',
                'type' => 'INNER',
                'conditions' => 'ac.id = t.area_comercializacao_id'
            ])
            ->group('Estados.id');
        $estadosPF = $this->Estados
            ->find('list')
            ->join([
                'table' => 'pf_areas_comercializacoes_estados_municipios',
                'alias' => 'pacem',
                'type' => 'INNER',
                'conditions' => 'Estados.id = pacem.estado_id'
            ])
            ->group('Estados.id');

        $estadosOdonto = $this->Estados
            ->find('list')
            ->join([
                'table' => 'odonto_operadoras',
                'alias' => 'o_o',
                'type' => 'INNER',
                'conditions' => 'Estados.id = o_o.estado_id'
            ])
            ->join([
                'table' => 'odonto_tabelas',
                'alias' => 'ot',
                'type' => 'INNER',
                'conditions' => 'o_o.id = ot.odonto_operadora_id and ot.validade = 1'
            ])
            ->group('Estados.id');

        $estados = $estadosPF->unionAll($estadosPME)->unionAll($estadosOdonto);

        $this->loadModel("PfProfissoes");
        $profissoes = $this->PfProfissoes->find('list', ["valueField" => "nome", 'conditions' => '1 = 1'])->order(["nome" => "ASC"])->toArray();
        $array = array(0 => "TODAS AS PROFISSÕES", 9999 => "SEM PROFISSÃO");
        $profissoes = $array + $profissoes;

        if ($this->Auth->user()) {
            $user_estado = $this->Auth->user('estado_id');

            $this->loadModel("Tabelas");
            $spj = $this->Tabelas->find('all')
                ->matching('AreasComercializacoes.Municipios', function ($q) use ($user_estado) {
                    return $q->where(['Municipios.estado_id' => $user_estado]);
                })
                ->where(['validade' => 1])
                ->group('Tabelas.id')
                ->count();

            //TABELAS PF
            $this->loadModel("PfTabelas");
            $spf = $this->PfTabelas->find('all')
                ->matching('PfAreasComercializacoes.Municipios', function ($q) use ($user_estado) {

                    return $q->where(['Municipios.estado_id' => $user_estado]);
                })
                ->where(['validade' => 1])
                ->group('PfTabelas.id')
                ->count();

            //TABELAS ODONTO PF
            $this->loadModel("OdontoTabelas");
            $opf = $this->OdontoTabelas
                ->find('all')
                ->where(['validade' => 1, 'estado_id' => $this->Auth->user('estado_id'), 'tipo_pessoa' => 'PF'])
                ->count();

            //TABELAS ODONTO PJ
            $this->loadModel("OdontoTabelas");
            $opj = $this->OdontoTabelas
                ->find('all')
                ->where(['validade' => 1, 'estado_id' => $this->Auth->user('estado_id'), 'tipo_pessoa' => 'PJ'])
                ->count();

            $ramos = array();

            if ($spf > 0) {
                $ramos['SPF'] = 'SAÚDE - PESSOA FÍSICA';
            }
            if ($spj > 0) {
                $ramos['SPJ'] = 'SAÚDE - PME';
            }

            if ($opf > 0) {
                $ramos['OPF'] = 'ODONTO - PESSOA FÍSICA';
            }

            if ($opj > 0) {
                $ramos['OPJ'] = 'ODONTO - PME';
            }

            $this->set('ramos', $ramos);
        }

        $this->set(compact('estados', 'profissoes'));
    }

    /*     * *********************************** GERAÇÃO DE TABELAS PERSONALIZADAS ABERTO - SEM USUÁRIO LOGADO ************************************* */

    /*

      Método Gerador de tabelas personalizadas

     */

    public function geradorTabelas()
    {
        if ($this->request->is(['post'])) {
            $ids = $this->request->data();

            //Removendo tabelas não selecionadas
            foreach ($ids as $chave => $valor) {
                if ($valor == 0) {
                    unset($ids[$chave]);
                } else {
                    $t[$valor] = $valor;
                }
            }

            $this->set('title', 'Tabela');
            $tabelas = $this->Tabelas->find('all')
                ->contain([
                    'Ramos',
                    'Regioes',
                    'Estados',
                    'Abrangencias',
                    'Tipos',
                    'Produtos' => ["TiposProdutos"],
                    'Operadoras' => ['Imagens', 'Carencias', 'Redes', 'Observacoes', 'Informacoes', 'Opcionais', 'Reembolsos'],
                    'Modalidades'
                ])
                ->where(['Tabelas.id IN' => $t]);
            $total = count($tabelas->toArray());

            foreach ($tabelas as $tabela) {
                if ($tabela['cod_ans'] != '') {
                    $tabelasOrdenadas[$tabela['produto']['nome']][$tabela['tipo']['nome']][$tabela['cod_ans']] = $tabela;
                } else {
                    $tabelasOrdenadas[$tabela['produto']['nome']][$tabela['tipo']['nome']][] = $tabela;
                }
            }

            $tabelas = $tabelasOrdenadas;


            foreach ($tabelas as $t) {
                foreach ($t as $dados) {
                    foreach ($dados as $info) {
                        $validade = $info['vigencia'];
                        $regiao = $info['regio']['estado']['nome'] . " / " . $info['regio']['nome'];
                        $titulo = $info['operadora']['nome'] . " - PLANO " . $info['modalidade']['nome'] . " " . $info['minimo_vidas'] . " À " . $info['maximo_vidas'] . " VIDAS";
                        $imagem = $info['operadora']['imagen']['caminho'] . $info['operadora']['imagen']['nome'];
                        break;
                    }
                }
            }


            //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
            $tipoPDF = 'D';
            //Nome do PDF
            $nomePDF = $titulo . ".pdf";

            $btnCancelar = ['controller' => 'tabelas', 'action' => 'index'];
            $this->set('tabela', $tabelas);

            $this->set('_serialize', ['tabelas']);

            $this->set(compact('tabelas', 'btnCancelar', 'sessao', 'total', 'nomePDF', 'tipoPDF', 'titulo'));
            $this->viewBuilder()->layout('tabela');
            $this->render('gerar_tabela');
        }

        $this->loadModel('Estados');
        $estadosPME = $this->Estados
            ->find('list')
            ->join([
                'table' => 'areas_comercializacoes_estados_municipios',
                'alias' => 'acem',
                'type' => 'INNER',
                'conditions' => 'Estados.id = acem.estado_id'
            ])
            ->join([
                'table' => 'areas_comercializacoes',
                'alias' => 'ac',
                'type' => 'INNER',
                'conditions' => 'acem.area_comercializacao_id = ac.id'
            ])
            ->join([
                'table' => 'tabelas',
                'alias' => 't',
                'type' => 'INNER',
                'conditions' => 'ac.id = t.area_comercializacao_id'
            ])
            ->group('Estados.id');
        $estadosPF = $this->Estados
            ->find('list')
            ->join([
                'table' => 'pf_areas_comercializacoes_estados_municipios',
                'alias' => 'pacem',
                'type' => 'INNER',
                'conditions' => 'Estados.id = pacem.estado_id'
            ])
            ->group('Estados.id');

        $estadosOdonto = $this->Estados
            ->find('list')
            ->join([
                'table' => 'odonto_operadoras',
                'alias' => 'o_o',
                'type' => 'INNER',
                'conditions' => 'Estados.id = o_o.estado_id'
            ])
            ->join([
                'table' => 'odonto_tabelas',
                'alias' => 'ot',
                'type' => 'INNER',
                'conditions' => 'o_o.id = ot.odonto_operadora_id and ot.validade = 1'
            ])
            ->group('Estados.id');

        $estados = $estadosPF->unionAll($estadosPME)->unionAll($estadosOdonto);

        $this->loadModel("PfProfissoes");
        $profissoes = $this->PfProfissoes->find('list', ["valueField" => "nome", 'conditions' => '1 = 1'])->order(["nome" => "ASC"])->toArray();
        $array = array(0 => "TODAS AS PROFISSÕES", 9999 => "SEM PROFISSÃO");
        $profissoes = $array + $profissoes;

        $this->set(compact('estados', 'profissoes'));
    }

    public function findOperadoras($ramo = null, $estado = null, $profissao = null)
    {
        $conditions = null;
        $idsPfOperadoras = null;

        //RAMO SELECIONADO
        switch ($ramo) {
            case "SPJ":
                $this->loadModel('Operadoras');
                $operadoras = $this->Operadoras
                    ->find('list', ['valueField' => function ($q) {
                        return $q->get('nome') . ' - ' . $q->get('detalhe');
                    }])
                    ->matching('AreasComercializacoes.Tabelas', function ($q) {
                        return $q->where(['Tabelas.new' => 1, 'Tabelas.validade' => 1]);
                    })
                    ->order('Operadoras.nome');
                break;
            case "OPF":
                $this->loadModel('OdontoOperadoras');
                $operadoras = $this->OdontoOperadoras
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('OdontoAreasComercializacoes.OdontoTabelas', function ($q) {
                        return $q->where(['OdontoTabelas.tipo_pessoa' => 'PF', 'OdontoTabelas.validade' => 1]);
                    })
                    ->matching('OdontoAreasComercializacoes.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->order('OdontoOperadoras.nome');
                // debug($operadoras->toArray());
                // die;
                break;
            case "OPJ":
                $this->loadModel('OdontoOperadoras');
                $operadoras = $this->OdontoOperadoras
                    ->find('list', ['valueField' => 'nome'])
                    ->matching('OdontoAreasComercializacoes.OdontoTabelas', function ($q) {
                        return $q->where(['OdontoTabelas.tipo_pessoa' => 'PJ', 'OdontoTabelas.validade' => 1]);
                    })
                    ->matching('OdontoAreasComercializacoes.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->order('OdontoOperadoras.nome');
                // debug($operadoras);
                // die;
                break;
        }
        $this->set(compact("operadoras"));
    }


    public function findPfOperadoras($estado = null, $profissao = null)
    {
        $this->loadModel('PfOperadoras');
        switch ($profissao) {
            case 0: //TODAS AS PROFISSOES - Todas as Operadoras do Estado com tabelas válidas
                $operadoras_municipios = $this->PfOperadoras
                    ->find('list', ['valueField' => 'id'])
                    ->matching('PfAreasComercializacoes.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas', function ($q) {
                        return $q->where(['PfTabelas.new' => 1, 'PfTabelas.validade' => 1]);
                    })
                    ->toArray();
                $operadoras_metropoles = $this->PfOperadoras
                    ->find('list', ['valueField' => 'id'])
                    ->matching('PfAreasComercializacoes.Metropoles.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas', function ($q) {
                        return $q->where(['PfTabelas.new' => 1, 'PfTabelas.validade' => 1]);
                    })
                    ->toArray();
                $operadoras = $operadoras_municipios + $operadoras_metropoles;
                $operadoras = $this->PfOperadoras
                    ->find('list', ['valueField' => function ($q) {
                        return $q->nome . ' - ' . $q->detalhe;
                    }])
                    ->where(['id IN' => $operadoras])
                    ->orderAsc('PfOperadoras.nome');

                break;
            case 9999: //SEM PROFISSAO - Apenas Operadoras com Tabelas INDIVIDUAIS
                $operadoras = $this->PfOperadoras
                    ->find('list', ['valueField' => function ($q) {
                        return $q->get('nome') . ' - ' . $q->get('detalhe');
                    }])
                    ->matching('PfAreasComercializacoes.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas', function ($q) {
                        return $q->where(['PfTabelas.new' => 1, 'PfTabelas.validade' => 1, 'PfTabelas.modalidade' => 'INDIVIDUAL']);
                    })
                    ->orderAsc('PfOperadoras.nome')
                    ->toArray();
                $operadoras_metropoles = $this->PfOperadoras
                    ->find('list', ['valueField' => function ($q) {
                        return $q->get('nome') . ' - ' . $q->get('detalhe');
                    }])
                    ->matching('PfAreasComercializacoes.Metropoles.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas', function ($q) {
                        return $q->where(['PfTabelas.new' => 1, 'PfTabelas.validade' => 1, 'PfTabelas.modalidade' => 'INDIVIDUAL']);
                    })
                    ->orderAsc('PfOperadoras.nome')
                    ->toArray();

                $operadoras = $operadoras + $operadoras_metropoles;
                break;
            default:
                $individual = $this->PfOperadoras
                    ->find('list', ['valueField' => 'id'])
                    ->matching('PfAreasComercializacoes.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas', function ($q) {
                        return $q->where(['PfTabelas.new' => 1, 'PfTabelas.validade' => 1, 'PfTabelas.modalidade' => 'INDIVIDUAL']);
                    })->toArray();
                $individual_metropoles = $this->PfOperadoras
                    ->find('list', ['valueField' => 'id'])
                    ->matching('PfAreasComercializacoes.Metropoles.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas', function ($q) {
                        return $q->where(['PfTabelas.new' => 1, 'PfTabelas.validade' => 1, 'PfTabelas.modalidade' => 'INDIVIDUAL']);
                    })->toArray();
                $individual = $individual + $individual_metropoles;

                $adesao = $this->PfOperadoras
                    ->find('list', ['valueField' => 'id'])
                    ->matching('PfAreasComercializacoes.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas', function ($q) {
                        return $q->where(['PfTabelas.new' => 1, 'PfTabelas.validade' => 1, 'PfTabelas.modalidade' => 'ADESAO']);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas.PfEntidadesGrupos.PfEntidades.PfProfissoes', function ($q) use ($profissao) {
                        return $q->where(['PfProfissoes.id' => $profissao]);
                    })->toArray();
                $adesao_metropoles = $this->PfOperadoras
                    ->find('list', ['valueField' => 'id'])
                    ->matching('PfAreasComercializacoes.Metropoles.Estados', function ($q) use ($estado) {
                        return $q->where(['Estados.id' => $estado]);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas', function ($q) {
                        return $q->where(['PfTabelas.new' => 1, 'PfTabelas.validade' => 1, 'PfTabelas.modalidade' => 'ADESAO']);
                    })
                    ->matching('PfAreasComercializacoes.PfTabelas.PfEntidadesGrupos.PfEntidades.PfProfissoes', function ($q) use ($profissao) {
                        return $q->where(['PfProfissoes.id' => $profissao]);
                    })->toArray();
                $adesao = $adesao + $adesao_metropoles;
                if (!empty($adesao)) {
                    $conditions[] = ['id IN' => $adesao];
                }
                if (!empty($individual)) {
                    $conditions[] = ['id IN' => $individual];
                }

                $operadoras = $this->PfOperadoras
                    ->find('list', ['valueField' => function ($q) {
                        return $q->nome . ' - ' . $q->detalhe;
                    }])
                    ->where([
                        'OR' => $conditions
                    ])
                    ->orderAsc('PfOperadoras.nome');
                break;
        }


        $this->set(compact("operadoras"));
        $this->render("findOperadoras");
    }

    public function findRamos()
    {
        ini_set('memory_limit', '-1');
        $estado = $this->request->data('estado_id');
        //TABELAS PME
        $this->loadModel("Tabelas");
        $spj = $this->Tabelas
            ->find('list')
            ->matching('AreasComercializacoes.Estados', function ($q) use ($estado) {
                return $q->where(['Estados.id' => $estado]);
            })
            ->where(['validade' => 1])
            ->count();

        //TABELAS PF
        $this->loadModel("PfTabelas");
        $spf = $this->PfTabelas
            ->find('list')
            ->matching('PfAreasComercializacoes.Estados', function ($q) use ($estado) {
                return $q->where(['Estados.id' => $estado]);
            })
            ->where(['validade' => 1])
            ->count();

        //TABELAS ODONTO PF
        $this->loadModel("OdontoTabelas");
        $opf = $this->OdontoTabelas
            ->find('list')
            ->matching('OdontoAreasComercializacoes.Estados', function ($q) use ($estado) {
                return $q->where(['Estados.id' => $estado]);
            })
            ->where(['validade' => 1, 'tipo_pessoa' => 'PF'])
            ->count();

        //TABELAS ODONTO PJ
        $this->loadModel("OdontoTabelas");
        $opj = $this->OdontoTabelas
            ->find('list')
            ->matching('OdontoAreasComercializacoes.Estados', function ($q) use ($estado) {
                return $q->where(['Estados.id' => $estado]);
            })
            ->where(['validade' => 1, 'tipo_pessoa' => 'PJ'])
            ->count();

        $ramos = array();

        if ($spf > 0) {
            $ramos['SPF'] = 'SAÚDE - PESSOA FÍSICA';
        }
        if ($spj > 0) {
            $ramos['SPJ'] = 'SAÚDE - PME';
        }
        if ($opf > 0) {
            $ramos['OPF'] = 'ODONTO - PESSOA FÍSICA';
        }
        if ($opj > 0) {
            $ramos['OPJ'] = 'ODONTO - PME';
        }

        $this->set(compact("ramos"));
    }

    public function geradorProdutos($id = null)
    {
        $dados = $this->request->data();
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        foreach ($dados["area_comercializacaos"] as $id => $produto) {
            if ($produto == '0') {
                unset($dados["area_comercializacaos"][$id]);
            }
        }

        //RAMO SELECIONADO
        switch ($dados["ramo_id"]) {
            case "SPF":
                $this->loadModel('PfTabelas');
                if (!empty($dados['area_comercializacaos'])) {
                    $produtos = $this->PfTabelas
                        ->find('list', ['valueField' => function ($q) use ($dados) {
                            $nome = $q->get('nome');
                            if ($dados['coparticipacao'] === '0') {
                                $nome .= ($q->get('coparticipacao') == 's' ? ' C/ COPARTICIPAÇÃO' : ' S/ COPARTICIPAÇÃO');
                            }
                            return $nome;
                        }])
                        ->matching('PfAreasComercializacoes', function ($q) use ($dados) {
                            return $q->where(['PfAreasComercializacoes.id IN' => $dados['area_comercializacaos']]);
                        })
                        ->where(['validade' => 1]);
                    if (isset($dados['coparticipacao']) && !empty($dados['coparticipacao']) && $dados['coparticipacao'] !== 0) {
                        $produtos = $produtos->where(['coparticipacao' => $dados['coparticipacao']]);
                    }
                    $produtos = $produtos->toArray();
                } else {
                    $produtos = [];
                }
                break;
            case "SPJ":
                $this->loadModel('Tabelas');
                $produtos = $this->Tabelas->find('list', ['valueField' => function ($q) use ($dados) {
                    $nome = $q->get('nome');
                    if ($dados['coparticipacao'] === '0') {
                        if ((int)$dados['operadora_id'] === 206) {
                            $nome .= ($q->get('coparticipacao') == 's' ? ' C/ COPARTICIPAÇÃO ' . $q->get('detalhe_coparticipacao') : ' S/ COPARTICIPAÇÃO');
                            $nome .= '--' . $q->get('minimo_vidas');
                        } else {
                            $nome .= ($q->get('coparticipacao') == 's' ? ' C/ COPARTICIPAÇÃO ' : ' S/ COPARTICIPAÇÃO');
                            $nome .= '--' . $q->get('minimo_vidas');
                        }
                    } else if ($dados['coparticipacao'] === 's') {
                        $nome .= ' ' . $q->get('detalhe_coparticipacao');
                        $nome .= '--' . $q->get('minimo_vidas');
                    }
                    return $nome;
                }])
                    ->matching('AreasComercializacoes', function ($q) use ($dados) {
                        return $q->where(['AreasComercializacoes.id IN' => $dados['area_comercializacaos']]);
                    });

                if (isset($dados['coparticipacao']) && !empty($dados['coparticipacao']) && $dados['coparticipacao'] !== 0) {
                    $produtos = $produtos->where(['coparticipacao' => $dados['coparticipacao']]);
                }
                if (isset($dados['contratacao_id'])) {
                    $produtos = $produtos->where(['tipo_contratacao' => $dados['contratacao_id']]);
                }
                if (isset($dados['vidas'])) {
                    $vidas = explode('-', $dados['vidas']);
                    // if ((int)$vidas[0] == 3 && (int)$vidas[1] == 29) {
                    //     $conditions_vidas['NOT'] = ['minimo_vidas' => 4];
                    // }
                    $conditions_vidas[] = [
                        'minimo_vidas' => $vidas[0],
                        'maximo_vidas' => $vidas[1]
                    ];
                    $produtos = $produtos->where($conditions_vidas);
                }

                break;
            case "OPF":
            case "OPJ":
                $vidas = explode('-', $dados['vidas']);
                $this->loadModel('OdontoTabelas');
                $produtos = $this->OdontoTabelas->find('list', ['valueField' => 'nome'])
                    ->matching('OdontoAreasComercializacoes', function ($q) use ($dados) {
                        return $q->where(['OdontoAreasComercializacoes.id IN' => $dados['area_comercializacaos']]);
                    })
                    ->where([
                        'minimo_vidas' => $vidas[0],
                        'maximo_vidas' => $vidas[1],
                        'validade' => 1,
                        'tipo_pessoa' => $dados["ramo_id"] == 'OPF' ? 'PF' : 'PJ'
                    ]);
                break;
        }

        $this->set(compact('produtos', 'sessao'));
    }

    public function geradorVidas()
    {

        $ramo = $this->request->getData('ramo_id');
        $idOperadora = $this->request->getData('operadora_id');
        $comercializacao = $this->request->getData('area_comercializacao_id');
        $contratacao = $this->request->getData('contratacao_id');
        $coparticipacao = $this->request->getData('coparticipacao');
        if ($idOperadora) {
            //RAMO SELECIONADO
            switch ($ramo) {
                case "SPF":
                    $model = "PfTabelas";
                    $campoOperadora = "pf_operadora_id";
                    $this->loadModel($model);
                    $tabelas = $this->$model->find('all')
                        ->where([$campoOperadora => $idOperadora, "validade" => 1])
                        ->order(["minimo_vidas" => "ASC"])->toArray();
                    break;
                case "SPJ":
                    $this->loadModel('Tabelas');
                    $options = $this->Tabelas
                        ->find('list', [
                            'keyField' => function ($q) {
                                return $q->get('minimo_vidas') . '-' . $q->get('maximo_vidas');
                            },
                            'valueField' => function ($q) {
                                return $q->get('minimo_vidas') . ' a ' . $q->get('maximo_vidas');
                            }
                        ])
                        ->matching('AreasComercializacoes.Operadoras', function ($q) use ($idOperadora) {
                            return $q->where(['Operadoras.id' => $idOperadora]);
                        });

                    if (isset($dados['coparticipacao']) && !empty($dados['coparticipacao']) && $dados['coparticipacao'] !== 0) {
                        $options->where(['coparticipacao' => $dados['coparticipacao']]);
                    }
                    if (isset($dados['contratacao_id'])) {
                        $options->where(['tipo_contratacao' => $dados['contratacao_id']]);
                    }
                    $options->group(['minimo_vidas', 'maximo_vidas']);
                    break;
                case "OPF":
                    $this->loadModel("OdontoTabelas");
                    $options = $this->OdontoTabelas
                        ->find('list', [
                            'keyField' => function ($q) {
                                return $q->get('minimo_vidas') . '-' . $q->get('maximo_vidas');
                            },
                            'valueField' => function ($q) {
                                return $q->get('minimo_vidas') . ' a ' . $q->get('maximo_vidas');
                            }
                        ])
                        ->matching('OdontoAreasComercializacoes.OdontoOperadoras', function ($q) use ($idOperadora) {
                            return $q->where(['OdontoOperadoras.id' => $idOperadora]);
                        })
                        ->where([
                            "OdontoTabelas.validade" => 1,
                            "OdontoTabelas.tipo_pessoa" => "PF"
                        ])
                        ->order(["minimo_vidas" => "ASC"])
                        ->toArray();
                    break;
                case "OPJ":
                    $this->loadModel("OdontoTabelas");
                    $options = $this->OdontoTabelas
                        ->find('list', [
                            'keyField' => function ($q) {
                                return $q->get('minimo_vidas') . '-' . $q->get('maximo_vidas');
                            },
                            'valueField' => function ($q) {
                                return $q->get('minimo_vidas') . ' a ' . $q->get('maximo_vidas');
                            }
                        ])
                        ->matching('OdontoAreasComercializacoes.OdontoOperadoras', function ($q) use ($idOperadora) {
                            return $q->where(['OdontoOperadoras.id' => $idOperadora]);
                        })
                        ->where([
                            "OdontoTabelas.validade" => 1,
                            "OdontoTabelas.tipo_pessoa" => "PJ"
                        ])
                        ->order(["minimo_vidas" => "ASC"])
                        ->toArray();
                    break;
            }
        }
        if (isset($tabelas) && !empty($tabelas)) {
            $options = array();
            foreach ($tabelas as $tabela) {
                $options[$tabela["minimo_vidas"] . "-" . $tabela["maximo_vidas"]] = $tabela["minimo_vidas"] . " a " . $tabela["maximo_vidas"];
            }
        }

        $this->set(compact('options'));
    }

    public function tabela()
    {
        $dados = $this->request->data();

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        if (isset($dados["vidas"])) {
            $vidas = explode("-", $dados["vidas"]);
        }
        foreach ($dados["Produtos"] as $id => $produto) {
            if ($produto == '0') {
                unset($dados["Produtos"][$id]);
            }
        }

        if (isset($dados["coparticipacao"]))
            $dados["coparticipacao"] = $dados["coparticipacao"] == '0' ? 99 : $dados["coparticipacao"];
        //RAMO SELECIONADO
        switch ($dados["ramo_id"]) {
            case "SPF":
                $this->loadModel("PfTabelas");

                $tabelas = $this->PfTabelas->find('all')
                    ->contain([
                        'PfAtendimentos',
                        'PfAcomodacoes',
                        'PfCoberturas',
                        'PfFormasPagamentos',
                        'PfCarencias',
                        'PfRedes',
                        'PfReembolsos',
                        'PfDocumentos',
                        'PfDependentes',
                        'PfComercializacoes',
                        'PfObservacoes',
                        'PfEntidadesGrupos' => [
                            'PfEntidades' => [
                                'PfProfissoes' =>  function ($q) {
                                    return $q->order('nome');
                                }
                            ],
                        ],
                        'PfEntidades' => [
                            'PfProfissoes' =>  function ($q) {
                                return $q->order('nome');
                            }
                        ],
                        'PfAreasComercializacoes' => [
                            'Metropoles',
                            'Municipios' => ['Estados'],
                            'PfOperadoras' => ['Imagens']
                        ]
                    ])
                    ->where(['PfTabelas.id IN' => $dados["Produtos"]])
                    ->toArray();

                $operadorasIds = array();
                $tabelas_ordenadas = [];
                $area_adicionada = [];
                $grupo_adicionado = [];

                foreach ($tabelas as $tabela) {
                    $operadora_id = $tabela['pf_areas_comercializaco']['pf_operadora']['id'];
                    @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['pf_atendimento']['nome']] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['pf_areas_comercializaco']['pf_operadora'];

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    if (!in_array($tabela['pf_areas_comercializaco']['id'], $area_adicionada)) {
                        foreach ($tabela['pf_areas_comercializaco']['metropoles'] as $metropole) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                        }
                        foreach ($tabela['pf_areas_comercializaco']['municipios'] as $municipio) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                        }
                        $area_adicionada[] = $tabela['pf_areas_comercializaco']['id'];
                    }

                    // NÃO REPETIÇÃO DOS GRUPOS DE ENTIDADES NAS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['tabelas'] .= $tabela['nome'] . ' • ';
                    if (!empty($tabela['pf_entidades_grupo'])) {
                        if (!in_array($tabela['pf_entidades_grupo']['id'], $grupo_adicionado)) {
                            foreach ($tabela['pf_entidades_grupo']['pf_entidades'] as $entidade) {
                                @$tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] .= '<strong>' . $entidade['nome'] . ':</strong> ';
                                foreach ($entidade['pf_profissoes'] as $profissao) {
                                    $tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] .= $profissao['nome'] . ', ';
                                }
                                $tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'] = substr($tabelas_ordenadas[$operadora_id]['grupo_entidades'][$tabela['pf_entidades_grupo']['id']]['data'], 0, -2) . '; ';
                            }
                            $grupo_adicionado[] = $tabela['pf_entidades_grupo']['id'];
                        }
                    }
                    unset($tabela['pf_entidades_grupo']);

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['data'] =  nl2br($tabela['pf_observaco']['descricao']);
                    unset($tabela['pf_observaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['data'] =  nl2br($tabela['pf_rede']['descricao']);
                    unset($tabela['pf_rede']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
                    unset($tabela['reembolsoEntity']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['data'] =  nl2br($tabela['pf_carencia']['descricao']);
                    unset($tabela['pf_carencia']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['data'] =  nl2br($tabela['pf_dependente']['descricao']);
                    unset($tabela['pf_dependente']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['data'] =  nl2br($tabela['pf_documento']['descricao']);
                    unset($tabela['pf_documento']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['data'] =  nl2br($tabela['pf_formas_pagamento']['descricao']);
                    unset($tabela['pf_formas_pagamento']);

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['nome'] =  $tabela['nome'];
                    foreach ($tabela['pf_entidades'] as $entidade) {
                        @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .=  '<strong>' . $entidade['nome'] . '</strong>: ';
                        foreach ($entidade['pf_profissoes'] as $profissao) {
                            @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .= $profissao['nome'] . ', ';
                        }
                        @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] = substr($tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'], 0, -2);
                        @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .= '. ';
                    }
                    @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .= '</br>';
                    // unset($tabela['pf_entidade']);

                    $tabela['cobertura'] = $tabela->pf_cobertura;
                    $tabela['acomodacao'] = $tabela->pf_acomodaco;

                    @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
                }

                $ids_vitalmed = [92];
                if (in_array($dados['operadora_id'], $ids_vitalmed)) {
                    $is_vitalmed = true;
                    $faixa_6 = (new \Cake\Collection\Collection($tabelas_ordenadas[$operadora_id]['tabelas']))
                        ->filter(function ($tabela) {
                            return $tabela->tipo_faixa === '6';
                        })
                        ->chunk(5)
                        ->toArray();
                    $faixa_3 = (new \Cake\Collection\Collection($tabelas_ordenadas[$operadora_id]['tabelas']))
                        ->filter(function ($tabela) {
                            return $tabela->tipo_faixa === '3';
                        })
                        ->chunk(5)
                        ->toArray();
                    $default = (new \Cake\Collection\Collection($tabelas_ordenadas[$operadora_id]['tabelas']))
                        ->reject(function ($tabela) {
                            return $tabela->tipo_faixa === '3' || $tabela->tipo_faixa === '6';
                        })
                        ->chunk(5)
                        ->toArray();

                    $tabelas_ordenadas[$operadora_id]['tabelas'] = [];
                    $tabelas_ordenadas[$operadora_id]['tabelas']['faixa_3'] = $faixa_3;
                    $tabelas_ordenadas[$operadora_id]['tabelas']['faixa_6'] = $faixa_6;
                    $tabelas_ordenadas[$operadora_id]['tabelas']['default'] = $default;
                } else {
                    $tabelas_ordenadas[$operadora_id]['tabelas'] = array_chunk($tabelas_ordenadas[$operadora_id]['tabelas'], 5, true);
                }

                $tabelasOrdenadas = $tabelas_ordenadas;

                $this->loadModel('PfOperadoras');
                $operadora = $this->PfOperadoras->get($dados['operadora_id']);
                $this->set(compact('is_vitalmed'));

                break;
            case "SPJ":
                $this->loadModel('Tabelas');
                $tabelas = $this->Tabelas
                    ->find('all')
                    ->contain([
                        'Abrangencias',
                        'Tipos',
                        'Coberturas',
                        'Carencias',
                        'Redes',
                        'Observacoes',
                        'Informacoes',
                        'Opcionais',
                        'Reembolsos',
                        'FormasPagamentos',
                        "TabelasCnpjs" => [
                            "Cnpjs"
                        ],
                        'AreasComercializacoes' => [
                            'Municipios' => ['Estados'],
                            'Operadoras' => ['Imagens']
                        ]
                    ])
                    ->where(['Tabelas.id IN' => $dados['Produtos']])
                    ->toArray();

                $tabelas_ordenadas = [];
                $area_adicionada = [];
                foreach ($tabelas as $tabela) {
                    $operadora_id = $tabela['areas_comercializaco']['operadora']['id'];
                    @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['areas_comercializaco']['operadora'];
                    @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['abrangencia']['nome']] .=  $tabela['nome'] . ' • ';

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    if (!in_array($tabela['areas_comercializaco']['id'], $area_adicionada)) {
                        foreach ($tabela['areas_comercializaco']['metropoles'] as $metropole) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                        }
                        foreach ($tabela['areas_comercializaco']['municipios'] as $municipio) {
                            @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                        }
                        $area_adicionada[] = $tabela['areas_comercializaco']['id'];
                    }
                    unset($tabela['areas_comercializaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['observaco']['id']]['data'] =  nl2br($tabela['observaco']['descricao']);
                    unset($tabela['observaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['rede']['id']]['data'] =  nl2br($tabela['rede']['descricao']);
                    unset($tabela['rede']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
                    unset($tabela['reembolsoEntity']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['carencia']['id']]['data'] =  nl2br($tabela['carencia']['descricao']);
                    unset($tabela['carencia']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['opcionai']['id']]['data'] =  nl2br($tabela['opcionai']['descricao']);
                    unset($tabela['opcionai']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['informaco']['id']]['data'] =  nl2br($tabela['informaco']['descricao']);
                    unset($tabela['informaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['formas_pagamento']['id']]['data'] =  nl2br($tabela['formas_pagamento']['descricao']);
                    unset($tabela['formas_pagamento']);

                    $tabela['cobertura'] = $tabela->cobertura;
                    $tabela['acomodacao'] = $tabela->tipo;

                    @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
                }
                $tabelas_ordenadas[$operadora_id]['tabelas'] = array_chunk($tabelas_ordenadas[$operadora_id]['tabelas'], 5, true);
                $tabelasOrdenadas = $tabelas_ordenadas;
                $this->loadModel('Operadoras');
                $operadora = $this->Operadoras->get($dados['operadora_id'], ['contain' => ['Imagens']]);
                $ids_vitalmed = [228];
                if (in_array($dados['operadora_id'], $ids_vitalmed)) {
                    $is_vitalmed = true;
                }

                $this->set(compact('is_vitalmed'));
                break;

            case "OPJ":
            case "OPF":
                $this->loadModel("OdontoTabelas");
                $tabelas = $this->OdontoTabelas
                    ->find("all")
                    ->contain([
                        'OdontoCarencias',
                        'OdontoComercializacoes',
                        'OdontoObservacaos',
                        'OdontoDocumentos',
                        'OdontoReembolsos',
                        'OdontoRedes',
                        'OdontoDependentes',
                        'OdontoFormasPagamentos',
                        "OdontoAtendimentos",
                        'OdontoAreasComercializacoes' => [
                            'Municipios' => ['Estados'],
                            'OdontoOperadoras' => ['Imagens']
                        ]
                    ])
                    ->where(["OdontoTabelas.id IN" => $dados['Produtos']])
                    ->order([
                        "OdontoTabelas.prioridade" => "ASC",
                        "OdontoTabelas.nome" => "ASC",
                    ])
                    ->toArray();
                $tabelas_ordenadas = [];
                $area_adicionada = [];
                foreach ($tabelas as $tabela) {
                    $operadora_id = $tabela['odonto_areas_comercializaco']['odonto_operadora']['id'];
                    @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['odonto_areas_comercializaco']['odonto_operadora'];
                    @$tabelas_ordenadas[$operadora_id]['atendimentos'][$tabela['odonto_atendimento']['nome']] .=  $tabela['nome'] . ' • ';

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    if (!empty($tabela['odonto_areas_comercializaco']['metropoles']) || !empty($tabela['odonto_areas_comercializaco']['municipios'])) {
                        @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        if (!in_array($tabela['odonto_areas_comercializaco']['id'], $area_adicionada)) {
                            foreach ($tabela['odonto_areas_comercializaco']['metropoles'] as $metropole) {
                                @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $metropole['nome'] . ', ';
                            }
                            foreach ($tabela['odonto_areas_comercializaco']['municipios'] as $municipio) {
                                @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['odonto_areas_comercializaco']['id']]['data'] .= $municipio['nome'] . ', ';
                            }
                            $area_adicionada[] = $tabela['odonto_areas_comercializaco']['id'];
                        }
                    } else {
                        @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'] = '';
                    }
                    unset($tabela['odonto_areas_comercializaco']);

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    if (!empty($tabela['odonto_observacao']['id'])) {
                        @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observacao']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['odonto_observacao']['id']]['data'] =  nl2br($tabela['odonto_observacao']['descricao']);
                        unset($tabela['odonto_observacao']);
                    }

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    if (!empty($tabela['odonto_rede']['id'])) {
                        @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['odonto_rede']['id']]['data'] =  nl2br($tabela['odonto_rede']['descricao']);
                        unset($tabela['odonto_rede']);
                    }
                    if (!empty($tabela['reembolsoEntity']['id'])) {
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  nl2br($tabela['reembolsoEntity']['descricao']);
                        unset($tabela['reembolsoEntity']);
                    }
                    if (!empty($tabela['odonto_carencia']['id'])) {
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['odonto_carencia']['id']]['data'] =  nl2br($tabela['odonto_carencia']['descricao']);
                        unset($tabela['odonto_carencia']);
                    }
                    if (!empty($tabela['odonto_dependente']['id'])) {
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['odonto_dependente']['id']]['data'] =  nl2br($tabela['odonto_dependente']['descricao']);
                        unset($tabela['odonto_dependente']);
                    }
                    if (!empty($tabela['odonto_documento']['id'])) {
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['odonto_documento']['id']]['data'] =  nl2br($tabela['odonto_documento']['descricao']);
                        unset($tabela['odonto_documento']);
                    }
                    if (!empty($tabela['odonto_formas_pagamento']['id'])) {
                        // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                        @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                        @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['odonto_formas_pagamento']['id']]['data'] =  nl2br($tabela['odonto_formas_pagamento']['descricao']);
                        unset($tabela['odonto_formas_pagamento']);
                    }

                    $tabela['cobertura'] = $tabela->cobertura;
                    // $tabela['acomodacao'] = $tabela->acomodacao;

                    @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
                }
                $tabelas_ordenadas[$operadora_id]['tabelas'] = array_chunk($tabelas_ordenadas[$operadora_id]['tabelas'], 5, true);
                $tabelasOrdenadas = $tabelas_ordenadas;

                $this->loadModel('OdontoOperadoras');
                $operadora = $this->OdontoOperadoras->get($dados['operadora_id'], ['contain' => ['Imagens']]);

                break;
        }
        $vidas = isset($vidas) ? $vidas : null;
        $entidades = isset($entidades) ? $entidades : null;
        $totalTabelas = isset($totalTabelas) ? $totalTabelas : null;
        $dadosOperadoras = isset($dadosOperadoras) ? $dadosOperadoras : null;

        $this->set(compact('vidas', 'entidades', 'operadora', 'sessao', "totalTabelas", "tabelasOrdenadas", "dadosOperadoras", "tabelas", "dados"));

        //VIEW DE ACORDO COM O RAMO SELECIONADO
        switch ($dados["ramo_id"]) {
            case "OPF":
                $this->render("odonto_tabela");
                break;
            case "OPJ":
                $this->render("odonto_tabela");
                break;
            default:
                $this->render("new_tabela");
                break;
        }
    }

    /*     * *********************************** GERAÇÃO DE TABELAS PERSONALIZADAS ************************************* */

    /*

      Método para gerar PDF com tabela personalizada.

     */

    public function pdf($tipo = 'I')
    {
        $dados = $this->request->data();
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        if (isset($dados["vidas"])) {
            $vidas = explode("-", $dados["vidas"]);
        }

        foreach ($dados["Produtos"] as $id => $produto) {
            if ($produto == '0') {
                unset($dados["Produtos"][$id]);
            }
        }

        //RAMO SELECIONADO
        switch ($dados["ramo_id"]) {
            case "SPF":
                $this->loadModel("PfTabelas");

                if (empty($dados["coparticipacao"])) {
                    $coparticipacao = "";
                } else {
                    $coparticipacao = array("PfTabelas.coparticipacao" => $dados["coparticipacao"]);
                }
                $tabelas = $this->PfTabelas
                    ->find("all")
                    ->contain([
                        'PfAcomodacoes',
                        'PfProdutos' => ["TiposProdutos"],
                        "PfAtendimentos",
                        "PfOperadoras"
                    ])
                    ->where([
                        "PfTabelas.estado_id" => $dados["estado_id"],
                        "PfTabelas.pf_operadora_id" => $dados["operadora_id"],
                        "PfTabelas.pf_produto_id IN " => $dados["Produtos"],
                        "PfTabelas.validade" => 1,
                        $coparticipacao
                    ])
                    ->order([
                        "PfProdutos.nome" => "ASC",
                        "PfTabelas.prioridade" => "ASC"
                    ])->toArray();

                //FIND ENTIDADES DA OPERADORA
                $this->loadModel("PfEntidadesPfOperadoras");
                $entidades = $this->PfEntidadesPfOperadoras->find("all")
                    ->contain(["PfEntidades" => ["PfEntidadesProfissoes" => ["PfProfissoes"]]])
                    ->where(["pf_operadora_id" => $dados["operadora_id"]])->toArray();

                //DADOS DA OPERADORA
                $this->loadModel("PfOperadoras");
                $operadora = $this->PfOperadoras
                    ->find("all")
                    ->contain([
                        'Imagens',
                        'PfCarencias',
                        'PfComercializacoes',
                        'PfObservacoes',
                        'PfDocumentos',
                        'PfReembolsos',
                        'PfRedes',
                        'Estados',
                        'PfDependentes',
                        'PfFormasPagamentos'
                    ])
                    ->where(["PfOperadoras.id" => $dados["operadora_id"]])
                    ->first();
                break;
            case "SPJ":
                if ($dados["coparticipacao"] == 99) {
                    $coparticipacao = "";
                } else {
                    $coparticipacao = array("Tabelas.coparticipacao" => $dados["coparticipacao"]);
                }
                $this->loadModel("Tabelas");
                $tabelas = $this->Tabelas
                    ->find("all")
                    ->contain([
                        'Tipos',
                        'Produtos' => ["TiposProdutos"],
                        "TabelasCnpjs" => ["Cnpjs"],
                        "Abrangencias",
                        "Operadoras"
                    ])
                    ->where([
                        "Tabelas.estado_id" => $dados["estado_id"],
                        "Tabelas.operadora_id" => $dados["operadora_id"],
                        "Tabelas.produto_id IN " => $dados["Produtos"],
                        "Tabelas.minimo_vidas <=" => $vidas[0],
                        "Tabelas.maximo_vidas >=" => $vidas[1],
                        "Tabelas.validade" => 1,
                        $coparticipacao,
                    ])
                    ->order([
                        "Produtos.nome" => "ASC",
                        "Tabelas.prioridade" => "ASC"
                    ])
                    ->toArray();

                //EXCLUSÃO DE TABELAS 3 A 29 CASO SEJA SELECIONADO 4 A 29
                $bradesco = array(1, 28, 29, 30, 31);
                foreach ($tabelas as $chave => $tabela) {
                    if ($vidas[0] == 4 && $vidas[1] == 29 && in_array($tabela["operadora_id"], $bradesco)) {
                        if ($tabela["minimo_vidas"] == 3) {
                            unset($tabelas[$chave]);
                        }
                    }
                }

                //DADOS DA OPERADORA
                $this->loadModel("Operadoras");
                $operadora = $this->Operadoras
                    ->find("all")
                    ->contain([
                        'Imagens',
                        'Carencias',
                        'Regioes',
                        'Observacoes',
                        'Informacoes',
                        'Reembolsos',
                        'Redes',
                        'Estados',
                        'Opcionais',
                        'FormasPagamentos'
                    ])
                    ->where(["Operadoras.id" => $dados["operadora_id"]])
                    ->first();
                break;

            case "OPF":
                $this->loadModel("OdontoTabelas");
                $tabelas = $this->OdontoTabelas
                    ->find("all")
                    ->contain([
                        'OdontoProdutos' => ["OdontoOperadoras" => [
                            'Imagens',
                            'OdontoCarencias',
                            'OdontoComercializacoes',
                            'OdontoObservacaos',
                            'OdontoDocumentos',
                            'OdontoReembolsos',
                            'OdontoRedes',
                            'Estados',
                            'OdontoDependentes',
                            'OdontoFormasPagamentos'
                        ]],
                        "OdontoAtendimentos"
                    ])
                    ->where([
                        "OdontoTabelas.estado_id" => $dados["estado_id"],
                        "OdontoProdutos.odonto_operadora_id" => $dados["operadora_id"],
                        "OdontoTabelas.odonto_produto_id IN " => $dados["Produtos"],
                        "OdontoTabelas.validade" => 1,
                        "OdontoTabelas.tipo_pessoa" => "PF"
                    ])
                    ->order([
                        "OdontoProdutos.nome" => "ASC",
                        "OdontoTabelas.prioridade" => "ASC"
                    ])
                    ->toArray();

                //DADOS DA OPERADORA
                $this->loadModel("OdontoOperadoras");
                $operadora = $this->OdontoOperadoras
                    ->find("all")
                    ->contain([
                        'Imagens',
                        'OdontoCarencias',
                        'OdontoComercializacoes',
                        'OdontoObservacaos',
                        'OdontoDocumentos',
                        'OdontoReembolsos',
                        'OdontoRedes',
                        'Estados',
                        'OdontoDependentes',
                        'OdontoFormasPagamentos'
                    ])
                    ->where(["OdontoOperadoras.id" => $dados["operadora_id"]])
                    ->first();

                break;
            case "OPJ":
                $this->loadModel("OdontoTabelas");
                $tabelas = $this->OdontoTabelas
                    ->find("all")
                    ->contain([
                        'OdontoProdutos' => ["OdontoOperadoras" => [
                            'Imagens',
                            'OdontoCarencias',
                            'OdontoComercializacoes',
                            'OdontoObservacaos',
                            'OdontoDocumentos',
                            'OdontoReembolsos',
                            'OdontoRedes',
                            'Estados',
                            'OdontoDependentes',
                            'OdontoFormasPagamentos'
                        ]],
                        "OdontoAtendimentos"
                    ])
                    ->where([
                        "OdontoTabelas.estado_id" => $dados["estado_id"],
                        "OdontoProdutos.odonto_operadora_id" => $dados["operadora_id"],
                        "OdontoTabelas.odonto_produto_id IN " => $dados["Produtos"],
                        "OdontoTabelas.minimo_vidas <=" => $vidas[0],
                        "OdontoTabelas.maximo_vidas >=" => $vidas[1],
                        "OdontoTabelas.validade" => 1,
                        "OdontoTabelas.tipo_pessoa" => "PJ"
                    ])
                    ->order([
                        "OdontoProdutos.nome" => "ASC",
                        "OdontoTabelas.prioridade" => "ASC"
                    ])
                    ->toArray();
                //DADOS DA OPERADORA
                $this->loadModel("OdontoOperadoras");
                $operadora = $this->OdontoOperadoras
                    ->find("all")
                    ->contain([
                        'Imagens',
                        'OdontoCarencias',
                        'OdontoComercializacoes',
                        'OdontoObservacaos',
                        'OdontoDocumentos',
                        'OdontoReembolsos',
                        'OdontoRedes',
                        'Estados',
                        'OdontoDependentes',
                        'OdontoFormasPagamentos'
                    ])
                    ->where(["OdontoOperadoras.id" => $dados["operadora_id"]])
                    ->first();
                break;
        }

        $totalTabelas = count($tabelas);

        //Divisão do ARRAY por 5 tabelas no máximo
        $tabelasDivididas = array_chunk($tabelas, 5, true);

        $ramo_nome_arquivo = '';
        foreach ($tabelasDivididas as $chave => $tabelasD) {
            foreach ($tabelasD as $tabelaF) {
                switch ($tabelaF["coparticipacao"]) {
                    case "s":
                        $co = "COPARTICIPAÇÃO ";
                        break;
                    case "n":
                        $co = "SEM COPARTICIPAÇÃO";
                        break;
                    default:
                        $co = "SEM COPARTICIPAÇÃO";
                        break;
                }
                //ORDENACAO DO VETOR DE ACORDO COM O RAMO SELECIONADO
                switch ($dados["ramo_id"]) {
                    case "SPF":
                        $ramo_nome_arquivo = 'Saude_Pessoa_Física';
                        $tabelasOrdenadas[$chave][$tabelaF["pf_produto"]["nome"]][$tabelaF["pf_produto"]["tipos_produto"]["nome"]][$tabelaF["pf_acomodaco"]["nome"]][$tabelaF["id"] . "-" . $co . $tabelaF["detalhe_coparticipacao"]] = $tabelaF;
                        break;
                    case "SPJ":
                        $ramo_nome_arquivo = 'Saude_Pessoa_Jurídica';
                        $tabelasOrdenadas[$chave][$tabelaF["produto"]["nome"]][$tabelaF["produto"]["tipos_produto"]["nome"]][$tabelaF["tipo"]["nome"]][$tabelaF["id"] . "-" . $co . $tabelaF["detalhe_coparticipacao"]] = $tabelaF;
                        break;
                        /*
                      case "SPE":
                      //     					$tabelasOrdenadas[$chave][$tabela["produto"]["nome"]][$tabela["tipo"]["nome"]][$tabela["id"]."-".$co.$tabela["detalhe_coparticipacao"]] = $tabela;
                      break;
                     */
                    case "OPF":
                        $ramo_nome_arquivo = 'Odonto_Pessoa_Física';
                        $tabelasOrdenadas[$chave][$tabelaF["odonto_produto"]["nome"]][$tabelaF["descricao"]][$tabelaF["id"] . "-" . $co . $tabelaF["detalhe_coparticipacao"]] = $tabelaF;
                        break;
                    case "OPJ":
                        $ramo_nome_arquivo = 'Odonto_Pessoa_Jurídica';
                        $tabelasOrdenadas[$chave][$tabelaF["odonto_produto"]["nome"]][$tabelaF["tipo_pessoa"]][$tabelaF["id"] . "-" . $co . $tabelaF["detalhe_coparticipacao"]] = $tabelaF;
                        break;
                        /*
                      case "OPE":
                      //     					$tabelasOrdenadas[$chave][$tabela["produto"]["nome"]][$tabela["tipo"]["nome"]][$tabela["id"]."-".$co.$tabela["detalhe_coparticipacao"]] = $tabela;
                      break;
                     */
                }
            }
        }



        //Tipo de PDF: F -> Salva no servidor; D-> Download via Browser
        $tipoPDF = $dados["tipo"];
        //Nome do PDF

        if (isset($dados["vidas"]) && $dados["vidas"] != '') {
            $info_vidas = $dados["vidas"] . " vidas";
        } else {
            $info_vidas = '';
        }

        if (isset($sessao) && $sessao != null) {
            //SALVAR ESTATISTICA DE GERAÇÃO DE PDFs
            $tabelasGerada["ramo"] = $dados["ramo_id"];
            $tabelasGerada["operadora"] = $dados["operadora_id"];
            $tabelasGerada["user_id"] = $sessao["id"];
            $tabelasGerada["estado_id"] = $dados["estado_id"];
            $tabelasGerada["pdf"] = $dados["tipo"];
            if (isset($dados["vidas"])) {
                $tabelasGerada["vidas"] = $dados["vidas"];
            } else {
                $tabelasGerada["vidas"] = "N/A";
            }
            $tabelasGerada["nome"] = $operadora["estado"]["nome"] . "-" . $ramo_nome_arquivo . "-" . $operadora["nome"] . "-" . $operadora["detalhe"] . "-" . $tabelasGerada["vidas"] . '.pdf';
            $tabelasGerada["coparticipacao"] = $dados["coparticipacao"];

            $tabelasGeradaEnt = $this->TabelasGeradas->newEntity();
            $tabelasGeradaEnt = $this->TabelasGeradas->patchEntity($tabelasGeradaEnt, $tabelasGerada);
            $this->TabelasGeradas->save($tabelasGeradaEnt);
        }


        if ($dados["tipo"] <> "F") {
            switch ($dados["coparticipacao"]) {
                case 's':
                    $coNome = "COM_COPARTICIPACAO";
                    break;
                case 'n':
                    $coNome = "SEM_COPARTICIPACAO";
                    // no break
                case '99':
                    $coNome = "";
                    break;
                default:
                    $coNome = "";
                    break;
            }

            $nomePDF = $operadora["nome"] . "-" . $operadora["detalhe"]  . "-" . $info_vidas . "-" . $ramo_nome_arquivo . "-" . $operadora["estado"]["nome"] . '.pdf';

            $nomePDF = transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove', $nomePDF);


            $this->set(compact('vidas', 'entidades', 'operadora', 'tabelas', "dados", 'totalTabelas', 'operadoras', "tabelasOrdenadas", "dadosOperadoras", 'sessao', 'nomePDF', 'tipoPDF'));
            $this->viewBuilder()->layout('gerador_tabela_pdf');

            if ($dados["ramo_id"] == "OPF" || $dados["ramo_id"] == "OPJ") {
                $this->render("odonto_pdf");
            }
        } else {
            $nomePDF = $_SERVER['DOCUMENT_ROOT'] . transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove', $operadora["estado"]["nome"] . "-" . $ramo_nome_arquivo . "-" . $operadora["nome"] . "-" . $operadora["detalhe"] . "-" . $info_vidas . '.pdf');

            $apelidoPDF = transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove', $operadora["estado"]["nome"] . "-" . $ramo_nome_arquivo . "-" . $operadora["nome"] . "-" . $operadora["detalhe"] . "-" . $info_vidas . '.pdf');



            if (isset($dados["email-cliente"]) && $dados["email-cliente"] <> '') {
                $emaildestino = $dados["email-cliente"];
            } else {
                $emaildestino = $sessao["email"];
            }

            $this->set(compact('vidas', 'entidades', 'apelidoPDF', 'emaildestino', 'operadora', 'tabelas', "dados", 'totalTabelas', 'operadoras', "tabelasOrdenadas", "dadosOperadoras", 'sessao', 'nomePDF', 'tipoPDF'));


            $this->viewBuilder()->layout('gerador_tabela_pdf');
            if ($dados["ramo_id"] == "OPF" || $dados["ramo_id"] == "OPJ") {
                $this->render("odonto_pdf");
            }
        }
    }

    public function email()
    {
        $email_destino = $this->request->query('email');
        $arquivo = $this->request->query('pdf');
        $apelido = $this->request->query('apelido');

        /*
          $string = $this->request->here;
          $prefix = "email/";
          $index = strpos($string, $prefix) + strlen($prefix);
          $arquivo = "/".substr($string, $index);
          // 		$arquivo = str_replace("%", " ", $arquivo);

          $prefixo = "htdocs/";
          $indice = strpos($arquivo, $prefixo) + strlen($prefixo);
          $nomePDF = substr($arquivo, $indice);

          $session = $this->request->session();
          $sessao = $session->read('Auth.User');
         */

        //            debug($sessao);die();

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $imagem = rawurlencode($session->read('Auth.User.imagem.nome'));
        $caminho = "uploads/imagens/usuarios/logos/";
        $imagem = $caminho . $imagem;
        if (empty($imagem)) {
            $imagem = 'img/logoCpEmail.png';
        }
        $email = new Email('default');
        //        $email = new Email();
        $email->transport('calculos');
        $email->from('nao-responda@corretorparceiro.com.br', 'Tabela de Preços - Cálculos')
            ->to($email_destino)
            ->sender('nao-responda@corretorparceiro.com.br', 'Tabela de Preços - Cálculos')
            ->emailFormat('html')
            ->attachments([
                $apelido => [
                    'file' => $arquivo
                ]
            ])
            ->subject('Tabela Gerada - Ferramentas de Apoio Corretor Parceiro')
            ->send('<html><img src="https://corretorparceiro.com.br/app/' . $imagem . '" width="230"/>'
                . '<br/>'
                . '<br/>'
                . 'Atenciosamente, ' . $sessao['nome'] . ' ' . $sessao['sobrenome']
                . '<br/> <b>Email:</b> ' . $sessao['email']
                . '<br/> <b>Celular:</b> ' . $sessao['celular']
                . '<br/> <b>WhatsApp:</b> ' . $sessao['whatsapp']
                . '</html>');



        unlink($arquivo);
        $this->Flash->error(__('Tabela enviada para o e-mail: ' . $email_destino));
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        if (isset($sessao) && $sessao != null) {
            $action = "gerador";
        } else {
            $action = "geradorAuto";
        }
        return $this->redirect(['action' => $action]);
        //            }
    }

    public function atualizarTabelas()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        //LISTAGEM DE TABELAS JÁ SALVAS PELO USUÁRIO
        $tabelas_geradas = $this->TabelasGeradas->find("list", ['valueField' => 'nome', 'conditions' => '1 = 1', 'order' => ['nome' => 'ASC']])->where([
            "user_id" => $sessao["id"],
            "pdf IS" => null,
            "compartilhada <>" => "S"
        ]);

        $this->set(compact('tabelas_geradas'));
    }
    public function tabelaSalva()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $id = $this->request->data();
            $id = $id['tabela_gerada_id'];
            $tabelaSalva = $this->TabelasGeradas->find('all')->where(['id' => $id])->toArray()[0];
            switch ($tabelaSalva["ramo"]) {
                case "SPF":
                    $model = "PfProdutos";
                    $campoOperadora = "pf_operadora_id";
                    break;
                case "SPJ":
                    $model = "Produtos";
                    $campoOperadora = "operadora_id";
                    break;
                case "SPE":
                    $model = "";
                    break;
                case "OPF":
                    $model = "OdontoProdutos";
                    $campoOperadora = "odonto_operadora_id";
                    break;
                case "OPJ":
                    $model = "OdontoProdutos";
                    $campoOperadora = "odonto_operadora_id";
                    break;
                case "OPE":
                    $model = "";
                    break;
            }
            $this->loadModel($model);
            $produtos = $this->$model->find('list', ['valueField' => 'nome', 'conditions' => '1 = 1'])
                ->where([
                    $campoOperadora => $tabelaSalva["operadora"]
                ])->toArray();
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $this->set(compact('tabelaSalva', 'produtoSalvo', 'produtos', "sessao"));
            $this->render('gerador_produtos');
        }
    }
    public function produtoSalvo($id = null)
    {
        $produtoSalvo = $this->TabelasGeradas->TabelasGeradasProdutos->find('list', ['keyField' => 'produto_id', 'valueField' => 'produto_id', 'conditions' => '1 = 1'])->where(['tabela_gerada_id' => $id])->toArray();
        $produtoSalvo = json_encode($produtoSalvo);
        $this->response->type('json');
        $this->response->body($produtoSalvo);
        return $this->response;
    }

    public function tabelaWhats()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $dados = $this->request->data();
        if (isset($dados['vidas'])) {
            $tabela = $this->TabelasGeradas->find('list', ['limit' => 1])->where(['user_id' => $sessao['id'], 'estado_id' => $dados['estado_id'], 'ramo' => $dados['ramo_id'], 'operadora' => $dados['operadora_id'], 'vidas' => $dados['vidas'], 'coparticipacao' => $dados['coparticipacao']])->order([
                "id" => "DESC"
            ])->toArray();
        } else {
            $tabela = $this->TabelasGeradas->find('list', ['limit' => 1])->where(['user_id' => $sessao['id'], 'estado_id' => $dados['estado_id'], 'ramo' => $dados['ramo_id'], 'operadora' => $dados['operadora_id'], 'coparticipacao' => $dados['coparticipacao']])->order([
                "id" => "DESC"
            ])->toArray();
        }
        foreach ($tabela as $tabela) {
            $id['key'] = $tabela;
        }
        $id = json_encode($id);

        $this->response->type('json');
        $this->response->body($id);
        return $this->response;
    }
    public function shared($id = null)
    {
        try {
            $dados = $this->TabelasGeradas->get($id, ['contain' => ['Users' => ['Imagens']], 'conditions' => ['compartilhada' => 'S']])->toArray();
        } catch (\Exception $e) {
            $this->viewBuilder()->layout('link_tela');
            $this->render('tabela_expirada');
            return;
        }
        $dados['Produtos'] = $this->TabelasGeradas->TabelasGeradasProdutos->find('list', ['valueField' => 'produto_id'])->where(['tabela_gerada_id' => $id])->toArray();
        $dados['ramo_id'] = $dados['ramo'];
        $dados['operadora_id'] = $dados['operadora'];
        $pdf = $dados['pdf'];

        if (isset($dados["vidas"])) {
            $vidas = explode("-", $dados["vidas"]);
        }
        foreach ($dados["Produtos"] as $id => $produto) {
            if ($produto == '0') {
                unset($dados["Produtos"][$id]);
            }
        }

        //RAMO SELECIONADO
        switch ($dados["ramo_id"]) {
            case "SPF":
                $this->loadModel("PfTabelas");
                $tabelas = $this->PfTabelas->find('all')
                    ->contain([
                        'PfAtendimentos',
                        'PfAcomodacoes',
                        'PfCoberturas',
                        'PfFormasPagamentos',
                        'PfCarencias',
                        'PfRedes',
                        'PfReembolsos',
                        'PfDocumentos',
                        'PfDependentes',
                        'PfComercializacoes',
                        'PfObservacoes',
                        'PfEntidades' => [
                            'PfProfissoes' =>  function ($q) {
                                return $q->order('nome');
                            }
                        ],
                        'PfAreasComercializacoes' => [
                            'Municipios' => ['Estados'],
                            'PfOperadoras' => ['Imagens']
                        ]
                    ])
                    ->where(['PfTabelas.id IN' => $dados["Produtos"]])
                    ->toArray();
                $operadorasIds = array();
                $tabelas_ordenadas = [];
                foreach ($tabelas as $tabela) {
                    $operadora_id = $tabela['pf_areas_comercializaco']['pf_operadora']['id'];
                    @$tabelas_ordenadas[$operadora_id]['operadora'] =  $tabela['pf_areas_comercializaco']['pf_operadora'];

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['data'] =  $tabela['pf_areas_comercializaco']['nome'] . '* Vide Anexo.';
                    @$tabelas_ordenadas[$operadora_id]['areas_comercializacoes'][$tabela['pf_areas_comercializaco']['id']]['municipios'] =  $tabela['pf_areas_comercializaco']['municipios'];
                    unset($tabela['pf_areas_comercializaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['observacoes'][$tabela['pf_observaco']['id']]['data'] =  $tabela['pf_observaco']['descricao'];
                    unset($tabela['pf_observaco']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['redes'][$tabela['pf_rede']['id']]['data'] =  $tabela['pf_rede']['descricao'];
                    unset($tabela['pf_rede']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['reembolsos'][$tabela['reembolsoEntity']['id']]['data'] =  $tabela['reembolsoEntity']['descricao'];
                    unset($tabela['reembolsoEntity']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['carencias'][$tabela['pf_carencia']['id']]['data'] =  $tabela['pf_carencia']['descricao'];
                    unset($tabela['pf_carencia']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['dependentes'][$tabela['pf_dependente']['id']]['data'] =  $tabela['pf_dependente']['descricao'];
                    unset($tabela['pf_dependente']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['documentos'][$tabela['pf_documento']['id']]['data'] =  $tabela['pf_documento']['descricao'];
                    unset($tabela['pf_documento']);
                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['tabelas'] .=  $tabela['nome'] . ' • ';
                    @$tabelas_ordenadas[$operadora_id]['pagamentos'][$tabela['pf_formas_pagamento']['id']]['data'] =  $tabela['pf_formas_pagamento']['descricao'];
                    unset($tabela['pf_formas_pagamento']);

                    // NÃO REPETIÇÃO DAS OBSERVAÇOES E AS TABELAS QUE AS POSSUEM
                    @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['nome'] =  $tabela['nome'];
                    foreach ($tabela['pf_entidades'] as $entidade) {
                        @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .=  '<strong>' . $entidade['nome'] . '</strong>: ';
                        foreach ($entidade['pf_profissoes'] as $profissao) {
                            @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .= $profissao['nome'] . ', ';
                        }
                        @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] = substr($tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'], 0, -2);
                        @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .= '. ';
                    }
                    @$tabelas_ordenadas[$operadora_id]['entidades'][$tabela['id']]['data'] .= '</br>';
                    // unset($tabela['pf_entidade']);

                    $tabela['cobertura'] = $tabela->pf_cobertura;
                    $tabela['acomodacao'] = $tabela->pf_acomodaco;

                    @$tabelas_ordenadas[$operadora_id]['tabelas'][] = $tabela;
                }

                $tabelas_ordenadas[$operadora_id]['tabelas'] = array_chunk($tabelas_ordenadas[$operadora_id]['tabelas'], 5, true);
                $tabelasOrdenadas = $tabelas_ordenadas;
                $this->loadModel('PfOperadoras');
                $operadora = $this->PfOperadoras->get($dados['operadora_id']);
                $this->set(compact('vidas', 'entidades', 'operadora', 'sessao', "totalTabelas", "tabelasOrdenadas", "dadosOperadoras", "tabelas", "dados"));
                $this->viewBuilder()->layout('link_tela');
                $this->render("shared");
                return;
                break;
            case "SPJ":
                if ($dados["coparticipacao"] == 99) {
                    $coparticipacao = "";
                } else {
                    $coparticipacao = array("Tabelas.coparticipacao" => $dados["coparticipacao"]);
                }
                $this->loadModel("Tabelas");
                $tabelas = $this->Tabelas
                    ->find("all")
                    ->contain([
                        'Tipos',
                        "TiposProdutos",
                        'Produtos',
                        "TabelasCnpjs" => ["Cnpjs"],
                        "Abrangencias",
                        "Operadoras"
                    ])
                    ->where([
                        "Tabelas.estado_id" => $dados["estado_id"],
                        "Tabelas.operadora_id" => $dados["operadora_id"],
                        "Tabelas.produto_id IN " => $dados["Produtos"],
                        "Tabelas.minimo_vidas" => $vidas[0],
                        "Tabelas.maximo_vidas" => $vidas[1],
                        "Tabelas.validade" => 1,
                        $coparticipacao,
                    ])
                    ->order([
                        "Tabelas.prioridade" => "ASC",
                        "Produtos.nome" => "ASC"
                    ])->toArray();

                $bradesco = array(1, 28, 29, 30, 31);
                foreach ($tabelas as $chave => $tabela) {
                    if ($vidas[0] == 4 && $vidas[1] == 29 && in_array($tabela["operadora_id"], $bradesco)) {
                        if ($tabela["minimo_vidas"] == 3) {
                            unset($tabelas[$chave]);
                        }
                    }
                }

                //DADOS DA OPERADORA
                $this->loadModel("Operadoras");
                $operadora = $this->Operadoras
                    ->find("all")
                    ->contain([
                        'Imagens',
                        'Carencias',
                        'Regioes',
                        'Observacoes',
                        'Informacoes',
                        'Reembolsos',
                        'Redes',
                        'Estados',
                        'Opcionais',
                        'FormasPagamentos'
                    ])
                    ->where(["Operadoras.id" => $dados["operadora_id"]])
                    ->first();
                break;

            case "OPF":
                $this->loadModel("OdontoTabelas");
                $tabelas = $this->OdontoTabelas
                    ->find("all")
                    ->contain([
                        'OdontoProdutos' => ["OdontoOperadoras" => [
                            'Imagens',
                            'OdontoCarencias',
                            'OdontoComercializacoes',
                            'OdontoObservacaos',
                            'OdontoDocumentos',
                            'OdontoReembolsos',
                            'OdontoRedes',
                            'Estados',
                            'OdontoDependentes',
                            'OdontoFormasPagamentos'
                        ]],
                        "OdontoAtendimentos"
                    ])
                    ->where([
                        "OdontoTabelas.estado_id" => $dados["estado_id"],
                        "OdontoProdutos.odonto_operadora_id" => $dados["operadora_id"],
                        "OdontoTabelas.odonto_produto_id IN " => $dados["Produtos"],
                        "OdontoTabelas.validade " => 1,
                        /*
                                  "OdontoTabelas.minimo_vidas <=" => $vidas[0],
                                  "OdontoTabelas.maximo_vidas >=" => $vidas[1],
                                 */
                        "OdontoTabelas.tipo_pessoa" => "PF"
                    ])
                    ->order([
                        "OdontoTabelas.prioridade" => "ASC",
                        "OdontoProdutos.nome" => "ASC"
                    ])
                    ->toArray();

                //DADOS DA OPERADORA
                $this->loadModel("OdontoOperadoras");
                $operadora = $this->OdontoOperadoras
                    ->find("all")
                    ->contain([
                        'Imagens',
                        'OdontoCarencias',
                        'OdontoComercializacoes',
                        'OdontoObservacaos',
                        'OdontoDocumentos',
                        'OdontoReembolsos',
                        'OdontoRedes',
                        'Estados',
                        'OdontoDependentes',
                        'OdontoFormasPagamentos'
                    ])
                    ->where(["OdontoOperadoras.id" => $dados["operadora_id"]])
                    ->first();

                break;
            case "OPJ":
                $this->loadModel("OdontoTabelas");
                $tabelas = $this->OdontoTabelas
                    ->find("all")
                    ->contain([
                        'OdontoProdutos' => ["OdontoOperadoras" => [
                            'Imagens',
                            'OdontoCarencias',
                            'OdontoComercializacoes',
                            'OdontoObservacaos',
                            'OdontoDocumentos',
                            'OdontoReembolsos',
                            'OdontoRedes',
                            'Estados',
                            'OdontoDependentes',
                            'OdontoFormasPagamentos'
                        ]],
                        "OdontoAtendimentos"
                    ])
                    ->where([
                        "OdontoTabelas.estado_id" => $dados["estado_id"],
                        "OdontoProdutos.odonto_operadora_id" => $dados["operadora_id"],
                        "OdontoTabelas.odonto_produto_id IN " => $dados["Produtos"],
                        "OdontoTabelas.minimo_vidas <=" => $vidas[0],
                        "OdontoTabelas.maximo_vidas >=" => $vidas[1],
                        "OdontoTabelas.validade" => 1,
                        "OdontoTabelas.tipo_pessoa" => "PJ"
                    ])
                    ->order([
                        "OdontoTabelas.prioridade" => "ASC",
                        "OdontoProdutos.nome" => "ASC"
                    ])
                    ->toArray();
                //DADOS DA OPERADORA
                $this->loadModel("OdontoOperadoras");
                $operadora = $this->OdontoOperadoras
                    ->find("all")
                    ->contain([
                        'Imagens',
                        'OdontoCarencias',
                        'OdontoComercializacoes',
                        'OdontoObservacaos',
                        'OdontoDocumentos',
                        'OdontoReembolsos',
                        'OdontoRedes',
                        'Estados',
                        'OdontoDependentes',
                        'OdontoFormasPagamentos'
                    ])
                    ->where(["OdontoOperadoras.id" => $dados["operadora_id"]])
                    ->first();
                break;
        }

        $totalTabelas = count($tabelas);
        //Divisão do ARRAY por 8 tabelas no máximo
        $tabelasDivididas = array_chunk($tabelas, 8, true);

        foreach ($tabelasDivididas as $chave => $tabelasD) {
            foreach ($tabelasD as $tabela) {
                switch ($tabela["coparticipacao"]) {
                    case "s":
                        $co = "COPARTICIPAÇÃO ";
                        break;
                    case "n":
                        $co = "SEM COPARTICIPAÇÃO";
                        break;
                    default:
                        $co = "SEM COPARTICIPAÇÃO";
                        break;
                }
                //ORDENACAO DO VETOR DE ACORDO COM O RAMO SELECIONADO
                switch ($dados["ramo_id"]) {
                    case "SPF":
                        $tabelasOrdenadas[$chave][$tabela["pf_produto"]["nome"]][$tabela["tipos_produto"]["nome"]][$tabela["pf_acomodaco"]["nome"]][$tabela["id"] . "-" . $co . $tabela["detalhe_coparticipacao"]] = $tabela;
                        break;
                    case "SPJ":
                        $tabelasOrdenadas[$chave][$tabela["produto"]["nome"]][$tabela["tipos_produto"]["nome"]][$tabela["tipo"]["nome"]][$tabela["id"] . "-" . $co . $tabela["detalhe_coparticipacao"]] = $tabela;
                        break;

                    case "OPF":
                        $tabelasOrdenadas[$chave][$tabela["odonto_produto"]["nome"]][$tabela["descricao"]][$tabela["id"] . "-" . $co . $tabela["detalhe_coparticipacao"]] = $tabela;
                        break;
                    case "OPJ":
                        $tabelasOrdenadas[$chave][$tabela["odonto_produto"]["nome"]][$tabela["tipo_pessoa"]][$tabela["id"] . "-" . $co . $tabela["detalhe_coparticipacao"]] = $tabela;
                        break;
                }
            }
        }

        $this->set(compact('vidas', 'entidades', 'operadora', 'sessao', "totalTabelas", "tabelasOrdenadas", "dadosOperadoras", "tabelas", "dados"));
        $this->viewBuilder()->layout('link_tela');

        //VIEW DE ACORDO COM O RAMO SELECIONADO
        switch ($dados["ramo_id"]) {
            case "OPF":
                $this->render("shared_odonto");
                break;
            case "OPJ":
                $this->render("shared_odonto");
                break;
            default:
                $this->render("shared");
                break;
        }
    }
    public function optionsSalvas($id = NULL)
    {
        $tabelaSalva = $this->TabelasGeradas->get($id)->toArray();
        $estado = $tabelaSalva['estado_id'];
        $profissao = $tabelaSalva['profissao_id'];
        $ramo = $tabelaSalva['ramo'];
        $profissao = $tabelaSalva['profissao_id'];
        $operadora = $tabelaSalva['operadora'];
        //FIND-RAMOS
        $this->loadModel("Tabelas");
        $spj = $this->Tabelas
            ->find('list')
            ->where(['validade' => 1, 'estado_id' => $estado])
            ->toArray();

        //TABELAS PF
        $this->loadModel("PfTabelas");
        $spf = $this->PfTabelas
            ->find('all')
            ->where(['validade' => 1, 'estado_id' => $estado])
            ->toArray();

        //TABELAS ODONTO PF
        $this->loadModel("OdontoTabelas");
        $opf = $this->OdontoTabelas
            ->find('all')
            ->where(['validade' => 1, 'estado_id' => $estado, 'tipo_pessoa' => 'PF'])
            ->toArray();

        //TABELAS ODONTO PJ
        $this->loadModel("OdontoTabelas");
        $opj = $this->OdontoTabelas
            ->find('all')
            ->where(['validade' => 1, 'estado_id' => $estado, 'tipo_pessoa' => 'PJ'])
            ->toArray();

        $optRamos = array();

        if (count($spf) > 0) {
            $optRamos['SPF'] = 'SAÚDE - PESSOA FÍSICA';
        }
        if (count($spj) > 0) {
            $optRamos['SPJ'] = 'SAÚDE - PME';
        }

        if (count($opf) > 0) {
            $optRamos['OPF'] = 'ODONTO - PESSOA FÍSICA';
        }

        if (count($opj) > 0) {
            $optRamos['OPJ'] = 'ODONTO - PME';
        }
        // FIM FIND RAMOS

        //OPTIONS DE OPERADORAS
        if ($ramo == "SPF") {
            $this->loadModel("PfTabelas");
            switch ($profissao) {
                case 0: //TODAS AS PROFISSOES - Todas as Operadoras do Estado com tabelas válidas
                    $ids = $this->PfTabelas->find("list", ["keyValue" => "pf_operadora_id", "valueField" => "pf_operadora_id"])->select(["pf_operadora_id"])->where(["estado_id" => $estado, "validade" => 1])->toArray();
                    $idsPfOperadorasFinal = $ids;
                    break;
                case 9999: //SEM PROFISSAO - Apenas Operadoras com Tabelas INDIVIDUAIS
                    $tabelasValidas = $this->PfTabelas->find("all", ["keyValue" => "pf_operadora_id", "valueField" => "pf_operadora_id"])->where(["estado_id" => $estado, "validade" => 1, "modalidade" => "INDIVIDUAL"])->toArray();

                    foreach ($tabelasValidas as $tabela) {
                        $ids[$tabela["pf_operadora_id"]] = $tabela["pf_operadora_id"];
                    }
                    $idsPfOperadorasFinal = $ids;
                    break;
                default:
                    //COLETIVO POR ADESAO
                    $this->loadModel("PfEntidadesProfissoes");
                    $entidadesIds = $this->PfEntidadesProfissoes->find("list", ["keyField" => "pf_entidade_id", "valueField" => "pf_entidade_id"])->where(["pf_profissao_id" => $profissao])->toArray();

                    $this->loadModel("PfEntidadesPfOperadoras");
                    $idsPfOperadoras = $this->PfEntidadesPfOperadoras->find("list", ["keyField" => "pf_operadora_id", "valueField" => "pf_operadora_id"])->where(["pf_entidade_id IN" => $entidadesIds])->select(["pf_operadora_id"])->toArray();

                    // INDIVIDUAIS
                    $tabelasValidas = $this->PfTabelas->find("list", ["keyValue" => "pf_operadora_id", "valueField" => "pf_operadora_id"])->where(["estado_id" => $estado, "validade" => 1, "modalidade" => "INDIVIDUAL"])->toArray();
                    foreach ($tabelasValidas as $tabela) {
                        $ids[$tabela["pf_operadora_id"]] = $tabela["pf_operadora_id"];
                    }
                    $idsPfOperadorasFinal = $ids + $idsPfOperadoras;
                    break;
            }
            $this->loadModel("PfOperadoras");
            $optOperadoras = $this->PfOperadoras->find("list", ['valueField' => function ($e) {
                return $e->get('nome') . '  ' . $e->get('detalhe');
            }])
                ->where(["id IN" => $idsPfOperadorasFinal, "estado_id" => $estado])
                ->order(["nome" => "ASC"])->toArray();
        } else {
            //CASO NÃO SEJA "SPF"
            $conditions = null;
            $idsPfOperadoras = null;
            switch ($ramo) {
                case "SPJ":
                    $model = "Operadoras";
                    $tabelas = "Tabelas";
                    $campoOperadora = "operadora_id";
                    break;
                case "OPF":
                    $model = "OdontoOperadoras";
                    $tabelas = "OdontoTabelas";
                    $campoOperadora = "odonto_operadora_id";
                    $conditions = array("tipo_pessoa" => "PF");
                    break;
                case "OPJ":
                    $model = "OdontoOperadoras";
                    $tabelas = "OdontoTabelas";
                    $campoOperadora = "odonto_operadora_id";
                    $conditions = array("tipo_pessoa" => "PJ");
                    break;
            }
            $this->loadModel($tabelas);
            $tabelasValidas = $this->$tabelas
                ->find('all')
                ->select([$campoOperadora])
                ->distinct([$campoOperadora])
                ->where(["validade" => 1])
                ->toArray();
            foreach ($tabelasValidas as $tabelaValida) {
                $operadoras_ids[] = $tabelaValida[$campoOperadora];
            }
            $this->loadModel($model);
            $optOperadoras = $this->$model->find("list", ['valueField' => function ($e) {
                return $e->get('nome') . '  ' . $e->get('detalhe');
            }])
                ->where(["id IN" => $operadoras_ids, $conditions, "estado_id" => $estado])
                ->order(["nome" => "ASC"])->toArray();
        }

        //OPTIONS DE COPARTICIPAÇÃO
        switch ($ramo) {
            case "SPF":
                $this->loadModel("PfTabelas");
                $tabelas = $this->PfTabelas->find("all")->where(["estado_id" => $estado, "pf_operadora_id" => $operadora]);
                break;
            case "SPJ":
                $this->loadModel("Tabelas");
                $tabelas = $this->Tabelas->find("all")->where(["estado_id" => $estado, "operadora_id" => $operadora]);
                break;
            case "OPF":
                $this->loadModel("OdontoTabelas");
                $tabelas = $this->OdontoTabelas->find("all")->where(["estado_id" => $estado, "odonto_operadora_id" => $operadora, "tipo_pessoa" => "PF"]);
                break;
            case "OPJ":
                $this->loadModel("OdontoTabelas");
                $tabelas = $this->OdontoTabelas->find("all")->where(["estado_id" => $estado, "odonto_operadora_id" => $operadora, "tipo_pessoa" => "PJ"]);
                break;
        }
        foreach ($tabelas as $tabela) {
            $coparticipacaoC[$tabela["coparticipacao"]] = $tabela["coparticipacao"];
        }
        foreach ($coparticipacaoC as $coparticipacaoIntermediaria) {
            switch ($coparticipacaoIntermediaria) {
                case "n":
                    $optCoparticipacao[$coparticipacaoIntermediaria] = "SEM COPARTICIPAÇÃO";
                    break;
                case "s":
                    $optCoparticipacao[$coparticipacaoIntermediaria] = "COM COPARTICIPAÇÃO";
                    break;
            }
        }
        $optCoparticipacao[99] = "TODAS AS OPÇÕES";


        //OPTIONS DE VIDAS
        if ($ramo == "SPJ" || $ramo == "OPJ") {
            if ($operadora) {
                //RAMO SELECIONADO
                switch ($ramo) {
                    case "SPJ":
                        $model = "Tabelas";
                        $campoOperadora = "operadora_id";
                        $this->loadModel($model);
                        $tabelas = $this->$model->find('all')
                            ->where([$campoOperadora => $operadora, "validade" => 1])
                            ->order(["minimo_vidas" => "ASC"])->toArray();

                        break;
                    case "OPJ":
                        $this->loadModel("OdontoTabelas");
                        $tabelas = $this->OdontoTabelas->find('all')
                            ->contain(["OdontoProdutos"])
                            ->where([
                                "OdontoProdutos.odonto_operadora_id" => $operadora,
                                "validade" => 1,
                                "tipo_pessoa" => "PJ"
                            ])
                            ->order(["minimo_vidas" => "ASC"])->toArray();
                        break;
                }
            }
            if ($tabelas) {
                $vidas = array();
                foreach ($tabelas as $tabela) {
                    $optVidas[$tabela["minimo_vidas"] . "-" . $tabela["maximo_vidas"]] = $tabela["minimo_vidas"] . " a " . $tabela["maximo_vidas"];
                }
            }
        } else {
            $optVidas = "sem";
        }
        $dados = [
            'ramos' => $optRamos,
            'operadoras' => $optOperadoras,
            'coparticipacao' => $optCoparticipacao,
            'vidas' => $optVidas,
            'selecteds' => $tabelaSalva
        ];
        $dados = json_encode($dados);
        $this->response->type('json');
        $this->response->body($dados);
        return $this->response;
    }

    public function filterAreasComercializacoes()
    {
        if ($this->request->is('POST') && !empty($this->request->getData())) {
            $dados = $this->request->getData();
            switch ($dados["ramo_id"]) {
                case "SPF":
                    $this->loadModel('PfAreasComercializacoes');
                    $areas_comercializacoes_municipios = $this->PfAreasComercializacoes
                        ->find('list', ['valueField' => 'nome'])
                        ->matching('Estados', function ($q) use ($dados) {
                            return $q->where(['Estados.id' => $dados['estado_id']]);
                        })
                        ->matching('PfOperadoras', function ($q) use ($dados) {
                            return $q->where(['PfOperadoras.id' => $dados['operadora_id']]);
                        })
                        ->matching('PfTabelas', function ($q) use ($dados) {
                            if ($dados['coparticipacao'])
                                return $q->where(['PfTabelas.coparticipacao' => $dados['coparticipacao']]);
                            return $q;
                        })
                        ->toArray();
                    $areas_comercializacoes_metropoles = $this->PfAreasComercializacoes
                        ->find('list', ['valueField' => 'nome'])
                        ->matching('Metropoles.Estados', function ($q) use ($dados) {
                            return $q->where(['Estados.id' => $dados['estado_id']]);
                        })
                        ->matching('PfOperadoras', function ($q) use ($dados) {
                            return $q->where(['PfOperadoras.id' => $dados['operadora_id']]);
                        })
                        ->matching('PfTabelas', function ($q) use ($dados) {
                            if ($dados['coparticipacao'])
                                return $q->where(['PfTabelas.coparticipacao' => $dados['coparticipacao']]);
                            return $q;
                        })
                        ->toArray();
                    $areas_comercializacoes = $areas_comercializacoes_municipios + $areas_comercializacoes_metropoles;
                    break;
                case "SPJ":
                    $this->loadModel('AreasComercializacoes');
                    $areas_comercializacoes_municipios = $this->AreasComercializacoes
                        ->find('list', ['valueField' => 'nome'])
                        ->matching('Estados', function ($q) use ($dados) {
                            return $q->where(['Estados.id' => $dados['estado_id']]);
                        })
                        ->matching('Operadoras', function ($q) use ($dados) {
                            return $q->where(['Operadoras.id' => $dados['operadora_id']]);
                        })
                        ->toArray();
                    $areas_comercializacoes_metropoles = $this->AreasComercializacoes
                        ->find('list', ['valueField' => 'nome'])
                        ->matching('Metropoles.Estados', function ($q) use ($dados) {
                            return $q->where(['Estados.id' => $dados['estado_id']]);
                        })
                        ->matching('Operadoras', function ($q) use ($dados) {
                            return $q->where(['Operadoras.id' => $dados['operadora_id']]);
                        })
                        ->toArray();
                    $areas_comercializacoes = $areas_comercializacoes_municipios + $areas_comercializacoes_metropoles;
                    break;
                case 'OPF':
                case 'OPJ':
                    $this->loadModel('OdontoAreasComercializacoes');
                    $areas_comercializacoes_municipios = $this->OdontoAreasComercializacoes
                        ->find('list', ['valueField' => 'nome'])
                        ->matching('Estados', function ($q) use ($dados) {
                            return $q->where(['Estados.id' => $dados['estado_id']]);
                        })
                        ->matching('OdontoOperadoras', function ($q) use ($dados) {
                            return $q->where(['OdontoOperadoras.id' => $dados['operadora_id']]);
                        })
                        ->toArray();
                    $areas_comercializacoes_metropoles = $this->OdontoAreasComercializacoes
                        ->find('list', ['valueField' => 'nome'])
                        ->matching('Metropoles.Estados', function ($q) use ($dados) {
                            return $q->where(['Estados.id' => $dados['estado_id']]);
                        })
                        ->matching('OdontoOperadoras', function ($q) use ($dados) {
                            return $q->where(['OdontoOperadoras.id' => $dados['operadora_id']]);
                        })
                        ->toArray();
                    $areas_comercializacoes = $areas_comercializacoes_municipios + $areas_comercializacoes_metropoles;
                    break;
            }
            $this->set(compact('areas_comercializacoes'));
        }
    }

    public function filterContratacao()
    {
        if ($this->request->is('POST') && !empty($this->request->getData())) {
            $dados = $this->request->getData();
            $this->loadModel('Tabelas');
            $contratacao = $this->Tabelas
                ->find('list', [
                    'keyField' => function ($q) {
                        return $q->get('tipo_contratacao');
                    },
                    'valueField' => function ($q) {
                        switch ($q->get('tipo_contratacao')) {
                            case 0:
                                return 'OPCIONAL';
                                break;
                            case 1:
                                return 'COMPULSÓRIO';
                                break;
                            case 2:
                                return 'AMBOS';
                                break;
                        }
                    }
                ])
                ->matching('AreasComercializacoes.Operadoras', function ($q) use ($dados) {
                    return $q->where(['Operadoras.id' => $dados['operadora_id']]);
                })
                ->group('tipo_contratacao');

            $this->set(compact('contratacao'));
        }
    }

    function array_equal($a, $b)
    {
        return (is_array($a)
            && is_array($b)
            && count($a) == count($b)
            && array_diff($a, $b) === array_diff($b, $a));
    }
}
