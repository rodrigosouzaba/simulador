<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TabelasGeradasProdutos Controller
 *
 * @property \App\Model\Table\TabelasGeradasProdutosTable $TabelasGeradasProdutos
 */
class TabelasGeradasProdutosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['TabelasGeradas', 'Produtos']
        ];
        $tabelasGeradasProdutos = $this->paginate($this->TabelasGeradasProdutos);

        $this->set(compact('tabelasGeradasProdutos'));
        $this->set('_serialize', ['tabelasGeradasProdutos']);
    }

    /**
     * View method
     *
     * @param string|null $id Tabelas Geradas Produto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tabelasGeradasProduto = $this->TabelasGeradasProdutos->get($id, [
            'contain' => ['TabelasGeradas', 'Produtos']
        ]);

        $this->set('tabelasGeradasProduto', $tabelasGeradasProduto);
        $this->set('_serialize', ['tabelasGeradasProduto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tabelasGeradasProduto = $this->TabelasGeradasProdutos->newEntity();
        if ($this->request->is('post')) {
            $tabelasGeradasProduto = $this->TabelasGeradasProdutos->patchEntity($tabelasGeradasProduto, $this->request->data);
            if ($this->TabelasGeradasProdutos->save($tabelasGeradasProduto)) {
                $this->Flash->success(__('The tabelas geradas produto has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tabelas geradas produto could not be saved. Please, try again.'));
            }
        }
        $tabelasGeradas = $this->TabelasGeradasProdutos->TabelasGeradas->find('list', ['limit' => 200]);
        $produtos = $this->TabelasGeradasProdutos->Produtos->find('list', ['limit' => 200]);
        $this->set(compact('tabelasGeradasProduto', 'tabelasGeradas', 'produtos'));
        $this->set('_serialize', ['tabelasGeradasProduto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tabelas Geradas Produto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tabelasGeradasProduto = $this->TabelasGeradasProdutos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tabelasGeradasProduto = $this->TabelasGeradasProdutos->patchEntity($tabelasGeradasProduto, $this->request->data);
            if ($this->TabelasGeradasProdutos->save($tabelasGeradasProduto)) {
                $this->Flash->success(__('The tabelas geradas produto has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tabelas geradas produto could not be saved. Please, try again.'));
            }
        }
        $tabelasGeradas = $this->TabelasGeradasProdutos->TabelasGeradas->find('list', ['limit' => 200]);
        $produtos = $this->TabelasGeradasProdutos->Produtos->find('list', ['limit' => 200]);
        $this->set(compact('tabelasGeradasProduto', 'tabelasGeradas', 'produtos'));
        $this->set('_serialize', ['tabelasGeradasProduto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tabelas Geradas Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tabelasGeradasProduto = $this->TabelasGeradasProdutos->get($id);
        if ($this->TabelasGeradasProdutos->delete($tabelasGeradasProduto)) {
            $this->Flash->success(__('The tabelas geradas produto has been deleted.'));
        } else {
            $this->Flash->error(__('The tabelas geradas produto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
