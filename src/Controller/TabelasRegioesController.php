<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TabelasRegioes Controller
 *
 * @property \App\Model\Table\TabelasRegioesTable $TabelasRegioes
 */
class TabelasRegioesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tabelas', 'Regioes']
        ];
        $tabelasRegioes = $this->paginate($this->TabelasRegioes);

        $this->set(compact('tabelasRegioes'));
        $this->set('_serialize', ['tabelasRegioes']);
    }

    /**
     * View method
     *
     * @param string|null $id Tabelas Regio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tabelasRegio = $this->TabelasRegioes->get($id, [
            'contain' => ['Tabelas', 'Regioes']
        ]);

        $this->set('tabelasRegio', $tabelasRegio);
        $this->set('_serialize', ['tabelasRegio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tabelasRegio = $this->TabelasRegioes->newEntity();
        if ($this->request->is('post')) {
            $tabelasRegio = $this->TabelasRegioes->patchEntity($tabelasRegio, $this->request->data);
            if ($this->TabelasRegioes->save($tabelasRegio)) {
                $this->Flash->success(__('The tabelas regio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tabelas regio could not be saved. Please, try again.'));
            }
        }
        $tabelas = $this->TabelasRegioes->Tabelas->find('list', ['limit' => 200]);
        $regioes = $this->TabelasRegioes->Regioes->find('list', ['limit' => 200]);
        $this->set(compact('tabelasRegio', 'tabelas', 'regioes'));
        $this->set('_serialize', ['tabelasRegio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tabelas Regio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tabelasRegio = $this->TabelasRegioes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tabelasRegio = $this->TabelasRegioes->patchEntity($tabelasRegio, $this->request->data);
            if ($this->TabelasRegioes->save($tabelasRegio)) {
                $this->Flash->success(__('The tabelas regio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tabelas regio could not be saved. Please, try again.'));
            }
        }
        $tabelas = $this->TabelasRegioes->Tabelas->find('list', ['limit' => 200]);
        $regioes = $this->TabelasRegioes->Regioes->find('list', ['limit' => 200]);
        $this->set(compact('tabelasRegio', 'tabelas', 'regioes'));
        $this->set('_serialize', ['tabelasRegio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tabelas Regio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tabelasRegio = $this->TabelasRegioes->get($id);
        if ($this->TabelasRegioes->delete($tabelasRegio)) {
            $this->Flash->success(__('The tabelas regio has been deleted.'));
        } else {
            $this->Flash->error(__('The tabelas regio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
