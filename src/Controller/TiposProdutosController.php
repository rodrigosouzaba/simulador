<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TiposProdutos Controller
 *
 * @property \App\Model\Table\TiposProdutosTable $TiposProdutos
 */
class TiposProdutosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $tiposProdutos = $this->paginate($this->TiposProdutos);

        $this->set(compact('tiposProdutos'));
        $this->set('_serialize', ['tiposProdutos']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipos Produto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tiposProduto = $this->TiposProdutos->get($id, [
            'contain' => []
        ]);

        $this->set('tiposProduto', $tiposProduto);
        $this->set('_serialize', ['tiposProduto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tiposProduto = $this->TiposProdutos->newEntity();
        if ($this->request->is('post')) {
            $tiposProduto = $this->TiposProdutos->patchEntity($tiposProduto, $this->request->data);
            if ($this->TiposProdutos->save($tiposProduto)) {
                $this->Flash->success(__('The tipos produto has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tipos produto could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('tiposProduto'));
        $this->set('_serialize', ['tiposProduto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipos Produto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tiposProduto = $this->TiposProdutos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tiposProduto = $this->TiposProdutos->patchEntity($tiposProduto, $this->request->data);
            if ($this->TiposProdutos->save($tiposProduto)) {
                $this->Flash->success(__('The tipos produto has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tipos produto could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('tiposProduto'));
        $this->set('_serialize', ['tiposProduto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipos Produto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tiposProduto = $this->TiposProdutos->get($id);
        if ($this->TiposProdutos->delete($tiposProduto)) {
            $this->Flash->success(__('The tipos produto has been deleted.'));
        } else {
            $this->Flash->error(__('The tipos produto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
