<?php

namespace App\Controller;

use Cake\Network\Http\Client;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Firebase\JWT\JWT;
use Cake\Utility\Security;
use Cake\Collection\Collection;
use Rest\Controller\RestController;
use Cake\Http\Response;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{


    public $paginate = [
        'limit' => 50,
        "first" => "<<",
        "last" => ">>",
        "prev" => "i:chevron-left",
        "next" => "i:chevron-right",
        "class" => "no-margin"
    ];

    //Api Key Mailchimp
    public $apiKey = 'b03000c8cd338729a407beddbdeb44a7-us9';

    // Dados de acesso Active Campaign
    public $activeCampaignKey = '813b271f8a5f15eea52bfa0ad4c7dbcb6b9db16d27b10983d367a09ff88be25f1f9f6d49';
    public $activeCampaignBaseUrl = 'https://corretorparceiro.api-us1.com/api';

    /**
     * Permissões para usuário NÃO LOGADO devem estar aqui
     *
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.

        $this->Auth->allow([
            'add',
            'ajusteautomatico',
            'ativacao',
            'ativacaocel',
            "ativacaoemail",
            'calculoremoto',
            'checkSession',
            'enviarArquivos',
            'enviarCalculoRemoto',
            'enviarIndicacao',
            'enviarSms',
            'envioAlertas',
            'filtroMunicipio',
            'gerador',
            'homeTemp',
            'inicio',
            'loginapi',
            'login',
            'loginUser',
            'logout',
            'lojas',
            'mudarcel',
            'novasenha',
            'obrigado',
            'recuperarSenha',
            'reenviarCodigo',
            'reenviarCodigoEmail',
            'recuperarSenhaEmail',
            "tabela",
            'zenvia',
            'lock'
        ]);
        // Checks if the user is already logged in on login/register actions.
        //        debug($this->Auth->user('id'));
        //        if (in_array($this->request->action, ['register', 'login']) && !empty($this->Auth->user('id'))) {
        //            throw new ForbiddenException(__('You are already logged in'));
        //        }

        if ($this->request->is('options')) {
            $this->setCorsHeaders();
            return $this->response;
        }

        # Trata as chamadas ajax com jsonp
        // if (isset($this->params->query['callback'])) {
        //     $this->autoRender = false;
        //     $this->RequestHandler->respondAs('json');
        //     $this->jsonpCallback = $this->params->query['callback'];
        //     unset($this->params->query['callback']);
        // }
    }
    public function afterFilter(event $event)
    {
        # Trata as chamadas ajax com jsonp
        if (!empty($this->jsonpCallback)) {
            $this->autoRender = false;
            $this->RequestHandler->respondAs('json');
            $json = $this->viewVars['_serialize'][0];
            $dados = $this->viewVars[$json];
            echo $this->jsonpCallback . "(" . json_encode(array($json => $dados)) . ");";
        }
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function beforeRender(event $event)
    {
        $this->setCorsHeaders();
        $this->set("_jsonp", true);
    }

    private function setCorsHeaders()
    {
        $this->response->cors($this->request)
            ->allowOrigin(['*'])
            ->allowMethods(['*'])
            ->allowHeaders(['Origin', 'Content-Type'])
            ->allowCredentials(['false'])
            ->exposeHeaders(['Link'])
            ->maxAge(300)
            ->build();
    }

    /**
     * Ação de LOGIN com redirecionamento
     */
    public function login()
    {
        $this->set('title', 'Login');
        $user = '';
        if (!is_null($this->request->getQuery('redirect'))) {
            return $this->redirect("https://corretorparceiro.com.br?login=true");
        }
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            // debug($this->request->getData());
            // debug($user);
            // die;

            if ($user["bloqueio"] <> "S") {
                if ($user) {
                    $user["user_id"] = $user["id"];

                    $usero = $user;
                    $userLogado = $this->Users->get($user['id']);

                    //SALVA ULTIMO LOGIN DO USUARIO
                    $userLogado->logado = 'S';
                    $userLogado->ultimologin = date('Y-m-d H:i:s');
                    $userLogado->dirty('ultimologin', true);
                    $this->Users->save($userLogado);
                    $this->Auth->setUser($user);

                    $session = $this->request->session();
                    $sessao = $session->read('Auth.User');

                    $this->loadModel('UsersGrupos');
                    $user_grupos = $this->UsersGrupos->find('list')->where(['user_id' => $user['id']])->toArray();
                    $session->write([
                        'Auth.User.grupos' => $user_grupos
                    ]);

                    if ($userLogado->imagem_id != null) {
                        $this->loadModel('Imagens');
                        $imagem = $this->Imagens->get($userLogado->imagem_id);
                        $session = $this->request->session();
                        $session->write(['Auth.User.imagem' => $imagem]);
                        $user['imagem'] = $imagem;
                    }

                    $this->set(compact('usero'));
                    if ($sessao['senha_provisoria'] == 'S') {
                        $this->request->session()->destroy();
                        return $this->redirect(['controller' => 'users', 'action' => 'novasenha/' . $userLogado->id]);
                    }

                    // Novas Interações
                    $this->loadModel('Avaliacaos');
                    $interacoes = $this->Avaliacaos->find('list')->where("created > '$userLogado->ultimologin'")->count();
                    $session->write(['Auth.User.new_interacoes' => $interacoes]);
                    // FIM - Novas Interações

                    //começo sessions
                    $user_id = $user['id'];
                    $this->loadModel('Sessions');
                    $delete = $this->Sessions->find('list')->where([
                        'id <>' => $this->request->session()->id(),
                        'user_id' => $user_id
                    ])
                        ->toArray();

                    if ($delete) {
                        $this->Sessions->deleteAll([
                            'id IN' => $delete
                        ]);
                    }
                    //fim sessions


                    $this->loadModel('UsersGrupos');
                    $user_grupos = $this->UsersGrupos->find('list', ['keyField' => 'grupo_id', 'valueField' => 'grupo_id'])->where(['user_id' => $user['id']])->toArray();
                    $session->write(['Auth.User.grupos' => array_values($user_grupos)]);

                    $user['perfisUser'] = array_values($this->Users->UsersSisGrupos->getPerfis($user['id']));

                    $dadosMenuPrincipal = $this->Menu->dadosMenu($user);
                    $dadosMenuFerramentas = $this->menuAdicional($dadosMenuPrincipal);
                    ksort($dadosMenuFerramentas['linha'][1]);
                    $this->request->session()->write('Auth.menu', $dadosMenuFerramentas);

                    //VERIFICA SE HÁ SESSAO NO BANCO E APAGA (SESSAO EM OUTROS DISPOSITIVOS)
                    $this->loadModel('Sessions');
                    $this->Sessions->deleteAll(['id <>' => $this->request->session()->id(), 'user_id' => $user["id"]]);
                    $token = JWT::encode(
                        [
                            'sub' => $user['id'],
                            'data' => $user,
                            'exp' =>  time() + 1380
                        ],
                        Security::getSalt()
                    );
                    unset($_COOKIE['perfis']);
                    unset($_COOKIE['token']);
                    setcookie('perfis', implode(",", $user['perfisUser']), time() + 1380, "/");
                    setcookie('token', $token, time() + 1380, "/");

                    $session->renew();

                    if ($sessao['validacao'] === null || $sessao['validacao'] <> 'ATIVO' || $sessao['validacao_email'] === null || $sessao['validacao_email'] <> 'ATIVO') {
                        return $this->redirect(['controller' => 'users', 'action' => 'ativacao/' . $sessao['id']]);
                    } else {
                        return $this->redirect(["controller" => "users", "action" => "central"]);
                    }
                }
                $this->Flash->error(__('Usuário ou senha inválida, tente novamente'));
            } else {
                $this->Flash->error(__(' Seu período de teste expirou. Entre em contato com o suporte a corretores.'));
                return $this->redirect(['action' => 'logout']);
            }
        }
        $this->set('user', $user);
        $this->viewBuilder()->layout('login');
    }

    /**
     * Ação de LOGIN com redirecionamento
     */
    public function loginUser()
    {
        $user = '';
        $test = '';
        if ($this->request->is('post')) {
            //        debug($user);
            //        die();
            $user = $this->Auth->identify();
            if ($user) {
                //                debug($user);
                //                die();
                $test = 'OK';

                $userLogado = $this->Users->get($user['id']);
                $userLogado->logado = 'S';
                $userLogado->ultimologin = date('Y-m-d H:i:s');
                //                debug($userLogado);
                $userLogado->dirty('ultimologin', true);
                $this->Users->save($userLogado);

                $this->Auth->setUser($user);
                $session = $this->request->session();
                $sessao = $session->read('Auth.User');

                $this->loadModel('UsersGrupos');
                $user_grupos = $this->UsersGrupos->find('list', ['keyField' => 'grupo_id', 'valueField' => 'grupo_id'])->where(['user_id' => $user['id']])->toArray();
                $session->write(['Auth.User.grupos' => array_values($user_grupos)]);

                $this->loadModel('Estados');
                $estado = $this->Estados->get($user['estado_id']);
                $session->write(['Auth.User.estado' => $estado]);

                $this->loadModel('Municipios');
                $municipio = $this->Municipios->get($user['municipio_id']);
                $session->write(['Auth.User.municipio' => $municipio]);

                //adiciona variável de sessão que é usada para exibir ou não um modal na action central
                $session->write('entrou', 1);

                if ($userLogado->imagem_id != null) {
                    $this->loadModel('Imagens');
                    $imagem = $this->Imagens->get($userLogado->imagem_id);

                    $session = $this->request->session();

                    $session->write([
                        'Auth.User.imagem' => $imagem
                    ]);

                    $session->renew();
                }
                //                debug($sessao);
                //                die();
                //                if ($sessao['logado'] === 'S') {
                //                    $this->Flash->error(__('Usuário já está logado em outro dispositivo.'));
                //                    return $this->redirect($this->Auth->logout());
                //                }
                if ($sessao['senha_provisoria'] == 'S') {
                    $id = $sessao['id'];
                    $this->request->session()->destroy();
                    return $this->redirect(['controller' => 'users', 'action' => 'novasenha/' . $id]);
                }

                if (($user['validacao'] === null) || ($user['validacao'] <> 'ATIVO') || ($user["validacao_email"] === null) || ($user["validacao_email"] <> 'ATIVO')) {
                    return $this->redirect(['controller' => 'users', 'action' => 'ativacao/' . $sessao['id']]);
                } else {

                    //                    $this->Auth->setUser($user);
                    //                    return $this->redirect($this->Auth->redirectUrl());
                    //                    $this->viewBuilder()->layout('default');
                    return $this->redirect(['action' => 'central']);
                }
                $this->set(compact('sessao'));
            }
            $this->Flash->error(__('Usuário ou senha ínvalido, tente novamente'));
        }
        //        $this->set(compact('test'));
        //        var_dump($test);
        $this->set('test', $test);

        $this->set('user', $user);
        $this->viewBuilder()->layout('default');
    }

    /**
     * Ação de LOGIN com redirecionamento
     */
    public function homeTemp()
    {
        $this->set('title', 'Login');
        $user = '';
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {

                $userLogado = $this->Users->get($user['id']);

                $userLogado->ultimologin = date('Y-m-d H:i:s');
                //                debug($userLogado);
                $userLogado->dirty('ultimologin', true);
                $this->Users->save($userLogado);

                $this->Auth->setUser($user);
                $session = $this->request->session();
                $sessao = $session->read('Auth.User');

                if ($userLogado->imagem_id != null) {
                    $this->loadModel('Imagens');
                    $imagem = $this->Imagens->get($userLogado->imagem_id);

                    $session = $this->request->session();

                    $session->write([
                        'Auth.User.imagem' => $imagem
                    ]);
                    $session->renew();
                }
                if ($sessao['validacao'] === null || $sessao['validacao'] <> 'ATIVO' || $sessao['validacao_email'] === null || $sessao['validacao_email'] <> 'ATIVO') {

                    //                 if ($sessao['validacao'] == null || $sessao['validacao'] <> 'ATIVO') {
                    return $this->redirect(['controller' => 'users', 'action' => 'ativacao/' . $sessao['id']]);
                } else {
                    return $this->redirect($this->Auth->redirectUrl());
                }
            }
            $this->Flash->error(__('Usuário ou senha ínvalido, tente novamente'));
        }
        $this->set('user', $user);
        $this->viewBuilder()->layout('nova_home');
    }

    /**
     * Ação de ATIVAÇÃO DE Usuário mediante código enviado por email com redirecionamento
     */
    public function ativacaocel($user_id = null, $codigo_cel = null)
    {
        $user = $this->Users->get($user_id);

        if ($this->request->is('post')) {
            if ($codigo_cel == $user['codigo']) {
                $user->validacao = 'ATIVO';
                if ($this->Users->save($user)) {
                    $session = $this->request->session();
                    $sessao = $session->read('Auth.User');
                    $sessao['validacao'] = 'ATIVO';

                    $session->write('Auth.User', $sessao);

                    $mensagem = "Celular Validado com Sucesso";
                }
            } else {
                $mensagem = "Código Celular inválido";
            }
        }
        $this->set(compact('mensagem'));
    }

    /**
     * Ação de ATIVAÇÃO DE Usuário mediante código enviado por email com redirecionamento
     */
    public function ativacaoemail($user_id = null, $codigo_email = null)
    {
        $user = $this->Users->get($user_id);
        if ($this->request->is('post')) {
            if ($codigo_email == $user['codigo_email']) {
                $user->validacao_email = 'ATIVO';
                if ($this->Users->save($user)) {
                    $session = $this->request->session();
                    $sessao = $session->read('Auth.User');
                    $sessao['validacao_email'] = 'ATIVO';

                    $session->write('Auth.User', $sessao);
                    $mensagem = "Validado com Sucesso";
                }
            } else {
                $mensagem = "Código E-mail inválido";
            }
            $this->set(compact('mensagem', 'user'));
        }
    }

    /**
     * Ação de ATIVAÇÃO DE Usuário mediante código enviado por email com redirecionamento
     */
    public function ativacao($id = null)
    {

        $this->set('title', 'Ativação de Cadastro');
        $userId = $id;

        $user = $this->Users->get($userId);
        if ($this->request->is('post')) {
            $dados = $this->request->data;
            /*
		if (isset($dados["codigo-cel"]) && !empty($dados["codigo-cel"])){
			if ($dados['codigo-cel'] === $user['codigo']) {
				$user->validacao = 'ATIVO';
                if ($this->Users->save($user)) {
                    $session = $this->request->session();
                    $sessao = $session->read('Auth.User');
                    $sessao['validacao'] = 'ATIVO';

                    $session->write('Auth.User', $sessao);
                    $sessao = $session->read('Auth.User');
				}
			}else{
				$this->Flash->error(__('Código SMS inválido'));
			}
		}
		if (isset($dados["codigo-email"]) && !empty($dados["codigo-email"])){
			if ($dados['codigo-email'] === $user['codigo_email']) {
				$user->validacao_email = 'ATIVO';
                if ($this->Users->save($user)) {
                    $session = $this->request->session();
                    $sessao = $session->read('Auth.User');
                    $sessao['validacao'] = 'ATIVO';

                    $session->write('Auth.User', $sessao);
                    $sessao = $session->read('Auth.User');
				}
			}else{
				$this->Flash->error(__('Código E-mail inválido'));
			}
		}
*/
        }
        if ($user["validacao"] == "ATIVO" && $user["validacao_email"] == "ATIVO") {
            return $this->redirect(['controller' => 'users', 'action' => 'central']);
        }
        $this->set(compact('userId', 'user'));
        //        $this->set('user', $user);
    }

    /**
     * Ação de LOGOUT com redirecionamento
     */
    public function logout()
    {
        setcookie('token', null, -1, '/');
        setcookie('perfis', null, -1, '/');
        setcookie('token', '', time() - 1000, '/');
        setcookie('perfis', '', time() - 1000, '/');
        $this->request->session()->destroy('Auth.User.Permission');
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Ação de Alterar SENHA com redirecionamento
     */
    public function senha($id = null)
    {
        //        debug($id);
        $user = $this->Users->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            //            debug($this->request->data());die();
            $user = $this->Users->patchEntity($user, [
                'password' => $this->request->data['new_password']
            ]);
            //            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Dados do Usuário alterados com sucesso.'));

                return $this->redirect(['controller' => 'simulacoes', 'action' => 'index']);
            } else {
                $this->Flash->error(__('Erro ao alterar os dados do Usuário. Tente Novamente.'));
            }
        }

        $this->set(compact('user'));
        $this->set('title', 'Alterar Senha');
        //        return $this->redirect($this->Auth->logout());
    }

    /**
     * Alteração de SENHA provisória com confirmação
     */
    public function novasenha($id = null)
    {
        //        debug($id);
        $user = $this->Users->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            //            debug($this->request->data());die();
            $user = $this->Users->patchEntity($user, [
                'password' => $this->request->data['password'],
                'senha_provisoria' => 'N'
            ]);
            //            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Senha alterada com sucesso.'));

                return $this->redirect(['action' => 'loginUser']);
            } else {
                $this->Flash->error(__('Erro ao alterar senha. Tente Novamente.'));
            }
        }

        $this->set(compact('user'));
        $this->set('title', 'Alterar Senha');
        //        return $this->redirect($this->Auth->logout());
    }

    /**
     * Método para buscar Usuário por parametros Nome / CPF / E-mail
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function encontrarUsuario()
    {
        if (!empty($this->request->data['info_busca'])) {
            switch (ctype_alpha(str_replace(" ", "", $this->request->data["info_busca"]))) {
                case true:
                    $conditions = 'nome LIKE "%' . $this->request->data['info_busca'] . '%" OR sobrenome LIKE "%' . $this->request->data['info_busca'] . '%"';
                    break;
                case false:
                    if (strpos($this->request->data['info_busca'], "@") <> 0) {
                        $conditions = 'email LIKE "%' . $this->request->data['info_busca'] . '%"';
                        //                     	debug($conditions);die();
                    }
                    if (ctype_digit($this->request->data['info_busca'])) {
                        $conditions = 'username = "' . substr($this->request->data['info_busca'], 0, 3) . '.' . substr($this->request->data['info_busca'], 3, 3) . '.' . substr($this->request->data['info_busca'], 6, 3) . '-' . substr($this->request->data['info_busca'], 9, 2) . '" OR celular = "' . '(' . substr($this->request->data['info_busca'], 0, 2) . ') ' . substr($this->request->data['info_busca'], 2, 1) . ' ' . substr($this->request->data['info_busca'], 3, 4) . '-' . substr($this->request->data['info_busca'], 7, 4) . '"';
                    }
                    break;
            }
            $users = $this->VwUsuarios->find('all')
                ->where([
                    $conditions
                ]);
            $this->set(compact('users'));
            $this->render("consulta_usuarios");
        }
    }

    /**
     * Método para buscar Usuário por parametros Nome / CPF / E-mail
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtrarstatus()
    {
        //        debug($this->request->data);
        $this->loadModel('VwUsuarios');
        if (!empty($this->request->data['status'])) {
            $users = $this->VwUsuarios->find('all')->where([
                'VwUsuarios.validacao' => $this->request->data['status']
            ]);
            $this->set(compact('users'));
        } else {
            $users = $this->VwUsuarios->find('all');
        }
    }


    /**
     * Método para Reenviar código de ativação para Email
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function reenviarCodigo($id = null)
    {
        $usuario = $this->Users->get($id);
        $codigo = null;
        for ($i = 0; $i < 8; $i++) {
            $codigo .= rand(1, 9); //sorteia 1 numero de 0 a 9
        }
        $usuario['codigo'] = $codigo;
        $usuario['validacao'] = 'PENDENTE';
        if ($this->Users->save($usuario)) {
            $this->Flash->success(__('Código de Ativação Reenviado para o celular: ' . $usuario['celular']));
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            if ($sessao['role'] <> 'admin') {
                return $this->redirect(['controller' => 'Users', 'action' => 'zenvia/Novo Codigo/' . $usuario['id'] . "/" . $codigo . "/ativacao"]);
            } else {
                return $this->redirect(['controller' => 'Users', 'action' => 'zenvia/Novo Codigo/' . $usuario['id'] . "/" . $codigo . "/index"]);
            }
        } else {
            $this->Flash->error(__('Erro ao reenviar SMS com código de ativação. Favor procurar o Suporte.'));
            return $this->redirect(['action' => 'index']);
        }
    }






    /*

	    Mudar o celular na tela de Ativação

    */
    public function mudarcel()
    {
        /*
	    debug($this->request->data());
	    die();
*/
        $dados = $this->request->data();
        $user = $this->Users->get($dados["id"]);
        $user->celular = $dados["novo-cel"];
        $user->dirty('celular', true);

        if ($this->Users->save($user)) {
            $this->Flash->success(__('Celular alterado para: ' . $user['celular']));
        } else {
            $this->Flash->error(__('Erro ao alterar Celular. Número já em uso no sistema.'));
        }
    }

    public function mudarEmail()
    {
        $dados = $this->request->data();
        $user = $this->Users->get($dados["id"]);
        $user->email = $dados["novo-email"];
        if ($this->Users->save($user)) {
            $this->Flash->success(__('Email alterado para: ' . $user['email']));
        } else {
            $this->Flash->error(__('Erro ao alterar Email. Email já em uso no sistema.'));
        }
        $this->redirect(['action' => 'ativacao', $user['id']]);
    }

    /*

     Ação de Reenvio de código de ATIVAÇÃO DO EMAIL

    */
    public function reenviarCodigoEmailAdmin($id = null)
    {

        $usuario = $this->Users->get($id);

        $codigoEmail = null;
        for ($i = 0; $i < 8; $i++) {
            $codigoEmail .= rand(1, 9); //sorteia 1 numero de 0 a 9
        }
        $user = $this->Users->patchEntity($usuario, [
            'codigo_email' => $codigoEmail,
            'validacao_email' => 'PENDENTE',
        ]);

        $email = new Email('default');

        $email->from(['suporte@corretorparceiro.com.br' => 'Cálculo Rápido'])
            ->to($usuario['email'])
            ->from('suporte@corretorparceiro.com.br', 'Ferramentas de Apoio Corretor Parceiro')
            ->sender('suporte@corretorparceiro.com.br', 'Ferramentas de Apoio Corretor Parceiro')
            ->replyTo('suporte@corretorparceiro.com.br')
            ->emailFormat('html')
            ->subject('Código E-mail - Ferramentas de Apoio Corretor Parceiro')
            ->send(
                '<html>'
                    . '<img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
                    . '<br/>'
                    . '<h2>Olá ' . $usuario['nome'] . " " . $usuario['sobrenome'] . '</h2>'
                    . '<h3>Ativação do E-mail</h3>'
                    . '<h4>Este é seu novo código de ativação de e-mail:  <b style="color:red;">' . $codigoEmail . '</b></h4>'
                    . '<br/>'
                    . '</html>'
            );

        if ($this->Users->save($user)) {
            $this->Flash->success(__('Novo código enviado para o e-mail: ' . $usuario['email']));
        } else {
            $this->Flash->error(__('Erro ao enviar Código para o e-mail: ' . $usuario['email']));
        }
        return $this->redirect(['action' => 'index']);
    }

    /*

     Ação de Reenvio de código de ATIVAÇÃO DO EMAIL

    */
    public function reenviarCodigoEmail($id = null)
    {

        $usuario = $this->Users->get($id);

        $codigoEmail = null;
        for ($i = 0; $i < 8; $i++) {
            $codigoEmail .= rand(1, 9); //sorteia 1 numero de 0 a 9
        }
        $user = $this->Users->patchEntity($usuario, [
            'codigo_email' => $codigoEmail,
            'validacao_email' => 'PENDENTE',
        ]);

        $email = new Email('default');

        $email->from(['suporte@corretorparceiro.com.br' => 'Cálculo Rápido'])
            ->to($usuario['email'])
            ->from('suporte@corretorparceiro.com.br', 'Ferramentas de Apoio Corretor Parceiro')
            ->sender('suporte@corretorparceiro.com.br', 'Ferramentas de Apoio Corretor Parceiro')
            ->replyTo('suporte@corretorparceiro.com.br')
            ->emailFormat('html')
            ->subject('Código E-mail - Ferramentas de Apoio Corretor Parceiro')
            ->send(
                '<html>'
                    . '<img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
                    . '<br/>'
                    . '<h2>Olá ' . $usuario['nome'] . " " . $usuario['sobrenome'] . '</h2>'
                    . '<h3>Ativação do E-mail</h3>'
                    . '<h4>Este é seu novo código de ativação de e-mail:  <b style="color:red;">' . $codigoEmail . '</b></h4>'
                    . '<br/>'
                    . '</html>'
            );
        if ($this->Users->save($user)) {
            $this->Flash->success(__('Novo código enviado para o e-mail: ' . $usuario['email']));
        } else {
            $this->Flash->error(__('Erro ao tentar enviar código para o e-mail: ' . $usuario['email']));
        }
        return $this->redirect(['action' => 'ativacao', $usuario["id"]]);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // $this->loadModel('VwUsuarios');
        // $query = $this->VwUsuarios->find();

        // $novos = $this->VwUsuarios->find();
        // $novosUsuarios = $novos->select(["soma" => $novos->func()->count("id")])->where(["created >= (now() - INTERVAL 80 hour)"])->first();

        // //TOTAIS DE CALCULOS POR MODULOS E POR MES
        // $totais = $query->select([
        //   "calculosSaudePJ" 		=> 	$query->func()->sum("calculos_pme"),
        //   "calculosSaudePF" 		=> 	$query->func()->sum("calculos_pf"),
        //   "calculosOdontoPF" 		=> 	$query->func()->sum("calculos_odonto_pf"),
        //   "calculosOdontoPJ" 		=> 	$query->func()->sum("calculos_odonto_pj"),
        //   "tabelasGeradas" 			=> 	$query->func()->sum("total_tabelas_geradas"),
        //   "tabelasGeradasMes"		=> 	$query->func()->sum("tabelasgeradasmes"),
        //   "calculosMesSaudePJ"		=> 	$query->func()->sum("calculosmes_pme"),
        //   "calculosMesSaudePF"		=> 	$query->func()->sum("calculosmes_pf"),
        //   "calculosMesOdontoPF"		=> 	$query->func()->sum("calculosmes_odontopf"),
        //   "calculosMesOdontoPJ" 	=> 	$query->func()->sum("calculosmes_odontopj")
        // ])->where(["role" => "padrao"])->toArray();

        // $totais = $totais[0];


        $estadosUsers = $this->Users->find('all')->extract('estado_id')->toArray();
        // debug($estadosUsers);die;
        $this->loadModel("Estados");
        $estados = $this->Estados->find("list", ["valueField" => "nome"])->where(["id IN" => $estadosUsers])->toArray();

        // $queryUsers = $this->VwUsuarios->find()->order(["ultimologin" => "DESC"]);
        // $totalusuarios = count($queryUsers->toArray());
        // $this->set('users', $this->paginate($queryUsers));

        $this->set(compact('estados'));
    }

    /**
     * Método de Consulta de Usuários
     *
     * @return \Cake\Network\Response|null
     */
    public function consultaUsuarios()
    {
        $conditions = array();
        if (!empty($this->request->data['status'])) {
            switch ($this->request->data['status']) {
                case "ATIVO":
                    array_push($conditions, ['VwUsuariosAlt.validacao' => "ATIVO", 'VwUsuariosAlt.validacao_email' => "ATIVO", "VwUsuariosAlt.ultimologin >= (now() - INTERVAL 90 DAY)"]);
                    break;
                case "ATIVADO":
                    array_push($conditions, ['VwUsuariosAlt.validacao' => "ATIVO", 'VwUsuariosAlt.validacao_email' => "ATIVO"]);
                    break;
                case "SEM-USO":
                    array_push($conditions, ['VwUsuariosAlt.validacao' => "ATIVO", 'VwUsuariosAlt.validacao_email' => "ATIVO", 'VwUsuariosAlt.ultimologin <= (now() - interval 90 DAY)']);
                    break;
                case "PARCIAL-PENDENTE":
                    array_push($conditions, [
                        'OR' => [
                            'VwUsuariosAlt.validacao' => 'PENDENTE',
                            'VwUsuariosAlt.validacao_email' => 'PENDENTE'
                        ],
                        'NOT' => [
                            'VwUsuariosAlt.validacao' => 'PENDENTE',
                            'VwUsuariosAlt.validacao_email' => 'PENDENTE'
                        ]
                    ]);
                    break;
                case "TOTAL-PENDENTE":
                    array_push($conditions, ['VwUsuariosAlt.validacao' => "PENDENTE", 'VwUsuariosAlt.validacao_email' => "PENDENTE"]);
                    break;
                case "INATIVO":
                    array_push($conditions, ["OR" => ['VwUsuariosAlt.validacao' => "INATIVO", 'VwUsuariosAlt.validacao_email' => "INATIVO"]]);
                    break;
                case "NOVOS":
                    array_push($conditions, ['VwUsuariosAlt.created >=' => Time::now()->modify("-7 days")]);
                    break;
            }
        }
        if (!empty($this->request->data['estado-id'])) {
            array_push($conditions, ['VwUsuariosAlt.estado_id' => $this->request->data['estado-id']]);
        }

        if (!empty($this->request->data['info_busca'])) {
            switch (ctype_alpha(str_replace(" ", "", $this->request->data["info_busca"]))) {
                case true:
                    $conditions = 'VwUsuariosAlt.nome LIKE "%' . $this->request->data['info_busca'] . '%" OR VwUsuariosAlt.sobrenome LIKE "%' . $this->request->data['info_busca'] . '%"';
                    break;
                case false:
                    if (strpos($this->request->data['info_busca'], "@") <> 0) {
                        $conditions = 'VwUsuariosAlt.email LIKE "%' . $this->request->data['info_busca'] . '%"';
                        //                     	debug($conditions);die();
                    }
                    if (ctype_digit($this->request->data['info_busca'])) {
                        $conditions = 'VwUsuariosAlt.username = "' . substr($this->request->data['info_busca'], 0, 3) . '.' . substr($this->request->data['info_busca'], 3, 3) . '.' . substr($this->request->data['info_busca'], 6, 3) . '-' . substr($this->request->data['info_busca'], 9, 2) . '" OR VwUsuariosAlt.celular = "' . '(' . substr($this->request->data['info_busca'], 0, 2) . ') ' . substr($this->request->data['info_busca'], 2, 1) . ' ' . substr($this->request->data['info_busca'], 3, 4) . '-' . substr($this->request->data['info_busca'], 7, 4) . '"';
                    }
                    break;
            }
        }

        //     	debug($conditions);
        $this->loadModel('VwUsuariosAlt');
        // $query = $this->VwUsuariosAlt->find();
        // //TOTAIS DE CALCULOS POR MODULOS E POR MES
        // $totais = $query->select([
        // 	"calculosSaudePJ" 		=> 	$query->func()->sum("calculos_pme"),
        // 	"calculosSaudePF" 		=> 	$query->func()->sum("calculos_pf"),
        // 	"calculosOdontoPF" 		=> 	$query->func()->sum("calculos_odonto_pf"),
        // 	"calculosOdontoPJ" 		=> 	$query->func()->sum("calculos_odonto_pj"),
        // 	"tabelasGeradas" 			=> 	$query->func()->sum("total_tabelas_geradas"),
        // 	"tabelasGeradasMes"		=> 	$query->func()->sum("tabelasgeradasmes"),
        // 	"calculosMesSaudePJ"		=> 	$query->func()->sum("calculosmes_pme"),
        // 	"calculosMesSaudePF"		=> 	$query->func()->sum("calculosmes_pf"),
        // 	"calculosMesOdontoPF"		=> 	$query->func()->sum("calculosmes_odontopf"),
        // 	"calculosMesOdontoPJ" 	=> 	$query->func()->sum("calculosmes_odontopj")
        // 	])->where(["VwUsuariosAlt.role" => "padrao", $conditions])->first();


        // 	$novos = $this->VwUsuariosAlt->find();
        // 	$novosUsuarios = $novos->select(["soma" => $novos->func()->count("id")])->where(["created >= (now() - INTERVAL 80 hour)"])->first();

        $this->loadModel("Estados");
        $estados = $this->Estados->find("list", ["valueField" => "nome"])->where(["id IN" => $this->VwUsuariosAlt->find()->select(["estado_id"])])->toArray();
        if (!empty($conditions)) {
            $queryUsers = $this->VwUsuariosAlt->find()->where([$conditions])->order(["ultimologin" => "DESC"]);
        } else {
            $queryUsers = $this->VwUsuariosAlt->find()->order(["ultimologin" => "DESC"]);
        }
        $totais['calculosSaudePJ'] = 0;
        $totais['calculosSaudePF'] = 0;
        $totais['calculosOdontoPF'] = 0;
        $totais['calculosOdontoPJ'] = 0;
        $totais['tabelasGeradas'] = 0;
        $totais['tabelasGeradasMes'] = 0;
        $totais['calculosMesSaudePJ'] = 0;
        $totais['calculosMesSaudePF'] = 0;
        $totais['calculosMesOdontoPF'] = 0;
        $totais['calculosMesOdontoPJ'] = 0;
        $totais['novos'] = 0;
        $totais['total'] = 0;
        $dif = Time::now();
        foreach ($queryUsers->toArray() as $user) {
            $totais['calculosSaudePJ'] = $totais['calculosSaudePJ'] + $user["calculos_pme"];
            $totais['calculosSaudePF'] = $totais['calculosSaudePF'] + $user["calculos_pf"];
            $totais['calculosOdontoPF'] = $totais['calculosOdontoPF'] + $user["calculos_odonto_pf"];
            $totais['calculosOdontoPJ'] = $totais['calculosOdontoPJ'] + $user["calculos_odonto_pj"];
            $totais['tabelasGeradas'] = $totais['tabelasGeradas'] + $user["total_tabelas_geradas"];
            $totais['tabelasGeradasMes'] = $totais['tabelasGeradasMes'] + $user["tabelasgeradasmes"];
            $totais['calculosMesSaudePJ'] = $totais['calculosMesSaudePJ'] + $user["calculosmes_pme"];
            $totais['calculosMesSaudePF'] = $totais['calculosMesSaudePF'] + $user["calculosmes_pf"];
            $totais['calculosMesOdontoPF'] = $totais['calculosMesOdontoPF'] + $user["calculosmes_odontopf"];
            $totais['calculosMesOdontoPJ'] = $totais['calculosMesOdontoPJ'] + $user["calculosmes_odontopj"];
            if (!empty($user["created"])) {
                $a = date_diff($dif, new Time($user['created']));
                ($a->days <= 4 ? $totais['novos']++ : '');
            }
            $totais['total']++;
        }
        // debug($totais);die;
        // $totalusuarios = count($queryUsers->toArray());
        $this->set('users', $this->paginate($queryUsers));

        $this->set(compact('estados', 'totais'));
    }

    /**
     * Método para buscar Usuários por estado
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function buscaespecial()
    {
        // 	    debug($this->request->data);die();
        $this->loadModel('VwUsuariosAlt');
        $conditions = array();
        switch ($this->request->data['status']) {
            case "ATIVO":
                array_push($conditions, ['VwUsuarios.validacao' => "ATIVO", 'VwUsuarios.validacao_email' => "ATIVO", "VwUsuarios.ultimologin >= (now() - INTERVAL 90 DAY)", "role" => "padrao"]);
                break;
            case "ATIVADO":
                array_push($conditions, ['VwUsuarios.validacao' => "ATIVO", 'VwUsuarios.validacao_email' => "ATIVO", "role" => "padrao"]);
                break;
            case "SEM-USO":
                array_push($conditions, ['VwUsuarios.validacao' => "ATIVO", 'VwUsuarios.validacao_email' => "ATIVO", 'VwUsuarios.ultimologin <= (now() - interval 90 DAY)', "role" => "padrao"]);
                break;
            case "PENDENTE":
                array_push($conditions, ["OR" => ['VwUsuarios.validacao' => "PENDENTE", 'VwUsuarios.validacao_email' => "PENDENTE", "role" => "padrao"]]);
                break;
            case "INATIVO":
                array_push($conditions, ["OR" => ['VwUsuarios.validacao' => "INATIVO", 'VwUsuarios.validacao_email' => "INATIVO"]]);
                break;
        }
        if (!empty($this->request->data['estado-id'])) {
            array_push($conditions, ['VwUsuarios.estado_id' => $this->request->data['estado-id'], "role" => "padrao"]);
        }

        $users = $this->VwUsuarios->find('all')->where([$conditions]);

        /* DADOS PARA ATUALIZAÇÃO DE ESTATÍSTICAS DO ESTADO SELECIONADO */

        // IDS dos Usuários para cálculo de estatísticas
        foreach ($users as $usuario) {
            $ids_usuarios[$usuario["id"]] = $usuario["id"];
        }
        $totalUsuarios = count($users->toArray());

        //TOTAIS DE CALCULOS POR MODULOS E POR MES
        $totais = $this->VwUsuarios->find();
        $totais->select([
            "calculosSaudePJ" => $totais->func()->sum("calculos_pme"),
            "calculosSaudePF" => $totais->func()->sum("calculos_pf"),
            "calculosOdontoPF" => $totais->func()->sum("calculos_odonto_pf"),
            "calculosOdontoPJ" => $totais->func()->sum("calculos_odonto_pj"),
            "tabelasGeradas" => $totais->func()->sum("total_tabelas_geradas"),
            "tabelasGeradasMes" => $totais->func()->sum("tabelasgeradasmes"),
            "calculosMesSaudePJ" => $totais->func()->sum("calculosmes_pme"),
            "calculosMesSaudePF" => $totais->func()->sum("calculosmes_pf"),
            "calculosMesOdontoPF" => $totais->func()->sum("calculosmes_odontopf"),
            "calculosMesOdontoPJ" => $totais->func()->sum("calculosmes_odontopj"),
        ])->where([$conditions]);
        $primeiroDia = '2017-04-20 12:16:16';
        $totais = $totais->toArray();
        $totais = $totais[0];

        $this->set(compact('users', 'totais', 'totalUsuarios', 'primeiroDia'));
    }


    /**
     * Visualizar logotipos dos Usuários
     *
     * @return \Cake\Network\Response|null
     */
    public function logotipos()
    {
        //        $this->loadModel('VwUsuarios');
        $this->paginate = array(
            'order' => array( // sets a default order to sort by
                'Users.imagem_id' => 'DESC'
            )
        );
        $users = $this->paginate($this->Users);

        $query = $this->Users->find('all')->contain(['Imagens'])->where(['Users.imagem_id !=' => 'null']);
        $this->set('users', $this->paginate($query));





        //        $this->set(compact('users'));
        //        $this->set('_serialize', ['users']);
    }
    public function insereLogoDefault($id)
    {
        try {
            $user = $this->Users->get($id);
            $logo = $this->Users->Imagens->get($user->imagem_id);
            $logo->nome = 'default.png';
            $this->Users->Imagens->save($logo);
            $this->redirect(['action' => 'edit', $id]);
        } catch (\Exception $e) {
            $this->Flash->error(__('Erro ao remover Logotipo. Tente novamente.'));
        }
        $this->redirect(['action' => 'edit', $id]);
    }
    /**
     * Visualizar logotipos dos Usuários
     *
     * @return \Cake\Network\Response|null
     */
    public function removerLogo($user_id = null)
    {

        $user = $this->Users->get($user_id);
        $apagarimagem = $user->imagem_id;
        $user->imagem_id = null;
        if ($this->Users->save($user)) {
            $this->loadModel('Imagens');
            $entidade = $this->Imagens->get($apagarimagem);
            unlink(WWW_ROOT . $entidade['caminho'] . $entidade['nome']);
            $session = $this->request->session();
            $session->write([
                'Auth.User.imagem_id' => null
            ]);
            $session->renew();
            $this->Imagens->delete($entidade);
            $this->Flash->success(__('Logo removida com sucesso.'));

            return $this->redirect(['action' => 'logotipos']);
        } else {
            $this->Flash->error(__('Erro ao remover Logotipo. Tente novamente.'));
            return $this->redirect(['action' => 'logotipos']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        if (!empty($sessao["id"])) {
            $this->loadModel('VwUsuarios');
            $user = $this->VwUsuarios->find('all')->where(['VwUsuarios.id' => $sessao["id"]])->first()->toArray();
            if ($user['imagem_id'] != null) {
                $this->loadModel('Imagens');
                $imagem = $this->Imagens->get($user['imagem_id']);
                $this->set(compact('imagem'));
            }
        } else {
            $this->Flash->error(__('Nenhum Usuário logado no sistema.'));
            return $this->redirect(['action' => 'central']);
        }

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Novo Usuário');

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            //            debug($this->request->data);
            //            die();
            $this->request->data['nome'] = ucwords(strtolower($this->request->data['nome']));
            $codigoSms = null;
            $codigoEmail = null;
            for ($i = 0; $i < 8; $i++) {
                $codigoSms .= rand(1, 9); //sorteia 1 numero de 1 a 9
            }
            for ($i = 0; $i < 8; $i++) {
                $codigoEmail .= rand(1, 9); //sorteia 1 numero de 1 a 9
            }
            $this->request->data['codigo'] = $codigoSms;
            $this->request->data['codigo_email'] = $codigoEmail;
            $this->request->data['created'] = Time::now();
            $this->request->data['modified'] = Time::now();
            $this->request->data['validacao'] = "INATIVO";
            $this->request->data['validacao_email'] = "INATIVO";

            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $idusuario = $user->id;
                $this->addOrUpdateUserActiveCampaign($user);

                $email = new Email('default');
                $email->from(['suporte@corretorparceiro.com.br' => 'Cálculo Rápido'])
                    ->to($this->request->data['email'])
                    ->from('suporte@corretorparceiro.com.br', 'Ferramentas de Apoio Corretor Parceiro')
                    ->sender('suporte@corretorparceiro.com.br', 'Ferramentas de Apoio Corretor Parceiro')
                    ->replyTo('suporte@corretorparceiro.com.br')
                    ->emailFormat('html')
                    ->subject('Cadastro de Usuário - Ferramentas de Apoio Corretor Parceiro')
                    ->send(
                        '<html>'
                            . '<img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
                            . '<br/>'
                            . '<br/>'
                            . ''
                            . '<h2>Olá ' . $this->request->data['nome'] . " " . $this->request->data['sobrenome'] . ', Bem Vindo!</h2>'
                            . '<h3>Ativação do E-mail</h3>'
                            . '<br/>'
                            . '<h4>Este é seu código de ativação de e-mail:  <b style="color:red;">' . $codigoEmail . '</b></h4>'
                            . '<br/>'
                            . '<br/>'
                            . '<b>Dados Cadastrais:</b>'
                            . '<b> Nome:</b> ' . $this->request->data['nome'] . " " . $this->request->data['sobrenome']
                            . '<br/>'
                            . '<b> E-mail:</b> ' . $this->request->data['email']
                            . '<br/>'
                            . '<b> Login (CPF):</b> ' . $this->request->data['username']
                            . '<br/>'
                            . '<b> Celular:</b> ' . $this->request->data['celular']
                            . '<br/>'
                            . '<br/>'
                            . '</html>'
                    );

                $this->Flash->success(__('Usuário criado com sucesso. Acesse o sistema com a senha criada e ATIVE seu usuário'));
                return $this->redirect(['controller' => 'Users', 'action' => 'zenvia/Código de Ativação/' . $user['id'] . "/" . $codigoSms . "/ativacao"]);
            }
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);

        $this->set(compact('user', 'estados'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $this->set('title', 'Alterar Usuário');

        $user = $this->Users->get($id, [
            'contain' => ['Estados', 'Imagens']
        ]);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        if ($this->request->is(['patch', 'post', 'put'])) {
            $idusuario = $user->id;

            $usuarioPadrao = false;
            if ($sessao['role'] != 'admin') {
                if ($user->celular <> $this->request->data['celular']) {
                    $usuarioPadrao = true;
                    $user->validacao = 'PENDENTE';
                    $codigo = null;
                    for ($i = 0; $i < 8; $i++) {
                        $codigo .= rand(1, 9); //sorteia 1 numero de 0 a 9
                    }
                    $this->request->data['codigo'] = $codigo;
                }
            }

            $this->request->data['modified'] = Time::now();
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->addOrUpdateUserActiveCampaign($user);

                $this->Auth->setUser($user->toArray());
                if ($sessao['role'] != 'admin') {
                    if ($usuarioPadrao === true) {
                        $mensagem_flash = 'Dados do Usuário alterados com sucesso. Um novo código de ATIVAÇÃO foi enviado para o celular ' . $user['celular'];
                    } else {
                        $mensagem_flash = 'Dados do Usuário alterados com sucesso';
                    }
                    $this->Flash->success(__($mensagem_flash));

                    if ((isset($codigo)) && ($codigo <> null)) {
                        return $this->redirect(['controller' => 'Users', 'action' => 'zenvia/Código Alteração/' . $user['id'] . "/" . $codigo . "/logout"]);
                    }
                } else {
                    $this->Flash->success('Usuário alterado com sucesso.');
                }
            } else {
                $this->Flash->error(__('Erro ao alterar os dados do Usuário. Tente Novamente.'));
            }
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);
        $municipios = $this->Users->Municipios->find('list', ['valueField' => 'nome'])->where(['estado_id' => $user['estado_id']]);
        $this->set(compact('user', 'estados', 'municipios'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {

        $this->loadModel('Simulacoes');
        $simulacoes = $this->Simulacoes->find('list')->where(['Simulacoes.user_id' => $id])->toArray();

        $this->loadModel('PfCalculos');
        $calculosPf = $this->PfCalculos->find('list')->where(['PfCalculos.user_id' => $id])->toArray();

        $this->loadModel('OdontoCalculos');
        $calculosOdonto = $this->OdontoCalculos->find('list')->where(['OdontoCalculos.user_id' => $id])->toArray();

        if (!empty($simulacoes)) {
            $this->loadModel('SimulacoesTabelas');
            $simulacoesTabelas = $this->SimulacoesTabelas->find('list')->where(['SimulacoesTabelas.simulacao_id IN' => $simulacoes]);
            foreach ($simulacoesTabelas as $simulacaoTabela) {
                $entidade = $this->SimulacoesTabelas->get($simulacaoTabela);
                $this->SimulacoesTabelas->delete($entidade);
            }
            $this->loadModel('Filtros');
            $filtros = $this->Filtros->find('list')->where(['Filtros.simulacao_id IN' => $simulacoes]);
            if (!empty($filtros)) {
                $this->loadModel('TabelasFiltradas');

                foreach ($filtros as $filtro) {

                    $tabelasFiltradas = $this->TabelasFiltradas->find('list')->where(['TabelasFiltradas.filtro_id' => $filtro]);
                    if (!empty($tabelasFiltradas)) {
                        foreach ($tabelasFiltradas as $tabelasFiltrada) {
                            $entidadeTabelaFiltrada = $this->TabelasFiltradas->get($tabelasFiltrada);
                            $this->TabelasFiltradas->delete($entidadeTabelaFiltrada);
                        }
                    }

                    $entidadeFiltro = $this->Filtros->get($filtro);
                    $this->Filtros->delete($entidadeFiltro);
                }
            }


            foreach ($simulacoes as $simulacoes) {
                $entidade = $this->Simulacoes->get($simulacoes);
                $this->Simulacoes->delete($entidade);
            }
        }

        if (!empty($calculosPf)) {
            $this->loadModel('PfCalculosDependentes');
            $calculosDependentes = $this->PfCalculosDependentes->find('list')->where(['PfCalculosDependentes.pf_calculo_id IN' => $calculosPf]);
            foreach ($calculosDependentes as $calculoDependente) {
                $entidade = $this->PfCalculosDependentes->get($calculoDependente);
                $this->PfCalculosDependentes->delete($entidade);
            }
            $this->loadModel('PfCalculosTabelas');
            $calculosTabelas = $this->PfCalculosTabelas->find('list')->where(['PfCalculosTabelas.pf_calculo_id IN' => $calculosPf]);
            foreach ($calculosTabelas as $calculoTabela) {
                $entidade = $this->PfCalculosTabelas->get($calculoTabela);
                $this->PfCalculosTabelas->delete($entidade);
            }

            $this->loadModel('PfFiltros');
            $calculoFiltros = $this->PfFiltros->find('list')->where(['PfFiltros.pf_calculo_id IN' => $calculosPf]);

            $this->loadModel('PfFiltrosTabelas');
            $filtrosTabelas = $this->PfFiltrosTabelas->find('list')->where(["PfFiltrosTabelas.pf_filtro_id IN" => $calculoFiltros]);
            foreach ($filtrosTabelas as $filtroTabela) {
                $entidade = $this->PfFiltrosTabelas->get($filtroTabela);
                $this->PfFiltrosTabelas->delete($entidade);
            }

            foreach ($calculoFiltros as $calculoFiltro) {
                $entidade = $this->PfFiltros->get($calculoFiltro);
                $this->PfFiltros->delete($entidade);
            }

            $this->loadModel('PfCalculos');
            $pfCalculos = $this->PfCalculos->find('list')->where(['PfCalculos.id IN' => $calculosPf]);
            foreach ($pfCalculos as $pfCalculo) {
                $entidade = $this->PfCalculos->get($pfCalculo);
                $this->PfCalculos->delete($entidade);
            }
        }
        if (!empty($calculosOdonto)) {
            $this->loadModel('OdontoCalculosTabelas');
            $odontoCalculosTabelas = $this->OdontoCalculosTabelas->find('list')->where(['OdontoCalculosTabelas.odonto_calculo_id IN' => $calculosOdonto]);
            foreach ($odontoCalculosTabelas as $odontoCalculoTabela) {
                $entidade = $this->OdontoCalculosTabelas->get($odontoCalculoTabela);
                $this->OdontoCalculosTabelas->delete($entidade);
            }
            $this->loadModel('OdontoCalculos');
            $odontoCalculos = $this->OdontoCalculos->find('list')->where(['OdontoCalculos.id IN' => $calculosOdonto]);
            foreach ($odontoCalculos as $odontoCalculo) {
                $entidade = $this->OdontoCalculos->get($odontoCalculo);
                $this->OdontoCalculos->delete($entidade);
            }
        }
        //
        $user = $this->Users->get($id);
        // $this->deleteUserActiveCampaign($user);
        //
        if (!$this->Users->delete($user)) {
            $this->Flash->error(__('Erro ao excluir Usuário. Tente novamente.'));
        }
    }

    /**
     * Método para envio de Erro por email
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function erro()
    {
        $dados = $this->request->data;
        if ($dados) {
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $email = new Email('default');
            $email->from(['suporte@corretorparceiro.com.br' => 'Cálculo Rápido'])
                ->to('suporte@corretorparceiro.com.br')
                ->emailFormat('html')
                ->replyTo($sessao['email'])
                ->subject('Sugestão / Erro Informado - Cálculo Rápido')
                ->send('<html><img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
                    . '<br/>'
                    . '<h3>Detalhes do ERRO</h3>'
                    . '<br/>'
                    . '<b> Data:</b> ' . date('d/m/Y')
                    . '<br/>'
                    . '<b> Usuário:</b> ' . $sessao['nome']
                    . '<br/>'
                    . '<b> Email:</b> ' . $sessao['email']
                    . '<br/>'
                    . '<b> Erro Reportado:</b><br/> ' . $dados['mensagem']
                    . '<br/>'
                    . '<br/>'
                    . '</html>');

            $this->Flash->success(__('Sugestão / Erro informado com Sucesso. Em breve entraremos em contato.'));
            return $this->redirect(['controller' => 'Users', 'action' => 'central']);
        }
    }

    /**
     * Ação de Recuperar SENHA com redirecionamento
     */
    public function recuperarSenha($cpf = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cpf = $this->request->data['cpf'];
            $usuario = $this->Users->find('all')->where(['username' => $cpf])->first();
            if ($usuario != null) {
                $senha = null;
                for ($i = 0; $i < 6; $i++) {
                    $senha .= rand(1, 9); //sorteia 1 numero de 1 a 9
                }

                $user = $this->Users->patchEntity($usuario, [
                    'password' => $senha,
                    'senha_provisoria' => 'S'
                ]);


                if ($this->request->_ext === 'json') {

                    $this->loadModel('Smsenviados');
                    $caracteres = array("(", ")", " ", "-");
                    $destinatario = "55" . str_replace($caracteres, "", $usuario['celular']);
                    $ultimosRegistro = $this->Smsenviados
                        ->find('list')
                        ->where(['destinatario' => $destinatario, 'created > DATE_SUB(NOW(), INTERVAL 6 HOUR)'])
                        ->count();
                    $res['ok'] = false;

                    if ($ultimosRegistro == 0) {

                        try {
                            $email = new Email('default');
                            $email->from(['suporte@corretorparceiro.com.br' => 'Ferramentas de Vendas - Corretor Parceiro'])
                                ->to($usuario->email)
                                ->emailFormat('html')
                                ->subject('Senha Provisória - Ferramentas de Vendas')
                                ->send('<html><img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
                                    . '<br/>'
                                    . '<h3>Nova senha Provisória</h3>'
                                    . '<br/>'
                                    . '<b> Data:</b> ' . date('d/m/Y')
                                    . '<br/>'
                                    . '<b> Usuário:</b> ' . $usuario->nome . " " . $usuario->sobrenome
                                    . '<br/>'
                                    . '<h2 style="text-align: center; background-color: #d1d1d1; padding: 10px"><b>Senha Provisória: </b>' . $senha . "</h2>"
                                    . '<br/>'
                                    . 'Ferramentas de Apoio ao Corretor - Boas vendas! https://corretorparceiro.com.br '
                                    . '</html>');
                            $res['email_success'] = true;
                        } catch (\Exception $e) {
                            $res['email_success'] = false;
                            $this->log($e->getMessage());
                        }

                        try {
                            $respostaEnvioSms = $this->Users->zenvia($usuario->id, "Senha Provisoria - " . $senha);
                            $res['sms_success'] = true;
                        } catch (\Exception $e) {
                            $res['sms_success'] = false;
                            $this->log($e->getMessage());
                        }

                        $res['msg'] = 'Senha provisória enviada para o celular cadastrado: ' . $usuario['celular'];
                        $res['ok'] = true;

                        $this->set('respostaEnvioSms', $respostaEnvioSms);

                        if ($respostaEnvioSms->sendSmsResponse->statusCode == "00") {
                            $dadosSMS['destinatario'] = $destinatario;
                            $dadosSMS['nome'] = $usuario['nome'];
                            $dadosSMS['cod_resultado'] = $respostaEnvioSms->sendSmsResponse->statusCode;
                            $dadosSMS['resultado'] = $respostaEnvioSms->sendSmsResponse->detailDescription;
                            $dadosSMS['mensagem'] = "Corretor Parceiro: " . mb_convert_encoding("Senha Provisoria - " . $senha . "Boas Vendas! https://corretorparceiro.com.br", 'utf-8', "auto");
                            $this->Smsenviados->salvar($dadosSMS);
                        }
                        $this->Users->save($user);
                    }
                } else {
                    return $this->redirect(['controller' => 'Users', 'action' => 'zenvia/Senha Provisoria/' . $usuario['id'] . "/" . $senha . "/loginUser"]);
                }
            } else {
                if ($this->request->_ext === 'json') {
                    $respostaEnvioSms = null;
                    $this->set('respostaEnvioSms', $respostaEnvioSms);
                } else {
                    $this->Flash->error(__('CPF não cadastrado.'));
                }
                $res['msg'] = 'CPF não cadastrado.';
                $res['ok'] = false;
            }
            $this->autoRender = false;
            $this->response->type('json');
            $this->response->body(json_encode($res));
        } else {
            $this->set('title', 'Recuperar Senha');
        }
    }

    /**
     * Ação de Recuperar SENHA com redirecionamento
     */
    public function recuperarSenhaAdmin($id = null)
    {
        $usuario = $this->Users->get($id);
        $senha = null;
        for ($i = 0; $i < 6; $i++) {
            $senha .= rand(0, 9); //sorteia 1 numero de 0 a 9
        }
        $user = $this->Users->patchEntity($usuario, [
            'password' => $senha,
            'senha_provisoria' => 'S'
        ]);

        $this->Users->save($user);
        $this->Flash->success(__('Senha provisória enviada para o celular cadastrado: ' . $usuario['celular']));
        return $this->redirect(['controller' => 'Users', 'action' => 'zenvia/Senha Provisoria/' . $usuario['id'] . "/" . $senha . "/index"]);

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Ação de Recuperar SENHA com redirecionamento
     */
    public function recuperarSenhaEmail($id = null)
    {

        $usuario = $this->Users->get($id);
        //        debug($usuario);
        //        die();

        $senha = null;
        for ($i = 0; $i < 6; $i++) {
            $senha .= rand(0, 9); //sorteia 1 numero de 0 a 9
        }
        $user = $this->Users->patchEntity($usuario, [
            'password' => $senha,
            'senha_provisoria' => 'S'
        ]);

        $caracteres = array("(", ")", " ", "-");
        $destinatario = str_replace($caracteres, "", $usuario['celular']);
        $msg = "Senha provisória: " . $senha . " - Ferramentas de Apoio ao Corretor - Boas vendas! https://corretorparceiro.com.br ";

        // $email = new Email('default');
        // $email->from(['suporte@corretorparceiro.com.br' => 'Ferramentas de Apoio - Corretor Parceiro'])
        //     ->to($usuario->email)
        //     ->emailFormat('html')
        //     ->subject('Senha Provisória - Ferramentas de Apoio')
        //     ->send('<html><img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
        //         . '<br/>'
        //         . '<h3>Nova senha Provisória</h3>'
        //         . '<br/>'
        //         . '<b> Data:</b> ' . date('d/m/Y')
        //         . '<br/>'
        //         . '<b> Usuário:</b> ' . $usuario->nome . " " . $usuario->sobrenome
        //         . '<br/>'
        //         . '<h2 style="text-align: center; background-color: #d1d1d1; padding: 10px"><b>Senha Provisória: </b>' . $senha . "</h2>"
        //         . '<br/>'
        //         . 'Ferramentas de Apoio ao Corretor - Boas vendas! https://corretorparceiro.com.br '
        //         . '</html>');

        $this->Users->save($user);
        $this->Flash->success(__('Senha enviada para o email cadastrado: ' . $senha));

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Definir Usuário como Administrador
     */
    public function admin($id = null)
    {


        $user = $this->Users->get($id);


        $user->role = 'admin';
        if ($this->Users->save($user)) {

            $this->Flash->success(__('Usuário definido como Administrador.'));

            return $this->redirect(['controller' => 'tabelas', 'action' => 'index']);
        } else {
            $this->Flash->error(__('Erro ao alterar Usuário. Entre em contato com o Suporte.'));
        }

        //        $this->set('user', $user);
    }

    /**
     * Definir usuário como Padrão
     */
    public function padrao($id = null)
    {


        $user = $this->Users->get($id);


        $user->role = 'padrao';
        if ($this->Users->save($user)) {

            $this->Flash->success(__('Usuário definido como Padrão.'));

            return $this->redirect(['controller' => 'tabelas', 'action' => 'index']);
        } else {
            $this->Flash->error(__('Erro ao alterar Usuário. Entre em contato com o Suporte.'));
        }

        //        $this->set('user', $user);
    }

    /*

     *
     *
     * Método para envio de SMS Individual ao Usuário
     *
     *      */

    public function sms($id = null)
    {
        if (isset($id)) {
            $user = $this->Users->get($id);
            $this->set(compact('user'));
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dados = $this->request->data();
            $this->Flash->success(__('Mensagem enviada para o celular cadastrado: ' . $dados['cel']));
            return $this->redirect(['controller' => 'Users', 'action' => 'zenvia/Corretor Parceiro/' . $dados['id'] . "/" . $dados['mensagem'] . "/index"]);
        }
    }

    /**
     * Adicionar Logotipo do Usuário  method
     *
     * @param string|null $id Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function addImagem($user_id = null)
    {
        $this->set('title', 'Adicionar Logotipo do Usuário');
        $uploadData = '';
        $this->loadModel('Imagens');
        $user = $this->Users->get($user_id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //            debug($this->request->data);
            //            die();
            $user_id = $this->request->data['user_id'];
            if (!empty($this->request->data['file'])) {
                if (
                    $this->request->data['file']['type'] === 'image/png' ||
                    $this->request->data['file']['type'] === 'image/jpeg' ||
                    $this->request->data['file']['type'] === 'image/jpg' ||
                    $this->request->data['file']['type'] === 'image/gif'
                ) {
                    $fileName = $user_id . $this->request->data['file']['name'];
                    $uploadPath = 'uploads/imagens/usuarios/logos/';
                    $uploadFile = $uploadPath . $fileName;

                    if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadFile)) {
                        $uploadData = $this->Imagens->newEntity();
                        $uploadData->nome = $fileName;
                        $uploadData->caminho = $uploadPath;
                        $uploadData->created = date("Y-m-d H:i:s");
                        $uploadData->modified = date("Y-m-d H:i:s");
                        if ($this->Imagens->save($uploadData)) {
                            $idImagem = $uploadData->id;



                            $this->loadModel('Imagens');
                            $imagemSalva = $this->Imagens->get($idImagem);
                            $user->imagem_id = $idImagem;
                            $this->Users->save($user);

                            $session = $this->request->session();
                            //                        debug($session);

                            $session->write([
                                'Auth.User.imagem' => $imagemSalva,
                                'Auth.User.imagem_id' => $idImagem
                            ]);
                            $session->renew();

                            return $this->redirect(['controller' => 'simulacoes', 'action' => 'index']);

                            //                        $this->Flash->success(__('Arquivo enviado e Salvo com sucesso.'));
                        } else {
                            $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                        }
                    } else {
                        $this->Flash->error(__('Erro ao enviar arquivo. Tente Novamente.'));
                    }
                } else {
                    $this->Flash->error(__('Formato de arquivo não permitido. Formatos permitidos são: PNG, JPG, JPEG ou GIF.'));
                }
            } else {
                $this->Flash->error(__('Nenhum arquivo selecionado.'));
            }
        }
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->set('sessao', $sessao);
        $this->set('uploadData', $uploadData);

        $files = $this->Imagens->find('all', ['order' => ['Imagens.created' => 'DESC']]);
        $filesRowNum = $files->count();
        $this->set('files', $files);
        $this->set('filesRowNum', $filesRowNum);

        $this->set(compact('files', 'user', 'filesRowNum', 'user_id'));
        $this->set('_serialize', ['operadora']);
    }

    /*

     *
     * Método para envio de SMS para usuário
     *
     *

     *      */

    public function enviarSms($user_id = null, $msg = null, $redirect = null)
    {

        if (isset($user_id)) {
            $msg = ucwords($msg);
            $this->Flash->success(__('Mensagem enviada para o celular cadastrado: ' . $dados['cel']));
            return $this->redirect(['controller' => 'Users', 'action' => 'zenvia/Corretor Parceiro/' . $user_id . "/" . $msg . "/" . $redirect]);
        }
    }

    /**
     * Método para Enviar email indicando amigo
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function indicar($id = null)
    {


        //        debug($this->request);
        //        die();
        if ($this->request->is(['post', 'put', 'patch'])) {

            $dados = $this->request->data;
            $dados['user_id'] = $dados['id'];

            $usuario = $this->Users->get($dados['id']);
            //            debug($dados);
            //            die();
            //Salva Indicação

            $this->loadModel('Indicados');
            $indicado = $this->Indicados->newEntity();
            $indicado = $this->Indicados->patchEntity($indicado, $dados);
            if ($this->Indicados->save($indicado)) {

                $this->Flash->success(__('Indicação enviada com Sucesso. Obrigado!'));
                //ENVIO DE SMS
                $msg = "Olá " . $dados['nome'] . ", " . $usuario['nome'] . " te indicou para utilizar o Multicálculo Saúde e Odonto. ";
                $caracteres = array("(", ")", " ", "-");

                $json = json_encode([
                    "sendSmsRequest" => [
                        "from" => ucwords("Corretor Parceiro"),
                        "to" => "55" . str_replace($caracteres, "", $dados['celular']),
                        "msg" => mb_convert_encoding($msg . " https://corretorparceiro.com.br/app", 'utf-8', "auto"),
                        "callbackOption" => "FINAL",
                        "aggregateId" => "32807"
                    ]
                ]);
                $ch = curl_init('https://private-anon-d11a4e7efd-zenviasms.apiary-proxy.com/services/send-sms');
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==', 'Accept:application/json']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);

                $result = json_decode($result);

                $this->loadModel('Smsenviados');
                $dadosSMS['destinatario'] = "55" . str_replace($caracteres, "", $dados['celular']);
                $dadosSMS['nome'] = $usuario['nome'];
                $dadosSMS['cod_resultado'] = $result->sendSmsResponse->statusCode;
                $dadosSMS['resultado'] = $result->sendSmsResponse->detailDescription;
                $dadosSMS['mensagem'] = ucwords("Corretor Parceiro") . ": " . mb_convert_encoding($msg . " https://corretorparceiro.com.br/app", 'utf-8', "auto");

                $entidadeSMS = $this->Smsenviados->newEntity();
                $entidadeSMS = $this->Smsenviados->patchEntity($entidadeSMS, $dadosSMS);
                $this->Smsenviados->save($entidadeSMS);

                // /ENVIO DE SMS
            } else {

                $this->Flash->error(__('Erro ao enviar indicação. Tente novamente clique em Avisar Erro.'));
            }

            return $this->redirect(['controller' => 'Users', 'action' => 'central']);
        }
        $user = $this->Users->get($id);
        $this->set(compact('user'));

        //            die();
        //            $this->Flash->error(__('Erro ao reenviar email de ativação. Favor procurar o Suporte.'));
    }

    /**
     * Método para Enviar email indicando amigo
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function enviarformulario($id = null)
    {


        //        debug($this->request);
        //        die();
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $id = $sessao['id'];
        if ($this->request->is(['post', 'put', 'patch'])) {
            $resposta = null;
            $dados = $this->request->data();
            //            debug($dados);die();
            if (isset($dados['email']) && $dados['email'] <> '') {
                $email = new Email('default');
                $email->from(['suporte@corretorparceiro.com.br' => 'Cálculo Rápido - Multicálculo Saúde'])
                    ->to($dados['email'])
                    ->from('suporte@corretorparceiro.com.br', 'Cálculo Rápido')
                    ->sender('suporte@corretorparceiro.com.br', 'Cálculo Rápido')
                    ->replyTo('suporte@corretorparceiro.com.br')
                    ->emailFormat('html')
                    ->subject('Multicálculo Saúde do Corretor Paceiro')
                    ->send('<html><img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
                        . '<br/>'
                        . '<br/>'
                        . ''
                        . '<br/>'
                        . '<h3>Olá <b>' . $dados['nome'] . '</b>!</h3>'
                        . ''
                        . 'Seu corretor acaba de enviar um formulário para realização do seu cálculo.'
                        . '<br/>'
                        . '<br/>'
                        . '<br/>'
                        . '<a href="https://corretorparceiro.com.br/app/users/calculoremoto/' . $sessao['id'] . '"><h2 style="text-align: center;"><b> Responda Aqui</b></h2></a>'
                        . '<br/>'
                        . '<br/>'
                        . '</html>');
                $this->Flash->success(__('Formulário enviado com Sucesso.'));
                $resposta = 'success';
            }
            if (isset($dados['celular']) && $dados['celular'] <> '') {
                $caracteres = array("(", ")", " ", "-");
                $destinatario = str_replace($caracteres, "", $dados['celular']);
                $msg = 'Olá ' . $dados['nome'] . '. Seu corretor acaba de enviar um formulário para realização do seu cálculo.';

                //ENVIO DE SMS

                $destinatario = "55" . $destinatario;

                $json = json_encode([
                    "sendSmsRequest" => [
                        "from" => ucwords("Corretor Parceiro"),
                        "to" => $destinatario,
                        "msg" => mb_convert_encoding($msg . " https://corretorparceiro.com.br/app", 'utf-8', "auto"),
                        "callbackOption" => "FINAL",
                        "aggregateId" => "32807"
                    ]
                ]);
                $ch = curl_init('https://private-anon-d11a4e7efd-zenviasms.apiary-proxy.com/services/send-sms');
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==', 'Accept:application/json']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);

                $result = json_decode($result);

                $this->loadModel('Smsenviados');
                $dadosSMS['destinatario'] = $destinatario;
                $dadosSMS['nome'] = $usuario['nome'];
                $dadosSMS['cod_resultado'] = $result->sendSmsResponse->statusCode;
                $dadosSMS['resultado'] = $result->sendSmsResponse->detailDescription;
                $dadosSMS['mensagem'] = ucwords($remetente) . ": " . mb_convert_encoding($msg . " https://corretorparceiro.com.br", 'utf-8', "auto");

                $entidadeSMS = $this->Smsenviados->newEntity();
                $entidadeSMS = $this->Smsenviados->patchEntity($entidadeSMS, $dadosSMS);
                $this->Smsenviados->save($entidadeSMS);

                // /ENVIO DE SMS

                $resposta = 'success';
            }
            if ($resposta == 'success') {
                $this->Flash->success(__('Formulário enviado com Sucesso.'));
            } else {
                $this->Flash->error(__('Erro ao enviar Formulário. Tente novamente ou clique em Avisar Erro.'));
            }
            return $this->redirect(['controller' => 'Simulacoes', 'action' => 'index']);
        }
        //        $user = $this->Users->get($id);
        $this->set(compact('user', 'sessao', 'form'));

        //            die();
        //            $this->Flash->error(__('Erro ao reenviar email de ativação. Favor procurar o Suporte.'));
    }

    public function enviarIndicacao()
    {

        $this->loadModel('Indicados');
        $indicacoes = $this->Indicados->find('all')->where([array('Indicados.situacao IS null')])->contain(['Users'])->toArray();

        //Envio de SMS
        foreach ($indicacoes as $indicacao) {
            $dados = $this->Indicados->get($indicacao['id']);
            $dados->situacao = "Enviada";
            $this->Indicados->save($dados);

            $caracteres = array("(", ")", " ", "-");
            $destinatario = str_replace($caracteres, "", $indicacao['celular']);
            $msg = $indicacao['nome'] . ', ' . $indicacao['user']['nome'] . ' te convidou para experimentar grátis o Cotador On-line Saude e Odonto.';


            //ENVIO DE SMS

            $caracteres = array("(", ")", " ", "-");
            $destinatario = "55" . $destinatario;

            $json = json_encode([
                "sendSmsRequest" => [
                    "from" => ucwords("Corretor Parceiro"),
                    "to" => "55" . str_replace($caracteres, "", $dados['user']['celular']),
                    "msg" => mb_convert_encoding($msg . " https://corretorparceiro.com.br/app", 'utf-8', "auto"),
                    "callbackOption" => "FINAL",
                    "aggregateId" => "32807"
                ]
            ]);
            $ch = curl_init('https://private-anon-d11a4e7efd-zenviasms.apiary-proxy.com/services/send-sms');
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==', 'Accept:application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $result = json_decode($result);

            $this->loadModel('Smsenviados');
            $dadosSMS['destinatario'] = $destinatario;
            $dadosSMS['nome'] = $usuario['nome'];
            $dadosSMS['cod_resultado'] = $result->sendSmsResponse->statusCode;
            $dadosSMS['resultado'] = $result->sendSmsResponse->detailDescription;
            $dadosSMS['mensagem'] = ucwords($remetente) . ": " . mb_convert_encoding($msg . " https://corretorparceiro.com.br", 'utf-8', "auto");

            $entidadeSMS = $this->Smsenviados->newEntity();
            $entidadeSMS = $this->Smsenviados->patchEntity($entidadeSMS, $dadosSMS);
            $this->Smsenviados->save($entidadeSMS);

            // /ENVIO DE SMS
            //Envio de Email
            $email = new Email('default');
            $email->from(['suporte@corretorparceiro.com.br' => 'Cálculo Rápido - Multicálculo Saúde'])
                ->to($indicacao['email'])
                ->from('suporte@corretorparceiro.com.br', 'Cálculo Rápido')
                ->sender('suporte@corretorparceiro.com.br', 'Cálculo Rápido')
                ->replyTo('suporte@corretorparceiro.com.br')
                ->emailFormat('html')
                ->subject('Multicálculo Saúde do Corretor Paceiro')
                ->send('<html><img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
                    . '<br/>'
                    . '<br/>'
                    . ''
                    . '<br/>'
                    . '<h3>Olá <b>' . $indicacao['nome'] . '</b>!</h3>'
                    . ''
                    . $indicacao['user']['nome'] . ' ' . $indicacao['user']['sobrenome'] . ' te convidou para experimentar grátis o Cotador on-line de Saúde Cálculo Rápido do Corretor Parceiro.'
                    . '<br/>'
                    . '<br/>'
                    . 'Faça as cotações para seus clientes em todos os Planos de Saúde em menos de 10 segundos.'
                    . '<br/>'
                    . '<a href="https://corretorparceiro.com.br/app/users/add"><h2 style="text-align: center;"><b> Acesse e cadastre-se grátis!</b></h2></a>'
                    . '<br/>'
                    . '<br/>'
                    . '</html>');
        }
        $this->Flash->success(__('Indicação enviada com Sucesso. Obrigado!'));
        return $this->redirect(['controller' => 'Users', 'action' => 'login']);
    }

    public function enviarCalculoRemoto()
    {
        $this->loadModel('Simulacoes');
        $simulacoes = $this->Simulacoes->find('all')->where([array('Simulacoes.calculoremoto' => '1')])->toArray();
        //Envio de SMS
        foreach ($simulacoes as $simulacao) {
            //            $dados = $this->Simulacoes->find('all')->where([array('Simulacoes.id' => $simulacao['id'])])->contain(['Users'])->first()->toArray();

            $dados = $this->Simulacoes->get($simulacao['id'], [
                'contain' => ['Users']
            ]);


            $dados->calculoremoto = "2";
            $this->Simulacoes->save($dados);

            $caracteres = array("(", ")", " ", "-");
            $msg = 'Novo cálculo solicitado. Responda o quanto antes ao seu cliente. https://corretorparceiro.com.br/app';



            //ENVIO DE SMS

            $caracteres = array("(", ")", " ", "-");
            $destinatario = "55" . $destinatario;

            $json = json_encode([
                "sendSmsRequest" => [
                    "from" => ucwords("Corretor Parceiro"),
                    "to" => "55" . str_replace($caracteres, "", $dados['user']['celular']),
                    "msg" => mb_convert_encoding($msg . " https://corretorparceiro.com.br", 'utf-8', "auto"),
                    "callbackOption" => "FINAL",
                    "aggregateId" => "32807"
                ]
            ]);
            $ch = curl_init('https://private-anon-d11a4e7efd-zenviasms.apiary-proxy.com/services/send-sms');
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==', 'Accept:application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $result = json_decode($result);

            $this->loadModel('Smsenviados');
            $dadosSMS['destinatario'] = $destinatario;
            $dadosSMS['nome'] = $usuario['nome'];
            $dadosSMS['cod_resultado'] = $result->sendSmsResponse->statusCode;
            $dadosSMS['resultado'] = $result->sendSmsResponse->detailDescription;
            $dadosSMS['mensagem'] = ucwords($remetente) . ": " . mb_convert_encoding($msg . " https://corretorparceiro.com.br", 'utf-8', "auto");

            $entidadeSMS = $this->Smsenviados->newEntity();
            $entidadeSMS = $this->Smsenviados->patchEntity($entidadeSMS, $dadosSMS);
            $this->Smsenviados->save($entidadeSMS);

            // /ENVIO DE SMS
            //Envio de Email
            $email = new Email('default');
            $email->from(['suporte@corretorparceiro.com.br' => 'Cálculo Rápido - Multicálculo Saúde'])
                ->to($dados['user']['email'])
                ->from('suporte@corretorparceiro.com.br', 'Cálculo Rápido')
                ->sender('suporte@corretorparceiro.com.br', 'Cálculo Rápido')
                ->replyTo('suporte@corretorparceiro.com.br')
                ->emailFormat('html')
                ->subject('Multicálculo Saúde do Corretor Paceiro')
                ->send('<html><img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
                    . '<br/>'
                    . '<br/>'
                    . ''
                    . '<br/>'
                    . '<h3>Olá <b>' . $dados['user']['nome'] . '</b>!</h3>'
                    . ''
                    . 'Você acaba de receber uma solicitação de cálculo. Verifique seu histórico de cálculos e responda o quanto antes ao seu cliente.'
                    . '<br/>'
                    . '<br/>'
                    . 'Faça as cotações para seus clientes em todos os Planos de Saúde em menos de 10 segundos.'
                    . '<br/>'
                    . '<a href="https://corretorparceiro.com.br/app"><h2 style="text-align: center;"><b> Responda Aqui</b></h2></a>'
                    . '<br/>'
                    . '<br/>'
                    . '</html>');


            $this->viewBuilder()->layout('calculoremoto');
            //            $this->viewBuilder()->layout('calculoremoto');
        }
    }

    /*

     *
     *
     * Método para envio de SMS Individual ao Usuário
     *
     *      */

    public function enviarEmail($id = null)
    {
        if (isset($id)) {
            $user = $this->Users->get($id);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dados = $this->request->data();
            $user = $this->Users->get($dados['id']);
            //            debug($dados);
            //            die();

            $msg = $dados['mensagem'];
            //Envio de Email
            $email = new Email('default');
            $email->from(['suporte@corretorparceiro.com.br' => 'Cálculo Rápido - Multicálculo Saúde'])
                ->to($user['email'])
                ->from('suporte@corretorparceiro.com.br', 'Cálculo Rápido')
                ->sender('suporte@corretorparceiro.com.br', 'Cálculo Rápido')
                ->replyTo('suporte@corretorparceiro.com.br')
                ->emailFormat('html')
                ->subject('Multicálculo Saúde do Corretor Paceiro')
                ->send('<html><img src="https://corretorparceiro.com.br/app/img/logo.png" width="200"/>'
                    . '<br/>'
                    . '<br/>'
                    . ''
                    . '<br/>'
                    . '<h3>Olá <b>' . $user['nome'] . '</b>!</h3>'
                    . ''
                    . '<br>'
                    . nl2br($msg)
                    . '<br/>'
                    . '<br/>'
                    . '<br/>'
                    . '</html>');

            $this->set(compact('dados'));
        }
        $this->set(compact('user'));
    }

    /**
     * Método para preenchimento de cálculo pelo cliente. Informações serão armazenadas no Histórico de Cálculos.
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function calculoremoto($user_id = null)
    {
        $user = $this->Users->find('all')->contain(['Imagens'])->where(['Users.id' => $user_id])->first()->toArray();
        $this->loadModel('Simulacoes');

        $userId = $user['id'];
        $simulacao = $this->request->data();
        $this->loadModel('Ramos');
        $ramos = $this->Ramos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'DESC']]);
        //        $tipos_produtos = $this->TiposProdutos->find('list', ['valueField' => 'nome', 'order' => ['nome' => 'ASC']]);

        if ($this->request->is('post')) {
            //            debug($this->request->data());
            //            die();
            //        Transformando 0 vidas em null para armazenar no Banco;
            $i = 1;
            for ($i = 1; $i <= 12; $i++) {
                if ($simulacao['faixa' . $i] === 0) {
                    $simulacao['faixa' . $i] = null;
                }
            }


            if ($simulacao['tipo_cnpj_id'] == null) {

                if ($simulacao['tipo_cnpj_id'] == null) {
                    $campoVazio = $campoVazio . 'Tipo de CNPJ';
                }
                $this->Flash->error(__('Campo ' . $campoVazio . ' é Obrigatório.'));
                return $this->redirect(['action' => 'add']);
            } else {
                $this->loadModel('Tabelas');
                $i = 1;
                $totalVidas = 0;
                for ($i = 1; $i <= 12; $i++) {
                    $totalVidas = $totalVidas + (int) $simulacao['faixa' . $i];
                }


                $this->loadModel('Operadoras');
                $operadoras = $this->Operadoras->find('all', ['contain' => ['Imagens']]);

                $tabelas = $this->Tabelas->find('all', ['contain' => ['Ramos', 'Abrangencias', 'Tipos', 'Produtos' => ['Carencias', 'Redes', 'Observacoes', 'Informacoes', 'Opcionais', 'Reembolsos'], 'Operadoras', 'Modalidades']], ['order' => ['Operadoras.nome' => 'ASC']], ['group' => ['Tabelas.operadora_id']])
                    ->order([
                        'Operadoras.prioridade' => 'ASC',
                        'Tabelas.prioridade' => 'ASC'
                    ])
                    ->where([
                        'Tabelas.ramo_id' => $simulacao['ramo_id'],
                        'Tabelas.validade' => 1,
                        'Tabelas.estado_id' => $simulacao['estado_id'],
                        'Tabelas.modalidade_id' => $simulacao['modalidade_id'],
                        'Tabelas.minimo_vidas <=' => $totalVidas,
                        'Tabelas.tipo_cnpj' => $simulacao['tipo_cnpj_id'],
                        'Tabelas.maximo_vidas >=' => $totalVidas
                    ])->toArray();





                if ($tabelas) {
                    //                debug($simulacao);
                    $simulacao = $this->Simulacoes->newEntity();
                    $simulacao = $this->Simulacoes->patchEntity($simulacao, $this->request->data);
                    $simulacao['user_id'] = $userId;
                    $simulacao['calculoremoto'] = '1';
                    //                debug($simulacao);
                    //                die();
                    if ($this->Simulacoes->save($simulacao)) {
                        $this->loadModel('Users');
                        $userLogado = $this->Users->get($simulacao['user_id']);

                        $userLogado->ultimocalculo = Time::now();
                        $userLogado->dirty('ultimocalculo', true);
                        $this->Users->save($userLogado);

                        $idSimulacao = $simulacao->id;
                        $this->loadModel('Tabelas');

                        //REMOÇÃO DE TABELAS BRADESCO DE ACORDO COM SELEÇÃO DE GRUPO FAMILIAR OU NÃO
                        $bradesco = array(1, 28, 29, 30, 31);
                        if ($simulacao['familia'] == 1) {
                            foreach ($tabelas as $chave => $tabela) {
                                if (in_array($tabela['operadora_id'], $bradesco) && $tabela['minimo_vidas'] == 4 && $tabela['maximo_vidas'] == 29) {
                                    unset($tabelas[$chave]);
                                }
                            }
                        } else {
                            if ($totalVidas >= 4) {
                                foreach ($tabelas as $chave => $tabela) {
                                    if (in_array($tabela['operadora_id'], $bradesco) && $tabela['minimo_vidas'] == 3 && $tabela['maximo_vidas'] == 29) {
                                        unset($tabelas[$chave]);
                                    }
                                }
                            }
                        }
                        //REMOÇÃO DE TABELAS SULAMÉRICA DE ACORDO COM TIPO DE CNPJ SELECIONADO
                        foreach ($tabelas as $chave => $tabela) {
                            if ($tabela['operadora_id'] == 16) {
                                if ($tabela['tipo_cnpj'] <> $simulacao['tipo_cnpj_id']) {
                                    unset($tabelas[$chave]);
                                }
                            }
                        }

                        $this->loadModel('SimulacoesTabelas');
                        foreach ($tabelas as $idTabela) {
                            $dados['simulacao_id'] = $idSimulacao;
                            $dados['tabela_id'] = $idTabela['id'];
                            $this->loadModel('SimulacoesTabelas');
                            $simulacoesTabelas = $this->SimulacoesTabelas->newEntity();
                            $simulacoesTabelas = $this->SimulacoesTabelas->patchEntity($simulacoesTabelas, $dados);
                            $this->SimulacoesTabelas->save($simulacoesTabelas);
                        }
                        $this->Flash->success(__('Enviado com sucesso!'));
                        return $this->redirect(['action' => 'obrigado', $userId]);
                    } else {
                        $this->Flash->error(__('Erro ao Salvar Simulação. Tente Novamente'));
                    }
                }
            }
        }
        $btnCancelar = '#';
        $this->set(compact('tabelas', 'ramos', 'simulacao', 'user', 'userId', 'tabelasOrdenadas', 'btnCancelar', 'operadoras'));
        //        $this->render('simulacao');
        $this->viewBuilder()->layout('calculoremoto');
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function obrigado($user_id = null)
    {
        $user = $this->Users->get($user_id, ['contain' => ['Imagens']]);
        $this->set(compact('user'));
        //        $this->viewBuilder()->layout('calculoremoto');
    }

    /**
     * Método que adiciona usuários a LISTA do Mailchimp Cálculo Rápido ID = 4d69a8297e
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function mailchimp($file = null)
    {

        $email = 'rodrigosouza_ba@yahoo.com.br';
        $fname = 'Rodrigo';
        $lname = 'Souza';
        $aniversario = '11/24';
        //        $lista = file(WWW_ROOT . "/xaa.txt");
        $lista = file(WWW_ROOT . "/" . $file . ".txt");
        //        debug(count($lista));
        //        debug($lista);
        //
        //        die();

        $listID = 'ee42bd6d30'; // Geral

        $grupo = '8c02a6aaed'; // Natuseg Ativos
        //        $grupo = 'c81fff0722'; // Ferramentas de Apoio - Código do GRUPO!!!
        //        $grupo = '1dd8fbd2a5'; // Natuseg Inativos
        //        $grupo = '24c3657c29'; // Natuseg Bloqueados
        //        $grupo = 'b8be87ae2d'; // Corretor Parceiro Ativos
        //        $grupo = '5a7503e12c'; // Corretor Parceiro Inativos
        //        $grupo = 'fdda6f1c5f'; // Corretor Parceiro Bloqueados
        //        $grupo = '835c25f778'; // Corretor Parceiro Prospects
        //        $grupo = '3961bd91ca'; // Corretor Parceiro News
        // MailChimp API URL
        //        $memberID = md5(strtolower($email));
        $dataCenter = substr($this->apiKey, strpos($this->apiKey, '-') + 1);

        //        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/';

        foreach ($lista as $item) {

            $item = explode(";", $item);
            //            "interests":{"760457":"TRUE"}}
            // member information
            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . md5(strtolower($item[0]));
            $json = json_encode([
                'email_address' => str_replace('"', '', $item[0]),
                'status' => 'subscribed',
                'merge_fields' => [
                    'NOME' => str_replace('"', '', $item[1]),
                    'NPREFERIDO' => str_replace('"', '', $item[2]),
                    'PESSOA' => str_replace('"', '', $item[3]),
                    'CEL' => str_replace('"', '', $item[4]),
                    'UF' => str_replace('"', '', $item[5]),
                    'SUSEP' => str_replace('"', '', $item[5])
                ]
                //                ,
                //                'interests' => [
                //                    "8c02a6aaed" => TRUE
                //                ]
            ]);

            //            debug($json);
            //            die();
            // send a HTTP POST request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $this->apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            debug($result);
            //            die();
            // Resultado do POST para o MAILCHIMP
            // store the status message based on response code
            if ($httpCode == 200) {
                $_SESSION['msg'] = '<p style="color: #34A853">You have successfully subscribed to CodexWorld.</p>';
            } else {
                switch ($httpCode) {
                    case 214:
                        $msg = 'You are already subscribed.';
                        break;
                    default:
                        $msg = 'Some problem occurred, please try again.';
                        break;
                }
                $_SESSION['msg'] = '<p style="color: #EA4335">' . $msg . '</p>';
            }
        }
        $this->set(compact('result'));
    }

    /**
     * Método que adiciona usuários a LISTA do Mailchimp Cálculo Rápido ID = 4d69a8297e
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function mailchimpferramentasapoio()
    {
        // GET /lists/{list_id}/interest-categories/{interest_category_id}/interests

        $listID = 'ee42bd6d30';
        $dataCenter = substr($this->apiKey, strpos($this->apiKey, '-') + 1);

        //Grupos MAILCHIMP
        /*
			Ferramentas = c81fff0722;
			Z ANTIGO - Natuseg - Inativos = 1dd8fbd2a5;
			Z ANTIGO - Natuseg - Ativos = 8c02a6aaed;
			Z ANTIGO - Natuseg - Bloqueados = 24c3657c29;
			Z ANTIGO - Corpar = b8be87ae2d;
			Z ANTIGO - Corpar - Bloqueados = fdda6f1c5f;
			Z ANTIGO - Corpar - Prospects = 835c25f778;
			Z ANTIGO - Corpar - News = 3961bd91ca;
			Z ANTIGO - Prospects - Qualicorp = ba5e8e707a;
			TODOS BA = 1f9b10bbad;
			TODOS ES = c238c953c3;
			TODOS AM = 58678d9cdd;
			TODOS MG = c9a9e2bbc2;
			TODOS RN = dc9e0f8472;
			TODOS SUSEP = 3bcf0ef8f8;
			CORRETORES ASSESSORIA = 4653094540;
			NATUSEG ATIVOS = 0b34c537ce;
			NATUSEG INATIVOS = 2dbceb5a19;
		*/

        //Leitura de arquivo CSV
        $lista = file(WWW_ROOT . "/completa.csv");
        foreach ($lista as $row) {
            $row = explode(";", $row);
            // debug($row[7]);

            /**
             * 0 = nome
             * 1 = email
             * 2 = UF
             * 3 = Tipo Pessoa
             * 4 = Susep tipo
             * 5 = Assessoria
             * 6 = Natuseg Ativo
             * 7 = Natuseg Inativo
             */
            $i = 0;
            for ($i = 0; $i <= 7; $i++) {
                $row[$i] = trim($row[$i]);
            }
            $interests = array();

            //Adicionando no GRUPO dos ESTADOS correspondente
            switch ($row[2]) {
                case "BA":
                    $interests["1f9b10bbad"] = true;
                    break;
                case "ES":
                    $interests["c238c953c3"] = true;
                    break;
                case "AM":
                    $interests["58678d9cdd"] = true;
                    break;
                case "MG":
                    $interests["c9a9e2bbc2"] = true;
                    break;
                case "RN":
                    $interests["dc9e0f8472"] = true;
                    break;
            }

            //Tipo SUSEP diferete de 0 adiciona grupo TODOS SUSEP
            if ($row[4] <> 0) {
                $interests["3bcf0ef8f8"] = true;
            }

            //Assessoria Verdadeiro
            if ($row[5] == "VERDADEIRO") {
                $interests["4653094540"] = true;
            }

            //Natuseg Ativo Verdadeiro
            if ($row[6] == "VERDADEIRO") {
                $interests["0b34c537ce"] = true;
            }

            //Natuseg Inativo Verdadeiro
            if ($row[7] == "VERDADEIRO") {
                $interests["2dbceb5a19"] =  true;
            }

            // member information
            $json = json_encode([
                'email_address' => str_replace('"', '', $row[1]),
                'status' => 'subscribed',
                'merge_fields' => [
                    'NOME' => str_replace('"', '', $row[0])
                ],
                'interests' => $interests,
            ]);

            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . md5(strtolower($row[1]));

            // send a HTTP POST request with curl
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $this->apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            // debug(json_decode($result));
            $a = json_decode($result);
            // debug($a);
            echo $a->status . " <br> " . (isset($a->detail) ? $a->detail : "OK");
            echo "<br>";
            // die;
        }
    }

    /**
     * Método que remove usuários a LISTA do Mailchimp Cálculo Rápido ID = 4d69a8297e
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function atualizarmailchimp()
    {

        $email = 'rodrigosouza_ba@yahoo.com.br';
        $fname = 'Rodrigo';
        $lname = 'Souza';
        $aniversario = '11/24';

        $listID = '4d69a8297e';

        // MailChimp API URL
        $memberID = md5(strtolower($email));

        $dataCenter = substr($this->apiKey, strpos($this->apiKey, '-') + 1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;


        // member information
        //        $json = json_encode([
        //            'email_address' => $email,
        //            'status' => 'subscribed',
        //            'merge_fields' => [
        //                'FNAME' => $fname,
        //                'LNAME' => $lname,
        //                'NIVER' => $aniversario
        //            ]
        //        ]);
        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $this->apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // Resultado do POST para o MAILCHIMP
        //         debug($result);
        // store the status message based on response code
        if ($httpCode == 200) {
            $_SESSION['msg'] = '<p style="color: #34A853">You have successfully subscribed to CodexWorld.</p>';
        } else {
            switch ($httpCode) {
                case 214:
                    $msg = 'You are already subscribed.';
                    break;
                default:
                    $msg = 'Some problem occurred, please try again.';
                    break;
            }
            $_SESSION['msg'] = '<p style="color: #EA4335">' . $msg . '</p>';
        }
        $this->set(compact('result'));
    }

    public function allowedsFunctions($user)
    {
        $alloweds = [];

        $this->loadModel('UsersGrupos');
        $grupos = $this->UsersGrupos
            ->find('list', ['keyField' => 'grupo_id', 'valueField' => 'grupo_id'])
            ->where(['user_id' => $user])
            ->toArray();
        if (!empty($grupos)) {
            $this->loadModel('Grupos');
            $grupos_pai = $this->Grupos
                ->find('list', ['valueField' => 'grupo_pai_id'])
                ->where(['id IN' => $grupos])
                ->toArray();

            $grupos =  array_merge($grupos_pai, $grupos);
            if (!empty($grupos)) {
                $this->loadModel('GruposFuncionalidades');
                $alloweds = $this->GruposFuncionalidades->find('list', ['valueField' => 'funcionalidade_id'])->where(['grupo_id IN' => $grupos])->toArray();
            }
        }
        return $alloweds;
    }

    public function central()
    {
        $session = $this->request->session();

        $menu1 = $session->read('Auth.menu1');

        //PRODUÇÃO
        // $url = WEB_ROOT . "/app/users/checkSession.json";
        // debug($_SERVER['REQUEST_URI']);
        //LOCAL
        // $url = "http://localhost/Ferramentas/users/checkSession.json";

        // $ch = curl_init($url);

        // curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==', 'Accept:application/json']);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        // $response = curl_exec($ch);
        // // 	        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // curl_close($ch);
        // $response = json_decode($response);

        // $http = new Client();
        // Simple get
        // $response = $http->get($url);

        //começo sessions

        // Pegando o id da sessao
        // $this->loadModel('Sessions');

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $id_sessao = $this->request->session()->id();
        $menuSession = $session->read('Auth.menu');

        $connection = ConnectionManager::get('default');
        // Pegando id do usuario logado
        $user_id = $this->Auth->user('id');
        $put_user_id = $connection->newQuery();
        $put_user_id->update('sessions')
            ->set([
                'user_id' => $user_id
            ])
            ->where([
                'id' => $id_sessao
            ]);
        $stmt = $put_user_id->execute();
        //fim sessions


        //INICIO DOS ESTADOS PARA PEQSUISA DE REDE CREDENCIADA
        $this->loadModel('Estados');
        $estado = $this->Estados->find('list', ['keyField' => 'id', "valueField" => "nome"]);
        //FIM DOS ESTADOS PARA PEQSUISA DE REDE CREDENCIADA

        // $session = $this->request->session();
        // $sessao = $session->read('Auth.User');
        $userId = $session->read('Auth.User.id');

        // verificar se ha necessidade

        // $this->LoadModel('Operadoras');
        // $estadoPme = $this->Operadoras->find("list", ['valueField' => 'estado_id']);
        // $estadoPme = $estadoPme->toArray();
        // $this->LoadModel('PfOperadoras');
        // $estadoPf = $this->PfOperadoras->find("list", ['valueField' => 'estado_id']);
        // $estadoPf = $estadoPf->toArray();
        // $estadoPesquisa = array_merge($estadoPme, $estadoPf);
        // $estadoPesquisa = $this->Estados->find('list', ['valueField' => 'nome'])->where(['id IN' => $estadoPesquisa]);

        $this->loadModel('BannerImagens');
        $banners = $this->BannerImagens->find('all')->toArray();
        $this->loadModel('VendasOnlines');
        $vendasOnlines = $this->VendasOnlines->find('all')->where(['exibir' => 1])->orderASC('ordem')->toArray();

        $estados = $this->Users->Estados->find('list', ['keyField' => 'id', 'valueField' => 'nome']);
        $municipios = $this->Users->Municipios->find('list', ['keyField' => 'id', 'valueField' => 'nome'])->where(['estado_id' => $sessao['estado_id']]);


        // PERMISSÃO POR GRUPOS AS FUNÇÕES
        if (empty($session->read('Auth.alloweds_functions'))) {
            $alloweds_functions = $this->allowedsFunctions($userId);
            $session->write([
                'Auth.alloweds_functions' => $alloweds_functions
            ]);
        } else {
            $alloweds_functions = $session->read('Auth.alloweds_functions');
        }

        // FIM PERMISSÃO POR GRUPOS AS FUNÇÕES
        $this->set(compact('sessao', 'menuSession', 'estadoPesquisa', 'banners', 'alloweds_functions', 'vendasOnlines', 'estados', 'municipios'));
    }

    public function saude()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->set(compact('sessao'));
    }

    public function odonto()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->set(compact('sessao'));
    }

    public function ajusteautomatico()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->loadModel('AgendamentosTabelas');
        $pendentes = $this->AgendamentosTabelas->find('all')
            ->where(['Agendamentos.situacao' => 'PENDENTE', 'Agendamentos.data_alteracao <=' => date('Y-m-d H:i:s')])
            ->contain(['Agendamentos'])->toArray();

        if (!empty($pendentes)) {
            $this->loadModel('Tabelas');
            foreach ($pendentes as $pendente) {
                $tabela = $this->Tabelas->get($pendente['tabela_id']);
                $tabela->faixa1 = round($tabela->faixa1 + $tabela->faixa1 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa2 = round($tabela->faixa2 + $tabela->faixa2 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa3 = round($tabela->faixa3 + $tabela->faixa3 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa4 = round($tabela->faixa4 + $tabela->faixa4 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa5 = round($tabela->faixa5 + $tabela->faixa5 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa6 = round($tabela->faixa6 + $tabela->faixa6 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa7 = round($tabela->faixa7 + $tabela->faixa7 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa8 = round($tabela->faixa8 + $tabela->faixa8 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa9 = round($tabela->faixa9 + $tabela->faixa9 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa10 = round($tabela->faixa10 + $tabela->faixa10 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa11 = round($tabela->faixa11 + $tabela->faixa11 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->faixa12 = round($tabela->faixa12 + $tabela->faixa12 * $pendente['agendamento']['percentual'] / 100, 2);
                $tabela->vigencia = $pendente['agendamento']['nova_vigencia'];
                if ($this->Tabelas->save($tabela)) {
                    $status = 'OK';
                } else {
                    $status = 'ERRO';
                    break;
                }
            }
            $agendamento = $pendente['agendamento']['id'];
            $this->loadModel('Agendamentos');
            $agendamento = $this->Agendamentos->get($agendamento);
            if ($status <> 'ERRO') {
                $agendamento->situacao = 'EXECUTADO';
            } else {
                $agendamento->situacao = 'ERRO AO EXECUTAR';
            }
            $this->Agendamentos->save($agendamento);
        }
        $this->viewBuilder()->layout('ajax');
    }

    public function zenvia($remetente = null, $id = null, $msg = null, $redirect = null)
    {
        $usuario = $this->Users->get($id);
        $caracteres = array("(", ")", " ", "-");
        $destinatario = "55" . str_replace($caracteres, "", $usuario['celular']);

        $json = json_encode([
            "sendSmsRequest" => [
                "from" => ucwords($remetente),
                "to" => $destinatario,
                "msg" => mb_convert_encoding($msg . ". Boas vendas! https://corretorparceiro.com.br", 'utf-8', "auto"),
                "callbackOption" => "FINAL",
                "aggregateId" => "32807"
            ]
        ]);

        $ch = curl_init('https://api-rest.zenvia.com/services/send-sms');

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==', 'Accept:application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = json_decode($result);

        $this->loadModel('Smsenviados');
        $dadosSMS['destinatario'] = $destinatario;
        $dadosSMS['nome'] = $usuario['nome'];
        $dadosSMS['cod_resultado'] = $result->sendSmsResponse->statusCode;
        $dadosSMS['resultado'] = $result->sendSmsResponse->detailDescription;
        $dadosSMS['mensagem'] = ucwords($remetente) . ": " . mb_convert_encoding($msg . " https://corretorparceiro.com.br", 'utf-8', "auto");

        $entidadeSMS = $this->Smsenviados->newEntity();
        $entidadeSMS = $this->Smsenviados->patchEntity($entidadeSMS, $dadosSMS);
        $this->Smsenviados->save($entidadeSMS);

        if ($redirect === 'ativacao') {
            return $this->redirect(['action' => 'ativacao', $id]);
        } else {
            return $this->redirect(['action' => $redirect]);
        }
    }

    /**
     * Ação de Recuperar SENHA com redirecionamento
     */
    public function enviarArquivos()
    {

        if ($this->request->is(['patch', 'post', 'put'])) {
            $email = $this->request->data();
            // debug();
            // die();
            $user = $this->Users->get($id);
            $cpf = $this->request->data['cpf'];
            $usuario = $this->Users->find('all')->where(['username' => $cpf])->first();
            $senha = null;
            for ($i = 0; $i < 6; $i++) {
                $senha .= rand(0, 9); //sorteia 1 numero de 0 a 9
            }

            $user = $this->Users->patchEntity($usuario, [
                'password' => $senha,
                'validacao' => 'ATIVO',
                'senha_provisoria' => 'S'
            ]);
            //            debug($usuario['id']);
            //            die();
            $this->Users->save($user);


            $this->Flash->success(__('Senha provisória enviada para o celular cadastrado: ' . $usuario['celular']));
            return $this->redirect(['controller' => 'Users', 'action' => 'zenvia/Senha Provisoria/' . $usuario['id'] . "/" . $senha . "/loginUser"]);
        }
        $this->loadModel('OperadorasFechadas');
        $operadoras_fechadas = $this->OperadorasFechadas->find('list', ['valueField' => 'nome'])->toArray();


        $this->set('title', 'Recuperar Senha');
        $this->set(compact('operadoras_fechadas'));


        $this->viewBuilder()->layout('ajax');
    }

    /*

	    Central de SMS

    */
    public function smsmarketing()
    {
    }

    public function gerenciador()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->set(compact('sessao'));
    }

    /*

	    Cálculos dos Usuários

    */

    public function calculos()
    {

        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->loadModel("VwCalculos");



        $siglasRamos = $this->VwCalculos->find("all", ["keyValue" => "id", "valueField" => "ramo"])->select(["VwCalculos.ramo"])->distinct("VwCalculos.ramo")->where(["VwCalculos.id IS NOT" => null, "VwCalculos.user_id" => $sessao["id"]])->toArray();

        foreach ($siglasRamos as $sigla) {

            switch ($sigla["ramo"]) {
                case "SPF":
                    $ramos[$sigla["ramo"]] = "Saúde Pessoa Física";
                    break;
                case "OPF":
                    $ramos[$sigla["ramo"]] = "Odonto Pessoa Física";
                    break;
                case "SPJ":
                    $ramos[$sigla["ramo"]] = "Saúde Pessoa Jurídica";
                    break;
                case "OPJ":
                    $ramos[$sigla["ramo"]] = "Odonto Pessoa Jurídica";
                    break;
            }
        }
        $ramos['todos'] = "Todos os Ramos";

        $this->loadModel("Status");
        $status = $this->Status->find("list", ["valueField" => "nome"])->toArray();
        $status["todos"] = "Todas as Fases";


        $this->loadModel("Alertas");
        $alertasUsuario = $this->Alertas->find("all")->where(["Alertas.user_id" => $sessao["id"]])->order(["Alertas.data_alerta"])->contain(["Simulacoes", "PfCalculos", "OdontoCalculos"])->toArray();
        foreach ($alertasUsuario as $alerta) {
            switch ($alerta["model"]) {
                case "SPJ":
                    $coluna = "simulacao_id";
                    $model = "simulaco";
                    break;
                case "SPF":
                    $coluna = "pf_calculo_id";
                    $model = "pf_calculo";
                    break;
                case "OPJ":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
                case "OPF":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
            }
            $alertas[$alerta["model"]][$alerta[$coluna]][] = $alerta;
        }
        $calculos = $this->VwCalculos->find("all")->where(["VwCalculos.id IS NOT" => null, "VwCalculos.user_id" => $sessao["id"]])->order(["VwCalculos.data" => "DESC"])->toArray();
        $alertasVencidos = $this->Alertas->checarVencidos($sessao["id"]);
        if (!empty($alertasVencidos)) {
            foreach ($calculos as $chave => $calculo) {
                if (array_key_exists($calculo["id"], $alertasVencidos) && $calculo["ramo"] === $alertasVencidos[$calculo["id"]]) {
                    unset($calculos[$chave]);
                    $calculo["alerta"] = "S";
                    array_unshift($calculos, $calculo);
                }
            }
        }

        $this->set(compact('status', 'ramos', 'alertas', 'calculos'));
    }

    /**
     * Método para buscar NEGOCIOS por RAMO
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroRamo($ramo = null)
    {
        //        debug($this->request->data);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('VwCalculos');
        if (!empty($ramo) && $ramo != "todos") {
            $calculos = $this->VwCalculos->find('all')->where([
                'VwCalculos.ramo' => $ramo,
                "VwCalculos.user_id" => $sessao["id"],
                "VwCalculos.user_id IS NOT" => null
            ])->toArray();
        } else {
            $calculos = $this->VwCalculos->find('all')->where(["VwCalculos.user_id" => $sessao["id"], "VwCalculos.user_id IS NOT" => null])->toArray();
        }

        $this->loadModel("Status");
        $status = $this->Status->find("list", ["valueField" => "nome"]);
        $this->loadModel("Alertas");
        $alertasUsuario = $this->Alertas->find("all")->where(["Alertas.user_id" => $sessao["id"]])->order(["Alertas.data_alerta"])->contain(["Simulacoes", "PfCalculos", "OdontoCalculos"])->toArray();
        foreach ($alertasUsuario as $alerta) {
            switch ($alerta["model"]) {
                case "SPJ":
                    $coluna = "simulacao_id";
                    $model = "simulaco";
                    break;
                case "SPF":
                    $coluna = "pf_calculo_id";
                    $model = "pf_calculo";
                    break;
                case "OPJ":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
                case "OPF":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
            }
            $alertas[$alerta["model"]][$alerta[$coluna]][] = $alerta;
        }

        $alertasVencidos = $this->Alertas->checarVencidos($sessao["id"]);
        if (!empty($alertasVencidos)) {
            foreach ($calculos as $chave => $calculo) {
                if (array_key_exists($calculo["id"], $alertasVencidos) && $calculo["ramo"] === $alertasVencidos[$calculo["id"]]) {
                    unset($calculos[$chave]);
                    $calculo["alerta"] = "S";
                    array_unshift($calculos, $calculo);
                }
            }
        }

        $this->set(compact('calculos', 'status', 'ramos', 'alertas'));
        $this->render("filtro_status");
    }

    /**
     * Método para buscar NEGOCIOS por STATUS das negociações
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function filtroStatus($status = null)
    {
        //        debug($this->request->data);
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('VwCalculos');
        if ($status && $status != "todos") {
            $calculos = $this->VwCalculos->find('all')->where([
                'VwCalculos.status_id' => $status,
                "VwCalculos.user_id" => $sessao["id"],
                "VwCalculos.user_id IS NOT" => null
            ])->toArray();
        } else {
            $calculos = $this->VwCalculos->find('all')->where(["VwCalculos.user_id" => $sessao["id"], "VwCalculos.user_id IS NOT" => null])->toArray();
        }
        $this->loadModel("Alertas");
        $alertasUsuario = $this->Alertas->find("all")->where(["Alertas.user_id" => $sessao["id"]])->order(["Alertas.data_alerta"])->contain(["Simulacoes", "PfCalculos", "OdontoCalculos"])->toArray();
        foreach ($alertasUsuario as $alerta) {
            switch ($alerta["model"]) {
                case "SPJ":
                    $coluna = "simulacao_id";
                    $model = "simulaco";
                    break;
                case "SPF":
                    $coluna = "pf_calculo_id";
                    $model = "pf_calculo";
                    break;
                case "OPJ":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
                case "OPF":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
            }
            $alertas[$alerta["model"]][$alerta[$coluna]][] = $alerta;
        }

        $alertasVencidos = $this->Alertas->checarVencidos($sessao["id"]);
        if (!empty($alertasVencidos)) {
            foreach ($calculos as $chave => $calculo) {
                if (array_key_exists($calculo["id"], $alertasVencidos) && $calculo["ramo"] === $alertasVencidos[$calculo["id"]]) {
                    unset($calculos[$chave]);
                    $calculo["alerta"] = "S";
                    array_unshift($calculos, $calculo);
                }
            }
        }

        $this->loadModel("Status");
        $status = $this->Status->find("list", ["valueField" => "nome"]);

        $this->set(compact('calculos', 'status', 'ramos', 'alertas'));
    }


    /**
     * Método para buscar NEGOCIOS por NUM DO CALCULO
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function pesquisarCalculo($num_calculo = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('VwCalculos');

        if (!empty($num_calculo)) {
            $calculos = $this->VwCalculos->find('all')->where([
                'VwCalculos.id' => $num_calculo,
                "VwCalculos.user_id" => $sessao["id"],
                "VwCalculos.user_id IS NOT" => null
            ])->toArray();
        } else {
            $calculos = $this->VwCalculos->find('all')->where([
                "VwCalculos.user_id" => $sessao["id"],
                "VwCalculos.user_id IS NOT" => null
            ])->toArray();
        }
        $this->loadModel("Status");
        $status = $this->Status->find("list", ["valueField" => "nome"]);

        $this->loadModel("Alertas");
        $alertasUsuario = $this->Alertas->find("all")->where(["Alertas.user_id" => $sessao["id"]])->order(["Alertas.data_alerta"])->contain(["Simulacoes", "PfCalculos", "OdontoCalculos"])->toArray();
        foreach ($alertasUsuario as $alerta) {
            switch ($alerta["model"]) {
                case "SPJ":
                    $coluna = "simulacao_id";
                    $model = "simulaco";
                    break;
                case "SPF":
                    $coluna = "pf_calculo_id";
                    $model = "pf_calculo";
                    break;
                case "OPJ":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
                case "OPF":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
            }
            $alertas[$alerta["model"]][$alerta[$coluna]][] = $alerta;
        }

        $alertasVencidos = $this->Alertas->checarVencidos($sessao["id"]);
        if (!empty($alertasVencidos)) {
            foreach ($calculos as $chave => $calculo) {
                if (array_key_exists($calculo["id"], $alertasVencidos) && $calculo["ramo"] === $alertasVencidos[$calculo["id"]]) {
                    unset($calculos[$chave]);
                    $calculo["alerta"] = "S";
                    array_unshift($calculos, $calculo);
                }
            }
        }


        $this->set(compact('calculos', 'status', 'ramos', 'alertas'));
        $this->render("filtro_status");
    }

    /**
     * Método para buscar NEGOCIOS por NOME DO CLIENTE
     *
     * @param string|null $id Simulaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function pesquisarCliente($nome_cliente = null)
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $this->loadModel('VwCalculos');

        if (!empty($nome_cliente)) {
            $calculos = $this->VwCalculos->find('all')->where([
                'VwCalculos.nome' => $nome_cliente,
                "VwCalculos.user_id" => $sessao["id"],
                "VwCalculos.user_id IS NOT" => null
            ])->toArray();
        } else {
            $calculos = $this->VwCalculos->find('all')->where([
                "VwCalculos.user_id" => $sessao["id"],
                "VwCalculos.user_id IS NOT" => null
            ])->toArray();
        }
        $this->loadModel("Status");
        $status = $this->Status->find("list", ["valueField" => "nome"]);
        $this->loadModel("Alertas");
        $alertasUsuario = $this->Alertas->find("all")->where(["Alertas.user_id" => $sessao["id"]])->order(["Alertas.data_alerta"])->contain(["Simulacoes", "PfCalculos", "OdontoCalculos"])->toArray();
        foreach ($alertasUsuario as $alerta) {
            switch ($alerta["model"]) {
                case "SPJ":
                    $coluna = "simulacao_id";
                    $model = "simulaco";
                    break;
                case "SPF":
                    $coluna = "pf_calculo_id";
                    $model = "pf_calculo";
                    break;
                case "OPJ":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
                case "OPF":
                    $coluna = "odonto_calculo_id";
                    $model = "odonto_calculo";
                    break;
            }
            $alertas[$alerta["model"]][$alerta[$coluna]][] = $alerta;
        }

        $alertasVencidos = $this->Alertas->checarVencidos($sessao["id"]);

        if (!empty($alertasVencidos)) {
            foreach ($calculos as $chave => $calculo) {
                if (array_key_exists($calculo["id"], $alertasVencidos) && $calculo["ramo"] === $alertasVencidos[$calculo["id"]]) {
                    unset($calculos[$chave]);
                    $calculo["alerta"] = "S";
                    array_unshift($calculos, $calculo);
                }
            }
        }


        $this->set(compact('calculos', 'status', 'ramos', 'alertas'));
        $this->render("filtro_status");
    }

    /*

	Método de Bloqueio / Desbloqueio de Usuários.

    */

    public function bloqueio($id = null, $tipo = null)
    {
        $user = $this->Users->get($id);
        if ($user) {
            if ($tipo == "BLOQUEIO") {
                $user->bloqueio = "S";
            } elseif ($tipo == "DESBLOQUEIO") {
                $user->bloqueio = "N";
            }

            $user->dirty('bloqueio', true);
            $this->Users->save($user);
        }
    }

    /*

		Método de envio de alertas em Lote todos os dias as 9h

	*/
    public function envioAlertas()
    {
        $this->loadModel("Alertas");
        $alertas = $this->Alertas->find("all")->where(["enviado IS NULL", "data_alerta <=" => "now()"])->contain(["Users"]);

        $i = 0;
        foreach ($alertas as $alerta) {
            $destinatario = str_replace('"', '', $alerta["user"]["celular"]);
            $destinatario = str_replace(" ", '', $destinatario);
            $destinatario = str_replace("(", '', $destinatario);
            $destinatario = str_replace(")", '', $destinatario);
            $destinatario = str_replace("-", '', $destinatario);
            $destinatario = str_replace(";", '', $destinatario);
            $dadosEnvio[$i]["to"] = '55' . trim($destinatario);
            $dadosEnvio[$i]["msg"] = "Alerta: " . $alerta["mensagem"] . " - Corretor Parceiro";
            $i++;
        }
        if (!empty($dadosEnvio)) {
            $json = json_encode([
                "sendSmsMultiRequest" => [
                    "aggregateId" => "32807",
                    "sendSmsRequestList" =>
                    $dadosEnvio
                ]
            ]);

            $ch = curl_init('https://api-rest.zenvia.com/services/send-sms-multiple');

            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==', 'Accept:application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $result = json_decode($result);

            if (!empty($alertas)) {
                foreach ($alertas as $alerta) {
                    $ids[$alerta["id"]] = $alerta["id"];
                }
                $this->Alertas->updateAll(["enviado" => "S"], ["id IN" => $ids]);
            }
        }

        $this->viewBuilder()->layout('ajax');
    }


    protected function setJsonResponse()
    {
        $this->loadComponent('RequestHandler');
        $this->RequestHandler->renderAs($this, 'json');
        $this->response->type('application/json');
    }



    public function checksession()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        $this->RequestHandler->renderAs($this, 'json');
        $this->set(['result' => $sessao, '_serialize' => 'result', '_jsonp' => true]);
    }

    /**
     * Ação de LOGIN com redirecionamento
     */
    public function loginapi()
    {
        $user = $this->Auth->identify();
        $checaUsuario = $this->Users->find("all")->where(["username" => $this->request->data["username"]])->first();

        if ($checaUsuario) { //CPF EXISTE
            if ($user <> false) {

                //Usuário não BLOQUEADO PELA ADMINISTRADOR
                if ($user["bloqueio"] <> "S") {

                    //SALVA ULTIMO LOGIN DO USUARIO
                    $userLogado = $this->Users->get($user['id']);
                    $userLogado->ultimologin = date('Y-m-d H:i:s');
                    $userLogado->dirty('ultimologin', true);
                    $this->Users->save($userLogado);

                    $this->Auth->setUser($user);
                    $session = $this->request->session();
                    $sessao = $session->read('Auth.User');

                    $this->loadModel('UsersGrupos');
                    $user_grupos = $this->UsersGrupos->find('list')->where(['user_id' => $user['id']])->toArray();
                    $session->write([
                        'Auth.User.grupos' => $user_grupos
                    ]);

                    $this->loadModel('Imagens');
                    $imagem = $this->Imagens->find('list')->where(['id' => $user['imagem_id']])->count();
                    if ($imagem > 0) {
                        $imagem = $this->Imagens->get($user['imagem_id']);
                        $session->write(['Auth.User.imagem' => $imagem]);
                    }

                    $dadosMenuPrincipal = $this->Menu->dadosMenu($user);
                    $dadosMenuFerramentas = $this->menuAdicional($dadosMenuPrincipal);
                    ksort($dadosMenuFerramentas['linha'][1]);

                    $this->request->session()->write('Auth.menu', $dadosMenuFerramentas);

                    //LOGIN CORRETO
                    $resposta["msg"] = '';
                    $resposta["user"] = $user;
                    $resposta["frame"] = '/ferramentas';
                    $resposta["status"] = 'sucesso';

                    //VERIFICA SE HÁ SESSAO NO BANCO E APAGA (SESSAO EM OUTROS DISPOSITIVOS)
                    $this->loadModel('Sessions');
                    $this->Sessions->deleteAll(['id <>' => $this->request->session()->id(), 'user_id' => $user["id"]]);
                } else {
                    $resposta["msg"] = 'Seu período de teste expirou.<br>Entre em contato com <br>o suporte a corretores.';
                    $resposta["user"] = null;
                    $resposta["frame"] = 'logout';
                    $resposta["status"] = 'erro';
                    $this->Auth->logout();
                }
            } else {
                //USUÁRIO OU SENHA INCORRETO
                $resposta["msg"] = 'Senha incorreta';
                $resposta["user"] = null;
                $resposta["frame"] = null;
                $resposta["status"] = 'erro';
            }
        } else {
            //USUÁRIO NÃO CADASTRADO
            $resposta["msg"] = 'Usuário não Cadastrado';
            $resposta["user"] = null;
            $resposta["frame"] = null;
            $resposta["status"] = 'erro';
        }
        $this->set('resposta', $resposta);
    }

    public function gestao()
    {
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        if ($sessao["role"] <> "admin") {
            $this->Flash->error(__(' Área restrita'));
            return $this->redirect(['action' => 'central']);
        }
    }

    public function configurarPf()
    {
    }

    public function configurarPme()
    {
    }

    public function configurarOdonto()
    {
    }
    public function deleteLote()
    {
        foreach ($this->request->data["id"] as $id) {
            $user = $this->Users->get($id);

            $this->deleteUserActiveCampaign($user);

            $this->Users->delete($user);
        }
    }


    public function mailchimpData($offset = null)
    {
        //Grupos MAILCHIMP
        /*
			Ferramentas = c81fff0722;
			Z ANTIGO - Natuseg - Inativos = 1dd8fbd2a5;
			Z ANTIGO - Natuseg - Ativos = 8c02a6aaed;
			Z ANTIGO - Natuseg - Bloqueados = 24c3657c29;
			Z ANTIGO - Corpar = b8be87ae2d;
			Z ANTIGO - Corpar - Bloqueados = fdda6f1c5f;
			Z ANTIGO - Corpar - Prospects = 835c25f778;
			Z ANTIGO - Corpar - News = 3961bd91ca;
			Z ANTIGO - Prospects - Qualicorp = ba5e8e707a;
			TODOS BA = 1f9b10bbad;
			TODOS ES = c238c953c3;
			TODOS AM = 58678d9cdd;
			TODOS MG = c9a9e2bbc2;
			TODOS RN = dc9e0f8472;
			TODOS SUSEP = 3bcf0ef8f8;
			CORRETORES ASSESSORIA = 4653094540;
			NATUSEG ATIVOS = 0b34c537ce;
			NATUSEG INATIVOS = 2dbceb5a19;
		*/

        $offset *= 50;

        $listID = 'ee42bd6d30';
        $dataCenter = substr($this->apiKey, strpos($this->apiKey, '-') + 1);

        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members?offset=' . $offset . '&count=50';

        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $this->apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $dados = json_decode($result);

        $this->set(['users' => $dados]);
    }

    public function syncmailchimp()
    {
        $users = $this->request->data();

        $listID = 'ee42bd6d30';
        $dataCenter = substr($this->apiKey, strpos($this->apiKey, '-') + 1);

        foreach ($users as $id => $data) {
            if ($data["changed"] == 'true') {
                $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $id;

                $json = json_encode([
                    'interests' => [
                        'c81fff0722' => ($data['interests']['c81fff0722'] == 1 ?  true : false),
                        '1f9b10bbad' => ($data['interests']['1f9b10bbad'] == 1 ?  true : false),
                        'c238c953c3' => ($data['interests']['c238c953c3'] == 1 ?  true : false),
                        '58678d9cdd' => ($data['interests']['58678d9cdd'] == 1 ?  true : false),
                        'c9a9e2bbc2' => ($data['interests']['c9a9e2bbc2'] == 1 ?  true : false),
                        'dc9e0f8472' => ($data['interests']['dc9e0f8472'] == 1 ?  true : false),
                        '3bcf0ef8f8' => ($data['interests']['3bcf0ef8f8'] == 1 ?  true : false),
                        '0b34c537ce' => ($data['interests']['0b34c537ce'] == 1 ?  true : false),
                        '2dbceb5a19' => ($data['interests']['2dbceb5a19'] == 1 ?  true : false),

                    ],
                ]);
                // send a HTTP POST request with curl
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $this->apiKey);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
            }
        }
    }

    public function filtroMunicipio($estado = null)
    {
        if ($this->request->is('get') && $estado != null) {
            $municipios = $this->Users->Municipios->find('list', ['valueField' => 'nome'])
                ->where(["estado_id" => $estado])
                ->order(['nome' => 'ASC']);
            $this->set(compact('municipios'));
        }
    }

    function buildTree(array &$elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as &$element) {
            if ($element['grupo_pai_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element['nome']] = $element;
                unset($element);
            }
        }
        return $branch;
    }

    public function editGrupos($id)
    {
        $this->set('title', 'Alterar Usuário');

        if ($this->request->is(['patch', 'post', 'put'])) {
        }

        $this->loadModel('UsersGrupos');
        $userGrupos = $this->UsersGrupos->find('list', ['keyField' => 'grupo_id'])->where(['user_id' => $id])->toArray();

        $this->loadModel('Grupos');
        $gruposAll = $this->Grupos->find('list', [
            'keyField' => 'id',
            'valueField' => function ($e) {
                $d = [];
                $d['nome'] = $e->nome;
                $d['id'] = $e->id;
                $d['grupo_pai_id'] = $e->grupo_pai_id;
                $d['status'] = $e->status;
                return $d;
            },
            'contain' =>  'GrupoPai'
        ])->order(['Grupos.id' => 'ASC'])->toArray();

        $grupos = $this->buildTree($gruposAll);

        $this->loadModel('GruposUsersLinks');
        $links = $this->GruposUsersLinks->find('list', ['keyField' => 'grupo_id', 'valueField' => 'link'])->where(['user_id' => $id])->toArray();

        $this->set(compact('grupos', 'id', 'userGrupos', 'links'));
    }

    public function listaUsuarios()
    {
        $estadosUsers = $this->Users->find('list', ['keyField' => 'estado_id', 'valueField' => 'estado_id'])->toArray();
        $this->loadModel("Estados");
        $estados = $this->Estados->find("list", ["valueField" => "nome"])->where(["id IN" => $estadosUsers])->toArray();
        $conditions = array();

        if (!empty($this->request->data['status'])) {

            switch ($this->request->data['status']) {
                case "ATIVO":
                    array_push($conditions, ['Users.validacao' => "ATIVO", 'Users.validacao_email' => "ATIVO", "Users.ultimologin >= (now() - INTERVAL 90 DAY)"]);
                    break;
                case "ATIVADO":
                    array_push($conditions, ['Users.validacao' => "ATIVO", 'Users.validacao_email' => "ATIVO"]);
                    break;
                case "SEM-USO":
                    array_push($conditions, ['Users.validacao' => "ATIVO", 'Users.validacao_email' => "ATIVO", 'Users.ultimologin <= (now() - interval 90 DAY)']);
                    break;
                case "PENDENTE":
                    array_push($conditions, ["OR" => ['Users.validacao' => "PENDENTE", 'Users.validacao_email' => "PENDENTE"]]);
                    break;
                case "INATIVO":
                    array_push($conditions, ["OR" => ['Users.validacao' => "INATIVO", 'Users.validacao_email' => "INATIVO"]]);
                    break;
                case "NOVOS":
                    array_push($conditions, ['Users.created >=' => Time::now()->modify("-7 days")]);
                    break;
            }
        }
        if (!empty($this->request->data['estado-id'])) {
            array_push($conditions, ['Users.estado_id' => $this->request->data['estado-id']]);
        }

        if (!empty($this->request->data['info_busca'])) {
            switch (ctype_alpha(str_replace(" ", "", $this->request->data["info_busca"]))) {
                case true:
                    $conditions = 'Users.nome LIKE "%' . $this->request->data['info_busca'] . '%" OR Users.sobrenome LIKE "%' . $this->request->data['info_busca'] . '%"';
                    break;
                case false:
                    if (strpos($this->request->data['info_busca'], "@") <> 0) {
                        $conditions = 'Users.email LIKE "%' . $this->request->data['info_busca'] . '%"';
                        //                     	debug($conditions);die();
                    }
                    if (ctype_digit(str_replace(".", "", str_replace("-", "", $this->request->data['info_busca'])))) {
                        $conditions = 'Users.username = "' . substr($this->request->data['info_busca'], 0, 3) . '.' . substr($this->request->data['info_busca'], 3, 3) . '.' . substr($this->request->data['info_busca'], 6, 3) . '-' . substr($this->request->data['info_busca'], 9, 2) . '" OR Users.celular = "' . '(' . substr($this->request->data['info_busca'], 0, 2) . ') ' . substr($this->request->data['info_busca'], 2, 1) . ' ' . substr($this->request->data['info_busca'], 3, 4) . '-' . substr($this->request->data['info_busca'], 7, 4) . '" OR username = "' . $this->request->data['info_busca'] . '"';
                    }
                    break;
            }
        }

        if (!empty($conditions)) {
            $queryUsers = $this->Users->find()->where([$conditions])->order(["ultimologin" => "DESC"]);
        } else {
            $queryUsers = $this->Users->find()->order(["ultimologin" => "DESC"]);
        }

        $this->set('users', $this->paginate($queryUsers));

        $this->set(compact('estados'));
    }

    public function menuAdicional($dadosMenu)
    {

        // venda Online
        $this->loadModel("VendasOnlines");
        $vendasOnline = $this->VendasOnlines->find('all', [
            'fields' => ['id', 'image' => 'icone', 'menu_linha1', 'menu_linha2', 'exibir_vendas_online'],
            'conditions' => ['exibir_menu' => '1'],
        ])->orderAsc('ordem')->toArray();

        foreach ($vendasOnline as $venda) {
            $vop = $this->VendasOnlines->VendasOnlinesOperadoras
                ->find('list')
                ->where(['id_venda_online' => $venda->id])
                ->toArray();
            $this->loadModel('VendasOnlinesOperadorasEstados');
            $ids_estados = $this->VendasOnlinesOperadorasEstados->find('list', ['keyField' => 'estado_id', 'valueField' => 'estado_id'])->where(['venda_online_operadora_id IN' => $vop])->toArray();
            $venda['menu_estados'] = $this->VendasOnlines->VendasOnlinesOperadoras->Estados->find('list', ['valueFields' => 'nome'])->where(['id IN' => $ids_estados])->toArray();
        }
        $dadosMenu['linha'][1][3]['texto'] = 'Venda Online';
        $dadosMenu['linha'][1][3]['link'] = '#';
        $dadosMenu['linha'][1][3]['image'] = WEB_ROOT . '/app/img/menu/carrinho.png';
        $dadosMenu['linha'][1][3]['toggle'] = "dropdown";
        $dadosMenu['linha'][1][3]['submenu'] = $vendasOnline;

        $midiasCompartilhadas = [];
        // Midias Compartilhadas
        $this->loadModel("MidiasCompartilhadas");
        // $midiaCompartilhada = new MidiasCompartilhadas();
        $midiasCompartilhadas = $this->MidiasCompartilhadas->find('all', [
            'fields' => ['id', 'nome', 'titulo', 'menu_linha1', 'menu_linha2'],
            'conditions' => ['exibir_midias_compartilhadas' => '1', 'active' => 1],
        ])->orderAsc('ordem')->toArray();

        if (!empty($midiasCompartilhadas)) {

            foreach ($midiasCompartilhadas as $midia) {

                $a = $this->MidiasCompartilhadas->MidiasCompartilhadasOperadoras->find('list')
                    ->where(['midia_compartilhada_id' => $midia->id])
                    ->toArray();

                if (!empty($a)) {
                    $this->loadModel('MidiasCompartilhadasOperadorasEstados');
                    $ids_estados = $this->MidiasCompartilhadasOperadorasEstados->find('list', ['keyField' => 'estado_id', 'valueField' => 'estado_id'])->where(['midia_compartilhada_operadora_id IN' => $a])->toArray();
                    $midia['menu_estados'] = $this->MidiasCompartilhadas->MidiasCompartilhadasOperadoras->MidiasCompartilhadasOperadorasEstados->Estados->find('list', ['valueFields' => 'nome'])->where(['id IN' => $ids_estados])->toArray();
                }
            }
        }

        $dadosMenu['linha'][1][4]['texto'] = 'Material de Divulgação';
        $dadosMenu['linha'][1][4]['link'] = '#';
        $dadosMenu['linha'][1][4]['image'] = WEB_ROOT . '/app/img/menu/luz.png';
        $dadosMenu['linha'][1][4]['toggle'] = "dropdown";
        $dadosMenu['linha'][1][4]['submenu'] = $midiasCompartilhadas;
        $dadosMenu['linha'][1][4]['requestjs'] = "AppMidiasCompartilhadas";

        return $dadosMenu;
    }



    public function addOrUpdateUserActiveCampaign($user)
    {
        $ano = $user->data_nascimento->year;
        $mes = str_pad($user->data_nascimento->month, 2, "0", STR_PAD_LEFT);
        $dia = str_pad($user->data_nascimento->day, 2, "0", STR_PAD_LEFT);

        $niver = sprintf('%s-%s-%s', $ano, $mes, $dia);
        debug($niver);
        $json = json_encode([
            'contact' => [
                'email' => $user['email'],
                'firstName' =>  str_replace('"', '', $user['nome']),
                'lastName' =>  str_replace('"', '', $user['sobrenome']),
                'phone' =>  str_replace('"', '', $user['celular']),
                'fieldValues' => [
                    [
                        'field' => "1",
                        'value' => $niver
                    ]
                ]
            ],
        ]);

        $url = sprintf('%s/3/contact/sync', $this->activeCampaignBaseUrl);

        $this->requestActiveCampaign($url, 'POST', $json);
    }

    public function deleteUserActiveCampaign($user)
    {
        $urlGetUserId = sprintf('%s/3/contacts?search=%s', $this->activeCampaignBaseUrl, $user['email']);

        $contact = $this->requestActiveCampaign($urlGetUserId, 'GET');
        $activeCampaignUserId = $contact->contacts[0]->id;

        if (!$activeCampaignUserId) {
            return;
        }

        $url = sprintf('%s/3/contacts/%d', $this->activeCampaignBaseUrl, $activeCampaignUserId);

        $this->requestActiveCampaign($url, 'DELETE');
    }

    public function requestActiveCampaign($url, $method, $json = null)
    {
        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [sprintf('Api-token: %s', $this->activeCampaignKey)]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return json_decode($result);
    }
    public function lock()
    {
        // ALTERA O LAYOUT
        $this->layout = 'ajax';

        // CAPITURA DADOS DO USUÁRIO LOGADO
        $session = $this->request->session();
        $user_id = $session->read('Auth.User.id');

        $dados = $this->Users->get($user_id);

        $name = $dados['User']['name'];
        $username = $dados['User']['username'];
        $photo = $dados['User']['photo'];

        // ENVIA DADOS PARA VIEW
        $this->set(compact('name', 'username', 'photo'));

        // MATA A SESSÃO DO USUÁRIO
        $this->Auth->logout();
    }

    public function list()
    {
        // ALTERA O LAYOUT
        $this->layout = 'ajax';

        $options = parent::_index();

        // TRATA CONDIÇÕES
        foreach ($options as $field => $value) {
            if ($field == 'Users.nome') {
                $options[$field . ' like'] = "%$value%";
                unset($options[$field]);
            }
            if ($field == 'Users.username') {
                $options[$field . ' like'] = "%$value%";
                unset($options[$field]);
            }
        }

        $query = $this->Users->find('all')->where($options)->order('Users.nome');

        $this->set('dados', $this->paginate($query));
    }


    public function editBasic($idUser)
    {
        // ALTERA O LAYOUT
        $this->layout = 'ajax';

        $this->loadModel('Users');
        if (!$this->Users->exists(['Users.id' => $idUser])) {
            $this->Flash->error(__('Registro inexistente'));
            return false;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $user = $this->Users->get($idUser);

            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('Registro salvo com sucesso.');
            } else {
                $this->Flash->error(__('Não foi possível salvar o registro. Favor tentar novamente.'));
            }
        }
        $this->request->data = $this->Users->get($idUser)->toArray();
        $this->request->data['nome'] = trim($this->request->data['nome']);

        $this->loadModel('UsersSisGrupos');
        $dados = $this->UsersSisGrupos->find('all')->where(['UsersSisGrupos.user_id' => $idUser])->toArray();

        $aux = [];
        foreach ($dados as $v) {
            $aux[] = $v->sis_grupo_id;
        }
        $this->set('selectedGrupos', $aux);

        // PEGA LISTA DE FUNCIONALIDADES
        $this->loadModel('SisGrupos');
        $this->set('optionsGrupos', $this->SisGrupos->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['SisGrupos.active' => 1])->toArray());
    }
}
