<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersFuncoes Controller
 *
 * @property \App\Model\Table\UsersFuncoesTable $UsersFuncoes
 */
class UsersFuncoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Funcoes']
        ];
        $usersFuncoes = $this->paginate($this->UsersFuncoes);

        $this->set(compact('usersFuncoes'));
        $this->set('_serialize', ['usersFuncoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Users Funco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersFuncao= $this->UsersFuncoes->get($id, [
            'contain' => ['Users', 'Funcoes']
        ]);

        $this->set('usersFunco', $usersFuncao);
        $this->set('_serialize', ['usersFuncao']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersFuncao= $this->UsersFuncoes->newEntity();
        if ($this->request->is('post')) {
            $usersFuncao= $this->UsersFuncoes->patchEntity($usersFuncao, $this->request->data);
            if ($this->UsersFuncoes->save($usersFuncao)) {
                $this->Flash->success(__('The users funcao has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users funcao could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersFuncoes->Users->find('list', ['limit' => 200]);
        $funcoes = $this->UsersFuncoes->Funcoes->find('list', ['limit' => 200]);
        $this->set(compact('usersFunco', 'users', 'funcoes'));
        $this->set('_serialize', ['usersFunco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Funco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersFuncao= $this->UsersFuncoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersFuncao= $this->UsersFuncoes->patchEntity($usersFuncao, $this->request->data);
            if ($this->UsersFuncoes->save($usersFuncao)) {
                $this->Flash->success(__('The users funcao has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users funcao could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersFuncoes->Users->find('list', ['limit' => 200]);
        $funcoes = $this->UsersFuncoes->Funcoes->find('list', ['limit' => 200]);
        $this->set(compact('usersFuncao', 'users', 'funcoes'));
        $this->set('_serialize', ['usersFuncao']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Funco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersFuncao= $this->UsersFuncoes->get($id);
        if ($this->UsersFuncoes->delete($usersFuncao)) {
            $this->Flash->success(__('The users funco has been deleted.'));
        } else {
            $this->Flash->error(__('The users funco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
