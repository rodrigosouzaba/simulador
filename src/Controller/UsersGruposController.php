<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersGrupos Controller
 *
 * @property \App\Model\Table\UsersGruposTable $UsersGrupos
 */
class UsersGruposController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Grupos']
        ];
        $usersGrupos = $this->paginate($this->UsersGrupos);

        $this->set(compact('usersGrupos'));
        $this->set('_serialize', ['usersGrupos']);
    }

    /**
     * View method
     *
     * @param string|null $id Users Grupo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersGrupo = $this->UsersGrupos->get($id, [
            'contain' => ['Users', 'Grupos']
        ]);

        $this->set('usersGrupo', $usersGrupo);
        $this->set('_serialize', ['usersGrupo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersGrupo = $this->UsersGrupos->newEntity();
        if ($this->request->is('post')) {
            $usersGrupo = $this->UsersGrupos->patchEntity($usersGrupo, $this->request->data);
            if ($this->UsersGrupos->save($usersGrupo)) {
                $this->Flash->success(__('The users grupo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users grupo could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersGrupos->Users->find('list', ['limit' => 200]);
        $grupos = $this->UsersGrupos->Grupos->find('list', ['limit' => 200]);
        $this->set(compact('usersGrupo', 'users', 'grupos'));
        $this->set('_serialize', ['usersGrupo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Grupo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersGrupo = $this->UsersGrupos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersGrupo = $this->UsersGrupos->patchEntity($usersGrupo, $this->request->data);
            if ($this->UsersGrupos->save($usersGrupo)) {
                $this->Flash->success(__('The users grupo has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users grupo could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersGrupos->Users->find('list', ['limit' => 200]);
        $grupos = $this->UsersGrupos->Grupos->find('list', ['limit' => 200]);
        $this->set(compact('usersGrupo', 'users', 'grupos'));
        $this->set('_serialize', ['usersGrupo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Grupo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersGrupo = $this->UsersGrupos->get($id);
        if ($this->UsersGrupos->delete($usersGrupo)) {
            $this->Flash->success(__('The users grupo has been deleted.'));
        } else {
            $this->Flash->error(__('The users grupo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function syncUser()
    {
        $data = $this->request->data();
        if ($this->request->is('post')) {
            $grupos = $this->UsersGrupos->find('all')->where(['user_id' => $data['user_id']]);

            // Apaga Grupos Existentes
            if ($grupos) {
                foreach ($grupos as $grupo) {
                    $usersGrupo = $this->UsersGrupos->get($grupo['id']);
                    $this->UsersGrupos->delete($usersGrupo);
                }
            }
            $userId = $data['user_id'];
            unset($data['user_id']);
            $dados = [];

            $links = $data['link'];
            unset($data['link']);

            // // Insere o nome dos grupos na coluna de grupos da tabela Users
            // $this->loadModel('Users');
            // $user = $this->Users->get($userId);

            // if (count($data) > 0) {
            //     $this->loadModel('Grupos');
            //     $grupos = $this->Grupos->find('list', ['valueField' => 'nome'])->where(['id IN' => $data])->toArray();
            //     $user->grupos = implode(', ', $grupos);
            // } else {
            //     $user->grupos = null;
            // }
            // $this->Users->save($user);
            $gruposTexto = '';
            foreach ($data as $item) {
                $dados['user_id'] = $userId;
                $dados['grupo_id'] = $item;
                $usersGrupo = $this->UsersGrupos->newEntity();
                $usersGrupo = $this->UsersGrupos->patchEntity($usersGrupo, $dados);
                $this->UsersGrupos->save($usersGrupo);

                // Salvando link do usuário
                if (key_exists($item, $links)) {
                    $dados['link'] = $links[$item];

                    $this->loadModel('GruposUsersLinks');
                    $linkExistente = $this->GruposUsersLinks->find('list')->where(['grupo_id' => $item, 'user_id' => $dados['user_id']]);
                    if ($linkExistente->count() > 0) {
                        $linkExistente = $this->GruposUsersLinks->get($linkExistente->toArray());
                        $this->GruposUsersLinks->delete($linkExistente);
                    }

                    $link = $this->GruposUsersLinks->newEntity();
                    $link = $this->GruposUsersLinks->patchEntity($link, $dados);
                    $this->GruposUsersLinks->save($link);
                }
            }
        }
    }
}
