<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\VendasOnline;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * VendasOnlines Controller
 *
 * @property \App\Model\Table\VendasOnlinesTable $VendasOnlines */
class VendasOnlinesController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Auth->allow(['lojas', 'filtroEstado']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $vendasOnlines = $this->VendasOnlines->find('all')->contain(['VendasOnlinesOperadoras' => ['sort' => ['prioridade' => 'ASC']]])->orderAsc('ordem');
        $vendasOnlinesOperadora = $this->VendasOnlines->VendasOnlinesOperadoras->newEntity();

        $this->set(compact('vendasOnlines', 'vendasOnlinesOperadora'));
        $this->set('_serialize', ['vendasOnlines']);
    }

    /**
     * View method
     *
     * @param string|null $id Vendas Online id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vendasOnline = $this->VendasOnlines->get($id, [
            'contain' => [],
        ]);

        $this->set('vendasOnline', $vendasOnline);
        $this->set('_serialize', ['vendasOnline']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vendasOnline = $this->VendasOnlines->newEntity();
        $funcionalidades = $this->VendasOnlines->Funcionalidades->find('list', ['valueField' => 'funcionalidade']);
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $now = Time::now()->getTimestamp();
            if (isset($data['imagem'])) {
                $data['imagem']['name'] = $now . $data['imagem']['name'];
                $imagem = $data['imagem'];
                $data['imagem'] = $imagem['name'];
                if (move_uploaded_file($imagem['tmp_name'], WWW_ROOT . 'img/vendas_online/' . $data['imagem'])) {
                    $vendasOnline = $this->VendasOnlines->patchEntity($vendasOnline, $data);

                    if ($this->VendasOnlines->save($vendasOnline)) {
                        $this->Flash->success(__('The vendas online has been saved.'));

                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('The vendas online could not be saved. Please, try again.'));
                    }
                }
            } else {
                $vendasOnline = $this->VendasOnlines->patchEntity($vendasOnline, $data);
                if ($this->VendasOnlines->save($vendasOnline)) {
                    $this->Flash->success(__('The vendas online has been saved.'));

                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The vendas online could not be saved. Please, try again.'));
                }
            }
        }
        $this->set(compact('vendasOnline', 'funcionalidades'));
        $this->set('_serialize', ['vendasOnline']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vendas Online id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vendasOnline = $this->VendasOnlines->get($id, [
            'contain' => []
        ]);
        $funcionalidades = $this->VendasOnlines->Funcionalidades->find('list', ['valueField' => 'funcionalidade'])->where(['0 = 0'])->toArray();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;

            $now = Time::now()->getTimestamp();
            if (empty($data['imagem']['name'])) {
                unset($data['imagem']);
            } else {
                $data['imagem']['name'] = $now . $data['imagem']['name'];
                $imagem = $data['imagem'];
                $data['imagem'] = $imagem['name'];
                move_uploaded_file($imagem['tmp_name'], WWW_ROOT . 'img/vendas_online/' . $data['imagem']);
            }

            if (empty($data['icone']['name'])) {
                unset($data['icone']);
            } else {
                $data['icone']['name'] = $now . $data['icone']['name'];
                $icone = $data['icone'];
                $data['icone'] = $icone['name'];
                move_uploaded_file($icone['tmp_name'], WWW_ROOT . 'img/vendas_online/icones/' . $data['icone']);
            }

            $vendasOnline = $this->VendasOnlines->patchEntity($vendasOnline, $data);
            if ($this->VendasOnlines->save($vendasOnline)) {
                $this->Flash->success(__('The vendas online has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The vendas online could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('vendasOnline', 'funcionalidades'));
        $this->set('_serialize', ['vendasOnline']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Vendas Online id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vendasOnline = $this->VendasOnlines->get($id);
        if ($this->VendasOnlines->delete($vendasOnline)) {
            $this->Flash->success(__('The vendas online has been deleted.'));
        } else {
            $this->Flash->error(__('The vendas online could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function lojas()
    {
        // if ($this->request->is('GET')) {
        //     $url = substr($this->request->referer(), strpos($this->request->referer(), '=') + 1);
        //     $this->set(compact('url'));
        // }
        if ($this->request->is('POST')) {
            $url = $this->request->data('destino');
            $aviso = $this->request->data('aviso');
            $this->set(compact('url', 'aviso'));
        }
        $this->viewBuilder()->layout('vendasOnline');
    }

    public function menu()
    {
        $vendasOnlines = $this->VendasOnlines->find('all')
            ->select(['id', 'icone', 'menu_linha1', 'menu_linha2', 'exibir_vendas_online'])
            ->where(['exibir_menu' => 1])
            ->order(['ordem' => 'ASC']);
        $this->set(compact('vendasOnlines'));
    }

    public function filtroEstado($vop = null, $estado)
    {
        $dados = $this->VendasOnlines->VendasOnlinesOperadoras->find('list', ['valueField' => 'id_venda_online'])
            ->matching('Estados', function ($estados) use ($estado) {
                return $estados->where(['Estados.id' => $estado]);
            })
            ->group('id_venda_online');

        $vendasOnline = $this->VendasOnlines->find('all')
            ->contain([
                'VendasOnlinesOperadoras' => function ($q) use ($estado) {
                    return $q
                        ->order(['prioridade' => 'ASC'])
                        ->where(['NOT' => ['status' => 1]])
                        ->matching('Estados', function ($estados) use ($estado) {
                            return $estados->where(['Estados.id' => $estado]);
                        });
                }
            ])
            ->where(['exibir_vendas_online' => 1])
            ->order(['ordem' => 'ASC'])
            ->toArray();

        $userCtlr = new \App\Controller\UsersController;
        $menuVendas = $userCtlr->menuAdicional([]);
        $estadosOdonto = TableRegistry::get('OdontoOperadoras')->getEstados();
        $estadosSaude = TableRegistry::get('Operadoras')->getEstados();
        $this->set(compact('vendasOnline',  'menuVendas', 'estadosOdonto', 'estadosSaude'));
    }
}
