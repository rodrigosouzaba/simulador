<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\LinksPersonalizado;
use Cake\I18n\Time;
use PDOException;
use Cake\Event\Event;

/**
 * VendasOnlinesOperadoras Controller
 *
 * @property \App\Model\Table\VendasOnlinesOperadorasTable $VendasOnlinesOperadoras */
class VendasOnlinesOperadorasController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        // $this->loadComponent('GlobalComponent');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.

        $this->Auth->allow(['link', 'getLinkPersonalizado']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $vendasOnlinesOperadoras = $this->paginate($this->VendasOnlinesOperadoras);

        $this->set(compact('vendasOnlinesOperadoras'));
        $this->set('_serialize', ['vendasOnlinesOperadoras']);
    }

    /**
     * View method
     *
     * @param string|null $id Vendas Onlines Operadora id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vendasOnlinesOperadora = $this->VendasOnlinesOperadoras->get($id, [
            'contain' => []
        ]);

        $this->set('vendasOnlinesOperadora', $vendasOnlinesOperadora);
        $this->set('_serialize', ['vendasOnlinesOperadora']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($vendasOnline)
    {
        $vendasOnline = $this->VendasOnlinesOperadoras->VendasOnlines->get($vendasOnline);

        $vop = $this->VendasOnlinesOperadoras->newEntity();
        $vop->id_venda_online = $vendasOnline->id;
        $vop->vendas_online = $vendasOnline;
        unset($vendasOnline);

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $now = Time::now()->getTimestamp();
            $data['imagem']['name'] = $now . $data['imagem']['name'];
            $imagem = $data['imagem'];
            $data['imagem'] = $imagem['name'];

            if (!empty($data['manual']['name'])) {
                $data['manual']['name'] = $now . $data['manual']['name'];
                $manual = $data['manual'];
                $data['manual'] = $manual['name'];
                if (!move_uploaded_file($manual['tmp_name'], WWW_ROOT . 'uploads/venda_online/manuais/' . $manual['name'])) {
                    $this->Flash->error(__('Não foi possível adicionar o Manual.'));
                }
            }

            if (move_uploaded_file($imagem['tmp_name'], WWW_ROOT . 'img/vendas_online/' . $data['imagem'])) {
                $vop = $this->VendasOnlinesOperadoras->patchEntity($vop, $data);

                if ($this->VendasOnlinesOperadoras->save($vop)) {
                    $this->Flash->success(__('The vendas onlines operadora has been saved.'));
                    return $this->redirect(['controller' => 'VendasOnlines', 'action' => 'index']);
                } else {
                    $this->Flash->error(__('The vendas onlines operadora could not be saved. Please, try again.'));
                    return $this->redirect(['controller' => 'VendasOnlines', 'action' => 'index']);
                }
            } else {
                $this->Flash->error(__('The vendas onlines operadora could not be saved. Please, try again.'));
                return $this->redirect(['controller' => 'VendasOnlines', 'action' => 'index']);
            }
        }
        $this->loadModel('Estados');
        $estados = $this->Estados->find('list', ['valueField' => 'nome']);
        $vendasOnlines = $this->VendasOnlinesOperadoras->VendasOnlines->find('list', ['valueField' => 'nome']);
        $ramos = [
            'OPF' => 'Odonto PF',
            'OPME' => 'Odonto PME',
            'SPF' => 'Saúde PF',
            'SPME' => 'Saúde PME',
        ];

        $this->set(compact('vop', 'vendasOnlines', 'estados', 'ramos'));
        $this->set('_serialize', ['vop']);
        $this->render('edit');
    }


    public function addLink()
    {
        if ($this->request->is(["POST"])) {
            $linkPers = $this->VendasOnlinesOperadoras->LinksPersonalizados->newEntity();
            $linkPers = $this->VendasOnlinesOperadoras->LinksPersonalizados->patchEntity($linkPers, $this->request->data);
            $linkPers = $this->VendasOnlinesOperadoras->LinksPersonalizados->save($linkPers);
        }
        $this->response->body(json_encode($linkPers['id']));
        $this->response->statusCode(200);
        $this->response->type('application/json');
        $this->autoRender = false;
    }

    /**
     * Edit method
     *
     * @param string|null $id Vendas Onlines Operadora id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $vop = $this->VendasOnlinesOperadoras->get($id, [
            'contain' => ['VendasOnlines', 'LinksPersonalizados', 'Estados']
        ]);
        $estados_selecionados = $this->VendasOnlinesOperadoras->VendasOnlinesOperadorasEstados->find('list', ['keyField' => 'estado.id', 'valueField' => 'estado.nome'])->contain('Estados')->where(['venda_online_operadora_id' => $vop->id]);
        $estados = $this->VendasOnlinesOperadoras->Estados->find('list', ['valueField' => 'nome']);


        $vendasOnlines = $this->VendasOnlinesOperadoras->VendasOnlines->find('list', ['valueField' => 'nome']);
        $ramos = [
            'OPF' => 'Odonto PF',
            'OPME' => 'Odonto PME',
            'SPF' => 'Saúde PF',
            'SPME' => 'Saúde PME',
        ];
        $now = Time::now()->getTimestamp();

        if (!empty($vop->operadora_id) && !empty($vop->ramo)) {
            switch ($vop->ramo) {
                case 'OPF':
                    $this->loadModel('OdontoOperadoras');
                    $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome'])->where(['OR' => ['status' => "", 'status is NULL'], 'estado_id' => $vop->estado_id, 'tipo_pessoa' => 'PF']);
                    break;
                case 'OPME':
                    $this->loadModel('OdontoOperadoras');
                    $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome'])->where(['OR' => ['status' => "", 'status is NULL'], 'estado_id' => $vop->estado_id, 'tipo_pessoa' => 'PJ']);
                    break;
                case 'SPF':
                    $this->loadModel('PfOperadoras');
                    $operadoras = $this->PfOperadoras->find('list', ['valueField' => 'nome'])->where(['OR' => ['status' => "", 'status is NULL'], 'estado_id' => $vop->estado_id]);
                    break;
                case 'SPME':
                    $this->loadModel('Operadoras');
                    $operadoras = $this->Operadoras->find('list', ['valueField' => 'nome'])->where(['OR' => ['status' => "", 'status is NULL'], 'estado_id' => $vop->estado_id]);
                    break;
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;

            if (empty($data['imagem']['name'])) {
                unset($data['imagem']);
            } else {
                $data = $this->request->data;
                $now = Time::now()->getTimestamp();
                $data['imagem']['name'] = $now . $data['imagem']['name'];
                $imagem = $data['imagem'];
                $data['imagem'] = $imagem['name'];
                unlink(WWW_ROOT . 'img/vendas_online/' . $vop->imagem);
                move_uploaded_file($imagem['tmp_name'], WWW_ROOT . 'img/vendas_online/' . $data['imagem']);
            }

            if (empty($data['manual']['name'])) {
                unset($data['manual']);
            } else {
                $data['manual']['name'] = $now . $data['manual']['name'];
                $manual = $data['manual'];
                $data['manual'] = $manual['name'];
                if (!move_uploaded_file($manual['tmp_name'], WWW_ROOT . 'uploads/venda_online/manuais/' . $manual['name'])) {
                    $this->Flash->error(__('Não foi possível adicionar o Manual.'));
                }
            }
            // debug($data);
            // die;
            $vop = $this->VendasOnlinesOperadoras->patchEntity($vop, $data);
            if ($this->VendasOnlinesOperadoras->save($vop)) {
                $this->Flash->success(__('The vendas onlines operadora has been saved.'));
                return $this->redirect(['controller' => 'VendasOnlines', 'action' => 'index']);
            } else {
                $this->Flash->error(__('Falha ao salvar.'));
            }
        }
        $this->set(compact('vop', 'vendasOnlines', 'estados', 'ramos', 'operadoras', 'estados_selecionados'));
        $this->set('_serialize', ['vendasOnlinesOperadora']);
    }
    /**
     * Delete method
     *
     * @param string|null $id Vendas Onlines Operadora id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['get', 'delete']);
        $vendasOnlinesOperadora = $this->VendasOnlinesOperadoras->get($id);
        if ($this->VendasOnlinesOperadoras->delete($vendasOnlinesOperadora)) {
            $this->Flash->success(__('The vendas onlines operadora has been deleted.'));
        } else {
            $this->Flash->error(__('The vendas onlines operadora could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'VendasOnlines', 'action' => 'index']);
    }

    public function getOperadoras($estado, $ramo)
    {
        switch ($ramo) {
            case 'OPF':
                $this->loadModel('OdontoOperadoras');
                $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome'])->where(['OR' => ['status' => "", 'status is NULL'], 'estado_id' => $estado, 'tipo_pessoa' => 'PF']);
                break;
            case 'OPME':
                $this->loadModel('OdontoOperadoras');
                $operadoras = $this->OdontoOperadoras->find('list', ['valueField' => 'nome'])->where(['OR' => ['status' => "", 'status is NULL'], 'estado_id' => $estado, 'tipo_pessoa' => 'PJ']);
                break;
            case 'SPF':
                $this->loadModel('PfOperadoras');
                $operadoras = $this->PfOperadoras->find('list', ['valueField' => 'nome'])->where(['OR' => ['status' => "", 'status is NULL'], 'estado_id' => $estado]);
                break;
            case 'SPME':
                $this->loadModel('Operadoras');
                $operadoras = $this->Operadoras->find('list', ['valueField' => 'nome'])->where(['OR' => ['status' => "", 'status is NULL'], 'estado_id' => $estado]);
                break;

            default:
                return;
                break;
        }
        $this->set(compact('operadoras'));
    }

    public function insertLinksIndividuais($vop, $cpfs, $links)
    {

        $this->loadModel('LinksPersonalizados');
        $links = array_combine($cpfs, $links);
        foreach ($links as $cpf => $link) {
            $lp = $this->LinksPersonalizados->newEntity();
            $lp = $this->LinksPersonalizados->patchEntity($lp, ['vendas_onlines_operadora_id' => $vop, 'cpf' => $cpf, 'link' => $link]);
            $this->LinksPersonalizados->save($lp);
        }
    }

    public function existUser($username)
    {
        $this->loadModel('Users');
        $user = $this->Users->find('list', ['valueField' => 'nome'])->where(['username' => $username]);
        $exist = $user->count() > 0 ? true : false;
        $data = [$exist, key($user->toArray()), current($user->toArray())];

        $this->response->body(json_encode($data));
        $this->response->statusCode(200);
        $this->response->type('application/json');
        $this->autoRender = false;

        return $this->response;
    }

    public function delManual($vop)
    {
        $vop = $this->VendasOnlinesOperadoras->get($vop);
        unlink(WWW_ROOT . 'uploads/venda_online/manuais/' . $vop->manual);
        $vop->manual = '';
        $this->VendasOnlinesOperadoras->save($vop);
        $this->autoRender = false;
    }
    public function duplicar($id)
    {
        $voo = $this->VendasOnlinesOperadoras->get($id);
        unset($voo->id);
        unset($voo->prioridade);
        $new = $this->VendasOnlinesOperadoras->newEntity($voo->toArray());
        $new = $this->VendasOnlinesOperadoras->save($new);
        $this->redirect(['action' => 'edit', $new->id]);
    }

    public function getLinkPersonalizado($voo, $user)
    {
        $arrConditions = ['vendas_onlines_operadora_id' => $voo, 'cpf' => $user];
        $link = $this->VendasOnlinesOperadoras->LinksPersonalizados->find('all')->where($arrConditions);
        $link = json_encode($link);
        $this->response->type('json');
        $this->response->body($link);
        return $this->response;
    }

    public function link($vop)
    {
        $vop = $this->VendasOnlinesOperadoras->get($vop);
        $this->set(compact('vop'));
        $this->viewBuilder()->setLayout('vendasOnline');
    }
}
