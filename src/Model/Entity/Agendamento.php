<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Agendamento Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $data_alteracao
 * @property float $percentual
 * @property \Cake\I18n\Time $nova_vigencia
 * @property int $user_id
 * @property \Cake\I18n\Time $created
 * @property string $situacao
 * @property int $operadora_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Tabela[] $tabelas
 */
class Agendamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
