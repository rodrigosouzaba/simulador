<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Alerta Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property int $user_id
 * @property string $model
 * @property \Cake\I18n\Time $data_alerta
 * @property string $mensagem
 * @property int $simulacao_id
 * @property int $pf_calculo_id
 * @property int $odonto_calculo_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Simulaco $simulaco
 * @property \App\Model\Entity\OdontoCalculo $odonto_calculo
 */
class Alerta extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
