<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AreasComercializaco Entity
 *
 * @property int $id
 * @property string $descricao
 * @property string $nome
 * @property int $operadora_id
 * @property int $abrangencia_id
 *
 * @property \App\Model\Entity\Operadora $operadora
 * @property \App\Model\Entity\Abrangencia $abrangencia
 */
class AreasComercializaco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
