<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AreasComercializacoesEstadosMunicipio Entity
 *
 * @property int $id
 * @property int $estado_id
 * @property int $area_comercializacao_id
 * @property int $municipio_id
 *
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\AreaComercializacao $area_comercializacao
 * @property \App\Model\Entity\Municipio $municipio
 */
class AreasComercializacoesEstadosMunicipio extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'estado_id' => true,
        'area_comercializacao_id' => true,
        'municipio_id' => true,
        'estado' => true,
        'area_comercializacao' => true,
        'municipio' => true,
    ];
}
