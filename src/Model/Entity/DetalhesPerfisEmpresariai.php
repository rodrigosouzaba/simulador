<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DetalhesPerfisEmpresariai Entity
 *
 * @property int $id
 * @property string $tipo
 * @property string $sexo
 * @property \Cake\I18n\Time $data_nascimento
 * @property int $perfil_empresarial_id
 *
 * @property \App\Model\Entity\PerfisEmpresariai $perfis_empresariai
 */
class DetalhesPerfisEmpresariai extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
