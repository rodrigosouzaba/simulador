<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Filtro Entity
 *
 * @property int $id
 * @property int $simulacao_id
 * @property int $tipo_produto_id
 * @property int $tipo_id
 * @property int $abrangencia_id
 * @property int $regiao_id
 * @property string $observacao
 * @property string $coparticipacao
 * @property \Cake\I18n\Time $created
 * @property string $informacao
 * @property string $reembolso
 * @property string $carencia
 * @property string $opcional
 * @property string $rede
 *
 * @property \App\Model\Entity\Simulaco $simulaco
 * @property \App\Model\Entity\TiposProduto $tipos_produto
 * @property \App\Model\Entity\Tipo $tipo
 * @property \App\Model\Entity\Regio $regio
 * @property \App\Model\Entity\TabelasFiltrada[] $tabelas_filtradas
 */
class Filtro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
