<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LinksTabela Entity
 *
 * @property int $id
 * @property int $link_id
 * @property \Cake\I18n\Time $created
 * @property int $tabela_id
 * @property int $pf_tabela_id
 * @property int $odonto_tabela_id
 *
 * @property \App\Model\Entity\Link $link
 * @property \App\Model\Entity\Tabela $tabela
 * @property \App\Model\Entity\PfTabela $pf_tabela
 * @property \App\Model\Entity\OdontoTabela $odonto_tabela
 */
class LinksTabela extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
