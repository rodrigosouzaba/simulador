<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MidiasCompartilhadasOperadora Entity
 *
 * @property int $id
 * @property string $nome
 * @property int $estado_id
 * @property string $ramo
 * @property int $operadora_id
 * @property \Cake\I18n\FrozenDate|null $validade
 * @property int|null $visibilidade
 *
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Operadora $operadora
 */
class MidiasCompartilhadasOperadora extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
