<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NotasUnica Entity
 *
 * @property int $id
 * @property int $nota_completa_id
 * @property int $created
 * @property int $caminho
 * @property int $nome_arquivo
 *
 * @property \App\Model\Entity\NotasCompleta $notas_completa
 */class NotasUnica extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
