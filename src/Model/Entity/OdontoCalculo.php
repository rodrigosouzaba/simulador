<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OdontoCalculo Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $contato
 * @property string $email
 * @property int $quantidade_vidas
 * @property \Cake\I18n\Time $data
 * @property int $telefone
 * @property int $user_id
 * @property string $calculoremoto
 * @property string $tipo_pessoa
 *
 * @property \App\Model\Entity\User $user
 */
class OdontoCalculo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
