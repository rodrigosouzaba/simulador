<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OdontoFiltrosTabela Entity
 *
 * @property int $id
 * @property int $odonto_filtro_id
 * @property int $odonto_tabela_id
 *
 * @property \App\Model\Entity\OdontoFiltro $odonto_filtro
 * @property \App\Model\Entity\OdontoTabela $odonto_tabela
 */
class OdontoFiltrosTabela extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
