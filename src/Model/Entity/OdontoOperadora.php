<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OdontoOperadora Entity
 *
 * @property int $id
 * @property string $nome
 * @property int $imagem_id
 * @property string $detalhe
 * @property float $prioridade
 * @property string $url
 *
 * @property \App\Model\Entity\Imagen $imagen
 * @property \App\Model\Entity\OdontoProduto[] $odonto_produtos
 */
class OdontoOperadora extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
