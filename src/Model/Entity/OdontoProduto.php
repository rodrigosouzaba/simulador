<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OdontoProduto Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property int $odonto_operadora_id
 * @property int $carencia_id
 * @property int $rede_id
 * @property int $reembolso_id
 * @property int $opcional_id
 * @property int $informacao_id
 * @property int $observacao_id
 *
 * @property \App\Model\Entity\OdontoOperadora $odonto_operadora
 * @property \App\Model\Entity\Carencia $carencia
 * @property \App\Model\Entity\Rede $rede
 * @property \App\Model\Entity\Reembolso $reembolso
 * @property \App\Model\Entity\Opcionai $opcionai
 * @property \App\Model\Entity\Informaco $informaco
 * @property \App\Model\Entity\Observaco $observaco
 * @property \App\Model\Entity\OdontoTabela[] $odonto_tabelas
 */
class OdontoProduto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
