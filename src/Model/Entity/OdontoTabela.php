<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OdontoTabela Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property \Cake\I18n\Time $vigencia
 * @property float $preco_vida
 * @property string $validade
 * @property int $minimo_vidas
 * @property int $maximo_vidas
 * @property int $titulares
 * @property float $prioridade
 * @property string $reembolso
 * @property string $tipo_pessoa
 * @property int $odonto_comercializacao_id
 * @property int $odonto_atendimento_id
 * @property int $odonto_produto_id
 * @property int $estado_id
 *
 * @property \App\Model\Entity\OdontoComercializaco $odonto_comercializaco
 * @property \App\Model\Entity\OdontoAtendimento $odonto_atendimento
 * @property \App\Model\Entity\OdontoProduto $odonto_produto
 * @property \App\Model\Entity\Estado $estado
 */
class OdontoTabela extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
