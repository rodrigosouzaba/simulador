<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OperadorasFechadasArquivo Entity
 *
 * @property int $id
 * @property int $operadora_fechada_id
 * @property int $arquivo_id
 *
 * @property \App\Model\Entity\OperadorasFechada $operadoras_fechada
 * @property \App\Model\Entity\Arquivo $arquivo
 */
class OperadorasFechadasArquivo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
