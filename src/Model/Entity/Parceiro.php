<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Parceiro Entity
 *
 * @property int $id
 * @property string $rg
 * @property string $nome
 * @property string $cpf
 * @property string $email
 * @property \Cake\I18n\Time $data_nascimento
 * @property \Cake\I18n\Time $created
 * @property string $cnpj_corpar
 * @property string $razao_social
 */
class Parceiro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
