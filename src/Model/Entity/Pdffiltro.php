<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pdffiltro Entity
 *
 * @property int $id
 * @property int $simulacao_id
 * @property int $abrangencia_id
 * @property int $tipo_id
 * @property int $tipo_produto_id
 * @property string $coparticipacao
 * @property int $tipo_contratacao
 * @property string $filtroreembolso
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Abrangencia $abrangencia
 * @property \App\Model\Entity\Tipo $tipo
 * @property \App\Model\Entity\TiposProduto $tipos_produto
 * @property \App\Model\Entity\Simulaco[] $simulacoes
 */
class Pdffiltro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
