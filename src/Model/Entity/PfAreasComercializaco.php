<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfAreasComercializaco Entity
 *
 * @property int $id
 * @property string|null $descricao
 * @property string $nome
 * @property int $pf_operadora_id
 * @property int $pf_atendimento_id
 *
 * @property \App\Model\Entity\PfOperadora $pf_operadora
 * @property \App\Model\Entity\PfAtendimento $pf_atendimento
 */
class PfAreasComercializaco extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'nome' => true,
        'pf_operadora_id' => true,
        'pf_operadora' => true,
        'municipios' => true,
        'metropoles' => true,
    ];
}
