<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfAreasComercializacoesEstadosMunicipio Entity
 *
 * @property int $id
 * @property int|null $estado_id
 * @property int $pf_comercializacao_id
 * @property int $municipio_id
 *
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\PfComercializacao $pf_comercializacao
 * @property \App\Model\Entity\Municipio $municipio
 */
class PfAreasComercializacoesEstadosMunicipio extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'estado_id' => true,
        'pf_comercializacao_id' => true,
        'municipio_id' => true,
        'estado' => true,
        'pf_areas_comercializacao' => true,
        'municipio' => true,
    ];
}
