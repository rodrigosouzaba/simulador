<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfCalculo Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $email
 * @property int $faixa1
 * @property int $faixa2
 * @property int $faixa3
 * @property int $faixa4
 * @property int $faixa5
 * @property int $faixa6
 * @property int $faixa7
 * @property int $faixa8
 * @property int $faixa9
 * @property int $faixa10
 * @property int $faixa11
 * @property int $faixa12
 * @property int $idade_titular
 * @property int $modalidade
 * @property int $pf_profissao_id
 * @property \Cake\I18n\Time $created
 * @property int $telefone
 * @property int $user_id
 * @property string $calculoremoto
 *
 * @property \App\Model\Entity\PfProfisso $pf_profisso
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\PfCalculosDependente[] $pf_calculos_dependentes
 * @property \App\Model\Entity\Tabela[] $tabelas
 */
class PfCalculo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
