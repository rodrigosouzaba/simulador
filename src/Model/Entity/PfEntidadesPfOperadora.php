<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfEntidadesPfOperadora Entity
 *
 * @property int $id
 * @property int $pf_entidade_id
 * @property int $pf_operadora_id
 *
 * @property \App\Model\Entity\PfEntidade $pf_entidade
 * @property \App\Model\Entity\PfOperadora $pf_operadora
 */
class PfEntidadesPfOperadora extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
