<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfEntidadesProfisso Entity
 *
 * @property int $id
 * @property int $pf_entidade_id
 * @property int $pf_profissao_id
 *
 * @property \App\Model\Entity\PfEntidade $pf_entidade
 * @property \App\Model\Entity\PfProfisso $pf_profisso
 */
class PfEntidadesProfisso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
