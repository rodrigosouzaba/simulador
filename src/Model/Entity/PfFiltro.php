<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfFiltro Entity
 *
 * @property int $id
 * @property int $pf_calculo_id
 * @property \Cake\I18n\Time $created
 * @property int $pf_atendimento_id
 * @property string $coparticipacao
 * @property int $pf_acomodacao_id
 * @property int $tipo_produto_id
 * @property int $user_id
 * @property int $carencia
 * @property int $reembolso
 * @property int $rede
 * @property int $documento
 * @property int $dependente
 * @property int $observacao
 *
 * @property \App\Model\Entity\PfCalculo $pf_calculo
 * @property \App\Model\Entity\PfAtendimento $pf_atendimento
 * @property \App\Model\Entity\PfAcomodaco $pf_acomodaco
 * @property \App\Model\Entity\TiposProduto $tipos_produto
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Tabela[] $tabelas
 */
class PfFiltro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
