<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfFiltrosTabela Entity
 *
 * @property int $id
 * @property int $pf_filtro_id
 * @property int $pf_tabela_id
 *
 * @property \App\Model\Entity\PfFiltro $pf_filtro
 * @property \App\Model\Entity\PfTabela $pf_tabela
 */
class PfFiltrosTabela extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
