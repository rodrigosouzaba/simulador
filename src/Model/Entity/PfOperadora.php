<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfOperadora Entity
 *
 * @property int $id
 * @property string $nome
 * @property int $imagem_id
 * @property string $detalhe
 * @property float $prioridade
 * @property string $url
 *
 * @property \App\Model\Entity\Imagen $imagen
 * @property \App\Model\Entity\PfCarencia[] $pf_carencias
 * @property \App\Model\Entity\PfComercializaco[] $pf_comercializacoes
 * @property \App\Model\Entity\PfDependente[] $pf_dependentes
 * @property \App\Model\Entity\PfDocumento[] $pf_documentos
 * @property \App\Model\Entity\PfObservaco[] $pf_observacoes
 * @property \App\Model\Entity\PfProduto[] $pf_produtos
 * @property \App\Model\Entity\PfRede[] $pf_redes
 * @property \App\Model\Entity\PfReembolso[] $pf_reembolsos
 * @property \App\Model\Entity\PfTabela[] $pf_tabelas
 */
class PfOperadora extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
