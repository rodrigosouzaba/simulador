<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfOperadorasFechadasArquivo Entity
 *
 * @property int $id
 * @property int $pf_operadora_fechada_id
 * @property int $arquivo_id
 *
 * @property \App\Model\Entity\PfOperadorasFechada $pf_operadoras_fechada
 * @property \App\Model\Entity\Arquivo $arquivo
 */
class PfOperadorasFechadasArquivo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
