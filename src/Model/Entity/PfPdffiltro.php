<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfPdffiltro Entity
 *
 * @property int $id
 * @property int $pf_calculo_id
 * @property int $pf_atendimento_id
 * @property int $pf_acomodacao_id
 * @property int $tipo_produto_id
 * @property string $coparticipacao
 * @property string $filtroreembolso
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\PfCalculo $pf_calculo
 * @property \App\Model\Entity\PfAtendimento $pf_atendimento
 * @property \App\Model\Entity\PfAcomodacao $pf_acomodacao
 * @property \App\Model\Entity\TipoProduto $tipo_produto
 */
class PfPdffiltro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
