<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfProduto Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property int $pf_operadora_id
 * @property int $pf_carencia_id
 * @property int $pf_rede_id
 * @property int $pf_reembolso_id
 * @property int $pf_documento_id
 * @property int $pf_dependente_id
 * @property int $pf_observacao_id
 * @property int $tipo_produto_id
 *
 * @property \App\Model\Entity\PfOperadora $pf_operadora
 * @property \App\Model\Entity\PfCarencia $pf_carencia
 * @property \App\Model\Entity\PfRede $pf_rede
 * @property \App\Model\Entity\PfReembolso $pf_reembolso
 * @property \App\Model\Entity\PfDocumento $pf_documento
 * @property \App\Model\Entity\PfDependente $pf_dependente
 * @property \App\Model\Entity\PfObservaco $pf_observaco
 * @property \App\Model\Entity\TiposProduto $tipos_produto
 * @property \App\Model\Entity\PfTabela[] $pf_tabelas
 */
class PfProduto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
