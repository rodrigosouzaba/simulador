<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PfTabela Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property \Cake\I18n\Time $vigencia
 * @property float $faixa1
 * @property float $faixa2
 * @property float $faixa3
 * @property float $faixa4
 * @property float $faixa5
 * @property float $faixa6
 * @property float $faixa7
 * @property float $faixa8
 * @property float $faixa9
 * @property float $faixa10
 * @property float $faixa11
 * @property float $faixa12
 * @property int $pf_produto_id
 * @property int $pf_operadora_id
 * @property int $pf_atendimento_id
 * @property int $pf_acomodacao_id
 * @property string $validade
 * @property int $estado_id
 * @property int $pf_comercializacao_id
 * @property string $cod_ans
 * @property int $minimo_vidas
 * @property int $maximo_vidas
 * @property float $prioridade
 * @property string $coparticipacao
 * @property string $detalhe_coparticipacao
 * @property string $modalidade
 * @property string $reembolso
 *
 * @property \App\Model\Entity\PfProduto $pf_produto
 * @property \App\Model\Entity\PfOperadora $pf_operadora
 * @property \App\Model\Entity\PfAtendimento $pf_atendimento
 * @property \App\Model\Entity\PfAcomodaco $pf_acomodaco
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Regio $regio
 * @property \App\Model\Entity\PfProfissoesTabela[] $pf_profissoes_tabelas
 */
class PfTabela extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
