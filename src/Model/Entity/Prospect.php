<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Prospect Entity
 *
 * @property int $id
 * @property string $email
 * @property string $celular
 * @property \Cake\I18n\Time $created
 * @property string $nome
 * @property string $sobrenome
 * @property \Cake\I18n\Time $data_nascimento
 * @property string $cpf
 * @property string $cnpj
 * @property string $susep
 * @property string $telefone
 * @property string $celular_secundario
 * @property string $whatsapp
 */
class Prospect extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
