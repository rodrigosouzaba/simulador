<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SimulacoesComentario Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property string $mensagem
 * @property string $alerta
 * @property \Cake\I18n\Time $data_alerta
 * @property int $simulacao_id
 *
 * @property \App\Model\Entity\Simulaco $simulaco
 */
class SimulacoesComentario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
