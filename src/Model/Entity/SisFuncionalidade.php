<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Tipo Entity
 *
 * @property int $id
 * @property string $descricao
 * @property string $nome
 * @property string $flag
 *
 */
class SisFuncionalidade extends Entity
{

    protected $_virtual = ['ativo', 'ativo_label', 'countPermissionsAssociated', 'countGroupsAssociateds'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected function _getAtivo(){
        return ($this->active == '1') ? 'Sim' : 'Não';
    }

    protected function _getAtivoLabel(){
        return ($this->active == '1') ? 'success' : 'danger';
    }

    protected function _getCountGroupsAssociateds(){
        $funPermissoes = TableRegistry::get('sis_funcionalidades_sis_grupos');
        return $funPermissoes->find('all')
                        ->where(['sis_funcionalidade_id' => $this->id])
                        ->count();
    }

    protected function _getCountPermissionsAssociated(){
        $funPermissoes = TableRegistry::get('sis_funcionalidades_sis_permissoes');
        return $funPermissoes->find('all')
                        ->where(['sis_funcionalidade_id' => $this->id])
                        ->count();  
    }
    
}
