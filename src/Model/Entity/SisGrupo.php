<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tipo Entity
 *
 * @property int $id
 * @property string $descricao
 * @property string $nome
 * @property string $flag
 *
 */
class SisGrupo extends Entity
{

    protected $_virtual = [ 'ativo', 
                            'ativo_label',
                            'countGruposAssociados', 
                            'countPermissionsAssociated',
                        ];

    protected function _getAtivo()
    {
        return ($this->active == '1') ? 'Sim' : 'Não';
    }

    protected function _getAtivoLabel()
    {
        return ($this->active == '1') ? 'success' : 'danger';
    }

    protected function _getCountGruposAssociados()
    {
        return  '1';
        // $dados = DB::table('sis_funcionalidades_sis_grupos')->where('funcionalidade_id', $this->id)->count();
        
        // return $dados;
    }
    
    protected function _getCountPermissionsAssociated()
    {
        return '1';
    }
    

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    
}
