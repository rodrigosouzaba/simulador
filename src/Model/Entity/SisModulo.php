<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Tipo Entity
 *
 * @property int $id
 * @property string $descricao
 * @property string $nome
 * @property string $flag
 *
 */
class SisModulo extends Entity
{

    protected $_virtual = ['ativo', 'ativo_label', 'funcionalidadesAssociadas'];

    protected function _getAtivo()
    {
        return ($this->active == '1') ? 'Sim' : 'Não';
    }

    protected function _getAtivoLabel()
    {
        return ($this->active == '1') ? 'success' : 'danger';
    }

    protected function _getFuncionalidadesAssociadas()
    {
        $funPermissoes = TableRegistry::get('sis_funcionalidades');
        return $funPermissoes->find('all')
                        ->where(['sis_modulo_id' => $this->id])
                        ->count(); 
       
    }


    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
