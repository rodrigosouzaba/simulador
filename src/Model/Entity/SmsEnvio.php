<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SmsEnvio Entity
 *
 * @property int $id
 * @property int $sms_lista_id
 * @property int $sms_situacao_id
 * @property int|null $sms_cabecalho_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property string $mensagem
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime|null $data_envio
 * @property \Cake\I18n\FrozenTime|null $data_agendamento
 * @property int|null $autorizado
 * @property string|null $descricao
 * @property string|null $tipo_envio
 *
 * @property \App\Model\Entity\SmsCabecalho $sms_cabecalho
 * @property \App\Model\Entity\SmsLista $sms_lista
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\SmsSituaco $sms_situaco
 * @property \App\Model\Entity\SmsRespostasEnvio[] $sms_respostas_envios
 */
class SmsEnvio extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sms_lista_id' => true,
        'sms_situacao_id' => true,
        'sms_cabecalho_id' => true,
        'created' => true,
        'mensagem' => true,
        'user_id' => true,
        'data_envio' => true,
        'data_agendamento' => true,
        'autorizado' => true,
        'descricao' => true,
        'tipo_envio' => true,
        'sms_cabecalho' => true,
        'sms_lista' => true,
        'user' => true,
        'sms_situaco' => true,
        'sms_respostas_envios' => true,
    ];
}
