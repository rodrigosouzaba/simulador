<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SmsLista Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property string $caminho
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Envio[] $envios
 * @property \App\Model\Entity\SmsEnvio[] $sms_envios
 */
class SmsLista extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
