<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tabela Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property int $produto_id
 * @property string $acomodacao
 * @property \Cake\I18n\Time $vigencia
 * @property int $faixa1
 * @property int $faixa2
 * @property int $faixa3
 *
 * @property \App\Model\Entity\Produto $produto
 * @property \App\Model\Entity\Regio[] $regioes
 */
class Tabela extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
