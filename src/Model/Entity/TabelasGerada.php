<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TabelasGerada Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $estado_id
 * @property string $ramo
 * @property string $operadora
 * @property string $vidas
 * @property string $coparticipacao
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Estado $estado
 */
class TabelasGerada extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
