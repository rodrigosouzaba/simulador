<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TabelasGeradasProduto Entity
 *
 * @property int $id
 * @property int $tabela_gerada_id
 * @property int $produto_id
 *
 * @property \App\Model\Entity\TabelasGerada $tabelas_gerada
 * @property \App\Model\Entity\Produto $produto
 */
class TabelasGeradasProduto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
