<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VendasOnlinesOperadora Entity
 *
 * @property int $id
 * @property string $link
 * @property string $imagem
 * @property string $linha1
 * @property string $linha2
 * @property string $tooltip
 * @property int $prioridade
 * @property string $link_video
 * @property string $aviso
 * @property int $id_venda_online
 * @property string $manual
 * @property int $status
 * @property int $redirect
 * @property int $estado_id
 * @property string $ramo
 * @property int $operadora_id
 *
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Operadora $operadora
 * @property \App\Model\Entity\LinksPersonalizado[] $links_personalizados
 */class VendasOnlinesOperadora extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
