<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Alertas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Simulacoes
 * @property \Cake\ORM\Association\BelongsTo $PfCalculos
 * @property \Cake\ORM\Association\BelongsTo $OdontoCalculos
 *
 * @method \App\Model\Entity\Alerta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Alerta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Alerta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Alerta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Alerta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AlertasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('alertas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Simulacoes', [
            'foreignKey' => 'simulacao_id'
        ]);
        $this->belongsTo('PfCalculos', [
            'foreignKey' => 'pf_calculo_id'
        ]);
        $this->belongsTo('OdontoCalculos', [
            'foreignKey' => 'odonto_calculo_id'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('model', 'create')
            ->notEmpty('model');

        $validator
            ->date('data_alerta')
            ->allowEmpty('data_alerta');

        $validator
            ->allowEmpty('mensagem');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['simulacao_id'], 'Simulacoes'));
        $rules->add($rules->existsIn(['pf_calculo_id'], 'PfCalculos'));
        $rules->add($rules->existsIn(['odonto_calculo_id'], 'OdontoCalculos'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));

        return $rules;
    }



    /*
	    
	    Método para criação de Histórico ao alterar status do cálculo
	    
    */
    function criarAlerta($user = null, $model = null, $id = null, $status = null)
    {
        switch ($model) {
            case "SPF":
                $colunaId = "pf_calculo_id";
                break;
            case "OPF":
                $colunaId = "odonto_calculo_id";
                break;
            case "SPJ":
                $colunaId = "simulacao_id";
                break;
            case "OPJ":
                $colunaId = "odonto_calculo_id";
                break;
        }
        $alerta = $this->newEntity();
        $alerta->user_id = $user;
        $alerta->model = $model;
        $alerta->mensagem = "Alteração de Status";
        $alerta->status_id = $status;
        $alerta->$colunaId = $id;

        $this->save($alerta);
    }

    function visualizarAlerta($model = null, $id = null, $user = null)
    {
        switch ($model) {
            case "SPF":
                $colunaId = "pf_calculo_id";
                break;
            case "OPF":
                $colunaId = "odonto_calculo_id";
                break;
            case "SPJ":
                $colunaId = "simulacao_id";
                break;
            case "OPJ":
                $colunaId = "odonto_calculo_id";
                break;
        }
        $alertas = $this->find('all')->where(['model' => $model, $colunaId => $id, "user_id" => $user])->toArray();

        if (!empty($alertas)) {
            foreach ($alertas as $alerta) {
                $ids[$alerta["id"]] = $alerta["id"];
            }
            $this->updateAll(["visualizado" => "S"], ["id IN" => $ids]);
        }
    }


    function checarVencidos($user = null)
    {
        $ids = null;
        $alertasVencidos = $this->find("all")->where(["Alertas.user_id" => $user, "OR" => ["Alertas.visualizado IS NULL", "Alertas.visualizado" => ''], "Alertas.data_alerta < CURRENT_DATE()"])->contain(["Simulacoes", "PfCalculos", "OdontoCalculos"])->toArray();
        if (!empty($alertasVencidos)) {
            foreach ($alertasVencidos as $alertaVencido) {
                switch ($alertaVencido["model"]) {
                    case "SPJ":
                        $coluna = "simulacao_id";
                        break;
                    case "SPF":
                        $coluna = "pf_calculo_id";
                        break;
                    case "OPJ":
                        $coluna = "odonto_calculo_id";
                        break;
                    case "OPF":
                        $coluna = "odonto_calculo_id";
                        break;
                }

                $ids[$alertaVencido[$coluna]] = $alertaVencido["model"];
            }
        }
        return $ids;
    }
}
