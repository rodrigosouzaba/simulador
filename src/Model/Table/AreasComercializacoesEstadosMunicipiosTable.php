<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AreasComercializacoesEstadosMunicipios Model
 *
 * @property \App\Model\Table\EstadosTable&\Cake\ORM\Association\BelongsTo $Estados
 * @property \App\Model\Table\AreaComercializacaosTable&\Cake\ORM\Association\BelongsTo $AreaComercializacaos
 * @property \App\Model\Table\MunicipiosTable&\Cake\ORM\Association\BelongsTo $Municipios
 *
 * @method \App\Model\Entity\AreasComercializacoesEstadosMunicipio get($primaryKey, $options = [])
 * @method \App\Model\Entity\AreasComercializacoesEstadosMunicipio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AreasComercializacoesEstadosMunicipio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AreasComercializacoesEstadosMunicipio|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AreasComercializacoesEstadosMunicipio saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AreasComercializacoesEstadosMunicipio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AreasComercializacoesEstadosMunicipio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AreasComercializacoesEstadosMunicipio findOrCreate($search, callable $callback = null, $options = [])
 */
class AreasComercializacoesEstadosMunicipiosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('areas_comercializacoes_estados_municipios');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('AreasComercializacoes', [
            'foreignKey' => 'area_comercializacao_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['area_comercializacao_id'], 'AreasComercializacoes'));
        $rules->add($rules->existsIn(['municipio_id'], 'Municipios'));

        return $rules;
    }
}
