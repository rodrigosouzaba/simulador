<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AreasComercializacoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Operadoras
 * @property \Cake\ORM\Association\BelongsTo $Abrangencias
 *
 * @method \App\Model\Entity\AreasComercializaco get($primaryKey, $options = [])
 * @method \App\Model\Entity\AreasComercializaco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AreasComercializaco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AreasComercializaco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AreasComercializaco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AreasComercializaco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AreasComercializaco findOrCreate($search, callable $callback = null)
 */
class AreasComercializacoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('areas_comercializacoes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Tabelas', [
            'foreignKey' => 'area_comercializacao_id'
        ]);
        $this->belongsTo('Operadoras', [
            'foreignKey' => 'operadora_id',
            // 'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Estados', [
            'joinTable' => 'areas_comercializacoes_estados_municipios',
            'foreignKey' => 'area_comercializacao_id'
        ]);
        $this->belongsToMany('Metropoles', [
            'foreignKey' => 'area_comercializacao_id',
            'targetForeignKey' => 'metropole_id',
            'joinTable' => 'areas_comercializacoes_metropoles',
        ]);
        $this->belongsToMany('Municipios', [
            'joinTable' => 'areas_comercializacoes_estados_municipios',
            'foreignKey' => 'area_comercializacao_id'
        ]);
        $this->hasOne('AreasComercializacoesEstadosMunicipios', [
            'joinTable' => 'areas_comercializacoes_estados_municipios',
            'foreignKey' => 'area_comercializacao_id',
            'joinType' => 'left'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));

        return $rules;
    }
}
