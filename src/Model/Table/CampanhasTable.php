<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Campanhas Model
 *
 * @method \App\Model\Entity\Campanha get($primaryKey, $options = [])
 * @method \App\Model\Entity\Campanha newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Campanha[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Campanha|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Campanha patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Campanha[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Campanha findOrCreate($search, callable $callback = null)
 */
class CampanhasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('campanhas');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('tipo_campanha')
            ->allowEmpty('tipo_campanha');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('url');

        $validator
            ->allowEmpty('descricao');

        return $validator;
    }
}
