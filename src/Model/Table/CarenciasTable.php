<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Carencias Model
 *
 * @property \Cake\ORM\Association\HasMany $Tabelas
 *
 * @method \App\Model\Entity\Carencia get($primaryKey, $options = [])
 * @method \App\Model\Entity\Carencia newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Carencia[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Carencia|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Carencia patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Carencia[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Carencia findOrCreate($search, callable $callback = null)
 */
class CarenciasTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('carencias');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Operadoras', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER'
        ]);

//        $this->hasMany('Operadoras', [
//            'foreignKey' => 'carencia_id'
//        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('descricao', 'create')
                ->notEmpty('descricao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));
//        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));

        return $rules;
    }

}
