<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cnpjs Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Tabelas
 *
 * @method \App\Model\Entity\Cnpj get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cnpj newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cnpj[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cnpj|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cnpj patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cnpj[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cnpj findOrCreate($search, callable $callback = null)
 */
class CnpjsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cnpjs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Tabelas', [
            'foreignKey' => 'cnpj_id',
            'targetForeignKey' => 'tabela_id',
            'joinTable' => 'tabelas_cnpjs'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
