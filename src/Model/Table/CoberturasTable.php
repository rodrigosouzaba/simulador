<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Coberturas Model
 *
 * @method \App\Model\Entity\Cobertura get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cobertura newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cobertura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cobertura|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cobertura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cobertura[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cobertura findOrCreate($search, callable $callback = null)
 */class CoberturasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('coberturas');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        $validator
            ->requirePresence('nome', 'create')            ->notEmpty('nome');
        $validator
            ->integer('prioridade')            ->allowEmpty('prioridade');
        return $validator;
    }
}
