<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * DetalhesPerfisEmpresariais Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PerfisEmpresariais
 *
 * @method \App\Model\Entity\DetalhesPerfisEmpresariai get($primaryKey, $options = [])
 * @method \App\Model\Entity\DetalhesPerfisEmpresariai newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DetalhesPerfisEmpresariai[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DetalhesPerfisEmpresariai|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DetalhesPerfisEmpresariai patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DetalhesPerfisEmpresariai[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DetalhesPerfisEmpresariai findOrCreate($search, callable $callback = null)
 */
class DetalhesPerfisEmpresariaisTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('detalhes_perfis_empresariais');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PerfisEmpresariais', [
            'foreignKey' => 'perfil_empresarial_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('tipo', 'create')
                ->notEmpty('tipo');

        $validator
                ->requirePresence('sexo', 'create')
                ->notEmpty('sexo');
//
//        $validator
//                ->add('data_nascimento', 'valid',['rule' => ['date', 'ymd']])
//                ->date('data_nascimento')
//                ->requirePresence('data_nascimento', 'create')
//                ->notEmpty('data_nascimento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['perfil_empresarial_id'], 'PerfisEmpresariais'));

        return $rules;
    }

}
