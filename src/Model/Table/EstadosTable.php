<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Estados Model
 *
 * @property \Cake\ORM\Association\HasMany $Regioes
 *
 * @method \App\Model\Entity\Estado get($primaryKey, $options = [])
 * @method \App\Model\Entity\Estado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Estado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Estado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Estado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Estado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Estado findOrCreate($search, callable $callback = null)
 */
class EstadosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('estados');
        $this->setDisplayField('nome');
        $this->setPrimaryKey('id');

        $this->hasMany('Regioes', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('Municipios', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('Metropoles', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('OdontoOperadoras', [
            'foreignKey' => 'estado_id'
        ]);
        $this->belongsToMany('VendasOnlinesOperadoras', [
            'foreignKey' => 'estado_id',
            'targetForeignKey' => 'venda_online_operadora_id',
            'joinTable' => 'vendas_onlines_operadoras_estados',
        ]);
        $this->belongsToMany('PfAreasComercializacoes', [
            'foreignKey' => 'estado_id',
            'targetForeignKey' => 'pf_comercializacao_id',
            'joinTable' => 'pf_areas_comercializacoes_estados_municipios',
        ]);
        $this->belongsToMany('AreasComercializacoes', [
            'foreignKey' => 'estado_id',
            'targetForeignKey' => 'area_comercializacao_id',
            'joinTable' => 'areas_comercializacoes_estados_municipios',
        ]);
        $this->belongsToMany('OdontoAreasComercializacoes', [
            'foreignKey' => 'estado_id',
            'targetForeignKey' => 'odonto_comercializacao_id',
            'joinTable' => 'odonto_areas_comercializacoes_estados_municipios',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
