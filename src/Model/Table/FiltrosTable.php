<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Filtros Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Simulacoes
 * @property \Cake\ORM\Association\BelongsTo $TiposProdutos
 * @property \Cake\ORM\Association\BelongsTo $Tipos
 * @property \Cake\ORM\Association\BelongsTo $Abrangencias
 * @property \Cake\ORM\Association\BelongsTo $Regioes
 * @property \Cake\ORM\Association\HasMany $TabelasFiltradas
 *
 * @method \App\Model\Entity\Filtro get($primaryKey, $options = [])
 * @method \App\Model\Entity\Filtro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Filtro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Filtro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Filtro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Filtro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Filtro findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FiltrosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('filtros');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Simulacoes', [
            'foreignKey' => 'simulacao_id'
        ]);
        $this->belongsTo('TiposProdutos', [
            'foreignKey' => 'tipo_produto_id'
        ]);
        $this->belongsTo('Tipos', [
            'foreignKey' => 'tipo_id'
        ]);
        $this->belongsTo('Abrangencias', [
            'foreignKey' => 'abrangencia_id'
        ]);
        $this->hasMany('TabelasFiltradas', [
            'foreignKey' => 'filtro_id'
        ]);
        $this->belongsToMany('Tabelas', [
            'foreignKey' => 'filtro_id',
            'targetForeignKey' => 'tabela_id',
            'joinTable' => 'tabelas_filtradas',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->allowEmpty('coparticipacao');

        $validator
            ->allowEmpty('informacao');

        $validator
            ->allowEmpty('reembolso');

        $validator
            ->allowEmpty('carencia');

        $validator
            ->allowEmpty('opcional');

        $validator
            ->allowEmpty('rede');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['simulacao_id'], 'Simulacoes'));
        $rules->add($rules->existsIn(['tipo_produto_id'], 'TiposProdutos'));
        $rules->add($rules->existsIn(['tipo_id'], 'Tipos'));
        $rules->add($rules->existsIn(['abrangencia_id'], 'Abrangencias'));

        return $rules;
    }
}
