<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FormasPagamentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Operadoras
 *
 * @method \App\Model\Entity\FormasPagamento get($primaryKey, $options = [])
 * @method \App\Model\Entity\FormasPagamento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FormasPagamento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FormasPagamento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FormasPagamento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FormasPagamento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FormasPagamento findOrCreate($search, callable $callback = null)
 */
class FormasPagamentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('formas_pagamentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Operadoras', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));

        return $rules;
    }
}
