<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Funcionalidades Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Grupos
 *
 * @method \App\Model\Entity\Funcionalidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\Funcionalidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Funcionalidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Funcionalidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Funcionalidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Funcionalidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Funcionalidade findOrCreate($search, callable $callback = null)
 */ class FuncionalidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('funcionalidades');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Grupos', [
            'foreignKey' => 'funcionalidade_id',
            'targetForeignKey' => 'grupo_id',
            'joinTable' => 'grupos_funcionalidades'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')->allowEmpty('id', 'create');
        $validator
            ->requirePresence('funcionalidade', 'create');
        $validator
            ->requirePresence('descricao', 'create')->allowEmpty('descricao');
        return $validator;
    }
}
