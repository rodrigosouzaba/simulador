<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GruposFuncionalidades Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Grupos
 * @property \Cake\ORM\Association\BelongsTo $Funcionalidades
 *
 * @method \App\Model\Entity\GruposFuncionalidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\GruposFuncionalidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\GruposFuncionalidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GruposFuncionalidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GruposFuncionalidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GruposFuncionalidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\GruposFuncionalidade findOrCreate($search, callable $callback = null)
 */class GruposFuncionalidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('grupos_funcionalidades');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Grupos', [
            'foreignKey' => 'grupo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Funcionalidades', [
            'foreignKey' => 'funcionalidade_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['grupo_id'], 'Grupos'));
        $rules->add($rules->existsIn(['funcionalidade_id'], 'Funcionalidades'));

        return $rules;
    }
}
