<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Abrangencias Model
 *
 * @property \Cake\ORM\Association\HasMany $Tabelas
 *
 * @method \App\Model\Entity\Abrangencia get($primaryKey, $options = [])
 * @method \App\Model\Entity\Abrangencia newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Abrangencia[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Abrangencia|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Abrangencia patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Abrangencia[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Abrangencia findOrCreate($search, callable $callback = null)
 */


class ImagensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->table('imagens');
        $this->displayField('id');
        $this->primaryKey('id');
        
          $this->hasMany('Operadoras', [
            'foreignKey' => 'imagem_id'
        ]);

      
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }
}
