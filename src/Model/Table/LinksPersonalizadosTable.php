<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LinksPersonalizados Model
 *
 * @property \Cake\ORM\Association\BelongsTo $VendasOnlinesOperadoras
 *
 * @method \App\Model\Entity\LinksPersonalizado get($primaryKey, $options = [])
 * @method \App\Model\Entity\LinksPersonalizado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LinksPersonalizado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LinksPersonalizado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LinksPersonalizado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LinksPersonalizado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LinksPersonalizado findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */class LinksPersonalizadosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('links_personalizados');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('VendasOnlinesOperadoras', [
            'foreignKey' => 'vendas_onlines_operadora_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        $validator
            ->requirePresence('cpf', 'create')            ->notEmpty('cpf');
        $validator
            ->requirePresence('link', 'create')            ->notEmpty('link');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['vendas_onlines_operadora_id'], 'VendasOnlinesOperadoras'));

        return $rules;
    }
}
