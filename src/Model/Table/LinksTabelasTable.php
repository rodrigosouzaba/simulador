<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LinksTabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Links
 * @property \Cake\ORM\Association\BelongsTo $Tabelas
 * @property \Cake\ORM\Association\BelongsTo $PfTabelas
 * @property \Cake\ORM\Association\BelongsTo $OdontoTabelas
 *
 * @method \App\Model\Entity\LinksTabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\LinksTabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LinksTabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LinksTabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LinksTabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LinksTabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LinksTabela findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LinksTabelasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('links_tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Links', [
            'foreignKey' => 'link_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tabelas', [
            'foreignKey' => 'tabela_id'
        ]);
        $this->belongsTo('PfTabelas', [
            'foreignKey' => 'pf_tabela_id'
        ]);
        $this->belongsTo('OdontoTabelas', [
            'foreignKey' => 'odonto_tabela_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['link_id'], 'Links'));
        $rules->add($rules->existsIn(['tabela_id'], 'Tabelas'));
        $rules->add($rules->existsIn(['pf_tabela_id'], 'PfTabelas'));
        $rules->add($rules->existsIn(['odonto_tabela_id'], 'OdontoTabelas'));

        return $rules;
    }

    // public function beforeDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    // {
    //     return false;
    // }
}
