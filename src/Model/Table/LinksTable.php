<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Links Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Tabelas
 *
 * @method \App\Model\Entity\Link get($primaryKey, $options = [])
 * @method \App\Model\Entity\Link newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Link[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Link|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Link patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Link[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Link findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LinksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('links');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Tabelas', [
            'foreignKey' => 'link_id',
            'targetForeignKey' => 'tabela_id',
            'joinTable' => 'links_tabelas'
        ]);
        
        $this->belongsToMany('PfTabelas', [
            'foreignKey' => 'link_id',
            'targetForeignKey' => 'pf_tabela_id',
            'joinTable' => 'links_tabelas'
        ]);
        
        $this->belongsToMany('OdontoTabelas', [
            'foreignKey' => 'link_id',
            'targetForeignKey' => 'odonto_tabela_id',
            'joinTable' => 'links_tabelas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

/*
        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');
*/

        $validator
            ->allowEmpty('descricao');

        $validator
            ->requirePresence('usuario', 'create')
            ->notEmpty('usuario');

        $validator
            ->requirePresence('ramo', 'create')
            ->notEmpty('ramo');

/*
        $validator
            ->requirePresence('url', 'create')
            ->notEmpty('url');
*/

        return $validator;
    }
}