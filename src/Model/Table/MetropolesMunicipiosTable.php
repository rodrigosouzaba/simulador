<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MetropolesMunicipios Model
 *
 * @property \App\Model\Table\MetropolesTable&\Cake\ORM\Association\BelongsTo $Metropoles
 * @property \App\Model\Table\MunicipiosTable&\Cake\ORM\Association\BelongsTo $Municipios
 *
 * @method \App\Model\Entity\MetropolesMunicipio get($primaryKey, $options = [])
 * @method \App\Model\Entity\MetropolesMunicipio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MetropolesMunicipio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MetropolesMunicipio|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MetropolesMunicipio saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MetropolesMunicipio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MetropolesMunicipio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MetropolesMunicipio findOrCreate($search, callable $callback = null, $options = [])
 */
class MetropolesMunicipiosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('metropoles_municipios');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Metropoles', [
            'foreignKey' => 'metropole_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['metropole_id'], 'Metropoles'));
        $rules->add($rules->existsIn(['municipio_id'], 'Municipios'));

        return $rules;
    }
}
