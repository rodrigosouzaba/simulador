<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Metropoles Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Estados
 * @property \App\Model\Table\MunicipiosTable&\Cake\ORM\Association\BelongsToMany $Municipios
 *
 * @method \App\Model\Entity\Metropole get($primaryKey, $options = [])
 * @method \App\Model\Entity\Metropole newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Metropole[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Metropole|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Metropole saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Metropole patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Metropole[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Metropole findOrCreate($search, callable $callback = null, $options = [])
 */
class MetropolesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('metropoles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('Municipios', [
            'foreignKey' => 'metropole_id',
            'targetForeignKey' => 'municipio_id',
            'joinTable' => 'metropoles_municipios',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 100)
            ->requirePresence('nome', 'create')
            ->notEmptyString('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));

        return $rules;
    }
}
