<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MidiasCompartilhadasOperadorasEstados Model
 *
 * @property \App\Model\Table\EstadosTable&\Cake\ORM\Association\BelongsTo $Estados
 * @property \App\Model\Table\OperadorasTable&\Cake\ORM\Association\BelongsTo $Operadoras
 *
 * @method \App\Model\Entity\MidiasCompartilhada get($primaryKey, $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada findOrCreate($search, callable $callback = null, $options = [])
 */
class MidiasCompartilhadasOperadorasEstadosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('midias_compartilhadas_operadoras_estados');
        $this->setPrimaryKey('id');

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
        ]);
        
        $this->belongsTo('MidiasCompartilhadasOperadoras', [
           'foreignKey' => 'midia_compartilhada_operadora_id'
        ]);
        
    }

    // /**
    //  * Returns a rules checker object that will be used for validating
    //  * application integrity.
    //  *
    //  * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
    //  * @return \Cake\ORM\RulesChecker
    //  */
    // public function buildRules(RulesChecker $rules)
    // {
    //     $rules->add($rules->existsIn(['estado_id'], 'Estados'));
    //     $rules->add($rules->existsIn(['midia_compartilhada_operadora_id'], 'MidasCompartilhadasOperadoras'));

    //     return $rules;
    // }
}
