<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MidiasCompartilhadasEstadosOperadoras Model
 *
 * @property \App\Model\Table\EstadosTable&\Cake\ORM\Association\BelongsTo $Estados
 * @property \App\Model\Table\OperadorasTable&\Cake\ORM\Association\BelongsTo $Operadoras
 *
 * @method \App\Model\Entity\MidiasCompartilhada get($primaryKey, $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada findOrCreate($search, callable $callback = null, $options = [])
 */
class MidiasCompartilhadasOperadorasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('midias_compartilhadas_operadoras');
        $this->setPrimaryKey('id');

        $this->belongsTo('MidiasCompartilhadas', [
           'foreignKey' => 'midia_compartilhada_id'
        ]);
        $this->hasMany('MidiasCompartilhadasOperadorasEstados', [
            'foreignKey' => 'midia_compartilhada_operadora_id'
        ]);
        
    }

    public function afterSave($event, $entity, $options) {
         // APAGA REGISTROS RELACIONADOS AO ID
         $this->MidiasCompartilhadasOperadorasEstados->deleteAll(['midia_compartilhada_operadora_id' => $event->data['entity']->id]);

        // ASSOCIA EStados AO MÓDULO
        foreach($event->data['entity']->estados as $v){
            $objMidiasCompartilhadasOperadorasEstados = $this->MidiasCompartilhadasOperadorasEstados->newEntity();
            
            $objMidiasCompartilhadasOperadorasEstados->estado_id = $v;
            $objMidiasCompartilhadasOperadorasEstados->midia_compartilhada_operadora_id = $event->data['entity']->id;
            
            $this->MidiasCompartilhadasOperadorasEstados->save($objMidiasCompartilhadasOperadorasEstados);
        }
    }
}
