<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * MidiasCompartilhadas Model
 *
 * @property \App\Model\Table\EstadosTable&\Cake\ORM\Association\BelongsTo $Estados
 * @property \App\Model\Table\OperadorasTable&\Cake\ORM\Association\BelongsTo $Operadoras
 *
 * @method \App\Model\Entity\MidiasCompartilhada get($primaryKey, $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MidiasCompartilhada findOrCreate($search, callable $callback = null, $options = [])
 */
class MidiasCompartilhadasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('midias_compartilhadas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('MidiasCompartilhadasOperadoras', [
            'foreignKey' => 'midia_compartilhada_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {

        $validator
            ->scalar('nome')
            ->maxLength('nome', 250)
            ->requirePresence('nome', 'create')
            ->notEmptyString('nome');

        $validator
            ->scalar('ramo')
            ->maxLength('ramo', 100)
            ->requirePresence('ramo', 'create')
            ->notEmptyString('ramo');

        $validator
            ->date('validade')
            ->allowEmptyDate('validade');

        $validator
            ->scalar('menu_linha1')
            ->requirePresence('menu_linha1', 'create');

        // $validator
        //     ->scalar('menu_linha2')
        //     ->requirePresence('menu_linha2', 'create');

        return $validator;
    }

    public function getOperadorasEstados($idMidia)
    {
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute("select mco.*, e.id as estado_id,e.nome as estado_nome
        from midias_compartilhadas as mc
                                inner join midias_compartilhadas_operadoras as mco on (mc.id = mco.midia_compartilhada_id)
                                left join midias_compartilhadas_operadoras_estados as mcos on (mco.id = mcos.midia_compartilhada_operadora_id)
                                inner join estados e on (mcos.estado_id = e.id)
                                where mc.id = $idMidia
        group by mco.id;");

        $dados =  $stmt->fetchAll('assoc');
        $res = [];
        if (!empty($dados))
            foreach ($dados as $value) {
                $res[$value['estado_id']] = $value['estado_nome'];
            }
        return $res;
    }
}
