<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Modalidades Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Carencias
 * @property \Cake\ORM\Association\BelongsTo $Informacoes
 * @property \Cake\ORM\Association\BelongsTo $Observacoes
 * @property \Cake\ORM\Association\BelongsTo $Opcionais
 * @property \Cake\ORM\Association\BelongsTo $Redes
 * @property \Cake\ORM\Association\BelongsTo $Reembolsos
 * @property \Cake\ORM\Association\HasMany $Tabelas
 *
 * @method \App\Model\Entity\Modalidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\Modalidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Modalidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Modalidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Modalidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Modalidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Modalidade findOrCreate($search, callable $callback = null)
 */
class ModalidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('modalidades');
        $this->displayField('id');
        $this->primaryKey('id');

//        $this->belongsTo('Carencias', [
//            'foreignKey' => 'carencia_id',
//            'joinType' => 'INNER'
//        ]);
//        $this->belongsTo('Informacoes', [
//            'foreignKey' => 'informacao_id',
//            'joinType' => 'INNER'
//        ]);
//        $this->belongsTo('Observacoes', [
//            'foreignKey' => 'observacao_id',
//            'joinType' => 'INNER'
//        ]);
//        $this->belongsTo('Opcionais', [
//            'foreignKey' => 'opcional_id',
//            'joinType' => 'INNER'
//        ]);
//        $this->belongsTo('Redes', [
//            'foreignKey' => 'rede_id',
//            'joinType' => 'INNER'
//        ]);
//        $this->belongsTo('Reembolsos', [
//            'foreignKey' => 'reembolso_id',
//            'joinType' => 'INNER'
//        ]);
        $this->hasMany('Tabelas', [
            'foreignKey' => 'modalidade_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//        $rules->add($rules->existsIn(['carencia_id'], 'Carencias'));
//        $rules->add($rules->existsIn(['informacao_id'], 'Informacoes'));
//        $rules->add($rules->existsIn(['observacao_id'], 'Observacoes'));
//        $rules->add($rules->existsIn(['opcional_id'], 'Opcionais'));
//        $rules->add($rules->existsIn(['rede_id'], 'Redes'));
//        $rules->add($rules->existsIn(['reembolso_id'], 'Reembolsos'));

        return $rules;
    }
}
