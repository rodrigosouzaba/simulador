<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MunicipiosOdontoComercializacoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Municipios
 * @property \Cake\ORM\Association\BelongsTo $OdontoComercializacoes
 *
 * @method \App\Model\Entity\MunicipiosOdontoComercializaco get($primaryKey, $options = [])
 * @method \App\Model\Entity\MunicipiosOdontoComercializaco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MunicipiosOdontoComercializaco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MunicipiosOdontoComercializaco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MunicipiosOdontoComercializaco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MunicipiosOdontoComercializaco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MunicipiosOdontoComercializaco findOrCreate($search, callable $callback = null)
 */class MunicipiosOdontoComercializacoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('municipios_odonto_comercializacoes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('OdontoComercializacoes', [
            'foreignKey' => 'odonto_comercializacao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['municipio_id'], 'Municipios'));
        $rules->add($rules->existsIn(['odonto_comercializacao_id'], 'OdontoComercializacoes'));

        return $rules;
    }
}
