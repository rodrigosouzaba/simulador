<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MunicipiosPfComercializacoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfComercializacoes
 * @property \Cake\ORM\Association\BelongsTo $Municipios
 *
 * @method \App\Model\Entity\MunicipiosPfComercializaco get($primaryKey, $options = [])
 * @method \App\Model\Entity\MunicipiosPfComercializaco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MunicipiosPfComercializaco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MunicipiosPfComercializaco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MunicipiosPfComercializaco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MunicipiosPfComercializaco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MunicipiosPfComercializaco findOrCreate($search, callable $callback = null)
 */class MunicipiosPfComercializacoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('municipios_pf_comercializacoes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfComercializacoes', [
            'foreignKey' => 'pf_comercializacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_comercializacao_id'], 'PfComercializacoes'));
        $rules->add($rules->existsIn(['municipio_id'], 'Municipios'));

        return $rules;
    }
}
