<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Municipios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsToMany $PfComercializacoes
 *
 * @method \App\Model\Entity\Municipio get($primaryKey, $options = [])
 * @method \App\Model\Entity\Municipio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Municipio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Municipio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Municipio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Municipio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Municipio findOrCreate($search, callable $callback = null)
 */ class MunicipiosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('municipios');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('PfComercializacoes', [
            'foreignKey' => 'municipio_id',
            'targetForeignKey' => 'pf_comercializaco_id',
            'joinTable' => 'municipios_pf_comercializacoes'
        ]);

        $this->belongsToMany('PfTabelas', [
            'foreignKey' => 'municipio_id',
            'targetForeignKey' => 'pf_tabela_id',
            'joinTable' => 'pf_comercializacoes_municipios'
        ]);

        $this->belongsToMany('Tabelas', [
            'foreignKey' => 'municipio_id',
            'targetForeignKey' => 'tabela_id',
            'joinTable' => 'regioes_municipios'
        ]);

        $this->belongsToMany('OdontoTabelas', [
            'foreignKey' => 'municipio_id',
            'targetForeignKey' => 'odonto_tabela_id',
            'joinTable' => 'odonto_comercializacoes_municipios'
        ]);

        $this->belongsToMany('Produtos', [
            'foreignKey' => 'municipio_id',
            'targetForeignKey' => 'produto_id',
            'joinTable' => 'municipios_produtos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')->allowEmpty('id', 'create');
        $validator
            ->integer('codigo')->allowEmpty('codigo');
        $validator
            ->allowEmpty('nome');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));

        return $rules;
    }
}
