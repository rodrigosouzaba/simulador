<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoAreasComercializacoes Model
 *
 * @property \App\Model\Table\OdontoOperadorasTable&\Cake\ORM\Association\BelongsTo $OdontoOperadoras
 *
 * @method \App\Model\Entity\OdontoAreasComercializaco get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoAreasComercializaco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoAreasComercializaco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoAreasComercializaco|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoAreasComercializaco saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoAreasComercializaco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoAreasComercializaco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoAreasComercializaco findOrCreate($search, callable $callback = null, $options = [])
 */
class OdontoAreasComercializacoesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('odonto_areas_comercializacoes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('OdontoTabelas', [
            'foreignKey' => 'odonto_area_comercializacao_id'
        ]);
        $this->belongsTo('OdontoOperadoras', [
            'foreignKey' => 'odonto_operadora_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('Estados', [
            'joinTable' => 'odonto_areas_comercializacoes_estados_municipios',
            'foreignKey' => 'odonto_comercializacao_id',
            'through' => 'OdontoAreasComercializacoesEstadosMunicipios'
        ]);
        $this->belongsToMany('Municipios', [
            'joinTable' => 'odonto_areas_comercializacoes_estados_municipios',
            'foreignKey' => 'odonto_comercializacao_id',
            'through' => 'OdontoAreasComercializacoesEstadosMunicipios'
        ]);
        $this->belongsToMany('Metropoles', [
            'foreignKey' => 'odonto_area_comercializacao_id',
            'targetForeignKey' => 'metropole_id',
            'joinTable' => 'odonto_areas_comercializacoes_metropoles',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('descricao')
            ->allowEmptyString('descricao');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 100)
            ->requirePresence('nome', 'create')
            ->notEmptyString('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['odonto_operadora_id'], 'OdontoOperadoras'));

        return $rules;
    }
}
