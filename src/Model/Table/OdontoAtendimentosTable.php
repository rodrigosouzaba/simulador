<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoAtendimentos Model
 *
 * @method \App\Model\Entity\OdontoAtendimento get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoAtendimento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoAtendimento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoAtendimento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoAtendimento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoAtendimento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoAtendimento findOrCreate($search, callable $callback = null)
 */
class OdontoAtendimentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_atendimentos');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
