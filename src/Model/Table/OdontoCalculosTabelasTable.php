<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoCalculosTabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OdontoCalculos
 * @property \Cake\ORM\Association\BelongsTo $OdontoTabelas
 *
 * @method \App\Model\Entity\OdontoCalculosTabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoCalculosTabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoCalculosTabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoCalculosTabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoCalculosTabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoCalculosTabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoCalculosTabela findOrCreate($search, callable $callback = null)
 */
class OdontoCalculosTabelasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_calculos_tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('OdontoCalculos', [
            'foreignKey' => 'odonto_calculo_id'
        ]);
        $this->belongsTo('OdontoTabelas', [
            'foreignKey' => 'odonto_tabela_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['odonto_calculo_id'], 'OdontoCalculos'));
        $rules->add($rules->existsIn(['odonto_tabela_id'], 'OdontoTabelas'));

        return $rules;
    }
}
