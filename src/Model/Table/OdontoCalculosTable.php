<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoCalculos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\OdontoCalculo get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoCalculo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoCalculo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoCalculo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoCalculo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoCalculo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoCalculo findOrCreate($search, callable $callback = null)
 */
class OdontoCalculosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_calculos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'dependent' => true
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsToMany('OdontoTabelas', [
            'foreignKey' => 'odonto_calculo_id',
            'targetForeignKey' => 'odonto_tabela_id',
            'joinTable' => 'odonto_calculos_tabelas',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('OdontoFiltros', [
            'foreignKey' => 'odonto_calculo_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('contato');

        $validator
            ->integer('quantidade_vidas')
            ->requirePresence('quantidade_vidas', 'create')
            ->notEmpty('quantidade_vidas');

        $validator
            ->dateTime('data')
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        $validator
            ->integer('telefone')
            ->allowEmpty('telefone');

        $validator
            ->allowEmpty('calculoremoto');

        $validator
            ->requirePresence('tipo_pessoa', 'create')
            ->notEmpty('tipo_pessoa');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));

        return $rules;
    }
}
