<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoComercializacoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $OdontoOperadoras
 *
 * @method \App\Model\Entity\OdontoComercializaco get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoComercializaco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoComercializaco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoComercializaco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoComercializaco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoComercializaco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoComercializaco findOrCreate($search, callable $callback = null)
 */
class OdontoComercializacoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_comercializacoes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('OdontoOperadoras', [
            'foreignKey' => 'odonto_operadora_id'
        ]);
        $this->belongsToMany('Municipios', [
            'foreignKey' => 'odonto_comercializacao_id',
            'targetForeignKey' => 'municipio_id',
            'joinTable' => 'municipios_odonto_comercializacoes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('descricao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['odonto_operadora_id'], 'OdontoOperadoras'));

        return $rules;
    }
}
