<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoDependentes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OdontoOperadoras
 * @property \Cake\ORM\Association\HasMany $OdontoProdutos
 *
 * @method \App\Model\Entity\OdontoDependente get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoDependente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoDependente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoDependente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoDependente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoDependente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoDependente findOrCreate($search, callable $callback = null)
 */
class OdontoDependentesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_dependentes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('OdontoOperadoras', [
            'foreignKey' => 'odonto_operadora_id'
        ]);
        $this->hasMany('OdontoProdutos', [
            'foreignKey' => 'odonto_dependente_id'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['odonto_operadora_id'], 'OdontoOperadoras'));

        return $rules;
    }
}
