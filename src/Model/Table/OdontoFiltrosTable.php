<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoFiltros Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OdontoCalculos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsToMany $Tabelas
 *
 * @method \App\Model\Entity\OdontoFiltro get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoFiltro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoFiltro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoFiltro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoFiltro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoFiltro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoFiltro findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OdontoFiltrosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_filtros');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('OdontoCalculos', [
            'foreignKey' => 'odonto_calculo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Tabelas', [
            'foreignKey' => 'odonto_filtro_id',
            'targetForeignKey' => 'tabela_id',
            'joinTable' => 'odonto_filtros_tabelas'
        ]);
        $this->belongsToMany('OdontoTabelas', [
            'foreignKey' => 'odonto_filtro_id',
            'targetForeignKey' => 'odonto_tabela_id',
            'joinTable' => 'odonto_filtros_tabelas',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['odonto_calculo_id'], 'OdontoCalculos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
