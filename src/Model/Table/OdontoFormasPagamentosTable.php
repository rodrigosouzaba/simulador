<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoFormasPagamentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OdontoOperadoras
 *
 * @method \App\Model\Entity\OdontoFormasPagamento get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoFormasPagamento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoFormasPagamento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoFormasPagamento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoFormasPagamento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoFormasPagamento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoFormasPagamento findOrCreate($search, callable $callback = null)
 */
class OdontoFormasPagamentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_formas_pagamentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('OdontoOperadoras', [
            'foreignKey' => 'odonto_operadora_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['odonto_operadora_id'], 'OdontoOperadoras'));

        return $rules;
    }
}
