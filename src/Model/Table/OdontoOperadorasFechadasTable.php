<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoOperadorasFechadas Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Arquivos
 *
 * @method \App\Model\Entity\OdontoOperadorasFechada get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoOperadorasFechada newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoOperadorasFechada[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoOperadorasFechada|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoOperadorasFechada patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoOperadorasFechada[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoOperadorasFechada findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OdontoOperadorasFechadasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_operadoras_fechadas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Arquivos', [
            'foreignKey' => 'odonto_operadora_fechada_id',
            'targetForeignKey' => 'arquivo_id',
            'joinTable' => 'odonto_operadoras_fechadas_arquivos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
