<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * OdontoOperadoras Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Imagens
 * @property \Cake\ORM\Association\HasMany $OdontoProdutos
 *
 * @method \App\Model\Entity\OdontoOperadora get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoOperadora newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoOperadora[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoOperadora|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoOperadora patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoOperadora[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoOperadora findOrCreate($search, callable $callback = null)
 */
class OdontoOperadorasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->table('odonto_operadoras');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Imagens', [
            'foreignKey' => 'imagem_id'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id'
        ]);

        $this->hasMany('OdontoRedes', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('OdontoCarencias', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('OdontoReembolsos', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('OdontoDocumentos', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('OdontoDependentes', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('OdontoFormasPagamentos', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('OdontoComercializacoes', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('OdontoObservacaos', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('OdontoTabelas', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('OdontoProdutos', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('OdontoAreasComercializacoes', [
            'foreignKey' => 'odonto_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('detalhe');

        $validator
            ->allowEmpty('tipo_pessoa');

        $validator
            ->numeric('prioridade')
            ->allowEmpty('prioridade');

        $validator
            ->allowEmpty('url');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['imagem_id'], 'Imagens'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));

        return $rules;
    }

    /**
     * Carrega o menu do sistema de acordo com o parametro informado.
     * @param String opcaoMenu : 3 => Pessoa Juridica (padrao) / 4 => Pessoa Fisica 
     * @return JSON 
     */
    public function getMenu($opcaoMenu)
    {

        $conn = ConnectionManager::get('default');

        if ($opcaoMenu == 3) { # Odonto PJ
            $stmt = $conn->execute('SELECT odonto_operadoras.id AS cod, odonto_operadoras.nome, odonto_operadoras.detalhe , odonto_operadoras.status, (SELECT nome FROM estados WHERE id = odonto_operadoras.estado_id) AS estado
            FROM odonto_operadoras inner join odonto_tabelas on odonto_operadoras.id = odonto_tabelas.odonto_operadora_id and (odonto_tabelas.validade = 1 or odonto_tabelas.validade is null)
            WHERE odonto_operadoras.status IS NULL OR odonto_operadoras.status = "" AND odonto_operadoras.tipo_pessoa = "PJ" 
            GROUP BY odonto_operadoras.id;');
        } else if ($opcaoMenu == 4) { # Odonto PF
            $stmt = $conn->execute('SELECT odonto_operadoras.id AS cod, odonto_operadoras.nome, odonto_operadoras.detalhe , odonto_operadoras.status, (SELECT nome FROM estados WHERE id = odonto_operadoras.estado_id) AS estado
            FROM odonto_operadoras inner join odonto_tabelas on odonto_operadoras.id = odonto_tabelas.odonto_operadora_id and (odonto_tabelas.validade = 1 or odonto_tabelas.validade is null)
            WHERE odonto_operadoras.status IS NULL OR odonto_operadoras.status = "" AND odonto_operadoras.tipo_pessoa = "PF" 
            GROUP BY odonto_operadoras.id;');
        }
        return $stmt->fetchAll('assoc');
    }

    public function getEstados($tipoPessoa = 'all')
    {

        $conn = ConnectionManager::get('default');
        if ($tipoPessoa == 'all') {
            $stmt = $conn->execute('SELECT odonto_tabelas.estado_id, (SELECT nome FROM estados WHERE id = odonto_tabelas.estado_id) AS estado
            FROM odonto_tabelas 
            WHERE odonto_tabelas.validade IS NULL OR odonto_tabelas.validade = 1 
            GROUP BY odonto_tabelas.estado_id;');
        } else {
            $stmt = $conn->execute('SELECT odonto_operadoras.estado_id, (SELECT nome FROM estados WHERE id = odonto_operadoras.estado_id) AS estado
            FROM odonto_operadoras 
            WHERE odonto_operadoras.status IS NULL OR odonto_operadoras.status = "" AND odonto_operadoras.tipo_pessoa = "' . $tipoPessoa . '" 
            GROUP BY odonto_operadoras.estado_id;');
        }

        return $stmt->fetchAll('assoc');
    }
}
