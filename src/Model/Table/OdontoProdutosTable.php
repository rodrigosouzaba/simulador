<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoProdutos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OdontoOperadoras
 * @property \Cake\ORM\Association\BelongsTo $OdontoCarencias
 * @property \Cake\ORM\Association\BelongsTo $OdontoRedes
 * @property \Cake\ORM\Association\BelongsTo $OdontoReembolsos
 * @property \Cake\ORM\Association\BelongsTo $OdontoDependentes
 * @property \Cake\ORM\Association\BelongsTo $OdontoDocumentos
 * @property \Cake\ORM\Association\BelongsTo $OdontoObservacaos
 * @property \Cake\ORM\Association\HasMany $OdontoTabelas
 *
 * @method \App\Model\Entity\OdontoProduto get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoProduto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoProduto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoProduto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoProduto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoProduto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoProduto findOrCreate($search, callable $callback = null)
 */
class OdontoProdutosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_produtos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('OdontoOperadoras', [
            'foreignKey' => 'odonto_operadora_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('OdontoCarencias', [
            'foreignKey' => 'odonto_carencia_id'
        ]);
        $this->belongsTo('OdontoRedes', [
            'foreignKey' => 'odonto_rede_id'
        ]);
        $this->belongsTo('OdontoReembolsos', [
            'foreignKey' => 'odonto_reembolso_id'
        ]);
        $this->belongsTo('OdontoDependentes', [
            'foreignKey' => 'odonto_dependente_id'
        ]);
        $this->belongsTo('OdontoDocumentos', [
            'foreignKey' => 'odonto_documento_id'
        ]);
        $this->belongsTo('OdontoObservacaos', [
            'foreignKey' => 'odonto_observacao_id'
        ]);
        $this->hasMany('OdontoTabelas', [
            'foreignKey' => 'odonto_produto_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

//        $validator
//            ->requirePresence('descricao', 'create')
//            ->notEmpty('descricao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['odonto_operadora_id'], 'OdontoOperadoras'));
        $rules->add($rules->existsIn(['odonto_carencia_id'], 'OdontoCarencias'));
        $rules->add($rules->existsIn(['odonto_rede_id'], 'OdontoRedes'));
        $rules->add($rules->existsIn(['odonto_reembolso_id'], 'OdontoReembolsos'));
        $rules->add($rules->existsIn(['odonto_dependente_id'], 'OdontoDependentes'));
        $rules->add($rules->existsIn(['odonto_documento_id'], 'OdontoDocumentos'));
        $rules->add($rules->existsIn(['odonto_observacao_id'], 'OdontoObservacaos'));

        return $rules;
    }
}
