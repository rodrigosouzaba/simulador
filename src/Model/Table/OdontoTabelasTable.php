<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OdontoTabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OdontoComercializacoes
 * @property \Cake\ORM\Association\BelongsTo $OdontoAtendimentos
 * @property \Cake\ORM\Association\BelongsTo $OdontoProdutos
 * @property \Cake\ORM\Association\BelongsTo $Estados
 *
 * @method \App\Model\Entity\OdontoTabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\OdontoTabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OdontoTabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OdontoTabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OdontoTabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoTabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OdontoTabela findOrCreate($search, callable $callback = null)
 */
class OdontoTabelasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('odonto_tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('OdontoComercializacoes', [
            'foreignKey' => 'odonto_comercializacao_id'
        ]);
        $this->belongsTo('OdontoAtendimentos', [
            'foreignKey' => 'odonto_atendimento_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('OdontoProdutos', [
            'foreignKey' => 'odonto_produto_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id'
        ]);
        $this->hasMany('OdontoOperadoras', [
            'foreignKey' => 'odonto_operadora_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('OdontoCalculos', [
            'foreignKey' => 'odonto_tabela_id',
            'targetForeignKey' => 'odonto_calculo_id',
            'joinTable' => 'odonto_calculos_tabelas',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->belongsTo('OdontoAreasComercializacoes', [
            'foreignKey' => 'odonto_area_comercializacao_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('OdontoRedes');
        $this->belongsTo('OdontoObservacaos');
        $this->belongsTo('OdontoReembolsos')->setProperty('reembolsoEntity');
        $this->belongsTo('OdontoCarencias');
        $this->belongsTo('OdontoDependentes');
        $this->belongsTo('OdontoDocumentos');
        $this->belongsTo('OdontoFormasPagamentos');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->date('vigencia')
            ->requirePresence('vigencia', 'create')
            ->notEmpty('vigencia');

        $validator
            ->numeric('preco_vida')
            ->allowEmpty('preco_vida');

        $validator
            ->allowEmpty('validade');

        $validator
            ->integer('minimo_vidas')
            ->allowEmpty('minimo_vidas');

        $validator
            ->integer('maximo_vidas')
            ->allowEmpty('maximo_vidas');

        $validator
            ->integer('titulares')
            ->allowEmpty('titulares');

        $validator
            ->numeric('prioridade')
            ->allowEmpty('prioridade');

        $validator
            ->allowEmpty('reembolso');

        $validator
            ->requirePresence('tipo_pessoa', 'create')
            ->notEmpty('tipo_pessoa');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['odonto_comercializacao_id'], 'OdontoComercializacoes'));
        $rules->add($rules->existsIn(['odonto_atendimento_id'], 'OdontoAtendimentos'));
        $rules->add($rules->existsIn(['odonto_produto_id'], 'OdontoProdutos'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));

        return $rules;
    }
}
