<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * Operadoras Model
 *
 * @property \Cake\ORM\Association\HasMany $Produtos
 *
 * @method \App\Model\Entity\Operadora get($primaryKey, $options = [])
 * @method \App\Model\Entity\Operadora newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Operadora[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Operadora|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Operadora patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Operadora[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Operadora findOrCreate($search, callable $callback = null)
 */
class OperadorasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->table('operadoras');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Agendamentos', [
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->belongsTo('Imagens', [
            'foreignKey' => 'imagem_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Tabelas', [
            'foreignKey' => 'operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Produtos', [
            'foreignKey' => 'operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('AreasComercializacoes', [
            'foreignKey' => 'operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('FormasPagamentos', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Regioes', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'LEFT',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Observacoes', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('Opcionais', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('Redes', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('Reembolsos', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Carencias', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Informacoes', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['imagem_id'], 'Imagens'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['carencia_id'], 'Carencias'));
        $rules->add($rules->existsIn(['informacao_id'], 'Informacoes'));
        $rules->add($rules->existsIn(['observacao_id'], 'Observacoes'));
        $rules->add($rules->existsIn(['opcional_id'], 'Opcionais'));
        $rules->add($rules->existsIn(['rede_id'], 'Redes'));
        $rules->add($rules->existsIn(['regiao_id'], 'Regioes'));
        $rules->add($rules->existsIn(['reembolso_id'], 'Reembolsos'));

        return $rules;
    }

    /**
     * Carrega o menu do sistema de acordo com o parametro informado.
     * @param String opcaoMenu : 1 => Pessoa Juridica (padrao) / 2 => Pessoa Fisica 
     * @return JSON 
     */
    public function getMenu($opcaoMenu = 1)
    {

        $conn = ConnectionManager::get('default');
        if ($opcaoMenu == 1) {
            $stmt = $conn->execute('SELECT pf_operadoras.id AS cod, pf_operadoras.nome,pf_operadoras.detalhe , (SELECT nome FROM estados WHERE id = pf_operadoras.estado_id) AS estado
                            FROM pf_operadoras LEFT JOIN pf_tabelas ON pf_operadoras.id = pf_tabelas.pf_operadora_id AND (pf_tabelas.validade = 1 or pf_tabelas.validade is null)
                            WHERE pf_operadoras.status IS NULL OR pf_operadoras.status = ""
                            GROUP BY pf_operadoras.id');
        } else {
            $stmt = $conn->execute('SELECT operadoras.id AS cod, operadoras.nome, operadoras.detalhe , (SELECT nome FROM estados WHERE id = operadoras.estado_id) AS estado
                            FROM operadoras LEFT JOIN tabelas ON operadoras.id = tabelas.operadora_id AND (tabelas.validade = 1 or tabelas.validade is null)
                            WHERE operadoras.status IS NULL OR operadoras.status = ""
                            GROUP BY operadoras.id');
        }
        return $stmt->fetchAll('assoc');
    }

    public function getEstados($param = 'all')
    {

        $conn = ConnectionManager::get('default');
        if ($param == 'pf') {
            $stmt = $conn->execute('SELECT pf_tabelas.estado_id, (SELECT nome FROM estados WHERE id = pf_tabelas.estado_id) AS estado 
                                    FROM pf_tabelas
                                    WHERE pf_tabelas.validade = 1 || pf_tabelas.validade is null
                                    group by pf_tabelas.estado_id');
        } elseif ($param == 'pj') {
            $stmt = $conn->execute('SELECT tabelas.estado_id, (SELECT nome FROM estados WHERE id = tabelas.estado_id) AS estado
                                    FROM tabelas 
                                    WHERE tabelas.validade = 1 || tabelas.validade is null
                                    GROUP BY tabelas.estado_id');
        } else {
            $stmt = $conn->execute('SELECT Estados.id AS `estado_id`, Estados.nome AS estado FROM estados Estados 
                                    INNER JOIN pf_areas_comercializacoes_estados_municipios pacem ON Estados.id = (pacem.estado_id) 
                                    INNER JOIN pf_areas_comercializacoes pac ON pac.id = (pacem.pf_comercializacao_id) 
                                    INNER JOIN pf_tabelas pt ON (validade = 1 AND pac.id = (pt.pf_area_comercializacao_id)) 
                                    GROUP BY Estados.id
                                    UNION
                                    SELECT Estados.id AS `estado_id`, Estados.nome AS `nome` FROM estados Estados 
                                    INNER JOIN areas_comercializacoes_estados_municipios AreasComercializacoesEstadosMunicipios ON Estados.id = (AreasComercializacoesEstadosMunicipios.estado_id) 
                                    INNER JOIN areas_comercializacoes AreasComercializacoes ON AreasComercializacoes.id = (AreasComercializacoesEstadosMunicipios.area_comercializacao_id) 
                                    INNER JOIN tabelas Tabelas ON (validade = 1 AND AreasComercializacoes.id = (Tabelas.area_comercializacao_id)) 
                                    GROUP BY Estados.id');
        }
        return $stmt->fetchAll('assoc');
    }
}
