<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pdffiltros Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Simulacoes
 * @property \Cake\ORM\Association\BelongsTo $Abrangencias
 * @property \Cake\ORM\Association\BelongsTo $Tipos
 * @property \Cake\ORM\Association\BelongsTo $TipoProdutos
 *
 * @method \App\Model\Entity\Pdffiltro get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pdffiltro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pdffiltro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pdffiltro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pdffiltro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pdffiltro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pdffiltro findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PdffiltrosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pdffiltros');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Simulacoes', [
            'foreignKey' => 'simulacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Abrangencias', [
            'foreignKey' => 'abrangencia_id'
        ]);
        $this->belongsTo('Tipos', [
            'foreignKey' => 'tipo_id'
        ]);
        $this->belongsTo('TiposProdutos', [
            'foreignKey' => 'tipo_produto_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('coparticipacao');

        $validator
            ->integer('tipo_contratacao')
            ->allowEmpty('tipo_contratacao');

        $validator
            ->allowEmpty('filtroreembolso');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['simulacao_id'], 'Simulacoes'));
        $rules->add($rules->existsIn(['abrangencia_id'], 'Abrangencias'));
        $rules->add($rules->existsIn(['tipo_id'], 'Tipos'));
        $rules->add($rules->existsIn(['tipo_produto_id'], 'TiposProdutos'));

        return $rules;
    }
}
