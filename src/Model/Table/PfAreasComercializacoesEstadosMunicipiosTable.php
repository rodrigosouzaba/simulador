<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfAreasComercializacoesEstadosMunicipios Model
 *
 * @property \App\Model\Table\EstadosTable&\Cake\ORM\Association\BelongsTo $Estados
 * @property \App\Model\Table\PfAreasComercializacaos&\Cake\ORM\Association\BelongsTo $PfComercializacaos
 * @property \App\Model\Table\MunicipiosTable&\Cake\ORM\Association\BelongsTo $Municipios
 *
 * @method \App\Model\Entity\PfAreasComercializacoesEstadosMunicipio get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfAreasComercializacoesEstadosMunicipio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfAreasComercializacoesEstadosMunicipio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfAreasComercializacoesEstadosMunicipio|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfAreasComercializacoesEstadosMunicipio saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfAreasComercializacoesEstadosMunicipio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfAreasComercializacoesEstadosMunicipio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfAreasComercializacoesEstadosMunicipio findOrCreate($search, callable $callback = null, $options = [])
 */
class PfAreasComercializacoesEstadosMunicipiosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pf_areas_comercializacoes_estados_municipios');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
        ]);
        $this->belongsTo('PfAreasComercializacoes', [
            'foreignKey' => 'pf_comercializacao_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['pf_comercializacao_id'], 'PfAreasComercializacoes'));
        $rules->add($rules->existsIn(['municipio_id'], 'Municipios'));

        return $rules;
    }
}
