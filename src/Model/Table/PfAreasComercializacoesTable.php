<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfAreasComercializacoes Model
 *
 * @property \App\Model\Table\PfOperadorasTable&\Cake\ORM\Association\BelongsTo $PfOperadoras
 * @property \App\Model\Table\PfAtendimentosTable&\Cake\ORM\Association\BelongsTo $PfAtendimentos
 *
 * @method \App\Model\Entity\PfAreasComercializaco get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfAreasComercializaco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfAreasComercializaco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfAreasComercializaco|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfAreasComercializaco saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfAreasComercializaco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfAreasComercializaco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfAreasComercializaco findOrCreate($search, callable $callback = null, $options = [])
 */
class PfAreasComercializacoesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pf_areas_comercializacoes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('PfOperadoras', [
            'foreignKey' => 'pf_operadora_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('PfTabelas', [
            'foreignKey' => 'pf_area_comercializacao_id'
        ]);
        $this->belongsToMany('Estados', [
            'joinTable' => 'pf_areas_comercializacoes_estados_municipios',
            'foreignKey' => 'pf_comercializacao_id',
            'through' => 'PfAreasComercializacoesEstadosMunicipios'
        ]);
        $this->belongsToMany('Municipios', [
            'joinTable' => 'pf_areas_comercializacoes_estados_municipios',
            'foreignKey' => 'pf_comercializacao_id',
            'through' => 'PfAreasComercializacoesEstadosMunicipios'
        ]);
        $this->belongsToMany('Metropoles', [
            'foreignKey' => 'pf_area_comercializacao_id',
            'targetForeignKey' => 'metropole_id',
            'joinTable' => 'pf_areas_comercializacoes_metropoles',
        ]);
        $this->hasOne('PfAreasComercializacoesEstadosMunicipios', [
            'joinTable' => 'pf_areas_comercializacoes_estados_municipios',
            'foreignKey' => 'pf_comercializacao_id',
            'joinType' => 'inner'
            // 'through' => 'PfAreasComercializacoesEstadosMunicipios'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao', 100)
            ->allowEmptyString('descricao');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 100)
            ->requirePresence('nome', 'create')
            ->notEmptyString('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_operadora_id'], 'PfOperadoras'));

        return $rules;
    }
}
