<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfAtendimentos Model
 *
 * @property \Cake\ORM\Association\HasMany $PfTabelas
 *
 * @method \App\Model\Entity\PfAtendimento get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfAtendimento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfAtendimento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfAtendimento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfAtendimento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfAtendimento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfAtendimento findOrCreate($search, callable $callback = null)
 */
class PfAtendimentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_atendimentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('PfTabelas', [
            'foreignKey' => 'pf_atendimento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
