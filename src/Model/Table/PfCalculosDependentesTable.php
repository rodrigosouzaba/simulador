<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfCalculosDependentes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfCalculos
 *
 * @method \App\Model\Entity\PfCalculosDependente get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfCalculosDependente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfCalculosDependente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfCalculosDependente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfCalculosDependente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfCalculosDependente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfCalculosDependente findOrCreate($search, callable $callback = null)
 */
class PfCalculosDependentesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_calculos_dependentes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfCalculos', [
            'foreignKey' => 'pf_calculo_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('idade_dependente')
            ->requirePresence('idade_dependente', 'create')
            ->notEmpty('idade_dependente');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_calculo_id'], 'PfCalculos'));

        return $rules;
    }
}
