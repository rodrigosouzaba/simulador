<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfCalculos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfProfissoes
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $PfCalculosDependentes
 * @property \Cake\ORM\Association\BelongsToMany $Tabelas
 *
 * @method \App\Model\Entity\PfCalculo get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfCalculo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfCalculo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfCalculo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfCalculo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfCalculo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfCalculo findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PfCalculosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_calculos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        //CHAVES ESTRANGEIRAS
        $this->belongsTo('PfProfissoes', [
            'foreignKey' => 'pf_profissao_id'
        ]);

        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
            // 	        'dependent' => true,
            //             'joinType' => 'LEFT'
        ]);

        //TABELAS EXTERNAS QUE UTILIZAM PFCALCULO
        $this->hasMany('PfCalculosTabelas', [
            'foreignKey' => 'pf_calculo_id',
            // 	        'dependent' => true
        ]);

        $this->hasMany('PfFiltros', [
            'foreignKey' => 'pf_calculo_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PfCalculosDependentes', [
            'foreignKey' => 'pf_calculo_id',
            'dependent' => true,
        ]);

        $this->belongsToMany('PfTabelas', [
            'foreignKey' => 'pf_calculo_id',
            'targetForeignKey' => 'pf_tabela_id',
            'joinTable' => 'pf_calculos_tabelas',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfPdffiltros', [
            'foreignKey' => 'pf_calculo_id',
            'dependent' => true,
        ]);

        $this->hasMany('Alertas', [
            'foreignKey' => 'pf_calculo_id',
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('calculoremoto');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {

        $rules->add($rules->existsIn(['pf_profissao_id'], 'PfProfissoes'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));

        return $rules;
    }
}
