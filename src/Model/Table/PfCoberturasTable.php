<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfCoberturas Model
 *
 * @method \App\Model\Entity\PfCobertura get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfCobertura newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfCobertura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfCobertura|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfCobertura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfCobertura[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfCobertura findOrCreate($search, callable $callback = null)
 */class PfCoberturasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_coberturas');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        $validator
            ->requirePresence('nome', 'create')            ->notEmpty('nome');
        $validator
            ->integer('prioridade')            ->allowEmpty('prioridade');
        return $validator;
    }
}
