<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfComercializacoesMunicipios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfTabelas
 * @property \Cake\ORM\Association\BelongsTo $Municipios
 *
 * @method \App\Model\Entity\PfComercializacoesMunicipio get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfComercializacoesMunicipio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfComercializacoesMunicipio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfComercializacoesMunicipio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfComercializacoesMunicipio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfComercializacoesMunicipio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfComercializacoesMunicipio findOrCreate($search, callable $callback = null)
 */
class PfComercializacoesMunicipiosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_comercializacoes_municipios');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfTabelas', [
            'foreignKey' => 'pf_tabela_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_tabela_id'], 'PfTabelas'));
        $rules->add($rules->existsIn(['municipio_id'], 'Municipios'));

        return $rules;
    }
}
