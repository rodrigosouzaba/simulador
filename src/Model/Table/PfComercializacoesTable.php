<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfComercializacoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $PfOperadoras
 *
 * @method \App\Model\Entity\PfComercializaco get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfComercializaco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfComercializaco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfComercializaco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfComercializaco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfComercializaco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfComercializaco findOrCreate($search, callable $callback = null)
 */
class PfComercializacoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_comercializacoes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PfOperadoras', [
            'foreignKey' => 'pf_operadora_id'
        ]);
        $this->belongsToMany('Municipios', [
            'foreignKey' => 'pf_comercializacao_id',
            'targetForeignKey' => 'municipio_id',
            'joinTable' => 'municipios_pf_comercializacoes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('descricao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['pf_operadora_id'], 'PfOperadoras'));

        return $rules;
    }
}
