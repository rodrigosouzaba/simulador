<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfEntidadesGrupos Model
 *
 * @property \App\Model\Table\PfOperadorasTable&\Cake\ORM\Association\BelongsTo $PfOperadoras
 * @property &\Cake\ORM\Association\HasMany $PfEntidadesGruposEntidades
 *
 * @method \App\Model\Entity\PfEntidadesGrupo get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfEntidadesGrupo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfEntidadesGrupo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidadesGrupo|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfEntidadesGrupo saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfEntidadesGrupo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidadesGrupo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidadesGrupo findOrCreate($search, callable $callback = null, $options = [])
 */
class PfEntidadesGruposTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pf_entidades_grupos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('PfOperadoras', [
            'foreignKey' => 'pf_operadora_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('PfEntidades', [
            'foreignKey' => 'pf_entidades_grupo_id',
            'joinTable' => 'pf_entidades_grupos_entidades'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 100)
            ->requirePresence('nome', 'create')
            ->notEmptyString('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_operadora_id'], 'PfOperadoras'));

        return $rules;
    }
}
