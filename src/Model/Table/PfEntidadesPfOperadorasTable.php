<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfEntidadesPfOperadoras Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfEntidades
 * @property \Cake\ORM\Association\BelongsTo $PfOperadoras
 *
 * @method \App\Model\Entity\PfEntidadesPfOperadora get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfEntidadesPfOperadora newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfEntidadesPfOperadora[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidadesPfOperadora|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfEntidadesPfOperadora patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidadesPfOperadora[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidadesPfOperadora findOrCreate($search, callable $callback = null)
 */
class PfEntidadesPfOperadorasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_entidades_pf_operadoras');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfEntidades', [
            'foreignKey' => 'pf_entidade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PfOperadoras', [
            'foreignKey' => 'pf_operadora_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_entidade_id'], 'PfEntidades'));
        $rules->add($rules->existsIn(['pf_operadora_id'], 'PfOperadoras'));

        return $rules;
    }
}
