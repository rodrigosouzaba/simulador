<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfEntidadesTabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfEntidades
 * @property \Cake\ORM\Association\BelongsTo $PfTabelas
 *
 * @method \App\Model\Entity\PfEntidadesTabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfEntidadesTabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfEntidadesTabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidadesTabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfEntidadesTabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidadesTabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidadesTabela findOrCreate($search, callable $callback = null)
 */
class PfEntidadesTabelasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_entidades_tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfEntidades', [
            'foreignKey' => 'pf_entidade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PfTabelas', [
            'foreignKey' => 'pf_tabela_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_entidade_id'], 'PfEntidades'));
        $rules->add($rules->existsIn(['pf_tabela_id'], 'PfTabelas'));

        return $rules;
    }
}
