<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfEntidades Model
 *
 * @property \Cake\ORM\Association\HasMany $PfEntidadesProfissoes
 * @property \Cake\ORM\Association\BelongsToMany $Tabelas
 *
 * @method \App\Model\Entity\PfEntidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfEntidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfEntidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfEntidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfEntidade findOrCreate($search, callable $callback = null)
 */
class PfEntidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_entidades');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('PfEntidadesProfissoes', [
            'foreignKey' => 'pf_entidade_id'
        ]);
        $this->belongsToMany('Tabelas', [
            'foreignKey' => 'pf_entidade_id',
            'targetForeignKey' => 'tabela_id',
            'joinTable' => 'pf_entidades_tabelas'
        ]);

        $this->belongsToMany('PfProfissoes', [
            'foreignKey' => 'pf_entidade_id',
            'targetForeignKey' => 'pf_profissao_id',
            'joinTable' => 'pf_entidades_profissoes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
