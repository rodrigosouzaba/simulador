<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfFiltrosTabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfFiltros
 * @property \Cake\ORM\Association\BelongsTo $PfTabelas
 *
 * @method \App\Model\Entity\PfFiltrosTabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfFiltrosTabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfFiltrosTabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfFiltrosTabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfFiltrosTabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfFiltrosTabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfFiltrosTabela findOrCreate($search, callable $callback = null)
 */
class PfFiltrosTabelasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_filtros_tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfFiltros', [
            'foreignKey' => 'pf_filtro_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PfTabelas', [
            'foreignKey' => 'pf_tabela_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_filtro_id'], 'PfFiltros'));
        $rules->add($rules->existsIn(['pf_tabela_id'], 'PfTabelas'));

        return $rules;
    }
}
