<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfFiltros Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfCalculos
 * @property \Cake\ORM\Association\BelongsTo $PfAtendimentos
 * @property \Cake\ORM\Association\BelongsTo $PfAcomodacoes
 * @property \Cake\ORM\Association\BelongsTo $TiposProdutos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsToMany $Tabelas
 *
 * @method \App\Model\Entity\PfFiltro get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfFiltro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfFiltro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfFiltro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfFiltro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfFiltro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfFiltro findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PfFiltrosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_filtros');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PfCalculos', [
            'foreignKey' => 'pf_calculo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PfAtendimentos', [
            'foreignKey' => 'pf_atendimento_id'
        ]);
        $this->belongsTo('PfAcomodacoes', [
            'foreignKey' => 'pf_acomodacao_id'
        ]);
        $this->belongsTo('TiposProdutos', [
            'foreignKey' => 'tipo_produto_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('PfTabelas', [
            'foreignKey' => 'pf_filtro_id',
            'targetForeignKey' => 'pf_tabela_id',
            'joinTable' => 'pf_filtros_tabelas',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PfFiltrosTabelas', [
            'foreignKey' => 'pf_filtro_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('coparticipacao');

        $validator
            ->integer('carencia')
            ->allowEmpty('carencia');

        $validator
            ->integer('reembolso')
            ->allowEmpty('reembolso');

        $validator
            ->integer('rede')
            ->allowEmpty('rede');

        $validator
            ->integer('documento')
            ->allowEmpty('documento');

        $validator
            ->integer('dependente')
            ->allowEmpty('dependente');

        $validator
            ->integer('observacao')
            ->allowEmpty('observacao');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_calculo_id'], 'PfCalculos'));
        $rules->add($rules->existsIn(['pf_atendimento_id'], 'PfAtendimentos'));
        $rules->add($rules->existsIn(['pf_acomodacao_id'], 'PfAcomodacoes'));
        $rules->add($rules->existsIn(['tipo_produto_id'], 'TiposProdutos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
