<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfOperadorasFechadasArquivos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfOperadorasFechadas
 * @property \Cake\ORM\Association\BelongsTo $Arquivos
 *
 * @method \App\Model\Entity\PfOperadorasFechadasArquivo get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfOperadorasFechadasArquivo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfOperadorasFechadasArquivo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfOperadorasFechadasArquivo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfOperadorasFechadasArquivo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfOperadorasFechadasArquivo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfOperadorasFechadasArquivo findOrCreate($search, callable $callback = null)
 */
class PfOperadorasFechadasArquivosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_operadoras_fechadas_arquivos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfOperadorasFechadas', [
            'foreignKey' => 'pf_operadora_fechada_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Arquivos', [
            'foreignKey' => 'arquivo_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_operadora_fechada_id'], 'PfOperadorasFechadas'));
        $rules->add($rules->existsIn(['arquivo_id'], 'Arquivos'));

        return $rules;
    }
}
