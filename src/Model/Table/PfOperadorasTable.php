<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfOperadoras Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Imagens
 * @property \Cake\ORM\Association\HasMany $PfCarencias
 * @property \Cake\ORM\Association\HasMany $PfComercializacoes
 * @property \Cake\ORM\Association\HasMany $PfDependentes
 * @property \Cake\ORM\Association\HasMany $PfDocumentos
 * @property \Cake\ORM\Association\HasMany $PfObservacoes
 * @property \Cake\ORM\Association\HasMany $PfProdutos
 * @property \Cake\ORM\Association\HasMany $PfRedes
 * @property \Cake\ORM\Association\HasMany $PfReembolsos
 * @property \Cake\ORM\Association\HasMany $PfTabelas
 *
 * @method \App\Model\Entity\PfOperadora get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfOperadora newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfOperadora[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfOperadora|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfOperadora patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfOperadora[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfOperadora findOrCreate($search, callable $callback = null)
 */
class PfOperadorasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->table('pf_operadoras');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Imagens', [
            'foreignKey' => 'imagem_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('PfCarencias', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfFormasPagamentos', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfComercializacoes', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfAreasComercializacoes', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfDependentes', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfDocumentos', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfObservacoes', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfProdutos', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfRedes', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfReembolsos', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('PfTabelas', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PfEntidadesGrupos', [
            'foreignKey' => 'pf_operadora_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->belongsToMany('PfEntidades', [
            'joinTable' => 'pf_entidades_pf_operadoras',
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('detalhe');

        $validator
            ->numeric('prioridade')
            ->allowEmpty('prioridade');

        $validator
            ->allowEmpty('url');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['imagem_id'], 'Imagens'));

        return $rules;
    }
}
