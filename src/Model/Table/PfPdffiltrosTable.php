<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfPdffiltros Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfCalculos
 * @property \Cake\ORM\Association\BelongsTo $PfAtendimentos
 * @property \Cake\ORM\Association\BelongsTo $PfAcomodacaos
 * @property \Cake\ORM\Association\BelongsTo $TipoProdutos
 *
 * @method \App\Model\Entity\PfPdffiltro get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfPdffiltro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfPdffiltro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfPdffiltro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfPdffiltro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfPdffiltro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfPdffiltro findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PfPdffiltrosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_pdffiltros');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PfCalculos', [
            'foreignKey' => 'pf_calculo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PfAtendimentos', [
            'foreignKey' => 'pf_atendimento_id'
        ]);
        $this->belongsTo('PfAcomodacoes', [
            'foreignKey' => 'pf_acomodacao_id'
        ]);
        $this->belongsTo('TiposProdutos', [
            'foreignKey' => 'tipo_produto_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('coparticipacao');

        $validator
            ->allowEmpty('filtroreembolso');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_calculo_id'], 'PfCalculos'));
        $rules->add($rules->existsIn(['pf_atendimento_id'], 'PfAtendimentos'));
        $rules->add($rules->existsIn(['pf_acomodacao_id'], 'PfAcomodacoes'));
        $rules->add($rules->existsIn(['tipo_produto_id'], 'TiposProdutos'));

        return $rules;
    }
}
