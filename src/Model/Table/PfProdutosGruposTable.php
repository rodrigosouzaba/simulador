<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfProdutosGrupos Model
 *
 * @property \App\Model\Table\PfOperadorasTable&\Cake\ORM\Association\BelongsTo $PfOperadoras
 *
 * @method \App\Model\Entity\PfProdutosGrupo get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfProdutosGrupo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfProdutosGrupo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfProdutosGrupo|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfProdutosGrupo saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfProdutosGrupo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfProdutosGrupo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfProdutosGrupo findOrCreate($search, callable $callback = null, $options = [])
 */
class PfProdutosGruposTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pf_produtos_grupos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('PfOperadoras', [
            'foreignKey' => 'pf_operadora_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('PfTabelas', [
            'foreignKey' => 'pf_produtos_grupo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 100)
            ->requirePresence('nome', 'create')
            ->notEmptyString('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_operadora_id'], 'PfOperadoras'));

        return $rules;
    }
}
