<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfProdutos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfOperadoras
 * @property \Cake\ORM\Association\BelongsTo $PfCarencias
 * @property \Cake\ORM\Association\BelongsTo $PfRedes
 * @property \Cake\ORM\Association\BelongsTo $PfReembolsos
 * @property \Cake\ORM\Association\BelongsTo $PfDocumentos
 * @property \Cake\ORM\Association\BelongsTo $PfDependentes
 * @property \Cake\ORM\Association\BelongsTo $PfObservacoes
 * @property \Cake\ORM\Association\BelongsTo $TiposProdutos
 * @property \Cake\ORM\Association\HasMany $PfTabelas
 *
 * @method \App\Model\Entity\PfProduto get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfProduto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfProduto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfProduto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfProduto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfProduto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfProduto findOrCreate($search, callable $callback = null)
 */
class PfProdutosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_produtos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfOperadoras', [
            'foreignKey' => 'pf_operadora_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('TiposProdutos', [
            'foreignKey' => 'tipo_produto_id'
        ]);
        $this->belongsTo('PfCoberturas', [
            'foreignKey' => 'pf_cobertura_id'
        ]);
        $this->hasMany('PfTabelas', [
            'foreignKey' => 'pf_produto_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('descricao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_operadora_id'], 'PfOperadoras'));
        $rules->add($rules->existsIn(['tipo_produto_id'], 'TiposProdutos'));

        return $rules;
    }
}
