<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfProfissoesAlias Model
 *
 * @property \App\Model\Table\PfProfissoesTable&\Cake\ORM\Association\BelongsTo $PfProfissoes
 *
 * @method \App\Model\Entity\PfProfissoesAlias get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfProfissoesAlias newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfProfissoesAlias[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfProfissoesAlias|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfProfissoesAlias saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfProfissoesAlias patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfProfissoesAlias[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfProfissoesAlias findOrCreate($search, callable $callback = null, $options = [])
 */
class PfProfissoesAliasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pf_profissoes_alias');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('PfProfissoes', [
            'foreignKey' => 'pf_profissao_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 100)
            ->requirePresence('nome', 'create')
            ->notEmptyString('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_profissao_id'], 'PfProfissoes'));

        return $rules;
    }
}
