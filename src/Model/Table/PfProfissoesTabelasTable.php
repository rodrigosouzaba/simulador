<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfProfissoesTabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfProfissoes
 * @property \Cake\ORM\Association\BelongsTo $PfTabelas
 *
 * @method \App\Model\Entity\PfProfissoesTabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfProfissoesTabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfProfissoesTabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfProfissoesTabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfProfissoesTabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfProfissoesTabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfProfissoesTabela findOrCreate($search, callable $callback = null)
 */
class PfProfissoesTabelasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_profissoes_tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfProfissoes', [
            'foreignKey' => 'pf_profissao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PfTabelas', [
            'foreignKey' => 'pf_tabela_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_profissao_id'], 'PfProfissoes'));
        $rules->add($rules->existsIn(['pf_tabela_id'], 'PfTabelas'));

        return $rules;
    }
}
