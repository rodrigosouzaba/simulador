<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfProfissoes Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Tabelas
 *
 * @method \App\Model\Entity\PfProfisso get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfProfisso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfProfisso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfProfisso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfProfisso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfProfisso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfProfisso findOrCreate($search, callable $callback = null)
 */
class PfProfissoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_profissoes');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsToMany('PfEntidades', [
            'foreignKey' => 'pf_profissao_id',
            'targetForeignKey' => 'pf_entidade_id',
            'joinTable' => 'pf_entidades_profissoes'
        ]);
        $this->hasMany('pf_profissoes_alias', [
            'foreignKey' => 'pf_profissao_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
