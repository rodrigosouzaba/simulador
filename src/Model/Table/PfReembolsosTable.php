<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfReembolsos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfOperadoras
 * @property \Cake\ORM\Association\HasMany $PfProdutos
 *
 * @method \App\Model\Entity\PfReembolso get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfReembolso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfReembolso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfReembolso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfReembolso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfReembolso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfReembolso findOrCreate($search, callable $callback = null)
 */
class PfReembolsosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_reembolsos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PfOperadoras', [
            'foreignKey' => 'pf_operadora_id'
        ]);
//        $this->hasMany('PfProdutos', [
//            'foreignKey' => 'pf_reembolso_id'
//        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_operadora_id'], 'PfOperadoras'));

        return $rules;
    }
}
