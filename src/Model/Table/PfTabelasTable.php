<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PfTabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PfProdutos
 * @property \Cake\ORM\Association\BelongsTo $PfOperadoras
 * @property \Cake\ORM\Association\BelongsTo $PfAtendimentos
 * @property \Cake\ORM\Association\BelongsTo $PfAcomodacoes
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $Regioes
 * @property \Cake\ORM\Association\HasMany $PfProfissoesTabelas
 *
 * @method \App\Model\Entity\PfTabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\PfTabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PfTabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PfTabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PfTabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PfTabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PfTabela findOrCreate($search, callable $callback = null)
 */
class PfTabelasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pf_tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PfProdutos', [
            'foreignKey' => 'pf_produto_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PfOperadoras', [
            'foreignKey' => 'pf_operadora_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('PfAtendimentos', [
            'foreignKey' => 'pf_atendimento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PfAcomodacoes', [
            'foreignKey' => 'pf_acomodacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id'
        ]);
        $this->belongsTo('PfComercializacoes', [
            'foreignKey' => 'pf_comercializacao_id'
        ]);

        $this->belongsToMany('PfEntidades', [
            'foreignKey' => 'pf_tabela_id',
            'targetForeignKey' => 'pf_entidade_id',
            'joinTable' => 'pf_entidades_tabelas'
        ]);

        $this->hasMany('PfEntidadesTabelas', [
            'foreignKey' => 'pf_tabela_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PfCalculosTabelas', [
            'foreignKey' => 'pf_tabela_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PfFiltrosTabelas', [
            'foreignKey' => 'pf_tabela_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->belongsTo('TiposProdutos', [
            'foreignKey' => 'tipo_produto_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('PfCoberturas', [
            'foreignKey' => 'pf_cobertura_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('PfAreasComercializacoes', [
            'foreignKey' => 'pf_area_comercializacao_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('PfProdutosGrupos', [
            'foreignKey' => 'pf_produtos_grupo_id',
            'joinType' => 'LEFT'
        ]);

        // NOVAS ASSOCIAÇÕES
        $this->belongsTo('PfFormasPagamentos');
        $this->belongsTo('PfDocumentos');
        $this->belongsTo('PfDependentes');
        $this->belongsTo('PfCarencias');
        $this->belongsTo('PfReembolsos')
            ->setProperty('reembolsoEntity');
        $this->belongsTo('PfObservacoes', [
            'foreignKey' => 'pf_observacao_id',
        ]);
        $this->belongsTo('PfRedes');
        $this->belongsTo('PfEntidadesGrupos');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        /*
        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');
*/


        $validator
            ->date('vigencia')
            ->requirePresence('vigencia', 'create')
            ->notEmpty('vigencia');

        $validator
            ->allowEmpty('validade');

        $validator
            ->allowEmpty('cod_ans');

        $validator
            ->integer('minimo_vidas')
            ->allowEmpty('minimo_vidas');

        $validator
            ->integer('maximo_vidas')
            ->allowEmpty('maximo_vidas');

        $validator
            ->numeric('prioridade')
            ->allowEmpty('prioridade');

        $validator
            ->allowEmpty('coparticipacao');

        $validator
            ->allowEmpty('detalhe_coparticipacao');

        $validator
            ->requirePresence('modalidade', 'create')
            ->notEmpty('modalidade');

        $validator
            ->allowEmpty('reembolso');

        $validator
            ->allowEmptyString('pf_operadora_id');
        // debug($validator);
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pf_operadora_id'], 'PfOperadoras'));
        $rules->add($rules->existsIn(['pf_atendimento_id'], 'PfAtendimentos'));
        $rules->add($rules->existsIn(['pf_acomodacao_id'], 'PfAcomodacoes'));
        $rules->add($rules->existsIn(['pf_operadora_id'], 'PfOperadoras'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['pf_comercializacao_id'], 'PfComercializacoes'));

        return $rules;
    }
}
