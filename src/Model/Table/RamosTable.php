<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ramos Model
 *
 * @property \Cake\ORM\Association\HasMany $Tabelas
 *
 * @method \App\Model\Entity\Ramo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ramo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ramo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ramo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ramo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ramo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ramo findOrCreate($search, callable $callback = null)
 */
class RamosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ramos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Tabelas', [
            'foreignKey' => 'ramo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
