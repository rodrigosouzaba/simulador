<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Regioes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsToMany $Tabelas
 *
 * @method \App\Model\Entity\Regio get($primaryKey, $options = [])
 * @method \App\Model\Entity\Regio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Regio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Regio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Regio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Regio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Regio findOrCreate($search, callable $callback = null)
 */
class RegioesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('regioes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Operadoras', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsToMany('Municipios', [
            'foreignKey' => 'regiao_id',
            'targetForeignKey' => 'municipio_id',
            'joinTable' => 'municipios_regioes'
        ]);

        //        $this->hasMany('Operadoras', [
        //            'foreignKey' => 'regiao_id'
        //        ]);

        //        $this->belongsToMany('Tabelas', [
        //            'foreignKey' => 'regio_id',
        //            'targetForeignKey' => 'tabela_id',
        //            'joinTable' => 'tabelas_regioes'
        //        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('nome', 'create');
        $validator
            ->notEmpty('estado_id', 'create');
        $validator
            ->notEmpty('operadora_id', 'create');
        $validator
            ->notEmpty('descricao', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));

        return $rules;
    }
}
