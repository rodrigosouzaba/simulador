<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SimulacoesComentarios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Simulacoes
 *
 * @method \App\Model\Entity\SimulacoesComentario get($primaryKey, $options = [])
 * @method \App\Model\Entity\SimulacoesComentario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SimulacoesComentario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SimulacoesComentario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SimulacoesComentario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SimulacoesComentario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SimulacoesComentario findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SimulacoesComentariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('simulacoes_comentarios');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Simulacoes', [
            'foreignKey' => 'simulacao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('mensagem', 'create')
            ->notEmpty('mensagem');

        $validator
            ->allowEmpty('alerta');

        $validator
            ->date('data_alerta')
            ->allowEmpty('data_alerta');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['simulacao_id'], 'Simulacoes'));

        return $rules;
    }
}
