<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SimulacoesTabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Simulacoes
 * @property \Cake\ORM\Association\BelongsTo $Tabelas
 *
 * @method \App\Model\Entity\SimulacoesTabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\SimulacoesTabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SimulacoesTabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SimulacoesTabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SimulacoesTabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SimulacoesTabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SimulacoesTabela findOrCreate($search, callable $callback = null)
 */
class SimulacoesTabelasTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('simulacoes_tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Simulacoes', [
            'foreignKey' => 'simulacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tabelas', [
            'foreignKey' => 'tabela_id',
            'joinType' => 'INNER'
        ]);

        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['simulacao_id'], 'Simulacoes'));
        $rules->add($rules->existsIn(['tabela_id'], 'Tabelas'));

        return $rules;
    }

}
