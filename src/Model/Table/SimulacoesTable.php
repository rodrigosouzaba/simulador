<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;

/**
 * Simulacoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TabelasRegioes
 *
 * @method \App\Model\Entity\Simulaco get($primaryKey, $options = [])
 * @method \App\Model\Entity\Simulaco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Simulaco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Simulaco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Simulaco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Simulaco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Simulaco findOrCreate($search, callable $callback = null)
 */
class SimulacoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('simulacoes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('TabelasRegioes', [
            'foreignKey' => 'tabela_regiao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Pdffiltros', [
            'foreignKey' => 'simulacao_id'
        ]);

        $this->hasMany('Filtros', [
            'foreignKey' => 'simulacao_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->belongsToMany('Tabelas', [
            'foreignKey' => 'simulacao_id',
            'targetForeignKey' => 'tabela_id',
            'joinTable' => 'simulacoes_tabelas',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasOne('SimulacoesComentarios', [
            'foreignKey' => 'simulacao_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Pdffiltros', [
            'foreignKey' => 'simulacao_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Alertas', [
            'foreignKey' => 'simulacao_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');



        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tabela_regiao_id'], 'TabelasRegioes'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        return $rules;
    }
}
