<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\ORM\TableRegistry;
use App\Model\Entity\SisFuncionalidadesSisPermissao;

/**
 * Alertas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Simulacoes
 * @property \Cake\ORM\Association\BelongsTo $PfCalculos
 * @property \Cake\ORM\Association\BelongsTo $OdontoCalculos
 *
 * @method \App\Model\Entity\Alerta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Alerta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Alerta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Alerta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Alerta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SisFuncionalidadesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sis_funcionalidades');

        $this->belongsTo('SisModulo');

        $this->hasMany('SisFuncionalidadesSisPermissoes');
        $this->hasMany('SisFuncionalidadesSisGrupos');

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }
    
    public function afterSave($event, $entity, $options) {
        

        // VERIFICIA SE FORAM PASSADAS PEMISSÕES COMO PARÂMETROS
        if (!empty($event->data['entity']->permission)){
            // PREPARA DADOS
            $dados = $this->_extractFieldsHABTM($event->data['entity']->permission, $event->data['entity']->id, 'sis_funcionalidade_id', 'sis_permissao_id');
            // APAGA REGISTROS RELACIONADOS AO ID
            $this->SisFuncionalidadesSisPermissoes->deleteAll(['sis_funcionalidade_id'=> $event->data['entity']->id]);
            
            // ASSOCIA PERMISSÕES A FUNCIONALIDADE
            foreach ($dados as $value) {
                $obj = $this->SisFuncionalidadesSisPermissoes->newEntity();
               
                $obj->sis_funcionalidade_id = $value['sis_funcionalidade_id'];
                $obj->sis_permissao_id = $value['sis_permissao_id'];
                $this->SisFuncionalidadesSisPermissoes->save($obj);
            }

        }

        // VERIFICIA SE FORAM PASSADOS GRUPOS COMO PARÂMETROS
        if (isset($event->data['entity']->sis_grupo)){
            // PREPARA DADOS
            $dados = $this->_extractFieldsHABTM($event->data['entity']->sis_grupo, $event->data['entity']->id, 'sis_funcionalidade_id', 'sis_grupo_id');

            // APAGA REGISTROS RELACIONADOS AO ID
            $this->SisFuncionalidadesSisGrupos->deleteAll(['sis_funcionalidade_id'=>$event->data['entity']->id]);

            // ASSOCIA PERMISSÕES A FUNCIONALIDADE
            foreach ($dados as $value) {
                $objSisGrupos = $this->SisFuncionalidadesSisGrupos->newEntity();
                $objSisGrupos->sis_funcionalidade_id = $value['sis_funcionalidade_id'];
                $objSisGrupos->sis_grupo_id = $value['sis_grupo_id'];
                $this->SisFuncionalidadesSisGrupos->save($objSisGrupos);
            }
        }
    }

     /**
     * 1 - data pra quem eu quero 
     * 2 - Id da tabela origem
     * 3 - nome da chave da tabela origem,
     * 4 - nome da chave da tabela relacional (lookup)
     */
    public function _extractFieldsHABTM($data, $id, $chave, $lookup) {
        $dados = [];

        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $dados[$k][$chave] = $id;
                $dados[$k][$lookup] = $v;
            }
        }

        return $dados;
    }

  
}
