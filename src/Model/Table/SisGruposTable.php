<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;

/**
 * Alertas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Simulacoes
 * @property \Cake\ORM\Association\BelongsTo $PfCalculos
 * @property \Cake\ORM\Association\BelongsTo $OdontoCalculos
 *
 * @method \App\Model\Entity\Alerta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Alerta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Alerta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Alerta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Alerta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SisGruposTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sis_grupos');

        $this->setPrimaryKey('id');

        $this->hasMany('SisFuncionalidadesSisGrupos');
        $this->hasMany('UsersSisGrupos');
        $this->hasMany('SmsEnvios', [
            'foreignKey' => 'sms_lista_id'
        ]);
        $this->belongsToMany('Users', ['joinTable' => 'users_sis_grupos']);
        $this->addBehavior('Timestamp');
    }

    public function afterSave($event, $entity, $options)
    {

        try {
            $ok = true;
            // PREPARA DADOS
            $dados = $this->_extractFieldsHABTM($event->data['entity']->sis_funcionalidade, $event->data['entity']->id, 'sis_grupo_id', 'sis_funcionalidade_id');

            // APAGA REGISTROS RELACIONADOS AO ID
            $this->SisFuncionalidadesSisGrupos->deleteAll(['sis_grupo_id' => $event->data['entity']->id]);

            // ASSOCIA PERMISSÕES A FUNCIONALIDADE
            foreach ($dados as $value) {
                $obj = $this->SisFuncionalidadesSisGrupos->newEntity();

                $obj->sis_funcionalidade_id = $value['sis_funcionalidade_id'];
                $obj->sis_grupo_id = $value['sis_grupo_id'];
                if (!$this->SisFuncionalidadesSisGrupos->save($obj)) {
                    $ok = false;
                }
            }

            // PREPARA DADOS
            $dados = $this->_extractFieldsHABTM($event->data['entity']->user_id, $event->data['entity']->id, 'sis_grupo_id', 'user_id');

            // APAGA REGISTROS RELACIONADOS AO ID
            $this->UsersSisGrupos->deleteAll(['sis_grupo_id' => $event->data['entity']->id]);

            // ASSOCIA PERMISSÕES A FUNCIONALIDADE
            foreach ($dados as $value) {
                $obj = $this->UsersSisGrupos->newEntity();

                $obj->user_id = $value['user_id'];
                $obj->sis_grupo_id = $value['sis_grupo_id'];
                if (!$this->UsersSisGrupos->save($obj)) {
                    $ok = false;
                }
            }

            if ($ok) {
                $dadosLogin = $this->loginWP();
                if ($dadosLogin['success']) {
                    if ($entity->isNew()) {
                        $this->createTagsWP($event->data['entity']->name, $dadosLogin['data']['token']);
                    } else {
                        $this->updateTag($entity->getOriginal('name'), $event->data['entity']->name, $dadosLogin['data']['token']);
                    }
                }
            }
        } catch (\Exception $e) {
            return $e;
        }
    }


    public function getFuncionalidadesPermissoes($userId = null)
    {

        $conn = ConnectionManager::get('default');
        $data = $conn->execute("SELECT p.name 
                                FROM users_sis_grupos ug 
                                        LEFT JOIN sis_funcionalidades_sis_grupos sfsg ON ug.sis_grupo_id = sfsg.sis_grupo_id
                                        LEFT JOIN sis_funcionalidades_sis_permissoes fp ON sfsg.sis_funcionalidade_id = fp.sis_funcionalidade_id
                                        INNER JOIN sis_permissoes p ON fp.sis_permissao_id = p.id
                                WHERE  user_id = $userId ");

        $data = Hash::extract($data->fetchAll('assoc'), '{n}.name');
        return $data;
    }

    /**
     * 1 - data pra quem eu quero 
     * 2 - Id da tabela origem
     * 3 - nome da chave da tabela origem,
     * 4 - nome da chave da tabela relacional (lookup)
     */
    public function _extractFieldsHABTM($data, $id, $chave, $lookup)
    {
        $dados = [];

        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $dados[$k][$chave] = $id;
                $dados[$k][$lookup] = $v;
            }
        }

        return $dados;
    }


    /**
     * loginWP
     * Funcao de login com wordpress
     *
     * @return array
     */
    public function loginWP()
    {
        $data = 'username=anderson&password=2!xo5$obbI2Bqy*8oAflC)gL';
        $server = 'https://corretorparceiro.com.br';
        $curlObj = curl_init();

        curl_setopt($curlObj, CURLOPT_URL, "$server/wp-json/jwt-auth/v1/token");
        curl_setopt($curlObj, CURLOPT_POST, 1);
        curl_setopt($curlObj, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curlObj);

        $response = json_decode($response, true);

        curl_close($curlObj);

        return $response;
    }

    /**
     * getTagsWP
     * Consulta as tags existentes 
     * 
     * @param  mixed $token
     * @return array
     */
    public function getTagsWP()
    {
        $curlObj = curl_init();
        $server = 'https://corretorparceiro.com.br';

        curl_setopt($curlObj, CURLOPT_URL, "$server/wp-json/wp/v2/categories");
        curl_setopt($curlObj, CURLOPT_POST, 0);
        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curlObj);
        $response = json_decode($response, true);
        curl_close($curlObj);

        $tags = [];

        foreach ($response as $key => $value) {
            $tags[$value['id']] = $value['name'];
        }

        return $tags;
    }



    /**
     * updateTag
     * 
     * Atualiza tag, busando o id e atualizando a tag
     *
     * @param  mixed $nameTagOld
     * @param  mixed $nameTagNew
     * @param  mixed $token
     * @return void
     */
    public function updateTag($nameTagOld, $nameTagNew, $token)
    {
        $server = 'https://corretorparceiro.com.br';
        $tagsAtuais = [];
        $tagsAtuais = $this->getTagsWP();
        $idTag = array_search(str_replace(' ', '-', strtolower($nameTagOld)), $tagsAtuais);
        $autorization = "Authorization: Bearer $token";

        $tagCustom = str_replace(' ', '-', strtolower($nameTagNew));
        $jsonData = json_encode([
            "description" => $tagCustom,
            "name" => $tagCustom,
            "slug" => $tagCustom,
        ], true);

        $curlObjUpdate = curl_init();

        curl_setopt($curlObjUpdate, CURLOPT_URL, "$server/wp-json/wp/v2/categories/$idTag");
        curl_setopt($curlObjUpdate, CURLOPT_POST, 1);
        curl_setopt($curlObjUpdate, CURLOPT_HTTPHEADER, ['content-type: application/json',  $autorization]);
        curl_setopt($curlObjUpdate, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($curlObjUpdate, CURLOPT_RETURNTRANSFER, true);

        curl_exec($curlObjUpdate);
        curl_close($curlObjUpdate);
    }

    /**
     * Criaçao de tags no wordpress
     *
     * @param  mixed $tag
     * @return void
     */
    public function createTagsWP($nameTag, $token)
    {
        $autorization = "Authorization: Bearer $token";
        $server = 'https://corretorparceiro.com.br';

        $tagCustom = str_replace(' ', '-', strtolower($nameTag));
        $jsonData = json_encode([
            "description" => $tagCustom,
            "name" => $tagCustom,
            "slug" => $tagCustom,
        ], true);

        $curlObjCreate = curl_init();

        curl_setopt($curlObjCreate, CURLOPT_URL, "$server/wp-json/wp/v2/categories");
        curl_setopt($curlObjCreate, CURLOPT_POST, 1);
        curl_setopt($curlObjCreate, CURLOPT_HTTPHEADER, ['content-type: application/json',  $autorization]);
        curl_setopt($curlObjCreate, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($curlObjCreate, CURLOPT_RETURNTRANSFER, true);

        curl_exec($curlObjCreate);
        curl_close($curlObjCreate);
    }
}
