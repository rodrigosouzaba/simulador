<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Alertas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Simulacoes
 * @property \Cake\ORM\Association\BelongsTo $PfCalculos
 * @property \Cake\ORM\Association\BelongsTo $OdontoCalculos
 *
 * @method \App\Model\Entity\Alerta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Alerta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Alerta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Alerta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Alerta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SisModulosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sis_modulos');

        $this->hasMany('SisFuncionalidades');

        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    public function afterSave($event, $entity, $options) {
        // ASSOCIA FUNCIONALIDADES AO MÓDULO

        foreach($event->data['entity']->funcionalidade as $v){
            $objFuncionalidade = $this->SisFuncionalidades->newEntity();

            $objFuncionalidade->id = $v;
            $objFuncionalidade->sis_modulo_id = $event->data['entity']->id;
            
            $this->SisFuncionalidades->save($objFuncionalidade);
        }
    }
  
}
