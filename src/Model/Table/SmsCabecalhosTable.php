<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SmsCabecalhos Model
 *
 * @method \App\Model\Entity\SmsCabecalho get($primaryKey, $options = [])
 * @method \App\Model\Entity\SmsCabecalho newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SmsCabecalho[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SmsCabecalho|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SmsCabecalho patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SmsCabecalho[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SmsCabecalho findOrCreate($search, callable $callback = null)
 */
class SmsCabecalhosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sms_cabecalhos');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
