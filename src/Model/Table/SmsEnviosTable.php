<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SmsEnvios Model
 *
 * @property \App\Model\Table\SmsListasTable&\Cake\ORM\Association\BelongsTo $SmsListas
 * @property \App\Model\Table\SmsSituacoesTable&\Cake\ORM\Association\BelongsTo $SmsSituacoes
 * @property \App\Model\Table\SmsCabecalhosTable&\Cake\ORM\Association\BelongsTo $SmsCabecalhos
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\SmsRespostasEnviosTable&\Cake\ORM\Association\HasMany $SmsRespostasEnvios
 *
 * @method \App\Model\Entity\SmsEnvio get($primaryKey, $options = [])
 * @method \App\Model\Entity\SmsEnvio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SmsEnvio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SmsEnvio|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SmsEnvio saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SmsEnvio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SmsEnvio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SmsEnvio findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SmsEnviosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sms_envios');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SmsSituacoes', [
            'foreignKey' => 'sms_situacao_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SmsCabecalhos', [
            'foreignKey' => 'sms_cabecalho_id',
        ]);
        $this->belongsTo('SisGrupos', [
            'foreignKey' => 'sms_lista_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('SmsRespostasEnvios', [
            'foreignKey' => 'sms_envio_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('mensagem')
            ->requirePresence('mensagem', 'create')
            ->notEmptyString('mensagem');

        $validator
            ->dateTime('data_envio')
            ->allowEmptyDateTime('data_envio');

        $validator
            ->dateTime('data_agendamento')
            ->allowEmptyDateTime('data_agendamento');

        $validator
            ->integer('autorizado')
            ->allowEmptyString('autorizado');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao', 300)
            ->allowEmptyString('descricao');

        $validator
            ->scalar('tipo_envio')
            ->maxLength('tipo_envio', 50)
            ->allowEmptyString('tipo_envio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sms_lista_id'], 'SisGrupos'));
        $rules->add($rules->existsIn(['sms_situacao_id'], 'SmsSituacoes'));
        $rules->add($rules->existsIn(['sms_cabecalho_id'], 'SmsCabecalhos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
