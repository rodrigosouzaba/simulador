<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SmsListas Model
 *
 * @property \Cake\ORM\Association\HasMany $Envios
 * @property \Cake\ORM\Association\HasMany $SmsEnvios
 *
 * @method \App\Model\Entity\SmsLista get($primaryKey, $options = [])
 * @method \App\Model\Entity\SmsLista newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SmsLista[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SmsLista|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SmsLista patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SmsLista[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SmsLista findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SmsListasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sms_listas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Envios', [
            'foreignKey' => 'sms_lista_id'
        ]);
        $this->hasMany('SmsEnvios', [
            'foreignKey' => 'sms_lista_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->allowEmpty('caminho');

        return $validator;
    }
}
