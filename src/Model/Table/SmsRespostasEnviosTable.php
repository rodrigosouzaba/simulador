<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SmsRespostasEnvios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SmsEnvios
 *
 * @method \App\Model\Entity\SmsRespostasEnvio get($primaryKey, $options = [])
 * @method \App\Model\Entity\SmsRespostasEnvio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SmsRespostasEnvio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SmsRespostasEnvio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SmsRespostasEnvio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SmsRespostasEnvio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SmsRespostasEnvio findOrCreate($search, callable $callback = null)
 */
class SmsRespostasEnviosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sms_respostas_envios');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('SmsEnvios', [
            'foreignKey' => 'sms_envio_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('celular');

        $validator
            ->dateTime('data')
            ->allowEmpty('data');

        $validator
            ->allowEmpty('resultado');

        $validator
            ->allowEmpty('porta_chipeira');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sms_envio_id'], 'SmsEnvios'));

        return $rules;
    }
}
