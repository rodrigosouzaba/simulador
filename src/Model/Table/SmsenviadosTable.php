<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Smsenviados Model
 *
 * @method \App\Model\Entity\Smsenviado get($primaryKey, $options = [])
 * @method \App\Model\Entity\Smsenviado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Smsenviado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Smsenviado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Smsenviado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Smsenviado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Smsenviado findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SmsenviadosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('smsenviados');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('destinatario');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('cod_resultado');

        $validator
            ->allowEmpty('resultado');

        return $validator;
    }
    
    public function salvar($dados = null){
    	$entidadeSMS = $this->newEntity();
    	$entidadeSMS = $this->patchEntity($entidadeSMS, $dados);
    	$this->save($entidadeSMS);
    }
}
