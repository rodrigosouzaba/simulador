<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TabelasGeradasProdutos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TabelasGeradas
 * @property \Cake\ORM\Association\BelongsTo $Produtos
 *
 * @method \App\Model\Entity\TabelasGeradasProduto get($primaryKey, $options = [])
 * @method \App\Model\Entity\TabelasGeradasProduto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TabelasGeradasProduto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TabelasGeradasProduto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TabelasGeradasProduto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TabelasGeradasProduto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TabelasGeradasProduto findOrCreate($search, callable $callback = null)
 */
class TabelasGeradasProdutosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tabelas_geradas_produtos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('TabelasGeradas', [
            'foreignKey' => 'tabela_gerada_id',
            'joinType' => 'INNER'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tabela_gerada_id'], 'TabelasGeradas'));

        return $rules;
    }
}
