<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TabelasGeradas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Estados
 *
 * @method \App\Model\Entity\TabelasGerada get($primaryKey, $options = [])
 * @method \App\Model\Entity\TabelasGerada newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TabelasGerada[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TabelasGerada|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TabelasGerada patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TabelasGerada[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TabelasGerada findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TabelasGeradasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tabelas_geradas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            // 	        'dependent' => true,
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'INNER'
        ]);
        /*
        $this->belongsTo('TabelasGeradasProdutos', [
            'foreignKey' => 'tabela_gerada_id',
            'joinType' => 'INNER'
        ]);
*/
        $this->hasMany('TabelasGeradasProdutos', [
            'foreignKey' => 'tabela_gerada_id'
        ]);

        $this->belongsToMany('Tabelas', [
            'foreignKey' => 'link_id',
            'targetForeignKey' => 'tabela_id',
            'joinTable' => 'links_tabelas'
        ]);

        $this->belongsToMany('PfTabelas', [
            'foreignKey' => 'link_id',
            'targetForeignKey' => 'pf_tabela_id',
            'joinTable' => 'links_tabelas'
        ]);

        $this->belongsToMany('OdontoTabelas', [
            'foreignKey' => 'link_id',
            'targetForeignKey' => 'odonto_tabela_id',
            'joinTable' => 'links_tabelas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('ramo', 'create')
            ->notEmpty('ramo');

        $validator
            ->requirePresence('operadora', 'create')
            ->notEmpty('operadora');

        $validator
            ->allowEmpty('vidas');

        $validator
            ->allowEmpty('coparticipacao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
