<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TabelasRegioes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Tabelas
 * @property \Cake\ORM\Association\BelongsTo $Regioes
 *
 * @method \App\Model\Entity\TabelasRegio get($primaryKey, $options = [])
 * @method \App\Model\Entity\TabelasRegio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TabelasRegio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TabelasRegio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TabelasRegio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TabelasRegio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TabelasRegio findOrCreate($search, callable $callback = null)
 */
class TabelasRegioesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tabelas_regioes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Tabelas', [
            'foreignKey' => 'tabela_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Regioes', [
            'foreignKey' => 'regiao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tabela_id'], 'Tabelas'));
        $rules->add($rules->existsIn(['regiao_id'], 'Regioes'));

        return $rules;
    }
}
