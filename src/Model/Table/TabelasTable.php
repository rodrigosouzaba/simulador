<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Produtos
 * @property \Cake\ORM\Association\BelongsToMany $Regioes
 *
 * @method \App\Model\Entity\Tabela get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tabela newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tabela[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tabela|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tabela patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tabela[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tabela findOrCreate($search, callable $callback = null)
 */
class TabelasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        // In a Table's initialize method.
        $this->hasMany('TabelasRegioes', [
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('AgendamentosTabelas', [
            'cascadeCallbacks' => true,
        ]);
        // In a Table's initialize method.
        $this->hasMany('TabelasCnpjs', [
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->belongsToMany('Cnpjs', [
            'foreignKey' => 'tabela_id',
            'joinTable' => 'tabelas_cnpjs'
        ]);
        // In a Table's initialize method.
        $this->hasMany('SimulacoesTabelas', [
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('TabelasFiltradas', [
            'foreignKey' => 'tabela_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        // In a Table's initialize method.
        $this->belongsTo('Produtos', [
            'foreignKey' => 'produto_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Operadoras', [
            'foreignKey' => 'operadora_id',
            'joinType' => 'LEFT'
        ]);
        /*
        $this->belongsTo('Ramos', [
            'foreignKey' => 'ramo_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Modalidades', [
            'foreignKey' => 'modalidade_id',
            'joinType' => 'LEFT'
        ]);
*/
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'LEFT'
        ]);
        /*
        $this->belongsTo('Regioes', [
            'foreignKey' => 'regiao_id',
            'joinType' => 'LEFT'
        ]);
*/
        $this->belongsTo('Abrangencias', [
            'foreignKey' => 'abrangencia_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Tipos', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('TiposProdutos', [
            'foreignKey' => 'tipo_produto_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Coberturas', [
            'foreignKey' => 'cobertura_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('AreasComercializacoes', [
            'foreignKey' => 'area_comercializacao_id',
            'joinType' => 'LEFT'
        ]);
        /*
        $this->belongsTo('Regioes', [
            'foreignKey' => 'regiao_id',
            'joinType' => 'LEFT'
        ]);
*/

        $this->belongsToMany('Municipios', [
            'foreignKey' => 'tabela_id',
            'targetForeignKey' => 'municipio_id',
            'joinTable' => 'regioes_municipios'
        ]);

        // NOVAS ASSOCIAÇÕES
        $this->belongsTo('FormasPagamentos');
        $this->belongsTo('Informacoes', [
            'foreignKey' => 'informacao_id',
        ]);
        $this->belongsTo('Opcionais', [
            'foreignKey' => 'opcional_id',
        ]);
        $this->belongsTo('Carencias');
        $this->belongsTo('Reembolsos')
            ->setProperty('reembolsoEntity');
        $this->belongsTo('Observacoes', [
            'foreignKey' => 'observacao_id',
        ]);
        $this->belongsTo('Redes');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->date('vigencia')
            ->requirePresence('vigencia', 'create')
            ->notEmpty('vigencia');

        /*
        $validator
                ->add('faixa1', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa2', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa3', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa4', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa5', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa6', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa7', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa8', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa9', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
        $validator
                ->add('faixa10', 'valid', [
                    'rule' => function($value) {
                        return (bool) preg_match('/^[a-z0-9\-]+$/', $value >0);
                    },
                    'message' => 'Valor inválido'
        ]);
*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['produto_id'], 'Produtos'));
        //         $rules->add($rules->existsIn(['ramo_id'], 'Ramos'));
        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));
        $rules->add($rules->existsIn(['tipo_id'], 'Tipos'));
        $rules->add($rules->existsIn(['tipo_produto_id'], 'TiposProdutos'));
        //         $rules->add($rules->existsIn(['modalidade_id'], 'Modalidades'));
        $rules->add($rules->existsIn(['abrangencia_id'], 'Abrangencias'));
        //         $rules->add($rules->existsIn(['regiao_id'], 'Regioes'));



        return $rules;
    }
}
