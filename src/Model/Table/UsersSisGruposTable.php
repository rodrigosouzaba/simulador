<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;

/**
 * Alertas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Simulacoes
 * @property \Cake\ORM\Association\BelongsTo $PfCalculos
 * @property \Cake\ORM\Association\BelongsTo $OdontoCalculos
 *
 * @method \App\Model\Entity\Alerta get($primaryKey, $options = [])
 * @method \App\Model\Entity\Alerta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Alerta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Alerta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Alerta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Alerta findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersSisGruposTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo('Users');
        $this->belongsTo('SisGrupos', [
            'foreignKey' => 'sis_grupo_id',
            'joinType' => 'left'
        ]);

        $this->primaryKey('id');
    }

    public function getPerfis($idUser)
    {   
        return $this->find('list', ['keyField' => 'sis_grupo_id','valueField' => 'sis_grupo.name_wp'])
                        ->contain(['SisGrupos'])
                        ->where(['UsersSisGrupos.user_id' => $idUser])
                        ->toArray();
    }
  
}
