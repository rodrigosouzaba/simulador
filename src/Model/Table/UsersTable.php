<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;
use Cake\I18n\Time;


use Cake\Network\Http\Client;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Datasource\ConnectionManager;
use Cake\Collection\Collection;
use Rest\Controller\RestController;
use Cake\Http\Response;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    protected $_virtual = [
        'ativo'
    ];
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('nome');
        $this->setPrimaryKey('id');

        $this->belongsTo('Imagens', [
            'foreignKey' => 'imagem_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('TabelasGeradas', [
            'foreignKey' => 'user_id',
            // 	        'dependent' => true
        ]);

        $this->hasMany('PfCalculos', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('OdontoCalculos', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);

        $this->hasMany('UsersGrupos', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);

        $this->hasMany('Simulacoes', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Filtros', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('OdontoFiltros', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PerfisEmpresariais', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PfCalculos', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('TabelasGeradas', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Indicados', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Agendamentos', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PfFiltros', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('SmsEnvios', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('UsersSisGrupos', ['cascadeCallbacks' => true]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Simulacoes.encontrarTabelas' => [
                    'ultimocalculo' => 'always'
                ]
            ]
        ]);
    }


    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create'); // id field is not required when creating a new user
        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'CPF já cadastrado']);
        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'E-mail já cadastrado']);
        $validator
            ->requirePresence('celular', 'create')
            ->notEmpty('celular')
            ->add('celular', 'unique', ['on' => 'create', 'rule' => 'validateUnique', 'provider' => 'table', 'message' => 'Celular já cadastrado']);
        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password', 'Senha Obrigatória', 'create')
            ->add('password', [
                'length' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Senha deve possuir ao menos 6 dígitos',
                ]
            ]);
        $validator
            ->requirePresence('confirmacao', 'create')
            ->notEmpty('confirmacao')
            ->allowEmpty('confirmacao', 'update')
            ->add('confirmacao', 'compareWith', [
                'rule' => ['compareWith', 'password'],
                'message' => 'Confirmação de senha não Confere com senha digitada'
            ]);

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');
        $validator
            ->requirePresence('sobrenome', 'create')
            ->notEmpty('sobrenome');
        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');
        $validator
            ->requirePresence('data_nascimento', 'create')
            ->notEmpty('data_nascimento');
        $validator
            ->requirePresence('role', 'create')
            ->notEmpty('role');

        return $validator;
    }
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['celular']));
        $rules->add($rules->existsIn(['imagem_id'], 'Imagens'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        return $rules;
    }

    /*
    * Return Fullname
    */
    public function getFullname()
    {
        $name = $this->nome . ' ' . $this->sobrenome;
        return $name;
    }

    public function zenvia($id = null, $msg = null)
    {
        $usuario = $this->get($id);
        $caracteres = array("(", ")", " ", "-");
        $destinatario = "55" . str_replace($caracteres, "", $usuario['celular']);

        $json = json_encode([
            "sendSmsRequest" => [
                "from" => "Corretor Parceiro",
                "to" => $destinatario,
                "msg" => mb_convert_encoding($msg . ". Boas vendas! https://www.corretorparceiro.com.br", 'utf-8', "auto"),
                "callbackOption" => "FINAL",
                "aggregateId" => "32807"
            ]
        ]);

        $ch = curl_init('https://api-rest.zenvia.com/services/send-sms');

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Basic bmF0dXNlZy5zbXNvbmxpbmU6TllJWTNsYzNoVQ==', 'Accept:application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $result = json_decode($result);

        if ($result->sendSmsResponse->statusCode === "00") {
            $resposta = $result;
        } else {
            $resposta =  "Erro ao salvar SMS";
        }

        return $resposta;
    }

    public function email($msg = null, $destinatario = null, $assunto = null)
    {
        $email = new Email('default');
        $email->transport('calculos');
        $email->from('nao-responda@corretorparceiro.com.br', 'Plataforma Digital - Corretor Parceiro')
            ->to($destinatario)
            ->sender('nao-responda@corretorparceiro.com.br', 'Plataforma Digital - Corretor Parceiro')
            ->replyTo('nao-responda@corretorparceiro.com.br')
            ->emailFormat('html')
            ->subject($assunto)
            ->send('<html><img src="https://corretorparceiro.com.br/app/img/logo-oficial.jpg" width="230"/>'
                . '<br/>'
                . '<br/>'
                . $msg
                . ''
                . '<br/>'
                . '</html>');
    }

    public function joinUsers($params = null)
    {

        if (!empty($params)) {
            $string = null;
            foreach ($params as $item) {
                debug($item);
                if (empty($string)) {
                    $string = $string . key($item) . ' = ' . current($item);
                } else {
                    $string = " AND " . $string . key($item) . ' = ' . current($item);
                }
            }
            debug($string);
            $params = "WHERE " .  $params;
        } else {
            $params = '';
        }

        // 	    ConnectionManager::config('default');
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute("SELECT
	    	(select count(`s`.`id`) FROM `simulacoes` `s` where ((`s`.`data` >= (now() - interval 30 day)) and (`s`.`user_id` = `u`.`id`))) AS `calculosmes_pme`,
	    	(select count(`pf`.`id`) from `pf_calculos` `pf` where ((`pf`.`created` >= (now() - interval 30 day)) and (`pf`.`user_id` = `u`.`id`))) AS `calculosmes_pf`,
	    	(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 30 day)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF'))) AS `calculosmes_odontopf`,
	    	(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 30 day)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ'))) AS `calculosmes_odontopj`,
	    	(select count(`tg`.`id`) from `tabelas_geradas` `tg` where ((`tg`.`user_id` = `u`.`id`) and (`tg`.`pdf` is not null) and (`tg`.`created` >= (now() - interval 30 day)))) AS `tabelasgeradasmes`,
	    	(select count(`s`.`data`) from `simulacoes` `s` where ((`s`.`data` >= (now() - interval 36 hour)) and (`s`.`user_id` = `u`.`id`))) AS `calculosdia_pme`,
	    	(select count(`s`.`id`) from `simulacoes` `s` where (`s`.`user_id` = `u`.`id`)) AS `calculos_pme`,
	    	(select count(`tg`.`id`) from `tabelas_geradas` `tg` where ((`tg`.`created` >= (now() - interval 36 hour)) and (`tg`.`user_id` = `u`.`id`) and (`tg`.`pdf` is not null))) AS `tabelas_geradas_dia`,
	    	(select count(`tg`.`id`) from `tabelas_geradas` `tg` where ((`tg`.`user_id` = `u`.`id`) and (`tg`.`pdf` is not null))) AS `total_tabelas_geradas`,
	    	(select count(`pf`.`id`) from `pf_calculos` `pf` where ((`pf`.`created` >= (now() - interval 36 hour)) and (`pf`.`user_id` = `u`.`id`))) AS `calculosdia_pf`,
	    	(select count(`pf`.`id`) from `pf_calculos` `pf` where (`pf`.`user_id` = `u`.`id`)) AS `calculos_pf`,
	    	(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 36 hour)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF'))) AS `calculosdia_odonto_pf`,
	    	(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF'))) AS `calculos_odonto_pf`,
	    	(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`data` >= (now() - interval 36 hour)) and (`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ'))) AS `calculosdia_odonto_pj`,
	    	(select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ'))) AS `calculos_odonto_pj`,
	    	((((select count(`s`.`id`) from `simulacoes` `s` where (`s`.`user_id` = `u`.`id`)) + (select count(`pf`.`id`) from `pf_calculos` `pf` where (`pf`.`user_id` = `u`.`id`))) + (select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PF')))) + (select count(`o`.`id`) from `odonto_calculos` `o` where ((`o`.`user_id` = `u`.`id`) and (`o`.`tipo_pessoa` = 'PJ')))) AS `total_calculos`,
	    	`u`.`id` AS `id`,`u`.`user_pai_id` AS `user_pai_id`,`u`.`imagem_id` AS `imagem_id`,`u`.`bloqueio` AS `bloqueio`,`u`.`username` AS `username`,`u`.`password` AS `password`,`u`.`role` AS `role`,`u`.`nome` AS `nome`,`u`.`sobrenome` AS `sobrenome`,`u`.`created` AS `created`,`u`.`modified` AS `modified`,`u`.`email` AS `email`,`u`.`validacao` AS `validacao`,`u`.`codigo` AS `codigo`,`u`.`celular` AS `celular`,`u`.`tipo` AS `tipo`,`u`.`ultimologin` AS `ultimologin`,`u`.`ultimocalculo` AS `ultimocalculo`,`u`.`data_nascimento` AS `data_nascimento`,`u`.`site` AS `site`,`u`.`whatsapp` AS `whatsapp`,`u`.`facebook` AS `facebook`,`u`.`senha_provisoria` AS `senha_provisoria`,`u`.`estado_id` AS `estado_id`,`u`.`logado` AS `logado`,`u`.`codigo_email` AS `codigo_email`,`u`.`validacao_email` AS `validacao_email`,`e`.`nome` AS `estado_nome`,`i`.`id` AS `id_da_imagem`,`i`.`caminho` AS `imagem_caminho`,`i`.`nome` AS `imagem_nome` from ((((`users` `u` left join `estados` `e` on((`e`.`id` = `u`.`estado_id`))) left join `pf_calculos` `pf` on((`pf`.`user_id` = `u`.`id`))) left join `simulacoes` `s` on((`s`.`user_id` = `u`.`id`))) left join `imagens` `i` on((`i`.`id` = `u`.`imagem_id`))) " . $params . " group by `u`.`id`;");

        $rows = $stmt->fetchAll('assoc');
        /*
	    	debug($rows);
	    	die;
*/
        return $rows;
    }

    public function afterSave($event, $entity, $options)
    {
        // PREPARA DADOS

        if (!empty($event->data['entity']->sis_grupo) && isset($event->data['entity']->sis_grupo)) {
            $dados = $this->_extractFieldsHABTM($event->data['entity']->sis_grupo, $event->data['entity']->id, 'user_id', 'sis_grupo_id');

            // APAGA REGISTROS RELACIONADOS AO ID
            $this->UsersSisGrupos->deleteAll(['user_id' => $event->data['entity']->id]);

            // ASSOCIA PERMISSÕES A FUNCIONALIDADE
            foreach ($dados as $value) {
                $obj = $this->UsersSisGrupos->newEntity();

                $obj->sis_grupo_id = $value['sis_grupo_id'];
                $obj->user_id = $event->data['entity']->id;
                $this->UsersSisGrupos->save($obj);
            }
        }
    }

    /**
     * 1 - data pra quem eu quero 
     * 2 - Id da tabela origem
     * 3 - nome da chave da tabela origem,
     * 4 - nome da chave da tabela relacional (lookup)
     */
    public function _extractFieldsHABTM($data, $id, $chave, $lookup)
    {
        $dados = [];

        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $dados[$k][$chave] = $id;
                $dados[$k][$lookup] = $v;
            }
        }

        return $dados;
    }
}
