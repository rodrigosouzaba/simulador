<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VendasOnlinesOperadoras Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $Operadoras
 * @property \Cake\ORM\Association\HasMany $LinksPersonalizados
 *
 * @method \App\Model\Entity\VendasOnlinesOperadora get($primaryKey, $options = [])
 * @method \App\Model\Entity\VendasOnlinesOperadora newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\VendasOnlinesOperadora[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\VendasOnlinesOperadora|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VendasOnlinesOperadora patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\VendasOnlinesOperadora[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\VendasOnlinesOperadora findOrCreate($search, callable $callback = null)
 */ class VendasOnlinesOperadorasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('vendas_onlines_operadoras');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('VendasOnlines', [
            'foreignKey' => 'id_venda_online',
            'joinType' => 'LEFT'
        ]);
        $this->belongsToMany('Estados', [
            'foreignKey' => 'venda_online_operadora_id',
            'targetForeignKey' => 'estado_id',
            'joinTable' => 'vendas_onlines_operadoras_estados',
            'through' => 'VendasOnlinesOperadorasEstados'
        ]);
        $this->belongsTo('Operadoras', [
            'foreignKey' => 'operadora_id'
        ]);
        $this->hasMany('LinksPersonalizados', [
            'foreignKey' => 'vendas_onlines_operadora_id',
            'dependent' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')->allowEmpty('id', 'create');
        $validator
            ->allowEmpty('imagem');
        $validator
            ->requirePresence('linha1', 'create')->notEmpty('linha1');
        $validator
            ->requirePresence('linha2', 'create')->notEmpty('linha2');
        $validator
            ->requirePresence('tooltip', 'create')->notEmpty('tooltip');
        $validator
            ->integer('prioridade')->allowEmpty('prioridade');
        $validator
            ->allowEmpty('link_video');
        $validator
            ->allowEmpty('aviso');
        $validator
            ->integer('id_venda_online')->requirePresence('id_venda_online', 'create')->notEmpty('id_venda_online');
        $validator
            ->allowEmpty('manual');
        $validator
            ->integer('status')->allowEmpty('status');
        $validator
            ->integer('redirect')->allowEmpty('redirect');
        $validator
            ->allowEmpty('ramo');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['operadora_id'], 'Operadoras'));
        $rules->add($rules->existsIn(['id_venda_online'], 'VendasOnlines'));

        return $rules;
    }
}
