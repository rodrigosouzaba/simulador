<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VendasOnlines Model
 *
 * @method \App\Model\Entity\VendasOnline get($primaryKey, $options = [])
 * @method \App\Model\Entity\VendasOnline newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\VendasOnline[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\VendasOnline|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VendasOnline patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\VendasOnline[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\VendasOnline findOrCreate($search, callable $callback = null)
 */ class VendasOnlinesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('vendas_onlines');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('VendasOnlinesOperadoras', ['foreignKey' => 'id_venda_online',]);
        $this->hasOne('Funcionalidades');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')->allowEmpty('id', 'create');
        $validator
            ->requirePresence('nome', 'create')->notEmpty('nome');
        $validator
            ->integer('ordem')->requirePresence('ordem', 'create')->notEmpty('ordem')->add(
                'ordem',
                'unique',
                [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Prioridade em uso'
                ]
            );
        return $validator;
    }
}
