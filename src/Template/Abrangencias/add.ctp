<?php echo $this->Form->create($abrangencia, ['class' => "form-inline"]) ?>

<div class="col-md-12 fonteReduzida">
    <?php
echo $this->Form->input('nome');
echo $this->Form->input('descricao');
?>
</div>
<div class="col-md-12 fonteReduzida centralizada">
<div class="spacer-lg"></div>
<?php echo $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-save', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => 'btn btn-md btn-primary']) ?>
<?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['action' => 'index'], ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false]);?>
</div>
<?php echo $this->Form->end() ?>
