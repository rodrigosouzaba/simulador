<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $abrangencia->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $abrangencia->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Abrangencias'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="abrangencias form large-9 medium-8 columns content">
    <?= $this->Form->create($abrangencia) ?>
    <fieldset>
        <legend><?= __('Edit Abrangencia') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
