<?php $this->assign('title', $title); ?>
<?=
$this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Abrangência', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addAbrangencia']);
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('Código') ?></th>-->
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('descricao') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($abrangencias as $abrangencia): ?>
            <tr>
                <!--<td><?= $this->Number->format($abrangencia->id) ?></td>-->
                <td><?= h($abrangencia->nome) ?></td>
                <td><?= h($abrangencia->descricao) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $abrangencia->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $abrangencia->id], ['confirm' => __('Tem certeza que deseja excluir a Abrangência {0}?', $abrangencia->nome)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
