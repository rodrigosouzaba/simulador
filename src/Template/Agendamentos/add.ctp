<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<?= $this->Form->create($agendamento) ?>
<div class="col-md-12 fonteReduzida">
    <div class="clearfix">&nbsp;</div>
    <?= $this->Form->hidden('user_id', ['value' => $sessao['id']]) ?>
    <?= $this->Form->hidden('situacao', ['value' => 'PENDENTE']) ?>
    <div class="col-md-6"><?= $this->Form->input('data_alteracao'); ?></div>
    <div class="col-md-6"><?= $this->Form->input('nova_vigencia'); ?></div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-6"><?= $this->Form->input('percentual'); ?></div>
    <div class="col-md-6"><?= $this->Form->input('operadora_id', ['options' => $operadoras, 'empty' => 'Selecione a OPERADORA']); ?></div>
</div>
<div class="col-md-12 fonteReduzida">
    <div id="tabelas">

    </div>
    <div class="col-md-12">
        <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => "btn btn-primary btn-md "]) ?>

    </div>
</div>

<?= $this->Form->end() ?>


<div class="col-md-2 fonteReduzida" id="loading">&nbsp;</div> 



<script type="text/javascript">
    $(document).ready(function () {
        if ('<?= $this->request->action ?>' === 'edit') {
            $('#operadora-id').trigger('change');
            var arrayFromPHP = <?= json_encode($agendamento->toArray()) ?>;
            $.each(arrayFromPHP, function (i, elem) {
                if (i === 'tabelas') {
                    $.each(elem, function (a, b) {
                        var r = '#' + b.id;
                        $("#" + b.id).prop("checked", true);
                    });
                }
            });
        }
    });
    $("#operadora-id").change(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>agendamentos/filtrar_tabelas/" + $("#operadora-id").val(),
            beforeSend: function () {
                $('#loading').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                $("#tabelas").empty();
                $("#tabelas").append(data);
                $("#loading").empty();
            }
        });
    });


    $("#5").prop("checked", true);


</script>


