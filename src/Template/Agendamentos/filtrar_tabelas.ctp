<div class="col-md-12 well well-sm" style="">
    <?php if ($tabelas) { ?>
        <h4 class="centralizada">Selecione as Tabelas para aplicar o reajuste na data definida</h4>
        <div class="col-md-12" style="overflow-y: scroll; height: 200px;">
            <?php
            foreach ($tabelas as $tabela) {
//                debug($tabela);
//                die();
                switch ($tabela['tipo_contratacao']) {
                    case '0':
                        $contratacao = 'OPCIONAL';
                        break;
                    case '1':
                        $contratacao = 'COMPULSÓRIO';
                        break;
                    case '2':
                        $contratacao = 'AMBOS';

                        break;
                }
                switch ($tabela['tipo_id']) {
                    case '1':
                        $acomodacao = 'ENFERMERIA';
                        break;
                    case '2':
                        $acomodacao = 'APARTAMENTO';
                        break;
                    case '5':
                        $acomodacao = 'AMBULATORIAL (SEM ACOMODAÇÃO)';
                        break;
                }
                echo $this->Form->checkbox('Tabelas['.$tabela['id'].']', ['label' => $tabela['nome'], 'value' => $tabela['id'], 'hiddenField' => 'false', 'id' => $tabela['id']]) . " " . $tabela['nome'] . " - " .$contratacao." - ". $acomodacao ." - ".$tabela['minimo_vidas'] . " a " . $tabela['maximo_vidas'] . " vidas<br/>";
            }
            ?>
        </div>
    <?php } else {
        ?>
        <div class="clearfix">&nbsp;</div>
        <p class="bg-danger centralizada">Nenhuma tabela encontrada nesta Operadora</p>
    <?php }
    ?>
</div>