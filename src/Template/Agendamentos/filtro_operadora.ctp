<div class="col-xs-12">

    <?php if ($agendamentos) { ?>
        <table class="table table-condensed">
            <thead>
                <tr >
                    <!--<td class="tituloTabela" style="width: 2%"><?= 'Sel.' ?></td>-->

                    <td class="tituloTabela">Usuário</td>
                    <td class="tituloTabela">Data de Criação</td>
                    <td class="tituloTabela">Percentual</td>
                    <td class="tituloTabela">Nova Vigência</td>
                    <td class="tituloTabela">Data de Alteração</td>
                    <td class="tituloTabela">Situação</td>
                    <td class="actions tituloTabela"><?= __('Actions') ?></td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($agendamentos as $agendamento) { ?>
                    <tr>
                        <!--<td></td>-->
                        <td><?= $agendamento['user']['nome'] ?></td>
                        <td><?= $agendamento['created'] ?></td>
                        <td><?= $agendamento['percentual'] . " %" ?></td>
                        <td><?= $agendamento['nova_vigencia'] ?></td>
                        <td><b><?= $agendamento['data_alteracao'] ?></b></td>
                        <td><?= $agendamento['situacao'] ?></td>
                        <td>
                            <?=
                            $this->Html->link(
                                    $this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']), '#', [
                                'class' => 'btn btn-xs btn-danger excluir',
                                'escape' => false,
                                'value' => $agendamento['id'],
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Excluir'
                            ]);
                            ?>
                        </td>
                    </tr>

                <?php }
                ?>
            </tbody>
        </table>
    <?php } else {
        ?>
        <div class="clearfix">&nbsp;</div>
        <p class="bg-danger centralizada">Nenhuma tabela encontrada</p>
    <?php }
    ?>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".excluir").click(function () {
            $.ajax({
                type: "post",
                url: "<?php echo $this->request->webroot ?>agendamentos/delete/" + $(this).attr('value'),
                success: function (data) {
                    alert('Excluído com sucesso!');
                    $('#operadora-id').trigger('change');
                }
            });
        });
    });
</script>