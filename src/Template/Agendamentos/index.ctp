<div class="col-md-3 fonteReduzida" >
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Agendamento', [ 'action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'salvar']);
    ?>


</div>
<div class="col-md-3 fonteReduzida" >
    <?= $this->Form->input('operadora_id', ['options' => $operadoras, 'label' => '', 'empty' => 'Selecione a OPERADORA']); ?>
</div>
<div class="col-md-2 fonteReduzida" id="loading">&nbsp;</div> 
<div class="spacer-sm">&nbsp;</div>
<div class="spacer-sm">&nbsp;</div>
<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<div id="listaAgendamentos">

</div>


<script type="text/javascript">
    $("#operadora-id").change(function () {
//        if ($("#operadora-id").val()!=='') {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>agendamentos/filtro_operadora/" + $("#operadora-id").val(),
//                data: $("#operadora-id").serialize(),
            beforeSend: function () {
                $('#loading').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                $("#listaAgendamentos").empty();
                $("#listaAgendamentos").append(data);
                $("#loading").empty();
            }
        });
//        }

    });

</script>


