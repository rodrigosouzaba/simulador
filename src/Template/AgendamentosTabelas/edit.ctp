<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $agendamentosTabela->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $agendamentosTabela->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Agendamentos Tabelas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Agendamentos'), ['controller' => 'Agendamentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agendamento'), ['controller' => 'Agendamentos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="agendamentosTabelas form large-9 medium-8 columns content">
    <?= $this->Form->create($agendamentosTabela) ?>
    <fieldset>
        <legend><?= __('Edit Agendamentos Tabela') ?></legend>
        <?php
            echo $this->Form->input('agendamento_id', ['options' => $agendamentos]);
            echo $this->Form->input('tabela_id', ['options' => $tabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
