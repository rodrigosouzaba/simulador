<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Agendamentos Tabela'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agendamentos'), ['controller' => 'Agendamentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agendamento'), ['controller' => 'Agendamentos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="agendamentosTabelas index large-9 medium-8 columns content">
    <h3><?= __('Agendamentos Tabelas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('agendamento_id') ?></th>
                <th><?= $this->Paginator->sort('tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($agendamentosTabelas as $agendamentosTabela): ?>
            <tr>
                <td><?= $this->Number->format($agendamentosTabela->id) ?></td>
                <td><?= $agendamentosTabela->has('agendamento') ? $this->Html->link($agendamentosTabela->agendamento->id, ['controller' => 'Agendamentos', 'action' => 'view', $agendamentosTabela->agendamento->id]) : '' ?></td>
                <td><?= $agendamentosTabela->has('tabela') ? $this->Html->link($agendamentosTabela->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $agendamentosTabela->tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $agendamentosTabela->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $agendamentosTabela->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $agendamentosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agendamentosTabela->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
