<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Agendamentos Tabela'), ['action' => 'edit', $agendamentosTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Agendamentos Tabela'), ['action' => 'delete', $agendamentosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $agendamentosTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Agendamentos Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agendamentos Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Agendamentos'), ['controller' => 'Agendamentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Agendamento'), ['controller' => 'Agendamentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="agendamentosTabelas view large-9 medium-8 columns content">
    <h3><?= h($agendamentosTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Agendamento') ?></th>
            <td><?= $agendamentosTabela->has('agendamento') ? $this->Html->link($agendamentosTabela->agendamento->id, ['controller' => 'Agendamentos', 'action' => 'view', $agendamentosTabela->agendamento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tabela') ?></th>
            <td><?= $agendamentosTabela->has('tabela') ? $this->Html->link($agendamentosTabela->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $agendamentosTabela->tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($agendamentosTabela->id) ?></td>
        </tr>
    </table>
</div>
