<div class="clearfix">
	<div class="centralizada clearfix">
		<?= $this->Form->create($alerta, ["id" => "form-alerta-add"]) ?>
		<?= $this->Form->hidden("model", ["value" => $ramo]) ?>
		<?= $this->Form->hidden("user_id", ["value" => $sessao["id"]]) ?>
		<?php

		switch ($ramo) {
			case "SPF":
				$campo_id = 'pf_calculo_id';
				break;
			case "SPJ":
				$campo_id = 'simulacao_id';
				break;
			case "OPF":
				$campo_id = 'odonto_calculo_id';
				break;
			case "OPJ":
				$campo_id = 'odonto_calculo_id';
				break;
		}

		?>
		<?= $this->Form->hidden($campo_id, ["value" => $calculo]) ?>

		<?= $this->Form->input('mensagem', ["value" => '', "label" => false, "required" => "required", "placeholder" => "Digite sua observação", "class" => "centralizada"]) ?>

		<div class="col-xs-12 text-center btn-salvar" style="margin: 15px 0 0 0 !important; padding: 0">
			<?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Criar Lembrete', "#", ['class' => "btn btn-default btn-sm toggler", "id" => "salvar-alerta", 'role' => 'button', 'escape' => false]) ?>
		</div>
		<div id="acoes" style="display: none;">
			<div class="col-xs-12 col-md-6 col-md-offset-3 row-lembrete" style="padding: 5px 0 !important;">
				<div id="div-data-alerta">
					<?= $this->Form->input('data_alerta', ['type' => 'text', 'empty' => true, "value" => date("d/m/Y"), "label" => false, 'class' => 'text-center date', 'style' => 'margin: 0;']) ?>
					<span class="col-xs-12 centralizada text-danger" style="margin-top:7px">Escolha o dia que deseja ser lembrado</span>
					<div class="col-xs-12 text-danger centralizada">Você receberá um lembrete por SMS ou E-mail</div>

				</div>
			</div>
		</div>

		<?= $this->Form->end() ?>
	</div>
	<div class="spacer-xs">&nbsp;</div>
	<?php if (!empty($alertas)) { ?>
		<div>

			<div>
				<table cellpadding="0" cellspacing="0" class="table table-hover table-striped fonteReduzida">
					<thead>
						<tr>
							<th style="width: 10%">Criação</th>
							<th style="width: 10%">Lembrete</th>
							<th style="width: 25%">Fase da Negociação</th>
							<th style="width: 50%">Comentários</th>
							<th style="width: 5%">Editar</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($alertas as $alerta) : ?>
							<tr>
								<td><?= h($alerta->created) ?></td>
								<td><?= h($alerta->data_alerta) ?></td>
								<td><?= (!empty($alerta->status_id) ? $alerta->status->nome : '') ?></td>
								<td><?= $alerta->mensagem ?></td>
								<td>
									<?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-edit', 'aria-hidden' => 'true']), "#" . $alerta->id, ['class' => 'btn btn-sm btn-default editalerta', 'role' => 'button', 'escape' => false, 'data-toggle' => 'collapse', "aria-controls" => "collapseExample", "alerta_id" => $alerta->id]) ?>
								</td>

							</tr>
							<tr class="collapse" id="<?= $alerta->id ?>">
								<td colspan="5" id="resposta<?= $alerta->id ?>">
									<div></div>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	<?php } ?>
</div>

<script type="text/javascript">
	$("#divCheckAlerta").css({
		"marginTop": "15px"
	});
	if ($(window).width() < 800) {
		$(".row-lembrete").css({
			"text-align": "center !important"
		});
		$(".btn-salvar").css({
			"text-align": "center !important"
		});
	} else {
		$("#divCheckAlerta").css({
			"textAlign": "left"
		});
		$("#div-data-alerta").css({
			"margin-top": "7px"
		});
		$(".btn-salvar").css({
			"textAlign": "right"
		});
		$("#acoes").css({
			"height": "60px"
		});
	}

	$('#modalLembrete').on('hidden.bs.modal', function(e) {
		var mensagem = $("#modalLembrete #mensagem").val();
		if (mensagem !== "") {
			salvarAlerta();
			$("#modal-alerta").empty();
		}
	})

	$(".checkbox").css("margin-bottom", "0");

	$(".editalerta").click(function() {
		var r = '#resposta' + $(this).attr("alerta_id");
		$.ajax({
			type: "GET",
			url: "<?= $this->request->webroot ?>" + "alertas/edit/" + $(this).attr("alerta_id"),
			success: function(data) {
				$(r).empty();
				$(r).append(data);
			}
		});
	});

	$(".toggler").click(function() {
		$("#acoes").slideToggle();
	})

	function salvarAlerta() {
		$.ajax({
			type: "post",
			url: "<?= $this->request->webroot . "alertas/add/" . $ramo . "/" . $calculo ?>",
			data: $("#form-alerta-add").serialize(),
		});

	};
	$('.date').mask("99/99/9999", {
		placeholder: "__/__/____"
	});
</script>