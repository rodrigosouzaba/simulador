<?= $this->Form->create($alerta, ["id" => "form".$alerta->id]) ?>
    <?php
        echo $this->Form->hidden('id');
        echo $this->Form->hidden('user_id');
        echo $this->Form->hidden('model');
        echo $this->Form->hidden('simulacao_id');
        echo $this->Form->hidden('pf_calculo_id');
        echo $this->Form->hidden('odonto_calculo_id');
        switch($alerta->model){
		    case "SPF":
		    	$campo_id = 'pf_calculo_id';
		    break;
		    case "SPJ":
		    	$campo_id = 'simulacao_id';
		    break;
		    case "OPF":
		    	$campo_id = 'odonto_calculo_id';
		    break;
		    case "OPJ":
			    $campo_id = 'odonto_calculo_id';
		    break;
	    }
    ?>
<div>
		<?= $this->Form->input('mensagem',["label" => false, "required" => "required"]) ?>

</div>

<div class="row" id="acoes" style="min-height: 60px !important" >
	<div class="col-xs-12 col-md-4 row-lembrete divCheckAlertaEdit" style="margin-top: 10px;">
		<?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-clock', 'aria-hidden' => 'true']) .' Agendar Lembrete', "#data-edit". $alerta->id , ['class' => "btn btn-primary btn-sm btn-alerta-edit", "value" => $alerta->id,'role' => 'button', 'escape' => false]) ?>
	</div>
	
	<div class="col-xs-12 col-md-4 row-lembrete centralizada" style="padding: 5px 0 !important;">
		<div id="data-edit<?= $alerta->id?>">
    		<?= $this->Form->input('data_alerta', ['empty' => true, "label" => false, ])?>
			<small class="col-xs-12 centralizada">Escolha quando deseja receber um Lembrete</small>
		</div>
	</div>
	
	<div class="col-xs-12 col-md-4 btn-salvar" style="margin: 10px 0 0 0 !important">
		<?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-save', 'aria-hidden' => 'true']) .' Salvar', "#", ['class' => "btn btn-default btn-sm salvar-edit-alerta", 'role' => 'button', 'escape' => false]) ?>
	</div>
</div>
<div id="resposta-edit<?=$alerta->id?>"></div>

	
<?= $this->Form->end() ?>

<script type="text/javascript">

	if($(window).width() < 800){
        $(".row-lembrete").css({"text-align" : "center !important"});
        $(".btn-salvar").css({"text-align" : "center !important"});
    }else{
	    $("#divCheckAlerta").css({"textAlign" : "left"});        
	    $("#div-data-alerta").css({"margin-top" : "7px"});        
	    $(".btn-salvar").css({"textAlign":"right"});
	    $("#acoes").css({"height" : "60px"});
    }
	
	$("#div-data-alerta").hide();
	$(".checkbox").css("margin-bottom","0");
	
	var a = "<?= $alerta->data_alerta?>";

	if(a){
		$("#data-edit"+ $(this).attr('value')).show();
		$(".btn-alerta-edit").addClass("active");

	}else{
		$("#data-edit"+ <?= $alerta->id?>).hide();
		$(".btn-alerta-edit").removeClass("active");	
	}
	
	$(".btn-alerta-edit").click(function () {
		if($(this).attr('class').includes("active") == true){
			$("#data-edit"+ $(this).attr('value')).hide();
			$(this).removeClass("active");
			$("#data-edit" + $(this).attr('value')).focus();
		}else{
			$(this).addClass("active");
			$("#data-edit"+ $(this).attr('value')).show();
			$("#data-edit"+ $(this).attr('value')).focus();
	
		}

	});
	

	
	$(".editalerta").click(function () {
		var r = '#resposta' + $(this).attr("alerta_id");
		$.ajax({
		        type: "GET",
		        url: "<?= $this->request->webroot ?>" +"alertas/edit/"+ $(this).attr("alerta_id"),
		        success: function (data) {
			        $(r).empty();
			        $(r).append(data);
		        }
		    });
	});
	
	$(".salvar-edit-alerta").click(function () {
		$(".salvar-edit-alerta").attr("disabled", "disabled");
		$.ajax({
		        type: "POST",
				data: $("#form"+"<?=$alerta->id?>").serialize(),
		        url: "<?= $this->request->webroot ?>" +"alertas/edit/<?= $alerta->id?>",
 		        success: function (data) {
			        $.ajax({
				        type: "GET",
				        url: "<?= $this->request->webroot."alertas/add/".$alerta->model."/".$alerta->$campo_id ?>",
				        success: function (data) {
					        $("#modal-alerta").empty();
					        $("#modal-alerta").append(data);
					        $(".salvar-edit-alerta").attr("disabled", "false");
				        }
				    });			        
		        }
		    });
	});

</script>