
<div>
	<div class="col-xs-12 centralizada"> 
	<?= $this->Html->link("Adicionar Lembrete", "#novo-alerta",["class" => "btn btn-default btn-sm", "data-toggle" => "collapse",  "aria-expanded" => "false", "aria-controls" => "novo-alerta", "role" => "button", "id" => "btnAddAlerta"]); ?>
	</div> 
	<div class="col-xs-12 collapse" id="novo-alerta" >
		<div class="col-xs-12" id="add-alerta">
			
		</div>
	</div>
	<div class="fonteReduzida">    
		<table cellpadding="0" cellspacing="0" class="table table-hover table-condensed table-striped">
	        <thead>
	            <tr>
	                <th><?= $this->Paginator->sort('created', ["label" => "Data"]) ?></th>
	                <th><?= $this->Paginator->sort('data_alerta',["label" => "Alerta"]) ?></th>
	                <th><?= $this->Paginator->sort('status_id', ["label" => "Status"]) ?></th>
	                <th style="width: 50%"><?= $this->Paginator->sort('mensagem', ["label" => "Lembrete / Nota"]) ?></th>
	            </tr>
	        </thead>
	        <tbody>
	            <?php foreach ($alertas as $alerta): ?>
	            <tr>
	                <td><?= h($alerta->created) ?></td>
	                <td><?= h($alerta->data_alerta) ?></td>
	                <td><?= (!empty($alerta->status_id) ? $alerta->status->nome : '') ?></td>
	                <td><?= $alerta->mensagem ?></td>
	
	            </tr>
	            <?php endforeach; ?>
	        </tbody>
	    </table>
	</div>
   </div>
   
<script type="text/javascript">
	$("#data-alerta").hide();
	$("#check-alerta").click(function () {
		if($("#check-alerta").is(":checked")){
			$("#data-alerta").show();
		}else{
			$("#data-alerta").hide();
		}
	});
	
	$("#btnAddAlerta").click(function () {
	    $.ajax({
	        type: "GET",
	        url: "<?= $this->request->webroot."alertas/add/".$ramo."/".$calculo ?>",
	        success: function (data) {
		        $("#add-alerta").empty();
		        $("#add-alerta").append(data);
	        }
	    });

    });
</script>