<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Alerta'), ['action' => 'edit', $alerta->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Alerta'), ['action' => 'delete', $alerta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $alerta->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Alertas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Alerta'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Calculos'), ['controller' => 'OdontoCalculos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Calculo'), ['controller' => 'OdontoCalculos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="alertas view large-9 medium-8 columns content">
    <h3><?= h($alerta->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $alerta->has('user') ? $this->Html->link($alerta->user->id, ['controller' => 'Users', 'action' => 'view', $alerta->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Model') ?></th>
            <td><?= h($alerta->model) ?></td>
        </tr>
        <tr>
            <th><?= __('Simulaco') ?></th>
            <td><?= $alerta->has('simulaco') ? $this->Html->link($alerta->simulaco->id, ['controller' => 'Simulacoes', 'action' => 'view', $alerta->simulaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Calculo') ?></th>
            <td><?= $alerta->has('odonto_calculo') ? $this->Html->link($alerta->odonto_calculo->id, ['controller' => 'OdontoCalculos', 'action' => 'view', $alerta->odonto_calculo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($alerta->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Simulacao Id') ?></th>
            <td><?= $this->Number->format($alerta->simulacao_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($alerta->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Data Alerta') ?></th>
            <td><?= h($alerta->data_alerta) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Mensagem') ?></h4>
        <?= $this->Text->autoParagraph(h($alerta->mensagem)); ?>
    </div>
</div>
