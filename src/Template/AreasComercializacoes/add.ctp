<style>
    .estados {
        border: 1px solid #ccc;
        padding: 0px;
        max-height: 300px;
        overflow: auto;
    }

    .select-estado-metropoles,
    .select-estado-municipios {
        display: flex;
        align-items: center;
        padding: 0 15px;
    }

    .select-estado-metropoles:hover,
    .select-estado-municipios:hover {
        background-color: #DDD;
    }

    .select-estado-metropoles span,
    .select-estado-municipios span {
        padding: 5px 0;
    }

    .active {
        background: #08c;
        color: #FFF;
    }
</style>
<div class="container fonteReduzida">
    <?= $this->Form->create($areasComercializaco) ?>
    <fieldset>
        <legend><?= __('Nova Area Comercialização') ?></legend>
        <div class="spacer-md">&nbsp</div>
        <div class="col-md-6">
            <?= $this->Form->input('nome') ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('operadora_id', ['options' => $operadoras]) ?>
        </div>
        <div class="col-md-12">
            <?= $this->Form->input('descricao', ['type' => 'textarea', 'label' => 'Descrição']) ?>
        </div>
    </fieldset>

    <!-- SELEÇÃO DE METRÓPOLES -->
    <div class="spacer-xs">&nbsp</div>
    <legend>Metrópoles</legend>
    <div class="col-md-6">
        <!-- <label>Estados</label> -->
        <div class="estados">
            <?php foreach ($estados as $estado) :
                if (!empty($estado->metropoles)) : ?>
                    <div class="select-estado-metropoles" id="select-estado-<?= $estado->id ?>" estado-id=<?= $estado->id ?>>
                        <span><?= $estado->nome ?></span>
                    </div>
            <?php endif;
            endforeach; ?>
        </div>
    </div>

    <div class="col-md-6 tab-content">
        <!-- <label>Metropoles</label> -->
        <?php foreach ($estados as $estado) : ?>
            <div class="metropoles tab-pane-metropoles" id="tab-metropoles-<?= $estado->id ?>" estado="<?= $estado->id ?>" style="display: none;">
                <div class="flex" style="justify-content: space-between;">
                    <a href='#' id='select-all-<?= $estado->id ?>'>Seleiconar Tudo</a>
                    <a href='#' id='deselect-all-<?= $estado->id ?>'>Limpar</a>
                </div>
                <select name="metropoles[_ids][]" class="multiselect" id="multiple-metropole-estado-<?= $estado->id ?>" estado-id="<?= $estado->id ?>" multiple>
                    <?php $metropoles = (new \Cake\Collection\Collection($estado->metropoles))->combine('id', 'nome')->toArray();
                    foreach ($metropoles as $id => $nome) : ?>
                        <option value="<?= $id ?>"><?= $nome ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <script>
                $(document).ready(function() {

                    var estado = $("#multiple-metropole-estado-<?= $estado->id ?>").attr("estado-id");

                    $("#multiple-metropole-estado-<?= $estado->id ?>").multiSelect({
                        selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                        selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                        afterInit: function(ms) {
                            var that = this,
                                $selectableSearch = that.$selectableUl.prev(),
                                $selectionSearch = that.$selectionUl.prev(),
                                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                                .on('keydown', function(e) {
                                    if (e.which === 40) {
                                        that.$selectableUl.focus();
                                        return false;
                                    }
                                });

                            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                                .on('keydown', function(e) {
                                    if (e.which == 40) {
                                        that.$selectionUl.focus();
                                        return false;
                                    }
                                });
                        },
                        afterSelect: function(values) {
                            this.qs1.cache();
                            this.qs2.cache();

                            var selecionados = $('#multiple-metropole-estado-<?= $estado->id ?> option:selected').length;
                            $("#select-estado-" + estado).addClass("active");
                        },
                        afterDeselect: function(values) {
                            this.qs1.cache();
                            this.qs2.cache();
                            var selecionados = $('#multiple-metropole-estado-<?= $estado->id ?> option:selected').length;
                            if (selecionados == 0) {
                                $("#select-estado-" + estado).removeClass("active");
                            }
                        }
                    })
                })

                $("#select-all-<?= $estado->id ?>").click(function() {
                    $("#multiple-estado-<?= $estado->id ?>").multiSelect('select_all')
                });
                $("#deselect-all-<?= $estado->id ?>").click(function() {
                    $("#multiple-estado-<?= $estado->id ?>").multiSelect('deselect_all')
                });
            </script>
        <?php endforeach; ?>
    </div>
    <!-- SELEÇÃO DE METRÓPOLES -->

    <div class="clearfix">&nbsp</div>
    <div class="spacer-xs">&nbsp</div>
    <legend>Municípios</legend>
    <!-- SELEÇÃO DE MUNICIPIOS -->
    <div class="col-md-6">
        <!-- <label>Estados</label> -->
        <div class="estados">
            <?php foreach ($estados as $estado) : ?>
                <div class="select-estado-municipios" id="select-estado-<?= $estado->id ?>" estado-id=<?= $estado->id ?>>
                    <span><?= $estado->nome ?></span>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="col-md-6 tab-content">
        <!-- <label>Municipios</label> -->
        <?php foreach ($estados as $estado) : ?>
            <div class="municipios tab-pane-municipios" id="tab-municipios-<?= $estado->id ?>" estado="<?= $estado->id ?>" style="display: none;">
                <div class="flex" style="justify-content: space-between;">
                    <a href='#' id='select-all-<?= $estado->id ?>'>Seleiconar Tudo</a>
                    <a href='#' id='deselect-all-<?= $estado->id ?>'>Limpar</a>
                </div>
                <select name="municipios[<?= $estado->id ?>][]" class="multiselect" id="multiple-municipio-estado-<?= $estado->id ?>" estado-id="<?= $estado->id ?>" multiple>
                    <?php $municipios = (new \Cake\Collection\Collection($estado->municipios))->combine('id', 'nome')->toArray();
                    foreach ($municipios as $id => $nome) : ?>
                        <option value="<?= $id ?>"><?= $nome ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <script>
                $(document).ready(function() {
                    var estado = $("#multiple-municipio-estado-<?= $estado->id ?>").attr("estado-id");

                    $("#multiple-municipio-estado-<?= $estado->id ?>").multiSelect({
                        selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                        selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                        afterInit: function(ms) {
                            var that = this,
                                $selectableSearch = that.$selectableUl.prev(),
                                $selectionSearch = that.$selectionUl.prev(),
                                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                                .on('keydown', function(e) {
                                    if (e.which === 40) {
                                        that.$selectableUl.focus();
                                        return false;
                                    }
                                });

                            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                                .on('keydown', function(e) {
                                    if (e.which == 40) {
                                        that.$selectionUl.focus();
                                        return false;
                                    }
                                });
                        },
                        afterSelect: function(values) {
                            this.qs1.cache();
                            this.qs2.cache();

                            var selecionados = $('#multiple-municipio-estado-<?= $estado->id ?> option:selected').length;
                            $("#select-estado-" + estado).addClass("active");
                        },
                        afterDeselect: function(values) {
                            this.qs1.cache();
                            this.qs2.cache();
                            var selecionados = $('#multiple-municipio-estado-<?= $estado->id ?> option:selected').length;
                            if (selecionados == 0) {
                                $("#select-estado-" + estado).removeClass("active");
                            }
                        }
                    })
                })
                $("#select-all-<?= $estado->id ?>").click(function() {
                    $("#multiple-municipio-estado-<?= $estado->id ?>").multiSelect('select_all')
                });
                $("#deselect-all-<?= $estado->id ?>").click(function() {
                    $("#multiple-municipio-estado-<?= $estado->id ?>").multiSelect('deselect_all')
                });
            </script>
        <?php endforeach; ?>
    </div>
    <!-- SELEÇÃO DE MUNICIPIOS -->

</div>


<div class="spacer-md">&nbsp</div>
<div class="col-md-12 text-center">
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
</div>
<?= $this->Form->end() ?>
</div>


<script>
    $(".select-estado-metropoles").click(function() {
        var estado = $(this).attr('estado-id');
        $('.tab-pane-metropoles').hide();
        $('#tab-metropoles-' + estado).show();
    });
    $(".select-estado-municipios").click(function() {
        var estado = $(this).attr('estado-id');
        $('.tab-pane-municipios').hide();
        $('#tab-municipios-' + estado).show();
    });
</script>