<div class="col-xs-12">
    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' Nova Área Comercialização', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary', 'escape' => false]) ?>
</div>
<div class="col-xs-12">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>#</th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('operadora_id') ?></th>
                <th><?= $this->Paginator->sort('descricao') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($areasComercializacoes as $areasComercializaco) : ?>
                <tr>
                    <td><?= (int) $areasComercializaco->operadora->nova ? '' : '<i class="fas fa-exclamation-circle text-danger"></i>' ?></td>
                    <td><?= h($areasComercializaco->nome) ?></td>
                    <td><?= $areasComercializaco->has('operadora') ? $this->Html->link($areasComercializaco->operadora->nome . ' - ' . $areasComercializaco->operadora->detalhe, ['controller' => 'Operadoras', 'action' => 'view', $areasComercializaco->operadora->id]) : '' ?></td>
                    <td><?= h($areasComercializaco->descricao) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Edit'), '/areasComercializacoes/edit/' . $areasComercializaco->id, ['sid' => 'areasComercializacoes.edit']) ?>
                        <?= $this->Form->postLink(__('Duplicar'), '/areasComercializacoes/duplicar/' . $areasComercializaco->id, ['sid' => 'areasComercializacoes.duplicar', 'confirm' => __('Tem certeza que quer duplicar a area de comercialização #{0}?', $areasComercializaco->nome)]) ?>
                        <?= $this->Form->postLink(__('Delete'), '/areasComercializacoes/delete/' . $areasComercializaco->id, ['sid' => 'areasComercializacoes.delete', 'confirm' => __('Are you sure you want to delete # {0}?', $areasComercializaco->id)]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>