<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Areas Comercializaco'), ['action' => 'edit', $areasComercializaco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Areas Comercializaco'), ['action' => 'delete', $areasComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $areasComercializaco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Areas Comercializacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Areas Comercializaco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Operadoras'), ['controller' => 'Operadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Operadora'), ['controller' => 'Operadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="areasComercializacoes view large-9 medium-8 columns content">
    <h3><?= h($areasComercializaco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($areasComercializaco->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($areasComercializaco->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Operadora') ?></th>
            <td><?= $areasComercializaco->has('operadora') ? $this->Html->link($areasComercializaco->operadora->id, ['controller' => 'Operadoras', 'action' => 'view', $areasComercializaco->operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($areasComercializaco->id) ?></td>
        </tr>
    </table>
</div>
