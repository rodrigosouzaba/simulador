<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $arquivo->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $arquivo->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Arquivos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Produtos Sugeridos'), ['controller' => 'ProdutosSugeridos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Produtos Sugerido'), ['controller' => 'ProdutosSugeridos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="arquivos form large-9 medium-8 columns content">
    <?= $this->Form->create($arquivo) ?>
    <fieldset>
        <legend><?= __('Edit Arquivo') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('caminho');
            echo $this->Form->input('produtos_sugeridos._ids', ['options' => $produtosSugeridos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
