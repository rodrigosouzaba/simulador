<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Arquivo'), ['action' => 'edit', $arquivo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Arquivo'), ['action' => 'delete', $arquivo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $arquivo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Arquivos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Arquivo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Produtos Sugeridos'), ['controller' => 'ProdutosSugeridos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Produtos Sugerido'), ['controller' => 'ProdutosSugeridos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="arquivos view large-9 medium-8 columns content">
    <h3><?= h($arquivo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($arquivo->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Caminho') ?></th>
            <td><?= h($arquivo->caminho) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($arquivo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($arquivo->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($arquivo->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Produtos Sugeridos') ?></h4>
        <?php if (!empty($arquivo->produtos_sugeridos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Operadora') ?></th>
                <th><?= __('Comercializacao') ?></th>
                <th><?= __('Area Atendimento') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($arquivo->produtos_sugeridos as $produtosSugeridos): ?>
            <tr>
                <td><?= h($produtosSugeridos->id) ?></td>
                <td><?= h($produtosSugeridos->nome) ?></td>
                <td><?= h($produtosSugeridos->estado_id) ?></td>
                <td><?= h($produtosSugeridos->operadora) ?></td>
                <td><?= h($produtosSugeridos->comercializacao) ?></td>
                <td><?= h($produtosSugeridos->area_atendimento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ProdutosSugeridos', 'action' => 'view', $produtosSugeridos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ProdutosSugeridos', 'action' => 'edit', $produtosSugeridos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ProdutosSugeridos', 'action' => 'delete', $produtosSugeridos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $produtosSugeridos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
