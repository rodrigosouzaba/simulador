<style type="text/css">
    .input-radio+label {
        margin-left: 0px !important;
        margin-right: 0px !important;
    }

    #responseAvaliacao {
        margin: 10px;
        margin-top: 20px:
    }

    #salvar {
        margin: 10px;
    }
</style>
<div class="container col-md-12">
    <?= $this->Form->create($avaliacao, ['id' => 'formAvaliacao']) ?>
    <div class="col-md-12">
        <?= $this->Form->hidden('user_id', ['value' => $sessao['id']]); ?>
    </div>
    <div class=" fonteReduzida col-md-12 centralizada">
        <?= $this->Html->tag('i', '', ['class' => 'icones far fa-meh', 'id' => 'meh', 'nota' => 1, 'cor' => '#a94442 !important']); ?>
        <?= $this->Html->tag('i', '', ['class' => 'icones far fa-smile', 'id' => 'smile', 'nota' => 2, 'cor' => '#8a6d3b !important']); ?>
        <?= $this->Html->tag('i', '', ['class' => 'icones far fa-grin-stars', 'id' => 'star', 'nota' => 3, 'cor' => '#3c763d !important']); ?>
        <?= $this->Html->tag('i', '', ['class' => 'icones far fa-grin-tongue-squint', 'id' => 'tongue', 'nota' => 4, 'cor' => '#337ab7 !important']); ?>
        <?= $this->Form->input('nota', ['class' => 'form-control', 'type' => 'hidden']) ?>
    </div>
    <div class="opicional">
        <div class="col-xs-12">
            <hr size="7">
        </div>
        <div id="indicacoes" class="centralizada">

        </div>
        <div class="col-md-12 centralizada">
            <h4>Deseja enviar um comentário?</h4>
        </div>
        <div class="col-md-8 col-md-offset-2 fonteReduzida centralizada">
            <div class="col-md-3  col-xs-3">
                <input class="input-radio" type="radio" name="tipo" id="critica" value="CRITICA">
                <label class="label-radio" for="critica">Crítica</label>
            </div>
            <div class="col-md-3  col-xs-3">
                <input class="input-radio" type="radio" name="tipo" id="elogio" value="ELOGIO">
                <label class="label-radio" for="Elogio">Elogio</label>
            </div>
            <div class="col-md-3  col-xs-3">
                <input class="input-radio" type="radio" name="tipo" id="erro" value="ERRO">
                <label class="label-radio" for="erro">Erro</label>
            </div>
            <div class="col-md-3  col-xs-3">
                <input class="input-radio" type="radio" name="tipo" id="sugestao" value="SUGESTAO">
                <label class="label-radio" for="erro" style="margin: 0px 0px;">Sugestão</label>
            </div>
        </div>
        <div class="col-md-12 fonteReduzida centralizada">
            <div class="spacer-lg"></div>
            <?= $this->Form->input('comentario', ['label' => false, 'placeholder' => 'Digite aqui seu comentário...', 'type' => 'textarea', 'class' => 'centralizada']); ?>
        </div>
        <div class="col-xs-12">
            <div class="text-danger centralizada" id="responseAvaliacao"></div>
        </div>
        <div class="col-md-12 fonteReduzida centralizada">
            <?= $this->Form->button('Enviar', ['class' => 'btn btn-md btn-primary pt-2', 'id' => 'salvar']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({
            type: 'GET',
            url: '<?php echo $this->request->webroot ?>indicados/add',
            success: function(data) {
                $("#indicacoes").empty();
                $("#indicacoes").append(data);
            }
        });
        // para testes        $("#cm_star-5").click();
    });
    $('#salvar').click(function(event) {
        if ($('#celular').val() != '' || $('#email').val() != '') {
            if ($("#nome").val() == '') {
                event.preventDefault();
                $("#responseAvaliacao").append("Por favor preencha o Nome!");
            }

        } else {
            $.ajax({
                type: "POST",
                data: $("#formIndicacao").serialize(),
                url: "<?php echo $this->request->webroot ?>/indicados/add",
            });
        }

    });

    //Ao comentar solicita o tipo do comentario
    $("#comentario").on('input', function() {
        if ($("#comentario").val().length > 0) {
            $("#critica").attr('required', 'required');
        } else {
            $("#critica").removeAttr('required');
        }
    });

    //Ao dar tipo comentar algo do tipo.
    $("input[name=tipo]").click(function() {
        $("#comentario").attr('required', 'required');
    });
    $(".icones").click(function() {
        $("#nota").val($(this).attr("nota"));
        $(".icones").css('color', '#777');
        $(this).css('color', $(this).attr("cor"));
    });
</script>
