<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $avaliacao->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $avaliacao->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Avaliacaos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="avaliacaos form large-9 medium-8 columns content">
    <?= $this->Form->create($avaliacao) ?>
    <fieldset>
        <legend><?= __('Edit Avaliacao') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('tipo');
            echo $this->Form->input('nota');
            echo $this->Form->input('comentario');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
