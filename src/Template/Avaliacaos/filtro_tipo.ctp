<?php foreach ($avaliacaos as $avaliacao): ?>
    <tr>
        <td><?= $avaliacao->user['nome']." ".$avaliacao->user['sobrenome'] ?></td>
        <td><?= $avaliacao->tipo ?></td>
        <td>
            <?php for($i = 1; $i <= $avaliacao->nota; $i++): ?>
                <label class="fas fa-star" style="width: 10%;  float: left;"></label>
            <?php endfor; ?>
        </td>
        <td><?= $avaliacao->comentario ?></td>
    </tr>
    <?php endforeach; ?>