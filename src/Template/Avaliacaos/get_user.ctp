<div class="">
    <table class="table table-condensed" style="margin-bottom: 0 !important;">
        <?= $this->Form->input('id', ['value' => $user['id'], 'type' => 'hidden']) ?>
        <tr>
            <td><i class="fa fa-user"></i> <?= __('Usuário') ?></td>
            <td colspan="2"><?= $this->Form->input(null, ['value' => $user['username'], 'class' => 'input-disabled', 'disabled' => true]) ?></td>
        </tr>

        <tr>
            <td class="first-td">
                <span>
                    <i class="fa fa-user"></i> <?= __('Nome') ?>
                </span>
            </td>
            <td>
                <?= $this->Form->input('nome', ['label' => false, 'value' => $user['nome'], 'class' => 'input-disabled', 'disabled' => true]) ?>
            </td>
            <td>
                <?= $this->Form->input('sobrenome', ['label' => false, 'value' => $user['sobrenome'], 'class' => 'input-disabled', 'disabled' => true]) ?>
            </td>
        </tr>
        <tr>
            <td class="first-td">
                <span>
                    <i class="fa fa-birthday-cake"></i> <?= __('Data de Nascimento') ?>
                </span>
            </td>
            <td colspan="2"><?= $this->Form->input('data_nascimento', ['label' => false, 'value' => $user['data_nascimento'], 'class' => 'input-disabled',  'disabled' => true]) ?></td>
        </tr>
        <tr>
            <td class="first-td">
                <span>
                    <i class="fa fa-map-marker"></i> Estado de Atuação
                </span>
            </td>
            <td id="field-estados"><?= $this->Form->input('Estados', ['value' => $user['estado']['nome'], 'id' => 'estado_id', 'label' => false, 'class' => 'input-disabled',  'disabled' => true]) ?></td>
            <td id="field-municipios"><?= $this->Form->input('Municipios', ['value' => $user['municipio']['nome'], 'label' => false, 'class' => 'input-disabled',  'disabled' => true]) ?></td>
        </tr>
        <tr>
            <td class="first-td">
                <span>
                    <i class="fa fa-envelope"></i> <?= __('E-mail') ?>
                </span>
            </td>
            <td colspan="2"><?= $this->Form->input('email', ['label' => false, 'value' => $user['email'], 'class' => 'input-disabled', 'disabled' => true]) ?></td>
        </tr>
        <tr>
            <td class="first-td">
                <span>
                    <i class="fa fa-phone"></i> <?= __('Celular') . " - SMS" ?>
                </span>
            </td>
            <td colspan="2"><?= $this->Form->input('celular', ['label' => false, 'value' => $user['celular'], 'class' => 'input-disabled',  'disabled' => true]) ?></td>
        </tr>
        <tr>
            <td class="first-td">
                <span>
                    <i class="fa fa-whatsapp"></i> Whatsapp
                </span>
            </td>
            <td colspan="2"><?= isset($user['whatsapp']) ? $this->Form->input('whatsapp', ['label' => false, 'value' => $user['whatsapp'], 'class' => 'input-disabled',  'disabled' => true]) : '<small>Não informado</small>'; ?></td>
        </tr>
        <tr>
            <td class="first-td">
                <span>
                    <i class="fa fa-desktop"></i> Site
                </span>
            </td>
            <td colspan="2"><?= isset($user['site']) ? $this->Form->input('site', ['label' => false, 'value' => $user['site'], 'class' => 'input-disabled',  'disabled' => true]) : '<small>Não informado</small>'; ?></td>
        </tr>
        <tr>
            <td class="first-td">
                <span>
                    <i class="fa fa-facebook-official"></i> Facebook (Perfil ou FanPage)
                </span>
            </td>
            <td colspan="2"><?= isset($user['facebook']) ? $this->Form->input('facebook', ['label' => false, 'value' => $user['facebook'], 'class' => 'input-disabled',  'disabled' => true]) : '<small>Não informado</small>'; ?></td>
        </tr>
    </table>
</div>
<script>
    $(".btn-editor").click(function() {
        $(".input-disabled").each(function() {
            $(this).removeAttr('disabled');
            $(this).addClass('input-enabled');
            $(this).removeClass('input-disabled');
        });
        $(this).attr('disabled', true);
        $(".buttons-edit").show();
    });

    $("#cancel-edition").click(function() {
        $(".input-enabled").each(function() {
            $(this).attr('disabled', true);
            $(this).addClass('input-disabled');
            $(this).removeClass('input-enabled');
        });
        $(".btn-editor").removeAttr('disabled');
        $(".buttons-edit").hide();
    });
    $("#save-edition").click(function() {
        $.post('<?= $this->request->webroot ?>users/edit/<?= $user['id'] ?>', function() {
            $("#modal-user").modal('hide');
        })
    })
</script>