<div class="modulo">Avaliações e Solicitações de Contato</div>
<div id="ajax">
    <div class="col-md-3">
    </div>
    <div class="col-md-3 centralizada">
        <?= $this->Form->input('tipo', ['empty' => 'Filtrar por comentário', 'options' => ['CRITICA' => 'Crítica', 'ELOGIO' => 'Elogio', 'ERRO' => 'Erro', 'SUGESTAO' => 'Sugestão'], 'label' => false, 'class' => 'centralizada fonteReduzida', 'style' => 'margin-top: 10px']); ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th class="tituloTabela"><?= $this->Paginator->sort('created', ['label' => 'Data']) ?></th>
                <th class="tituloTabela"><?= $this->Paginator->sort('user_id', ['label' => 'Usuário']) ?></th>
                <th class="tituloTabela"><?= $this->Paginator->sort('nota') ?></th>
                <th class="tituloTabela"><?= $this->Paginator->sort('tipo') ?></th>
                <th class="tituloTabela"><?= $this->Paginator->sort('comentarios', ['label' => 'Comentários']) ?></th>
                <th class="tituloTabela"></th>
            </tr>
        </thead>
        <tbody id="tabela">
            <?php foreach ($avaliacaos as $avaliacao) : ?>
                <tr>
                    <td><?= $avaliacao->created ?></td>
                    <td><?= $this->Html->link($avaliacao->user['nome'] . " " . $avaliacao->user['sobrenome'], '#', ['class' => 'link-user', 'user-id' => $avaliacao->user['id']]) ?></td>
                    <td>
                        <?php
                        $imagem = '';
                        if (isset($avaliacao->nota)) {

                            switch ($avaliacao->nota) {
                                case 1:
                                    $imagem = 'botoes/insatisfeito.png';
                                    break;
                                case 2:
                                    $imagem = 'botoes/razoavel.png';
                                    break;
                                case 3:
                                    $imagem = 'botoes/bom.png';
                                    break;
                                case 4:
                                    $imagem = 'botoes/satisfeito.png';
                                    break;
                                case 5:
                                    $imagem = 'botoes/muito-satisfeito.png';
                                    break;
                            }
                            echo $this->Html->image($imagem, ['style' => ['width: 30px;']]);
                        }
                        ?>
                    </td>
                    <td><?= $avaliacao->tipo ?></td>
                    <td><?= $avaliacao->comentario ?></td>
                    <td>
                        <?= $avaliacao->resposta ? $avaliacao->resposta :
                            "<a href='#modal-resposta' class='link-resposta' resposta-id=$avaliacao->id>Responder</a>"
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('próximo')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('anterior') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-resposta" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content row">
            <div class="modal-header" style="border: 0">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title centralizada">Responder</h4>
            </div>
            <?= $this->Form->create($avaliacao, ['id' => 'form-resposta']) ?>
            <div class="modal-body col-xs-12">
                <?= $this->Form->input('resposta', ['type' => 'textarea', 'label' => false]) ?>
            </div>
            <div class="modal-footer" style="border: 0">
                <?= $this->Form->button('Enviar', ['class' => 'btn btn-primary col-xs-4 col-xs-offset-4']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
</div>
<!--Fim do Modal resposta-->
<!-- Modal -->
<div class="modal fade" id="modal-user-avaliacao" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content row">
            <div class="modal-header" style="border: 0">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title centralizada" id="modal-title">Dados do Usuário</h4>
            </div>
            <div class="modal-body col-xs-12" id="data-user">

            </div>
            <div class="modal-footer" style="border: 0">
            </div>
            <?= $this->Form->end() ?>
        </div>

    </div>
</div>
<!--Fim do Modal resposta-->
<script type="text/javascript">
    $("#tipo").change(function() {
        $("#loader").click();
        $.ajax({
            url: "<?php echo $this->request->webroot ?>avaliacaos/filtroTipo/" + $("#tipo").val(),
            type: "POST",
            data: $(this),
            success: function(data) {
                $("#ajax").empty();
                $("#ajax").append(data);
                $("#fechar").click();
            }
        });
    });
    $(".link-resposta").click(function() {
        var id = $(this).attr('resposta-id');
        $("#form-resposta").attr('action', '/avaliacaos/responder/' + id);
        $("#modal-resposta").modal('show');
    });
    $(".link-user").click(function() {
        let user = $(this).attr('user-id');
        $.get(baseUrl + 'avaliacaos/get-user/' + user, function(data) {
            $("#data-user").empty();
            $("#data-user").append(data);
            $("#modal-user-avaliacao").modal("show");
        });
    });
</script>