<div class="col-md-12">
    <h3><?= $avaliacao->id ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $avaliacao->has('user') ? $this->Html->link($avaliacao->user->id, ['controller' => 'Users', 'action' => 'view', $avaliacao->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo') ?></th>
            <td><?= h($avaliacao->tipo) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($avaliacao->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Nota') ?></th>
            <td><?= $this->Number->format($avaliacao->nota) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($avaliacao->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Comentario') ?></h4>
        <?= $this->Text->autoParagraph(h($avaliacao->comentario)); ?>
    </div>
</div>
