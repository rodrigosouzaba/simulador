<div class="col-xs-12">
    <?= $this->Form->create($bannerImagen,  ['enctype' => 'multipart/form-data', 'id' => 'form-add-imagem']) ?>
    <fieldset>
        <h3>Novo Banner</h3>
        <div class="col-xs-12">
            <?= $this->Form->input('banner_id', ['options' => $banners]); ?>
        </div>
        <div class="clearfix">&nbsp </div>
        <div class="col-xs-12">
            <?= $this->Form->input('imagem', ['class' => 'btn btn-default centralizada', 'label' => false, 'placeholder' => 'nome', 'type' => 'file', 'accept' => "image/*"]); ?>
        </div>
        <div class="col-xs-12">
            <?= $this->Form->input('descricao', ['type' => 'textarea']); ?>
        </div>
    </fieldset>
    <div class="clearfix">&nbsp </div>
    <div class="col-xs-12 centralizada">
        <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>