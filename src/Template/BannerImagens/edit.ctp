<div class="col-xs-12">
    <?= $this->Form->create($bannerImagen) ?>
    <fieldset>
        <?php
        echo $this->Form->input('banner_id', ['options' => $banners, 'label' => 'Local']);
        echo $this->Form->input('nome', ['type' => 'hidden']);
        echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <div class="col-xs-12 centralizada">
        <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>