<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Banner Imagen'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bannerImagens index large-9 medium-8 columns content">
    <h3><?= __('Banner Imagens') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('banner_id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('descricao') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bannerImagens as $bannerImagen): ?>
            <tr>
                <td><?= $this->Number->format($bannerImagen->id) ?></td>
                <td><?= $this->Number->format($bannerImagen->banner_id) ?></td>
                <td><?= h($bannerImagen->nome) ?></td>
                <td><?= h($bannerImagen->descricao) ?></td>
                <td><?= h($bannerImagen->created) ?></td>
                <td><?= h($bannerImagen->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $bannerImagen->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bannerImagen->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bannerImagen->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bannerImagen->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
