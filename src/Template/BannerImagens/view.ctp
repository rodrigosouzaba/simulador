<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Banner Imagen'), ['action' => 'edit', $bannerImagen->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Banner Imagen'), ['action' => 'delete', $bannerImagen->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bannerImagen->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Banner Imagens'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Banner Imagen'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bannerImagens view large-9 medium-8 columns content">
    <h3><?= h($bannerImagen->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($bannerImagen->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($bannerImagen->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($bannerImagen->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Banner Id') ?></th>
            <td><?= $this->Number->format($bannerImagen->banner_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($bannerImagen->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($bannerImagen->modified) ?></td>
        </tr>
    </table>
</div>
