<div class="col-xs-12 col-md-4 col-md-offset-4">
    <?= $this->Form->create($banner) ?>
    <fieldset>
        <legend>
            <h3>Adicionar novo Local</h3>
        </legend>
        <?php
        echo $this->Form->input('local');
        echo $this->Form->input('icone');
        ?>
    </fieldset>
    <div class="centralizada col-xs-12">
        <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>