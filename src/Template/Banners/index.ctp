<style>
    .panel-heading {
        background-color: #003656 !important;
    }

    .panel-title {
        font-size: 13px !important;
        margin-bottom: 0 !important;
        color: #FFF !important;
        text-align: center;
    }

    .botoesCentral {
        padding: 15px !important;
    }

    .botoesCentral:hover {
        background-color: #e6e6e6 !important;
    }
</style>
<div class="col-xs-12">
    <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title" style="">BANNERS</h2>
            </div>
            <div class="panel-body">
                <?php foreach ($banners as $banner) : ?>
                    <?php if ($banner->tipo == "banner") : ?>
                        <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                            <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Simule <b>Planos de Saúde</b> para seus clientes em segundos" class="text-danger">
                                <?= $this->Html->link($this->Html->image("banners/icones/" . $banner->icone, ['style' => 'color: #003e58 !important', 'style' => 'max-height: 72px']) . '<br/><p class="botaoCentralInterna" >' . $banner->local . '<br/>' . $banner->posicao . '</p>', ['action' => 'view/' . $banner->id], ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral', 'role' => 'button', 'escape' => false]); ?>
                            </span>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title" style="">MODAIS</h2>
            </div>
            <div class="panel-body">
                <?php foreach ($banners as $banner) : ?>
                    <?php if ($banner->tipo == "modal") : ?>
                        <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                            <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Simule <b>Planos de Saúde</b> para seus clientes em segundos" class="text-danger">
                                <?= $this->Html->link($this->Html->image("banners/icones/" . $banner->icone, ['style' => 'color: #003e58 !important', 'style' => 'max-height: 72px']) . '<br/><p class="botaoCentralInterna" >'  . $banner->local . '<br/>' . $banner->posicao . '</p>', ['action' => 'view/' . $banner->id], ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral', 'role' => 'button', 'escape' => false]); ?>
                            </span>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>