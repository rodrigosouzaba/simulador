<style>
    .modulo {
        margin: 15px;
    }

    .imagem {
        width: 100%;
        height: 200px;
        border: 1px #ccc solid;
    }

    .lixeira {
        font-size: 22pt;
    }

    .grupo-imagem {
        margin-bottom: 20px !important;
        margin: 0px;
    }

    #btn-novo-banner {
        width: 100%;
        height: 100px;
    }

    .moldura {
        padding: 0px;
    }

    .descricao {
        word-wrap: break-word;
        background-color: #ddd;
    }

    .descricao span {
        font-size: 10pt;
    }

    .actions {
        justify-content: center;
        display: flex;
        align-items: center;
        height: inherit;
    }

    .actions a {
        margin: 5px;
        padding: 0px 4px;
    }

    #btn-novo-banner {
        width: 100%;
        height: 200px;
        background-color: #ccc;
        justify-content: center;
        align-items: center;
        padding-top: 10%;
    }

    .mais {
        background-color: white;
        padding: 5px 10px;
        font-size: 28pt;
        border-radius: 50%;
    }

    #text-add {
        font-size: 14pt;
    }

    #btn-novo-banner:hover {
        box-shadow: inset 0px 0px 10px -5px;
        color: black;
    }

    .input label {
        text-align: initial;
    }

    #form-add {
        display: none;
    }
</style>
<div class="col-xs-12">
    <div class="modulo col-xs-12">IMAGENS - <?= $banner->local ?></div>
    <div class="clearfix">&nbsp</div>
    <div class="grupo-imagem col-xs-12 col-md-3  centralizada">
        <div class="moldura col-xs-12" id="novo-banner">
            <?= $this->Form->create("form-add",  ['enctype' => 'multipart/form-data', 'id' => 'form-add', 'url' => ['controller' => 'BannerImagens', 'action' => 'add']]) ?>
            <fieldset>
                <div class="col-xs-12">
                    <?= $this->Form->input('banner_id', ['type' => 'hidden', 'value' => $banner->id]); ?>
                </div>
                <div class="clearfix">&nbsp </div>
                <div class="col-xs-12">
                    <?= $this->Form->input('imagem', ['class' => 'btn btn-default centralizada', 'label' => 'Selecione a Imagem', 'placeholder' => 'nome', 'type' => 'file', 'accept' => "image/*", 'required' => 'required']); ?>
                </div>
                <div class="col-xs-12">
                    <?= $this->Form->input('descricao', ['required' => 'required']); ?>
                </div>
            </fieldset>
            <div class="col-xs-12 centralizada">
                <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary col-md-4 col-md-offset-1']) ?>
                <?= $this->Form->button('Cancelar', ['class' => 'btn btn-default col-md-4 col-md-offset-2', 'id' => 'cancelar-add']) ?>
            </div>
            <?= $this->Form->end() ?>
            <button id="btn-novo-banner">
                <p style="width: 100%">
                    <span id="text-add"><i class="fa fa-plus mais"></i> Adicionar <br> Imagem ou Vídeo</span>
                    <br>
                </p>
                <p class="text-danger" style="width: 100%"><strong>Dimensão 800x200</strong></p>
            </button>
        </div>
    </div>
    <?php foreach ($banner->banner_imagens as $imagem) : ?>
        <div class="grupo-imagem col-xs-12 col-md-3  centralizada">
            <div class="moldura col-xs-12">
                <?= $this->Html->image("banners/imagens/" . $imagem->nome, ['class' => 'imagem img-responsive center-block']) ?>
            </div>
            <div class="descricao col-xs-12">
                <div class="col-md-9">
                    <span><?= $imagem->descricao ?></span>
                </div>
                <div class="col-md-3 actions">
                    <?php if ($banner->id == 2) : ?>
                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-desktop']), "#", ['imagem' => $imagem->nome, 'class' => 'btn btn-default btn-modal-teste', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "bottom",  "title" => "Ver Modal", "imagem-id" => $imagem->id]) ?>
                    <?php endif; ?>
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'far fa-edit']), "#", ['class' => 'btn btn-default btn-editar', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "bottom",  "title" => "Editar Banner", "imagem-id" => $imagem->id]) ?>
                    <?= $this->Form->postLink($this->Html->tag('i', '', ['class' => 'fas fa-trash']), ['controller' => 'bannerImagens', 'action' => 'delete', $imagem->id], ['class' => 'btn btn-danger', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "bottom",  "title" => "Excluir Banner"]) ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<div class="modal fade" id="modal-imagem" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border: 0px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titulo-imagem">Alterar Imagem</h4>
            </div>
            <div class="modal-body" id="corpo-imagem">

            </div>
            <div class="modal-footer" style="border: 0px;">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-teste" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <img id="imagem-teste" src="" />
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $(".btn-modal-teste").click(function() {
        $("#modal-teste").modal('show');
        let imagem = "/img/banners/imagens/" + $(this).attr('imagem')
        $("#imagem-teste").attr('src', '');
        $("#imagem-teste").attr('src', imagem);
        console.log(imagem);
    });
    $("#btn-novo-banner").click(function() {
        $("#btn-novo-banner").hide();
        $("#form-add").show();
    });

    $("#cancelar-add").click(function(evt) {
        evt.preventDefault();
        $("#form-add").hide();
        $("#btn-novo-banner").show();
    });
    $(".btn-editar").click(function() {
        let imagem = $(this).attr("imagem-id");
        $.ajax({
            type: "GET",
            url: "<?= $this->request->webroot ?>bannerImagens/edit/" + imagem,
            success: function(data) {
                $("#titulo-imagem").empty();
                $("#titulo-imagem").append("Alterar Imagem");
                $("#corpo-imagem").empty();
                $("#corpo-imagem").html(data);
                $("#modal-imagem").modal("show");
            }
        });
    });
</script>
