<div class="col-xs-12">

    <?= $this->Form->create($uploadData, ['type' => 'file']) ?>

    <div class="col-xs-6">
        <?php
        $tipos = array(1 => 'Divulgação', 2 => 'Datas Comemorativas');
        ?>
        <?= $this->Form->input('nome', ['label' => 'Nome da Campanha']); ?>
    </div>
    <div class="col-xs-6">
        <?= $this->Form->input('tipo_campanha', ['options' => $tipos, 'empty' => 'SELECIONE']); ?>
    </div>
    <div class="col-xs-12">
        <?= $this->Form->input('descricao', ['label' => 'Descrição']); ?>
    </div>
    <div class="col-xs-12 upload-frm">
        <?= $this->Form->input('url', ['type' => 'file', 'label' => '', 'class' => 'centralizada']); ?>
    </div>

    <div class="col-xs-12 upload-frm centralizada">
        <?= $this->Form->button(__('Enviar'), ['class' => 'btn btn-default']) ?>
        <?= $this->Form->end() ?>
    </div>

</div>
<!--
<div class="col-md-6 col-md-offset-3 centralizada jumbotron">
    
<?= $this->Flash->render() ?>
    <h4>Selecione o arquivo desejado e clique no botão enviar.</h4>
    <h6 style="line-height: 20px;">Arquivos personalizados de logotipos devem estar nos formatos <b>GIF, PNG, JPG ou JPEG. </b> <br/>
        Para que seus cálculos gerados em PDFs sejam mais leves o tamanho indicado dos arquivos devem ser de no <b>máximo 1MB.</b></h6>
<?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>
<?php echo $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id]); ?>
        <div class="upload-frm">
<?php echo $this->Form->input('file', ['type' => 'file', 'label' => '', 'class' => 'centralizada']); ?>
        </div>
        <div class="upload-frm">
<?php echo $this->Form->button(__('Enviar'), ['type' => 'submit', 'class' => 'btn btn-md btn-primary']); ?>
        </div>
<?php echo $this->Form->end(); ?>
  
</div>-->