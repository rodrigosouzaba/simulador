<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $campanha->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $campanha->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Campanhas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="campanhas form large-9 medium-8 columns content">
    <?= $this->Form->create($campanha) ?>
    <fieldset>
        <legend><?= __('Edit Campanha') ?></legend>
        <?php
            echo $this->Form->input('tipo_campanha');
            echo $this->Form->input('nome');
            echo $this->Form->input('url');
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
