<div class="col-xs-12">
    <?php
//    debug($sessao);
    if($sessao['role'] === 'admin'){?>
    <div class="col-xs-12">
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-picture-o', 'aria-hidden' => 'true']) . '  Cadastrar Campanha', ['action' => 'add'], ['class' => 'btn btn-sm btn-default right', 'role' => 'button', 'escape' => false]);
        ?>
    </div>
    <?php }?>
    <?php foreach ($campanhas as $campanha): ?>
        <div class="col-xs-12 col-md-2">
            <div class="centralizada">
                <?=
                $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-picture-o', 'aria-hidden' => 'true']) . '  Gerar Campanha', ['action' => 'gerarcampanha', $campanha['id']], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
                ?>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="centralizada">
                <a href="/simulador/campanhas/gerarcampanha/<?= $campanha['id'] ?>">
                    <img src="<?= $this->request->webroot . $campanha['url'] ?>" class="list-camapanha" />    
                </a>
            </div>
        </div>
    <?php endforeach; ?>
</div>


