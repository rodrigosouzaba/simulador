<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Campanha'), ['action' => 'edit', $campanha->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Campanha'), ['action' => 'delete', $campanha->id], ['confirm' => __('Are you sure you want to delete # {0}?', $campanha->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Campanhas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Campanha'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="campanhas view large-9 medium-8 columns content">
    <h3><?= h($campanha->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($campanha->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Url') ?></th>
            <td><?= h($campanha->url) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($campanha->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo Campanha') ?></th>
            <td><?= $this->Number->format($campanha->tipo_campanha) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($campanha->descricao)); ?>
    </div>
</div>
