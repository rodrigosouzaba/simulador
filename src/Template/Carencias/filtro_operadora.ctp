<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($carencias as $carencia) : ?>
            <tr>
                <td><?= $carencia->nome ?></td>
                <td><?= $carencia->descricao ?></td>
                <td>
                    <?php
                    if (isset($carencia->operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $carencia->operadora->imagen->caminho . "/" . $carencia->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $carencia->operadora->detalhe;
                    } else {
                        echo $carencia->operadora->nome . " - " . $carencia->operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $carencia->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Carência', 'sid' => 'carencias.edit']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $carencia->id], ['confirm' => __('Confirma exclusão da Carência?', $carencia->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'carencias.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
