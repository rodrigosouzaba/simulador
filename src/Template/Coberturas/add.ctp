<div class="col-xs-12">
    <?= $this->Form->create($cobertura) ?>
    <fieldset>
        <legend><?= __('Add Cobertura') ?></legend>
        <?php
        echo $this->Form->input('nome');
        echo $this->Form->input('prioridade');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
