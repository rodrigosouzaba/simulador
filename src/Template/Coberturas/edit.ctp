<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cobertura->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cobertura->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Coberturas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="coberturas form large-9 medium-8 columns content">
    <?= $this->Form->create($cobertura) ?>
    <fieldset>
        <legend><?= __('Edit Cobertura') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('prioridade');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
