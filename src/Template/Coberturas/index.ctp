<div class="col-md-3 fonteReduzida">
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Cobertura', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'coberturas.add']); ?>
</div>
<div class="col-xs-12">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('prioridade') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($coberturas as $cobertura) : ?>
                <tr>
                    <td><?= $this->Number->format($cobertura->id) ?></td>
                    <td><?= h($cobertura->nome) ?></td>
                    <td><?= $this->Number->format($cobertura->prioridade) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cobertura->id],['sid' => 'coberturas.add']) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cobertura->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cobertura->id),'sid' => 'coberturas.add']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
