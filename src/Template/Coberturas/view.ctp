<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cobertura'), ['action' => 'edit', $cobertura->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cobertura'), ['action' => 'delete', $cobertura->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cobertura->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Coberturas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cobertura'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="coberturas view large-9 medium-8 columns content">
    <h3><?= h($cobertura->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($cobertura->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($cobertura->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prioridade') ?></th>
            <td><?= $this->Number->format($cobertura->prioridade) ?></td>
        </tr>
    </table>
</div>
