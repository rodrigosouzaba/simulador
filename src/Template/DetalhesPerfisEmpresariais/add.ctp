<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Detalhes Perfis Empresariais'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Perfis Empresariais'), ['controller' => 'PerfisEmpresariais', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Perfis Empresariai'), ['controller' => 'PerfisEmpresariais', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="detalhesPerfisEmpresariais form large-9 medium-8 columns content">
    <?= $this->Form->create($detalhesPerfisEmpresariai) ?>
    <fieldset>
        <legend><?= __('Add Detalhes Perfis Empresariai') ?></legend>
        <?php
            echo $this->Form->input('tipo');
            echo $this->Form->input('sexo');
            echo $this->Form->input('data_nascimento');
            echo $this->Form->input('perfil_empresarial_id', ['options' => $perfisEmpresariais, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
