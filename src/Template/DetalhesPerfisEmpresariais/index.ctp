<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Detalhes Perfis Empresariai'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Perfis Empresariais'), ['controller' => 'PerfisEmpresariais', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Perfis Empresariai'), ['controller' => 'PerfisEmpresariais', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="detalhesPerfisEmpresariais index large-9 medium-8 columns content">
    <h3><?= __('Detalhes Perfis Empresariais') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('tipo') ?></th>
                <th><?= $this->Paginator->sort('sexo') ?></th>
                <th><?= $this->Paginator->sort('data_nascimento') ?></th>
                <th><?= $this->Paginator->sort('perfil_empresarial_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($detalhesPerfisEmpresariais as $detalhesPerfisEmpresariai): ?>
            <tr>
                <td><?= $this->Number->format($detalhesPerfisEmpresariai->id) ?></td>
                <td><?= h($detalhesPerfisEmpresariai->tipo) ?></td>
                <td><?= h($detalhesPerfisEmpresariai->sexo) ?></td>
                <td><?= h($detalhesPerfisEmpresariai->data_nascimento) ?></td>
                <td><?= $detalhesPerfisEmpresariai->has('perfis_empresariai') ? $this->Html->link($detalhesPerfisEmpresariai->perfis_empresariai->id, ['controller' => 'PerfisEmpresariais', 'action' => 'view', $detalhesPerfisEmpresariai->perfis_empresariai->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $detalhesPerfisEmpresariai->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $detalhesPerfisEmpresariai->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $detalhesPerfisEmpresariai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detalhesPerfisEmpresariai->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
