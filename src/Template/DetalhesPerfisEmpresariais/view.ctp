<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Detalhes Perfis Empresariai'), ['action' => 'edit', $detalhesPerfisEmpresariai->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Detalhes Perfis Empresariai'), ['action' => 'delete', $detalhesPerfisEmpresariai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detalhesPerfisEmpresariai->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Detalhes Perfis Empresariais'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Detalhes Perfis Empresariai'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Perfis Empresariais'), ['controller' => 'PerfisEmpresariais', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Perfis Empresariai'), ['controller' => 'PerfisEmpresariais', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="detalhesPerfisEmpresariais view large-9 medium-8 columns content">
    <h3><?= h($detalhesPerfisEmpresariai->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Tipo') ?></th>
            <td><?= h($detalhesPerfisEmpresariai->tipo) ?></td>
        </tr>
        <tr>
            <th><?= __('Sexo') ?></th>
            <td><?= h($detalhesPerfisEmpresariai->sexo) ?></td>
        </tr>
        <tr>
            <th><?= __('Perfis Empresariai') ?></th>
            <td><?= $detalhesPerfisEmpresariai->has('perfis_empresariai') ? $this->Html->link($detalhesPerfisEmpresariai->perfis_empresariai->id, ['controller' => 'PerfisEmpresariais', 'action' => 'view', $detalhesPerfisEmpresariai->perfis_empresariai->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($detalhesPerfisEmpresariai->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Data Nascimento') ?></th>
            <td><?= h($detalhesPerfisEmpresariai->data_nascimento) ?></td>
        </tr>
    </table>
</div>
