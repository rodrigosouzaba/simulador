<!-- <div class="alert alert-danger" onclick="this.classList.add('hidden')"><?= h($message) ?></div> -->
<script>
    $.notify({
        message: "<?= h($message) ?>"
    }, {
        type: 'danger',
        placement: {
            from: "top",
            align: "center"
        }
    });
</script>
