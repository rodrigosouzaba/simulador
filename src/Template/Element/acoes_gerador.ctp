<style>
    .botao-icones {
        padding: 0px;
    }

    .botao-icones:hover {
        background-color: transparent;
    }

    .botao-icones img {
        width: 35px;
    }

    #acoesgerador {
        background-color: transparent;
        border: 0;
        position: fixed;
        top: 170px;
        z-index: 1;
    }

    @media(max-width: 767px) {
        #acoesgerador {
            top: 220px;
        }
    }

    @media(min-width: 768px) and (max-width: 895px) {
        #acoesgerador {
            top: 260px;
        }
    }

    @media(min-width: 896px) and (max-width: 1218px) {
        #acoesgerador {
            top: 190px;
        }
    }
</style>
<div class="col-xs-12 centralizada well well-sm" id="acoesgerador">

    <!-- DOWNLOAD DO PDF -->
    <?= $this->Form->button($this->Html->image('botoes/download.png'), ['class' => 'botao-icones btn btn-default', "id" => "pdfdownload", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "bottom", "title" => "Baixar em PDF"]); ?>

    <!-- PDF NO BROWSER -->
    <?= $this->Form->button($this->Html->image('botoes/pdf.png'), ['class' => 'botao-icones btn btn-default', 'style' => 'display: inline !important; border: none !important;', 'role' => 'button', 'escape' => false, 'formtarget' => '_blank', "id" => "pdfbrowser", "data-toggle" => "tooltip", "data-placement" => "bottom", "title" => "Imprimir"]); ?>

    <!-- PDF VIA EMAIL -->
    <div data-toggle="tooltip" title="Enviar por Email" data-placement="bottom" style="width: max-content; display: inline-block;">
        <?= $this->Html->link(
            $this->Html->image('botoes/mail.png'),
            '#',
            [
                'class' => 'botao-icones btn btn-default', 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, "data-toggle" => "modal", "data-target" => "#modalCompartilhamento", "data-placement" => "bottom", "id" => "btn-email"
            ]
        ); ?>
    </div>

    <!-- PDF VIA WHATSAPP -->
    <div data-toggle="tooltip" data-placement="bottom" title="Enviar por Whatsapp" style="width: max-content; display: inline-block;">
        <?= $this->Html->link($this->Html->image('botoes/whatsapp.png'), "#", ['class' => 'botao-icones btn btn-default', 'style' => 'display: inline !important; border: none !important', 'role' => 'button', 'escape' => false, "id" => "whatsapp", "target" => "_blank"]); ?>
    </div>

    <?php if (!empty($this->request->data["logo_cp"])) {
    ?>
        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-link', 'aria-hidden' => 'true']), "#", ['class' => ' btn btn-lg btn-default', 'style' => 'display: inline !important; border: none !important', 'role' => 'button', 'escape' => false, "id" => "exibir-nome", "data-toggle" => "tooltip", "data-placement" => "bottom", "title" => "Salvar Link da Tabela para o Corretor Parceiro"]); ?>
    <?php
    } ?>
</div>
