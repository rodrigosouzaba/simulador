<style>
    #engrenagem {
        font-size: 50pt;
        margin-left: 42px;
        font-size: 65pt;
        opacity: 0.2;
    }

    #sub-info {
        color: #003956;
        margin-left: 32px;
        text-align: left;
    }
</style>
<!-- Modal -->
<div class="modal fade" id="modalAviso" tabindex="-1" role="dialog" aria-labelledby="modalAvisoLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row" id="conteudoAviso">
                </div>
            </div>
        </div>
    </div>
</div>
