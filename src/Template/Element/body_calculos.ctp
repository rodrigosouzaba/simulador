 <div class="well well-sm col-xs-12" style="padding: 5px; min-height: 75px;margin: 5px 0 !important;" id="dadosCalculo">

     <?php
        $x = 1;
        $vidas = 0;
        for ($x = 1; $x <= 10; $x++) {
            $vidas = $vidas + $simulacao['faixa' . $x];
        }
        //                echo "Total de vidas:<b>" . $vidas . "</b>";
        if (isset($simulacao['nome_titular'])) {
            $titular = "Nome Titular: " . $simulacao['nome_titular'];
            $classe = "col-md-3";
        } else {
            $classe = "col-md-4";
        }
        ?>
     <div class="col-md-4 col-xs-12" style="text-align: left;padding-left: 0 !important;">
         <div class="col-xs-12">
             Cálculo Nº <a href="#" simulacao_id="<?= $simulacao['id'] ?>" id="btnModalEdit"><span><?= $simulacao['id'] . "   " ?> - <u>EDITAR SIMULAÇÃO</u></span></a>
         </div>
         <div class="col-xs-12">
             Total de vidas: <b><?= $vidas ?></b>
         </div>
         <div class="col-xs-12">
             Empresa: <b><?= $simulacao['nome'] ?></b>
         </div>
     </div>

     <div class="col-md-8 col-xs-12" style="text-align: left;">
         <div class="col-xs-12" style="padding-left: 0 !important;">
             <?php
                $i = 0;
                $total = 0;
                for ($i = 1; $i <= 10; $i++) {
                    $total = $total + ($simulacao['faixa' . $i] * $maisbarato['faixa' . $i]);
                    //                        debug($maiscaro);die();
                }
                ?>
             <b class="tabulacao"><?= $this->Number->currency($total, "R$ ") ?></b>Menor Preço <small>
                 (<?= $maisbarato['operadora']['nome'] ?>)</small>
         </div>

         <div class="col-xs-12" style="padding-left: 0 !important;">

             <?php
                $i = 0;
                $total = 0;
                for ($i = 1; $i <= 10; $i++) {
                    $total = $total + ($simulacao['faixa' . $i] * $intermediario['faixa' . $i]);
                    //                        debug($maiscaro);
                }
                ?>
             <b class="tabulacao"><?= $this->Number->currency($total, "R$ ") ?></b>Preço Intermediário <small>
                 (<?= $intermediario['operadora']['nome'] ?>)</small>
         </div>

         <div class="col-xs-12" style="padding-left: 0 !important;">
             <?php
                $i = 0;
                $total = 0;
                for ($i = 1; $i <= 10; $i++) {
                    $total = $total + ($simulacao['faixa' . $i] * $maiscaro['faixa' . $i]);
                }
                ?>
             <b class="tabulacao"><?= $this->Number->currency($total, "R$ ") ?></b><span>Maior Preço <small>
                     (<?= $maiscaro['operadora']['nome'] ?>)</small></span>
         </div>
     </div>
 </div>


 <?php
    if (isset($tabelasOrdenadas)) :
        $idOperadora = null;
        foreach ($tabelasOrdenadas as $chave => $tabelas) {
            $numeroTabelas = 0;
            foreach ($tabelas as $p => $t) {
                $numeroTabelas = $numeroTabelas + count($t);
            }
    ?>

         <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">

             <div class="panel panel-default col-xs-12" style="margin: 5px 0;">

                 <!-- PANEL HEADING -->
                 <div class="panel-heading" role="tab" id="heading<?= $chave ?>" style="min-height: 100px; background-color: #fff !important;">
                     <!-- ABRIR CALCULOS -->
                     <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                         <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $chave ?>" aria-expanded="true" aria-controls="collapse<?= $chave ?>">
                             <i class="fas fa-chevron-down" aria-hidden="true" style="font-size: 110% !important;"></i>
                             <span style="font-size: 14px !important;">Abrir <?= $numeroTabelas ?> cálculo(s)</span>
                         </a>
                     </div>

                     <!-- EXIBIR TODOS NO PDF -->
                     <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                         <label for="checkAll<?= $chave ?>" style="color: #337ab7; display: inline !important">
                             <?= $this->Form->checkbox('published', ['hiddenField' => false, 'id' => 'checkAll' . $chave, 'class' => 'todas', 'name' => 'checkAll' . $chave, 'value' => $chave, 'style' => 'color: #23527c']); ?> <span style="font-size: 14px;">Exibir todos no cálculo</span>
                         </label>
                     </div>

                     <!-- MATERIAL DE VENDAS -->
                     <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                         <?php
                            foreach ($operadoras as $operadora) {
                                if ($operadora['id'] === $chave) {

                                    $linkcp = $operadora['url'];
                            ?>
                                 <a class="controle" href="<?php echo $linkcp ?>" target="_blank" style="font-size: 14px !important;"> Ver Material de Vendas</a>
                         <?php
                                    break;
                                }
                            }
                            ?>
                     </div>

                     <!-- LOGO OPERADORA -->
                     <div class="col-xs-12 col-md-6 centralizada">
                         <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $chave ?>" aria-expanded="true" aria-controls="collapse<?= $chave ?>">
                             <?php
                                foreach ($operadoras as $operadora) {
                                    if ($operadora['id'] == $chave) {
                                        echo $this->Html->image("../" . $operadora->imagen->caminho . "/" . $operadora->imagen->nome, ['class' => 'logoSimulacao']);
                                        echo "<p class='detalheOperadora'>" . $operadora['detalhe'] . "</p>";
                                    }
                                }
                                ?>
                         </a>
                     </div>
                 </div> <!-- /PANEL HEADING -->

                 <div id="collapse<?= $chave ?>" class="panel-collapse collapse col-xs-12" role="tabpanel" aria-labelledby="heading<?= $chave ?>">




                     <table class="table table-condensed">
                         <tr>
                             <td class="beneficiarios" style="width: 4%;">Exibir<br>Cálculo
                             </td>
                             <?php
                                $i = 1;

                                for ($i = 1; $i <= 10; $i++) {

                                    if ($simulacao['faixa' . $i] > 0) {
                                ?>
                                     <td class='beneficiarios negrito centralizadaVertical' style="border: solid 1px #ddd;">
                                         <?php
                                            switch ($i) {
                                                case 1:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                                    break;
                                                case 2:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                                    break;
                                                case 3:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                                    break;
                                                case 4:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                                    break;
                                                case 5:
                                                    echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                                    break;
                                                case 6:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                                    break;
                                                case 7:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                                    break;
                                                case 8:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                                    break;
                                                case 9:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                                    break;
                                                case 10:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 ou +";
                                                    break;
                                                    /*
                                                  case 11:
                                                  echo $simulacao['faixa' . $i] . " vida(s)<br/> 65 à 80";
                                                  break;
                                                  case 12:
                                                  echo $simulacao['faixa' . $i] . " vida(s)<br/> + de 81";
                                                  break;
                                                 */
                                            }
                                            ?>

                                     </td>

                             <?php
                                    }
                                }
                                ?>
                             <td class="beneficiarios negrito">
                                 <?php
                                    $x = 1;
                                    $vidas = 0;
                                    for ($x = 1; $x <= 10; $x++) {
                                        $vidas = $vidas + $simulacao['faixa' . $x];
                                    }
                                    echo "Total:<br>" . $vidas . " Vida(s)";
                                    ?>
                             </td>
                         </tr>
                         <?php
                            $faixas = null;
                            $i = 1;
                            for ($i = 1; $i <= 10; $i++) {
                                if ($simulacao['faixa' . $i] > 0) {
                                    $faixas = $faixas + 1;
                                }
                            }

                            foreach ($tabelas as $produto) {

                                foreach ($produto as $produto) {
                            ?>
                                 <tr>
                                     <td></td>
                                 </tr>
                                 <tr style="background-color: #eee; border: solid 1px #ddd">
                                     <td class="centralizada beneficiarios"><?= $this->Form->input($produto['id'], ['type' => 'checkbox', 'id' => 'imprimir' . $produto['id'], 'value' => $produto['id'], 'class' => 'noMarginBottom ' . $chave, 'title' => 'Selecionar Para Impresão', 'label' => '']); ?></td>
                                     <td class="beneficiariosTransparente centralizadaVertical centralizada" colspan="<?= $faixas + 1 ?>">

                                         <?php
                                            if ($produto['coparticipacao'] === 's') {
                                                $co = "C/COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'];
                                            } else {
                                                $co = "S/COPARTICIPAÇÃO ";
                                            }
                                            ?>
                                         <?php
                                            switch ($produto['tipo_contratacao']) {
                                                case '0':
                                                    $tipo_contratacao = "OPCIONAL";
                                                    break;
                                                case '1':
                                                    $tipo_contratacao = "COMPULSÓRIO";
                                                    break;
                                                case '2':
                                                    $tipo_contratacao = "";
                                                    break;
                                            }

                                            // debug($produto); die();
                                            ?>

                                         <?= "<b>" . $produto['produto']['nome'] . " - " . $produto['descricao'] . " - " . $tipo_contratacao . " " . $co . " - " . $produto["cobertura"]["nome"] . "</b> <br> " . $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas - " . $produto['tipo']['nome'] ?>
                                         <?php
                                            $tamanho = count($produto["tabelas_cnpjs"]);
                                            $i = 1;
                                            $cnpjFinal = "";
                                            foreach ($produto["tabelas_cnpjs"] as $cnpj) {
                                                if ($i < $tamanho) {
                                                    $cnpjFinal = $cnpjFinal . " " . $cnpj["cnpj"]["nome"] . ",";
                                                } else {
                                                    $cnpjFinal = $cnpjFinal . " " . $cnpj["cnpj"]["nome"];
                                                }
                                                $i++;
                                            }
                                            echo "-" . $cnpjFinal;
                                            ?>
                                     </td>
                                 </tr>
                                 <tr style="border: solid 1px #ddd">
                                     <td>&nbsp;</td>
                                     <?php
                                        $i = 1;


                                        for ($i = 1; $i <= 10; $i++) {

                                            if ($simulacao['faixa' . $i] > 0) {
                                        ?>
                                             <td class='beneficiariosTransparente centralizadaVertical'>
                                                 <?php if ($produto['faixa' . $i] == 0) : ?>
                                                     ***
                                                 <?php elseif ($produto['atualizacao'] == 1) : ?>
                                                     <strong>Tabela em Atualização de Preço</strong>
                                                 <?php else : ?>
                                                     <!--$this->Number->currency($HdViewBestellingen->INKBLPRIJS,'EUR', ['locale' => 'it_IT'])-->
                                                     <?= $this->Number->currency($simulacao['faixa' . $i] * $produto['faixa' . $i], "R$ ") ?>
                                                     <br />
                                                     <span style="font-size: 100%;">
                                                         <?= number_format($produto['faixa' . $i], 2, ",", ".") ?> p/ vida
                                                     </span>
                                                 <?php endif ?>
                                             </td>
                                     <?php
                                            }
                                        }
                                        ?>
                                     <td class="beneficiariosTransparente  centralizada centralizadaVertical">
                                         <?php if ($produto['atualizacao'] == 1) : ?>
                                             <strong>Tabela em Atualização de Preço</strong>
                                         <?php else : ?>
                                             <?php
                                                $i = 1;
                                                $totalPorProduto = 0;
                                                for ($i = 1; $i <= 10; $i++) {
                                                    $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * $produto['faixa' . $i]);
                                                }

                                                $operadorascomiof = array(1, 28, 29, 30, 31, 16);
                                                if (in_array($produto['operadora']['id'], $operadorascomiof)) {
                                                ?>
                                                 <?= $this->Number->currency($totalPorProduto) ?>
                                                 <?=
                                                    "<br>
                                                    + IOF: " . $this->Number->currency($totalPorProduto * 0.0238, "R$ ") . "<br>"
                                                        . "<b>" . $this->Number->currency($totalPorProduto * 1.0238, "R$ ") . "</b>"
                                                    ?>
                                             <?php } else { ?>

                                                 <b><?= $this->Number->currency($totalPorProduto) ?></b>
                                             <?php } ?>
                                         <?php endif; ?>
                                     </td>

                                 </tr>
                         <?php
                                }
                            }
                            ?>
                         </tr>


                     </table>
                     <!-- OBS IMPORTANTES -->
                     <div class="nobreak">
                         <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                             OBSERVAÇÕES IMPORTANTES
                         </div>
                         <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                             <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
                             <?php
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        echo ($produto['operadora']['observacoes'] != null) ? nl2br($produto['operadora']['observacoes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                        break;
                                    }
                                    break;
                                }
                                ?>
                         </div>
                     </div>
                     <!-- /OBS IMPORTANTES-->

                     <!-- REDE CREDENCIADA -->
                     <div class="nobreak">
                         <div class="clearfix">&nbsp;</div>
                         <?php
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    if (isset($produto['operadora']['url_rede']) && $produto['operadora']['url_rede'] <> null) {
                                        $linkRedeCompleta = "- Para rede completa " . $this->Html->link('clique aqui', $produto['operadora']['url_rede'], ['target' => "_blank"]) . ".";
                                    } else {
                                        $linkRedeCompleta = "";
                                    }
                                    break;
                                }
                                break;
                            }
                            ?>
                         <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                             REDE REFERENCIADA (Resumo) <span><?= $linkRedeCompleta ?></span>
                         </div>
                         <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                             <?php
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {

                                        echo ($produto['operadora']['redes'] != null) ? nl2br($produto['operadora']['redes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";

                                        break;
                                    }
                                    break;
                                }
                                ?>
                         </div>
                     </div>
                     <!-- /REDE CREDENCIADA -->

                     <!-- DIFERENCIAIS -->
                     <div class="nobreak">
                         <div class="clearfix">&nbsp;</div>
                         <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                             OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                         </div>



                         <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">

                             <?php
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        echo ($produto['operadora']['reembolsos'] != null) ? nl2br($produto['operadora']['reembolsos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                        break;
                                    }
                                    break;
                                }
                                ?>
                         </div>
                     </div>
                     <!-- /DIFERENCIAIS -->

                     <!-- AREA DE COMERCIALIZACAO E ATENDIMENTO -->
                     <div class="nobreak">
                         <div class="clearfix">&nbsp;</div>
                         <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                             ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
                         </div>
                         <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                             <?php
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        echo ($produto['operadora']['regioes'][0] != null) ? "<b>ÁREA DE COMERCIALIZAÇÃO: </b><br>" . nl2br($produto['operadora']['regioes'][0]['descricao']) : "<b>ÁREA DE COMERCIALIZAÇÃO: </b><br/><span class='info'>Consulte Operadora</span>";
                                        break;
                                    }
                                    break;
                                }
                                ?>
                             <br>
                             <br>
                             <b>ÁREA DE ATENDIMENTO </b>
                             <div class="fonteReduzida">
                                 <?php
                                    $redes = null;
                                    foreach ($tabelas as $produto) {
                                        foreach ($produto as $produto) {
                                            $redes[$produto['produto']['descricao']] = $produto['regio']['descricao'] . " / " . $produto['abrangencia']['descricao'];
                                        }
                                    }
                                    ?>

                                 <?php
                                    foreach ($redes as $chave => $valor) {
                                        $arr = explode("/", $valor, 2);
                                        echo "<span><b>" . nl2br($chave) . ":</b> " . $arr[1] . "</span><br/>";
                                    }
                                    ?>
                             </div>
                         </div>
                     </div>
                     <!-- /AREA DE COMERCIALIZACAO E ATENDIMENTO -->

                     <!-- CARENCIAS -->
                     <div class="nobreak">
                         <div class="clearfix">&nbsp;</div>
                         <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                             CARÊNCIAS (Resumo)
                         </div>

                         <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                             <?php
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        echo ($produto['operadora']['carencias'] != null) ? nl2br($produto['operadora']['carencias'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                        break;
                                    }
                                    break;
                                }
                                ?>
                         </div>
                     </div>
                     <!-- /CARENCIAS -->

                     <!-- DEPENDENTES -->
                     <div class="nobreak">
                         <div class="clearfix">&nbsp;</div>
                         <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                             DEPENDENTES
                         </div>
                         <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                             <?php
                                $redes = null;
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {

                                        echo ($produto['operadora']['opcionais'] != null) ? nl2br($produto['operadora']['opcionais'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";

                                        break;
                                    }
                                    break;
                                }
                                ?>
                         </div>
                     </div>
                     <!-- /DEPENDENTES -->

                     <!-- DOCUMENTOS NECESSÁRIOS -->
                     <div class="nobreak">
                         <div class="clearfix">&nbsp;</div>
                         <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                             DOCUMENTOS NECESSÁRIOS
                         </div>
                         <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                             <?php
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        echo ($produto['operadora']['informacoes'] != null) ? nl2br($produto['operadora']['informacoes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                        break;
                                    }
                                    break;
                                }
                                ?>
                         </div>
                     </div>
                     <!-- /DOCUMENTOS NECESSÁRIOS -->

                     <!-- FORMAS DE PAGAMENTOS -->
                     <div class="nobreak">
                         <div class="clearfix">&nbsp;</div>
                         <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                             FORMAS DE PAGAMENTOS
                         </div>
                         <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                             <?php
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        echo ($produto['operadora']['formas_pagamentos'] != null) ? nl2br($produto['operadora']['formas_pagamentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                        break;
                                    }
                                    break;
                                }
                                ?>
                         </div>
                         <div class="clearfix">&nbsp;</div>
                     </div>
                     <!-- /FORMAS DE PAGAMENTOS -->

                 </div>
             </div>
         </div>



     <?php
        }
        ?>

     <div class="centralizada">
         <?php
            foreach ($operadoras as $operadora) {
                $caixa = array(5, 33, 34, 35);
                if (!in_array($operadora['id'], $caixa) && !in_array($operadora['id'], array_keys($tabelasOrdenadas)) && $operadora['estado_id'] === $simulacao['estado_id']) {
                    $vetor[$operadora['nome']] = $operadora;
                }
            }
            if (isset($vetor)) {
                foreach ($vetor as $chave => $valor) {
                    if ($valor["status"] != "OCULTA"  && $valor["status"] != 'INATIVA') {
            ?>
                     <div class="col-xs-12 alert alert-secondary" style="margin: 5px 0; border: 1px solid #ddd;">

                         <div class="col-md-4 col-xs-12 vermais" style="font-size: 12px;text-align: left; padding-left: 50px !important">
                             <b>
                                 <?php if ($chave === 'BRADESCO') { ?>
                                     <span class="text-danger">OPERADORA EM PROCESSO DE ATUALIZAÇÃO</span>

                                 <?php
                                    } else {
                                        switch ($valor["status"]) {
                                            case 'EM ATUALIZAÇÃO':
                                                $msg = "OPERADORA EM PROCESSO DE ATUALIZAÇÃO";
                                                break;
                                            default:
                                                $msg = "SEM OPÇÕES PARA ESTE PERFIL DE CLIENTE";
                                                break;
                                        }
                                    ?>
                                     <span class="text-danger"><?= $msg ?></span>
                                 <?php } ?>
                             </b>
                         </div>

                         <div class="col-md-2 col-xs-12 vermais" style="font-size: 14px; text-align: center; padding-left: 1%;">
                             <a class="controle" href="<?= $valor['url'] ?>" target="_blank"> Ver Material de Vendas</a>
                         </div>

                         <div class="col-md-6 col-xs-12" operadora-id="<?= $valor['id'] ?>">
                             <?= $this->Html->image("../" . $valor['imagen']['caminho'] . $valor['imagen']['nome'], ['class' => 'logoSimulacao']); ?>
                             <div class='detalheOperadora'><?= $valor['detalhe'] ?></div>
                         </div>
                     </div>
         <?php
                    }
                }
            }
            //            debug(array_keys($tabelasOrdenadas));
            ?>
     </div>
     <?= $this->element("link-cotacao"); ?>
     </div>
 <?php else :
    ?>
     <div class="clearfix">&nbsp;</div>
     <div class="alert alert-danger centralizada" role="alert"> Nenhuma tabela encontrada dentro dos Filtros selecionados</div>

 <?php endif; ?>