<div class="well well-sm col-xs-12" style="padding: 5px; min-height: 75px;margin: 5px 0 !important;" id="dadosCalculo">
    <?php
    $x = 1;
    $vidas = 0;
    for ($x = 1; $x <= 12; $x++) {
        $vidas = $vidas + $simulacao['faixa' . $x];
    }
    //                echo "Total de vidas:<b>" . $vidas . "</b>";
    if (isset($simulacao['nome_titular'])) {
        $titular = "Nome Titular: " . $simulacao['nome_titular'];
        $classe = "col-md-3";
    } else {
        $classe = "col-md-4";
    }
    ?>
    <div class="col-md-4 col-xs-12" style="text-align: left;padding-left: 0 !important;">
        <div class="col-xs-12">
            Cálculo Nº <a href="#" id="btnModalEdit" simulacao_id="<?= $simulacao['id'] ?>"><span><?= $simulacao['id'] . "   " ?> - <u>EDITAR SIMULAÇÃO</u></span></a>
        </div>
        <div class="col-xs-12">
            Total de vidas: <b><?= $vidas ?></b>
        </div>
        <div class="col-xs-12">
            Cliente: <b><?= $simulacao['nome'] ?></b>
        </div>
    </div>

    <div class="col-md-8 col-xs-12" style="text-align: left;">
        <div class="col-xs-12" style="padding-left: 0 !important;">
            <?php
            $i = 0;
            $total = 0;
            for ($i = 1; $i <= 12; $i++) {
                $total = $total + ($simulacao['faixa' . $i] * $maisbarato['faixa' . $i]);
                //                        debug($maiscaro);die();
            }
            //             debug($maisbarato);
            ?>
            <b class="tabulacao"><?= $this->Number->currency($total, "R$ ") ?></b>Menor Preço <small>
                (<?= $maisbarato['pf_areas_comercializaco']['pf_operadora']['nome'] ?>)</small>
        </div>

        <div class="col-xs-12" style="padding-left: 0 !important;">

            <?php
            $i = 0;
            $total = 0;
            for ($i = 1; $i <= 12; $i++) {
                $total = $total + ($simulacao['faixa' . $i] * $intermediario['faixa' . $i]);
                //                        debug($maiscaro);
            }
            ?>
            <b class="tabulacao"><?= $this->Number->currency($total, "R$ ") ?></b>Preço Intermediário <small>
                (<?= $intermediario['pf_areas_comercializaco']['pf_operadora']['nome'] ?>)</small>
        </div>

        <div class="col-xs-12" style="padding-left: 0 !important;">
            <?php
            $i = 0;
            $total = 0;
            for ($i = 1; $i <= 12; $i++) {
                $total = $total + ($simulacao['faixa' . $i] * $maiscaro['faixa' . $i]);
            }
            ?>
            <b class="tabulacao"><?= $this->Number->currency($total, "R$ ") ?></b><span>Maior Preço <small>
                    (<?= $maiscaro['pf_areas_comercializaco']['pf_operadora']['nome'] ?>)</small></span>
        </div>
    </div>
</div>

<?php
if (isset($tabelasOrdenadas)) :

    $idOperadora = null;

    foreach ($tabelasOrdenadas as $chave => $tabelas) {
        $numeroTabelas = 0;
        foreach ($tabelas as $p => $t) {
            $numeroTabelas = $numeroTabelas + count($t);
        }
?>

        <div class="panel-group listaCalculos" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default col-xs-12" style="margin: 5px 0;">
                <div class="panel-heading" role="tab" id="heading<?= $chave ?>" style="min-height: 100px; background-color: #fff !important;">
                    <!-- ABRIR CALCULOS -->
                    <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                        <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $chave ?>" aria-expanded="true" aria-controls="collapse<?= $chave ?>">
                            <i class="fas fa-chevron-down" aria-hidden="true" style="font-size: 110% !important;"></i>
                            <span style="font-size: 14px !important;">Abrir <?= $numeroTabelas ?> cálculo(s)</span>
                        </a>
                    </div>

                    <!-- EXIBIR TODOS NO PDF -->
                    <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                        <label for="checkAll<?= $chave ?>" style="color: #337ab7; display: inline !important">
                            <?= $this->Form->checkbox('published', ['hiddenField' => false, 'id' => 'checkAll' . $chave, 'class' => 'todas', 'name' => 'checkAll' . $chave, 'value' => $chave, 'style' => 'color: #23527c']); ?> <span style="font-size: 14px;">Exibir todos no cálculo</span>
                        </label>
                    </div>

                    <!-- MATERIAL DE VENDAS -->
                    <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                        <?php
                        foreach ($operadoras as $operadora) {
                            if ($operadora['id'] === $chave) {

                                $linkcp = $operadora['url'];
                        ?>
                                <a class="controle" href="<?php echo $linkcp ?>" target="_blank" style="font-size: 14px !important;"> Ver Material de Vendas</a>
                        <?php
                                break;
                            }
                        }
                        ?>
                    </div>


                    <!-- LOGO OPERADORA -->
                    <div class="col-xs-12 col-md-6 centralizada">
                        <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $chave ?>" aria-expanded="true" aria-controls="collapse<?= $chave ?>">
                            <?php
                            foreach ($operadoras as $operadora) {
                                if ($operadora['id'] == $chave) {
                                    // debug($operadora);
                                    echo !is_null($operadora->imagen) ? $this->Html->image("../" . $operadora->imagen->caminho . "/" . $operadora->imagen->nome, ['class' => 'logoSimulacao']) : $operadora->nome;
                                    echo "<p class='detalheOperadora'>" . $operadora['detalhe'] . "</p>";
                                }
                            }
                            ?>
                        </a>
                    </div>
                </div> <!-- /PANEL HEADING -->

                <div id="collapse<?= $chave ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $chave ?>">
                    <div class="panel-body" style="border-top: none !important">
                        <table class="table table-condensed">
                            <tr>
                                <td class="beneficiarios" style="width: 4%;">Exibir<br>Cálculo
                                </td>
                                <?php
                                $i = 1;

                                for ($i = 1; $i <= 10; $i++) {

                                    if ($simulacao['faixa' . $i] > 0) {
                                ?>
                                        <td class='beneficiarios negrito centralizadaVertical' style="border: solid 1px #ddd;">
                                            <?php
                                            switch ($i) {
                                                case 1:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                                    break;
                                                case 2:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                                    break;
                                                case 3:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                                    break;
                                                case 4:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                                    break;
                                                case 5:
                                                    echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                                    break;
                                                case 6:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                                    break;
                                                case 7:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                                    break;
                                                case 8:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                                    break;
                                                case 9:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                                    break;
                                                case 10:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 ou +";
                                                    break;
                                            }
                                            ?>

                                        </td>

                                <?php
                                    }
                                }
                                ?>
                                <td class="beneficiarios negrito">
                                    <?php
                                    $x = 1;
                                    $vidas = 0;
                                    for ($x = 1; $x <= 10; $x++) {
                                        $vidas = $vidas + $simulacao['faixa' . $x];
                                    }
                                    echo "Total:<br>" . $vidas . " Vida(s)";
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $faixas = null;
                            $i = 1;
                            for ($i = 1; $i <= 10; $i++) {
                                if ($simulacao['faixa' . $i] > 0) {
                                    $faixas = $faixas + 1;
                                }
                            }

                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                            ?>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr style="background-color: #eee; border: solid 1px #ddd">
                                        <td class="centralizada beneficiarios"><?= $this->Form->input($produto['id'], ['type' => 'checkbox', 'id' => 'imprimir' . $produto['id'], 'value' => $produto['id'], 'class' => 'noMarginBottom ' . $produto['pf_areas_comercializaco']['pf_operadora']['id'], 'title' => 'Selecionar Para Impresão', 'label' => '']); ?></td>
                                        <td class="beneficiariosTransparente centralizadaVertical centralizada" colspan="<?= $faixas + 1 ?>">

                                            <?php
                                            if ($produto['coparticipacao'] == 's') {
                                                $co = "C/COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'];
                                            } else {
                                                $co = "S/COPARTICIPAÇÃO ";
                                            }
                                            ?>

                                            <!-- <b><?= $chave ?> -->
                                            <?= "<b>" .  $produto['pf_areas_comercializaco']['nome'] . " - " . $produto['descricao'] . " - " . $co . " - " . $produto["pf_cobertura"]["nome"] . "</b> <br> " .
                                                $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas - " . $produto['pf_acomodaco']['nome'] ?>
                                        </td>
                                    </tr>
                                    <tr style="border: solid 1px #ddd">
                                        <td>&nbsp;</td>
                                        <?php
                                        $i = 1;


                                        for ($i = 1; $i <= 10; $i++) {

                                            if ($simulacao['faixa' . $i] > 0) {
                                        ?>
                                                <td class='beneficiariosTransparente centralizadaVertical'>
                                                    <?php if ($produto['faixa' . $i] == 0) : ?>
                                                        ***
                                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                                        <strong>Tabela em Atualização de Preço</strong>
                                                    <?php else : ?>
                                                        <!--$this->Number->currency($HdViewBestellingen->INKBLPRIJS,'EUR', ['locale' => 'it_IT'])-->
                                                        <?= $this->Number->currency($simulacao['faixa' . $i] * $produto['faixa' . $i], "R$ ") ?>
                                                        <br />
                                                        <span style="font-size: 100%;">
                                                            <?= number_format($produto['faixa' . $i], 2, ",", ".") ?> p/ vida
                                                        </span>
                                                    <?php endif ?>
                                                </td>
                                        <?php
                                            }
                                        }
                                        ?>
                                        <td class="beneficiariosTransparente  centralizada centralizadaVertical">
                                            <?php if ($produto['atualizacao'] == 1) : ?>
                                                <strong>Tabela em Atualização de Preço</strong>
                                            <?php else : ?>
                                                <?php
                                                $i = 1;
                                                $totalPorProduto = 0;
                                                for ($i = 1; $i <= 10; $i++) {
                                                    $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * $produto['faixa' . $i]);
                                                }

                                                $operadorascomiof = array(1, 28, 29, 30, 31, 16);
                                                if (in_array($produto['operadora']['id'], $operadorascomiof)) {
                                                ?>
                                                    <?= $this->Number->currency($totalPorProduto) ?>
                                                    <?=
                                                    "<br>
                                                            + IOF: " . $this->Number->currency($totalPorProduto * 0.0238, "R$ ") . "<br>"
                                                        . "<b>" . $this->Number->currency($totalPorProduto * 1.0238, "R$ ") . "</b>"
                                                    ?>
                                                <?php } else { ?>

                                                    <b><?= $this->Number->currency($totalPorProduto) ?></b>
                                                <?php } ?>
                                            <?php endif; ?>
                                        </td>

                                    </tr>
                            <?php
                                }
                            }
                            ?>
                            </tr>


                        </table>
                        <?php

                        foreach ($operadoras as $operadora) {
                            if (($operadora['id'] == $chave) && ($produto['modalidade'] <> 'INDIVIDUAL')) {
                        ?>
                                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                    ENTIDADES E PROFISSÕES
                                </div>
                                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc; text-align: justify !important" class="fonteReduzida">
                                    <?php
                                    if (isset($ordenada[$chave])) {
                                        foreach ($ordenada[$chave] as $entidade => $profissoes) {

                                            $indice = 1;
                                            foreach (array_keys($profissoes) as $profissao) {
                                                if ($indice === 1 && $indice) {
                                                    echo "<b>" . $entidade . ": </b>";
                                                }
                                                if ($indice === count($profissoes)) {
                                                    echo $profissao . ". ";
                                                } else {
                                                    echo $profissao . ", ";
                                                }

                                                $indice++;
                                            }
                                        }
                                    }

                                    ?>
                                </div>
                        <?php
                            }
                        }

                        ?>

                        <div class="clearfix">&nbsp;</div>



                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            OBSERVAÇÕES IMPORTANTES
                        </div>

                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
                            <?php
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    echo ($produto['pf_areas_comercializaco']['pf_operadora']['pf_observacoes'] != null) ? nl2br($produto['pf_areas_comercializaco']['pf_operadora']['pf_observacoes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                    break;
                                }
                                break;
                            }
                            ?>
                        </div>


                        <div class="clearfix">&nbsp;</div>
                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            DEPENDENTES
                        </div>

                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    echo ($produto['pf_areas_comercializaco']['pf_operadora']['pf_dependentes'] != null) ? nl2br($produto['pf_areas_comercializaco']['pf_operadora']['pf_dependentes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                    break;
                                }
                                break;
                            }
                            ?>
                        </div>
                        <div class="clearfix">&nbsp;</div>

                        <?php
                        foreach ($tabelas as $produto) {
                            foreach ($produto as $produto) {
                                // debug($produto['operadora']);die();
                                if (isset($produto['pf_areas_comercializaco']['pf_operadora']['url_rede']) && $produto['pf_areas_comercializaco']['pf_operadora']['url_rede'] <> null) {
                                    $linkRedeCompleta = "- Para rede completa " . $this->Html->link('clique aqui', $produto['pf_areas_comercializaco']['pf_operadora']['url_rede'], ['target' => "_blank"]) . ".";
                                } else {
                                    $linkRedeCompleta = "";
                                }
                                break;
                            }
                            break;
                        }
                        ?>
                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            REDE REFERENCIADA (Resumo) <span><?= $linkRedeCompleta ?></span>
                        </div>

                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    echo ($produto['pf_areas_comercializaco']['pf_operadora']['pf_redes'] != null) ? nl2br($produto['pf_areas_comercializaco']['pf_operadora']['pf_redes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                    break;
                                }
                                break;
                            }
                            ?>
                        </div>


                        <div class="clearfix">&nbsp;</div>
                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                        </div>



                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    echo ($produto['pf_areas_comercializaco']['pf_operadora']['pf_reembolsos'] != null) ? nl2br($produto['pf_areas_comercializaco']['pf_operadora']['pf_reembolsos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                    break;
                                }
                                break;
                            }
                            ?>
                        </div>




                        <div class="clearfix">&nbsp;</div>
                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            CARÊNCIAS (Resumo)
                        </div>



                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    echo ($produto['pf_areas_comercializaco']['pf_operadora']['pf_carencias'] != null) ? nl2br($produto['pf_areas_comercializaco']['pf_operadora']['pf_carencias'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                    break;
                                }
                                break;
                            }
                            ?>
                        </div>

                        <div class="clearfix">&nbsp;</div>

                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
                        </div>
                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    echo ($produto['pf_areas_comercializaco']['pf_operadora']['pf_comercializacoes'] != null) ? "<b>COMERCIALIZAÇÃO: </b><br>" . nl2br($produto['pf_areas_comercializaco']['pf_operadora']['pf_comercializacoes'][0]['descricao']) : "<b>COMERCIALIZAÇÃO: </b><br><span class='info'>Consulte Operadora</span>";
                                    break;
                                }
                                break;
                            }
                            ?>
                            <br>
                            <b>ATENDIMENTO </b>
                            <div class="fonteReduzida">
                                <?php
                                $redes = null;
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        $redes[$produto['pf_areas_comercializaco']['descricao']] = $produto['pf_areas_comercializaco']['pf_operadora']['pf_comercializaco']['descricao'] . " / " . $produto['pf_atendimento']['descricao'];
                                    }
                                }
                                ?>

                                <?php
                                foreach ($redes as $chave => $valor) {
                                    $arr = explode("/", $valor, 2);
                                    echo "<span><b>" . nl2br($chave) . ":</b> " . $arr[1] . "</span><br/>";
                                }
                                ?>
                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>


                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            DOCUMENTOS NECESSÁRIOS
                        </div>



                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    echo ($produto['pf_areas_comercializaco']['pf_operadora']['pf_documentos'] != null) ? nl2br($produto['pf_areas_comercializaco']['pf_operadora']['pf_documentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                    break;
                                }
                                break;
                            }
                            ?>
                        </div>

                        <div class="clearfix">&nbsp;</div>

                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            FORMAS DE PAGAMENTOS
                        </div>
                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    //                                            debug($produto);
                                    //                                            die();
                                    echo ($produto['pf_areas_comercializaco']['pf_operadora']['pf_formas_pagamentos'] != null) ? nl2br($produto['pf_areas_comercializaco']['pf_operadora']['pf_formas_pagamentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                    break;
                                }
                                break;
                            }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>



    <?php
    }
    echo $this->Form->end();
    ?>

    <div class="centralizada">
        <?php
        foreach ($operadoras as $key => $operadora) {
            $caixa = array(5, 33, 34, 35, 38);

            if (!in_array($operadora['id'], $caixa) && !in_array($operadora['id'], array_keys($tabelasOrdenadas)) && $operadora["status"] <> 'OCULTA' && $operadora["status"] <> 'INATIVA') {
                $vetor[$key]['img'] = $this->Html->image("../" . $operadora['imagen']['caminho'] . $operadora['imagen']['nome'], ['class' => 'logoSimulacao']);
                $vetor[$key]['url'] = $operadora['url'];
                $vetor[$key]['id'] = $operadora['id'];
                $vetor[$key]['status'] = $operadora['status'];
            }
        }
        if (isset($vetor)) {
            foreach ($vetor as $item) {
                if ($item['status'] == 'EM ATUALIZAÇÃO') {
                    $msg = "OPERADORA EM PROCESSO DE ATUALIZAÇÃO";
                } else {
                    $msg = "SEM OPÇÕES PARA ESTE PERFIL DE CLIENTE";
                }
        ?>
                <div class="col-xs-12 alert alert-secondary" style="margin: 5px 0; border: 1px solid #ddd;">
                    <div class="col-md-4 col-xs-12 vermais" style="font-size: 12px;text-align: left; padding-left: 50px !important;">
                        <span class="text-danger"><?= $msg ?></span>
                    </div>
                    <div class="col-md-2 col-xs-12 vermais" style="font-size: 14px; text-align: center; padding-left: 1%;">
                        <a class="controle" href="<?= $item['url'] ?>" target="_blank"> Ver Material de Vendas</a>
                    </div>
                    <div class="col-md-6 col-xs-12" operadora-id='<?= $item['id'] ?>'>
                        <?= $item['img']; ?>
                    </div>
                </div>
        <?php
            }
        }
        ?>
    </div>

    <?= $this->element("link-cotacao"); ?>
<?php else :
?>
    <div class="clearfix">&nbsp;</div>
    <div class="alert alert-danger centralizada" role="alert"> Nenhuma tabela encontrada dentro dos Filtros selecionados</div>

<?php endif; ?>
<div id="modal-cotacao" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div> -->
            <div class="modal-body container-fluid" id="edt-cotacao-body">
            </div>
            <!-- <div class="modal-footer texr-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary">Salvar</button>
            </div> -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->