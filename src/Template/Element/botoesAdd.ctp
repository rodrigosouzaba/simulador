<?php
	$array_saude_pme = array('Operadoras', "Tabelas", "Produtos", "Carencias", "FormasPagamentos","Informacoes", "Observacoes","Redes", "Reembolsos", "Regioes","Tipos","Abrangencias", "Opcionais");
	$array_saude_pf = array('PfOperadoras', "PfTabelas", "PfProdutos", "PfCarencias", "PfAcomodacoes", "PfAtendimentos","PfCarencias","PfComercializacoes", "PfDependentes","PfDocumentos","PfEntidades", "PfProfissoes", "PfFormasPagamentos",    "PfRedes", "PfReembolsos");
	$array_odonto = array('OdontoTabelas', "OdontoOperadoras", "OdontoProdutos", "OdontoComercializacoes", "OdontoAtendimentos", "OdontoCarencias","OdontoDependentes","OdontoDocumentos", "OdontoObservacaos","OdontoRedes","OdontoFormasPagamentos", "OdontoReembolsos");
?>
<div class="clearfix">&nbsp;</div>
<div class="col-md-12 centralizada fonteReduzida">
    <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => "btn btn-primary btn-md "]) ?>

    <?php if(in_array($this->request->controller, $array_saude_pme)): ?>
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true',]) . ' Cancelar', ["controller" => "Users", "action" => "configurarPme", 'destino' =>  $this->request->controller],
    ['class' => 'btn btn-md btn-default', 'role' => 'button', "escape" =>false]) ?>

    <?php elseif(in_array($this->request->controller, $array_saude_pf)):?>
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true',]) . ' Cancelar', ["controller" => "Users", "action" => "configurarPf", 'destino' =>  $this->request->controller],
    ['class' => 'btn btn-md btn-default', 'role' => 'button', "escape" =>false]) ?>

    <?php else: ?>
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true',]) . ' Cancelar', ["controller" => "Users", "action" => "configurarOdonto", 'destino' =>  $this->request->controller],
    ['class' => 'btn btn-md btn-default', 'role' => 'button', "escape" =>false]) ?>

    <?php endif; ?>
</div>