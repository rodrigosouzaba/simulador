<style>
    .navbar {
        margin-bottom: 0;
    }

    .logo-grande {
        display: none;
        max-width: 338px;
    }

    .mobile {
        display: block;
    }

    .desktop {
        display: none;
    }

    .links a {
        color: #333 !important;
        font-family: 'Open Sans', sans-serif !important;
        font-size: 12px;
        font-weight: lighter;
        line-height: 28px;
        padding: 14px 10px !important;
    }

    .links a span {
        padding-bottom: 5px;
        border-bottom: 2px solid transparent;
    }

    .links:hover a span {
        border-bottom: 2px solid #003e55;
    }

    .view-desktop {
        display: none;
    }

    .view-mobile {
        display: block;
    }

    .header-desktop {
        width: 100%;
    }

    .btn-area {
        margin-right: 15px;
    }

    @media(min-width: 768px) {
        .view-desktop {
            display: block;
        }

        .view-mobile {
            display: none;
        }

        .container-fluid {
            padding-top: 10px;
        }

        .logo-grande {
            display: block;
        }

        .navbar-brand {
            display: none;
        }

        .desktop {
            display: flex;
            width: 100%;
            justify-content: center;
        }

        .mobile {
            display: none;
        }
    }
</style>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header header-desktop view-desktop">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="nav navbar-nav navbar-right">
                <a href="#modal-login" data-toggle="modal" class="btn btn-sm btn-default btn-area">Área do Corretor</a>
            </div>
            <?= $this->Html->image('logo2019', ['class' => 'logo-grande']) ?>
            <?= $this->Html->image('logo2019', ['class' => 'navbar-brand']) ?>

        </div>

        <div class="navbar-header view-mobile">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?= $this->Html->image('logo2019', ['class' => 'logo-grande']) ?>
            <?= $this->Html->image('logo2019', ['class' => 'navbar-brand']) ?>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="menu">
            <ul class="nav navbar-nav desktop">
                <li class="links">
                    <a href="#">
                        <i class="fa fa-home" style="font-size: 15px;"></i>
                    </a>
                </li>
                <li class="links">
                    <a href="#">
                        <span>FERRAMENTAS DE VENDAS</span>
                    </a>
                </li>
                <li class="links">
                    <a href="#">
                        <span>PLANOS DE SAÚDE</span>
                    </a>
                </li>
                <li class="links">
                    <a href="#">
                        <span>PLANOS ODONTOLÓGICOS</span>
                    </a>
                </li>
                <li class="links">
                    <a href="#">
                        <span>SEGUROS</span>
                    </a>
                </li>
                <li class="links">
                    <a href="#">
                        <span>CONSÓRCIOS</span>
                    </a>
                </li>
                <li class="links">
                    <a href="#">
                        <span>BENEFÍCIOS</span>
                    </a>
                </li>
                <li class="links">
                    <a href="#">
                        <span>QUEM SOMOS</span>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav mobile">
                <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                <li><a href="#">Link</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<?= $this->element('modal-login') ?>
<script>
    $(document).ready(() => {
        $("#labelModalLogin").empty();
    })
</script>
