<table style="width: 100%;">
        <tr>
                <td class="" style="padding-right: 10px;width: 33%;">
                        <?php if ($user['imagem_id'] === null) { ?>
                                <img src="https://corretorparceiro.com.br/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png" id="img" width="200" height="80" class="logo-user-cotacao" />
                        <?php } else { ?>
                                <?= $this->Html->image('/webroot/' . $imagem['caminho'] . $imagem['nome'], ["width" => "200", "height" => "80", "id" => "img", "class" => "logo-user-cotacao", 'fullBase' => true]) ?>
                        <?php } ?>
                </td>
                <td style="font-size: 80%; padding: 0 10px;width: 33%;">
                        <?php if (isset($sessao) && $sessao != null) { ?>
                                <b style="text-transform: uppercase"> <?= $user["nome"] . " " . $user["sobrenome"]; ?></b><br>
                                <img src="https://corretorparceiro.com.br/app/img/celular.png" height="15" style="height: 15px !important;" /><b>Celular: </b> <?= $user["celular"]; ?><br>
                                <img src="https://corretorparceiro.com.br/app/img/whatsapp.png" height="15" style="height: 15px !important;" /><b>Whatsapp: </b> <?= $user["whatsapp"]; ?><br>
                                <img src="https://corretorparceiro.com.br/app/img/email.png" height="15" style="height: 15px !important;" /> <b>Email: </b><?= $user["email"]; ?><br>
                                <img src="https://corretorparceiro.com.br/app/img/site.png" height="15" style="height: 15px !important;" /> <b>Site: </b><?= $user["site"]; ?><br>
                        <?php } ?>
                </td>
                <td style="text-align: right; width: 33%;">
                        <p style="margin: 1px !important"><?= 'Cálculo Nº: ' . $simulacao['id'] ?></p>
                        <?php echo "<p style='font-size:80%; margin: 1px !important'>Estado: " . "</p>"; ?>
                        <p style="font-size:80%; margin: 1px !important">Gerado em <?= date('d/m/Y') ?></p>
                        <?php
                        switch (date('m')):
                                case '01':
                                        $vigencia = 'Janeiro';
                                        break;
                                case '02':
                                        $vigencia = 'Fevereiro';
                                        break;
                                case '03':
                                        $vigencia = 'Março';
                                        break;
                                case '04':
                                        $vigencia = 'Abril';
                                        break;
                                case '05':
                                        $vigencia = 'Maio';
                                        break;
                                case '06':
                                        $vigencia = 'Junho';
                                        break;
                                case '07':
                                        $vigencia = 'Julho';
                                        break;
                                case '08':
                                        $vigencia = 'Agosto';
                                        break;
                                case '09':
                                        $vigencia = 'Setembro';
                                        break;
                                case '10':
                                        $vigencia = 'Outubro';
                                        break;
                                case '11':
                                        $vigencia = 'Novembro';
                                        break;
                                case '12':
                                        $vigencia = 'Dezembro';
                                        break;

                        endswitch;
                        ?>
                        <p style="font-size:80%; margin: 1px !important">Vigência: <?= $vigencia . "/" . date('Y') ?> </p>
                        <small style="font-size: 70%; "><b>*Preços, condições e critérios de aceitação, estão sujeitos a confirmação da operadora no processo de fechamento do condivato</b></small>
                </td>
        </tr>
</table>