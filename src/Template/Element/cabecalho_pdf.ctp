    <table style="width: 100%; margin-bottom: 30px !important;">
        <tr style="margin-bottom: 30px !important;">

            <td style="width: 34%">
                <?php if ($sessao['imagem_id'] === null || !file_exists(WWW_ROOT . $sessao['imagem']['caminho'] . $sessao['imagem']['nome'])) { ?>
                    <img src="<?= WEB_ROOT ?>/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png" style=" margin-left: 20px; max-height: 70px; padding: 5px 0; max-width: 220px;" />
                <?php } else { ?>
                    <img src="<?= WWW_ROOT . $sessao['imagem']['caminho'] . $sessao['imagem']['nome'] ?>" style="margin-left: 20px; max-height: 70px; padding: 5px 0; max-width: 220px;" />
                <?php } ?>
            </td>
            <td style="width: 39%">

                <table style="width: 100%;">
                    <tr style="width: 100%;">
                        <td style="width: 100%;font-size:80%; margin-left: 2px;">
                            <b> <?= $sessao['nome'] . " " . $sessao['sobrenome']; ?></b>
                        </td>
                    </tr>
                    <tr style="font-size:80%">
                        <td>
                            <img src="<?= WWW_ROOT ?>img/celular.png" height="15" style="height: 15px !important;" /><b>Celular: </b> <?= $sessao['celular']; ?>
                        </td>
                    </tr>
                    <tr style="font-size:80%">
                        <td>
                            <img src="<?= WWW_ROOT ?>img/whatsapp.png" height="15" style="height: 15px !important;" /><b>Whatsapp: </b> <?= $sessao['whatsapp']; ?>
                        </td>
                    </tr>
                    <tr style="font-size:80%">
                        <td>
                            <img src="<?= WWW_ROOT ?>img/email.png" height="15" style="height: 15px !important;" /> <b>Email: </b><?= $sessao['email']; ?>
                        </td>

                    </tr>
                    <tr style="font-size:80%">
                        <td>
                            <img src="<?= WWW_ROOT ?>img/site.png" height="15" style="height: 15px !important;" /> <b>Site: </b><?= $sessao['site']; ?>
                        </td>
                    </tr>
                </table>

            </td>
            <td style="text-align: right; width: 25%;">

                <p style="margin: 1px !important"><?= 'Cálculo Nº: ' . $simulacao['id'] ?></p>
                <?php echo "<p style='font-size:80%; margin: 1px !important'>Estado: " . $tabelas[0]['estado']['nome'] . "</p>"; ?>
                <p style="font-size:80%; margin: 1px !important">Gerado em <?= date('d/m/Y') ?></p>
                <?php
                switch (date('m')):
                    case '01':
                        $vigencia = 'Janeiro';
                        break;
                    case '02':
                        $vigencia = 'Fevereiro';
                        break;
                    case '03':
                        $vigencia = 'Março';
                        break;
                    case '04':
                        $vigencia = 'Abril';
                        break;
                    case '05':
                        $vigencia = 'Maio';
                        break;
                    case '06':
                        $vigencia = 'Junho';
                        break;
                    case '07':
                        $vigencia = 'Julho';
                        break;
                    case '08':
                        $vigencia = 'Agosto';
                        break;
                    case '09':
                        $vigencia = 'Setembro';
                        break;
                    case '10':
                        $vigencia = 'Outubro';
                        break;
                    case '11':
                        $vigencia = 'Novembro';
                        break;
                    case '12':
                        $vigencia = 'Dezembro';
                        break;

                endswitch;
                ?>
                <p style="font-size:80%; margin: 1px !important">Vigência: <?= $vigencia . "/" . date('Y') ?> </p>
                <small style="font-size: 70%; "><b>*Preços, condições e critérios de aceitação, estão sujeitos a confirmação da operadora no processo de fechamento do contrato</b></small>
            </td>
        </tr>
    </table>
    <?php //die();
    ?>