<style type="text/css">
    .jumbotron {
        background-color: #C0C0C0;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 0 30px 0;
        padding-left: 20px;
        line-height: 1.5;
        /*width: 97%;*/
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }

    .TituloOperadora {
        background-color: #337ab7;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        line-height: 1.5;
        width: 100%;
    }

    .detalhesOperadora {
        padding: 10px 0px;
        font-weight: bold;
    }

    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .esquerda {
        float: left;
        margin: 15px;

        font-family: sans-serif;
    }

    .direita {
        float: right;
    }

    .total {
        width: 100%;
    }

    .p15 {
        width: 15%;
    }

    .p85 {
        width: 85%;
    }

    .totalTabela {
        width: 100% !important;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .t100 {
        width: 100%;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .tabela-topico {
        width: 100%;
        background-color: #ccc;
        text-align: center;
        font-weight: bold;
        padding: 5px;
        font-size: 90%
    }

    .tabela-produto {
        background-color: #004057;
        color: #ffffff;
        padding: 5px 0;
        text-align: center;
        font-weight: bold;
    }

    .4col {
        width: 22% !important;
    }

    .jumbotron {
        background-color: #C0C0C0;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 0 30px 0;
        padding-left: 20px;
        line-height: 1.5;
        /*width: 97%;*/
    }

    .fonteReduzida {
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        font-size: 7px !important;
        line-height: 1.5;
        margin-bottom: 0;
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }

    .TituloOperadora {
        background-color: #337ab7;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        line-height: 1.5;
        width: 100%;
    }

    .detalhesOperadora {
        padding: 10px 0px;
        font-weight: bold;
    }

    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .centralizada {
        text-align: center;
    }

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .fonteReduzida {
        font-size: 10px;
    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .esquerda {
        float: left;
        margin: 15px;

        font-family: sans-serif;
    }

    .direita {
        float: right;
    }

    .total {
        width: 100%;
    }

    .p15 {
        width: 15%;
    }

    .p85 {
        width: 85%;
    }

    .totalTabela {
        width: 100% !important;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .t100 {
        width: 100%;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .4col {
        width: 25% !important;
    }

    .corpo-gerador-tabelas {
        border: 1px solid #ddd !important;
        /* 	    font-size: 100%; */
        padding: 5px;
        width: 100%;
    }

    .titulo-tabela {
        text-align: center;
        font-weight: bold;
        padding: 5px;
        font-size: 85% !important;
    }

    .precos {
        border: solid 0.5px #ddd;
        text-align: center;
        padding: 3px 0 !important;
    }

    .nobreak {
        page-break-inside: avoid;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .page_header {
        width: 100% !important;
        max-width: 100% !important;
        min-width: 100% !important;
        /* 	    margin: 0 100px; */
    }
</style>

<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
if ($operadora['status'] != 'OCULTA' && $operadora['status'] != 'EM ATUALIZAÇÃO') :
?>
    <table style="width: 96.5%;margin-left: 20px">
        <tr>
            <td style="padding-right: 10px; width: 20%;">
                <img src="<?= "/app/" . $operadora["imagen"]["caminho"] . $operadora["imagen"]["nome"]; ?>" style="height: 70px; position: relative; text-align: center" />
            </td>
            <?php if (!empty($sessao) && $sessao["role"] <> "admin") { ?>

                <td style="padding-right: 10px;width: 20%;">
                    <?php if (isset($sessao["imagem"])) { ?>
                        <img src="<?= "https://corretorparceiro.com.br/app/" . $sessao["imagem"]["caminho"] . $sessao["imagem"]["nome"] ?>" style="width: 70%; position: relative; text-align: center" />
                    <?php } ?>
                </td>
                <td style="font-size: 70%; padding: 0 10px;width: 35%;">
                    <?php if (isset($sessao) && $sessao != null) { ?>
                        <b style="text-transform: uppercase"> <?= $sessao["nome"] . " " . $sessao["sobrenome"]; ?></b><br>
                        <img src="https://corretorparceiro.com.br/app/img/celular.png" height="15" style="height: 15px !important;" /><b>Celular: </b> <?= $sessao["celular"]; ?><br>
                        <img src="https://corretorparceiro.com.br/app/img/whatsapp.png" height="15" style="height: 15px !important;" /><b>Whatsapp: </b> <?= $sessao["whatsapp"]; ?><br>
                        <img src="https://corretorparceiro.com.br/app/img/email.png" height="15" style="height: 15px !important;" /> <b>Email: </b><?= $sessao["email"]; ?><br>
                        <img src="https://corretorparceiro.com.br/app/img/site.png" height="15" style="height: 15px !important;" /> <b>Site: </b><?= $sessao["site"]; ?><br>
                    <?php } ?>
                </td>
            <?php } else { ?>
                <td style="width: 55%; text-align: center">
                    <img src="https://corretorparceiro.com.br/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png" style="height: 70px; position: relative; text-align: center" />
                </td>
            <?php } ?>


            <td style="font-size: 70%; text-align: right;padding-left: 5px; width: 24%; line-height: 12px">
                <?php
                switch (date("m")):
                    case "01":
                        $vigencia = "Janeiro";
                        break;
                    case "02":
                        $vigencia = "Fevereiro";
                        break;
                    case "03":
                        $vigencia = "Março";
                        break;
                    case "04":
                        $vigencia = "Abril";
                        break;
                    case "05":
                        $vigencia = "Maio";
                        break;
                    case "06":
                        $vigencia = "Junho";
                        break;
                    case "07":
                        $vigencia = "Julho";
                        break;
                    case "08":
                        $vigencia = "Agosto";
                        break;
                    case "09":
                        $vigencia = "Setembro";
                        break;
                    case "10":
                        $vigencia = "Outubro";
                        break;
                    case "11":
                        $vigencia = "Novembro";
                        break;
                    case "12":
                        $vigencia = "Dezembro";
                        break;
                endswitch;
                ?>
                <?= "<b>Vigência: </b>" . $vigencia . "/" . date("Y") ?><br>
                <?= "<b>Gerado em</b>: " . date("d/m/Y") ?><br>
                <small><b>*Preços, condições e critérios de aceitação, estão sujeitos a confirmação da operadora no processo
                        de fechamento do contrato</b></small>
            </td>
        </tr>
    </table>
<?php else : ?>
    <div class="centralizada col-xs-12 text-danger">
        <h1>EM ATUALIZAÇÃO</h1>
    </div>
<?php endif; ?>