<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');

if ($operadora['status'] != 'OCULTA' && $operadora['status'] != 'EM ATUALIZAÇÃO') :
?>
    <div class="col-xs-12">
        <div class="logo-operadora">
            <img src="<?= "/app/" . $operadora["imagen"]["caminho"] . $operadora["imagen"]["nome"]; ?>" />
        </div>

        <?php if (!empty($sessao) && $sessao["role"]) { ?>
            <div class="espaco-user">
                <?php if (isset($sessao["imagem"])) { ?>
                    <div class="text-center col-xs-6">
                        <img class="logo-user" src="<?= "https://corretorparceiro.com.br/app/" . $sessao["imagem"]["caminho"] . $sessao["imagem"]["nome"] ?>" />
                    </div>
                <?php } ?>
                <?php if (isset($sessao) && $sessao != null) { ?>
                    <div class="col-xs-6">
                        <b style="text-transform: uppercase"> <?= $sessao["nome"] . " " . $sessao["sobrenome"]; ?></b><br>
                        <img src="https://corretorparceiro.com.br/app/img/celular.png" height="15" style="height: 15px !important;" /><b>Celular: </b> <?= $sessao["celular"]; ?><br>
                        <img src="https://corretorparceiro.com.br/app/img/whatsapp.png" height="15" style="height: 15px !important;" /><b>Whatsapp: </b> <?= $sessao["whatsapp"]; ?><br>
                        <img src="https://corretorparceiro.com.br/app/img/email.png" height="15" style="height: 15px !important;" /> <b>Email: </b><?= $sessao["email"]; ?><br>
                        <img src="https://corretorparceiro.com.br/app/img/site.png" height="15" style="height: 15px !important;" /> <b>Site: </b><?= $sessao["site"]; ?><br>
                    </div>
                <?php } ?>
            </div>
        <?php } else { ?>
            <div class="logo-cp">
                <img src="https://corretorparceiro.com.br/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png" />
            </div>
        <?php } ?>


        <div class="informacoes">
            <span>

                <?php
                switch (date("m")):
                    case "01":
                        $vigencia = "Janeiro";
                        break;
                    case "02":
                        $vigencia = "Fevereiro";
                        break;
                    case "03":
                        $vigencia = "Março";
                        break;
                    case "04":
                        $vigencia = "Abril";
                        break;
                    case "05":
                        $vigencia = "Maio";
                        break;
                    case "06":
                        $vigencia = "Junho";
                        break;
                    case "07":
                        $vigencia = "Julho";
                        break;
                    case "08":
                        $vigencia = "Agosto";
                        break;
                    case "09":
                        $vigencia = "Setembro";
                        break;
                    case "10":
                        $vigencia = "Outubro";
                        break;
                    case "11":
                        $vigencia = "Novembro";
                        break;
                    case "12":
                        $vigencia = "Dezembro";
                        break;
                endswitch;
                ?>
                <?= "<b>Vigência: </b>" . $vigencia . "/" . date("Y") ?><br>
                <?= "<b>Gerado em</b>: " . date("d/m/Y") ?><br>
                <small>
                    <b>
                        *Preços, condições e critérios de aceitação, estão sujeitos a confirmação da operadora no processo
                        de fechamento do contrato
                    </b>
                </small>
            </span>
        </div>
    </div>
<?php
else :
?>
    <div class="centralizada col-xs-12 text-danger">
        <h1>EM ATUALIZAÇÃO</h1>
    </div>
<?php
endif;
?>