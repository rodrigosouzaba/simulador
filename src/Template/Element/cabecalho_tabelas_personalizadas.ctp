<style type="text/css">
    .jumbotron {
        background-color: #C0C0C0;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 0 30px 0;
        padding-left: 20px;
        line-height: 1.5;
        /*width: 97%;*/
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }

    .TituloOperadora {
        background-color: #337ab7;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        line-height: 1.5;
        width: 100%;
    }

    .detalhesOperadora {
        padding: 10px 0px;
        font-weight: bold;
    }

    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .esquerda {
        float: left;
        margin: 15px;

        font-family: sans-serif;
    }

    .direita {
        float: right;
    }

    .total {
        width: 100%;
    }

    .p15 {
        width: 15%;
    }

    .p85 {
        width: 85%;
    }

    .totalTabela {
        width: 100% !important;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .t100 {
        width: 100%;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .4col {
        width: 22% !important;
    }
</style>
<div class="col-xs-12" style="margin-bottom: 1rem;">
    <div class="col-xs-6" style="display: flex; justify-content:center; align-items: center;">
        <img src="<?= "https://corretorparceiro.com.br/app/" . $operadora["imagen"]["caminho"] . $operadora["imagen"]["nome"]; ?>" style="width: 170px; position: relative; text-align: center" />
    </div>
    <div class="col-xs-6" style="display: flex; justify-content:center; align-items: center;">
        <?php if (isset($sessao["imagem"])) : ?>
            <img src="<?= "https://corretorparceiro.com.br/app/" . $sessao["imagem"]["caminho"] . $sessao["imagem"]["nome"] ?>" style="width: 170px; position: relative; text-align: center" />
        <?php else : ?>
            <img src="https://corretorparceiro.com.br/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png" style="position: relative; text-align: center;max-height: 100px;" />
        <?php endif; ?>
    </div>
</div>

<div class="col-xs-12" style="margin-bottom: 2rem;">
    <div class="col-xs-6">
        <?php if (!empty($sessao) && $sessao["role"]) : ?>
            <b style="text-transform: uppercase"> <?= $sessao["nome"] . " " . $sessao["sobrenome"]; ?></b><br>
            <img src="https://corretorparceiro.com.br/app/img/celular.png" height="15" style="height: 15px !important;" /><b>Celular: </b> <?= $sessao["celular"]; ?><br>
            <img src="https://corretorparceiro.com.br/app/img/whatsapp.png" height="15" style="height: 15px !important;" /><b>Whatsapp: </b> <?= $sessao["whatsapp"]; ?><br>
            <img src="https://corretorparceiro.com.br/app/img/email.png" height="15" style="height: 15px !important;" /> <b>Email: </b><?= $sessao["email"]; ?><br>
            <img src="https://corretorparceiro.com.br/app/img/site.png" height="15" style="height: 15px !important;" /> <b>Site: </b><?= $sessao["site"]; ?><br>
        <?php else : ?>
            <img src="https://corretorparceiro.com.br/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png" style="width: 60%; position: relative; text-align: center" />
        <?php endif; ?>
    </div>
    <div class="col-xs-6 text-right">
        <?php
        switch (date("m")):
            case "01":
                $vigencia = "Janeiro";
                break;
            case "02":
                $vigencia = "Fevereiro";
                break;
            case "03":
                $vigencia = "Março";
                break;
            case "04":
                $vigencia = "Abril";
                break;
            case "05":
                $vigencia = "Maio";
                break;
            case "06":
                $vigencia = "Junho";
                break;
            case "07":
                $vigencia = "Julho";
                break;
            case "08":
                $vigencia = "Agosto";
                break;
            case "09":
                $vigencia = "Setembro";
                break;
            case "10":
                $vigencia = "Outubro";
                break;
            case "11":
                $vigencia = "Novembro";
                break;
            case "12":
                $vigencia = "Dezembro";
                break;
        endswitch;
        ?>
        <?= "<b>Estado: </b>" . $operadora["estado"]["nome"] ?><br>
        <?= "<b>Vigência: </b>" . $vigencia . "/" . date("Y") ?><br>
        <?= "<b>Gerado em</b>: " . date("d/m/Y") ?><br>
        <small><b>*Preços, condições e critérios de aceitação, estão sujeitos a confirmação da operadora no processo
                de fechamento do contrato</b></small>
    </div>
</div>