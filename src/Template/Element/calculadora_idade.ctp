<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
      <!-- <a href="#" id="calculadoraIdade" class="text-danger"><i class="fas fa-calculator"></i> </a> -->
        <div class="tituloField centralizada">
            PREENCHA A(S) IDADE(S) <br>
            <a role="button" data-toggle="collapse" data-parent="#accordion" class="text-danger" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                 Calculadora de Idade
            </a>
        </div>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 centralizada">
                <?= $this->Form->input('idade', ['class' => 'form-control fonteReduzida centralizada date idade', 'label' => 'Digite a Data de Nascimento', 'placeholder' => '01/01/2000']) ?>
                <div id="respostaIdade" class="centralizada text-danger" style="font-size: 13px;">
                </div>
                <div id="respostaAniversario" class="centralizada text-danger" style="font-size: 13px;">
                </div>
                <div id="respostaIdadeErrada" class="centralizada text-danger" style="font-size: 13px;">
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- FIM Calduladora de Idade-->
<script>

$("#respostaIdade").empty();

$("#idade2").change(function() {
    $("#respostaIdade").empty();
    $("#respostaIdadeErrada").empty();
    $("#respostaAniversario").empty();

    var hoje = new Date();
    var data = $("#idade").val();
    resposta = data.split("/");


    var nascimento = new Date(resposta[2], resposta[1] - 1, resposta[0]);
    var dia = hoje.getDate() - nascimento.getDate();
    var mes = hoje.getMonth() - nascimento.getMonth();
    var anos = hoje.getFullYear() - nascimento.getFullYear();

    if (
        resposta[0] > 31 ||
        resposta[1] > 12 ||
        resposta[2] > hoje.getFullYear() ||
        (resposta[1] == 2 && resposta[0] > 29) ||
        (resposta[1] == 4 && resposta[0] > 30) ||
        (resposta[1] == 6 && resposta[0] > 30) ||
        (resposta[1] == 9 && resposta[0] > 30) ||
        (resposta[1] == 11 && resposta[0] > 30) ||
        (resposta[2] == hoje.getFullYear() && (resposta[1] - 1) > hoje.getMonth()) ||
        (resposta[2] == hoje.getFullYear() && (resposta[1] - 1) == hoje.getMonth() && resposta[0] > hoje
            .getDate())
    ) {
        $("#respostaIdade").empty();
        $("#respostaAniversario").empty();
        $("#respostaIdadeErrada").empty();
        $("#respostaIdadeErrada").append("Data inválida");
    } else {

        if (dia < 0) {
            mes--;

            if (mes == 3 || mes == 5 || mes == 8 || mes == 10) {
                dia = 30 + dia;

            } else if (mes == 1) {

                if (anos % 4 == 0) {
                    dia = 29 + dia;
                } else {
                    dia = 28 + dia;
                }

            } else {

                dia = 31 + dia;

            }
        }
        if (mes < 0) {
            anos--;
            mes = 12 + mes;
        }
        if (anos < 0) {
            anos++;
            mes = 12 - mes;
        }
        var final = $("#idade").val();
        final = final.split("");
        final = final[9];

        //começo do aniversário


        mesAniversario = 12 - mes;
        if (dia > 0) {
            mesAniversario--;
        }
        if (hoje.getMonth() == 1) {
            diaAniversario = 28 - dia;
        } else if (hoje.getMonth() == 10 || hoje.getMonth() == 3 || hoje.getMonth() == 5 || hoje.getMonth() ==
            8) {
            diaAniversario = 30 - dia;
        } else {
            diaAniversario = 31 - dia;
        }

        if (final != "_") {
            $("#respostaIdade").empty();
            $("#respostaAniversario").empty();
            $("#respostaIdadeErrada").empty();

            var txtDia;
            var txtMes;
            var txtAno;

            txtMes = mes > 1 ? "meses" : "mês";
            txtDia = anos > 1 ? "dias" : "dia";
            txtAno = dia > 1 ? "anos" : "ano";

            $("#respostaIdade").append("<b>" + anos + "</b> " + txtAno + ", <b>" + mes + "</b> " + txtMes +
                " e <b>" + dia +
                "</b> " + txtDia + ".");

            txtMes = mesAniversario > 1 ? "meses" : "mês";
            txtDia = diaAniversario > 1 ? "dias" : "dia";
            txtAno = (anos + 1) > 1 ? "anos." : "ano.";
            $("#respostaAniversario").append(mesAniversario + " " + txtMes + " e " + diaAniversario + " " +
                txtDia + " para " + (
                    anos + 1) + " " + txtAno);
        }
    }
});
</script>