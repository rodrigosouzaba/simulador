<?php

$controller = $this->request->params['controller'];
$session = $this->request->session();
$sessao = $session->read('Auth.User');

$calculo = $simulacao;
?>
<div class="col-md-12 centralizada" style=" margin-top: 20px;clear: both;">

    <div class="clearfix">&nbsp;</div>

    <div class="col-md-12 centralizada" style=" font-weight: bold">
        ESCOLHA O QUE DESEJA EXIBIR NO PDF
    </div>
    <div class="col-md-2 col-md-offset-1">
        <?= $this->Form->input('observacao', ['type' => 'checkbox', 'label' => 'Observações Importantes', 'checked' => 'checked']); ?>
    </div>
    <div class="col-md-2">
        <?= $this->Form->input('rede', ['type' => 'checkbox', 'label' => 'Rede Credenciada (Resumo)', 'checked' => 'checked']); ?>
    </div>
    <div class="col-md-2">
        <?= $this->Form->input('reembolso', ['type' => 'checkbox', 'label' => 'Coberturas e Diferenciais', 'checked' => 'checked']); ?>
    </div>
    <div class="col-md-2">
        <?= $this->Form->input('carencia', ['type' => 'checkbox', 'label' => 'Carência (Resumo)', 'checked' => 'checked']); ?>
    </div>
    <div class="col-md-2">
        <?= $this->Form->input('documento', ['type' => 'checkbox', 'label' => 'Documentos Necessários', 'checked' => 'checked']); ?>
    </div>


    <!-- Botões de compartilhamento  -->
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12 centralizada">

        <!-- HIDDEN FIELD COM O TIPO DE AÇÃO REFERENTE AO PDF (DOWNLOAD ou GERAÇÃO NO BROWSER) VIA jQuery -->
        <?= $this->Form->input("tipopdf", ["type" => "hidden", "value" => ""]); ?>

        <!-- DOWNLOAD DO PDF -->
        <?= $this->Form->button($this->Html->tag('span', '', ['class' => 'fa fa-download', 'aria-hidden' => 'true']), ['class' => ' btn btn-lg btn-primary', "id" => "pdfdownload", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "bottom", "title" => "Download do PDF"]); ?>

        <!-- PDF NO BROWSER -->
        <?= $this->Form->button($this->Html->tag('span', '', ['class' => 'fas fa-print', 'aria-hidden' => 'true']), ['class' => ' btn btn-lg btn-primary', 'style' => 'display: inline !important; border: none !important', 'role' => 'button', 'escape' => false, 'formtarget' => '_blank', "id" => "pdfbrowser", "data-toggle" => "tooltip", "data-placement" => "bottom", "title" => "Visualizar PDF"]); ?> 


        <!-- PDF VIA EMAIL -->
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-envelope', 'aria-hidden' => 'true']), '#', ['class' => ' btn btn-lg btn-primary', 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, "data-toggle" => "modal", "data-tooltip" => "true",
            "data-target" => "#modalShare", "data-placement" => "bottom", "title" => "Enviar Tabela por Email",
            "id" => "compartilhar"]);
            ?>
        </div>

        <div id="salvarSimulacao"></div>
    </div>



    <!-- Modal Compartilhamento via EMAIL -->
    <div class="modal fade" id="modalShare" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 99999 !important;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body clearfix">
                    <div class="col-xs-12">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>	     
                    <div class="col-xs-12 col-md-6 centralizada">
                        <?=
                        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-envelope fa-2x', 'aria-hidden' => 'true']) . "<br>Enviar para o meu E-mail", "#", ['class' => 'btn btn-lg btn-block btn-default', 'style' => 'border: none !important', 'role' => 'button', 'escape' => false, 'id' => 'pdfemail']);
                        ?>
                        <small><?= $sessao["email"] ?></small>

                    </div>
                    <div class="col-xs-12 col-md-6 centralizada">
                        <?=
                        $this->Html->link($this->Html->tag('span', '', [
                            'class' => 'fas fa-share fa-2x',
                            'aria-hidden' => 'true']) . '<br>Enviar para <u>outro</u> E-mail', "#", [
                            'class' => 'btn btn-block btn-lg btn-default',
                            'role' => 'button',
                            'escape' => false,
                            'style' => 'border: none !important',
                            "id" => "outroemail"]);
                            ?>

                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12 centralizada" id="fieldShareEmail">
                            <?= $this->Form->input('email-cliente', ["label" => false, "placeholder" => "Digite o e-mail", "class" => "centralizada"]); ?>
                            <?= $this->Html->link("Enviar", "#", ['class' => ' btn btn-md btn-primary', "id" => "pdfoutroemail", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false,]); ?>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="centralizada col-xs-12" id="responseEmail">
                        </div>
                        <div class="centralizada col-xs-12">
                              <?= $this->Html->image('ciranda.gif', ['alt' => 'Wait...', 'width' => '30px;', "id" => "cirandaGif", "style" => "display: none;", 'class' => 'centralizada']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            $(document).ready(function () {
                $("#fieldShareEmail").hide();

                $(function () {
                    $('[data-tooltip="true"]').tooltip();
                });
                $('[data-toggle="tooltip"]').tooltip();
            });
            $("#outroemail").click(function () {
                $("#fieldShareEmail").toggle();
                $("#responseEmail").empty();
            });

            var action = '<?= $this->request->params["action"]?>';
            if (action === 'filtro') {
                var serialize = 'operadorasFiltro';
            } else {
                var serialize = 'operadoras';
            }

            $("#pdf").click(function () {

                $.ajax({
                    type: "post",
                    beforeSend: function () {
                        $('#loader').trigger('click');
                    },
                    success: function (data) {
                        $('#fechar').trigger('click');
                    }
                });
            });

            $("#pdfdownload").click(function () {
                $("#tipopdf").val('D');
                $.ajax({
                    type: "post",
                    url: "<?php echo $this->request->webroot.$controller ?>/pdf/",
                    data: $("#" + serialize).serialize(),
                    beforeSend: function () {
                        $('#loader').trigger("click");
                    },
                    complete: function () {
                        // $('#fechar').trigger("click");   
                        setTimeout(function () {
                            $("#fechar").trigger('click');
                        }, 6000);
                    }
                });
            });
            $("#pdfemail").click(function () {
                $("#responseEmail").empty();
                $("#tipopdf").val('F');
                $.ajax({
                    type: "post",
                    url: "<?php echo $this->request->webroot.$controller ?>/pdf/",
                    data: $("#" + serialize).serialize(),
                    beforeSend: function () {
                        $("#cirandaGif").show();
                    },
                    success: function (data) {
                        $.ajax({
                            type: "GET",
                            url: "<?= $this->request->webroot .$controller. '/email/' . $calculo['id'] ?>",
                            success: function (data) {
                                $("#responseEmail").empty();
                                $("#cirandaGif").hide();
                                $('#fechar').trigger('click');
                                $('.modal-backdrop').remove();
                                $("#responseEmail").append(data);
                            },
                            error: function () {
                                $("#responseEmail").empty();
                                $("#cirandaGif").hide();
                                $('#fechar').trigger('click');
                                $('.modal-backdrop').remove();
                                $("#responseEmail").append("Erro ao enviar o e-mail, por favor tente novamente.");
                            }
                        });
                    }
                });
            });

            $("#pdfoutroemail").click(function () {
                $("#tipopdf").val('F');
                $.ajax({
                    type: "post",
                    url: "<?php echo $this->request->webroot.$controller ?>/pdf/",
                    data: $("#" + serialize).serialize(),
                    beforeSend: function () {
                        $("#cirandaGif").show();
                    },
                    success: function (data) {
                        $.ajax({
                            type: "GET",
                            url: "<?= $this->request->webroot .$controller. '/email/' . $calculo['id'] ?>",
                            success: function (data) {
                                $("#responseEmail").empty();
                                $("#cirandaGif").hide();
                                $('#fechar').trigger('click');
                                $('.modal-backdrop').remove();
                                $("#responseEmail").append(data);
                            },
                            error: function () {
                                $("#responseEmail").empty();
                                $("#cirandaGif").hide();
                                $('#fechar').trigger('click');
                                $('.modal-backdrop').remove();
                                $("#responseEmail").append("Erro ao enviar o e-mail, por favor tente novamente.");
                            }
                        });
                    }
                });
            });

            $("#pdfbrowser").click(function () {
                $("#tipopdf").val('I');
                $.ajax({
                    type: "post",
                    url: "<?php echo $this->request->webroot.$controller ?>/pdf/",
                    data: $("#" + serialize).serialize(),
                });
            });
            $("#compartilhar").click(function () {
                $("body").prop("scrollTop", "300px");
            });
        </script>