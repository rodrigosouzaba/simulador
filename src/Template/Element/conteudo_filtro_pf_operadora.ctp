<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($filtradas as $item) : ?>
            <tr>
                <td><?= $item->nome ?></td>
                <td><?= $item->descricao ?></td>
                <td>
                    <?php
                    //                        debug($produto);die();
                    if (isset($item->pf_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $item->pf_operadora->imagen->caminho . "/" . $item->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $item->pf_operadora->detalhe;
                    } else {
                        echo $item->pf_operadora->nome . " - " . $item->pf_operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?php if ($sessao['role'] == 'admin') { ?>


                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $item->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Carência']) ?>

                        <?= $this->Form->postLink('', ['action' => 'delete', $item->id], ['confirm' => __('Confirma exclusão da Carência?', $item->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash'])
                        ?>
                    <?php } ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
