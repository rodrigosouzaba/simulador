<?php
$controler = $this->request->params['controller'];
$session = $this->request->session();
$sessao = $session->read('Auth.User');
if (!empty($sessao) && $sessao['role'] !== "") :
    ?>
    <div class="modal fade" id="modalCompartilhamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 99999 !important;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body clearfix">
                    <div class="col-xs-12">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="col-xs-12 col-md-6 centralizada">
                        <?=
                            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-envelope fa-2x', 'aria-hidden' => 'true']) . "<br>Enviar para o meu E-mail", "#", ['class' => 'btn btn-lg btn-block btn-default', 'style' => 'border: none !important', 'role' => 'button', 'escape' => false, 'id' => 'pdfemail', "link_id" => $filtro->id]);
                        ?>
                        <small><?= $sessao["email"] ?></small>

                    </div>
                    <div class="col-xs-12 col-md-6 centralizada">
                        <?=
                            $this->Html->link($this->Html->tag('span', '', [
                                'class' => 'fas fa-share fa-2x',
                                'aria-hidden' => 'true'
                            ]) . '<br>Enviar para <u>outro</u> E-mail', "#", [
                                'class' => 'btn btn-block btn-lg btn-default',
                                'role' => 'button',
                                'escape' => false,
                                'style' => 'border: none !important',
                                "id" => "outroemail"
                            ]);
                        ?>

                    </div>
                    <div class="col-xs-12">&nbsp;</div>

                    <div class="col-xs-12 centralizada" id="fieldShareEmail">
                        <div class="col-xs-12">
                            <?= $this->Form->input('cliente-email', ["label" => false, "placeholder" => "Digite o e-mail", "class" => "form-control centralizada"]); ?>
                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12 centralizada" id="fieldShareEmail">
                            <?= $this->Form->input("pdfoutroemail", ['value' => "Enviar", 'label' => false, 'class' => ' btn btn-md btn-primary', "link_id" => $filtro->id, 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false,]); ?>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                    <div class="centralizada col-xs-12" id="responseEmail">
                    </div>
                    <div class="centralizada col-xs-12">
                        <?= $this->Html->image('ciranda.gif', ['alt' => 'Wait...', 'width' => '30px;', "id" => "cirandaGif", "style" => "display: none;", 'class' => 'centralizada']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<script>
    $(document).ready(function() {
        $("#fieldShareEmail").hide();

        $(function() {
            $('[data-tooltip="true"]').tooltip();
        });
        $('[data-toggle="tooltip"]').tooltip();
    });

    // MOSTRA CAMPOS DE ENVIO DE EMAIL
    $("#outroemail").click(function() {
        $("#fieldShareEmail").slideToggle();
    });


    //ENVIA EMAIL PARA USUÁRIO LOGADO
    $("#pdfemail").click(function() {
        $("#responseEmail").empty();
        $("#tipopdf").val('F');
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot . $controler ?>/pdf/",
            data: $("#dadosPDF").serialize(),
            beforeSend: function() {
                $("#cirandaGif").show();
            },
            success: function(data) {
                $.ajax({
                    type: "GET",
                    url: "<?= $this->request->webroot . $controler . '/email/' . $simulacao['id'] ?>",
                    success: function(data) {
                        $("#responseEmail").empty();
                        $("#cirandaGif").hide();
                        $('#fechar').trigger('click');
                        $('.modal-backdrop').remove();
                        $("#responseEmail").append(data);
                    },
                    error: function() {
                        $("#responseEmail").empty();
                        $("#cirandaGif").hide();
                        $('#fechar').trigger('click');
                        $('.modal-backdrop').remove();
                        $("#responseEmail").append("Erro ao enviar o e-mail, por favor tente novamente.");
                    }
                });
            }
        });
    });

    $("#pdfoutroemail").click(function() {
        $("#tipopdf").val('F');
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot . $controler ?>/pdf/",
            data: $("#dadosPDF").serialize(),
            beforeSend: function() {
                $("#cirandaGif").show();
            },
            success: function(data) {
                $.ajax({
                    type: "GET",
                    url: "<?= $this->request->webroot . $controler . '/email/' . $simulacao['id'] . "/" ?>" + $("#cliente-email").val(),
                    success: function(data) {
                        $("#responseEmail").empty();
                        $("#cirandaGif").hide();
                        $('#fechar').trigger('click');
                        $('.modal-backdrop').remove();
                        $("#responseEmail").append(data);
                    },
                    error: function(xhr, status, thrown) {
                        $("#responseEmail").empty();
                        $("#cirandaGif").hide();
                        $('#fechar').trigger('click');
                        $('.modal-backdrop').remove();
                        $("#responseEmail").append("Erro ao enviar o e-mail, por favor tente novamente.");
                    }
                });
            }
        });
    });
</script>