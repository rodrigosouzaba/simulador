<style type="text/css">
    .jumbotron {
        background-color: #C0C0C0;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 0 30px 0;
        padding-left: 20px;
        line-height: 1.5;
        /*width: 97%;*/
    }

    .fonteReduzida {
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        font-size: 7px !important;
        line-height: 1.5;
        margin-bottom: 0;
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }

    .TituloOperadora {
        background-color: #337ab7;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        line-height: 1.5;
        width: 100%;
    }

    .detalhesOperadora {
        padding: 10px 0px;
        font-weight: bold;
    }

    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .centralizada {
        text-align: center;
    }

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .fonteReduzida {
        font-size: 9px;
    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .esquerda {
        float: left;
        margin: 15px;

        font-family: sans-serif;
    }

    .direita {
        float: right;
    }

    .total {
        width: 100%;
    }

    .p15 {
        width: 15%;
    }

    .p85 {
        width: 85%;
    }

    .totalTabela {
        width: 100% !important;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .t100 {
        width: 100%;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .corpo-gerador-tabelas {
        border: 1px solid #ddd !important;
        font-size: 10px;
        padding: 5px;
        width: 100%;
    }

    .titulo-tabela {
        text-align: center;
        font-weight: bold;
        padding: 5px;
        font-size: 85% !important;
    }

    .precos {
        border: solid 0.5px #ddd;
        text-align: center;
        padding: 3px 0 !important;
    }

    .nobreak {
        page-break-inside: avoid;
        max-width: 100%;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .page_header {
        width: 100% !important;
        max-width: 100% !important;
        min-width: 100% !important;
        /* 	    margin: 0 100px; */
    }

    .produtos {
        background-color: #ccc;
        text-align: center;
    }

    .produtos td {
        padding: 5px 0 5px 3px;
        border-top: solid 0.1mm #5a5a5a;
        border-left: solid 0.1mm #5a5a5a;
        border-right: solid 0.1mm #5a5a5a;
    }

    .info {
        width: 98%;
        text-align: center;
    }
</style>