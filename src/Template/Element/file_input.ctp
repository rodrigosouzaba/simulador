<?= $this->Form->input($name, [
    'type' => 'file',
    'accept' => $accept,
    'id' => isset($id) ? $id : '', /* OPCIONAL */
    'value' => isset($value) ? $value : '', /* OPCIONAL */
    'style' => 'display: none;',
    'label' => false
]) ?>
<?php
if (isset($label)) {
    echo "<label for='$id'>$label</label>";
}
?>
<input class="file_input_personality" placeholder="<?= $placeholder ?>" id="fake-<?= $name ?>" value="<?= $value ?>" readonly <?= isset($disabled) ? 'disabled' : '' ?> />

<script>
    $("#fake-<?= $name ?>").click(function(e) {
        e.preventDefault();
        $("#<?= $id ?>").click()
    });
    $("input[name=<?= $name ?>]").change(function() {
        let arquivo = $(this).val();
        $("#fake-<?= $name ?>").val(arquivo.replace('C:\\fakepath\\', ''));
    })
</script>

<style>
    .file_input_personality,
    .file_input_personality:hover,
    .file_input_personality:active {
        -moz-appearance: none;
        border-radius: 0;
        background-color: #fff;
        border-style: solid;
        border-width: 1px;
        border-color: #ccc;
        box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
        color: rgba(0, 0, 0, 0.75);
        display: block;
        font-family: inherit;
        font-size: 0.875rem;
        height: 2.3125rem;
        margin: 0 0 0 0 !important;
        padding: 0.5rem;
        width: 100%;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-transition: border-color 0.15s linear, background 0.15s linear;
        -moz-transition: border-color 0.15s linear, background 0.15s linear;
        -ms-transition: border-color 0.15s linear, background 0.15s linear;
        -o-transition: border-color 0.15s linear, background 0.15s linear;
        transition: border-color 0.15s linear, background 0.15s linear;
    }

    .file_input_personality[disabled]{
        background-color: #ddd;
        cursor: default;
    }
</style>