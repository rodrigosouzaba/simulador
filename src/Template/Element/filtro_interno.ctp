<div class="col-md-2 fonteReduzida">
    <?= $this->Form->input('nova', ['value' => isset($nova) ? $nova : "", 'options' => [1 => 'Sim', 0 => 'Não'], 'label' => '', 'empty' => 'Nova']); ?>
</div>
<div class="col-md-2 fonteReduzida">
    <?php if (empty($estados)) :
        echo $this->Form->input('estado', ['value' => isset($estado) ? $estado : "", 'options' =>  '', 'label' => '', 'empty' => 'Selecione ESTADO', 'disabled' => true]);
    else :
        echo $this->Form->input('estado', ['value' => isset($estado) ? $estado : "", 'options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']);
    endif;
    ?>
</div>

<div id="operadoras" class="col-md-2 fonteReduzida">
    <?php if (empty($operadoras)) : ?>
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
    <?php else : ?>
        <?= $this->Form->input('operadora_id', ['value' =>  $operadora, 'options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
    <?php endif; ?>
</div>
<script>
    var action = '<?= $this->request->getParam('controller') ?>'.charAt(0).toLowerCase() + '<?= $this->request->getParam('controller') ?>'.slice(1);
    $("#nova").change(function() {
        filtra('nova', action)
    });
    $("#estado").change(function() {
        filtra('estado', action)
    });
    $("#operadora-id").change(function() {
        filtra('operadora', action)
    });
</script>