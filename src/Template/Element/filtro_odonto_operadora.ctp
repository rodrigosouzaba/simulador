<div class="fonteReduzida">
    <?= $this->Form->input('operadora_id', ['options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
</div>
<script type="text/javascript">
    $("#operadora-id").change(function() {
        $.ajax({
            type: "POST",
            url: "<?= $this->request->webroot . $this->request->controller ?>/filtroOperadora/" + $("#operadora-id").val(),
            data: $("#operadora-id").serialize(),
            beforeSend: function() {
                $("#loader").click();
            },
            success: function(data) {
                $("#respostaFiltroOperadora").empty();
                $("#respostaFiltroOperadora").append(data);
                $("#fechar").click();
                $(".modal").modal('hide');
                $(".modal-backdrop").hide();
            }
        });
    });
</script>
