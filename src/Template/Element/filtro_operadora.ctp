<div class="fonteReduzida">
    <?= $this->Form->input('operadora_id', ['options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
    <?= isset($new) ? $this->Form->hidden('new', ['id' => 'new', 'value' => 1]) : $this->Form->hidden('new', ['id' => 'new', 'value' => 0]) ?>
</div>
<script type="text/javascript">
    $("#operadora-id").change(function() {
        if ($("#operadora-id").val() != '') {
            $.ajax({
                type: "POST",
                url: "<?= $this->request->webroot . $this->request->controller ?>/filtroOperadora/" + $("#operadora-id").val(),
                data: {
                    operadora_id: $("#operadora-id").val(),
                    new: $("#new").val()
                },
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $("#respostaFiltroOperadora").empty();
                    $("#respostaFiltroOperadora").append(data);
                    $("#fechar").click();
                    $(".modal").modal('hide');
                    $(".modal-backdrop").hide();
                }
            });
        }
    });
</script>