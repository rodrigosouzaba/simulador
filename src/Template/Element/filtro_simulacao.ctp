<div id="ancora"></div>
<?php
$todos = 'TODAS AS OPÇÕES';
array_push($acomodacoes, $todos);
?>
<style>
    .col {
        flex: 1 0 0%;
    }
</style>

<div id="fixa">
    <div id="filtro-responsivo">
        <a id="btn-filtro" data-toggle="collapse" data-target="#body-filtro" role="button" aria-expanded="false" class="btn">
            <div class="col-xs-12 text-center text-nowrap" style="margin: 5px !important;">
                <b> <?= $totalTabelas ?></b> CÁLCULOS GERADOS. FILTRE <b><u>AQUI</u></b> SEUS CÁLCULOS.
            </div>
        </a>
        <div class="collapse" id="body-filtro">
            <div class="flex" style="justify-content: center;">

                <div class="col-xs-12 col">
                    <?= $this->Form->hidden('simulacao_id', ['value' => $simulacao['id']]) ?>
                    <?php
                    $abrangencias_ordenadas = array(
                        "1" => "NACIONAL",
                        "5" => "GRUPO DE ESTADOS",
                        "2" => "ESTADUAL",
                        "4" => "GRUPO DE MUNICÍPIOS",
                        "3" => "MUNICIPAL",
                        "6" => "TODOS",
                    );
                    ?>
                    <?= $this->Form->input('abrangencia_id', ['options' => $abrangencias_ordenadas, 'label' => '', 'empty' => 'Atendimento', 'class' => 'selectFiltro']); ?>
                </div>
                <div class="col-xs-12 col">
                    <?= $this->Form->input('tipo_id', ['options' => $acomodacoes, 'label' => '', 'empty' => 'Acomodação', 'class' => 'selectFiltro']); ?>
                </div>
                <div class="col-xs-12 col">
                    <?= $this->Form->input('cobertura_id', ['options' => $coberturas, 'label' => '', 'empty' => 'Cobertura', 'class' => 'selectFiltro']); ?>
                </div>
                <div class="col-xs-12 col">
                    <?php $simnao = array('s' => 'COM COPARTICIPAÇÃO', 'n' => 'SEM COPARTICIPAÇÃO', '99' => 'TODAS AS OPÇÕES',) ?>
                    <?= $this->Form->input('coparticipacao', ['options' => $simnao, 'label' => '', 'empty' => 'Coparticipação', 'class' => 'selectFiltro']); ?>
                </div>
                <div class="col-xs-12 col">
                    <?php $tipo_contratacao = array('2' => 'OPCIONAL', '1' => 'COMPULSÓRIO', '99' => 'TODAS AS OPÇÕES') ?>
                    <?= $this->Form->input('tipo_contratacao', ['options' => $tipo_contratacao, 'label' => '', 'empty' => 'Contratação', 'class' => 'selectFiltro']); ?>
                </div>
                <div class="col-xs-12 col">
                    <?php $filtroreembolso = array('N' => 'SEM REEMBOLSO', 'S' => 'COM REEMBOLSO', '99' => 'TODAS AS OPÇÕES') ?>
                    <?= $this->Form->input('filtroreembolso', ['options' => $filtroreembolso, 'label' => '', 'empty' => 'Reembolso', 'class' => 'selectFiltro']); ?>
                </div>
                <div class="col-xs-12 col">
                    <?= $this->Form->input('municipios', ['options' => $municipios, 'label' => '', 'empty' => 'Municipios', 'class' => 'selectFiltro']); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="editModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar Cálculo</h4>
            </div>
            <div class="modal-body">
                <div id="inputEdit"></div>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript">
    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var div_top = $('#ancora').offset().top;
        var window_width = $('#dadosCalculo').outerWidth();
        if (window_top > div_top) {
            $('#fixa').addClass('fixa');
            $('#fixa').width(window_width);
            $('#ancora').height($('#fixa').outerHeight());
        } else {
            $('#fixa').removeClass('fixa');
            $('#ancora').height(0);
        }
    }
    $(function() {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });
</script>