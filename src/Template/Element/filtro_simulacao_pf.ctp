<div id="ancora"></div>
<?php
$todos = 'TODAS AS OPÇÕES';
$pfAtendimentos[99] = $todos;
$pfAcomodacoes[99] = $todos;
$pfCoberturas[99] = $todos;
?>

<div id="fixa">
    <div id="filtro-responsivo" class="col-xs-12" style="padding: 0 !important;">
        <a id="btn-filtro" data-toggle="collapse" data-target="#body-filtro" role="button" aria-expanded="false" class="btn">
            <b> <?= $totalTabelas ?></b> CÁLCULOS GERADOS. FILTRE <b><u>AQUI</u></b> SEUS CÁLCULOS.
        </a>
        <div class="collapse col-xs-12" id="body-filtro">
            <div class="col-xs-12 col-md-2">
                <?= $this->Form->hidden('simulacao_id', ['value' => $simulacao['id']]) ?>
                <?= $this->Form->input('pf_atendimento_id', ['options' => $pfAtendimentos, 'label' => '', 'empty' => 'Atendimento', 'class' => 'filtroSimulacao']); ?>
            </div>
            <div class="col-xs-12 col-md-2">
                <?= $this->Form->input('pf_acomodacao_id', ['options' => $pfAcomodacoes, 'label' => '', 'empty' => 'Acomodação', 'class' => 'filtroSimulacao']); ?>
            </div>
            <div class="col-xs-12 col-md-2">
                <?= $this->Form->input('pf_cobertura_id', ['options' => $pfCoberturas, 'label' => '', 'empty' => 'Coberturas', 'class' => 'filtroSimulacao']); ?>
            </div>
            <div class="col-xs-12 col-md-2">
                <?php $simnao = array('s' => 'COM COPARTICIPAÇÃO', 'n' => 'SEM COPARTICIPAÇÃO', '99' => 'TODAS AS OPÇÕES',) ?>
                <?= $this->Form->input('coparticipacao', ['options' => $simnao, 'label' => '', 'empty' => 'Coparticipação', 'class' => 'filtroSimulacao']); ?>
            </div>
            <div class="col-xs-12 col-md-2">
                <?php $filtroreembolso = array('N' => 'SEM REEMBOLSO', 'S' => 'COM REEMBOLSO', '99' => 'TODAS AS OPÇÕES') ?>
                <?= $this->Form->input('filtroreembolso', ['options' => $filtroreembolso, 'label' => '', 'empty' => 'Reembolso', 'class' => 'filtroSimulacao']); ?>
            </div>
            <div class="col-xs-12 col-md-2">
                <?= $this->Form->input('municipios', ['options' => $municipios, 'label' => '', 'empty' => 'Municipios', 'class' => 'filtroSimulacao']); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var window_width = $('#dadosCalculo').outerWidth();

        var div_top = $('#ancora').offset().top;
        if (window_top > div_top) {
            $('#fixa').addClass('fixa');
            $('#fixa').width(window_width);
            $('#ancora').height($('#fixa').outerHeight());
        } else {
            $('#fixa').removeClass('fixa');
            $('#ancora').height(0);
        }
    }
    $(function() {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });
</script>