<?php $estado = $this->request->session()->read('Auth.User.estado_id'); ?>
<style>
    #fundo {
        background-color: #fff;
        opacity: 0.5;
        width: 100%;
        height: 100%;
        z-index: 9998;
        position: absolute;
        display: none;
        top: 0;
        left: 0;
    }

    #fake-select {
        background-color: #FAFAFA;
        border-radius: 0px;
        width: 100%;
    }

    .option {
        padding: 5px;
    }

    .link {
        float: left;
        font-size: 85%;
        cursor: default;
    }

    .link:hover {
        text-decoration: underline;
    }

    .deletar {
        font-size: 15pt;
        color: #d9534f;
        text-align: center;
        border-radius: 5%;
        padding: 2px;
        float: right;
    }

    #fakeoption {
        padding: 5px;
        max-height: 170px;
        min-width: 590.667px;
        overflow-y: auto;
        margin: 0 14px 0;
    }

    .col-xs-12 .input {
        margin-bottom: 0.6rem;
    }

    .grid-gerador {
        display: flex;
        float: left;
        width: 100%;
        justify-content: center;
        padding: 0 3%;
    }

    .grid-gerador>div {
        display: block;
        width: 100%;
        margin: 0 5px;
    }

    .respostasGerador {
        max-height: 200px;
        padding: 5px;
        box-shadow: 0 3px 6px -5px;
        overflow: auto;
    }

    @media (max-width: 991px) {
        .grid-gerador {
            flex-direction: column;
        }

        .grid-gerador>div {
            margin: 0;
        }
    }

    @media print {
        .print {
            display: none;
        }

        #newacoes {
            display: none;
        }
    }
</style>
<div class="print">
    <?= $this->element("processandofull"); ?>
    <div id="fundo">
    </div>
    <div class="col-xs-12" style="margin-bottom: 10px;">
        <div class="col-xs-6 col-md-2 col-md-offset-4 centralizada" style="text-align: right">
            <?= $this->Form->button("Criar Tabelas", ['id' => 'nova-tabela', "class" => "btn btn-default col-xs-12", "style" => "font-size: 15px "]) ?>
        </div>
        <div class="col-xs-6 col-md-2" style="padding-left: 5px;">
            <?php
            if (!empty($sessao)) :
                echo $this->Form->button("Tabelas Favoritas", ['id' => 'tabelas-salvas', "class" => "btn btn-default col-xs-12", "style" => " font-size: 15px"]);
            else :
                echo $this->Form->button("Tabelas Favoritas", ["class" => "btn btn-default col-xs-12", "style" => " font-size: 15px", 'data-toggle' => 'modal', 'data-target' => "#modal-login"]);
            endif;
            ?>
        </div>
    </div>


    <?= $this->Form->create("gerador", ["id" => "gerador", 'url' => ['action' => 'pdf']]) ?>
    <div class="grid-gerador fonteReduzida">
        <div>
            <?= $this->Form->input('estado_id', ['id' => 'estado-id', 'options' => $estados, 'default' => $estado, "label" => false, "empty" => "ESTADO", "class" => "fonteReduzida estado-id-gerador"]); ?>
        </div>
        <div id="ramosdisponiveis">
            <?php $ramos = array("SPF" => "SAÚDE - PESSOA FÍSICA", "SPJ" => "SAÚDE - PME", "OPF" => "ODONTO - PESSOA FÍSICA", "OPJ" => "ODONTO - PME") ?>
            <?= $this->Form->input('ramo_id', ['options' => $ramos, "label" => false, "empty" => "RAMO", "class" => "fonteReduzida", "disabled" => "disabled"]); ?>
        </div>
        <div id="ocupacoes">
            <?= $this->Form->input("profissao_id", ["options" => $profissoes, "empty" => "PROFISSÃO", "label" => false, "class" => "fonteReduzida"]); ?>
        </div>
        <div id="op">
            <?= $this->Form->input('operadora_id', ['options' => "", "label" => false, "empty" => "OPERADORA", "disabled" => "disabled", "class" => "fonteReduzida", "class" => "fonteReduzida selectpicker", "data-dropup-auto" => "false"]); ?>
        </div>
        <div class="hide" id="contratacao">
            <?= $this->Form->input('fake', ['options' => '', "label" => false, "empty" => "CONTRATAÇÃO", "disabled" => "disabled"]); ?>
        </div>
        <div id="coparticipacao-combo">
            <?= $this->Form->input('coparticipacao', ['options' => '', "label" => false, "empty" => "TIPO DE COPARTICIPAÇÃO", "disabled" => "disabled", "class" => "fonteReduzida"]); ?>
        </div>
        <div id="vidaspme">
            <?= $this->Form->input('produto_id', ['options' => "", "label" => false, "empty" => "NÚMERO VIDAS", "disabled" => "disabled", "class" => "fonteReduzida"]); ?>
        </div>
    </div>
    <?= $this->Form->hidden('tipo', ['id' => 'tipo', 'value' => 'I']) ?>
    <?= $this->Form->hidden('new', ['id' => 'new', 'value' => '1']) ?>

    <div class="col-xs-12 col-md-10 col-md-offset-1 respostasGerador hide" id="respostaAreasComercializacoes">
    </div>
    <div class="col-xs-12 col-md-10 col-md-offset-1 respostasGerador hide" id="respostaProdutos">
    </div>
    <?= $this->Form->end() ?>
</div>


<div class="col-xs-12" style="height: 10px !important">
    <?= "&nbsp;" ?>
</div>

<div class="container" id="resposta">
</div>
<div class="col-xs-12" id="ss">
</div>
<!-- MODAL TABELAS SALVAS -->
<style>
    .modal-header {
        border-bottom: 0px;
        padding-bottom: 0px;
    }

    #titulo-modal {
        font-size: 14pt;
    }

    #corpo-modal {
        max-height: 500px;
        overflow-y: auto;
        padding-bottom: 10px;
    }
</style>
<?= $this->element('modal-login') ?>
<div class="modal fade" id="modalTabelasSalvas" tabindex="-1" role="dialog" aria-labelledby="modalTabelasSalvasLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="z-index: 9999">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTabelasSalvasLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 1;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row">
                <div class="centralizada" id="titulo-modal">TABELAS FAVORITAS</div>
                <div class="col-xs-12" id="corpo-modal">
                    <div class="clearfix">&nbsp</div>
                    <?php foreach ($tabelas_geradas as $key => $tabela_gerada) : ?>
                        <?php
                        $tabela_gerada = str_replace("_", " ", $tabela_gerada);
                        ?>
                        <div class="col-xs-12 option">
                            <div class="col-xs-11 link" value="<?= $key ?>"><?= $tabela_gerada ?></div>
                            <a href="#" class="col-xs-1 deletar"><i class="fa fa-trash"></i></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ajaxStart(function() {
        $("#pre-loader").show();
    });
    $(document).ajaxComplete(function() {
        $("#pre-loader").hide();
    })

    $(document).ready(function() {
        if ($('#estado-id').val() != "") {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>tabelasGeradas/findRamos/",
                data: $("#estado-id").serialize(),
                success: function(data) {
                    $("#ramosdisponiveis").empty();
                    $("#ramosdisponiveis").append(data);
                    $("#fechar").click();
                }
            });
        }
        $("#myModal").attr("data-backdrop", "false");

        $("#fundo").hide();
        $("#resposta").on("contextmenu", function(e) {
            return false;
        });
        $('#resposta').bind('cut copy paste', function(e) {
            e.preventDefault();
        });
    });


    if ($(window).width() < 960) {
        // 		alert("AQUI");
        $("#tabela-gerada-id").width("80%");
        $("#tabela-gerada-id").css("margin-left", "10%");
    }

    $("#ocupacoes").hide();
    $("#fieldShareEmail").hide();
    $("#shareEmail").click(function() {
        $("#fieldShareEmail").show();
    });
    $("#ramo-id").attr('disabled', true);
    $("#ramo-id option[value='OPE']").attr('disabled', true);

    $("#estado-id").change(function() {
        $("#ramo-id").removeAttr("disabled");
        $("#profissao-id").val("");
        $("#profissao-id").prop("disabled", true);
        $("#ocupacoes").hide();
        $("#operadora-id").val("");
        $("#operadora-id").prop("disabled", true);
        $("#coparticipacao").val("");
        $("#coparticipacao").prop("disabled", true);
        $("#produto-id").val("");
        $("#produto-id").prop("disabled", true);
        $("#vidas").val("");
        $("#vidas").prop("disabled", true);
        $("#2etapa").hide();
        $("#resposta").empty();
        $("#loader").click();
        $("#areas_comercializacoes").addClass("hide");
        $("#contratacao").addClass("hide");

        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>tabelasGeradas/findRamos/",
            data: $("#estado-id").serialize(),
            success: function(data) {
                $("#ramosdisponiveis").empty();
                $("#ramosdisponiveis").append(data);
                $("#fechar").click();
            }
        });
    });

    $("#produto-id").change(function() {
        $("#coparticipacao").val("");
        $("#coparticipacao").prop("disabled", false);
    });
    $("#coparticipacao").change(function() {
        $("#gerarTabela").removeAttr("disabled");
    });

    $("#gerarTabela").click(function() {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>tabelasGeradas/tabela/",
            data: $("#gerador").serialize(),
            success: function(data) {
                $("#resposta").empty();
                $("#resposta").append(data);
                $("#gerarPDF").removeAttr("disabled");

            }
        });
    });


    $("#tabela-gerada-id").change(function() {
        if ($(this).val() != '') {
            $(".modal-backdrop.in").css("opacity", "1");
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>tabelasGeradas/view/",
                data: $("#tabela-gerada-id").serialize(),
                success: function(data) {
                    $("#resposta").empty();
                    $("#resposta").append(data);
                    $("#fechar").click();
                    setTimeout(
                        function() {
                            $("#fecharfull").click();
                            $(".modal-backdrop").hide();
                        },
                        6000);
                }
            });
        }
    });

    $(".link").click(function() {
        let id = $(this).attr("value");
        if (id != '') {
            $.ajax({
                type: "POST",
                url: "<?= $this->request->webroot ?>tabelasGeradas/tabelaSalva",
                data: {
                    "tabela_gerada_id": id
                },
                beforeSend: function() {
                    $("#loader").click()
                },
                success: function(data) {
                    $("#respostaProdutos").empty();
                    $("#resposta").empty();
                    $("#respostaProdutos").append(data);
                    $("#fechar").click()
                    $.ajax({
                        type: "GET",
                        url: "<?= $this->request->webroot ?>tabelasGeradas/produtoSalvo/" + id,
                        crossOrigin: true,
                        success: function(data) {
                            var result = Object.keys(data).map(function(key) {
                                return [data[key]];
                            });
                            result.forEach(function(salvo) {
                                $(".produto").each(function() {
                                    if ($(this).val() == salvo) {
                                        $("#produtos-" + salvo).click();
                                        $("#fechar").click();
                                        $(".modal-backdrop").remove();
                                    }
                                });
                            });
                        }
                    });
                    $(".modal-backdrop").remove();
                    $("#modalTabelasSalvas").modal('hide');
                }
            });
            $.ajax({
                type: "GET",
                url: "<?= $this->request->webroot ?>tabelasGeradas/optionsSalvas/" + id,
                success: function(data) {

                    //PREENCHIMENTO DE RAMOS
                    $("#ramo-id").empty();
                    $("#ramo-id").append("<option value=''>SELECIONE RAMO</option>");
                    $.each(data['ramos'], function(key, value) {
                        if (key == data['selecteds']['ramo']) {
                            $("#ramo-id").append("<option value='" + key + "' selected>" + value + "</option>");
                        } else {
                            $("#ramo-id").append("<option value='" + key + "'>" + value + "</option>");
                        }
                    });

                    //PREENCHIMENTO DE OPERADORAS
                    $("#operadora-id").empty();
                    $("#operadora-id").append("<option value=''>SELECIONE OPERADORA</option>");
                    $.each(data['operadoras'], function(key, value) {
                        if (key == data['selecteds']['operadora']) {
                            $("#operadora-id").append("<option value=" + key + " selected>" + value + "</option>");
                        } else {
                            $("#operadora-id").append("<option value=" + key + ">" + value + "</option>");
                        }
                    });
                    $("#operadora-id").attr("disabled", false);

                    //PREENCHIMENTO DE COPARTICIPACAO
                    $("#coparticipacao").empty();
                    $("#coparticipacao").append("<option value=''>COPARTICIPACAO</option>");
                    $.each(data['coparticipacao'], function(key, value) {
                        if (key == data['selecteds']['coparticipacao']) {
                            $("#coparticipacao").append("<option value=" + key + " selected>" + value + "</option>");
                        } else {
                            $("#coparticipacao").append("<option value=" + key + ">" + value + "</option>");
                        }
                    });
                    $("#coparticipacao").attr("disabled", false);

                    //MOSTRA PROFISSÕES ESCONDE VIDAS
                    if (data['selecteds']['ramo'] == "SPF") {
                        $("#ocupacoes").show();
                        $("#vidaspme").hide();
                        $("#profissao-id").val(data['selecteds']['profissao_id']);
                    } else {
                        $("#ocupacoes").hide();
                        $("#vidaspme").show();
                    }
                    //CASO HAJA VIDA MOSTRA VIDA
                    if (data['vidas'] != 'sem') {
                        //CRIAÇÃO DO ELEMENTO
                        $("#vidaspme").show();
                        $("#vidaspme").empty();
                        $('<select>', {
                            id: 'vidas',
                            name: "vidas",
                            class: 'fonteReduzida'
                        }).appendTo('#vidaspme');

                        //ADIÇÃO DE EVENTO
                        $("#vidas").change(function() {
                            $("#loader").click();
                            $.ajax({
                                type: "POST",
                                url: baseUrl + "tabelasGeradas/geradorProdutos/",
                                data: $("#gerador").serialize(),
                                success: function(data) {
                                    $("#resposta").empty();
                                    $("#respostaProdutos").empty();
                                    $("#respostaProdutos").append(data);
                                    $("#fechar").click();

                                }
                            });
                        });
                        //PREENCHIMENTO DE COPARTICIPACAO
                        $("#vidas").empty();
                        $("#vidas").append("<option value=''>NÚMERO DE VIDAS</option>");
                        $.each(data['vidas'], function(key, value) {
                            if (data['selecteds']['vidas'] == key) {
                                $("#vidas").append("<option value=" + key + " selected>" + value + "</option>");
                            } else {
                                $("#vidas").append("<option value=" + key + ">" + value + "</option>");
                            }
                        });
                        $("#vidas").attr("disabled", false);
                    }


                    //EVENTOS

                    //EVENTO NO CHANGE SAUDE
                    $("#operadora-id").change(function() {
                        $("#vidas").val("");
                        $("#vidas").prop("disabled", true);
                        $("#2etapa").hide();
                        $("#resposta").empty();

                        $("#loader").click();
                        $("#coparticipacao").prop("disabled", false);
                        $("#coparticipacao").val("");

                        $.ajax({
                            type: "POST",
                            url: baseUrl + "tabelasGeradas/coparticipacao/" + $("#estado-id").val() + "/" + $("#ramo-id").val() + "/" + $("#operadora-id").val(),
                            success: function(data) {
                                $("#coparticipacao-combo").empty();
                                $("#coparticipacao-combo").append(data);
                                $("#fechar").click();
                            }
                        });
                    });

                    //EVENTO NO CHANGE DE COPARTICIPAÇÃO
                    $("#coparticipacao").change(function() {
                        $("#resposta").empty();
                        $("#respostaProdutos").empty();
                        $("#loader").click();
                        if ($("#ramo-id").val() == 'SPJ' || $("#ramo-id").val() == 'OPJ') {
                            $.ajax({
                                type: "POST",
                                url: baseUrl + "tabelasGeradas/geradorVidas/" + $("#ramo-id").val() + "/" + $("#operadora-id").val(),
                                data: $("#estado-id").serialize(),
                                success: function(data) {
                                    $("#vidaspme").empty();
                                    $("#vidaspme").append(data);
                                    $(".modal-backdrop").remove();
                                    $("#fechar").click();
                                }
                            });

                        } else {
                            $.ajax({
                                type: "POST",
                                url: baseUrl + "tabelasGeradas/geradorProdutos/",
                                data: $("#gerador").serialize(),
                                success: function(data) {
                                    $("#respostaProdutos").empty();
                                    $("#respostaProdutos").append(data);
                                    $(".modal-backdrop").remove();
                                    $("#fechar").click();

                                }
                            });
                        }
                    });

                    //EVENTO NO CHANGE DE VIDAS
                    $("#vidas").change(function() {
                        // $("#loader").click();

                        // // 	$("#coparticipacao").prop("disabled", false);
                        // $.ajax({
                        //     type: "POST",
                        //     url: baseUrl + "tabelasGeradas/geradorProdutos/",
                        //     data: $("#gerador").serialize(),
                        //     success: function(data) {
                        //         $("#resposta").empty();
                        //         $("#respostaProdutos").empty();
                        //         $("#respostaProdutos").append(data);
                        //         $("#fechar").click();

                        //     }
                        // });
                    });
                    //EVENTO NO CHANGE DE PRODUTOS


                }
            });
        }
    });
    $(".deletar").click(function() {
        let id = $(this).siblings(".link").attr("value");
        $.ajax({
            type: "POST",
            url: baseUrl + "tabelasGeradas/delete/" + id,
            beforeSend: function() {
                $("#loader").click()
            },
            success: function(data) {
                $.ajax({
                    type: "POST",
                    url: baseUrl + "tabelasGeradas/atualizarTabelas/",
                    success: function(data) {
                        $("#corpo-modal").empty();
                        $("#corpo-modal").append(data);
                        $("#modalProcessando").modal("hide");
                    }
                });
            }
        });
    });
    $("#tabela-salva").change(function() {
        if ($(this).val() != '') {
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelasGeradas/tabelaSalva",
                data: $("#tabela-salva").serialize(),
                beforeSend: function() {
                    $("#loader").click()
                },
                success: function(data) {
                    $("#respostaProdutos").empty();
                    $("#resposta").empty();
                    $("#respostaProdutos").append(data);
                    $("#fechar").click()
                }
            });
        }
    });
    $("#tabelas-salvas").click(function() {
        $("#modalTabelasSalvas").modal("show");
    })
    $("#nova-tabela").click(function() {
        $("#respostaProdutos").empty();
        $("#resposta").empty();
        $("#ocupacoes").hide();
        $("#vidaspme").show();
        $("#estado-id").change();
    });
</script>