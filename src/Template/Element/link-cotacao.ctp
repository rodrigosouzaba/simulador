<div class="col-xs-12 centralizada">
    <div class="col-md-12 centralizada" style=" font-weight: bold">
        ESCOLHA O QUE DESEJA EXIBIR
    </div>
    <div class="col-md-2 col-md-offset-1">
        <?= $this->Form->input('observacao', ['type' => 'checkbox', 'label' => 'Observações Importantes', 'checked' => 'checked']); ?>
    </div>
    <div class="col-md-2">
        <?= $this->Form->input('rede', ['type' => 'checkbox', 'label' => 'Rede Credenciada (Resumo)', 'checked' => 'checked']); ?>
    </div>
    <div class="col-md-2">
        <?= $this->Form->input('reembolso', ['type' => 'checkbox', 'label' => 'Coberturas e Diferenciais', 'checked' => 'checked']); ?>
    </div>
    <div class="col-md-2">
        <?= $this->Form->input('carencia', ['type' => 'checkbox', 'label' => 'Carência (Resumo)', 'checked' => 'checked']); ?>
    </div>
    <div class="col-md-2">
        <?= $this->Form->input('documento', ['type' => 'checkbox', 'label' => 'Documentos Necessários', 'checked' => 'checked']); ?>
    </div>
    <div class="col-xs-12" data-toggle="tooltip" data-placement="top" title="É necessário selecionar uma opção de cálculo" >
        <button class="btn btn-danger btn-lg" id="btnVisualizarCotacao" style="margin-top: 15px;">Visualizar Cotações</button>
    </div>
</div>
