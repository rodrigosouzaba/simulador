<?php
if ($operadora['status'] != 'OCULTA' && $operadora['status'] != 'EM ATUALIZAÇÃO') :
?>
    <?php if (isset($tabelasOrdenadas) && $tabelasOrdenadas != '') {
    ?>
        <div class="col-xs-12" style="background-color: #004057; color: #ffffff;  padding: 5px 0; text-align: center; font-weight: bold; ">
            <?php
            switch ($link["ramo"]) {
                case "OPF":
                    $tipo_pessoa = "Pessoa Física";
                    break;
                case "OPJ":
                    $tipo_pessoa = "Pessoa Jurídica";
                    break;
            } ?>
            <?= $operadora['nome'] . ' ' . $operadora['detalhe'] . " - " . $tipo_pessoa ?>
        </div>


        <table style="width: 100%; border-collapse:collapse;" class="table table-condensed table-striped table-bordered">
            <tr>
                <td width="110" style="border: solid 0.5px #ddd;"></td>
                <?php
                foreach ($tabelasOrdenadas as $produto => $tipos_produto) {
                    foreach ($tipos_produto as $tipo_produto => $acomodacoes) {
                ?>
                        <td style="text-align: center;border: solid 0.5px #ddd; font-weight: bold; " colspan="<?= array_sum(array_map("count", $acomodacoes)); ?>">
                            <?= $tipo_produto; ?>
                        </td>
                <?php
                    }
                } ?>
            </tr>

            <!-- TIPO PRODUTO -->
            <tr>
                <td width="80" style="border: solid 0.5px #ddd;"></td>
                <?php
                $x = 0;

                foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                    foreach ($tipo_produto as $acomodacoes) {
                        foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                            foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                ?>
                                <!-- <td class="centralizada titulo-tabela"> -->
                                <td style="border: solid 0.5px #ddd;text-align: center; vertical-align: middle; font-size: 80%;">
                                    <?php
                                    $c = explode('-', $coparticipacao); ?>
                                    <?= $tabela['nome']; ?><br>
                                    <small style="font-weight: 100 !important">
                                        <?php
                                        $x++;
                                        if ($this->request->data('ramo_id') === 'SPJ') {
                                            $i = 1;
                                            foreach ($tabela['tabelas_cnpjs'] as $cnpj) {
                                                // 							    debug($cnpj);die();
                                                if ($i < count($tabela['tabelas_cnpjs'])) {
                                                    echo $cnpj['cnpj']['nome'] . ',';
                                                } else {
                                                    echo $cnpj['cnpj']['nome'] . '<br>';
                                                }
                                                ++$i;
                                            }
                                        }
                                        if ($tabela['cod_ans']) {
                                            echo 'Cód. ANS: ' . $tabela['cod_ans'];
                                        } ?>
                                    </small>
                                </td>
                <?php
                            }
                        }
                    }
                } ?>
            </tr>
            <!-- /TIPO PRODUTO -->
            <tr>
                <td width="80" style="border: solid 0.5px #ddd;">Preço p/ vida</td>
                <?php
                if ($totalTabelas >= 5) {
                    $medida = 626;
                } else {
                    $medida = 630;
                }

                foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                    foreach ($tipo_produto as $acomodacoes) {
                        foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                            foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                ?>
                                <td style="border: solid 0.5px #ddd;" class="centralizada" width="<?= $medida / $x ?>">
                                    <?= $tabela['preco_vida'] == 0 ? "..." : $this->Number->format($tabela['preco_vida'], ['before' => 'R$ ', 'places' => 2]); ?>
                                </td>
                <?php
                            }
                        }
                    }
                } ?>
            </tr>
        </table>
        <div class="clearfix">&nbsp;</div>
    <?php
    }
    ?>
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            OBSERVAÇÕES IMPORTANTES
        </div>
        <div class="corpo-gerador-tabelas">
            <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
            <?php
            foreach ($operadora["odonto_observacaos"] as $obs) {
                echo ($obs['descricao'] != null) ? nl2br($obs['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            REDE CREDENCIADA<small> (Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora["odonto_redes"] as $rede) {
                echo ($rede['descricao'] != null) ? nl2br($rede['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora['odonto_reembolsos'] as $diferenciais) {
                echo ($diferenciais['descricao'] != null) ? nl2br($diferenciais['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
        </div>
        <div class="corpo-gerador-tabelas">
            <div>
                <b>ÁREA DE COMERCIALIZAÇÃO:</b>
            </div>
            <div>
                <?php
                foreach ($operadora['odonto_comercializacoes'] as $comercializacao) {
                    echo ($comercializacao['descricao'] != null) ? nl2br($comercializacao['descricao']) : "<div class='info'>Consulte Operadora</div>";
                }
                ?>
            </div>
            <div>&nbsp;</div>
            <div>
                <b>ÁREA DE ATENDIMENTO</b>
            </div>
            <?php
            $dadosT = array();

            foreach ($link['odonto_tabelas'] as $tabela) {
                $dadosT[$tabela['odonto_produto']['nome']] = $tabela["odonto_atendimento"]['descricao'];
            }
            foreach ($dadosT as $produto => $atendimento) {
                echo '<div><b>' . $produto . ':</b>' . ' ' . $atendimento . '</div>';
            }
            ?>
        </div>
    </div>

    <div>&nbsp;</div>
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            CARÊNCIAS <small>(Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora['odonto_carencias'] as $carencias) {
                echo ($carencias['descricao'] != null) ? nl2br($carencias['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            DEPENDENTES
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora['odonto_dependentes'] as $dependentes) {
                echo ($dependentes['descricao'] != null) ? nl2br($dependentes['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="nobreak">

        <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            DOCUMENTOS NECESSÁRIOS
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora['odonto_documentos'] as $documentos) {
                echo ($documentos['descricao'] != null) ? nl2br($documentos['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            FORMAS DE PAGAMENTOS
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora['odonto_formas_pagamentos'] as $pgto) {
                echo ($pgto['descricao'] != null) ? nl2br($pgto['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
<?php
endif;
?>