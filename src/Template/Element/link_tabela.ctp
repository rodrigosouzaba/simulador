<?php
if ($operadora['status'] != 'OCULTA' && $operadora['status'] != 'EM ATUALIZAÇÃO') :
?>
    <?php if (isset($tabelasOrdenadas) && $tabelasOrdenadas != '') {
    ?>
        <div class="col-xs-12 tabela-produto">
            <?= $dadosOperadora = $operadora['nome'] . ' ' . $operadora['detalhe'] . ' ' . $vidas["min"] . ' A ' . $vidas["max"] . ' VIDAS' ?>
        </div>
        <?php
        // debug($tabelasOrdenadas);
        // die;
        foreach ($tabelasOrdenadas as $tabelasOrdenadas) {
        ?>
            <table style="width: 100%; border-collapse:collapse;" class="col-xs-12 table table-condensed table-striped table-bordered">
                <tr>
                    <td width="100" style="border: solid 0.5px #ddd;"></td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $tipo_produto => $acomodacao) {
                    ?>
                            <td style="text-align: center;border: solid 0.5px #ddd; font-weight: bold; " colspan="<?= array_sum(array_map("count", $acomodacao)); ?>">
                                <?= $produto; ?>
                            </td>
                    <?php
                        }
                    } ?>
                </tr>

                <!-- TIPO PRODUTO -->
                <tr>
                    <td width="110" style="border: solid 0.5px #ddd;"></td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $tipos_produto) {
                        foreach ($tipos_produto as $tipo_produto => $acomodacoes) {
                            $text = '';
                            $t = explode("(", $tipo_produto);
                            if (isset($t[1])) {
                                $text =  rtrim($t[0]) . "<br>(" . rtrim($t[1]);
                            } else {
                                $text = rtrim($t[0]);
                            } ?>
                            <td style="font-size: 80%; border: solid 0.5px #ddd; text-align: center;" colspan="<?= array_sum(array_map("count", $acomodacoes)) ?>">
                                <span style="background-color: #FFF"><?= $text ?></span>
                            </td>
                    <?php
                        }
                    } ?>
                </tr>
                <!-- /TIPO PRODUTO -->

                <!-- ACOMODAÇÃO -->
                <tr>
                    <td width="100" style="border: solid 0.5px #ddd;"></td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as $acomodacao => $co) {
                    ?>
                                <td style="border: solid 0.5px #ddd;text-align: center; vertical-align: middle; font-size: 80%;" colspan="<?= count($co, COUNT_RECURSIVE) ?>">
                                    <strong><?= ($acomodacao == 'AMBULATORIAL' ? 'SEM ACOMODAÇÃO' : $acomodacao); ?></strong>
                                </td>
                    <?php
                            }
                        }
                    } ?>
                </tr>
                <!-- /ACOMODAÇÃO -->

                <tr>
                    <td width="100" style="border: solid 0.5px #ddd;"></td>
                    <?php
                    $x = 0;

                    foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd; font-size: 75%; text-align: center;">
                                        <?php
                                        $x++;
                                        $c = explode('-', $coparticipacao); ?>
                                        <b><?= $tabela['nome']; ?></b><br>
                                        <?= substr($c[1], 0, 3) != "SEM" ? "COM " . $c[1] : $c[1] ?><br>
                                        <small>
                                            <?php

                                            $i = 1;
                                            foreach ($tabela['tabelas_cnpjs'] as $cnpj) {
                                                if ($i < count($tabela['tabelas_cnpjs'])) {
                                                    echo $cnpj['cnpj']['nome'] . ',';
                                                } else {
                                                    echo $cnpj['cnpj']['nome'] . '<br>';
                                                }
                                                ++$i;
                                            }

                                            if ($tabela['cod_ans']) {
                                                echo 'Cód. ANS: ' . $tabela['cod_ans'];
                                            } ?>
                                        </small>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">até 18 anos</td>
                    <?php
                    if ($totalTabelas >= 5) {
                        $medida = 607;
                    } else {
                        $medida = 617;
                    }

                    foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada" width="<?= $medida / $x ?>">
                                        <?= $tabela['check1'] == 1 ? "..." : $this->Number->format($tabela['faixa1'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">19 a 23 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada">
                                        <?= $tabela['check2'] == 1 ? "..." : $this->Number->format($tabela['faixa2'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">24 a 28 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada">
                                        <?= $tabela['check3'] == 1 ? "..." : $this->Number->format($tabela['faixa3'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">29 a 33 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada">
                                        <?= $tabela['check4'] == 1 ? "..." : $this->Number->format($tabela['faixa4'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">34 a 38 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada">
                                        <?= $tabela['check5'] == 1 ? "..." : $this->Number->format($tabela['faixa5'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">39 a 43 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada">
                                        <?= $tabela['check6'] == 1 ? "..." : $this->Number->format($tabela['faixa6'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">44 a 48 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada">
                                        <?= $tabela['check7'] == 1 ? "..." : $this->Number->format($tabela['faixa7'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">49 a 53 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada">
                                        <?= $tabela['check8'] == 1 ? "..." : $this->Number->format($tabela['faixa8'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">54 a 58 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada">
                                        <?= $tabela['check9'] == 1 ? "..." : $this->Number->format($tabela['faixa9'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="border: solid 0.5px #ddd;" class="precos" width="80">59 ou + anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td style="border: solid 0.5px #ddd;" class="centralizada">
                                        <?= $tabela['check10'] == 1 ? "..." : $this->Number->format($tabela['faixa10'], ['before' => 'R$ ', 'places' => 2]); ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>


            </table>
            <div></div>

    <?php
        }
    }

    $forma_pagamentos = 'formas_pagamentos';
    $obsD = 'observacoes';
    $dependentesD = 'opcionais';
    $redes = 'redes';
    $reembolsos = 'reembolsos';
    $carenciasD = 'carencias';
    $comercializacoes = 'regioes';
    $atendimentos = 'abrangencia';
    $documentosD = 'informacoes';
    $produtosD = 'produto';
    ?>
    <div class="nobreak">
        <div class="col-xs-12 tabela-topico">
            OBSERVAÇÕES IMPORTANTES
        </div>
        <div class="corpo-gerador-tabelas">
            <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$obsD] as $obs) {
                echo ($obs['descricao'] != null) ? nl2br($obs['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>
    <div class="nobreak">
        <div class="col-xs-12 tabela-topico">
            REDE REFERENCIADA<small> (Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$redes] as $rede) {
                echo ($rede['descricao'] != null) ? nl2br($rede['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>

    <div class="nobreak">
        <div class="col-xs-12 tabela-topico">
            OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$reembolsos] as $diferenciais) {
                echo ($diferenciais['descricao'] != null) ? nl2br($diferenciais['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>

    <div class="nobreak">
        <div class="col-xs-12 tabela-topico">
            ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
        </div>
        <div class="corpo-gerador-tabelas">
            <div>
                <b>ÁREA DE COMERCIALIZAÇÃO:</b>
            </div>
            <div>
                <?php
                foreach ($operadora[$comercializacoes] as $comercializacao) {
                    echo ($comercializacao['descricao'] != null) ? nl2br($comercializacao['descricao']) : "<div class='info'>Consulte Operadora</div>";
                }
                ?>
            </div>
            <div>&nbsp;</div>
            <div>
                <b>ÁREA DE ATENDIMENTO</b>
            </div>
            <?php
            $dadosT = array();

            foreach ($link['tabelas'] as $tabela) {
                $dadosT[$tabela['produto']['nome']] = $tabela[$atendimentos]['descricao'];
            }
            foreach ($dadosT as $produto => $atendimento) {
                echo '<div><b>' . $produto . ':</b>' . ' ' . $atendimento . '</div>';
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>

    <div class="nobreak">
        <div class="col-xs-12 tabela-topico">
            CARÊNCIAS <small>(Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$carenciasD] as $carencias) {
                echo ($carencias['descricao'] != null) ? nl2br($carencias['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>

    <div class="nobreak">
        <div class="col-xs-12 tabela-topico">
            DEPENDENTES
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$dependentesD] as $dependentes) {
                echo ($dependentes['descricao'] != null) ? nl2br($dependentes['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>

    <div class="nobreak">
        <div class="col-xs-12 tabela-topico">
            DOCUMENTOS NECESSÁRIOS
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$documentosD] as $documentos) {
                echo ($documentos['descricao'] != null) ? nl2br($documentos['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div>&nbsp;</div>

    <div class="nobreak">
        <div class="col-xs-12 tabela-topico">
            FORMAS DE PAGAMENTOS
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$forma_pagamentos] as $pgto) {
                echo ($pgto['descricao'] != null) ? nl2br($pgto['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
<?php
endif;
?>