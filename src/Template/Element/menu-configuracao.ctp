<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');

$controller = $this->request->controller;
$action = $this->request->action;
$actionsBloqueadas = array("lojas", "calculos", "central", "gerenciador", "gerador", "gerador-auto", "saude", "odonto", "gerador-tabelas", "smsmarketing");
$controllersBloqueados = array("Avaliacaos", "Lojas", "Simulacoes", "PfCalculos", "OdontoCalculos", "Agendamentos", "NotasCompletas", "NotasUnicas", "Banners", "BannerImagens", "VendasOnlines");

$controllersPF = ['PfAreasComercializacoes', 'PfOperadoras', 'PfProdutos', 'PfCoberturas', 'PfTabelas', 'PfEntidades', 'PfProfissoes', 'PfObservacoes', 'PfRedes', 'PfReembolsos', 'PfComercializacoes', 'PfComercializacoesMunicipios', 'PfCarencias', 'PfDependentes', 'PfDocumentos', 'PfFormasPagamentos'];
$controllersPME = ['AreasComercializacoes', 'Operadoras', 'Produtos', 'Coberturas', 'Tabelas', 'Observacoes', 'Redes', 'Reembolsos', 'Regioes', 'RegioesMunicipios', 'Carencias', 'Opcionais', 'Informacoes', 'FormasPagamentos'];
$controllersOdonto = ['OdontoOperadoras', 'OdontoCoberturas', 'OdontoTabelas', 'OdontoAtendimentos', 'OdontoCarencias', 'OdontoObservacaos', 'OdontoRedes', 'OdontoReembolsos', 'OdontoDependentes', 'OdontoDocumentos', 'OdontoFormasPagamentos', 'OdontoProdutos'];
if ($sessao["role"] === 'admin' && !in_array($action, $actionsBloqueadas) && !in_array($controller, $controllersBloqueados)) : ?>

    <div class="col-xs-12 text-center sub" style="margin-bottom: 6px;" id="menu-configuracao">
        <?php
        //CONTROLLERS DO MODULO SAUDE PME
        //SEQUENCIA: Operadoa, Produto, Tabela
        $array_saude_pme = array('AreasComercializacoes', 'Operadoras', "Tabelas", "Produtos", "Carencias", "FormasPagamentos", "Informacoes", "Observacoes", "Redes", "Reembolsos", "Regioes", "Tipos", "Abrangencias", "Opcionais");

        //CONTROLLERS DO MODULO SAUDE PF
        $array_saude_pf = array('PfAreasComercializacoes', 'PfOperadoras', "PfTabelas", "PfProdutos", "PfCarencias", "PfAcomodacoes", "PfAtendimentos", "PfCarencias", "PfComercializacoes", "PfDependentes", "PfDocumentos", "PfEntidades", "PfProfissoes", "PfFormasPagamentos",    "PfRedes", "PfReembolsos");

        //CONTROLLERS DO ODONTO
        $array_odonto = array('OdontoTabelas', "OdontoOperadoras", "OdontoProdutos", "OdontoComercializacoes", "OdontoAtendimentos", "OdontoCarencias", "OdontoDependentes", "OdontoDocumentos", "OdontoObservacaos", "OdontoRedes", "OdontoFormasPagamentos", "OdontoReembolsos");

        //CONTROLLERS DO MODULO USUARIOS
        $array_usuarios = array('Users', "Indicados", "Smsenviados");

        //CONTROLLERS DO MODULO GERADOR DE TABELAS
        $array_tabelas = array('TabelasGeradas', "Links");

        if (in_array($controller, $controllersPME) || $action === 'configurarPme') { ?>
            <div class="modulo">Configurar Saúde PME</div>
            <div class="btn-group centralizada" role="group">
                <?= $this->Html->link(__('Operadoras'), ["controller" => 'Operadoras', "action" => "new"], ["class" => ($controller == 'Operadoras' && $action = "new") ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Operadoras'), ["controller" => 'Operadoras', "action" => "index"], ["class" => ($controller == 'Operadoras' && $action = "index") ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Áreas Comercialização'), ["controller" => "AreasComercializacoes", "action" => "index"], ["class" =>  $controller == 'AreasComercializacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-success linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Coberturas'), ["controller" => "Coberturas", "action" => "index"], ["class" =>  $controller == 'Coberturas' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Produtos e Preços'), ["controller" => "Tabelas", "action" => "index"], ["class" =>  $controller == 'Tabelas' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Observacões'), ["controller" => "Observacoes", "action" => "index"], ["class" =>  $controller == 'Observacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Redes'), ["controller" => "Redes", "action" => "index"], ["class" =>  $controller == 'Redes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Reembolsos'), ["controller" => "Reembolsos", "action" => "index"], ["class" =>  $controller == 'Reembolsos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Carências'), ["controller" => "Carencias", "action" => "index"], ["class" =>  $controller == 'Carencias' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Dependentes'), ["controller" => "Opcionais", "action" => "index"], ["class" =>  $controller == 'Opcionais' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Documentos'), ["controller" => "Informacoes", "action" => "index"], ["class" =>  $controller == 'Informacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Pagamentos'), ["controller" => "FormasPagamentos", "action" => "index"], ["class" =>  $controller == 'FormasPagamentos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Áreas Comercialização'), ["controller" => "Regioes", "action" => "index"], ["class" =>  $controller == 'Regioes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Produtos'), ["controller" => "Produtos", "action" => "index"], ["class" =>  $controller == 'Produtos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button"]) ?>
            </div>

        <?php } ?>

        <?php if (in_array($controller, $array_tabelas)) { ?>
            <div class="modulo">Configurar Tabelas</div>
            <div class="btn-group centralizada" role="group">
                <?= $this->Html->link(__('Configurar Tabelas'), ['controller' => 'TabelasGeradas', 'action' => 'gerador-tabelas'], ["class" => "btn btn-sm btn-default", "type" => "button"]) ?>
                <?= $this->Html->link(__('Links de Tabelas'), ['controller' => 'Links', 'action' => 'index'], ["class" => "btn btn-sm btn-default", "type" => "button"]) ?>
            </div>

        <?php }
        if (in_array($controller, $controllersPF) || $action == 'configurarPf') { ?>
            <div class="modulo">Configurar Saúde PF</div>
            <div class="btn-group" role="group">
                <?= $this->Html->link(__('Operadoras'), ["controller" => 'PfOperadoras', 'action' => 'new'], ["class" =>  $controller == 'PfOperadoras' && $action == 'new' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'id' => 'inicial']) ?>
                <?= $this->Html->link(__('Operadoras'), ["controller" => 'PfOperadoras', 'action' => 'index'], ["class" =>  $controller == 'PfOperadoras' && $action == 'index' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button", 'id' => 'inicial']) ?>
                <?= $this->Html->link(__('Áreas Comercialização'), ["controller" => "PfAreasComercializacoes", "action" => "index"], ["class" =>  $controller == 'PfAreasComercializacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-success linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Coberturas'), ["controller" => 'PfCoberturas', 'action' => 'index'], ["class" =>  $controller == 'PfCoberturas' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Produtos e Preços'), ["controller" => 'PfTabelas', 'action' => 'index'], ["class" =>  $controller == 'PfTabelas' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Entidades'), ["controller" => 'PfEntidades', 'action' => 'index'], ["class" =>  $controller == 'PfEntidades' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Profissões'), ["controller" => 'PfProfissoes', 'action' => 'index'], ["class" =>  $controller == 'PfProfissoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Observações'), ["controller" => 'PfObservacoes', 'action' => 'index'], ["class" =>  $controller == 'PfObservacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Redes'), ["controller" => 'PfRedes', 'action' => 'index'], ["class" =>  $controller == 'PfRedes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Reembolsos'), ["controller" => 'PfReembolsos', 'action' => 'index'], ["class" =>  $controller == 'PfReembolsos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Carências'), ["controller" => 'PfCarencias', 'action' => 'index'], ["class" =>  $controller == 'PfCarencias' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Dependentes'), ["controller" => 'PfDependentes', 'action' => 'index'], ["class" =>  $controller == 'PfDependentes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Documentos'), ["controller" => 'PfDocumentos', 'action' => 'index'], ["class" =>  $controller == 'PfDocumentos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Pagamentos'), ["controller" => 'PfFormasPagamentos', 'action' => 'index'], ["class" =>  $controller == 'PfFormasPagamentos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Áreas Comercialização'), ["controller" => 'PfComercializacoes', 'action' => 'index'], ["class" =>  $controller == 'PfComercializacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button"]) ?>
                <?= $this->Html->link(__('Produtos'), ["controller" => 'PfProdutos', 'action' => 'index'], ["class" =>  $controller == 'PfProdutos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button"]) ?>
            </div>
        <?php }

        if (in_array($controller, $controllersOdonto) || $action === 'configurarOdonto') { ?>

            <div class="modulo">Configurar Odonto</div>
            <div class="btn-group" role="group">
                <?= $this->Html->link(__('Operadoras'), ['controller' => 'OdontoOperadoras', 'action' => 'new'], ["class" => $controller == 'OdontoOperadoras' && $action == 'new' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Operadoras'), ['controller' => 'OdontoOperadoras', 'action' => 'index'], ["class" => $controller == 'OdontoOperadoras' && $action == 'index' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Coberturas'), ['controller' => 'OdontoCoberturas'], ["class" => $controller == 'OdontoCoberturas' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Produtos e Preços'), ['controller' => 'OdontoTabelas'], ["class" => $controller == 'OdontoTabelas' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Áreas Atendimentos'), ['controller' => 'OdontoAtendimentos'], ["class" => $controller == 'OdontoAtendimentos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Carências'), ['controller' => 'OdontoCarencias'], ["class" => $controller == 'OdontoCarencias' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Observações'), ['controller' => 'OdontoObservacaos'], ["class" => $controller == 'OdontoObservacaos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Redes'), ['controller' => 'OdontoRedes'], ["class" => $controller == 'OdontoRedes' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Reembolsos'), ['controller' => 'OdontoReembolsos'], ["class" => $controller == 'OdontoReembolsos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Dependentes'), ['controller' => 'OdontoDependentes'], ["class" => $controller == 'OdontoDependentes' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Documentos'), ['controller' => 'OdontoDocumentos'], ["class" => $controller == 'OdontoDocumentos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Pagamentos'), ['controller' => 'OdontoFormasPagamentos'], ["class" => $controller == 'OdontoFormasPagamentos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
                <?= $this->Html->link(__('Produtos'), ['controller' => 'OdontoProdutos'], ["class" => $controller == 'OdontoProdutos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>
            </div>
            <?= $this->Html->link(__('Áreas Comercialização'), '#', ['controller' => 'OdontoComercializacoes', "class" => "btn btn-sm btn-default linkAjax", "type" => "button"]) ?>

        <?php }

        /*  if (in_array($controller, $array_usuarios) && $action <> 'configurarPf' && $action <> 'configurarPme' && $action <> 'configurarOdonto') { ?>
        <div class="btn-group" role="group">
            <?= $this->Html->link(__('Listar'), ['controller' => 'Users', 'action' => 'index'], ["class" => "btn btn-sm btn-default", "type" => "button"]) ?>
            <?= $this->Html->link(__('Mailchimp'), ['controller' => 'Users', 'action' => 'mailchimp-data'], ["class" => "btn btn-sm btn-default", "type" => "button"]) ?>
            <?= $this->Html->link(__('Logotipos'), ['controller' => 'Users', 'action' => 'logotipos'], ["class" => "btn btn-sm btn-default", "type" => "button"]) ?>
            <?= $this->Html->link(__('Indicações'), ['controller' => 'Indicados', 'action' => 'index'], ["class" => "btn btn-sm btn-default", "type" => "button"]) ?>
            <?= $this->Html->link(__('SMS Enviados'), ['controller' => 'Smsenviados', 'action' => 'index'], ["class" => "btn btn-sm btn-default", "type" => "button"]) ?>
        </div>

        <?php
        }
 */
        ?>
        <?php if (0 == 1 && $controller <> "Users" && !in_array($controller, $array_tabelas) && !in_array($controller, $array_odonto) && !in_array($action, $actionsBloqueadas) && !in_array($controller, $array_tabelas) && !in_array($controller, $array_usuarios)) { ?>
            <div class="btn-group centralizada" role="group">
                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fas fa-cog', 'aria-hidden' => 'true']), "#", ['role' => 'button', 'escape' => false, "class" => "dropdown-toggle btn btn-default btn-sm core", "data-toggle" => "dropdown"]) ?>
                <ul class="dropdown-menu multi-level sub pull-right">
                    <li><?= $this->Html->link("Ramos", ["controller" => "Ramos", "action" => "index"], ["role" => "button"]) ?>
                    </li>
                    <li class="divider"></li>
                    <li><?= $this->Html->link("Modalidades", ["controller" => "Modalidades", "action" => "index"], ["role" => "button"]) ?>
                    </li>
                    <?php
                    if (in_array($controller, $array_saude_pme)) {
                        $atendimento = "Abrangencias";
                    }
                    if (in_array($controller, $array_saude_pf)) {
                        $atendimento = "PfAtendimentos";
                    }
                    ?>
                    <li class="divider"></li>
                    <li><?= $this->Html->link("Áreas de Atendimento", ["controller" => $atendimento, "action" => "index"], ["role" => "button"]) ?>
                    </li>
                    <li class="divider"></li>

                    <li><?= $this->Html->link("Tipos de Acomodação", ["controller" => "Tipos", "action" => "index"], ["role" => "button"]) ?>
                    </li>
                    <?php
                    if (in_array($controller, $array_saude_pf) || in_array($controller, $array_saude_pme)) {
                        echo "<li class='divider'></li><li>" . $this->Html->link("Tipos de Contratação", ["controller" => "TiposProdutos", "action" => "index"], ["role" => "button"]) . "</li>";
                    }
                    ?>
                </ul>
            </div>

        <?php
        }
        ?>
    </div>
    <div class="clearfix">&nbsp</div>
<?php endif; ?>