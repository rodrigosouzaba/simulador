<?php
$opcaoLink = ''; 

if(empty($dadosSessao)){
    $optionTarget = '_blank';
}

?>

<style>
    #menu {
        width: 100%;
        background: #f0f0f0;
        border: 1px solid #ccc;
        border-right: none;

    }

    #menu ul {
        overflow: hidden;
        margin: 0;
        padding: 0;
    }

    #menu ul li {
        list-style: none;
        float: left;
        text-align: center;
        border-left: 1px solid #fff;
        border-right: 1px solid #ccc;
        width: 14.2857142857%;
        /* fallback for non-calc() browsers */
        width: calc(100% / 7);
        box-sizing: border-box;
    }

    #menu ul li:first-child {
        border-left: none;
    }

    .navbar-default .navbar-nav>li>a {
        color: #333 !important;
    }

    #menu ul li a {
        display: block;
        text-decoration: none;
        color: #616161;
        padding: 10px 0;
    }

    .p {
        text-align: center;
        font-size: 14px;
        margin-top: 80px;
    }


    .container-menu {
        padding-left: 0;
        padding-right: 0;
    }

    .dropdown-submenu {
        position: relative;
    }

    .dropdown-submenu>.dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -6px;
        margin-left: -1px;
        -webkit-border-radius: 0 6px 6px 6px;
        -moz-border-radius: 0 6px 6px;
        border-radius: 0 6px 6px 6px;
    }

    .dropdown-submenu:hover>.dropdown-menu {
        display: block;
    }

    .dropdown-submenu>a:after {
        display: block;
        content: " ";
        float: right;
        width: 0;
        height: 0;
        border-color: transparent;
        border-style: solid;
        border-width: 5px 0 5px 5px;
        border-left-color: #ccc;
        margin-top: 5px;
        margin-right: -10px;
    }

    .dropdown-submenu:hover>a:after {
        border-left-color: #fff;
    }

    .dropdown-submenu.pull-left {
        float: none;
    }

    .dropdown-submenu.pull-left>.dropdown-menu {
        left: -100%;
        margin-left: 10px;
        -webkit-border-radius: 6px 0 6px 6px;
        -moz-border-radius: 6px 0 6px 6px;
        border-radius: 6px 0 6px 6px;
    }

    .disabled {
        /* 	color: #333 !important; */
    }

    #menu-fixado {
        position: fixed;
        z-index: 999;
    }

    #getAvaliacao {
        margin: 0;
    }

    .dropdown-submenu {
        position: relative;
    }

    .dropdown-submenu .dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -1px;
    }

    #submenu ul {
        list-style: none;
        margin-left: 0px;
        background-color: #f5f5f5;
    }

    #submenu ul li {
        text-align: left;
        padding-left: 35px;
    }

    #submenu ul li:hover {
        background-color: #fff;
    }

    #submenu ul li a {
        text-decoration: none;
        color: #333;
    }

    .menu-item:hover {
        background-color: #e7e7e7;
    }

    #ul-menu {
        font-size: 13px !important;
        min-width: 200px;
    }

    .direcao {
        display: none;
        margin-top: 4px;
    }

    .menu-center {
        display: flex;
        justify-content: center;
    }



    @media(min-width: 767px) {
        #btn-config {
            position: absolute !important;
            left: 10px !important;
        }

        #aviso-teste {
            position: absolute !important;
            right: 10px !important;
        }

    }

    .navbar-brand {
        display: none;
    }

    @media(max-width: 767px) {

        .navbar-toggle {
            display: inline-flex;
            align-items: center;
            border: 0;
        }

        .navbar-toggle div {
            margin: 5px;
        }

        .navbar-toggle:hover {
            background-color: #FFF !important;
            color: #888 !important;
        }


        #ul-menu {
            font-size: 15px !important;
        }

        .direcao {
            display: block;
        }
    }

    .dropdown-submenu {
  position: relative;
}

.dropdown-submenu>.dropdown-menu {
  top: 0;
  left: 100%;
  margin-top: -6px;
  margin-left: -1px;
  -webkit-border-radius: 0 6px 6px 6px;
  -moz-border-radius: 0 6px 6px;
  border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
  display: block;
}

.dropdown-submenu>a:after {
  display: block;
  content: " ";
  float: right;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
  border-width: 5px 0 5px 5px;
  border-left-color: #ccc;
  margin-top: 5px;
  margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
  border-left-color: #fff;
}

.dropdown-submenu.pull-left {
  float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
  left: -100%;
  margin-left: 10px;
  -webkit-border-radius: 6px 0 6px 6px;
  -moz-border-radius: 6px 0 6px 6px;
  border-radius: 6px 0 6px 6px;
}
</style>

<?php $action = $this->request->action; ?>
<div class="col-xs-12 container-menu" id="menu-fixado" style="border: 0;">

    <div aaa="aaaa" class="navbar navbar-default" role="navigation" style="background-color: #fff;margin-bottom: 10px !important; border: 0;">
        <div class="col-xs-12 container-menu    ">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <div>Menu</div>
                    <div>
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>
                </button>
                <?php
                if (isset($dadosSessao["imagem"]["id"]) && $dadosSessao["imagem"]["id"] <> null) {
                    $logo = $this->Html->image($dadosSessao["imagem"]["caminho"] . $dadosSessao["imagem"]["nome"], ['class' => 'logoUsuario']);
                    $padding = "4px 15px 0 25px";
                } else {
                    $logo = $this->Html->link("Adicione seu Logotipo", ["controller" => "Users", "action" => "addImagem", $dadosSessao["id"]], ["class" => "btn btn-danger btn-sm centralizada logoUsuario"]);
                    $padding = "12px 0 0 25px";
                }
                ?>
                <div class="navbar-brand canvas-logo-usuario" style="padding:<?= $padding ?>  !important; width: 338px; text-align: center;float: left;">
                    <?= $logo ?>
                </div>
            </div>
            <div class="collapse navbar-collapse">
                <div class="col-xs-12 container-menu menu-center">

                    <ul class="nav navbar-nav" id="ul-menu">
                        <li id="btn-gerencie" class="menu-item" style="color: #333 !important">
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'far fa-user', 'aria-hidden' => 'true']) . empty($dadosSessao['nome']) ? 'Olá Visitante' : $dadosSessao['nome']  . $this->Html->tag('i', '', ["class" => "dropdown-toggle direcao fas fa-chevron-down right", 'escape' => false]), '#', ['id' => 'btn-minha-conta', 'role' => 'button', 'escape' => false, 'data-toggle' => 'dropdown']) ?>
                            <?php if(!empty($dadosSessao)){ ?>
                                <ul class="dropdown-menu multi-level">
                                    <li>
                                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'far fa-user', 'aria-hidden' => 'true']) . ' Meus Dados', "#modal-user", ['role' => 'button', 'escape' => false, "data-toggle" => "modal"]) ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'far fa-handshake', 'aria-hidden' => 'true']) . ' Meus Negócios', ["controller" => "Users", "action" => "calculos"], ['role' => 'button', 'escape' => false]) ?>
                                    </li>
                                </ul>
                            <?php } ?>
                        </li>

                        <li class="menu-item" id="btn-cotacoes">
                            <a href="#" class="dropdown-toggle" data-toggle='dropdown'><i class="far fa-list-alt"></i> Planos<i class="direcao fas fa-chevron-down right"></i></a>
                            
                            
                            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                                <li class="dropdown-submenu">
                                    <a tabindex="-1" href="#">Plano de Saúde</a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-submenu">
                                            <a href="#">Pessoa Física</a>
                                            <ul class="dropdown-menu">
                                                <?php foreach ($dados['saude']['pf'] as $key => $value) { ?>
                                                    <li>
                                                        <a href="https://corretoresparceiros.com.br/index.php/operadoras/?estado-id=<?= $value['estado_id'] ?>&ramo=saude"><?= $value['estado'] ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                        <li class="divider"></li>
                                        <li class="dropdown-submenu">
                                            <a href="#">Pessoa Juridica</a>
                                            <ul class="dropdown-menu">
                                            <?php foreach ($dados['saude']['pf'] as $key => $value) { ?>
                                                <li>
                                                    <a href="https://corretoresparceiros.com.br/index.php/operadoras/?estado-id=<?= $value['estado_id'] ?>&ramo=saude"><?= $value['estado'] ?></a>
                                                </li>
                                            <?php } ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="divider"></li>
                                <?php // debug($dados) ; die('1')?>
                                <li class="dropdown-submenu">
                                    <a tabindex="-1" href="#">Odontológicos</a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-submenu">
                                            <a href="#">Pessoa Física</a>
                                            <ul class="dropdown-menu">
                                                <?php foreach ($dados['odonto']['pf'] as $key => $value) { ?>
                                                    <li>
                                                        <a href="https://corretoresparceiros.com.br/index.php/operadoras/?estado-id=<?= $value['estado_id'] ?>&ramo=odonto"><?= $value['estado'] ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                        <li class="divider"></li>
                                        <li class="dropdown-submenu">
                                            <a href="#">Pessoa Juridica</a>
                                            <ul class="dropdown-menu">
                                            <?php foreach ($dados['odonto']['pj'] as $key => $value) { ?>
                                                <li>
                                                    <a href="https://corretoresparceiros.com.br/index.php/operadoras/?estado-id=<?= $value['estado_id'] ?>&ramo=odonto"><?= $value['estado'] ?></a>
                                                </li>
                                            <?php } ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="divider"></li>

                                <li><a href="https://corretorparceiro.com.br/seguros-nacional/" target = "_blank">Seguros</a></li>
                                <li class="divider"></li>
                                <li><a href="https://corretorparceiro.com.br/consorcios/" target = "_blank">Consórcios</a></li>
                                <li class="divider"></li>
                                <li><a href="https://corretorparceiro.com.br/beneficios/" target = "_blank">Benefícios</a></li>
                            </ul>
                        </li>

                        <li class="menu-item" style="color: #333 !important">
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fas fa-cog', 'aria-hidden' => 'true']) . ' Ferramentas de Vendas', ["controller" => "Users", "action" => "central"], ['role' => 'button', 'escape' => false, 'target' => $optionTarget]) ?>
                        </li>
                        <li class="menu-item" id="btn-cotacoes">
                            <a href="#" class="dropdown-toggle" data-toggle='dropdown'><i class="far fa-list-alt"></i> Cotações <i class="direcao fas fa-chevron-down right"></i></a>
                            <ul class="dropdown-menu multi-level" id="cotacoes-list">
                                <li>
                                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'far fa-list-alt', 'aria-hidden' => 'true']) . ' Tabelas de Preços<br><small style="margin-left: 20px">Saúde e Odonto</small>', ["controller" => "TabelasGeradas", "action" => "gerador"], ['role' => 'button', 'escape' => false, 'target' => $optionTarget]) ?>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fas fa-stethoscope', 'aria-hidden' => 'true']) . ' Multicálculo Saúde', ["controller" => "Users", "action" => "saude"], ['role' => 'button', 'escape' => false, 'target' => $optionTarget]) ?>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fas fa-tooth', 'aria-hidden' => 'true']) . ' Multicálculo Odonto', ["controller" => "Users", "action" => "odonto"], ['role' => 'button', 'escape' => false, 'target' => $optionTarget]) ?>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'far fa-handshake', 'aria-hidden' => 'true']) . ' Histórico de Cotações', ["controller" => "Users", "action" => "calculos"], ['role' => 'button', 'escape' => false, 'target' => $optionTarget]) ?>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="#" class="alerta">
                                        <i class="fas fa-users"></i> Solicitação de Cotação <br><small style="margin-left: 20px">Saúde Grandes Empresas</small>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#" class="alerta">
                                        <i class="fas fa-car"></i> Multicálculo Automóvel
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#" class="alerta">
                                        <i class="fas fa-home"></i> Multicálculo Residencial
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="menu-item" id="btn-vendas">
                            <a href="#" class="dropdown-toggle" data-toggle='dropdown'>
                                <i class="fas fa-shopping-cart"></i>
                                Vendas Online
                                <i class="direcao fas fa-chevron-down right"></i>
                            </a>
                            <ul class="dropdown-menu multi-level" id="vendas-list">

                            </ul>
                        </li>

                        <li class="menu-item" style="color: #333 !important" id="btn-suporte">
                            <a href="#" class="dropdown-toggle" data-toggle='dropdown'>
                                <i class="far fa-comment"></i>
                                Suporte
                                <i class="direcao fas fa-chevron-down right"></i>
                                </b>
                            </a>
                            <ul class="dropdown-menu multi-level" id="suporte-list">
                                <li>
                                    <a href="#" class="alerta">
                                        <i class="far fa-comments"></i> Dúvidas e Fale Conosco
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#" class="alerta">
                                        <i class="fas fa-play"></i> Treinamentos e Videoaulas
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#" class="alerta">
                                        <i class="fas fa-at"></i> Agenda de Contatos Úteis
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item" style="color: #333 !important">
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fas fa-bullhorn', 'aria-hidden' => 'true']) . ' Comunicados', "#", ['role' => 'button', 'escape' => false, "class" => "alerta"]) ?>
                        </li>
                        <li class="menu-item" style="color: #333 !important">
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'far fa-star', 'aria-hidden' => 'true']) . ' Avalie', "#", ['role' => 'button', 'escape' => false, "data-toggle" => "modal", "data-target" => "#modalAvaliacao", "id" => "btnAvalie"]) ?>
                        </li>
                        <li class="menu-item" style="color: #333 !important">
                            <?= $this->Html->link( ' Quem Somos', "https://corretorparceiro.com.br/quem-somos/", ['role' => 'button', 'escape' => false]) ?>
                        </li>
                        <?php if(!empty($dadosSessao)) { ?>
                            <li class="menu-item" style="color: #000 !important">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-cogs"></i> Gerenciar</a>
                                <ul class="dropdown-menu multi-level" id="config-list">
                                    <li>
                                        <?= $this->html->link("Avaliações", ["controller" => "Avaliacaos", "action" => "index"]); ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->html->link("Endomarketing", ["controller" => "Users", "action" => "gerenciador"]); ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->html->link("Financeiro", ["controller" => "NotasCompletas", "action" => "index"]); ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->html->link("Odontológico", ["controller" => "Users", "action" => "configurarOdonto"]); ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->html->link("Saúde PF", ["controller" => "Users", "action" => "configurar-pf"]); ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->html->link("Saúde PME", ["controller" => "Users", "action" => "configurar-pme"]); ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->html->link("Tabelas de Preços", ["controller" => "TabelasGeradas", "action" => "gerador-tabelas"]); ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li id="btn-submenu" style="margin-bottom: 9px;">
                                        <?= $this->html->link(
                                            "Usuários do Sistema " . $this->Html->tag('i', '', ['id' => 'submenu-direction', 'class' => 'fas fa-chevron-down']),
                                            "#submenu",
                                            [
                                                "id" => "link-usuarios",
                                                "data-toggle" => "collapse",
                                                "data-parent" => "#accordion",
                                                "aria-expanded" => "true",
                                                "aria-controls" => "collapseOne",
                                                "escape" => false
                                            ]
                                        ); ?>
                                    </li>
                                    <li role="separator" class="divider divisor" style="display:none; margin: 9px 0 0;"></li>
                                    <li id="submenu" class="collapse">
                                        <ul style="padding-top: 9px; padding-bottom: 9px;">
                                            <li>
                                                <?= $this->html->link("Lista Usuários", ["controller" => "Users", "action" => "listaUsuarios"]); ?>
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li>
                                                <?= $this->html->link("Estatísticas", ["controller" => "users"]); ?>
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li>
                                                <?= $this->html->link("Grupos", ["controller" => "grupos", "action" => "index"]); ?>
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li>
                                                <?= $this->html->link("Logotipos", ["controller" => "users", "action" => "logotipos"]); ?>
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li>
                                                <?= $this->html->link("Indicações", ["controller" => "indicados"]); ?>
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li>
                                                <?= $this->html->link("SMS Enviados", ["controller" => "smsenviados"]); ?>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->html->link("Vendas Online", ["controller" => "vendas-onlines", "action" => "index"]); ?>
                                    </li>
                                </ul>

                            </li>
                       
                            <li class="menu-item" style="color: #333 !important">
                                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fas fa-sign-out-alt', 'aria-hidden' => 'true']) . ' Sair', ['Controller' => 'Users', 'action' => 'logout'], ['role' => 'button', 'escape' => false]) ?>
                            </li>
                        <?php } ?>            
                    </ul>
                </div>
            </div>
        

        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="indicacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="renderViewIndicacao">

        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="formcliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="conteudoFormularioCliente">
        </div>
    </div>
</div>

        <!-- ?= $this->element('modal_avaliacao') ? -->
        <!-- ?= $this->element('modal_user') ? -->
        <!-- FIM Modal Avaliação-->

        <script type="text/javascript">
            $(document).ready(function() {
                $.get('<?= $this->request->webroot . "vendas-onlines/menu" ?>', function(data) {
                    $("#vendas-list").append(data);
                });
            });

            if ($(window).width() <= 500) {
                $(".canvas-logo-usuario").css("width", "auto");
            }

            $(".botaoMenu").attr('style', 'min-height: unset !important; margin-bottom: 0 !important;padding: 0 !important;');
            $(".menumenu").attr('style', 'float: unset !important');
            if ($(window).width() <= 1080) {
                $(".userLogado").attr('class', 'userLogadoMobile');
                $(".espaco").remove();
                $(".quebra").append("<br/>");
                $(".menuCR").attr('style', 'float: unset !important');
                $(".botaoMenu").attr('style', 'float: unset !important');
                $(".botaoMenu").attr('style', 'min-height: unset !important');
            }

            $("#modalCliente").click(function() {
                $.ajax({
                    type: "get",
                    url: "<?= $this->request->webroot ?>users/enviarformulario/" + <?= $dadosSessao['id'] ?>,
                    success: function(data) {
                        $("#conteudoFormularioCliente").empty();
                        $("#conteudoFormularioCliente").append(data);
                    }
                });


            });


            $("#btn-submenu").click(event => {
                event.stopPropagation()
                $("#submenu").collapse("toggle")
                $(".divisor").toggle();
                if ($("#submenu-direction").hasClass("fa-chevron-down")) {
                    $("#submenu-direction").removeClass("fa-chevron-down")
                    $("#submenu-direction").addClass("fa-chevron-up")
                } else {
                    $("#submenu-direction").removeClass("fa-chevron-up")
                    $("#submenu-direction").addClass("fa-chevron-down")
                }
            });

            if ($(window).width() > 767) {

                $("#btn-config").mouseenter(function() {
                    $(this).addClass("open");
                });
                $("#btn-config").mouseleave(function() {
                    $(this).removeClass("open");
                });

                $("#btn-gerencie").mouseenter(function() {
                    $(this).addClass("open");
                });

                $("#btn-gerencie").mouseleave(function() {
                    $(this).removeClass("open");
                });

                $("#btn-cotacoes").mouseenter(function() {
                    $(this).addClass("open");
                });
                $("#btn-cotacoes").mouseleave(function() {
                    $(this).removeClass("open");
                });

                $("#btn-vendas").mouseenter(function() {
                    $(this).addClass("open");
                });
                $("#btn-vendas").mouseleave(function() {
                    $(this).removeClass("open");
                });

                $("#btn-suporte").mouseenter(function() {
                    $(this).addClass("open");
                });
                $("#btn-suporte").mouseleave(function() {
                    $(this).removeClass("open");
                });
            } else {
                $("#btn-gerencie").click(function() {
                    let icone = $(this).children("a").children(".direcao");
                    if (icone.hasClass("fa-chevron-down")) {
                        icone.addClass("fa-chevron-up");
                        icone.removeClass("fa-chevron-down");
                    } else {
                        icone.addClass("fa-chevron-down");
                        icone.removeClass("fa-chevron-up");
                    }
                });

                $("#btn-cotacoes").click(function() {
                    let icone = $(this).children("a").children(".direcao");
                    if (icone.hasClass("fa-chevron-down")) {
                        icone.addClass("fa-chevron-up");
                        icone.removeClass("fa-chevron-down");
                    } else {
                        icone.addClass("fa-chevron-down");
                        icone.removeClass("fa-chevron-up");
                    }
                });

                $("#btn-vendas").click(function() {
                    let icone = $(this).children("a").children(".direcao");
                    if (icone.hasClass("fa-chevron-down")) {
                        icone.addClass("fa-chevron-up");
                        icone.removeClass("fa-chevron-down");
                    } else {
                        icone.addClass("fa-chevron-down");
                        icone.removeClass("fa-chevron-up");
                    }
                });

                $("#btn-suporte").click(function() {
                    let icone = $(this).children("a").children(".direcao");
                    if (icone.hasClass("fa-chevron-down")) {
                        icone.addClass("fa-chevron-up");
                        icone.removeClass("fa-chevron-down");
                    } else {
                        icone.addClass("fa-chevron-down");
                        icone.removeClass("fa-chevron-up");
                    }
                });
            }
        </script>

