<style>
    @media (max-width: 991px) {
        .navbar-header {
            float: none;
        }

        .welcome-user-mobile {
            display: flex !important;
        }

        .welcome-user-mobile>div>a {
            color: #333;
        }

        .wellcome-user {
            display: none;
        }

        .navbar-toggle {
            display: inline-flex;
            align-items: center;
            border: 0;
            float: left;
            margin: 0;
            padding: 0;
            font-size: 18px;
        }

        .navbar-toggle div {
            margin: 5px;
            color: #888;
        }

        .navbar-collapse {
            border-top: 1px solid transparent;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.1);
        }

        .navbar-collapse.collapse {
            display: none !important;
        }

        .navbar-nav {
            float: none !important;
            margin: 7.5px 0px;
            width: 100%;
            padding-left: 40px;
        }

        .nav .navbar-nav {
            width: 100%;
        }

        .navbar-nav>li:not(:last-child) {
            border-bottom: 1px solid #eee;
        }

        .navbar-nav>li {
            float: none;
            padding: 10px 0px;
        }

        .navbar-nav>li>a {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .navbar-text {
            float: none;
            margin: 15px 0;
        }

        /* since 3.1.0 */
        .navbar-collapse.collapse.in {
            display: block !important;
        }

        .collapsing {
            overflow: hidden !important;
        }

        .menu-center {
            display: block;
            float: left;
            padding-left: 15px;
        }
    }

    .scrolled-down {
        transform: translateY(-100%);
        transition: all 0.3s ease-in-out;
    }

    .scrolled-up {
        transform: translateY(0);
        transition: all 0.3s ease-in-out;
    }

    .notifications {
        padding: 2px 7px;
        position: absolute;
        right: 10px;
        top: 25%;
        background-color: #005A9D;
        /* background-color: #dc3545; */
    }

    .notifications .fa-bell {
        font-size: 10px;
    }
</style>
<?php
if (!isset($sessao) && $this->request->getSession()->read('Auth')) {
    $sessao = $this->request->getSession()->read('Auth.User');
}
if (!isset($menu)) {
    $menu = $this->request->getSession()->read('Auth.menu');
    if ($menu == null) {
        $menu = $this->Menu->makeMenu($menuVendas['linha'][1][3], $estadosSaude, $estadosOdonto, $menuVendas['linha'][1][4]);
    }
}
?>

<?= $this->Html->css('menu', ['fullBase' => true]) ?>
<div class="col-xs-12 container-menu" style="border: 0; height: 0;">
    <div class="topo-logos">
        <div class="tg-container">
            <a href="<?= WEB_ROOT ?>">
                <img src="<?= WEB_ROOT ?>/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png" style="height: 80px;" class="custom-logo" alt="Corretor Parceiro" width="157.8" height="48.1">
            </a>
            <?php if (isset($sessao) && isset($sessao['id'])) : ?>
                <a href="/app/users/edit/<?= $sessao['id'] ?>">
                    <?php if (file_exists(WWW_ROOT . "uploads/imagens/usuarios/logos/" . $sessao['imagem']['nome']) && $sessao['imagem_id'] != null) : ?>
                        <?= $this->Html->image('../uploads/imagens/usuarios/logos/' . $sessao['imagem']['nome'], ['class' => 'custom-logo', 'alt' => 'Logo do usuário', 'id' => 'photoUser']) ?>
                    <?php else : ?>
                        <?= $this->Html->image('../uploads/imagens/usuarios/logos/default.png', ['class' => 'custom-logo', 'alt' => 'Logo do usuário', 'id' => 'photoUser']) ?>
                    <?php endif; ?>
                </a>
                <div class="wellcome-user">
                    <?= $this->Html->link($this->Html->image('menu/boneco.png', ['class' => 'menu-icons']) . ' Olá ' . $sessao['nome'], ['controller' => 'Users', 'action' => 'edit', $sessao['id']], ['escape' => false]) ?>
                    </br>
                    <?= $this->Html->link('Sair ' . $this->Html->image('menu/sair.png', ['class' => 'menu-icons']), ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]) ?>
                </div>
            <?php else : ?>
                <div class="wellcome-user">
                    <?= $this->Html->link($this->Html->image('menu/boneco.png', ['class' => 'menu-icons']) . ' Área do Corretor', '#modal-login', ['data-toggle' => 'modal', 'escape' => false]) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div id="menu" class="navbar navbar-default navbar-expand-lg" role="navigation" style="border: 0;">
        <div class="welcome-user-mobile" style="justify-content: center;">
            <?php if (isset($sessao) && isset($sessao['id'])) : ?>
                <div style="width: 20%;">&nbsp;</div>
                <div style="width: 60%;" class="text-center">
                    <?= $this->Html->link($this->Html->image('menu/boneco.png', ['class' => 'menu-icons']) . ' Olá ' . $sessao['nome'], ['controller' => 'Users', 'action' => 'edit', $sessao['id']], ['escape' => false]) ?>
                </div>
                <div style="width: 20%;">
                    <?= $this->Html->link('Sair ' . $this->Html->image('menu/sair.png', ['class' => 'menu-icons']), ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]) ?>
                </div>
            <?php else : ?>
                <div style='width: 100%' class="text-right">
                    <?= $this->Html->link($this->Html->image('menu/boneco.png', ['class' => 'menu-icons']) . ' Área do Corretor', '#modal-login', ['data-toggle' => 'modal', 'escape' => false]) ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-xs-12">

            <!--Primeira Linha  -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-menu-1">
                    <div>
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>
                    <div>Produtos</div>
                </button>
            </div>
            <div id="navbar-collapse-menu-1" class="collapse navbar-collapse">
                <div class="col-xs-12 menu-center">
                    <ul class="nav navbar-nav">
                        <?php foreach ($menu['linha'][0] as $key =>  $menuItem) : ?>
                            <li>
                                <a href="<?= $menuItem['link'] ?>" <?= isset($menuItem['toggle']) ? "data-toggle='" . $menuItem['toggle'] . "'" : "" ?> role='button' escape='false'>
                                    <img src="<?= $menuItem['image'] ?>" class="menu-icons" />
                                    <?= $menuItem['texto'] ?>
                                </a>
                                <?php if (isset($menuItem['submenu'])) : ?>
                                    <ul class="dropdown-menu">
                                        <?php $index = 0;
                                        foreach ($menuItem['submenu'] as $subItem) :
                                            if ($index > 0) : ?>
                                                <li role="separator" class="divider"></li>
                                            <?php endif; ?>
                                            <li>
                                                <?php if ($key == 1) : ?>
                                                    <a href="<?= WEB_ROOT . "/index.php/planos-de-saude-estados/estado/?estado-cod=" . $subItem['estado_id'] . "&estado-name=" . $subItem['estado'] ?>" role='button' escape=false>
                                                        <?= $subItem['estado'] ?>
                                                    </a>
                                                <?php else : ?>
                                                    <a href="<?= WEB_ROOT . "/index.php/planos-odontologicos/?estado-cod=" . $subItem['estado_id'] . "&estado-name=" . $subItem['estado'] ?>" role='button' escape=false>
                                                        <?= $subItem['estado'] ?>
                                                    </a>
                                                <?php endif; ?>
                                            </li>
                                        <?php $index++;
                                        endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>


            <!-- Segunda Linha -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-menu-2">
                    <div>
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>
                    <div>Ferramentas de Vendas</div>
                </button>
            </div>
            <div id="navbar-collapse-menu-2" class="collapse navbar-collapse">
                <div class="col-xs-12 menu-center">
                    <ul class="nav navbar-nav">
                        <?php foreach ($menu['linha'][1] as $key =>  $menuItem) : ?>
                            <li>
                                <a href="<?= $menuItem['link'] ?>" class="<?= isset($menuItem['class']) ? $menuItem['class'] : "" ?>" <?= isset($menuItem['id']) ? "id='" . $menuItem['id'] . "'" : "" ?> <?= isset($menuItem['toggle']) ? "data-toggle='" . $menuItem['toggle'] . "'" : "" ?> <?= isset($menuItem['target']) ? "data-target='" . $menuItem['target'] . "'" : "" ?> role='button' escape='false'>
                                    <img src="<?= $menuItem['image'] ?>" class="menu-icons" />
                                    <?= $menuItem['texto'] ?>
                                </a>
                                <?php if (isset($menuItem['submenu'])) : ?>
                                    <ul class="dropdown-menu">
                                        <?php
                                        $index = 0;
                                        foreach ($menuItem['submenu'] as $key => $subItem) :
                                            if ($index > 0) : ?>
                                                <li role="separator" class="divider"></li>
                                            <?php endif; ?>
                                            <?php $modulo = $menuItem['texto'] == 'Venda Online' ? 'vendaOnline' : 'midiasCompartilhadas' ?>
                                            <li class="<?= isset($subItem['menu_estados']) ? "dropdown-submenu" : "" ?>">
                                                <?php $requestjs = isset($subItem['requestjs']) ? "requestjs='" . $subItem['requestjs'] . "'" : '' ?>
                                                <?php $classLinkExterno = ($subItem['texto'] == 'Multicálculo Automóvel' || $subItem['texto'] == 'Multicálculo Residencial') ? "class='link-externo' target='_blank' " : '' ?>
                                                <a href="<?= isset($subItem['link'])  ? $subItem['link'] : (isset($subItem['menu_estados']) ? "#sub-" . $modulo . '-' . $subItem['id'] : "#") ?>" class="<?= isset($subItem['menu_estados']) ? "dropdown-hamburger" : "" ?>" role='button' <?= $requestjs ?> <?= $classLinkExterno ?> <?= isset($subItem['toggle']) ? "data-toggle='" . $subItem['toggle'] . "'" : "" ?> escape='false'>
                                                    <?php if (isset($subItem['image'])) : ?>
                                                        <img src="<?= ($menuItem['texto'] == 'Venda Online' ? WEB_ROOT . '/app/img/vendas_online/icones/' : '') .  $subItem['image'] ?>" class="menu-icons" />
                                                    <?php elseif (isset($subItem['nome'])) : ?>
                                                        <img src="<?= ($menuItem['texto'] == 'Material de Divulgação' ? WEB_ROOT . '/app/uploads/midias_compartilhadas/' : '') .  $subItem['nome'] ?>" class="menu-icons" />
                                                    <?php else : ?>
                                                        <i class="<?= $subItem['icone'] ?>"></i>
                                                    <?php endif; ?>

                                                    <?php if ($menuItem['texto'] == 'Venda Online' || $menuItem['texto'] == 'Cotações') { ?>
                                                        <?= isset($subItem['texto']) ? $subItem['texto'] : $subItem['menu_linha1'] ?>
                                                        <?= isset($subItem['texto']) ? "" : "</br><small>" . $subItem['menu_linha2'] . "</small>" ?>
                                                    <?php } else {  ?>
                                                        <?= $subItem['titulo'] . "</br><small>" . $subItem['menu_linha1'] . "</small>"  ?>
                                                    <?php } ?>
                                                </a>

                                                <?php if (isset($subItem['menu_estados'])) : ?>
                                                    <ul id="sub-<?= $modulo . '-' . $subItem['id'] ?>" class="collapse hamburguer">
                                                        <?php
                                                        $indexB = 0;
                                                        foreach ($subItem['menu_estados'] as $key => $estado) :
                                                        ?>
                                                            <li role="separator" class="divider"></li>
                                                            <li>
                                                                <?php $requestjs = isset($subItem['requestjs']) ? "requestjs='" . $subItem['requestjs'] . "'" : '' ?>
                                                                <?php if ($menuItem['texto'] == 'Venda Online') { ?>
                                                                    <a href="<?= '/app/vendas/' . $subItem['id'] . '/' . $key ?>" role='button' <?= $requestjs ?> escape='false'>
                                                                        <?= $estado ?>
                                                                    </a>
                                                                <?php } else { ?>
                                                                    <a href="<?= '/app/midias/' . $subItem['id'] . '/' . $key ?>" role='button' requestjs=<?= ($menuItem['texto'] == 'Material de Divulgação') ? 'AppMidiasCompartilhadas' : '' ?> escape='false'>
                                                                        <?= $estado ?>
                                                                    </a>
                                                                <?php } ?>
                                                            </li>
                                                        <?php $indexB++;
                                                        endforeach; ?>
                                                    </ul>
                                                <?php endif; ?>


                                            </li>
                                        <?php $index++;
                                        endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                        <!-- MENU CONFIGURAÇÕES -->
                        <?php if (isset($sessao) && !empty($sessao)) { ?>
                            <?php if ($sessao["role"] === "admin") : ?>
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="<?= WEB_ROOT ?>/app/img/menu/gerenciar.png" class="menu-icons" />

                                    </a>
                                    <ul class="dropdown-menu" style="min-width: 180px; left: unset; right: 0;">
                                        <li class="dropdown-submenu">
                                            <a class="dropdown-hamburger" tabindex="-1" href="#sub-conteudo">Conteúdo</a>
                                            <ul id="sub-conteudo" class="collapse hamburguer">
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Marketing", '/users/gerenciador', ['si/d' => 'users.gerenciador']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Odontológico", '/users/configurarOdonto', ['s-id' => 'users.configurarOdonto', 'requestjs' => 'AppOdontos']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Saúde PF", '/users/configurarPf', ['si-d' => 'users.configurarPf', 'requestjs' => 'AppSaudePF']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Saúde PME", '/users/configurarPme', ['s-id' => 'users.configurarPme', 'requestjs' => 'AppSaudePME']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Tabelas de Preços", '/tabelasGeradas/geradorTabelas', ['s-id' => 'tabelasGeradas.geradorTabelas', 'requestJS' => 'AppTabelasGeradas']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Vendas Online", '/vendasOnlines/index', ['s-id' => 'vendasOnlines.index', 'requestJS' => 'AppSisConfiguracoes']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Midas", '/midiasCompartilhadas/index', ['s-id' => 'midiasCompartilhadas.index', 'requestJS' => 'AppMidiasCompartilhadas', 'id' => 'btnModuloMidiaCompartilhada']); ?>
                                                </li>
                                            </ul>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <?= $this->Html->link("Financeiro", '/notasCompletas/index', ['si/d' => 'notasCompletas.index']); ?>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li style="position: relative;">
                                            <?= $this->Html->link("Interações", '/avaliacaos/index', ['si/d' => 'avaliacaos.index']); ?>
                                            <?php if (isset($sessao['new_interacoes']) && $sessao['new_interacoes'] > 0) : ?>
                                                <span class="badge notifications" title="Novas Interações">
                                                    <i class="fas fa-bell"></i>
                                                </span>
                                            <?php endif; ?>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <?= $this->Html->link("Configuração", '/sisConfiguracoes/permissions', ['requestJS' => 'AppSisConfiguracoes', 'escape' => false, 'si/d' => 'sisConfiguracoes.permissions']); ?>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li class="dropdown-submenu">
                                            <a class="dropdown-hamburger" href="#sub-usuarios">Usuários</a>
                                            <ul id="sub-usuarios" class="collapse hamburguer">
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Estatísticas", '/users/index',  ['si/d' => 'users.index']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>

                                                <li>
                                                    <?= $this->Html->link("Grupos", '/grupos/index',  ['si/d' => 'grupos.index']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Indicações", '/indicados/index', ['requestJS' => 'AppIndicados']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Listar Usuários", '/users/listaUsuarios', ["controller" => "Users", "action" => "listaUsuarios"]); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("Logotipos", '/users/logoTipos', ['requestJS' => 'AppSisCoonfiguracoes']); ?>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <?= $this->Html->link("SMS Enviados", '/smsEnviados/logoTipos', ['requestJS' => 'AppSisCoonfiguracoes']); ?>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            <?php endif; ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="indicacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="renderViewIndicacao">

        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="formcliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="conteudoFormularioCliente">
        </div>
    </div>
</div>
<div class="preencimento-menu"></div>

<script>
    $('body').css('padding-top', $('.navbar').outerHeight() + 'px')

    // detect scroll top or down
    if ($('#menu').length > 0) { // check if element exists
        var last_scroll_top = 80;
        $(window).on('scroll', function() {
            scroll_top = $(this).scrollTop();
            if (scroll_top < last_scroll_top) {
                $('#menu').removeClass('scrolled-down').addClass('scrolled-up');
            } else {
                $('#menu').removeClass('scrolled-up').addClass('scrolled-down');
            }
            last_scroll_top = scroll_top;
        });
    }
    $(".dropdown-hamburger").click(function(evt) {
        evt.stopPropagation();
        var dest = $(this).attr("href")
        $(dest).collapse('toggle');
    })
</script>