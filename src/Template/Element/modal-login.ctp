<style>
    #entrar:focus {
        color: #FFF;
    }
</style>
<div class="modal fade" id="modal-login" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-top: 35px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="top: 5px; right: 5px; position: absolute; color: #ccc;">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-title" style="font-size: 16px; color: #333; text-align: center;" id="labelModalLogin">
                    Para acessar essa funcionalidade</br> é necessário entrar no sistema
                </div>
            </div>
            <div class="modal-body">
                <div class="row" style="margin: auto;">
                    <div class="col-xs-12 arealogin">

                        <form method="post" accept-charset="utf-8" id="loginapi" action="/app/">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <div class="input text">
                                <input type="text" name="username" class="form-control text-center input" placeholder="Usuário (CPF)" id="username">
                            </div>
                            <div class="input password">
                                <input type="password" name="password" class="form-control text-center input" placeholder="Senha" id="password">
                            </div>
                            <button id="entrar" class="col-xs-12 btn btn-primary btn-login" style="margin-bottom: 5px" type="button">
                                Entrar <span id="loading-login"></span>
                            </button>
                        </form>

                        <div class="link-texto text-center" style="font-size: 14px; margin-bottom: 15px;">
                            Esqueceu sua senha?
                            <a href="#" id="btn-recuperar" style="color: #ea1919; text-decoration: none;">Clique Aqui</a>
                        </div>
                        <div id="recuperar" style="display: none;">
                            <div class="col-xs-12 text-center" style="margin-bottom: 15px;">
                                <div class="input text"><input type="text" name="cpf" class="text-center col-xs-12 col-md-3 col-md-offset-3 placeholder" placeholder="Digite o CPF Cadastrado" style="margin: 0px;" id="cpf"></div>
                                <a href="#" id="post-senha" style="color: #ea1919; font-size: 13px; text-decoration: none;">Enviar Nova Senha</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        Não possui cadastro?
                        <a href="/app/cadastro" class="btn btn-md btn-danger col-xs-12">
                            Cadastre-se Grátis
                        </a>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery('#entrar').click(function(e) {
        e.preventDefault();
        var dadosLogin = jQuery('#loginapi').serialize();
        jQuery.ajax({
            url: "/app/api/login.json",
            type: "POST",
            data: dadosLogin,
            async: true,
            success: function(result) {
                if (result.status == 'sucesso') {
                    jQuery.ajax({
                        url: "/app/api/users/login.json",
                        type: "POST",
                        data: dadosLogin,
                        success: function(res) {
                            if (res.success) {
                                var token = res.data.token;
                                var expire = res.data.expires_in;
                                var perfis = res.data.perfisUser;
                                document.cookie = 'token=' + token + ';max-age=' + expire;
                                document.cookie = 'perfis=' + perfis;
                                jQuery(`#loginapi`).submit();
                            }
                        },
                    });

                } else {
                    alert('error');
                }
            },
            complete: function() {
                jQuery('#myModal').modal('hide');
            }
        });
    });

    var count = 0;

    function updateCount() {
        if ($(this).val().length == 14) {
            $("#username").mask("99.999.999/9999-99");
        }
        if ($(this).val().length == 11) {
            $("#username").mask("999.999.999-99");
        }
    }

    function removermascara() {
        $("#username").unmask();
    }

    $("#logar").click(function() {
        $.ajax({
            type: "post",
            data: $("#login").serialize(),
            url: "<?php echo $this->request->webroot ?>users/",
        });
    });

    $("#senha").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/recuperarSenha/",
            data: $("#cpf").serialize(),
            success: function(data) {
                $("#recuperarsenha").empty();
                $("#recuperarsenha").append(data);

            }
        });
    });
    $("#add").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/add/",
            success: function(data) {
                $("#cad").empty();
                $("#cad").append(data);

            }
        });
    });
    $("#novoCadastro").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/add/",
            success: function(data) {
                $("#cad").empty();
                $("#cad").append(data);

            }
        });
    });

    $("#btn-recuperar").click(function() {
        $('#recuperar').css('display', 'block');
        $('#cpf').mask("999.999.999-99");
        $('#post-senha').click(function() {
            $.ajax({
                type: "post",
                url: "/app/api/senha.json",
                data: {
                    cpf: $('#cpf').val()
                },
                beforeSend: function() {
                    if ($('#cpf').val() == '') {
                        alert('necessário informa o cpf');
                        $('#cpf').focus();
                        return false;
                    }
                },
                success: function(data) {
                    if (data.ok == true) {
                        $('#recuperar').css('display', 'none');
                        $('#username').text($('#cpf').val());
                        $('#password').focus();
                    }

                },
            });
        })
    });

    $("#modalLogin").appendTo("body");
</script>