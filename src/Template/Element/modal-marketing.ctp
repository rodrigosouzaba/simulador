<style>
    #content-marketing {
        background-color: transparent;
        -webkit-box-shadow: 0px 0px 0px 0px;
        box-shadow: 0px 0px 0px 0px;
        border: 0px
    }

    #close-marketing {
        position: absolute;
        right: 20px;
        top: 20px;
        background-color: white;
        opacity: 1;
        padding-left: 5px;
        padding-right: 5px;
        border-radius: 10px;
    }

    #close-marketing:hover {
        background-color: transparent;
        color: white;
    }
</style>

<?php
//ESTE LOCAL É O LOCAL = 2
$local = 2;
$imagens = array();
foreach ($banners as $banner) {
    if ($banner->banner_id == $local) {
        array_push($imagens, $banner->nome);
    }
}
$tamanho = count($imagens) - 1;
if ($tamanho >= 0) {
    $imagem = $imagens[rand(0, $tamanho)];
}

$session = $this->request->session();
if ($session->read('entrou') == 1) {
    $login = $session->read('entrou');
    $session->delete('entrou');
}
?>

<?php
$sessao = $session->read('Auth.User');
$role = $sessao['role'];
?>
<!-- Modal -->
<div class="modal fade" id="marketing" tabindex="-1" role="dialog" aria-labelledby="marketingLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div id="content-marketing" class="modal-content">
            <div class="modal-header" style="border-bottom: 0px;">
            </div>
            <div class="modal-body">
                <button id="close-marketing" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= $this->Html->image('banners/imagens/' . $imagem) ?>
            </div>
        </div>
    </div>
</div>
