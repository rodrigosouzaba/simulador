<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat|Montserrat+Alternates&display=swap');


    @media(max-width: 420px) {
        .steps {
            justify-content: center;
        }

    }

    .flex {
        display: flex
    }

    .titulos {
        font-family: 'Montserrat', sans-serif;
        color: #003956;
        width: 100%;
        font-size: 20pt;
        font-weight: 500;
        margin-bottom: 20px;
    }

    .steps {
        display: flex;
        height: 10%;
        width: 100%;
        padding: 20px;
        justify-content: start;
        align-items: center;
    }


    .content-step {
        height: 70%;
        width: 100%;
    }

    .content-1 {
        align-items: center;
        justify-content: space-evenly;
    }

    .footer-step {
        margin-top: 20px;
        margin-Bottom: 20px;
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .footer-step button {
        margin-bottom: 0;
    }

    .step {
        height: 4px;
        width: 60px;
        margin-left: 2px;
        margin-right: 2px;
        background: lightgrey;
        border-radius: 4px;
    }

    .step-active {
        background-color: #003956;
    }

    .icones {
        font-size: 30pt;
        width: 40%;
        transition: width 0.2s;
    }

    .icones:hover {
        width: 60%;
    }

    .cara-triste {
        color: #c10000;
    }

    .selecionado {
        width: 60%;
    }

    .selecionado:hover {
        width: 60%;
    }

    .continuar-avaliacao {
        padding: 5px 10px;
        font-size: 12pt;
        height: 40px;
        color: #003956;
        background-color: #FFF;
        border: 1px solid #003956;
        margin-right: 10%;
        width: 100px;
    }

    .continuar-avaliacao:hover {
        background-color: #003956;
        color: #FFF;
    }

    .continuar-avaliacao:focus {
        background-color: #FFF;
        color: #003956;
    }

    .salvar-avaliacao {
        display: none;
        padding: 5px 10px;
        font-size: 12pt;
        height: 40px;
        color: #003956;
        background-color: #FFF;
        border: 1px solid #003956;
        margin-right: 10%;
        width: 100px;
    }

    .salvar-avaliacao:hover {
        background-color: #003956;
        color: #FFF;
    }

    .salvar-avaliacao:focus {
        background-color: #FFF;
        color: #003956;
    }

    .voltar-avaliacao {
        visibility: hidden;
        padding: 5px 10px;
        font-size: 12pt;
        height: 40px;
        color: #003956;
        background-color: #FFF;
        border: 1px solid #003956;
        margin-left: 10%;
        width: 100px;
    }


    .voltar-avaliacao:hover {
        background-color: #003956;
        color: #FFF;
    }

    .voltar-avaliacao:focus {
        background-color: #FFF;
        color: #003956;
    }

    .content-step {
        display: none;
        flex-wrap: wrap;
        align-content: space-evenly;
    }

    .content-active-1 {
        display: flex;
    }

    .content-active-2 {
        display: block;
    }

    .content-active-3 {
        display: block;
    }

    .input-indicacao {
        border: 0 !important;
        box-shadow: 0 0 0 0 !important;
        border-bottom: 2px solid #f5f5f5 !important;
        transition: background-color 2s ease;
    }

    .input-indicacao:focus {
        border-bottom: 2px solid #003956 !important;
        background-color: #FFF !important;
    }

    .group-indicacao label {
        margin-top: 15px;
        font-size: 9pt !important;
        color: #003956;
    }

    button[disabled] {
        opacity: 0.7;
        background-color: #FFF;
        color: #003956;
    }

    button[disabled]:hover {
        opacity: 0.7;
        background-color: #FFF;
        color: #003956;
    }

    .grid {
        display: flex;

    }

    .radios label input {
        margin: 2px 4px;
    }
</style>

<!-- Modal Avaliação-->
<div class="modal fade" id="modalAvaliacao" tabindex="-1" role="dialog" aria-labelledby="modalAvaliacaoLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 15px; top: 15px; z-index: 5;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-body" style="overflow: hidden; min-height: 350px;">
                <div class="steps">
                    <span class="step step-1 step-active"></span>
                    <span class="step step-2"></span>
                </div>
                <div class="content-step content-1 content-active-1">
                    <div class="text-center titulos">Você está gostando?</div>
                    <div class="grid">
                        <div class="icon-text text-center">
                            <?= $this->Html->image('botoes/insatisfeito.png', ['class' => 'icones', 'nota' => '1']); ?>
                            <br />
                            <span>Ruim</span>
                        </div>
                        <div class="icon-text text-center">
                            <?= $this->Html->image('botoes/razoavel.png', ['class' => 'icones', 'nota' => '2']); ?>
                            <br />
                            <span>Razoável</span>
                        </div>
                        <div class="icon-text text-center">
                            <?= $this->Html->image('botoes/bom.png', ['class' => 'icones', 'nota' => '3']); ?>
                            <br />
                            <span>Bom</span>
                        </div>
                        <div class="icon-text text-center">
                            <?= $this->Html->image('botoes/satisfeito.png', ['class' => 'icones', 'nota' => '4']); ?>
                            <br />
                            <span>Show</span>
                        </div>
                        <div class="icon-text text-center">
                            <?= $this->Html->image('botoes/muito-satisfeito.png', ['class' => 'icones', 'nota' => '5']); ?>
                            <br />
                            <span>Amei</span>
                        </div>
                    </div>
                    <?= $this->Form->input('nota', ['class' => 'form-control', 'type' => 'hidden']) ?>
                </div>
                <div class="content-step content-2">
                    <div class="text-center titulos" style="height: 10%;">Deseja deixar um comentário?</div>
                    <div style="height: 10%">&nbsp</div>
                    <div class="radios flex" style="justify-content: space-evenly; padding: 5px; height: 20%; align-items: center;">
                        <?= $this->Form->radio('tipo', ['CRITICA' => 'Crítica', 'ERRO' => 'Erro', 'ELOGIO' => 'Elogio', 'SUGESTAO' => 'Sugestão']) ?>
                    </div>
                    <div style="height: 10%">&nbsp</div>
                    <?= $this->Form->textarea('comentario', ['id' => 'comentario', 'style' => 'height: 50%; width: 80%; margin: 0 auto; text-align: center; display: block;']) ?>
                    <div class="footer-step">
                        <?= $this->Form->button('Voltar', ['id' => 'voltar-avaliacao', 'class' => 'voltar-avaliacao']) ?>
                        <!-- <?= $this->Form->button('Continuar', ['id' => 'continuar-avaliacao', 'class' => 'continuar-avaliacao', 'disabled']) ?> -->
                        <?= $this->Form->button('Enviar', ['id' => 'salvar-avaliacao', 'class' => 'salvar-avaliacao', "data-dismiss" => "modal", "aria-label" => "Close"]) ?>
                    </div>
                </div>
                <!-- <div class="content-step content-3">
                    <div class="text-center titulos" style="height: 50px;">Indique-nos para um amigo</div>
                    <div style="width: 80%; margin: 0 auto; padding-top: 20px;">
                        <div class="form-group group-indicacao">
                            <label>Nome</label>
                            <input type="text" name="nome" class="input-indicacao" placeholder="Nome do amigo" id="indicacao-nome">
                        </div>
                        <div class="form-group group-indicacao">
                            <label>Email</label>
                            <input type="text" name="email" class="input-indicacao" placeholder="Email do amigo" id="indicacao-email">
                        </div>
                        <div class="form-group group-indicacao">
                            <label>Celular</label>
                            <input type="text" id="indicacao-celular" name="telefone" class="input-indicacao" placeholder="Celular do amigo">
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>