<style>
    #solicitar {
        margin-right: 20px;
    }

    @media(max-width: 766px) {
        #solicitar {
            margin-right: 0px;
        }
    }
</style>
<!-- Modal -->
<div class="modal fade" id="<?= isset($id) ? $id : "modalAviso" ?>" tabindex="-1" role="dialog" aria-labelledby="modalAvisoLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="conteudoAviso">
                <?= isset($texto) ? $texto : 'Sem texto' ?>
            </div>
            <div class="modal-footer" style="border: 0;">
                <div class="col-md-8 col-md-offset-2 centralizada">
                    <?= $this->Form->button('Solicitar Contato', ['id' => 'solicitar', 'class' => 'btn btn-danger']) ?>
                    <?= $this->Form->button('Continuar', ['id' => 'continuar', 'class' => 'btn btn-primary', 'style' => 'width: 133px;']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
