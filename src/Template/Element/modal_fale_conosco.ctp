<style>
    /* CORREÇÃO CSS WORDPRESS */
    .modal-footer {
        border: 0;
        text-align: center;
    }

    .modal-footer button {
        margin-bottom: 0;
    }
</style>

<div class="modal fade" id="<?= isset($id) ? $id : "modalFaleConosco" ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center" style="border: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-center" id="faleConoscoContent" style="font-size: 18px !important;">
                <?= isset($message) ? $message : "Deseja que a nossa equipe entre em contato com você?" ?>
            </div>
            <div class="modal-footer text-center" id="botoes-resposta">
                <?= $this->Form->button('Solicitar Contato', ['id' => 'sim-faleConosco', 'class' => 'btn btn-danger']) ?>
            </div>
        </div>
    </div>
</div>