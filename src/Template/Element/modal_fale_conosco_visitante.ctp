<style>
    /* CORREÇÃO CSS WORDPRESS */
    .modal-footer {
        border: 0;
        text-align: center;
    }

    .modal-footer button {
        margin-bottom: 0;
    }
</style>

<div class="modal fade" id="faleConoscoVisitante" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-top: 35px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="top: 5px; right: 5px; position: absolute; color: #ccc;">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-title" style="font-size: 16px; color: #333; text-align: center;" id="labelModalLogin">
                    Fale Conosco
                </div>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <?= $this->Form->create(null, ['url' => ['controller' => 'Avaliacaos', 'action' => 'add']]); ?>
                    <?= $this->Form->hidden('nota', ['value' => 99]); ?>
                    <?= $this->Form->hidden('visitante', ['value' => 1]); ?>
                    <div class="input text">
                        <input type="text" name="nome" placeholder="Nome" class="form-control text-center input">
                    </div>
                    <div class="input text">
                        <input type="text" name="telefone" placeholder="Telefone" class="form-control text-center input">
                    </div>
                    <div class="input text">
                        <input type="text" name="comentario" placeholder="Assunto" class="form-control text-center input">
                    </div>
                    <button id="entrar" class="col-xs-12 btn btn-primary btn-login" style="margin-bottom: 5px" type="button">
                        Solicitar Contato
                    </button>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>