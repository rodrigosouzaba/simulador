<style>
    .topo {
        width: 100%;
        display: flex;
        justify-content: space-around;
        align-items: center;
    }

    .user-logo {
        justify-content: center;
        align-items: center;
        vertical-align: middle;
    }

    .user-options {
        justify-content: center;
        align-items: center;
        vertical-align: middle;
    }

    .versao-teste {
        justify-content: center;
        align-items: center;
        vertical-align: middle;
    }

    .first-td {
        padding-top: 15px !important;
    }

    .input-disabled {
        background-color: transparent !important;
        box-shadow: none !important;
        border: 0 !important;
        margin: 0 !important;
    }

    .input-enabled {
        background-color: transparent !important;
        /* box-shadow: none !important; */
        /* border: 0 !important; */
        margin: 0 !important;
    }

    .buttons-edit {
        display: none;
    }

    .table-condensed tr,
    .table-condensed td {
        /* border: 0 !important;
        padding: 3px !important; */
    }
</style>

<!-- Modal -->
<div class="modal fade" id="modal-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="data-user">

        </div>
    </div>
</div>