<div id="newacoes">
    <?php
    $controller = $this->request->controller;
    $action = $this->request->action;

    $linkWhats = 'https://wa.me?text=Visualize%20sua%20tabela%20de%20pre%C3%A7os%20aqui%20https%3A%2F%2Fcorretorparceiro.com.br/app%2Ftabelas-geradas%2Fshared%2F';

    $temSessao = !empty($this->request->session()->read('Auth.User'));

    // if ($action === 'TabelasGeradas' && $controller === 'GeradorTabelas') :
    //     echo $this->Form->button($this->Html->tag('span', '', ['class' => 'fa fa-download', 'aria-hidden' => 'true']), ['class' => ' btn btn-lg btn-default', "id" => "pdfdownload", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "left", "title" => "Download do PDF"]);
    // endif;
    // // <!-- DOWNLOAD DO PDF -->
    // if ($temSessao || ($controller == 'Links' && $action == 'view')) {
    //     echo $this->Form->button($this->Html->image('botoes/download.png'), ['class' => ' btn btn-default icones-btn', "id" => "pdfdownload", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "left", "title" => "Baixar em PDF"]);
    // } else {
    //     echo "&nbsp";
    //     echo $this->Html->link($this->Html->image('botoes/download.png'), '#modal-login', ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, "data-tt" => "tooltip", "data-toggle" => "modal", "data-placement" => "bottom", "title" => "Baixar em PDF"]);
    // }

    // <!-- PDF NO BROWSER -->
    if ($temSessao || ($controller == 'Links' && $action == 'view')) {
        echo "&nbsp";
        echo $this->Form->button($this->Html->image('botoes/pdf.png'), ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, 'formtarget' => '_blank', "id" => ($controller === 'Links' && $action === 'view' ? 'print-js' : "pdfbrowser"), "data-toggle" => "tooltip", "data-placement" => "left", "title" => "Imprimir"]);
    } else {
        echo "&nbsp";
        echo $this->Html->link($this->Html->image('botoes/pdf.png'), '#modal-login', ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, "data-tt" => "tooltip", "data-toggle" => "modal", "data-placement" => "bottom", "title" => "Imprimir"]);
    }

    // <!-- FAVORITAR TABELA GERADA -->
    if ($temSessao) {
        if (!$controller === 'Links') {
            echo "&nbsp";
            echo $this->Html->link($this->Html->image('botoes/favoritas.png'), "#", ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, "id" => "salvartabela", "data-toggle" => "tooltip", "data-placement" => "left", "title" => "Favoritar Tabela"]);
        }
    } else {
        echo "&nbsp";
        echo $this->Html->link($this->Html->image('botoes/favoritas.png'), '#modal-login', ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, "data-tt" => "tooltip", "data-toggle" => "modal", "data-placement" => "bottom", "title" => "Favoritar Tabela"]);
    }

    // EMAIL
    if ($temSessao) {
        echo "&nbsp";
        if ($controller === 'Links' && $action === 'view') {
            echo $this->Html->link($this->Html->image('botoes/mail.png'), '#', ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, 'data-toggle' => "tooltip", "id" => 'btn-email', 'title' => 'Compartilhar por Email', "data-placement" => "left"]);
        } elseif ($action == 'cotacao') {
            echo $this->Html->link($this->Html->image('botoes/mail.png'), '#', ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, "data-toggle" => 'modal', "data-target" => '#modalCompartilhamento', "id" => "compartilhar", 'title' => 'Compartilhar por Email', "data-placement" => "left"]);
        } else {
            echo $this->Html->link($this->Html->image('botoes/mail.png'), '#', ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, "data-toggle" => 'modal', "data-target" => '#modalShare', "id" => "compartilhar", 'title' => 'Compartilhar por Email', "data-placement" => "left"]);
        }
    } else {
        echo "&nbsp";
        echo $this->Html->link($this->Html->image('botoes/mail.png'), '#modal-login', ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, "data-tt" => "tooltip", "data-toggle" => "modal", "data-placement" => "bottom", "title" => "Enviar por Email"]);
    }

    // WHATSAPP
    if ($temSessao) {
        echo "&nbsp";
        // Verifica se a Tabela a ser compartilhada já está salva
        if (isset($tabelaSalva)) {
            echo  $this->Html->link($this->Html->image('botoes/whatsapp.png'), $linkWhats . $tabelaSalva->id, ['target' => '_blank', 'class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false,  "data-toggle" => "tooltip", "data-placement" => "left", "title" => "Enviar por WhatsApp", "dados" => "com"]);
        } else {
            echo $this->Html->link($this->Html->image('botoes/whatsapp.png'), "#", ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, "id" => "whatsapp", "data-toggle" => "tooltip", "data-placement" => "left", "title" => "Enviar por WhatsApp", "dados" => ""]);
        }
        echo "&nbsp";
    } else {
        echo "&nbsp";
        echo $this->Html->link($this->Html->image('botoes/whatsapp.png'), '#modal-login', ['class' => ' btn btn-default icones-btn', 'role' => 'button', 'escape' => false, "data-tt" => "tooltip", "data-toggle" => "modal", "data-placement" => "bottom", "title" => "Enviar por WhatsApp", "dados" => ""]);
    }

    //Criação de Links pro CP
    if ($temSessao) {
        if (!empty($this->request->session()->read('Auth.User.role')) && $this->request->session()->read('Auth.User.role') === 'admin') :
            echo $this->Html->link($this->Html->image('botoes/link.png'), "#", ['class' => 'icones-btn btn btn-default', 'role' => 'button', 'escape' => false, "id" => "exibir-nome", "data-toggle" => "tooltip", "data-placement" => "left", "title" => "Salvar Link da Tabela para o Corretor Parceiro"]);
        endif;
    }
    ?>

</div>
<script>
    $("#fieldShareEmail").hide();

    $("#outroemail").click(function() {
        $("#fieldShareEmail").show();
    });

    $("#acoesgerador").hide();
    $("#pdfbrowser").click(function() {
        $("#tipo").val("DI");
        $("#dadosPDF").attr("target", "_blank");
        $("#dadosPDF").submit();
    });
    $("#pdfdownload").click(function() {
        $("#tipo").val("D");
        $("#loader").click();

        setTimeout(function() {
            $("#fechar").click();
        }, 15000)
    });
    $("#pdfemail").click(function() {
        $("#email-cliente").val('');
        $("#tipo").val("F");
    });
    $("#pdfoutroemail").click(function() {
        $("#tipo").val("F");
    });
    $("#pdfoutroemail2").click(function() {
        $("#tipo").val("F");
    });

    $("#salvartabela").click(function() {
        $("#loader").click();
        let dados = $("#gerador").serialize();
        dados += '&compartilhada=N';
        $.ajax({
            type: "POST",
            url: baseUrl + "tabelasGeradas/add/",
            data: dados,
            success: function(data) {
                $("#fechar").click();
                $.ajax({
                    type: "POST",
                    url: baseUrl + "tabelasGeradas/atualizarTabelas/",
                    success: function(data) {
                        $("#corpo-modal").empty();
                        $("#corpo-modal").append(data);
                        //$("#modalDialogo").modal("show");
                    }
                });
            }
        });
    });

    $("#exibir-nome").click(function() {
        $("#loader").click();
        $.ajax({
            type: "POST",
            url: baseUrl + "links/add/",
            data: $("#gerador").serialize(),
            success: function(data) {
                $("#fechar").click();
            }
        });
    });

    $("#whatsapp").click(function() {
        var tabela = $(this).attr("dados");
        let dados = $("#gerador").serialize();
        dados += '&compartilhada=S';
        $.ajax({
            type: "POST",
            url: baseUrl + "tabelasGeradas/add/",
            data: dados,
            beforeSend: function() {
                $("#loader").click();
            },
            success: function(data) {
                $.ajax({
                    type: "POST",
                    url: baseUrl + "tabelasGeradas/tabelaWhats/",
                    data: dados,
                    success: function(data) {
                        let tabela_id = data.key;
                        window.open("<?= $linkWhats ?>" + tabela_id, '_blank');
                    }
                });
                $("#fechar").click();
                $("#modalDialogo").modal('show');
            }
        });
    });
</script>