<?php if ($operadora['status'] == 'OCULTA' && $operadora['status'] == 'EM ATUALIZAÇÃO') : ?>
    <div class="centralizada col-xs-12 text-danger">
        <h1>EM ATUALIZAÇÃO</h1>
    </div>
<?php elseif ($operadora['status'] == 'INATIVA') : ?>
    <div class="centralizada col-xs-12 text-danger">
        <h1>COMERCIALIZAÇÃO SUSPENSA</h1>
    </div>
<?php else : ?>

    <?php if (isset($tabelasOrdenadas) && $tabelasOrdenadas != '') { ?>
        <?= $this->Form->create('download-pdf', ['id' => 'download-pdf', 'url' => ['action' => 'pdf',]]); ?>
        <div class="col-xs-12" style="background-color: #004057; color: #ffffff;  padding: 5px 0; text-align: center; font-weight: bold; ">
            <?php
            $dadosOperadora = $operadora["nome"] . " " . $operadora["detalhe"] . " ";
            switch ($dados->ramo) {
                case "SPJ":
                    echo $dadosOperadora . $vidas['min'] . " A " . $vidas['max'] . " VIDAS";
                    break;
                case "SPF":
                    echo $dadosOperadora;
                    break;
            }
            ?>
        </div>
        <style>
            table td {
                border: solid 0.5px #cccccc !important;
            }
        </style>
        <?php foreach ($tabelasOrdenadas  as $operadora_id => $operadora) : ?>
            <?php if ($is_vitalmed) : ?>
                <?= $this->element('tabelas_geradas/vitalmed', ['operadora' => $operadora]) ?>
            <?php else : ?>
                <?php foreach ($operadora['tabelas'] as $grupos) :  ?>
                    <table class="table table-condensed table-striped">
                        <colgroup>
                            <col span="1" style="width: 110px;">
                        </colgroup>
                        <tr style="width: 100%;">
                            <td style="max-width: 50px !important; text-align:center; vertical-align: middle;">Produto</td>
                            <?php foreach ($grupos as $key => $produto) { ?>
                                <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                                    <?php $complemento = !is_null($produto['tipo_contratacao']) ? ($produto['tipo_contratacao'] == 0 ? ' - OPCIONAL' : ($produto['tipo_contratacao'] == 1 ? ' - COMPULSÓRIO' : '')) : '';
                                    ?>
                                    <?= $produto['nome'] . $complemento ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <tr style="width: 100%;">
                            <td style="max-width: 50px !important;text-align:center; vertical-align: middle;">Cobertura</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                                    <?= $produto['cobertura']['nome'] ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <tr style="width: 100%;">
                            <td style="max-width: 50px !important;text-align:center; vertical-align: middle;">Acomodação</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                                    <strong><?= ($produto['acomodacao']['nome'] == 'AMBULATORIAL' ? "SEM ACOMODAÇÃO" : $produto['acomodacao']['nome']) ?></strong>
                                </td>
                            <?php } ?>
                        </tr>
                        <tr style="width: 100%;">
                            <td style="max-width: 50px !important;text-align:center; vertical-align: middle;">Coparticipação</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                                    <?= ($produto['coparticipacao'] == 's' ? "COM COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'] : "SEM COPARTICIPAÇÃO") ?>
                                    </br>
                                    <small>Cód. ANS: <?= $produto['cod_ans'] ?></small>
                                </td>
                            <?php } ?>
                        </tr>

                        <!-- PREÇOS -->
                        <!-- até 18 anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">até 18 anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check1'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa1'] + $produto['odonto_valor'] : $produto['faixa1'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- 19 a 23 anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">19 a 23 anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check2'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa2'] + $produto['odonto_valor'] : $produto['faixa2'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- 24 a 28 anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">24 a 28 anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check3'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa3'] + $produto['odonto_valor'] : $produto['faixa3'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- 29 a 33 anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">29 a 33 anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check4'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa4'] + $produto['odonto_valor'] : $produto['faixa4'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- 34 a 38 anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">34 a 38 anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check5'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa5'] + $produto['odonto_valor'] : $produto['faixa5'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- 39 a 43 anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">39 a 43 anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check6'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa6'] + $produto['odonto_valor'] : $produto['faixa6'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- 44 a 48 anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">44 a 48 anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check7'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa7'] + $produto['odonto_valor'] : $produto['faixa7'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- 49 a 53 anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">49 a 53 anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check8'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa8'] + $produto['odonto_valor'] : $produto['faixa8'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- 54 a 58 anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">54 a 58 anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check9'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa9'] + $produto['odonto_valor'] : $produto['faixa9'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- 59 ou + anos -->
                        <tr style="width: 100%;">
                            <td class="text-center idades">59 ou + anos</td>
                            <?php foreach ($grupos as $produto) { ?>
                                <td class="centralizada" style="margin: 3px; text-align: center;">
                                    <?php if ($produto['check10'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Comercialização suspensa</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto['faixa10'] + $produto['odonto_valor'] : $produto['faixa10'], ['before' => 'R$ ', 'places' => 2]) ?>
                                    <?php endif; ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <!-- FIM - PREÇOS -->
                    </table>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if (!empty($operadora['grupo_entidades'])) : ?>
                <div class="nobreak">
                    <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                        ENTIDADES E PROFISSÕES
                    </div>
                    <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                        <?php foreach ($operadora['grupo_entidades'] as $key => $value) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= !empty($value['data']) ? $value['data'] : "<div class='info'>Consulte Operadora</div>"  ?>
                            <?= count($operadora['grupo_entidades']) > 1 ? '<hr>' : '' ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            <?php endif; ?>

            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    OBSERVAÇÕES IMPORTANTES
                </div>
                <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                    <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong>
                    <br><br>
                    <?php foreach ($operadora['observacoes'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>

            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    REDE REFERENCIADA (Resumo) <span><?= (isset($operadora['operadora']['url_rede']) && !is_null($operadora['operadora']['url_rede'])) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['operadora']['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php foreach ($operadora['redes'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>

            </div>
            <div class="clearfix">&nbsp;</div>


            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">

                <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                </div>

                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php foreach ($operadora['reembolsos'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['reembolsos']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>

            </div>



            <div class="clearfix">&nbsp;</div>

            <!-- COMERCIALIZACAO -->
            <div class="nobreak">
                <div class="clearfix">&nbsp;</div>
                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; width: 100%;max-width: 100% !important">
                    ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
                </div>
                <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 100%;max-width: 100% !important">
                    <strong>Areas de Atendimento</strong>
                    <br>
                    <?php foreach ($operadora['atendimentos'] as $tipo => $tabelas) {
                        echo '<strong>' . $tipo  . '</strong>: ' . $tabelas . '<br>';
                    } ?>
                    <br>
                    <strong>Areas de Comercialização</strong>
                    <?php if (!empty($operadora['areas_comercializacoes'])) {
                        foreach ($operadora['areas_comercializacoes'] as $areas_comercializacoes) {
                            echo '</br></br><strong>Produtos: ' . substr($areas_comercializacoes['tabelas'], 0, -5) . '</strong></br>' . substr($areas_comercializacoes['data'], 0, -2) . '.';
                        }
                    } else {
                        echo "<div class='info'>Consulte Operadora</div>";
                    }
                    ?>
                </div>
            </div>
            <!-- /COMERCIALIZACAO -->

            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">

                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;">
                    CARÊNCIAS (Resumo)
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php foreach ($operadora['carencias'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['carencias']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>


            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    DEPENDENTES
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php foreach ($operadora['dependentes'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['dependentes']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
                <div class="clearfix">&nbsp;</div>

            </div>

            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                    DOCUMENTOS NECESSÁRIOS
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php foreach ($operadora['documentos'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['documentos']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    FORMAS DE PAGAMENTO
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php foreach ($operadora['pagamentos'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['pagamentos']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>


        <?php endforeach; ?>

    <?php } else {
        echo "<div class='col-xs-12'>Nenhuma Tabela se aplica as Características selecionadas</div>";
    } ?>
    <script type="text/javascript">
        $('#myTabs a').click(function(e) {
            e.preventDefault()
            $(this).tab('show')
        });
        $("#aaa").click(function() {
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelasGeradas/pdf",
                data: $("#gerador").serialize(),
                success: function(data) {
                    $("#ss").empty();
                    $("#ss").append(data);
                }
            });
        });
    </script>

<?php
endif;
?>
<div class="spacer-md">&nbsp;</div>