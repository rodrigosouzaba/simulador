<?php
$controller = $this->request->controller;
$action = $this->request->action;
$direction = $this->request->query('direction') ? $this->request->query('direction') == 'asc' ? 'desc' : 'asc' : 'asc';
?>
<script>
    function order(sort, div) {
        $.ajax({
            type: "POST",
            url: "<?= $this->request->webroot . $controller . '/' . $action . '?sort=' ?>" + sort + "<?= '&direction=' . $direction ?>",
            data: {
                'estado_id': $("#estado-id").val(),
            },
            success: function(data) {
                $("#" + div).empty();
                $("#" + div).append(data);
            }
        })
    }
</script>
