 <div class="paginator">
     <ul class="pagination">
         <?= $this->Paginator->first(__('Primeira', true)); ?>
         <?= $this->Paginator->prev('< ' . __('')) ?>
         <?= $this->Paginator->numbers() ?>
         <?= $this->Paginator->next(__('') . ' >') ?>
         <?= $this->Paginator->last(__('Última', true), array('class' => 'disabled')); ?>
     </ul>
     <p><?= $this->Paginator->counter() ?></p>
 </div>