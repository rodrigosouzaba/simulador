<!-- Modal -->
<div id="modal-rede-credenciada" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0px; padding-bottom: 15px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row" style="margin: 0px;">
                    <div class="col-xs-12 centralizada">
                        <div class="col-md-6" style="padding-left: 0px;">
                            <?= $this->Form->input('estado', ['options' => $estadoPesquisa, 'label' => false, 'class' => 'fonteReduzida', "empty" => 'SELECIONE Estado']) ?>
                        </div>
                        <div class="col-md-6" style="padding-right: 0px;">
                            <?= $this->Form->input('ramo', ['options' => ["PF" => "Saúde Pessoa Física", "PME" => "Saúde PME"], 'label' => false, 'empty' => 'SELECIONE Ramo', 'class' => 'fonteReduzida']) ?>
                        </div>
                        <div clas="clearfix">&nbsp</div>
                        <div class="col-xs-12" style="padding: 0px;">
                            <div class="input-group" id="input-group" style="display: none;">
                                <?= $this->Form->input('rede-input', ['class' => 'form-control', 'label' => false, 'placeholder' => "Pesquisar em Rede Credenciada", "spellcheck" => "true",  "style" => "text-align: center;"]); ?>
                                <span class="input-group-btn">
                                    <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'fas fa-search', 'id' => 'icon-search', 'style' => 'height: 20px; width: 20px;']), ['class' => 'btn btn-default', 'style' => 'height: 37px;', 'id' => 'rede-submit']) ?>
                                </span>
                            </div>
                        </div>
                        <div clas="clearfix">&nbsp</div>
                        <div id="error"></div>
                        <div id="rede-resposta" style="height: 500px; overflow-y: auto;margin-right: -24px;padding-right: 8px;margin-top: 15px;">
                            <i class="fas fa-hospital" style="font-size: 150px; margin: 25%; opacity: 0.1;"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

