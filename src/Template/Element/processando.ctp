<style>
    .image {
        width: 50px;
        -webkit-animation: spin 1s linear infinite;
        -moz-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @-moz-keyframes spin {
        100% {
            -moz-transform: rotate(360deg);
        }
    }

    @-webkit-keyframes spin {
        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    #dialogoModal {
        margin-top: 20%;
        margin-left: 45% !important;
        width: max-content;
        width: -moz-max-content;
    }

    #conteudoModal {
        padding: 10px;
        padding-left: 30px;
        padding-right: 30px;
        width: max-content;
        width: -moz-max-content;
        background-color: #003956;
    }

    #modalProcessando {
        z-index: 1051;
    }
</style>
<!-- Button trigger modal -->
<button type="button" id="loader" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#modalProcessando">
</button>
<!-- Modal -->
<div class="modal fade" data-backdrop="static" data-show="false" id="modalProcessando" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: -120px">
    <div class="modal-dialog" role="document" style="" id="dialogoModal">
        <div class="modal-content" id="conteudoModal">
            <button type="button" class="close fechar" style="display: none" id="fechar" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= $this->Html->image("new_loader.png", ['class' => 'image']) ?>
        </div>
    </div>
</div>
