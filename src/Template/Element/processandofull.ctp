<style>
    .image {
        width: 50px;
        -webkit-animation: spin 1s linear infinite;
        -moz-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @-moz-keyframes spin {
        100% {
            -moz-transform: rotate(360deg);
        }
    }

    @-webkit-keyframes spin {
        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    #dialogoModal {
        margin-top: 20%;
        margin-left: 45% !important;
        width: max-content;
        width: -moz-max-content;
    }

    #conteudoModal {
        padding: 10px;
        padding-left: 30px;
        padding-right: 30px;
        width: max-content;
        width: -moz-max-content;
        background-color: #003956;
    }
</style>
<!-- Button trigger modal -->
<button type="button" id="loaderfull" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#modalFull">
</button>


<div class="modal fade" id="modalFull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: -120px" style="z-index: 9999">
    <div class="modal-dialog" role="document" style="" id="dialogoModal">
        <div class="modal-content" id="conteudoModal">
            <button type="button" class="close fecharfull" style="display: none" id="fecharfull" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= $this->Html->image("new_loader.png", ['class' => 'image']) ?>
        </div>
    </div>
</div>