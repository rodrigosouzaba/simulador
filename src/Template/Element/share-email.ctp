<?php

$controller = $this->request->params['controller'];
$session = $this->request->session();
$sessao = $session->read('Auth.User');
if (!empty($sessao) && $sessao['role'] !== "") :
    ?>
    <div class="modal fade" id="modalCompartilhamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 99999 !important;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body clearfix">
                    <div class="col-xs-12">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="col-xs-12 col-md-6 centralizada">
                        <?=
                            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-envelope fa-2x', 'aria-hidden' => 'true']) . "<br>Enviar para o meu E-mail", "#", ['class' => 'btn btn-lg btn-block btn-default', 'style' => 'border: none !important', 'role' => 'button', 'escape' => false, 'id' => 'pdfemail', "link_id" => $link->id]);
                        ?>
                        <small><?= $sessao["email"] ?></small>

                    </div>
                    <div class="col-xs-12 col-md-6 centralizada">
                        <?=
                            $this->Html->link($this->Html->tag('span', '', [
                                'class' => 'fas fa-share fa-2x',
                                'aria-hidden' => 'true'
                            ]) . '<br>Enviar para <u>outro</u> E-mail', "#", [
                                'class' => 'btn btn-block btn-lg btn-default',
                                'role' => 'button',
                                'escape' => false,
                                'style' => 'border: none !important',
                                "id" => "outroemail"
                            ]);
                        ?>

                    </div>
                    <div class="col-xs-12">&nbsp;</div>

                    <div class="col-xs-12 centralizada" id="fieldShareEmail">
                        <div class="col-xs-12">
                            <?= $this->Form->input('cliente-email', ["label" => false, "placeholder" => "Digite o e-mail", "class" => "form-control centralizada"]); ?>
                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12 centralizada" id="fieldShareEmail">
                            <?= $this->Html->link("Enviar", "#", ['class' => ' btn btn-md btn-primary', "id" => "pdfoutroemail", "link_id" => $link->id, 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false,]); ?>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                    <div class="centralizada col-xs-12" id="responseEmail">
                    </div>
                    <div class="centralizada col-xs-12">
                        <?= $this->Html->image('ciranda.gif', ['alt' => 'Wait...', 'width' => '30px;', "id" => "cirandaGif", "style" => "display: none;", 'class' => 'centralizada']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<script>
    $(document).ready(function() {
        $("#fieldShareEmail").hide();

        $(function() {
            $('[data-tooltip="true"]').tooltip();
        });
        $('[data-toggle="tooltip"]').tooltip();
    });
    // MOSTRA CAMPOS DE ENVIO DE EMAIL
    $("#outroemail").click(function() {
        $("#fieldShareEmail").slideToggle();
    });


    //ENVIA EMAIL PARA USUÁRIO LOGADO
    $("#pdfemail").click(function() {
        $(this).prop("disabled", "disabled");
        $.ajax({
            type: "POST",
            data: {
                _method: "POST",
                tipo: "email",
                email: "<?= $sessao['email'] ?>"
            },
            url: "<?php echo $this->request->webroot ?>links/email/" + $(this).attr("link_id"),
            beforeSend: function() {
                $("#cirandaGif").show();
                $("#pdfemail").attr("disabled", true);
            },
            success: function(data) {
                $("#cirandaGif").hide();
                $("#responseEmail").append("Concluído com sucesso.")
                $("#pdfemail").removeAttr('disabled');
            }
        });
    });

    //ENVIA EMAIL PARA CLIENTE DO USUÁRIO
    $("#pdfoutroemail").click(function() {
        $(this).prop("disabled", "disabled");
        $.ajax({
            type: "POST",
            data: {
                _method: "POST",
                tipo: "email",
                email: $("#cliente-email").val()
            },
            url: "<?php echo $this->request->webroot ?>links/email/" + $(this).attr("link_id"),
            beforeSend: function() {
                $("#cirandaGif").show();
            },
            success: function(data) {
                $("#enviar").removeAttr('disabled');
                $("#cirandaGif").hide();
                $("#responseEmail").append("Concluído com sucesso.")
            }
        });
    });
</script>