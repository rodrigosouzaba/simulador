<?= $this->element('new_acoes') ?>
<style>
    table td {
        border: 0 !important;
    }
</style>
<div class="col-xs-12 container" style="margin-top: 50px;">
    <!-- SE A OPERADORA ESTIVER EM ATUALIZAÇÃO OU OCULTA-->
    <?php if ($operadora['status'] != 'OCULTA' && $operadora['status'] != 'EM ATUALIZAÇÃO') : ?>

        <table border="0">
            <tr>
                <!-- IMAGEM DA OPERADORA-->
                <td style="padding-right: 10px; width: 25%;">
                    <img src="<?= "https://corretorparceiro.com.br/app/" . $operadora["imagen"]["caminho"] . $operadora["imagen"]["nome"]; ?>" style="width: 170px;  position: relative; text-align: center" />
                </td>

                <?php
                $logo = $dados['user']['imagen'];
                $user = $dados['user'];
                if (!empty($logo['id'])) :
                ?>

                    <!-- LOGO DO USUÁRIO -->
                    <td style="padding-right: 10px;width: 25%;">
                        <img src="<?= "https://corretorparceiro.com.br/app/" . $logo["caminho"] . $logo["nome"] ?>" style="max-width: 200px;max-height: 80px; width: auto; position: relative; text-align: center" />
                    </td>

                    <!-- DADOS DO USUÁRIO-->
                    <td style="font-size: 70%; padding: 0 10px;width: 25%;">
                        <b style="text-transform: uppercase"> <?= $user["nome"] . " " . $user["sobrenome"]; ?></b><br>
                        <img src="https://corretorparceiro.com.br/app/img/celular.png" height="15" style="height: 15px !important;" /><b>Celular: </b> <?= $user["celular"]; ?><br>
                        <img src="https://corretorparceiro.com.br/app/img/whatsapp.png" height="15" style="height: 15px !important;" /><b>Whatsapp: </b> <?= $user["whatsapp"]; ?><br>
                        <img src="https://corretorparceiro.com.br/app/img/email.png" height="15" style="height: 15px !important;" /> <b>Email: </b><?= $user["email"]; ?><br>
                        <img src="https://corretorparceiro.com.br/app/img/site.png" height="15" style="height: 15px !important;" /> <b>Site: </b><?= $user["site"]; ?><br>
                    </td>

                    <!-- CASO NÃO TENHA UMA UMA LOGO-->
                <?php else : ?>
                    <td style="width: 55%; text-align: center">
                        <img src="https://corretorparceiro.com.br/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png" style="max-width: 60%; max-height: 80px; position: relative; text-align: center" />
                    </td>
                <?php endif; ?>

                <td style="font-size: 70%; text-align: right;padding-left: 5px; width: 15%; line-height: 12px">
                    <?php
                    switch (date("m")):
                        case "01":
                            $vigencia = "Janeiro";
                            break;
                        case "02":
                            $vigencia = "Fevereiro";
                            break;
                        case "03":
                            $vigencia = "Março";
                            break;
                        case "04":
                            $vigencia = "Abril";
                            break;
                        case "05":
                            $vigencia = "Maio";
                            break;
                        case "06":
                            $vigencia = "Junho";
                            break;
                        case "07":
                            $vigencia = "Julho";
                            break;
                        case "08":
                            $vigencia = "Agosto";
                            break;
                        case "09":
                            $vigencia = "Setembro";
                            break;
                        case "10":
                            $vigencia = "Outubro";
                            break;
                        case "11":
                            $vigencia = "Novembro";
                            break;
                        case "12":
                            $vigencia = "Dezembro";
                            break;
                    endswitch;
                    ?>
                    <?= "<b>Estado: </b>" . $operadora["estado"]["nome"] ?><br>
                    <?= "<b>Vigência: </b>" . $vigencia . "/" . date("Y") ?><br>
                    <?= "<b>Gerado em</b>: " . date("d/m/Y") ?><br>
                    <small><b>*Preços, condições e critérios de aceitação, estão sujeitos a confirmação da operadora no processo
                            de fechamento do contrato</b></small>
                </td>
            </tr>
        </table>
    <?php else : ?>
        <div class="centralizada col-xs-12 text-danger">
            <h1>EM ATUALIZAÇÃO</h1>
        </div>
    <?php endif; ?>
</div>