<div id="slider-wrapper" class="col-xs-12" style="padding-left: 0 !important;padding-right: 0 !important;">
    <div id="layerslider" style="height:250px;">
        <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
            <img class="ls-l" style="top:0px;left:75%;white-space: nowrap; height: 250px !important; width: auto" data-ls="  offsetxin:0;
                 delayin:0;
                 easingin:easeInOutQuart;
                 scalexin:0.7;
                 scaleyin:0.7;
                 offsetxout:-800;
                 durationout:10000;" src="sliderimages/capa3.jpg" alt="">

            <p class="ls-l subtituloSlide" style="top:50px;left:30%;" data-ls="    offsetxin:0;
               durationin:1000;
               delayin:700;
               easingin:easeInOutQuart;
               scalexin:0.5;
               scaleyin:0.5;

               offsetxout:-400;">
                Ferramentas que facilitam<br />
                o trabalho dos corretores
                <!--Faça e organize seus  cálculos com rapidez-->
            </p>

            <p class="ls-l subtituloSlide" style="top:140px;left:30%;text-align: center;" data-ls="    offsetxin:0;
               durationin:1000;
               delayin:1200;
               easingin:easeInOutQuart;
               scalexin:0.5;
               scaleyin:0.5;

               offsetxout:-400;">
                Informações atualizadas <br />em um só lugar


            </p>
        </div>
        <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
            <img class="ls-l" style="top:0px;left:23%;white-space: nowrap; height: 250px !important; width: auto" data-ls="  offsetxin:0;
                 delayin:0;
                 easingin:easeInOutQuart;
                 scalexin:0.7;
                 scaleyin:0.7;
                 offsetxout:-800;
                 durationout:10000;" src="sliderimages/homeslide3.jpg" alt="">

            <p class="ls-l subtituloSlide" style="top:50px;left:65%;text-align: center" data-ls="    offsetxin:0;
               durationin:1000;
               delayin:700;
               easingin:easeInOutQuart;
               scalexin:0.5;
               scaleyin:0.5;

               offsetxout:-400;">
                Tecnologia para <br>facilitar seus negócios
            </p>

            <p class="ls-l subtituloSlide" style="top:140px;left:65%;text-align: center;" data-ls="    offsetxin:0;
               durationin:1000;
               delayin:1200;
               easingin:easeInOutQuart;
               scalexin:0.5;
               scaleyin:0.5;

               offsetxout:-400;">
                Informações restritas somente <br />aos próprios usuários

            </p>
        </div>
        <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
            <img class="ls-l" style="top:0px;left:75%;white-space: nowrap;height: 250px !important; width: auto" data-ls="  offsetxin:0;
                 delayin:0;
                 easingin:easeInOutQuart;
                 scalexin:0.7;
                 scaleyin:0.7;
                 offsetxout:-800;
                 durationout:10000;" src="sliderimages/homeslide5.png" alt="">
            <?=
                $this->Html->image("screenFull.png", array(
                    'class' => 'screen'
                ));
            ?>
            <p class="ls-l subtituloSlide" style="top:50px;left:30%;text-align: center" data-ls="    offsetxin:0;
               durationin:1000;
               delayin:700;
               easingin:easeInOutQuart;
               scalexin:0.5;
               scaleyin:0.5;

               offsetxout:-400;">
                Na palma da mão,<br />no escritório, em qualquer lugar
            </p>

            <p class="ls-l subtituloSlide" style="top:150px;left:30%;text-align: center;" data-ls="    offsetxin:0;
               durationin:1000;
               delayin:1200;
               easingin:easeInOutQuart;
               scalexin:0.5;
               scaleyin:0.5;

               offsetxout:-400;">
                24h por dia, 7 dias por semana

            </p>
            <p class="ls-l tituloSlide" style="top:200px;left:30%; text-align: center" data-ls="   offsetxin:0;
               durationin:1000;
               delayin:1500;
               easingin:easeInOutQuart;
               scalexin:0.5;
               scaleyin:0.5;
               offsetxout:-200;">
            </p>
        </div>

    </div>
</div>
<!--
<div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 centralizada">
    <button type="button" class="btn btn-danger cadastroBanner" data-toggle = "modal" data-target = "#modalCad">Cadastre-se Grátis</button>
</div>
-->

<script type="text/javascript">
    $(document).ready(function() {
        $('#layerslider').layerSlider({
            autoStart: true,
            firstLayer: 1,
            pauseOnHover: false,
            showBarTimer: false,
            showCircleTimer: false,
            skin: 'minimal',
            thumbnailNavigation: 'disabled',
            skinsPath: 'https://corretorparceiro.com.br/app/layerslider/',
            responsiveUnder: 960,
            layersContainer: 960
            // Slider options goes here,
            // please check the 'List of slider options' section in the documentation
        });

        $(".ls-bottom-nav-wrapper").hide();
    });
</script>
