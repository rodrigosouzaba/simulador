<?php foreach ($operadora['tabelas'] as $nome_faixa => $grupos_com_5) : ?>
    <?php foreach ($grupos_com_5 as $grupos) : ?>
        <table class="table table-condensed table-striped">
            <colgroup>
                <col span="1" style="width: 110px;">
            </colgroup>
            <tr style="width: 100%;">
                <td style="max-width: 50px !important; text-align:center; vertical-align: middle">Produto</td>
                <?php foreach ($grupos as $key => $produto) {  ?>
                    <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                        <?= $produto['nome'] ?>
                    </td>
                <?php } ?>
            </tr>
            <tr style="width: 100%;">
                <td style="max-width: 50px !important; text-align:center; vertical-align: middle">Cobertura</td>
                <?php foreach ($grupos as $key => $produto) { ?>
                    <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                        <?= $produto['cobertura']['nome'] ?>
                    </td>
                <?php } ?>
            </tr>
            <tr style="width: 100%;">
                <td style="max-width: 50px !important; text-align:center; vertical-align: middle">Acomodação</td>
                <?php foreach ($grupos as $key => $produto) { ?>
                    <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                        <strong><?= ($produto['acomodacao']['nome'] == 'AMBULATORIAL' ? "SEM ACOMODAÇÃO" : $produto['acomodacao']['nome']) ?></strong>
                    </td>
                <?php } ?>
            </tr>
            <tr style="width: 100%;">
                <td style="max-width: 50px !important; text-align:center; vertical-align: middle">Coparticipação</td>
                <?php foreach ($grupos as $key => $produto) { ?>
                    <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                        <?= ($produto['coparticipacao'] == 's' ? "COM COPARTICIPAÇÃO" . " " . $produto['detalhe_coparticipacao'] : "SEM COPARTICIPAÇÃO") ?>
                        </br>
                        <small>Cód. ANS: <?= $produto['cod_ans'] ?></small>
                    </td>
                <?php } ?>
            </tr>

            <!-- PREÇOS -->
            <?php switch ($nome_faixa) {
                case 'faixa_6':
                    $idades['faixa_extra_1'] = "0 à 14 anos";
                    $idades['faixa_extra_2'] = "15 à 29 anos";
                    $idades['faixa_extra_3'] = "30 à 39 anos";
                    $idades['faixa_extra_4'] = "40 à 49 anos";
                    $idades['faixa_extra_5'] = "50 à 58 anos";
                    $idades['faixa_extra_6'] = "+ de 58 anos";
                    break;
                case 'faixa_3':
                    $idades['faixa_promo_1'] = "18 à 39 anos";
                    $idades['faixa_promo_2'] = "40 à 58 anos";
                    $idades['faixa_promo_3'] = "+ de 59 anos";
                    break;
                default:
                    $idades['faixa1'] = "0 à 18";
                    $idades['faixa2'] = "19 à 23";
                    $idades['faixa3'] = "24 à 28";
                    $idades['faixa4'] = "29 à 33";
                    $idades['faixa5'] = "34 à 38";
                    $idades['faixa6'] = "39 à 43";
                    $idades['faixa7'] = "44 à 48";
                    $idades['faixa8'] = "49 à 53";
                    $idades['faixa9'] = "54 à 58";
                    $idades['faixa10'] = "59 ou +";
                    break;
            }
            foreach ($idades as $key => $idade) : ?>
                <tr style="width: 100%;">
                    <td class="text-center idades"><?= $idade ?></td>
                    <?php foreach ($grupos as $produto) : ?>
                        <td class="centralizada" style="margin: 3px; text-align: center;">
                            <?php if ($produto[$key] == 1) : ?>
                                <strong>Comercialização suspensa</strong>
                            <?php elseif ($produto['atualizacao'] == 1) : ?>
                                <strong>Comercialização suspensa</strong>
                            <?php else : ?>
                                <?= $this->Number->format(!empty($produto['odonto_valor']) ? $produto[$key] + $produto['odonto_valor'] : $produto[$key], ['before' => 'R$ ', 'places' => 2]) ?>
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            <!-- FIM - PREÇOS -->

        </table>
    <?php
    endforeach;
    $idades = []; ?>
<?php endforeach; ?>