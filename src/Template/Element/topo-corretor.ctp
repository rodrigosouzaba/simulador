<!--
<div class="menu-topo">
    <div class="contatos">
         <i class="fa fa-phone" title="Fale Conosco" aria-hidden="true"></i>(71) 2105-0600
    </div>
    <div class="cadastro">
        <a href="/fale-conosco" style=" margin-right: 10px !important; color:#004057 !important;">
            <i class="fa fa-at" aria-hidden="true"></i> FALE CONOSCO
        </a>
        <a href="#"  style=" margin-right: 10px !important; color:#004057 !important;" data-toggle="modal" data-target="#myModalNewsletter">
            <i class="fa fa-envelope" aria-hidden="true"></i> NEWSLETTER
        </a>
        <a href="/pt-br/cadastre-se-conosco" style="margin-right: 15px !important; color:#004057 !important;">
            <i class="fa fa-briefcase" title="Cadastre-se Conosco" aria-hidden="true"></i> SEJA NOSSO PARCEIRO
        </a>

    </div>
</div>
-->
<style>
	.frases-area-corretor {
		font-size: 20px;
		font-variant: small-caps;
		white-space: nowrap;
		font-weight: 400;
		text-align: center !important;
		color: #fff !important;
		padding: 8px 0 0 20px;
	}

	.pipe {
		font-size: 60%;
	}

	.teste {
		text-transform: uppercase;
		font-size: 90%;
		margin-right: 10px;
	}

	.logado {
		font-size: 90%;
		padding: 10px 0;
	}

	#divSlideTopo {
		padding-left: 0 !important;
	}

	#slider-wrapper-topo {
		height: 40px;
		overflow: hidden;
	}

	#area-usuario {
		z-index: 9999;
	}
</style>

<div id="area-usuario" class="col-xs-12" style="background-color: #003956; padding: 0 !important">
	<div class="col-xs-12 col-md-6 col-md-offset-3" id="divSlideTopo">
		<div class="col-xs-12" style="padding:  0 !important">
			<div class="col-xs-12" style="padding: 0 !important">
				<div id="slider-wrapper-topo">
					<div id="slide-area-corretor">

						<div class="ls-slide" data-ls="transition2d:5;slidedelay:2500" style="width: 100%;text-align: center !important">
							<p class="ls-l frases-area-corretor" style="top: 0px;left:0;width: 100%; text-align: center !important" data-ls="
		                       durationin:1000;">
								sempre trabalhando para você vender mais e melhor
							</p>
						</div>

						<div class="ls-slide" data-ls="transition2d:5;slidedelay:2500" style="width: 100%;">
							<p class="ls-l frases-area-corretor" style="top:0px;left:0;width: 100%; text-align: center !important" data-ls="
		                       durationin:1000;">
								agilidade para suas vendas
							</p>
						</div>
						<div class="ls-slide" data-ls="transition2d:5;slidedelay:2500" style="width: 100%;">
							<p class="ls-l frases-area-corretor" style="top:0px;left:0;width: 100%; text-align: center !important" data-ls="
		                       durationin:1000;
		                       color: #D9534F;">
								para corretores modernos e eficientes
							</p>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-3" style="text-align: right !important; color: #fff" id="btnModal">
		<a href="#" class="btn btn-xs btn-default" data-backdrop="static" role="button" style="min-width: 24px !important; padding-top: 2px;margin:10px 0 !important" data-toggle="modal" data-target="#modalLogin"><span class="far fa-user" aria-hidden="true" style="margin-right: 2px;"></span>
			Área do Corretor
		</a>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm modal-login" role="document" style="margin-right: 15px !important; margin-top: 0">
		<div class="modal-content" style="box-shadow: none !important;">
			<div class="modal-body" style="text-align: center">
				<!-- 		    <form method="post" accept-charset="utf-8" id="login" class="form-inline" action="https://corretorparceiro.com.br/app/users/login"> -->
				<form method="post" accept-charset="utf-8" id="login" class="form-inline" action="http://localhost:8888/simulador/users/login">
					<div style="display:none;">
						<input type="hidden" name="_method" value="POST">
					</div>
					<div class="row" style="width: auto !important">
						<div class="form-group col-xs-12">
							<div class="input text">
								<input type="text" name="username" placeholder="CPF" id="username" style="text-align: center">
							</div>
						</div>
					</div>
					<div class="row" style="width: auto !important">
						<div class="form-group col-xs-12">
							<div class="input password">
								<input type="password" name="password" placeholder="Senha" id="password" style="text-align: center">
							</div>
						</div>
					</div>
					<div class="row" style="width: auto !important">
						<div class="col-xs-12">
							Esqueceu sua senha? <a href="#" style="color: #D9534F !important" data-toggle="modal" data-target="#modal-senha-add" id="senha">Clique Aqui</a>
						</div>
					</div>
					<div class="row" style="width: auto !important">
						<div class="col-xs-12">
							Não possui cadastro? <a href="#" style="color: #D9534F !important" id="cadastro" data-toggle="modal" data-target="#modal-senha-add">Clique Aqui</a>
						</div>
					</div>

					<div class="row" style="width: auto !important;margin-top: 16px">
						<div class="col-xs-12" style="text-align: center !important">
							<button class="btn btn-md btn-primary " target="_blank" role="button" data-toggle="tooltip" data-placement="top" title="" id="logar" type="submit" data-original-title="Login">
								Entrar
							</button>
						</div>
					</div>


				</form>
			</div>
		</div>
	</div>
</div>

<!-- Modal Senha e ADD-->
<div class="modal fade" id="modal-senha-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" style="margin-top: 0">
		<div class="modal-content" style="box-shadow: none !important;">
			<div class="modal-body" style="text-align: center" id="resposta-modal">

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		/*
			        var request = new XMLHttpRequest();

			request.open('GET', 'http://localhost:8888/simulador/users/checkSession.json', true);
			request.onload = function () {
			var div = document.getElementById('btnModal');
			  // Begin accessing JSON data here
			  var data = JSON.parse(this.response);
			  console.log(data);
			  for(x in data){
				  if(data["sessao"] != null){
					  div.innerHTML = "<div class='logado'><span class='teste' >Versão de Teste</span> Olá <b>"+data["sessao"]["nome"]+ "</b>  <a href='<?= $this->request->webroot . "users/view/" ?>" + data["sessao"]["id"]+"' style='color:#fff; margin:0 8px'><i class='far fa-user'></i> Meu Cadastro</a>  <a href='<?= $this->request->webroot . 'users/logout' ?>' style='color:#fff !important;'> <img src='<?= $this->request->webroot . "img/sair.png" ?>' style='height: 15px;margin-right: 2px; '/> Sair</a></div>";
				  }
			  }

			}

			request.send();
		*/


		$('.cpf').mask('999.999.999-99');
		var largura = $(window).width();
		var altura = $("#area-usuario").height();
		$(".modal").css("margin-top", altura + 'px');
		// 	    console.log(largura);
		if (largura < 500) {
			$('#divSlide').css("text-align", 'center');
			$('#btnModal').css("textAlign", 'center');
		}
	});


	$('[data-toggle="popover"]').popover();

	$('#slide-area-corretor').layerSlider({
		autoStart: true,
		responsiveUnder: 500,
		firstLayer: 1,
		pauseOnHover: false,
		showBarTimer: false,
		showCircleTimer: false,
		skin: 'noskin',
		skinsPath: '<?= $this->request->webroot . '/simulador/layerslider/'; ?>',
	});

	/*
	            $('#textohome').layerSlider({
	            autoStart: true,
	            firstLayer: 1,
	            pauseOnHover: false,
	            showBarTimer: false,
	            showCircleTimer: false,
	            skin: 'noskin',
	            skinsPath: '<?= $this->request->weroot . '/simulador/layerslider/'; ?>',
	            responsiveUnder: 960,
	//            layersContainer: 960
	            // Slider options goes here,
	            // please check the 'List of slider options' section in the documentation
	        });
	*/









	$("#cadastro").click(function() {
		$.ajax({
			type: "get",
			//                 data: $("#a").serialize(),
			url: "<?php echo $this->request->webroot ?>users/add/",
			success: function(data) {
				$("#resposta-modal").empty();
				$("#resposta-modal").append(data);

			}
		});
	});
	$("#senha").click(function() {
		$.ajax({
			type: "get",
			//                 data: $("#a").serialize(),
			url: "<?php echo $this->request->webroot ?>users/recuperar-senha/",
			success: function(data) {
				$("#resposta-modal").empty();
				$("#resposta-modal").append(data);

			}
		});
	});
</script>
