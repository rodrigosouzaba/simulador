<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $filtro->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $filtro->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Filtros'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['controller' => 'TiposProdutos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['controller' => 'TiposProdutos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas Filtradas'), ['controller' => 'TabelasFiltradas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabelas Filtrada'), ['controller' => 'TabelasFiltradas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="filtros form large-9 medium-8 columns content">
    <?= $this->Form->create($filtro) ?>
    <fieldset>
        <legend><?= __('Edit Filtro') ?></legend>
        <?php
            echo $this->Form->input('simulacao_id', ['options' => $simulacoes, 'empty' => true]);
            echo $this->Form->input('tipo_produto_id', ['options' => $tiposProdutos, 'empty' => true]);
            echo $this->Form->input('tipo_id', ['options' => $tipos, 'empty' => true]);
            echo $this->Form->input('abrangencia_id');
            echo $this->Form->input('regiao_id', ['options' => $regioes, 'empty' => true]);
            echo $this->Form->input('observacao');
            echo $this->Form->input('coparticipacao');
            echo $this->Form->input('informacao');
            echo $this->Form->input('reembolso');
            echo $this->Form->input('carencia');
            echo $this->Form->input('opcional');
            echo $this->Form->input('rede');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
