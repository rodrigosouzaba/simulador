<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Filtro'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['controller' => 'TiposProdutos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['controller' => 'TiposProdutos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas Filtradas'), ['controller' => 'TabelasFiltradas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabelas Filtrada'), ['controller' => 'TabelasFiltradas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="filtros index large-9 medium-8 columns content">
    <h3><?= __('Filtros') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('simulacao_id') ?></th>
                <th><?= $this->Paginator->sort('tipo_produto_id') ?></th>
                <th><?= $this->Paginator->sort('tipo_id') ?></th>
                <th><?= $this->Paginator->sort('abrangencia_id') ?></th>
                <th><?= $this->Paginator->sort('regiao_id') ?></th>
                <th><?= $this->Paginator->sort('observacao') ?></th>
                <th><?= $this->Paginator->sort('coparticipacao') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('informacao') ?></th>
                <th><?= $this->Paginator->sort('reembolso') ?></th>
                <th><?= $this->Paginator->sort('carencia') ?></th>
                <th><?= $this->Paginator->sort('opcional') ?></th>
                <th><?= $this->Paginator->sort('rede') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($filtros as $filtro): ?>
            <tr>
                <td><?= $this->Number->format($filtro->id) ?></td>
                <td><?= $filtro->has('simulaco') ? $this->Html->link($filtro->simulaco->id, ['controller' => 'Simulacoes', 'action' => 'view', $filtro->simulaco->id]) : '' ?></td>
                <td><?= $filtro->has('tipos_produto') ? $this->Html->link($filtro->tipos_produto->id, ['controller' => 'TiposProdutos', 'action' => 'view', $filtro->tipos_produto->id]) : '' ?></td>
                <td><?= $filtro->has('tipo') ? $this->Html->link($filtro->tipo->id, ['controller' => 'Tipos', 'action' => 'view', $filtro->tipo->id]) : '' ?></td>
                <td><?= $this->Number->format($filtro->abrangencia_id) ?></td>
                <td><?= $filtro->has('regio') ? $this->Html->link($filtro->regio->id, ['controller' => 'Regioes', 'action' => 'view', $filtro->regio->id]) : '' ?></td>
                <td><?= h($filtro->observacao) ?></td>
                <td><?= h($filtro->coparticipacao) ?></td>
                <td><?= h($filtro->created) ?></td>
                <td><?= h($filtro->informacao) ?></td>
                <td><?= h($filtro->reembolso) ?></td>
                <td><?= h($filtro->carencia) ?></td>
                <td><?= h($filtro->opcional) ?></td>
                <td><?= h($filtro->rede) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $filtro->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $filtro->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $filtro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $filtro->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
