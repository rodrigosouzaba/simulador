<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Filtro'), ['action' => 'edit', $filtro->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Filtro'), ['action' => 'delete', $filtro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $filtro->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Filtros'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Filtro'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['controller' => 'TiposProdutos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['controller' => 'TiposProdutos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas Filtradas'), ['controller' => 'TabelasFiltradas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabelas Filtrada'), ['controller' => 'TabelasFiltradas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="filtros view large-9 medium-8 columns content">
    <h3><?= h($filtro->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Simulaco') ?></th>
            <td><?= $filtro->has('simulaco') ? $this->Html->link($filtro->simulaco->id, ['controller' => 'Simulacoes', 'action' => 'view', $filtro->simulaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tipos Produto') ?></th>
            <td><?= $filtro->has('tipos_produto') ? $this->Html->link($filtro->tipos_produto->id, ['controller' => 'TiposProdutos', 'action' => 'view', $filtro->tipos_produto->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo') ?></th>
            <td><?= $filtro->has('tipo') ? $this->Html->link($filtro->tipo->id, ['controller' => 'Tipos', 'action' => 'view', $filtro->tipo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Regio') ?></th>
            <td><?= $filtro->has('regio') ? $this->Html->link($filtro->regio->id, ['controller' => 'Regioes', 'action' => 'view', $filtro->regio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Observacao') ?></th>
            <td><?= h($filtro->observacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Coparticipacao') ?></th>
            <td><?= h($filtro->coparticipacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Informacao') ?></th>
            <td><?= h($filtro->informacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Reembolso') ?></th>
            <td><?= h($filtro->reembolso) ?></td>
        </tr>
        <tr>
            <th><?= __('Carencia') ?></th>
            <td><?= h($filtro->carencia) ?></td>
        </tr>
        <tr>
            <th><?= __('Opcional') ?></th>
            <td><?= h($filtro->opcional) ?></td>
        </tr>
        <tr>
            <th><?= __('Rede') ?></th>
            <td><?= h($filtro->rede) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($filtro->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Abrangencia Id') ?></th>
            <td><?= $this->Number->format($filtro->abrangencia_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($filtro->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Tabelas Filtradas') ?></h4>
        <?php if (!empty($filtro->tabelas_filtradas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Filtro Id') ?></th>
                <th><?= __('Tabela Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($filtro->tabelas_filtradas as $tabelasFiltradas): ?>
            <tr>
                <td><?= h($tabelasFiltradas->id) ?></td>
                <td><?= h($tabelasFiltradas->filtro_id) ?></td>
                <td><?= h($tabelasFiltradas->tabela_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TabelasFiltradas', 'action' => 'view', $tabelasFiltradas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TabelasFiltradas', 'action' => 'edit', $tabelasFiltradas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TabelasFiltradas', 'action' => 'delete', $tabelasFiltradas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasFiltradas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
