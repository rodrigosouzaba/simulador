<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Formas Pagamentos'), '/formasPagamentos/index') ?></li>
    </ul>
</nav>
<div class="formasPagamentos form large-9 medium-8 columns content">
    <?= $this->Form->create($formasPagamento) ?>
    <fieldset>
        <legend><?= __('Edit Formas Pagamento') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('operadora_id', ['options' => $operadoras]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
