<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($formasPagamentos as $formaPagamento) : ?>
            <tr>
                <td><?= $formaPagamento->nome ?></td>
                <td><?= $formaPagamento->descricao ?></td>
                <td>
                    <?php
                    if (isset($formaPagamento->operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $formaPagamento->operadora->imagen->caminho . "/" . $formaPagamento->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $formaPagamento->operadora->detalhe;
                    } else {
                        echo $formaPagamento->operadora->nome . " - " . $formaPagamento->operadora->detalhe;
                    }
                    ?>
                </td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']),  "/formasPagamentos/edit/$formaPagamento->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'formasPagamentos.edit']) ?>
                    <?= $this->Form->postLink('', "/formasPagamentos/delete/$formaPagamento->id", ['confirm' => __('Confirma exclusão?', $formaPagamento->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'formasPagamentos.delete'])?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
