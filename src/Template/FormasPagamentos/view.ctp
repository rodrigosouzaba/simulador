<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Formas Pagamento'), ['action' => 'edit', $formasPagamento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Formas Pagamento'), ['action' => 'delete', $formasPagamento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $formasPagamento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Formas Pagamentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Formas Pagamento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Operadoras'), ['controller' => 'Operadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Operadora'), ['controller' => 'Operadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="formasPagamentos view large-9 medium-8 columns content">
    <h3><?= h($formasPagamento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($formasPagamento->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Operadora') ?></th>
            <td><?= $formasPagamento->has('operadora') ? $this->Html->link($formasPagamento->operadora->id, ['controller' => 'Operadoras', 'action' => 'view', $formasPagamento->operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($formasPagamento->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($formasPagamento->descricao)); ?>
    </div>
</div>
