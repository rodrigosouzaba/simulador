<div class="col-xs-12">
    <?= $this->Form->create($funcionalidade) ?>
    <fieldset>
        <div class="col-xs-12">
            <?= $this->Form->input('funcionalidade'); ?>
        </div>
        <div class="col-xs-12">
            <?= $this->Form->input('descricao', ['type' => 'textarea']); ?>
        </div>
        <div class="clearfix">&nbsp</div>
        <div id="grupos_selecao">
            <div class="col-md-5">
                <?php
                $nao_selecionados = [];
                foreach ($grupos as $id => $grupo) {
                    if (!in_array($id, $grupos_selecionados)) {
                        $nao_selecionados[$id] = $grupo;
                    }
                }
                ?>
                <?= $this->Form->input('lista-grupos', ['options' => $nao_selecionados, 'multiple' => 'multiple']) ?>
            </div>
            <div class="col-md-2 centralizada">
                <br />
                <div class="clearfix">&nbsp;</div>
                <?=
                    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-arrow-right', 'aria-hidden' => 'true']), '#sel_prof', ['class' => 'add btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'title' => 'Adicionar']);
                ?>
                <br />
                <br />
                <?=
                    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-arrow-left', 'aria-hidden' => 'true']), '#sel_prof', ['class' => 'remove btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'title' => 'Remover']);
                ?>
            </div>
            <div class="col-md-5">
                <?php
                $selecionados = [];
                foreach ($grupos as $id => $grupo) {
                    if (in_array($id, $grupos_selecionados)) {
                        $selecionados[$id] = $grupo;
                    }
                }
                ?>
                <?= $this->Form->input('grupos._ids', ['options' => $selecionados, 'multiple' => 'multiple', 'label' => 'Grupos permitidos para a Função', 'style' => 'margin-bottom: 0 !important']); ?>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#grupos-ids').children().prop("selected", true);
                    $('#salvar').click(function() {
                        $('#grupos-ids').children().prop("selected", true);
                        $('#grupos-ids option').sort();
                    });
                    $('.add').click(function() {
                        $('#lista-grupos option:selected').appendTo('#grupos-ids');
                        ordenarSelect();
                    });
                    $('.remove').click(function() {
                        $('#grupos-ids option:selected').appendTo('#lista-grupos');
                        ordenarSelect();
                    });
                    $('.add-all').click(function() {
                        $('#lista-grupos option').appendTo('#grupos-ids');
                    });
                    $('.remove-all').click(function() {
                        $('#grupos-ids option').appendTo('#lista-grupos');
                    });
                });

                function ordenarSelect() {
                    $("#grupos-ids").html($("option", $("#grupos-ids")).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
                    }));
                }
            </script>
        </div>
        <div class="clearfix">&nbsp;</div>
    </fieldset>
    <?= $this->element('botoesAdd') ?>
    <?= $this->Form->end() ?>
</div>
