<div class="col-xs-12">
    <?= $this->Form->create($funcionalidade) ?>
    <fieldset>
        <?= $this->Form->input('funcionalidade'); ?>
        <?= $this->Form->input('descricao'); ?>
        <?= $this->Form->input('grupos._ids', ['options' => $grupos]); ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
