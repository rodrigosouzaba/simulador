 <style>
     .group-list {
         display: inline-flex;
         list-style: none;
         margin: 0px;
         flex-wrap: wrap;
     }

     .group-list li {
         width: max-content;
         background-color: #95bcd3;
         margin: 2px;
         padding: 2px 10px;
         border-radius: 5px;
         color: #FFF;
     }

     .group-list li:hover {
         box-shadow: inset 0px 0px 5px -1px black;
         cursor: default;
     }

     .little-close {
         display: none;
         margin: 5px 0px 0px 5px;
     }

     .little-close:hover {
         background-color: gray;
         padding: 1px 3px 2px 3px;
         border-radius: 8px;
         cursor: pointer;
     }

     .group-list li:hover .little-close {
         display: block;
         float: right;
     }

     #titulo-grupos {
         margin: 17px 0px 0px;
     }

     .modal-xl {
         width: 1140px;
     }
 </style>
 <div class="col-xs-12 col-md-4">
     <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Funcionalidade', '#', [
                'id' => 'btn-nova-funcionalidade', 'class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'data-toggle' => 'modal',
                'data-target' => '#modalFuncionalidades',
                'aria-expanded' => "false"
            ]);
        ?>
     <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fas fa-users', 'aria-hidden' => 'true']) . ' Grupos', ['controller' => 'grupos'], [
                'id' => 'link-grupos', 'class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false
            ]);
        ?>
 </div>
 <div class="col-md-4">
     <div class="modulo titulo-grupos">Funcionalidades</div>
 </div>
 <div class="col-md-12">
     <table>
         <thead>
             <tr>
                 <td>Funcionalidade</td>
                 <td>Descrição</td>
                 <td colspan="2">Grupos</td>
                 <td>Actions</td>
             </tr>
         </thead>
         <tbody>
             <?php foreach ($funcoes as $funcao) : ?>
                 <tr>
                     <td><?= $funcao->funcionalidade ?></td>
                     <td><?= $funcao->descricao ?></td>
                     <td colspan="2">
                         <ul class="group-list">
                             <?php foreach ($funcao->grupos as $grupo) : ?>
                                 <li>
                                     <?= $grupo->nome ?><i class="fa fa-times little-close"></i>
                                 </li>
                             <?php endforeach ?>
                         </ul>
                     </td>
                     <td class="actions">
                         <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil']), ['controller' => 'funcionalidades', 'action' => 'edit', $funcao->id], ['class' => 'btn btn-sm btn-default', 'escape' => false]) ?>
                         <?= $this->Form->postLink($this->Html->tag('i', '', ['class' => 'fa fa-trash']), ['controller' => 'funcionalidades', 'action' => 'delete', $funcao->id], ['class' => 'btn btn-sm btn-danger', 'escape' => false], ['confirm' => __('Are you sure you want to delete # {0}?', $funcao->id)]) ?>
                     </td>
                 </tr>
             <?php endforeach ?>
         </tbody>
     </table>
 </div>

 <!-- Modal Funcionalidades-->
 <div class="modal fade" id="modalFuncionalidades" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog modal-xl" role="document">
         <div class="modal-content">
             <div class="modal-header centralizada">
                 <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title" id="myModalLabel">Funcionalidades</h4>
             </div>
             <div class="modal-body">
                 <div id="conteudoModalFuncionalidades" class="row">
                 </div>
             </div>
         </div>
     </div>
 </div>
 <script>
     $('#btn-nova-funcionalidade').click(function() {
         $.ajax({
             type: "get",
             url: "<?= $this->request->webroot ?>funcionalidades/add/",
             success: function(data) {
                 $("#conteudoModalFuncionalidades").empty();
                 $("#conteudoModalFuncionalidades").append(data);
             }
         });
     });
 </script>
