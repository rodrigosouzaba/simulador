<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Funcionalidade'), ['action' => 'edit', $funcionalidade->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Funcionalidade'), ['action' => 'delete', $funcionalidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $funcionalidade->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Funcionalidades'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Funcionalidade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="funcionalidades view large-9 medium-8 columns content">
    <h3><?= h($funcionalidade->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Funcionalidade') ?></th>
            <td><?= h($funcionalidade->funcionalidade) ?></td>
        </tr>
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($funcionalidade->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($funcionalidade->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Grupos') ?></h4>
        <?php if (!empty($funcionalidade->grupos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Grupo Pai Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Is Dad') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($funcionalidade->grupos as $grupos): ?>
            <tr>
                <td><?= h($grupos->id) ?></td>
                <td><?= h($grupos->nome) ?></td>
                <td><?= h($grupos->status) ?></td>
                <td><?= h($grupos->grupo_pai_id) ?></td>
                <td><?= h($grupos->created) ?></td>
                <td><?= h($grupos->is_dad) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Grupos', 'action' => 'view', $grupos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Grupos', 'action' => 'edit', $grupos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Grupos', 'action' => 'delete', $grupos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $grupos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
