    <table cellpadding="0" cellspacing="0" class="table table-condensed table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
<!-- //             <?php debug($user);?> -->
            <tr>
                <td>
	                <?= $user->user->nome ." ".$user->user->sobrenome?><br>
	                <small><?= $user->user->email?></small><br>
	                <small>CPF: <?= $user->user->username?></small>
                </td>
                <td><?= $user->created ?></td>
                <td>
	                <?= 
		                $this->Html->link(
                            $this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']), '#', [
                                'class' => 'btn btn-xs btn-danger remover',
                                'escape' => false,
                                'value' => $user->id,
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Remover Função'
                        ]);
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
