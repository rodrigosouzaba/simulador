<div class="col-md-2 fonteReduzida" >
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Função', [ 'action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'salvar']);
    ?>

</div>
<div class="col-md-4 fonteReduzida" >
    <?= $this->Form->input('funcao_id', ['options' => $funcoes, 'label' => '', 'empty' => 'Selecione a Função']); ?>
</div>

<div class="spacer-md">&nbsp;</div>

<div id="listaUsuarios">
    
</div>
<?= $this->element("processando");?>

<script type="text/javascript">
    $("#funcao-id").change(function(){
	    if($(this).val()){
	        $.ajax({
	            type: "POST",
	            url: "<?php echo $this->request->webroot ?>funcoes/filtro/",
	            data: $("#funcao-id").serialize(),
	            beforeSend: function () {
			        $('#loader').click();
				},
	            success: function (data) {
	                $("#listaUsuarios").empty();
	                $("#listaUsuarios").append(data);
	                $("#fechar").click();
	            }
	        });
        }else{
            $("#listaTabelas").empty();
            $("#odonto-operadora-id").prop("disabled", "disabled");
        }
    });
</script>