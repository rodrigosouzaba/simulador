<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Funcao'), ['action' => 'edit', $funcao->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Funco'), ['action' => 'delete', $funcao->id], ['confirm' => __('Are you sure you want to delete # {0}?', $funcao->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Funcoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Funco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="funcoes view large-9 medium-8 columns content">
    <h3><?= h($funcao->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($funcao->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($funcao->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($funcao->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($funcao->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Pai Id') ?></th>
                <th><?= __('Imagem Id') ?></th>
                <th><?= __('Username') ?></th>
                <th><?= __('Password') ?></th>
                <th><?= __('Role') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Sobrenome') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Validacao') ?></th>
                <th><?= __('Codigo') ?></th>
                <th><?= __('Celular') ?></th>
                <th><?= __('Tipo') ?></th>
                <th><?= __('Ultimologin') ?></th>
                <th><?= __('Ultimocalculo') ?></th>
                <th><?= __('Data Nascimento') ?></th>
                <th><?= __('Site') ?></th>
                <th><?= __('Whatsapp') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Senha Provisoria') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Logado') ?></th>
                <th><?= __('Codigo Email') ?></th>
                <th><?= __('Validacao Email') ?></th>
                <th><?= __('Bloqueio') ?></th>
                <th><?= __('Cnpj') ?></th>
                <th><?= __('Susep') ?></th>
                <th><?= __('Parceiro') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($funcao->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->user_pai_id) ?></td>
                <td><?= h($users->imagem_id) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->role) ?></td>
                <td><?= h($users->nome) ?></td>
                <td><?= h($users->sobrenome) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->validacao) ?></td>
                <td><?= h($users->codigo) ?></td>
                <td><?= h($users->celular) ?></td>
                <td><?= h($users->tipo) ?></td>
                <td><?= h($users->ultimologin) ?></td>
                <td><?= h($users->ultimocalculo) ?></td>
                <td><?= h($users->data_nascimento) ?></td>
                <td><?= h($users->site) ?></td>
                <td><?= h($users->whatsapp) ?></td>
                <td><?= h($users->facebook) ?></td>
                <td><?= h($users->senha_provisoria) ?></td>
                <td><?= h($users->estado_id) ?></td>
                <td><?= h($users->logado) ?></td>
                <td><?= h($users->codigo_email) ?></td>
                <td><?= h($users->validacao_email) ?></td>
                <td><?= h($users->bloqueio) ?></td>
                <td><?= h($users->cnpj) ?></td>
                <td><?= h($users->susep) ?></td>
                <td><?= h($users->parceiro) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
