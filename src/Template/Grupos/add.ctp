<style>
    #grupo_is_dad {
        display: flex;
        height: 79px;
        align-items: center;
        justify-content: center;
    }

    #grupo_is_dad label {
        width: 50%;
        padding: 0;
        margin: 0;
    }

    #is_dad {
        margin: 0;
    }
</style>
<?= $this->Form->create($grupo) ?>
<div class="col-md-2" id="grupo_is_dad">
    <label>Grupo Pai</label><input type="checkbox" name="is_dad" id="is_dad">
</div>
<div class="col-md-6">
    <?= $this->Form->input('nome') ?>
</div>
<div class="col-md-4" id="div_grupo_pai">
    <?= $this->Form->input('grupo_pai_id', ['options' => $grupos, 'empty' => 'Selecione']) ?>
</div>
<?= $this->Form->input('status', ['hidden' => 'true', 'value' => 1, 'label' => false]) ?>
<div class="col-xs-12 centralizada">
    <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => "btn btn-primary btn-md "]) ?>
    <?= $this->Html->link(
        $this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true',]) . ' Cancelar',
        "#",
        ['class' => 'btn btn-md btn-default', 'role' => 'button', "escape" => false, "id" => 'cancelar']
    ) ?>
</div>
<div class='clearfix'>&nbsp;</div>

<?= $this->Form->end() ?>
<script>
    $(document).ready(function() {
        $("#modalProcessando").modal('hide');
        $("#modalGrupos").modal('show');
        $("#is_dad").click(function() {
            if ($(this).is(":checked")) {
                $("#grupo-pai-id").val("");
                $("#div_grupo_pai").hide();
            } else {
                $("#div_grupo_pai").show();
            }
        });
        $("#grupo-pai-id").change(function() {
            if ($(this).val() == "") {
                $("#is_dad").prop("checked", true);
            }
        });
    });
    $("#cancelar").click(function() {
        $("#modalGrupos").modal('hide');
    });
</script>
