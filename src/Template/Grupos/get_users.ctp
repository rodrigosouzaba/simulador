<style>
    .tabela {
        height: 79vh;
        overflow-y: auto;
    }
</style>
<div class="col-xs-12">
    <div class="col-md-4">
        <div class="input-group" style="margin-top: 5px;">
            <input type="text" name="info_busca" class="form-control" placeholder="Pesquisar por Nome, CPF, Email ou Celular" id="info-busca">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" id="encontrar" style="height: 37px"> <i class="fas fa-search"></i> </button>
            </span>
        </div>
    </div>
    <?= $this->Form->create("busca-especial", ["id" => "busca-especial"]); ?>
    <div class="col-md-4">
        <?= $this->Form->input('estado-id', ['options' => $estados, 'label' => '', 'empty' => 'Filtrar por Estado', 'id' => 'estado-id']); ?>
    </div>
    <div class="col-md-4">
        <?php $status_usuarios = array("NOVOS" => "NOVOS", 'ATIVO' => 'ATIVOS', "ATIVADO" => "ATIVADOS", "SEM-USO" => "ATIVADOS SEM USO", 'PARCIAL-PENDENTE' => 'REATIVAÇÃO PARCIAL PENDENTE', 'TOTAL-PENDENTE' => 'REATIVAÇÃO TOTAL PENDENTE', 'INATIVO' => 'ATIVAÇÃO PENDENTE'); ?>
        <?= $this->Form->input('status', ['options' => $status_usuarios, 'label' => '', 'empty' => 'Filtrar por status', 'id' => 'status_usuario']); ?>
    </div>
    <?= $this->Form->end(); ?>
    <table style="margin-bottom: 0;">
        <thead>
            <tr>
                <th>Último Acesso</th>
                <th>Usuário</th>
                <th>Grupos</th>
                <th></th>
            </tr>
        </thead>
    </table>
</div>
<div class="col-xs-12 tabela">
    <table>
        <tbody>
            <?php foreach ($users as $user) : ?>
                <tr>
                    <td><?= $user->ultimologin ?></td>
                    <td>
                        <?= $user->nome . " " . $user->sobrenome ?>
                        <span class="fonteReduzida">
                            <?= $user->username ?><br />
                            <?= $user->estado ?><br />
                            <?= $user->celular ?><br />
                            <?= $user->email ?><br />
                        </span>
                    </td>
                    <td><?= $user->grupos ?></td>
                    <td class="centralizada">
                        <?= $this->Form->input('checkuser', ['type' => 'checkbox', 'label' => false]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>
<script>
    $("#status_usuario").change(function() {
        if ($(this).val()) {
            $("#loader").click();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
                data: $("#busca-especial").serialize(),
                success: function(data) {
                    $("#respostaConsulta").empty();
                    $("#respostaConsulta").append(data);
                    $("#fechar").click();
                }
            });
        }
    });
    $("#estado-id").change(function() {
        $("#loader").click();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
            data: $("#busca-especial").serialize(),
            success: function(data) {
                $("#respostaConsulta").empty();
                $("#respostaConsulta").append(data);
                $("#fechar").click();
            }
        });
    });

    $("#encontrar").click(function() {
        $("#loader").click();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
            data: $("#info-busca").serialize(),
            success: function(data) {
                $("#respostaConsulta").empty();
                $("#respostaConsulta").append(data);
                $("#fechar").click();
            }
        });
    });
</script>
