<style>
    #conteudoModalGrupo {
        max-height: 600px;
    }

    #users {
        overflow-y: auto;
        max-height: 400px;
    }

    .modal-xl {
        width: 1140px;
    }

    .collapse:hover {
        background-color: #FFF !important;
    }

    .membros {
        background-color: #f5f5f5;
    }

    .list-members {
        margin: 0;
    }

    .desvanecendo {
        -webkit-animation: desvanecer 1s linear;
        -moz-animation: desvanecer 1s linear;
        -o-animation: desvanecer 1s linear;
        animation: desvanecer 1s linear;
    }

    .desvanecido {
        opacity: 0.2 !important;
    }

    @-webkit-keyframes desvanecer {
        from {
            opacity: 1;
        }

        to {
            opacity: 0.2;
        }
    }

    @-moz-keyframes desvanecer {
        from {
            opacity: 1;
        }

        to {
            opacity: 0.2;
        }
    }

    @-o-keyframes desvanecer {
        from {
            opacity: 1;
        }

        to {
            opacity: 0.2;
        }
    }

    @keyframes desvanecer {
        from {
            opacity: 1;
        }

        to {
            opacity: 0.2;
        }
    }

    .list-group {
        display: flex;
        flex-wrap: wrap;
    }

    .list-group li {
        list-style: none;
        background-color: #c1eeff;
        width: max-content;
        padding: 0px 5px;
        border-radius: 2px;
        margin: 3px;
    }

    .form-checkbox {
        cursor: auto;
        padding: 3px 4px 0px 4px;
    }

    .form-checkbox input {
        margin: 0;
    }

    #list-users {
        max-height: 80vh;
        overflow-y: auto;
    }

    #titulo-grupos {
        margin: 17px 0px 0px;
    }
</style>
<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Grupo', '#', [
            'id' => 'btn-novo-grupo', 'class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'data-toggle' => 'modal',
            'data-target' => '#modalGrupos',
            'aria-expanded' => "false"
        ]);
    ?>
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fas fa-sitemap', 'aria-hidden' => 'true']) . ' Funcionalidades', ['controller' => 'funcionalidades'], [
            'id' => 'link-funcionalidades', 'class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false
        ]);
    ?>
</div>
<div class="col-md-4 col-md-offset-1 centralizada">
    <div class="modulo titulo-grupos">Grupos</div>
</div>
<div class="col-xs-12">
    <table class="">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('grupo_pai_id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($grupos as $grupo) : ?>
                <?php if ($grupo->is_dad != "on") : ?>
                    <tr>
                        <td><?= $grupo->has('GrupoPai') ? $grupo->GrupoPai->nome : '' ?>
                        <td> <?= $this->Html->link($grupo->nome, "#membros" . $grupo->id, ['data-toggle' => 'collapse', 'aria-expanded' => 'false', 'aria-controls' => 'membros' . $grupo->id]); ?></a></td>
                        </td>
                        <td class="actions">
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), '#', [
                                        'value' => $grupo->id, 'class' => 'btn btn-sm btn-default edit', 'role' => 'button', 'escape' => false,
                                        'title' => 'Editar Grupo'
                                    ]) ?>
                            <?= $this->Form->postLink('', ['action' => 'delete', $grupo->id], ['confirm' => __('Confirma exclusão?', $grupo->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash']) ?>
                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']), '#', ['role' => 'button', 'escape' => false, 'class' => 'btn btn-sm btn-default btn-addMember', 'title' => 'Adicionar Membros']) ?>
                        </td>
                    </tr>
                    <tr id="membros<?= $grupo->id ?>" class="collapse">
                        <td colspan="3" class="membros">
                            <ul class="list-members">
                                <?php
                                        if (count($grupo->users) >= 1) : ?>
                                    <table>
                                        <?php foreach ($grupo->users as $membro) : ?>
                                            <tr>
                                                <td class="centralizada"><?= ucwords(strtolower($membro->nome . " " . $membro->sobrenome)) ?></td>
                                                <td class="centralizada"></td>
                                                <td class="actions">
                                                    <?= $this->Html->link(
                                                                        $this->Html->tag('span', '', ['class' => 'fa fa-user', 'aria-hidden' => 'true']),
                                                                        '#',
                                                                        [
                                                                            'class' => 'btn btn-xs btn-default edit-grupo',
                                                                            'role' => 'button',
                                                                            'escape' => false,
                                                                            'data-toggle' => 'modal',
                                                                            'data-target' => '#myModal',
                                                                            'aria-expanded' => "false", 'title' => 'Editar Grupos', 'value' => $membro->id, "style" => "min-width: 24px !important; margin:1px 0 !important"
                                                                        ]
                                                                    ); ?>
                                                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-edit', 'aria-hidden' => 'true']), ['controller' => 'Users', 'action' => 'edit', $membro->id], ['class' => 'btn btn-xs btn-default cod-celular', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Usuário', "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>

                                                    <!-- ATIVACAO CELULAR -->
                                                    <?php

                                                                    switch ([$membro->validacao]) {
                                                                        case ['ATIVO']:
                                                                            $icone = "fa fa-check";
                                                                            $cor = "success";
                                                                            $title = 'CELULAR ATIVADO';
                                                                            break;
                                                                        case ['INATIVO']:
                                                                            $icone = "fa fa-minus";
                                                                            $cor = "default";
                                                                            $title = 'ATIVAÇÃO PENDENTE<br/><small>Celular inativo</small>';
                                                                            break;
                                                                        case ['PENDENTE']:
                                                                            $icone = "fa fa-times";
                                                                            $cor = "warning";
                                                                            $title = 'REATIVAÇÃO PENDENTE<br/><small>Celular alterado</small>';
                                                                            break;
                                                                    }
                                                                    ?>
                                                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => $icone, 'aria-hidden' => 'true']), ['controller' => 'Users', 'action' => 'reenviarCodigo', $membro->id], ['class' => 'cod-email btn btn-xs btn-' . $cor, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-html' => "true", 'title' => $title, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                                                        <!-- /ATIVACAO CELULAR -->

                                                        <!-- ATIVACAO E-MAIL -->
                                                        <?php
                                                                        switch ([$membro->validacao_email]) {
                                                                            case ['ATIVO']:
                                                                                $iconeEmail = "fa fa-check";
                                                                                $corEmail = "success";
                                                                                $titleEmail = 'E-MAIL ATIVADO';
                                                                                break;
                                                                            case ['INATIVO']:
                                                                                $iconeEmail = "fa fa-minus";
                                                                                $corEmail = "default";
                                                                                $titleEmail = 'INATIVO<br/><small>E-mail inativo</small>';
                                                                                break;
                                                                            case ['PENDENTE']:
                                                                                $iconeEmail = "fa fa-times";
                                                                                $corEmail = "warning";
                                                                                $titleEmail = 'REATIVAÇÃO PENDENTE<br/><small>E-mail alterado</small>';
                                                                                break;
                                                                        }
                                                                        ?>
                                                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => $iconeEmail, 'aria-hidden' => 'true']), ['controller' => 'Users', 'action' => 'reenviarCodigoEmailAdmin', $membro->id], ["value" => $membro->id, 'class' => 'btn btn-xs btn-' . $corEmail, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-html' => "true", 'title' => $titleEmail, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                                                            <!-- /ATIVACAO E-MAIL -->

                                                            <?=
                                                                                $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-key ', 'aria-hidden' => 'true']), ['controller' => 'Users', 'action' => 'recuperarPadrao', $membro->username], ['class' => 'btn btn-xs btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Senha Provisória', "style" => "min-width: 24px !important; margin:1px 0 !important"]);
                                                                            ?>


                                                            <?php
                                                                            switch ($membro->bloqueio) {
                                                                                case "S":
                                                                                    $iconeBloqueio = "fa fa-lock";
                                                                                    $acaoBloqueio = "DESBLOQUEIO";
                                                                                    $title = "Desbloquear Usuário";
                                                                                    break;
                                                                                case "N":
                                                                                    $acaoBloqueio = "BLOQUEIO";
                                                                                    $iconeBloqueio = "fa fa-unlock";
                                                                                    $title = "Bloquear Usuário";
                                                                                    break;
                                                                            }
                                                                            ?>
                                                                <?= $this->Html->link($this->Html->tag('span', '', ['class' => $iconeBloqueio, 'aria-hidden' => 'true']), '#', ['class' => 'btn btn-xs btn-default bloqueio', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $title, 'value' => $membro->id, "acao" => $acaoBloqueio, "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                                                                <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'fa fa-close', 'aria-hidden' => 'true']), ['class' => 'btn btn-xs btn-danger remover', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Remover do Grupo', 'user' => $membro->id, 'grupo' => $grupo->id, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                <?php else : ?>
                                    <div class="centralizada" style="width: 100%;">
                                        <span class="text-danger">Grupo Vazio!</span>
                                    </div>
                                <?php endif; ?>
                            </ul>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<!-- Modal FILTRO-->
<div class="modal fade" id="modalGrupos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Grupo </h4>
            </div>
            <div class="modal-body" id="conteudoModalGrupo"></div>
        </div>
    </div>
</div>

<!-- Modal ADD MEMBER-->
<div class="modal fade" id="addMember" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content" style="padding: 5px;">
            <div class="modal-header centralizada">
                <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Adicionar Membro </h4>
            </div>
            <div class="modal-body" id="list-users"></div>
            <div class="modal-footer">
                <?= $this->element('botoesAdd') ?>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(".btn-addMember").click(function() {
        $("#list-users").empty();
        $("#modalProcessando").modal("show");
        $.ajax({
            type: 'GET',
            url: '<?= $this->request->webroot ?>grupos/get_users',
            success: function(data) {
                $("#modalProcessando").modal("hide");
                $("#list-users").append(data);
                $("#addMember").modal("show");
            }
        });
    });

    $('.edit').click(function() {
        $.ajax({
            type: "get",
            url: "<?= $this->request->webroot ?>grupos/edit/" + $(this).attr('value'),
            beforeSend: function() {
                $("#modalProcessando").modal('show');
            },
            success: function(data) {
                $(".modal-dialog").addClass("modal-xl");
                $("#conteudoModalGrupo").empty();
                $("#conteudoModalGrupo").append(data);
            }
        });
    });

    $('#btn-novo-grupo').click(function() {
        $.ajax({
            type: "get",
            url: "<?= $this->request->webroot ?>grupos/add/",
            success: function(data) {
                $("#conteudoModalGrupo").empty();
                $("#conteudoModalGrupo").append(data);
            }
        });
    });


    $(".remover").click(function() {
        let coluna = $(this).parent(".actions");
        let row = $(this).parent(".actions").parent("tr");
        let user = $(this).attr('user');
        let grupo = $(this).attr('grupo');
        row.addClass("desvanecendo");
        setInterval(function() {
            row.addClass("desvanecido");
            coluna.html("<span class='text-danger small'>Usuário Removido do Grupo</span>");
        }, 1000);
        $.ajax({
            type: 'POST',
            data: {
                'user': user,
                'grupo': grupo,
            },
            url: '<?= $this->request->webroot . $this->request->controller ?>/remove-user',
            error: function() {
                coluna.html("<span class='text-danger small'>Erro ao Excluir usuário</span>");
            }
        });
    });
</script>
