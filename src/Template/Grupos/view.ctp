<div class="grupos view large-9 medium-8 columns content">
    <h3><?= h($grupo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($grupo->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Grupo') ?></th>
            <td><?= $grupo->has('grupo') ? $this->Html->link($grupo->grupo->id, ['controller' => 'Grupos', 'action' => 'view', $grupo->grupo->id]) : '' ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($grupo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($grupo->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $grupo->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>