<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $gruposFuncionalidade->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $gruposFuncionalidade->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Grupos Funcionalidades'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Funcionalidades'), ['controller' => 'Funcionalidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Funcionalidade'), ['controller' => 'Funcionalidades', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="gruposFuncionalidades form large-9 medium-8 columns content">
    <?= $this->Form->create($gruposFuncionalidade) ?>
    <fieldset>
        <legend><?= __('Edit Grupos Funcionalidade') ?></legend>
        <?php
            echo $this->Form->input('grupo_id', ['options' => $grupos]);
            echo $this->Form->input('funcionalidade_id', ['options' => $funcionalidades]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
