<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Grupos Funcionalidade'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Funcionalidades'), ['controller' => 'Funcionalidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Funcionalidade'), ['controller' => 'Funcionalidades', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="gruposFuncionalidades index large-9 medium-8 columns content">
    <h3><?= __('Grupos Funcionalidades') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('grupo_id') ?></th>
                <th><?= $this->Paginator->sort('funcionalidade_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($gruposFuncionalidades as $gruposFuncionalidade): ?>
            <tr>
                <td><?= $this->Number->format($gruposFuncionalidade->id) ?></td>
                <td><?= $gruposFuncionalidade->has('grupo') ? $this->Html->link($gruposFuncionalidade->grupo->id, ['controller' => 'Grupos', 'action' => 'view', $gruposFuncionalidade->grupo->id]) : '' ?></td>
                <td><?= $gruposFuncionalidade->has('funcionalidade') ? $this->Html->link($gruposFuncionalidade->funcionalidade->id, ['controller' => 'Funcionalidades', 'action' => 'view', $gruposFuncionalidade->funcionalidade->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $gruposFuncionalidade->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gruposFuncionalidade->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gruposFuncionalidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gruposFuncionalidade->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
