<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Grupos Funcionalidade'), ['action' => 'edit', $gruposFuncionalidade->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Grupos Funcionalidade'), ['action' => 'delete', $gruposFuncionalidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gruposFuncionalidade->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Grupos Funcionalidades'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Grupos Funcionalidade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Funcionalidades'), ['controller' => 'Funcionalidades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Funcionalidade'), ['controller' => 'Funcionalidades', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="gruposFuncionalidades view large-9 medium-8 columns content">
    <h3><?= h($gruposFuncionalidade->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Grupo') ?></th>
            <td><?= $gruposFuncionalidade->has('grupo') ? $this->Html->link($gruposFuncionalidade->grupo->id, ['controller' => 'Grupos', 'action' => 'view', $gruposFuncionalidade->grupo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Funcionalidade') ?></th>
            <td><?= $gruposFuncionalidade->has('funcionalidade') ? $this->Html->link($gruposFuncionalidade->funcionalidade->id, ['controller' => 'Funcionalidades', 'action' => 'view', $gruposFuncionalidade->funcionalidade->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($gruposFuncionalidade->id) ?></td>
        </tr>
    </table>
</div>
