<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Grupos Users Link'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="gruposUsersLinks index large-9 medium-8 columns content">
    <h3><?= __('Grupos Users Links') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('grupo_id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('link') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($gruposUsersLinks as $gruposUsersLink): ?>
            <tr>
                <td><?= $this->Number->format($gruposUsersLink->id) ?></td>
                <td><?= $gruposUsersLink->has('grupo') ? $this->Html->link($gruposUsersLink->grupo->id, ['controller' => 'Grupos', 'action' => 'view', $gruposUsersLink->grupo->id]) : '' ?></td>
                <td><?= $gruposUsersLink->has('user') ? $this->Html->link($gruposUsersLink->user->id, ['controller' => 'Users', 'action' => 'view', $gruposUsersLink->user->id]) : '' ?></td>
                <td><?= h($gruposUsersLink->link) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $gruposUsersLink->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gruposUsersLink->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gruposUsersLink->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gruposUsersLink->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
