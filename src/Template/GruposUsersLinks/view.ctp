<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Grupos Users Link'), ['action' => 'edit', $gruposUsersLink->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Grupos Users Link'), ['action' => 'delete', $gruposUsersLink->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gruposUsersLink->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Grupos Users Links'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Grupos Users Link'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="gruposUsersLinks view large-9 medium-8 columns content">
    <h3><?= h($gruposUsersLink->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Grupo') ?></th>
            <td><?= $gruposUsersLink->has('grupo') ? $this->Html->link($gruposUsersLink->grupo->id, ['controller' => 'Grupos', 'action' => 'view', $gruposUsersLink->grupo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $gruposUsersLink->has('user') ? $this->Html->link($gruposUsersLink->user->id, ['controller' => 'Users', 'action' => 'view', $gruposUsersLink->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Link') ?></th>
            <td><?= h($gruposUsersLink->link) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($gruposUsersLink->id) ?></td>
        </tr>
    </table>
</div>
