<?php $this->assign('title', $title); ?>
<?= $this->Form->create($carencia) ?>
<?php
echo $this->Form->input('nome');
echo $this->Form->input('descricao');
?>
<div class="clearfix">&nbsp;</div>
<div class="col-md-12 centralizada fonteReduzida">
    <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => "btn btn-primary btn-md "]) ?>


    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['action' => 'index'], ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false]);
    ?>
</div>
<?= $this->Form->end() ?>
