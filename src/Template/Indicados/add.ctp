<style type="text/css">
.required > label::after {
    content: '';
    color: white;
}
</style>
<div>
    <h4>Gostaria de convidar alguém para experimentar o sistema?</h4>
    <?= $this->Form->create($indicado, ['id' => 'formIndicacao']) ?>
    <fieldset>
      <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('nome', ['label' => '', 'placeholder' => 'Nome', 'required' => 'required', 'class' => 'centralizada']) ?>
    </div>

    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('celular', ['label' => '', 'placeholder' => 'Celular', 'required' => 'required', 'class' => 'centralizada']) ?>
    </div>

    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('email', ['label' => '', 'placeholder' => 'E-mail', 'type' => 'text', 'required' => 'required', 'class' => 'centralizada']) ?>
    </div>

    <?= $this->Form->input('user_id', ['hidden' => true, 'value' => $this->request->session()->read('Auth.User')['id'], 'label' => '']) ?> 
    <div class="col-xs-12"><?= $this->Form->button('indicar', ['hidden' => true, 'id' => 'indicar']); ?></div>
</fieldset>
<?= $this->Form->end() ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#celular").mask("(99) 9 9999-9999");
    });
</script>
