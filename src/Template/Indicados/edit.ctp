<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $indicado->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $indicado->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Indicados'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="indicados form large-9 medium-8 columns content">
    <?= $this->Form->create($indicado) ?>
    <fieldset>
        <legend><?= __('Edit Indicado') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('celular');
            echo $this->Form->input('email');
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
