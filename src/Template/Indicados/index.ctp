<div class="col-md-12">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('user_id',['label' => 'Indicador']) ?></th>
                <th><?= $this->Paginator->sort('nome',['label' => 'Indicado']) ?></th>
                <th><?= $this->Paginator->sort('celular') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('created', ['label' => 'Data de indicação']) ?></th>
<!--                <th class="actions"><?= __('Actions') ?></th>-->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($indicados as $indicado): ?>
            <tr>
                <td><?= $indicado->user->nome." ".$indicado->user->sobrenome  ?></td>
                <td><?= h($indicado->nome) ?></td>
                <td><?= h($indicado->celular) ?></td>
                <td><?= h($indicado->email) ?></td>
                <td><?= h($indicado->created) ?></td>
<!--                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $indicado->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $indicado->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $indicado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $indicado->id)]) ?>
                </td>-->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>