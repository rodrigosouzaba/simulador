<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Indicado'), ['action' => 'edit', $indicado->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Indicado'), ['action' => 'delete', $indicado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $indicado->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Indicados'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Indicado'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="indicados view large-9 medium-8 columns content">
    <h3><?= h($indicado->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($indicado->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Celular') ?></th>
            <td><?= h($indicado->celular) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($indicado->email) ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $indicado->has('user') ? $this->Html->link($indicado->user->id, ['controller' => 'Users', 'action' => 'view', $indicado->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($indicado->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($indicado->created) ?></td>
        </tr>
    </table>
</div>
