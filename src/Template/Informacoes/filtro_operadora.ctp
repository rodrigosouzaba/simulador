<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($informacoes as $informacao) : ?>
            <tr>
                <td><?= $informacao->nome ?></td>
                <td><?= $informacao->descricao ?></td>
                <td>
                    <?php
                    if (isset($informacao->operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $informacao->operadora->imagen->caminho . "/" . $informacao->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $informacao->operadora->detalhe;
                    } else {
                        echo $informacao->operadora->nome . " - " . $informacao->operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $informacao->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'informacoes.edit']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $informacao->id], ['confirm' => __('Confirma exclusão?', $informacao->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'informacoes.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
