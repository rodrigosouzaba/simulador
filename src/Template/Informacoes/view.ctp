<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Informaco'), ['action' => 'edit', $informaco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Informaco'), ['action' => 'delete', $informaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $informaco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Informacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Informaco'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="informacoes view large-9 medium-8 columns content">
    <h3><?= h($informaco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($informaco->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($informaco->descricao)); ?>
    </div>
</div>
