<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Calculo Rápido: Multi-Cálculo para Planos de Saúde e Odontológicos';
?>


<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            Calculo Rápido: Multicálculo para Planos de Saúde e Odontológicos

        </title>
        <?= $this->Html->script('jquery-3.1.0.min') ?>
        <?= $this->Html->script('maskInput') ?>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


        <?php
        echo $this->Html->meta('favicon.ico', '/favicon.ico', array(
            'type' => 'icon'
        ));
        ?>

        <?= $this->Html->css('base.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('cake.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('sistema.css', ['fullBase' => true]) ?>

        <?= $this->Html->css('jquery-ui.min.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('jquery-ui.structure.min.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('jquery-ui.theme.min.css', ['fullBase' => true]) ?>


        <!--Latest compiled and minified JavaScript--> 
        <?= $this->Html->css('font-awesome.css', ['fullBase' => true]) ?>
        <?= $this->Html->script('jquery.maskMoney') ?>
        <?= $this->Html->script('jquery-ui.min') ?>
        <?= $this->Html->script('datepicker-pt-BR') ?>
        <?= $this->Html->script('jquery.cpfcnpj.min') ?>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
<!--        <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">-->
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding-right: 0 !important; padding-left: 0 !important">


        <?php
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
//        debug($sessao);
//        die();
        $action = $this->request->action;
        ?>
        <!--<div class="container col-md-12 col-lg-12 col-sm-12 col-xs-12" >-->

        <!--<div class="clearfix">&nbsp;</div>-->
        <div class="header clearfix cabecalho">
            <?php
      
                $classLogo = 'col-xs-12 centralizada';
           
            ?>
            <div class="<?= $classLogo ?>">
                <?php
                $url = '';
                if (isset($sessao['role'])) {

                    switch ($sessao['role']) {
                        case "admin":
                            if ($sessao['validacao'] === 'ATIVO') {
                                $url = array('controller' => 'tabelas', 'action' => 'index');
                            } else {
                                $url = '';
                            }
                            break;
                        case "padrao":
                            if ($sessao['validacao'] === 'ATIVO') {
                                if ($action == 'ativacao') {
                                    $url = '';
                                } else {
                                    $url = array('controller' => 'simulacoes', 'action' => 'add');
                                }
                            } else {
                                $url = '';
                            }
                            break;
                        default :
                            $url = '';
                            break;
                    }
                }
//                debug($sessao);
//                 debug($sessao);
//                 die();

                if ($user['imagem_id'] === null) {
                    

                    echo $this->Html->image("logo.png", array(
                        'url' => $url,
                        'class' => 'logo'
                    ));
                    if ($sessao['validacao'] === 'ATIVO') {
                        ?>
                        <div class="btn-group botaologo" role="group" aria-label="..." >
                            <?=
                            $this->Html->link('Insira seu Logotipo', ['controller' => 'Users', 'action' => 'addImagem', $sessao['id']], ['class' => 'btn btn-sm  btn-danger ', 'role' => 'button', 'escape' => false]);
                            ?>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <img src="<?= $this->request->webroot . $user['imagen']['caminho'] . $user['imagen']['nome'] ?>" class="logoUsuario" />
                    <?php
                }
                ?>

            </div>
    
        </div>



        <div class="clearfix">&nbsp; </div>
        <?= $this->Flash->render() ?>

        <div>
            <?php
                echo $this->fetch('content');
            ?>
        </div>

    </body>

</html>
<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip();
    $(document).ready(function () {
        $.datepicker.setDefaults($.datepicker.regional[ "pt-BR" ]);
        $('#username').focus();
//        $("#username").mask("999.999.999-99");
        $("#celular").mask("(99) 9 9999-9999");

        $(".controle").click(function () {
            $(".fa-chevron-circle-up").toggleClass('fa-chevron-circle-down');
        });

    });
</script>

<!--Start of Tawk.to Script
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/598ca78b1b1bed47ceb03fec/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>-->
<!--End of Tawk.to Script-->
