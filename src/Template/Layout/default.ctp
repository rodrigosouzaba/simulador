<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Calculo Rápido: Multi-Cálculo para Planos de Saúde e Odontológicos';
?>


<!DOCTYPE html>
<html>

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106538674-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-106538674-3');
    </script>

    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Portal do Corretor Parceiro - Corretor Parceiro

    </title>

    <?= $this->Html->script('jqueryNEW') ?>
    <?= $this->Html->script('maskInput') ?>
    <?= $this->Html->script('greensock') ?>
    <?= $this->Html->script('layerslider.transitions') ?>
    <?= $this->Html->script('layerslider.kreaturamedia.jquery') ?>
    <!-- <?= $this->Html->script('bootstrap') ?> -->
    <?= $this->Html->script('bootstrap-notify.min') ?>

    <?php
    echo $this->Html->meta('favicon.ico', '/favicon.ico', array(
        'type' => 'icon'
    ));
    ?>
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
    <?= $this->Html->css('font-awesome/css/fontawesome-all.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('base.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('cake.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('sistema.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('layerslider.css', ['fullBase' => true]) ?>

    <?= $this->Html->css('jquery-ui.min.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('jquery-ui.structure.min.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('jquery-ui.theme.min.css', ['fullBase' => true]) ?>

    <!--Latest compiled and minified JavaScript-->
    <?= $this->Html->css('font-awesome/css/font-awesome.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('bootstrap/bootstrap.min.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('bootstrap/multi-select.css', ['fullBase' => true]) ?>

    <?= $this->Html->css('bootstrap-validator/bootstrapValidator.min.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('toastr/toastr.css', ['fullBase' => true]) ?>

    <!-- SUMMERNOTE TEXTAREA -->
    <?= $this->Html->css('summernote-lite/summernote-lite.css', ['fullBase' => true]) ?>

    <?= $this->Html->css('dropzone/dropzone.css') ?>
    <?= $this->Html->css('dropzone/basic.css') ?>

    <?= $this->Html->css('font/font-family-exo2.css') ?>
    <?= $this->Html->css('font/font-family-open-sans.css') ?>
    <?= $this->Html->css('link.css', ['fullBase' => true]) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <script>
        var baseURL = baseUrl = baseSub = "<?php echo $this->request->webroot; ?>";
        var urlOrigin = window.location.origin;
        var userId = "<?php echo $this->Session->read('Auth.User.id'); ?>";
        var userName = "<?php echo $this->Session->read('Auth.User.nome') . " " . $this->Session->read('Auth.User.sobrenome') ?>";
        var userPhoto = "<?php echo $this->Session->read('Auth.User.photo'); ?>";
        var userPermissions = <?= !empty($this->Session->read('Auth.User.Permission')) ? json_encode($this->Session->read('Auth.User.Permission'))  : json_encode([]); ?>;
    </script>
    <style>
        .note-editable {
            line-height: 1 !important;
            font-size: 12 !important;
        }

        .note-editable p {
            line-height: 1;
            font-size: 12;
        }
    </style>
    <!-- FIM SUMMERNOTE TEXTAREA -->
</head>


<body class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding-right: 0 !important; padding-left: 0 !important">

    <?php
    $session = $this->request->session();
    $sessao = $session->read('Auth.User');
    $action = $this->request->action;
    ?>

    <div id="pre-loader"><img src="https://corretorparceiro.com.br/app/img/logo-icone.png" alt="" /></div>

    <?= $this->element('menu') ?>
    <?= $this->Flash->render() ?>
    <div class="spacer-md">&nbsp;</div>
    <div id="content" class="conteudo col-xs-12">
        <!-- <?= $this->element("menu-configuracao") ?> -->
        <?php
        if ($action != 'loginUser' || $action != 'ativacao' || $action != 'login') {
            echo $this->fetch('content');
        }
        ?>
    </div>

    <?php /*$this->element('modal_avaliacao')*/ ?>
    <?= $this->element('modal_fale_conosco') ?>
    <?= $this->element('modal_fale_conosco', ['id' => 'modal-leads', 'message' => 'Essa funcionalidade só está disponivel para parceiros autorizados']); ?>
    <?= $this->element('modal_user') ?>

    <?= $this->element('layout/modal', ['id' => 'nivel1']); ?>
    <?= $this->element('layout/modal', ['id' => 'nivel2']); ?>
    <?= $this->element('layout/modal', ['id' => 'nivel3']); ?>
    <?= $this->element('layout/modal', ['id' => 'nivel4']); ?>
    <?= $this->element('layout/modal', ['id' => 'nivel5']); ?>


    <?= $this->Html->script('jquery.maskMoney') ?>

    <?= $this->Html->script('jquery-ui.min') ?>
    <?= $this->Html->script('datepicker-pt-BR') ?>
    <?= $this->Html->script('clipboard.min') ?>
    <?= $this->Html->script('jquery.cpfcnpj.min') ?>


    <?= $this->Html->script('jquery.multi-select.js') ?>
    <?= $this->Html->script('jquery.quicksearch.js') ?>

    <?= $this->Html->script('summernote/summernote-lite.js') ?>
    <?= $this->Html->script('summernote/lang/pt-br.js') ?>

    <?= $this->Html->script('bootstrap-validator/bootstrap-validator.min') ?>
    <?= $this->Html->script('toastr/toastr') ?>
    <?= $this->Html->script('moment/moment') ?>
    <?= $this->Html->script('moment/moment-with-locales') ?>
    <?= $this->Html->script('moment/moment-precise-range') ?>

    <?= $this->Html->script('dropzone/dropzone') ?>
    <?= $this->Html->script('dropzone/dropzone-amd-module') ?>

    <?= $this->Html->script('bootstrap.min') ?>

    <?= $this->Html->script('core/App.js') . "\n"; ?>
    <?= $this->Html->script('core/AppNavigation.js') . "\n"; ?>
    <?= $this->Html->script('core/AppVendor.js') . "\n"; ?>
    <?= $this->Html->script('core/AppGrid.js') . "\n"; ?>
    <?= $this->Html->script('core/AppHandleAjaxError.js') . "\n"; ?>
    <?= $this->Html->script('core/AppForm.js') . "\n"; ?>
    <?= $this->Html->script('core/AppPermissions.js') . "\n"; ?>

    <?= $this->Html->script('source/AppSisConfiguracoes.js') . "\n"; ?>
    <?= $this->Html->script('source/AppSisGrupos.js') . "\n"; ?>
    <?= $this->Html->script('source/AppSisModulos.js') . "\n"; ?>
    <?= $this->Html->script('source/AppUsers.js') . "\n"; ?>
    <?= $this->Html->script('source/AppSisModulos.js') . "\n"; ?>
    <?= $this->Html->script('source/AppCalculos.js') . "\n"; ?>
    <?= $this->Html->script('source/AppOdontos.js') . "\n"; ?>
    <?= $this->Html->script('source/AppSaudePF.js') . "\n"; ?>
    <?= $this->Html->script('source/AppSaudePME.js') . "\n"; ?>
    <?= $this->Html->script('source/AppOdontoCalculos.js') . "\n"; ?>
    <?= $this->Html->script('source/AppPfTabelas.js') . "\n"; ?>
    <?= $this->Html->script('source/AppMidiasCompartilhadas.js') . "\n"; ?>
    <?= $this->Html->script('source/AppTabelas.js') . "\n"; ?>
    <?= $this->Html->script('source/AppRegioes.js') . "\n"; ?>
    <?= $this->Html->script('source/AppPfRedes.js') . "\n"; ?>
    <?= $this->Html->script('basics.js') . "\n"; ?>

    <?= $this->fetch('script') ?>
</body>

</html>

<!-- Modal -->
<div class="modal fade" id="modal-alerta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body centralizada">
                <h4>Esta funcionalidade não está habilitada para o seu usuário.</h4>
            </div>
            <div class="modal-footer" style="text-align: center !important;">
                <button type="button" class="btn btn-default centralizada" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(window).on('load', function() {
        $('#pre-loader .inner').fadeOut();
        $('#pre-loader').delay(350).fadeOut('slow');
        $('body').delay(350).css({
            'overflow': 'visible'
        });
    })


    $(document).ready(function() {

        if ($(window).width() <= 1080) {
            $(".arealogin").attr('class', 'loginreduzido');
            $(".botaologo").attr('class', 'col-xs-12 centralizada');
        }

        if ($(window).width() <= 800) {
            $("#insira-logo").css('text-align', 'center');
        }

        new Clipboard('.copiar');

        $('.editor-especial').summernote({
            height: 200,
            lang: 'pt-BR',
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
        });


        <?php
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        ?>
        var id = '<?= isset($sessao["id"]) ? $sessao["id"] : '' ?>';
        var celular = '<?= isset($sessao["validacao"]) ? $sessao["validacao"] : '' ?>';
        var email = '<?= isset($sessao["validacao_email"]) ? $sessao["validacao_email"] : '' ?>';
        var action = '<?= $this->request->params['action'] ?>';

        if (id) {
            if (action !== 'ativacao' && action !== 'loginUser') {
                if (email !== 'ATIVO' || celular !== 'ATIVO') {
                    window.location.href = "<?= $this->request->webroot ?>users/ativacao/" + id;
                }
            }
        }
    });
</script>

<!-- <script type=”text/javascript”> function noBack(){ window.history.forward() } noBack(); window.onload=noBack; window.onpageshow=function(evt){ if(evt.persisted)noBack() } window.onunload=function(){void(0)} </script> -->