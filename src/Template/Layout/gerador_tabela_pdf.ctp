<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'CakePHP: the rapid development php framework';

use Cake\Mailer\Email;
?>
<?php
//ob_start();
// This is where your script would normally output the HTML using echo or print
// Now collect the output buffer into a variable
?>
<!DOCTYPE html>
<!-- <html dir="ltr" lang="pt-BR"> -->
<!-- <page backtop="32mm" backbottom="7mm" backleft="5mm" backright="5mm" style="font-size: 11px">  -->

                <?php
                /**
                 * Send the document to a given destination: string, local file or browser.
                 * Dest can be :
                 *  I : send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
                 *  D : send to the browser and force a file download with the name given by name.
                 *  F : save to a local server file with the name given by name.
                 *  S : return the document as a string. name is ignored.
                 *  FI: equivalent to F + I option
                 *  FD: equivalent to F + D option
                 *  true  => I
                 *  false => S
                 */
/*
                 				$nomePDF = str_replace(" ", '', $nomePDF);

               debug($tipoPDF);
debug(json_encode($nomePDF));die();
*/
                require_once('../vendor/spipu/html2pdf/html2pdf.class.php');
                $pdf = new HTML2PDF('P', 'A4', 'pt', true, 'UTF-8', array(2,5,2,5));
                $pdf->setDefaultFont('arial');
                $pdf->setTestIsImage(false);
                $pdf->WriteHTML($this->fetch('content'), isset($_GET['vuehtml']));
                $pdf->pdf->SetDisplayMode('fullpage');
                $pdf->pdf->SetProtection(array('modify', 'copy'));
                
                ob_get_clean();
                $pdf->output($nomePDF, $tipoPDF);
                mb_internal_encoding('UTF-8');
                ?>
<!-- </page> -->
<!--             </div> -->
<!-- </html> -->

<script type="text/javascript">
// 	$("#fechar").click();	
	var tipoPDF = '<?= $tipoPDF?>';
	if (tipoPDF = 'F'){
		var apelido = '<?= $apelidoPDF?>';
		var a = <?= json_encode($nomePDF); ?>;
	    window.location = '<?= $this->request->webroot."tabelas-geradas/email?apelido=" ?>' +apelido+ "&email=<?= $emaildestino?>&pdf=<?= $nomePDF ?>";
    }
</script>