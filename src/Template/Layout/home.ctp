<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Calculo Rápido: Multicálculo para Planos de Saúde e Odontológicos';
?><!--

<script type="text/javascript">
    var sessao = '<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
echo (isset($sessao) ? $sessao["id"] : '');
?>';
if(window.location != "http://localhost:8888/simulador/" || window.location != "http://simulador/"){
    if (sessao !== '') {
        window.location.replace("<?= $this->request->webroot . "users/central" ?>");

    }else{
	    if (window.location !== window.parent.location ) {
	    	window.parent.location.href = "https://corretorparceiro.com.br";
	    }else{
			window.location.replace("https://corretorparceiro.com.br");
	    }
    }
}

</script>
-->


<!DOCTYPE html>
<html>
    <head>
    	<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106538674-3"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-106538674-3');
		</script>



        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ferramentas de Apoio para Corretores - Corretor Parceiro
        </title>

        <?= $this->Html->script('jqueryNEW') ?>
        <?= $this->Html->script('maskInput') ?>



        <?= $this->Html->script('greensock') ?>
        <?= $this->Html->script('layerslider.transitions') ?>
        <?= $this->Html->script('layerslider.kreaturamedia.jquery') ?>

        <?= $this->Html->script('bootstrap-v3.3.4.min') ?>

        <?php
        echo $this->Html->meta('favicon.ico', '/favicon.ico', array(
            'type' => 'icon'
        ));
        ?>
        <?= $this->Html->css('font-awesome/css/fontawesome-all.css', ['fullBase' => true]) ?>
        <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
        <?= $this->Html->css('base.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('cake.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('scrolling-nav.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('sistema.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('layerslider.css', ['fullBase' => true]) ?>


        <!--Latest compiled and minified JavaScript-->
        <?= $this->Html->css('font-awesome.css', ['fullBase' => true]) ?>
        <?= $this->Html->script('jquery.maskMoney') ?>
        <?= $this->Html->script('jquery.cpfcnpj.min') ?>

        <?= $this->Html->css('bootstrap/bootstrap.min.css', ['fullBase' => true]) ?>
        
        <?= $this->Html->css('font/font-family-exo2.css') ?>
        <?= $this->Html->css('font/font-family-open-sans.css') ?>
        <!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" /> -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet"> -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> -->
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding-right: 0 !important; padding-left: 0 !important">

        <?= $this->element('cabecalho'); ?>
        <?php
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $action = $this->request->action;
        ?>
        <div class="container col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding: 0 !important;">

            <!--<div class="clearfix">&nbsp;</div>-->
            <?php
            if ($action == 'login') {
                $classLogo = 'col-md-5 col-xs-12';
            } else {
                $classLogo = 'col-md-5';
            }
            ?>
            <div class="header clearfix cabecalho" >

            <div class="col-xs-12" style=" z-index: 9 !important;">
                <div id="slider-wrapper2" style=" z-index: 9 !important;">
                    <div id="textohome" style="height:35px;  z-index: 9 !important;">
                        <div class="ls-slide" data-ls="transition2d:5;slidedelay:2500" style="width: 100%; z-index: 9 !important;">
                            <p class="ls-l subtituloSlide" style="top:0px;left:0;text-align: center;width: 100%;color: #D9534F;  z-index: 9 !important;"
                               data-ls="
                               durationin:1000;">
                                Ferramentas de Apoio a Corretores
                            </p>
                        </div>
                        <div class="ls-slide" data-ls="transition2d:5;slidedelay:2500" style="width: 100%; z-index: 9 !important;">
                            <p class="ls-l subtituloSlide" style="top:0px;left:0;text-align: center;width: 100%;color: #D9534F;  z-index: 9 !important;"
                               data-ls="
                               durationin:1000;">
                                Crie sua conta grátis
                            </p>
                        </div>
                        <div class="ls-slide" data-ls="transition2d:5;slidedelay:2500" style="width: 100%; z-index: 9 !important;">
                            <p class="ls-l subtituloSlide" style="top:0px;left:0;text-align: center;width: 100%;color: #D9534F;  z-index: 9 !important;"
                               data-ls="
                               durationin:1000;">
                                Agilidade para suas vendas
                            </p>
                        </div>
                        <div class="ls-slide" data-ls="transition2d:5;slidedelay:2500" style="width: 100%; z-index: 9 !important;">


                            <p class="ls-l subtituloSlide" style="top:0px;left:0; text-align: center; width: 100%;color: #D9534F; z-index: 9 !important;"
                               data-ls="
                               durationin:1000;
                               color: #D9534F;">
                                Para corretores modernos e eficientes
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-10 col-xs-offset-1 botoes centralizada">

                <div class="clearfix"> <?= $this->Flash->render() ?></div>
                <!-- TABELAS DE PREÇOS -->
                <div class="col-xs-6 col-md-2 colunaBotoesHome" style="color: #003e58 !important">
                    <div data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Crie <b>Tabelas de Preços</b> como preferir e com seu próprio <b>logotipo</b>">
                        <?= $this->Html->image("lista.png", ['style' => 'max-height: 72px']);?>
                        <p class="botaoCentral">Tabelas de Preços<br/>Personalizadas</p>
                    </div>
                </div>

                <!-- SAUDE -->
                <div class="col-xs-6 col-md-2 colunaBotoesHome" style="color: #003e58 !important">
                    <div data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Simule <b>Planos de Saúde</b> para seus clientes em segundos">
                        <i class="fa fa-stethoscope fa-5x"> </i>
                        <p class="botaoCentral" >Multicálculo <br/>Planos de Saúde</p>
                    </div>
                </div>

                <!-- ODONTO -->
                <div class="col-xs-6 col-md-2 colunaBotoesHome" style="color: #003e58 !important" id="odonto">
                    <div data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Simule <b>Planos Odontológicos</b> para seus clientes em segundos">
                        <?= $this->Html->image("iconeOdonto.png", ['style' => 'max-height: 72px']) ?>
                        <p class="botaoCentral">Multicálculo <br/>Planos Odontológicos</p>
                    </div>
                </div>

                <!-- PERFIL ETÁRIO -->
                <div class="col-xs-6 col-md-2 colunaBotoesHome" style="color: #003e58 !important">
                    <div data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Preencha e organize <b>Perfil de Planos Empresariais</b> com rapidez e praticidade">
                        <?= $this->Html->image("iconePerfilEtario.png", ['style' => 'max-height: 72px']) ?>
                        <p class="botaoCentral">Formulário de Cotação<br/>Saúde Empresarial</p>
                    </div>
                </div>

                <!-- DIVULGAÇÃO -->
                <div class="col-xs-6 col-md-2 colunaBotoesHome" style="color: #003e58 !important">
                    <div data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Folhetos e banners digitais para <b>divulgar em suas redes sociais</b>">
                        <?= $this->Html->image("central.png", ['style' => 'max-height: 72px']) ?>
                        <p class="botaoCentral">Material de Divulgação <br/>Personalizado</p>
                    </div>
                </div>

                <!-- CHAT -->
                <div class="col-xs-6 col-md-2 colunaBotoesHome" style="color: #003e58 !important">
                    <div data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Tire suas dúvidas on-line com nossos especialistas">
                        <?= $this->Html->image("chat-final.png", ['style' => 'max-height: 72px']);?>
                        <p class="botaoCentral">Dúvidas<br/>Fale Conosco</p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-md-10 col-md-offset-1">
                <?= $this->element('slides_home'); ?>
            </div>


            <div class="col-xs-12" >
                <div class="divisoria" ></div>
                <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3" style="text-align: center">

                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-xs-6">
                    <div class="alert alert-info" role="alert">
                        <i class="fa fa-bolt fa-lg" aria-hidden="true"></i>
                        <strong>Produtividade - </strong> Rapidez para vender mais
                    </div>
                    <div class="alert alert-info" role="alert">
                        <i class="fa fa-tablet fa-lg" aria-hidden="true"></i>
                        <strong>Mobilidade - </strong> Acessível em computadores, tablets e smartphones
                    </div>
                    <div class="alert alert-info" role="alert">
                        <i class="fas fa-wrench fa-lg" aria-hidden="true"></i>
                        <strong>Praticidade - </strong> Informações atualizadas em um só lugar
                    </div>
                    <div class="alert alert-info" role="alert">
                        <i class="fa fa-lightbulb-o fa-lg" aria-hidden="true"></i>
                        <strong>Inovação - </strong> Tecnologia para facilitar seus negócios
                    </div>
                </div>
                <div class="col-xs-6" >
                    <div class="alert alert-info" role="alert">
                        <i class="fas fa-clock fa-lg" aria-hidden="true"></i>
                        <strong>Disponibilidade - </strong> 24h por dia, 7 dias por semana
                    </div>
                    <div class="alert alert-info" role="alert">
                        <i class="fa fa-user-secret fa-lg" aria-hidden="true"></i>
                        <strong>Privacidade - </strong> Informações restritas somente aos próprios usuários
                    </div>
                    <div class="alert alert-info" role="alert">
                        <i class="fa fa-thumbs-up fa-lg" aria-hidden="true"></i>
                        <strong>Simplicidade - </strong> Descomplicado e fácil de usar
                    </div>
                    <div class="alert alert-info" role="alert">
                        <i class="fa fa-calendar-check-o fa-lg" aria-hidden="true"></i>
                        <strong>Organização - </strong> Gerencie suas oportunidades de vendas
                    </div>
                </div>

            </div>

            <div class="clearfix">&nbsp;</div>
            <div class="rodape-home col-xs-12" style="text-align: center;">
                Ferramentas de Vendas para Corretores.<br> Desenvolvido por <a href="https://corretorparceiro.com.br" style ='color: #337ab7 !important; text-decoration: none !important'><?= '<img src="https://corretorparceiro.com.br/app/img/ciranda.png" style="height: 15px; margin-left: 5px;" />' ?><span style="text-decoration: none !important"> corretorparceiro.com.br</span></a>
            </div>


        </div>





        <!--
            </div>-->


    </body>

</html>
<script type="text/javascript">
    $(document).ready(function () {

	    var altura = "147px";
	    $('.colunaBotoesHome').outerHeight(altura);



        $('[data-toggle="popover"]').popover();
        if ($(window).width() <= 1080) {
            $(".arealogin").attr('class', 'loginreduzido');
            $(".navbar-header").attr('style', 'padding-left: 15px !important');
            $("#espacolink").empty();
            $("#espacolink").append("<br>");
            $("#linkslogin").attr("style", "text-align: center !important");
        }
        $('#textohome').layerSlider({
            autoStart: true,
            firstLayer: 1,
            pauseOnHover: false,
            showBarTimer: false,
            showCircleTimer: false,
            skin: 'noskin',
            skinsPath: '<?= $this->request->weroot . '/simulador/layerslider/'; ?>',
            responsiveUnder: 960,
//            layersContainer: 960
            // Slider options goes here,
            // please check the 'List of slider options' section in the documentation
        });

        $('[data-toggle="tooltip"]').tooltip();

        $("#celular").mask("(99) 9 9999-9999");

        $(".controle").click(function () {
            $(".fa-chevron-circle-up").toggleClass('fa-chevron-circle-down');
        });


        $(".cadastroBanner").click(function () {
            $.ajax({
                type: "get",
                url: "<?php echo $this->request->webroot ?>users/add/",
                success: function (data) {
                    $("#cad").empty();
                    $("#cad").append(data);

                }
            });
        });


    });
</script>

<!-- Modal -->
<div class="modal fade" id="modalCad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content col-xs-12">
            <!--            <div class="modal-header centralizada">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabelCadastro">Cadastro de Novo Usuário</h4>
                        </div>-->
            <div class="modal-body" id="cad">

            </div>

        </div>
    </div>
</div>
