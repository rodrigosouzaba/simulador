<?php
ob_start();
set_time_limit(60);

/**
 * Send the document to a given destination: string, local file or browser.
 * Dest can be :
 *  I : send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
 *  D : send to the browser and force a file download with the name given by name.
 *  F : save to a local server file with the name given by name.
 *  S : return the document as a string. name is ignored.
 *  FI: equivalent to F + I option
 *  FD: equivalent to F + D option
 *  true  => I
 *  false => S
 */

require_once('../vendor/spipu/html2pdf/html2pdf.class.php');
// [508, 285.75]
$pdf = new HTML2PDF('P', 'A4', 'pt', true, 'UTF-8', array(2, 5, 2, 5));
$pdf->setDefaultFont('arial');
$pdf->setTestIsImage(false);
$pdf->WriteHTML($this->fetch('content'), isset($_GET['vuehtml']));
$pdf->pdf->SetDisplayMode('fullpage');
$pdf->pdf->SetProtection(array('modify', 'copy'));
ob_get_clean();
header('Content-Type: application/pdf');
$pdf->output($nomePDF, isset($tipoPDF) ? $tipoPDF : "D");
