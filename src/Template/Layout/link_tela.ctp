<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Mailer\Email;

?>
<?php
//ob_start();
// This is where your script would normally output the HTML using echo or print
// Now collect the output buffer into a variable
?>
<!DOCTYPE html>
<html dir="ltr" lang="pt-BR">

<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= 'Corretor Parceiro - Assessoria de apoio a Corretores' ?>:
    </title>
    <?= $this->Html->script('jquery-3.1.0.min') ?>
    <?= $this->Html->script('maskInput') ?>
    <?= $this->Html->meta('icon') ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js">
    </script>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <?= $this->Html->css('sistema.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('font-awesome.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('link.css', ['fullBase' => true]) ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <script>
        var baseURL = baseUrl = baseSub = "<?php echo $this->request->webroot; ?>";
    </script>
</head>

<body>
    <?php

    $session = $this->request->session();
    $sessao = $session->read('Auth.User');
    $action = $this->request->action;
    ?>

    <div class="header cabecalho col-xs-12" style=" padding: 0">


        <?php
        if ($action == 'login') {
            $classLogo = 'col-md-3 col-xs-12';
        } else {
            $classLogo = 'col-md-5 col-xs-12';
        }
        ?>
        <?php
        if (isset($sessao['role'])) {
            if ($sessao['validacao'] == 'ATIVO' && $sessao["validacao_email"] == 'ATIVO') {
                echo $this->element('menu');
            }
        } else {
        ?>
            <div class="col-md-9 col-xs-12">
                <?php
                if ($action == 'home') {
                    echo $this->fetch('content');
                } ?>
            </div>
        <?php
        }
        ?>

    </div>
    </div>



    <?= $this->element("menu-configuracao"); ?>
    <div class="col-xs-12 centralizada">
        <?= $this->Flash->render() ?>
    </div>

    <div class="container-fluid">
        <?= $this->fetch('content') ?>
    </div>


</body>

</html>