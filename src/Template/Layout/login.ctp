<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>


<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portal do Corretor Parceiro - Corretor Parceiro</title>
    <?= $this->Html->script('jquery-3.1.0.min.js') ?>
    <?= $this->Html->script('maskInput.js') ?>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


    <?= $this->Html->meta('favicon.ico', '/favicon.ico', array('type' => 'icon')) ?>

    <?= $this->Html->css('base.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('sistema.css', ['fullBase' => true]) ?>

    <!-- TEMPLATE BOOTSTRAP -->
    <?= $this->Html->css('freelancer.min.css', ['fullBase' => true]) ?>

    <!--Latest compiled and minified JavaScript-->
    <?= $this->Html->css('font-awesome.css', ['fullBase' => true]) ?>
    <?= $this->Html->script('jquery.maskMoney.js') ?>
    <?= $this->Html->script('jquery.cpfcnpj.min.js') ?>

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <?= $this->Html->css('bootstrap/bootstrap.min.css', ['fullBase' => true]) ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <?= $this->fetch('content') ?>
</body>

</html>
<?= $this->Html->script('bootstrap.min.js') ?>
<?= $this->Html->script('jquery.min.js') ?>
<?= $this->Html->script('jquery.easing.min.js') ?>
<?= $this->Html->script('freelancer.min.js') ?>