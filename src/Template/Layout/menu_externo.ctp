<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = '';
?>


<!DOCTYPE html>
<html>

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106538674-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-106538674-3');
    </script>



    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Portal do Corretor Parceiro - Corretor Parceiro

    </title>
    <?= $this->Html->script('jqueryNEW') ?>

    <?= $this->Html->script('maskInput') ?>
    <?= $this->Html->script('greensock') ?>

    <?= $this->Html->script('layerslider.transitions') ?>
    <?= $this->Html->script('layerslider.kreaturamedia.jquery') ?>

    <?php
    echo $this->Html->meta('favicon.ico', '/favicon.ico', array(
        'type' => 'icon'
    ));
    ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <?= $this->Html->css('base.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('cake.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('sistema.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('layerslider.css', ['fullBase' => true]) ?>

    <?= $this->Html->css('jquery-ui.min.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('jquery-ui.structure.min.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('jquery-ui.theme.min.css', ['fullBase' => true]) ?>


    <!--Latest compiled and minified JavaScript-->
    <?= $this->Html->css('font-awesome.css', ['fullBase' => true]) ?>
    <?= $this->Html->script('jquery.maskMoney') ?>
    <?= $this->Html->script('jquery-ui.min') ?>
    <?= $this->Html->script('datepicker-pt-BR') ?>
    <?= $this->Html->script('clipboard.min') ?>
    <?= $this->Html->script('jquery.cpfcnpj.min') ?>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <!-- SUMMERNOTE TEXTAREA -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    <?= $this->Html->script('summernote/lang/pt-br.js') ?>


    <style>
        .note-editable {
            line-height: 1 !important;
            font-size: 12 !important;
        }

        .note-editable p {
            line-height: 1;
            font-size: 12;
        }

        .conteudo {
            margin-top: 60px;
        }
    </style>
    <!-- FIM SUMMERNOTE TEXTAREA -->
</head>


<body class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding-right: 0 !important; padding-left: 0 !important">

    <?php

    $session = $this->request->session();
    $sessao = $session->read('Auth.User');
    $action = $this->request->action;
    ?>

    <div class="header cabecalho col-xs-12" style=" padding: 0">

    </div>


    <div class="conteudo col-xs-12">
        <?php
        // if ($action != 'loginUser' || $action != 'ativacao' || $action != 'login') {
            echo $this->fetch('content');
        // }
        ?>
    </div>

</body>

</html>

<!-- Modal -->
<div class="modal fade" id="modal-alerta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body centralizada">
                <h4>Esta funcionalidade não está habilitada para o seu usuário.</h4>
            </div>
            <div class="modal-footer" style="text-align: center !important;">
                <button type="button" class="btn btn-default centralizada" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<script>
   
    $("a").click(function() {
        let tamanho = $(this).attr("href").length;
        if ($(this).attr("href") != "#" && $(this).attr("href")[0] != "#" && $(this).attr("target") != "_blank") {
            $("#modalProcessando").modal("show");
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
    $(document).ready(function() {
        $(".alerta").click(function() {
            $('#modal-alerta').modal('show');
        });
        //         alert($(window).width());
        if ($(window).width() <= 1080) {
            $(".arealogin").attr('class', 'loginreduzido');


            $(".botaologo").attr('class', 'col-xs-12 centralizada');
            //        $(".navbar-header").attr('style', 'padding-left: 15px !important');

        }
        if ($(window).width() <= 800) {
            $("#insira-logo").css('text-align', 'center');
        }
       
        <?php
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');
        ?>
        var id = '<?= $sessao["id"] ?>';
        var action = '<?= $this->request->params['action'] ?>';

        // if (id) {
        //     if (action !== 'ativacao' && action !== 'loginUser') {
        //         if (email !== 'ATIVO' || celular !== 'ATIVO') {
        //             window.location.href = "<?= $this->request->webroot ?>users/ativacao/" + id;
        //         }
        //     }
        // }


    });
</script>
