<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Calculo Rápido: Multicálculo para Planos de Saúde e Odontológicos';
?>


<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Calculo Rápido: Multicálculo para Planos de Saúde e Odontológicos
        </title>

        <?= $this->Html->script('jqueryNEW') ?>
        <?= $this->Html->script('maskInput') ?>



        <?= $this->Html->script('greensock') ?>
        <?= $this->Html->script('layerslider.transitions') ?>
        <?= $this->Html->script('layerslider.kreaturamedia.jquery') ?>

        <!--<script src="<?= WWW_ROOT ?>js/greensock.js" type="text/javascript"></script>-->
        <!--
                 LayerSlider script files 
                <script src="/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
                <script src="/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>-->



        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


        <?php
        echo $this->Html->meta('favicon.ico', '/favicon.ico', array(
            'type' => 'icon'
        ));
        ?>

        <?= $this->Html->css('base.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('cake.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('scrolling-nav.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('sistema.css', ['fullBase' => true]) ?>
        <?= $this->Html->css('layerslider.css', ['fullBase' => true]) ?>


        <!--Latest compiled and minified JavaScript--> 
        <?= $this->Html->css('font-awesome.css', ['fullBase' => true]) ?>
        <?= $this->Html->script('jquery.maskMoney') ?>
        <?= $this->Html->script('jquery.cpfcnpj.min') ?>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding-right: 0 !important; padding-left: 0 !important">
        <!--        <div id="layerslider" style="width: 800px; height: 400px;">
                     The contents on your slider will be here 
                </div>-->




        <?= $this->element('cabecalho'); ?>
        <?php
        $session = $this->request->session();
        $sessao = $session->read('Auth.User');

        $action = $this->request->action;
        ?>
        <div class="container col-md-12 col-lg-12 col-sm-12 col-xs-12" style="min-height: 100%; padding: 0 !important;">

            <!--<div class="clearfix">&nbsp;</div>-->
            <?php
            if ($action == 'login') {
                $classLogo = 'col-md-4 col-xs-12';
            } else {
                $classLogo = 'col-md-4';
            }
            ?>
            <div class="header clearfix cabecalho" >

                <div class="<?= $classLogo ?>">
                    <?php
                    $url = '';
                    if (isset($sessao['role'])) {
                        switch ($sessao['role']) {
                            case "admin":
                                $url = array('controller' => 'tabelas', 'action' => 'index');
                                break;
                            case "padrao":
                                $url = array('controller' => 'simulacoes', 'action' => 'add');
                                break;
                            default :
                                $url = '';
                                break;
                        }
                    }
//                    echo $this->Html->image("logoFerramentasApoioCentralizada.png", array(
                    echo $this->Html->image("logoFerramentasApoio.png", array(
                        'url' => $url,
                        'class' => 'logoFa'
                    ));
                    ?>
                </div>
                <?php if (isset($sessao['role'])) { ?>
                    <!--                    <div class="col-md-2 centralizada versao">
                    <?=
                    $this->Html->link(' Versão de Teste', '#', ['class' => 'btn btn-sm btn-danger btn-block', 'role' => 'button', 'escape' => false]);
                    ?>
                                        </div>
                                        <div class="col-md-2 centralizada versao">
                    <?=
                    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Sugestões', ['controller' => 'Users', 'action' => 'erro'], ['class' => 'btn btn-sm btn-danger btn-block', 'role' => 'button', 'escape' => false]);
                    ?>
                                        </div>-->
                    <?php
                    if ($sessao['validacao'] === 'ATIVO') {
                        echo $this->element('menu');
                    } else {
                        echo "<div class='col-md-8'><div class='col-md-2' style='float:right'>";
                        echo $this->Html->link(' Ativar Usuário', ['controller' => 'Users', 'action' => 'ativacao/' . $sessao['id']], ['class' => 'btn btn-sm btn-danger  botaologo', 'role' => 'button', 'escape' => false]);
                        echo "</div></div>";
                    }
                } else {
                    ?>
                    <div class="col-md-8 col-xs-12">

                        <div class="col-md-12 formlogin">

                            <?= $this->Flash->render('auth') ?>

                            <div class="col-md-12  users form" style="float: right; text-align: right">




                                <?= $this->Form->create($user, ['id' => 'login', 'class' => 'form-inline']) ?>
    <!--        <input name="tipo" value="" type="hidden">
        <div class="form-group">
            <label for="tipo-pf"><input name="tipo" value="pf" id="tipo-pf" type="radio" class="form-control">Pessoa Física</label>
        </div>
        <div class="form-group">
            <label for="tipo-pj"><input name="tipo" value="pj" id="tipo-pj" type="radio" class="form-control">Pessoa Jurídica</label>
        </div>-->
                                <div class="form-group"><?= $this->Form->input('username', ['label' => '', 'placeholder' => 'CPF', 'style' => 'height: 30px; padding: 2px; font-size: 100%']) ?></div>
                                <div class="form-group"><?= $this->Form->input('password', ['label' => '', 'placeholder' => 'Senha', 'style' => 'height: 30px; padding: 2px; font-size: 100%']) ?></div>
                                <div class="form-group" style="margin-bottom: 12px !important">
                                    <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-user', 'aria-hidden' => 'true']) . ' Login'), ['class' => 'btn btn-sm btn-default ', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Login', 'id' => 'logar']); ?>    
                                    <span data-toggle='tooltip' data-placement = 'top' title ='Esqueci minha senha'> 
                                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-unlock-alt', 'aria-hidden' => 'true']) . ' Esqueci a senha', '#', ['class' => 'btn btn-sm btn-default ', 'data-toggle' => "modal", 'data-target' => "#myModal", 'role' => 'button', 'escape' => false, 'id' => 'senha', 'data-placement' => 'top', 'title' => 'Esqueci minha Senha']); ?>
                                    </span>
                                    <span data-toggle='tooltip' data-placement = 'top' title ='Cadastrar Usuário'> 
                                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Cadastro Novo', ['controller' => 'users', 'action' => 'add'], ['class' => 'btn btn-sm btn-primary', 'role' => 'button', 'escape' => false, 'title' => 'Cadastro Novo', 'id' => 'add']) ?>
                                    </span>

                                </div>





                            </div>



                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header centralizada">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Digite o CPF cadastrado no Sistema</h4>
                                        </div>
                                        <div class="modal-body" style="min-height: 150px;">

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header centralizada">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Cadastro de Novo Usuário</h4>
                                        </div>
                                        <div class="modal-body" style="min-height: 500px;">

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <?= $this->Form->end() ?>
                            <script type="text/javascript">
                                $("#username").mask("999.999.999-99");

                                var count = 0;
                                //    $('#username').focus(removermascara);
                                //    $('#username').blur(updateCount);

                                function updateCount() {
                                    if ($(this).val().length == 14) {
                                        $("#username").mask("99.999.999/9999-99");
                                    }
                                    if ($(this).val().length == 11) {
                                        $("#username").mask("999.999.999-99");
                                    }

                                }

                                function removermascara() {
                                    $("#username").unmask();
                                }

                                $("#logar").click(function () {
                                    $.ajax({
                                        type: "post",
                                        data: $("#login").serialize(),
                                        url: "<?php echo $this->request->webroot ?>users/",
                                    });
                                });

                                $("#senha").click(function () {
                                    $.ajax({
                                        type: "get",
                                        data: $("#a").serialize(),
                                        url: "<?php echo $this->request->webroot ?>users/recuperarSenha/",
                                        success: function (data) {
                                            $(".modal-body").empty();
                                            $(".modal-body").append(data);

                                        }
                                    });
                                });
                                //    $("#add").click(function () {
                                //        $.ajax({
                                //            type: "get",
                                ////            data: $("#a").serialize(),
                                //            url: "<?php echo $this->request->webroot ?>users/add/",
                                //            success: function (data) {
                                //                $(".modal-body").empty();
                                //                $(".modal-body").append(data);
                                //
                                //            }
                                //        });
                                //    });



                                $('#myModal').on('shown.bs.modal', function () {
                                    $('#myInput').focus()
                                });
                                $('#modalAdd').on('shown.bs.modal', function () {
                                    $('#myInput').focus()
                                });

                            </script>

                            <!--Start of Tawk.to Script-->
                            <!--<script type="text/javascript">
                                var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
                                (function () {
                                    var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                                    s1.async = true;
                                    s1.src = 'https://embed.tawk.to/598ca78b1b1bed47ceb03fec/default';
                                    s1.charset = 'UTF-8';
                                    s1.setAttribute('crossorigin', '*');
                                    s0.parentNode.insertBefore(s1, s0);
                                })();
                            </script>-->
                            <!--End of Tawk.to Script-->

                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="clearfix"> <?= $this->Flash->render() ?></div>
            </div>
            <div class="col-xs-12 botoes">
                <div class="col-xs-12 col-md-12 centralizada centralizadaVertical">
                    <h2 style="color:#003956;margin-top: 0 !important;">Agilidade para suas vendas</h2>
                </div>
                <div class="col-xs-12 col-md-2 colunaBotoesHome">
                    <?= $this->Html->link($this->Html->image("iconeSaudePme.png", ['style' => 'max-height: 72px']) . '<br/><span class="botaoCentral"> Multicálculo <br/>Saúde PME</span>', ['controller' => 'Simulacoes', 'action' => 'add'], ['class' => 'btn btn-lg  btn-default btn-block botaoHome', 'role' => 'button', 'escape' => false]); ?>
                </div>
                <div class="col-xs-12 col-md-2 colunaBotoesHome">
                    <?= $this->Html->link($this->Html->image("iconeSaudePf.png", ['style' => 'max-height: 72px']) . '<br/> <span class="botaoCentral">Multicálculo <br/>Saúde Pessoa Física</span>', ['controller' => 'PfCalculos', 'action' => 'add'], ['class' => 'botaoHome btn btn-lg  btn-default btn-block', 'data-dismiss' => "modal", 'role' => 'button', 'escape' => false]); ?>
                </div>
                <div class="col-xs-12 col-md-2 colunaBotoesHome">
                    <?= $this->Html->link($this->Html->image("iconeOdontoPME.png", ['style' => 'max-height: 72px']) . '<br/> <span class="botaoCentral">Multicálculo <br/>Odonto PME</span>', ['controller' => 'OdontoCalculos', 'action' => 'add', 'PJ'], ['class' => 'botaoHome btn btn-lg  btn-default btn-block', 'data-dismiss' => "modal", 'role' => 'button', 'escape' => false]); ?>
                </div>
                <div class="col-xs-12 col-md-2 colunaBotoesHome">
                    <?= $this->Html->link($this->Html->image("iconeOdontoPf.png", ['style' => 'max-height: 72px']) . '<br/> <span class="botaoCentral">Multicálculo <br/>Odonto Pessoa Física</span>', ['controller' => 'OdontoCalculos', 'action' => 'add', 'PF'], ['class' => 'botaoHome btn btn-lg  btn-default btn-block', 'data-dismiss' => "modal", 'role' => 'button', 'escape' => false]); ?>
                </div>
                <div class="col-xs-12 col-md-2 colunaBotoesHome">
                    <?= $this->Html->link($this->Html->image("iconePerfilEtario.png", ['style' => 'max-height: 72px']) . '<br/> <span class="botaoCentral">Perfil Empresarial <br/>Planos de Saúde</span>', ['controller' => 'Simulacoes', 'action' => 'perfiletario'], ['class' => 'botaoHome btn btn-lg  btn-default btn-block', 'data-dismiss' => "modal", 'role' => 'button', 'escape' => false]); ?>
                </div>
                <div class="col-xs-12 col-md-2 colunaBotoesHome">
                    <?= $this->Html->link($this->Html->image("iconePerfilEtario.png", ['style' => 'max-height: 72px']) . '<br/> <span class="botaoCentral">Perfil Empresarial <br/>Planos de Saúde</span>', ['controller' => 'Simulacoes', 'action' => 'perfiletario'], ['class' => 'botaoHome btn btn-lg  btn-default btn-block', 'data-dismiss' => "modal", 'role' => 'button', 'escape' => false]); ?>
                </div>


            </div>
            <div class="col-xs-12 col-md-10 col-md-offset-1">

                <?= $this->element('slides_home');?>
            </div>
            <!--
                         SEÇÃO AZUL 
                        <div class="col-xs-12" style="padding: 0 !important;">
                            <div id="secao-home" class="page-scroll">
                                <h3>Simples, Rápido e Prático</h3>
                                <div class="col-xs-12">
                                    <div class="divisoria" style="opacity: 0.07"></div>
                                </div>
                                <br>
                                <div class="col-xs-12 col-md-4" style="padding-top: 15px;">
                                    <i class="fa fa-user-plus fa-4x" aria-hidden="true"></i><br>
                                    <h3>1. Cadastre-se</h3>
                                    <small>Crie seu usuário de forma rápida e segura.</small>
            
                                </div>
                                <div class="col-xs-12 col-md-4" style="padding-top: 15px;"> 
                                    <i class="fa fa-check-circle fa-4x" aria-hidden="true"></i><br>
                                    <h3>2. Ative seu usuário</h3>
                                    <small>Confirme sua identidade com o código SMS</small>
                                </div>
                                <div class="col-xs-12 col-md-4" style="padding-top: 15px;">
                                    <i class="fa fa-calculator fa-4x" aria-hidden="true"></i><br>
                                    <h3>3. Faça os Cálculos</h3>
                                    <small>Disponível em Smartphones, Tablets ou PCs</small>
                                </div>
                                <div class="clearfix">&nbsp;</div>
            
                            </div>
            
                        </div>-->

            <!-- SEÇÃO COM CHECKS -->
            <div class="col-xs-12" >
                <div id="secao-portabilidade" class="page-scroll">
                    <h2 class="centralizada" style="color:#003956;margin-top: 0 !important;">Para corretores modernos e eficientes</h2>
                    <div class="col-xs-12">
                        <div class="divisoria" ></div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="col-xs-12 col-sm-6" >
                        <div class="alert alert-info" role="alert">
                            <i class="fa fa-bolt fa-lg" aria-hidden="true"></i>  
                            <strong>Produtividade - </strong> Agilidade para suas vendas
                        </div>
                        <div class="alert alert-info" role="alert">
                            <i class="fa fa-tablet fa-lg" aria-hidden="true"></i>  
                            <strong>Mobilidade - </strong> Na palma da mão, no escritório, em qualquer lugar!
                        </div>
                        <div class="alert alert-info" role="alert">
                            <i class="fa fa-sitemap fa-lg" aria-hidden="true"></i>  
                            <strong>Praticidade - </strong> Todas as informações atualizadas em um só lugar
                        </div>
                        <div class="alert alert-info" role="alert">
                            <i class="fa fa-lightbulb-o fa-lg" aria-hidden="true"></i>  
                            <strong>Inovação - </strong> Tecnologia para facilitar seus negócios
                        </div>
                        <div class="alert alert-info" role="alert">
                            <i class="fa fa-thumbs-up fa-lg" aria-hidden="true"></i>  
                            <strong>Disponibilidade - </strong> 24h por dia, 7 dias por semana
                        </div>
                        <div class="alert alert-info" role="alert">
                            <i class="fa fa-user-secret fa-lg" aria-hidden="true"></i>  
                            <strong>Privacidade - </strong> Informações restritas somente aos próprios usuários
                        </div>







                    </div>
                    <div class="col-xs-12 col-sm-6" style="text-align: center">
                        <?=
                        $this->Html->image("screenFull.png", array(
                            'class' => 'screen'
                        ));
                        ?>
                    </div>

                </div>


            </div>



            <div class="rodape-home col-xs-12">
                <!--Copyright 2017 - Todos os diretos Reservados-->
                Ferramentas de apoio<?= $this->Html->image("ciranda.png", ['style' => 'max-height: 25px; margin: 0 10px;']); ?> corretorparceiro.com.br
            </div>
        </div>






    </div>


</body>

</html>
<script type="text/javascript">
    $(document).ready(function () {
//       alert('<?= WWW_ROOT . 'layerslider'; ?>');
        $('#layerslider').layerSlider({
            navButtons: true,
            autoStart: true,
            firstLayer: 1,
            hoverBottomNav: true,
            skin: 'fullwidth',
            skinsPath: '<?= WWW_ROOT . 'layerslider'; ?>',
            responsiveUnder: 960,
            layersContainer: 960
                    // Slider options goes here,
                    // please check the 'List of slider options' section in the documentation
        });

        $('[data-toggle="tooltip"]').tooltip();
        $('#username').focus();
//        $("#username").mask("999.999.999-99");
        $("#celular").mask("(99) 9 9999-9999");

        $(".controle").click(function () {
            $(".fa-chevron-circle-up").toggleClass('fa-chevron-circle-down');
        });


        $(".cadastroBanner").click(function () {
            $.ajax({
                type: "get",
//            data: $("#a").serialize(),
                url: "<?php echo $this->request->webroot ?>users/add/",
                success: function (data) {
                    $("#cad").empty();
                    $("#cad").append(data);

                }
            });
        });



        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').focus()
        });
        $('#modalCad').on('shown.bs.modal', function () {
            $('#myInput').focus()
        });

    });
</script>

<!-- jQuery -->

<script type="text/javascript" id="script_JeKevvke3BUroA3zreff68c757">
    var _egoiLoaderMulti = _egoiLoaderMulti || [];
    var _egoiLoaderValues = [];
    _egoiLoaderValues.push(["_setForm", "JeKevvke3BUroA3zreff68c757"]);
    _egoiLoaderValues.push(["_setResource", "af6acd6f1e99b55b72605c132cafd3a0"]);
    _egoiLoaderValues.push(["_setUrl", "https://53.e-goi.com/"]);
    _egoiLoaderValues.push(["_setType", "normal"]);
    _egoiLoaderValues.push(["_setWidth", "750"]);
    _egoiLoaderValues.push(["_setHeight", "619"]);
    _egoiLoaderMulti.push(_egoiLoaderValues);
    (function () {
        var egoi = document.createElement("script");
        egoi.type = "text/javascript";
        egoi.async = true;
        egoi.src = "https://53.e-goi.com/include/javascript/egoi.js";
        var e = document.getElementById("script_JeKevvke3BUroA3zreff68c757");
        e.parentNode.insertBefore(egoi, e);
    })();
</script>

<!-- Modal -->
<div class="modal fade" id="modalCad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cadastro de Novo Usuário</h4>
            </div>
            <div class="modal-body" id="cad" style="min-height: 500px;">

            </div>

        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 840px;">
        <div class="modal-content">
            <!--            <div class="modal-header centralizada">
                            <h4 class="modal-title" id="myModalLabel">Cadastre-se</h4>
                        </div>-->
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 100%; margin:10px;"> Fechar <i class="fa fa-times"></i></button>
            <div class="clearfix">&nbsp;</div>
            <div class="modal-body" id="video" >
                <div class="embed-responsive embed-responsive-16by9">
                    <!--<iframe class="embed-responsive-item" width="840" height="473" src="https://youtu.be/CRdPI2w-m70" frameborder="0" allowfullscreen></iframe>-->
                    <iframe class="embed-responsive-item" width="840" height="473" src="https://www.youtube.com/embed/CRdPI2w-m70" frameborder="0" allowfullscreen></iframe>
                    <!--                        
                                    <iframe  src="//youtu.be/REH_tg_k4OM"></iframe>-->
                </div>

            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalNewsletter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 9999 !important;">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Receba nossos comunicados e atualizações</h4>
            </div>
            <div class="modal-body">

                <!--Paste this code where you want the form to appear-->
                <div id="aJeKevvke3BUroA3zreff68c757"></div>

            </div>
        </div>
    </div>
</div>

