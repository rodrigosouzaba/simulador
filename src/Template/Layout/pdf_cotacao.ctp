<?php
// ob_start();
// This is where your script would normally output the HTML using echo or print
// Now collect the output buffer into a variable
?>
<style type="text/css">
    .jumbotron {
        background-color: #C0C0C0;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 0 30px 0;
        padding-left: 20px;
        line-height: 1.5;
        /*width: 97%;*/
    }

    .fonteReduzida {
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        font-size: 7px !important;
        line-height: 1.5;
        margin-bottom: 0;
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }

    .TituloOperadora {
        background-color: #337ab7;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        line-height: 1.5;
        width: 100%;
    }

    .detalhesOperadora {
        padding: 10px 0px;
        font-weight: bold;
    }

    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .centralizada {
        text-align: center;
    }

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .fonteReduzida {
        font-size: 10px;
    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .esquerda {
        float: left;
        margin: 15px;

        font-family: sans-serif;
    }

    .direita {
        float: right;
    }

    .total {
        width: 100%;
    }

    .p15 {
        width: 15%;
    }

    .p85 {
        width: 85%;
    }

    .totalTabela {
        width: 100% !important;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .t100 {
        width: 100%;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .corpo-gerador-tabelas {
        border: 1px solid #ddd !important;
        /* 	    font-size: 100%; */
        padding: 5px;
        width: 100%;
    }

    .titulo-tabela {
        text-align: center;
        font-weight: bold;
        padding: 5px;
        font-size: 85% !important;
    }

    .precos {
        border: solid 0.5px #ddd;
        text-align: center;
        padding: 3px 0 !important;
    }

    .nobreak {
        page-break-inside: avoid;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .page_header {
        width: 100% !important;
        max-width: 100% !important;
        min-width: 100% !important;
        /* 	    margin: 0 100px; */
    }
</style>
<html dir="ltr" lang="pt-BR">

<page>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
        <title>Corretor Parceiro - Plataforma Digital</title>
        <?= $this->fetch('title') ?>
        <?= $this->Html->meta('icon') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>


    <body>
        <div class="container" style="margin-top: 20px; width: 97% !important;">

            <?php
            set_time_limit(60);

            /**
             * Send the document to a given destination: string, local file or browser.
             * Dest can be :
             *  I : send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
             *  D : send to the browser and force a file download with the name given by name.
             *  F : save to a local server file with the name given by name.
             *  S : return the document as a string. name is ignored.
             *  FI: equivalent to F + I option
             *  FD: equivalent to F + D option
             *  true  => I
             *  false => S
             */

            require_once('../vendor/spipu/html2pdf/html2pdf.class.php');
       
            $pdf = new HTML2PDF('P', 'A4', 'pt', true, 'UTF-8');
            $pdf->setDefaultFont('arial');
            $pdf->setTestIsImage(false);
            $pdf->WriteHTML($this->fetch('content'), isset($_GET['vuehtml']));
            $pdf->pdf->SetDisplayMode('fullpage');
            $pdf->pdf->SetProtection(array('modify', 'copy'));
            ob_get_clean();
            $pdf->output($nomePDF, "I");
            ?>
        </div>
    </body>
</page>

</html>