
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Processando...';

use Cake\Mailer\Email;

//use Spipu\Html2Pdf\Html2Pdf;
//use Spipu\Html2Pdf\Exception\Html2PdfException;
//use Spipu\Html2Pdf\Exception\ExceptionFormatter;
?>
<?php
//ob_start();
// This is where your script would normally output the HTML using echo or print
// Now collect the output buffer into a variable
?>
<!DOCTYPE html>
<page>
    <html>
        <head>
            <?php echo $this->Html->charset() ?>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>
                <?php echo $cakeDescription ?>
            </title>
            <?php echo $this->Html->script('jquery-3.1.0.min') ?>
            <!--<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>-->


            <?php echo $this->Html->meta('icon') ?>
            <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
            <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />

            <?php echo $this->Html->css('font-awesome.css', ['fullBase' => true]) ?>
            <?php echo $this->Html->script('jquery.maskMoney') ?>

            <?php echo $this->fetch('meta') ?>
            <?php echo $this->fetch('css') ?>
            <?php echo $this->fetch('script') ?>

        </head>
        <body>
            <div class="container" style="margin-top: 20px; width: 97% !important;">

                <div class="clearfix">&nbsp; </div>
                <?php echo $this->Flash->render() ?>
                <div class="titulo"><?php echo $this->fetch('title') ?></div>

                <?php
                /**
                 * Send the document to a given destination: string, local file or browser.
                 * Dest can be :
                 *  I : send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
                 *  D : send to the browser and force a file download with the name given by name.
                 *  F : save to a local server file with the name given by name.
                 *  S : return the document as a string. name is ignored.
                 *  FI: equivalent to F + I option
                 *  FD: equivalent to F + D option
                 *  true  => I
                 *  false => S
                 */
//                debug($this->fetch('content'));die();
                require_once('../vendor/spipu/html2pdf/html2pdf.class.php');
                $pdf = new HTML2PDF('P', 'A4', 'pt', true, 'UTF-8', array(0, 0, 0, 0));
                $pdf->setDefaultFont('arial');
                $pdf->setTestIsImage(false);
                $pdf->WriteHTML($this->fetch('content'), isset($_GET['vuehtml']));
                $pdf->pdf->SetDisplayMode('fullpage');
                $pdf->pdf->SetProtection(array('modify', 'copy'));
                ob_get_clean();
                $pdf->Output($nomePDF, $tipoPDF);
                ?>
            </div>


        </body>
    </html>
</page>
<script type="text/javascript">

//     window.location = '<?php echo $this->request->webroot ?>pf-calculos/email/' +<?php echo $simulacao['id'] ?>

</script>