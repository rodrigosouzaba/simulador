<!DOCTYPE html>
<html>

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106538674-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-106538674-3');
    </script>



    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Portal do Corretor Parceiro - Corretor Parceiro
    </title>
    <?= $this->Html->script('jqueryNEW') ?>

    <?= $this->Html->script('maskInput') ?>
    <?= $this->Html->script('greensock') ?>

    <?= $this->Html->script('layerslider.transitions') ?>
    <?= $this->Html->script('layerslider.kreaturamedia.jquery') ?>

    <?php
    echo $this->Html->meta('favicon.ico', '/favicon.ico', array(
        'type' => 'icon'
    ));
    ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- <?= $this->Html->css('base.css', ['fullBase' => true]) ?> -->
    <!-- <?= $this->Html->css('cake.css', ['fullBase' => true]) ?> -->
    <!-- <?= $this->Html->css('sistema.css', ['fullBase' => true]) ?> -->
    <!-- <?= $this->Html->css('layerslider.css', ['fullBase' => true]) ?> -->

    <?= $this->Html->css('jquery-ui.min.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('jquery-ui.structure.min.css', ['fullBase' => true]) ?>
    <?= $this->Html->css('jquery-ui.theme.min.css', ['fullBase' => true]) ?>


    <!--Latest compiled and minified JavaScript-->
    <?= $this->Html->css('font-awesome.css', ['fullBase' => true]) ?>
    <?= $this->Html->script('jquery.maskMoney') ?>
    <?= $this->Html->script('jquery-ui.min') ?>
    <?= $this->Html->script('datepicker-pt-BR') ?>
    <?= $this->Html->script('clipboard.min') ?>
    <?= $this->Html->script('jquery.cpfcnpj.min') ?>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        .body {
            overflow: hidden;
        }

        .iframe {
            width: 100%;
            height: 100vh;
            border: 0;
        }
    </style>
</head>


<body>
    <?= $this->fetch('content') ?>
</body>

</html>
