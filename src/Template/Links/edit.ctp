<div class="col-xs-12">
    <?= $this->Form->create($link) ?>
    <fieldset>
        <legend><?= __('Editar Link') ?></legend>

        <div class="col-xs-8">
            <?= $this->Form->input('nome'); ?>
        </div>

        <div class="col-xs-4" id="PME" style="display: none;">
            <?= $this->Form->input('atendimentos[]', ['options' => $abrangencias, 'empty' => 'SELECIONE', 'label' => 'Atendimentos']); ?>
        </div>
        <div class="col-xs-4" id="PF" style="display: none;">
            <?= $this->Form->input('atendimentos[]', ['options' => $pfAtendimentos, 'empty' => 'SELECIONE', 'label' => 'Atendimentos']); ?>
        </div>
        <div class="col-xs-4" id="ODONTO" style="display: none;">
            <?= $this->Form->input('atendimentos[]', ['options' => $odontoAtendimentos, 'empty' => 'SELECIONE', 'label' => 'Atendimentos']); ?>
        </div>

        <div class="col-xs-12">
            <?= $this->Form->input('descricao', ['type' => 'textarea']); ?>
        </div>
        <div class="col-xs-12">
            <?= $this->Form->input('ramo', ['readonly']); ?>
        </div>
        <div class="col-xs-12">
            <?= $this->Form->input('usuario', ['readonly']); ?>
        </div>
</div>
</fieldset>
<div class="centralizada col-xs-12">
    <?= $this->Form->button("Salvar", ['class' => 'btn btn-primary']) ?>
</div>
<?= $this->Form->end() ?>
</div>
<script>
    var ramo = $("#ramo").val();
    switch (ramo) {
        case "SPF":
            $("#PF").show();
            break;
        case "SPJ":
            $("#PME").show();
            break;
        case "OPJ":
            $("#ODONTO").show();
            break;
        case "OPF":
            $("#ODONTO").show();
            break;
    }
</script>