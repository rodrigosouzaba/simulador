<div class="row">
    <?= $this->Form->create("compartilhar", ["id" => "compartilhar"]);?>
    <?= $this->Form->hidden("tipo", ["value" => "email", "class" => "tipo"]);?>
    <?= $this->Form->input("email", ["label" => false, "placeholder" => "Digite o Email que deseja enviar", "class" => "col-xs-12 col-md-8 col-md-offset-2 centralizada celular"]);?>
    <?= $this->Form->end()?>
</div>