<div class="col-xs-12">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('atendimentos', ["label" => "Atendimento"]) ?></th>
                <th><?= $this->Paginator->sort('modified', ["label" => "Data Alteração"]) ?></th>
                <th><?= $this->Paginator->sort('usuario', ["label" => "Usuário"]) ?></th>
                <th><?= $this->Paginator->sort('ramo') ?></th>
                <th>Link</th>
                <th class="actions centralizada" style="width: 5%"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($links as $link) : ?>
                <tr>
                    <td><?= h($link->nome) ?></td>
                    <td><?= h($link->atendimentos) ?></td>
                    <td><?= h($link->modified) ?></td>
                    <td><?= h($link->usuario) ?></td>
                    <td>
                        <?php
                        switch ($link->ramo) {
                            case "SPF":
                                echo "Saúde PF";
                                break;
                            case "SPJ":
                                echo "Saúde PME";
                                break;
                            case "OPF":
                                echo "Odonto PF";
                                break;
                            case "OPJ":
                                echo "Odonto PME";
                                break;
                        }
                        ?>
                    </td>
                    <td>
                        <?= $this->Html->link(strtolower("https://" . $_SERVER['SERVER_NAME'] . "/app/links/view/" . $link->id), "#", ["class" => "url", "data-toggle" => "tooltip", "role" => "button", "title" => "Copiado!", "data-trigger" => "click", 'sid' => 'links.view']); ?><span id="msg" class="text-danger" style="margin-left: 10px"></span>
                    </td>
                    <td class="actions centralizada center">
                        <div class="dropdown" style="float: none;">
                            <button id="dropDownMenu" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-cog"> </i>
                            </button>
                            <ul class="dropdown-menu " aria-labelledby="dropDownMenu" aria-labelledby="dLabel">
                                <li><?= $this->Html->link('<i class="fa fa-eye"></i> Visualizar', ['action' => 'view', $link->id], ['class' => 'dropdown-item', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Visualizar Tabela', 'sid' => 'links.view']) ?></li>
                                <li><?= $this->Html->link('<i class="fa fa-edit"></i> Editar', ['action' => 'edit', $link->id], ['class' => 'dropdown-item', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Editar Tabela', 'sid' => 'links.edit']) ?></li>
                                <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> Excluir ', ['action' => 'delete', $link->id], ['confirm' => __('Tem certeza que deseja excluir o Link da Tabela?', $link->id), 'class' => 'text-danger', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Excluir Tabela', 'sid' => 'links.delete']) ?></li>
                                <li class="divider"></li>
                                <li style="background: #F1F1F1; font-size: 9px; text-align: center;" data-toggle='tooltip' data-placement='left' title='Criado em: <?= $link->created ?>'>Criado em: <?= $link->created ?></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<script type="text/javascript">
    $(".url").click(function() {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(this).text()).select();
        document.execCommand("copy");
        $temp.remove();
    });
</script>