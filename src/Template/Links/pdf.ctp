<style type="text/css">
    .jumbotron {
        background-color: #C0C0C0;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 0 30px 0;
        padding-left: 20px;
        line-height: 1.5;
        /*width: 97%;*/
    }

    .fonteReduzida {
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        font-size: 7px !important;
        line-height: 1.5;
        margin-bottom: 0;
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }

    .TituloOperadora {
        background-color: #337ab7;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        line-height: 1.5;
        width: 100%;
    }

    .detalhesOperadora {
        padding: 10px 0px;
        font-weight: bold;
    }

    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
    }

    .operadoras {}

    .centralizada {
        text-align: center;
    }

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .fonteReduzida {
        font-size: 10px;
    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .esquerda {
        float: left;
        margin: 15px;

        font-family: sans-serif;
    }

    .direita {
        float: right;
    }

    .total {
        width: 100%;
    }

    .p15 {
        width: 15%;
    }

    .p85 {
        width: 85%;
    }

    .totalTabela {
        width: 100% !important;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .t100 {
        width: 100%;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .4col {
        width: 25% !important;
    }

    .corpo-gerador-tabelas {
        border: 1px solid #ddd !important;
        padding: 5px;
        width: 100%;
    }

    .titulo-tabela {
        background-color: #ccc;
        text-align: center;
        font-weight: bold;
        padding: 5px;
    }

    .precos {
        border: solid 0.5px #ddd;
        text-align: center;
        padding: 3px 0 3px 0 !important;
    }

    .nobreak {
        page-break-inside: avoid;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .page_header {
        width: 100% !important;
        max-width: 100% !important;
        min-width: 100% !important;
    }
</style>
<page backtop="32mm" backbottom="7mm" backleft="5mm" backright="5mm" style="font-size: 11px">
    <page_header style="margin-right: 20px;margin-left: 20px; margin-top: 20px; width: 100%; margin-bottom: 300px !important;">
        <?php echo  $this->element('cabecalho_tabela'); ?>
    </page_header>
    <page_footer style="font-size: 15px;">
        <div class="rodape-home col-xs-12" style="text-align: center;">
            <span style="color: #004057">Assessoria de Apoio para Corretores.</span><br>
            <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
                <?= '<img src="https://corretorparceiro.com.br/app/img/ciranda.png" style="height: 15px; margin-left: 5px;" />' ?>
                <span style="text-decoration: none !important"> corretorparceiro.com.br</span>
            </a>
        </div>
    </page_footer>
    <?php
    switch ($link->ramo) {
        case "SPF":
            echo $this->element("link_pf_tabela");
            break;

        case "SPJ":
            echo $this->element("link_tabela");
            break;

        case "OPF":
        case "OPJ":
            echo $this->element("link_odonto_tabela");
            break;
    }
    ?>
</page>