<?php
$linkWhats = 'https://wa.me/?text=' . urlencode('Visualize sua tabela de preços aqui https://corretorparceiro.com.br/app/links/view/') . $link->id;
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

<?php
if ($sessao['id']) { ?>
    <style>
        #acoesgerador {
            top: 150px;
        }
    </style>
<?php
}

?>
<?= $this->Form->create(null, ['url' => ['action' => 'pdf']]) ?>
<?= $this->Form->hidden('tipo', ['id' => 'tipo']) ?>
<?= $this->Form->hidden('link_id', ['value' => $link->id]) ?>
<?= $this->element('new_acoes') ?>
<?= $this->Form->end() ?>

<div class="container">

    <?php
    echo $this->element('share-email');
    echo $this->element('modal-login');
    echo $this->element('cabecalho_tabela_links');

    switch ($link->ramo) {
        case "SPF":
            if (is_null($link->new)) {
                echo $this->element("link_pf_tabela");
            } else {
                echo $this->element("new_link_tabela");
            }
            break;

        case "SPJ":
            if (is_null($link->new)) {
                echo $this->element("link_tabela");
            } else {
                echo $this->element("new_link_tabela");
            }

            break;
        case "OPF":
        case "OPJ":
            if (is_null($link->new)) {
                echo $this->element("link_odonto_tabela");
            } else {
                echo $this->element("new_link_odonto_tabela");
            }
            break;
    }
    ?>
</div>
<div class="spacer-md">&nbsp;</div>
<script type="text/javascript">
    $("#link").submit(function() {
        $("#loader").click();
        return true;
    });
    $("#link").bind('ajax:complete', function() {
        $("#fechar").click();
    });
    $(".url").click(function() {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(this).text()).select();
        document.execCommand("copy");
        $temp.remove();
    });
    $("#pdfdownload").click(function() {
        $("#tipo").val("D");
        setInterval(function() {
            $('#fechar').click();
        }, 50000);
    });



    $("#pdfbrowser").click(function() {
        $("#tipo").val("I");
        setInterval(function() {
            $('#fechar').click();
        }, 50000);
    });

    $("#whatsapp").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        let link = "<?= $linkWhats ?>";
        if (sessao != "") {
            window.open(link);
        } else {
            $("#modal-login").modal("show");
        }

    });
    $("#btn-email").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';

        if (sessao != "") {
            $("#modalCompartilhamento").modal("show");
        } else {
            $("#modal-login").modal("show");
        }
    });

    $("#print-js").click(function(e) {
        e.preventDefault();
        window.print();
    });
</script>