<div class="row">
    <?= $this->Form->create("compartilhar", ["id" => "compartilhar"]);?>
    <?= $this->Form->hidden("tipo", ["value" => "whatsapp", "id" => "tipo"]);?>
    <?= $this->Form->input("celular", ["label" => false, "maxlength" => "11","placeholder" => "Digite o número do WhatsApp que deseja enviar", "class" => "col-xs-12 col-md-8 col-md-offset-2 centralizada celular"]);?>
    <?= $this->Form->end()?>
</div>