<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Links Personalizados'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Vendas Onlines Operadoras'), ['controller' => 'VendasOnlinesOperadoras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Vendas Onlines Operadora'), ['controller' => 'VendasOnlinesOperadoras', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="linksPersonalizados form large-9 medium-8 columns content">
    <?= $this->Form->create($linksPersonalizado) ?>
    <fieldset>
        <legend><?= __('Add Links Personalizado') ?></legend>
        <?php
            echo $this->Form->input('vendas_onlines_operadora_id', ['options' => $vendasOnlinesOperadoras]);
            echo $this->Form->input('cpf');
            echo $this->Form->input('link');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
