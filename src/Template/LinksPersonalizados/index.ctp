<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Links Personalizado'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Vendas Onlines Operadoras'), ['controller' => 'VendasOnlinesOperadoras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Vendas Onlines Operadora'), ['controller' => 'VendasOnlinesOperadoras', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="linksPersonalizados index large-9 medium-8 columns content">
    <h3><?= __('Links Personalizados') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('vendas_onlines_operadora_id') ?></th>
                <th><?= $this->Paginator->sort('cpf') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($linksPersonalizados as $linksPersonalizado): ?>
            <tr>
                <td><?= $this->Number->format($linksPersonalizado->id) ?></td>
                <td><?= $linksPersonalizado->has('vendas_onlines_operadora') ? $this->Html->link($linksPersonalizado->vendas_onlines_operadora->id, ['controller' => 'VendasOnlinesOperadoras', 'action' => 'view', $linksPersonalizado->vendas_onlines_operadora->id]) : '' ?></td>
                <td><?= h($linksPersonalizado->cpf) ?></td>
                <td><?= h($linksPersonalizado->created) ?></td>
                <td><?= h($linksPersonalizado->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $linksPersonalizado->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $linksPersonalizado->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $linksPersonalizado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $linksPersonalizado->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
