<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Links Personalizado'), ['action' => 'edit', $linksPersonalizado->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Links Personalizado'), ['action' => 'delete', $linksPersonalizado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $linksPersonalizado->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Links Personalizados'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Links Personalizado'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Vendas Onlines Operadoras'), ['controller' => 'VendasOnlinesOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vendas Onlines Operadora'), ['controller' => 'VendasOnlinesOperadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="linksPersonalizados view large-9 medium-8 columns content">
    <h3><?= h($linksPersonalizado->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Vendas Onlines Operadora') ?></th>
            <td><?= $linksPersonalizado->has('vendas_onlines_operadora') ? $this->Html->link($linksPersonalizado->vendas_onlines_operadora->id, ['controller' => 'VendasOnlinesOperadoras', 'action' => 'view', $linksPersonalizado->vendas_onlines_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Cpf') ?></th>
            <td><?= h($linksPersonalizado->cpf) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($linksPersonalizado->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($linksPersonalizado->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($linksPersonalizado->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Link') ?></h4>
        <?= $this->Text->autoParagraph(h($linksPersonalizado->link)); ?>
    </div>
</div>
