<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $linksTabela->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $linksTabela->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Links Tabelas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Links'), ['controller' => 'Links', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Link'), ['controller' => 'Links', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="linksTabelas form large-9 medium-8 columns content">
    <?= $this->Form->create($linksTabela) ?>
    <fieldset>
        <legend><?= __('Edit Links Tabela') ?></legend>
        <?php
            echo $this->Form->input('link_id', ['options' => $links]);
            echo $this->Form->input('tabela_id', ['options' => $tabelas, 'empty' => true]);
            echo $this->Form->input('pf_tabela_id', ['options' => $pfTabelas, 'empty' => true]);
            echo $this->Form->input('odonto_tabela_id', ['options' => $odontoTabelas, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
