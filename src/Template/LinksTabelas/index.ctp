<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Links Tabela'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Links'), ['controller' => 'Links', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Link'), ['controller' => 'Links', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="linksTabelas index large-9 medium-8 columns content">
    <h3><?= __('Links Tabelas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('link_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('tabela_id') ?></th>
                <th><?= $this->Paginator->sort('pf_tabela_id') ?></th>
                <th><?= $this->Paginator->sort('odonto_tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($linksTabelas as $linksTabela): ?>
            <tr>
                <td><?= $this->Number->format($linksTabela->id) ?></td>
                <td><?= $linksTabela->has('link') ? $this->Html->link($linksTabela->link->id, ['controller' => 'Links', 'action' => 'view', $linksTabela->link->id]) : '' ?></td>
                <td><?= h($linksTabela->created) ?></td>
                <td><?= $linksTabela->has('tabela') ? $this->Html->link($linksTabela->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $linksTabela->tabela->id]) : '' ?></td>
                <td><?= $linksTabela->has('pf_tabela') ? $this->Html->link($linksTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $linksTabela->pf_tabela->id]) : '' ?></td>
                <td><?= $linksTabela->has('odonto_tabela') ? $this->Html->link($linksTabela->odonto_tabela->id, ['controller' => 'OdontoTabelas', 'action' => 'view', $linksTabela->odonto_tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $linksTabela->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $linksTabela->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $linksTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $linksTabela->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
