<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Links Tabela'), ['action' => 'edit', $linksTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Links Tabela'), ['action' => 'delete', $linksTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $linksTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Links Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Links Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Links'), ['controller' => 'Links', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Link'), ['controller' => 'Links', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="linksTabelas view large-9 medium-8 columns content">
    <h3><?= h($linksTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Link') ?></th>
            <td><?= $linksTabela->has('link') ? $this->Html->link($linksTabela->link->id, ['controller' => 'Links', 'action' => 'view', $linksTabela->link->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tabela') ?></th>
            <td><?= $linksTabela->has('tabela') ? $this->Html->link($linksTabela->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $linksTabela->tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Tabela') ?></th>
            <td><?= $linksTabela->has('pf_tabela') ? $this->Html->link($linksTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $linksTabela->pf_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Tabela') ?></th>
            <td><?= $linksTabela->has('odonto_tabela') ? $this->Html->link($linksTabela->odonto_tabela->id, ['controller' => 'OdontoTabelas', 'action' => 'view', $linksTabela->odonto_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($linksTabela->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($linksTabela->created) ?></td>
        </tr>
    </table>
</div>
