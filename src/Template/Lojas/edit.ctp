<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $loja->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $loja->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Lojas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Ramos'), ['controller' => 'Ramos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ramo'), ['controller' => 'Ramos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="lojas form large-9 medium-8 columns content">
    <?= $this->Form->create($loja) ?>
    <fieldset>
        <legend><?= __('Edit Loja') ?></legend>
        <?php
            echo $this->Form->input('ramo_id', ['options' => $ramos]);
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('nome');
            echo $this->Form->input('descricao');
            echo $this->Form->input('status');
            echo $this->Form->input('url');
            echo $this->Form->input('tipo_pessoa');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
