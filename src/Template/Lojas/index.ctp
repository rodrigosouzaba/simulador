<?php
//   header('Access-Control-Allow-Origin: http://api.google.com');
  header('Access-Control-Allow-Origin: https://www.odontoutilis.com.br');
?>
<div class="col-xs-12 fonteReduzida">
    <h3 class="centralizada"> Minhas Lojas </h3>
</div>
<div class="col-xs-12 fonteReduzida">
        <?php foreach ($lojas as $ramo => $tipo_pessoa): ?>
		<div class="col-xs-12">
			<div class="col-md-3">
				<a class="btn btn-default" role="button" data-toggle="collapse" href="#<?=$ramo?>" aria-expanded="false" aria-controls="collapseExample">
					<?= $ramo ?>
				</a>
			</div>
		</div>
		
		<div class="col-xs-12">
			<div class="collapse" id="<?=$ramo?>">
				<?php 
					foreach($tipo_pessoa as $chave => $tipo_pessoa) {
						foreach($tipo_pessoa as $id => $dadosLoja){ ?>
							<div class="spacer-sm"></div>
							<div class="col-xs-6 col-md-2 centralizada">
								<?php echo $this->Html->image("caixa-odonto.jpg", ["class" => "img-thumbnail", "style" => "padding: 25% 5%;"])?><br>
							</div>
							<div class="col-xs-12" style="margin-top: 10px; padding-left: 0; padding-right: 0;" id="teste">
								<iframe id="frame" width="100%" height="100%" src="<?= $dadosLoja['url']?>" scrolling="yes" style="overflow-y: scroll; border: none"></iframe>
							</div>		
			</div>		
							
				<?php  	
						}
					}
				?>
		</div>
		
        <?php endforeach; ?>
</div>

<script>
    $(document).ready(function () {
	    $("#frame").height($(window).height()*3);
	    $("#teste").height($(window).height());
    });
</script>
<!--
<div id="a"></div>
<script type="text/javascript">
xhr = new XMLHttpRequest();
xhr.onreadystatechange = function() {
if(xhr.readyState == 4 && xhr.status == 200) {
    document.getElementById('a').innerHTML = xhr.responseText;
  }
};
xhr.open('GET', 'https://www.odontoutilis.com.br/ConnectOdontoWeb/lojaprospecto/showIniciarProposta.action?idcfg=20&token=44487&idUsuario=46117&tokenUsuario=16cbf50656c7b04935a318bd7c2d61d9&tokenExterno=6a29b95c68b5ad6df3ad18b99a6a5f9520190206&publico=true', true);
xhr.send();
</script>
-->