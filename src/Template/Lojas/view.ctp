<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Loja'), ['action' => 'edit', $loja->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Loja'), ['action' => 'delete', $loja->id], ['confirm' => __('Are you sure you want to delete # {0}?', $loja->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Lojas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Loja'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ramos'), ['controller' => 'Ramos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ramo'), ['controller' => 'Ramos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="lojas view large-9 medium-8 columns content">
    <h3><?= h($loja->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Ramo') ?></th>
            <td><?= $loja->has('ramo') ? $this->Html->link($loja->ramo->id, ['controller' => 'Ramos', 'action' => 'view', $loja->ramo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $loja->has('user') ? $this->Html->link($loja->user->id, ['controller' => 'Users', 'action' => 'view', $loja->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($loja->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= h($loja->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo Pessoa') ?></th>
            <td><?= h($loja->tipo_pessoa) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($loja->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($loja->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($loja->descricao)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url') ?></h4>
        <?= $this->Text->autoParagraph(h($loja->url)); ?>
    </div>
</div>
