<style>
    .estados {
        border: 1px solid #ccc;
        padding: 0px;
    }

    .select-estado {
        display: flex;
        align-items: center;
        padding: 0 15px;
    }

    .select-estado:hover {
        background-color: #DDD;
    }

    .select-estado span {
        padding: 5px 0;
    }

    .active {
        background: #08c;
        color: #FFF;
    }
</style>
<div class="container content">
    <?= $this->Form->create($metropole) ?>
    <fieldset>
        <h3><?= $this->request->getParam('action') == 'add' ? 'Nova' : 'Editar' ?> Metropole</h3>

        <div class="col-md-12">
            <?= $this->Form->control('nome') ?>
        </div>

        <div class="col-md-6" style="margin-top: 1rem; min-height: 220px">
            <?= $this->Form->input('estado_id', ['class' => 'select-estado', 'empty' => 'Selecione']) ?>
        </div>

        <div class="col-md-6 tab-content" style="margin-top: 1rem;">
            <label>Municipios</label>
            <?php foreach ($municipios as $estado) : ?>
                <div class="municipios tab-pane <?= $metropole->estado_id == $estado->id ? 'show' : '' ?>" id="tab-<?= $estado->id ?>" estado="<?= $estado->id ?>">
                    <div class="flex" style="justify-content: space-between;">
                        <a href='#' id='select-all-<?= $estado->id ?>'>Seleiconar Tudo</a>
                        <a href='#' id='deselect-all-<?= $estado->id ?>'>Limpar</a>
                    </div>
                    <select name="municipios[<?= $estado->id ?>][]" class="multiselect" id="multiple-municipio-estado-<?= $estado->id ?>" estado-id="<?= $estado->id ?>" multiple>
                        <?php $municipios = (new \Cake\Collection\Collection($estado->municipios))->combine('id', 'nome')->toArray();
                        foreach ($municipios as $id => $nome) : ?>
                            <option value="<?= $id ?>"><?= $nome ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <script>
                    $(document).ready(function() {
                        var estado = $("#multiple-municipio-estado-<?= $estado->id ?>").attr("estado-id");

                        $("#multiple-municipio-estado-<?= $estado->id ?>").multiSelect({
                            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                            afterInit: function(ms) {
                                var that = this,
                                    $selectableSearch = that.$selectableUl.prev(),
                                    $selectionSearch = that.$selectionUl.prev(),
                                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                                    .on('keydown', function(e) {
                                        if (e.which === 40) {
                                            that.$selectableUl.focus();
                                            return false;
                                        }
                                    });

                                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                                    .on('keydown', function(e) {
                                        if (e.which == 40) {
                                            that.$selectionUl.focus();
                                            return false;
                                        }
                                    });
                            },
                            afterSelect: function(values) {
                                this.qs1.cache();
                                this.qs2.cache();
                                var selecionados = $('#multiple-municipio-estado-<?= $estado->id ?> option:selected').length;
                                $("#select-estado-" + estado).addClass("active");
                            },
                            afterDeselect: function(values) {
                                this.qs1.cache();
                                this.qs2.cache();
                                var selecionados = $('#multiple-municipio-estado-<?= $estado->id ?> option:selected').length;
                                if (selecionados == 0) {
                                    $("#select-estado-" + estado).removeClass("active");
                                }
                            }
                        })
                    })
                    $("#select-all-<?= $estado->id ?>").click(function() {
                        $("#multiple-municipio-estado-<?= $estado->id ?>").multiSelect('select_all')
                    });
                    $("#deselect-all-<?= $estado->id ?>").click(function() {
                        $("#multiple-municipio-estado-<?= $estado->id ?>").multiSelect('deselect_all')
                    });
                </script>
            <?php endforeach; ?>
        </div>

    </fieldset>
    <div class="text-center">
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
<script>
    $(".select-estado").change(function() {
        var estado = $(this).val();
        $('.tab-pane').hide();
        $('#tab-' + estado).show();
    });
</script>