<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Metropole[]|\Cake\Collection\CollectionInterface $metropoles
 */
?>
<div class="container content">
    <!-- <h3><?= __('Metropoles') ?></h3> -->
    <div class="col-md-3 fonteReduzida">
        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Metropole', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'observacoes.add']) ?>
    </div>
    <div class="spacer-md">&nbsp;</div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($metropoles as $metropole) : ?>
                <tr>
                    <td><?= $this->Number->format($metropole->id) ?></td>
                    <td><?= h($metropole->nome) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $metropole->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $metropole->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $metropole->id], ['confirm' => __('Are you sure you want to delete # {0}?', $metropole->id)]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>