<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Metropole $metropole
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Metropole'), ['action' => 'edit', $metropole->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Metropole'), ['action' => 'delete', $metropole->id], ['confirm' => __('Are you sure you want to delete # {0}?', $metropole->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Metropoles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Metropole'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="metropoles view large-9 medium-8 columns content">
    <h3><?= h($metropole->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($metropole->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($metropole->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Municipios') ?></h4>
        <?php if (!empty($metropole->municipios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Codigo') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Estado Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($metropole->municipios as $municipios): ?>
            <tr>
                <td><?= h($municipios->id) ?></td>
                <td><?= h($municipios->codigo) ?></td>
                <td><?= h($municipios->nome) ?></td>
                <td><?= h($municipios->estado_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Municipios', 'action' => 'view', $municipios->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Municipios', 'action' => 'edit', $municipios->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Municipios', 'action' => 'delete', $municipios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipios->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
