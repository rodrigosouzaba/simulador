<?php echo $this->element('forms/title', ['title' => '']); ?>
<?= $this->Form->hidden('nome', ['id' => 'userNome', 'value' =>  $user['nome']]); ?>
<?= $this->Form->hidden('celular', ['id' => 'userCelular', 'value' =>  $user['celular']]); ?>
<?= $this->Form->hidden('whatsapp', ['id' => 'userWhatsapp', 'value' =>  $user['whatsapp']]); ?>
<?= $this->Form->hidden('email', ['id' => 'userEmail', 'value' =>  $user['email']]); ?>
<?= $this->Form->hidden('validade', ['id' => 'validadeMidia', 'value' =>  $midia['validade']]); ?>

<div class="card-body" >
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12" style="padding: 0px;">
            <div class="imagens" style="display: none;" midia="<?= $midia->id ?>" path="<?= $this->request->webroot . "uploads/midias_compartilhadas/" . $midia->imagem ?>">
                <?= $this->Html->image('/uploads/midias_compartilhadas/' . $midia->imagem) ?>
            </div>
            <div class="w-100 text-center" style="margin-top: 10px; ">
                <canvas class="canva-preview" id="canvas-<?= $midia->id ?>" style="width: 100%;"></canvas>
            </div>
        </div>
    </div>
    <div class="acoesModal">
            <?= $this->Html->link(
                $this->Html->image('botoes/download.png', ['style' => 'width:50px; border-radius: 15px']),
                "javascript: void(0);",
                [
                    'path' => $this->request->webroot . "uploads/midias_compartilhadas/{$midia->imagem}",
                    'escape' => false,
                    "nome" => $midia->imagem,
                    'midia_id' => $midia->id,
                    'title' => 'Download',
                    'class' => 'download btn btn-default icones-btn'
                ]
            ) ?>
            &nbsp
            <?= $this->Html->link(
                $this->Html->image('botoes/mail.png', ['style' => 'width:50px; border-radius: 15px']),
                'javascript: void(0);',
                ['escape' => false, 'target' => '_blank', 'title' => 'Enviar email', 'class' => "email btn btn-default icones-btn"]
            ) ?>
    </div>
</div>