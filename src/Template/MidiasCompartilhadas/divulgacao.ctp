<style>
    .heading-color {
        background-color: #003656 !important;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 10px 10px 5px 10px;
    }

    .heading-color .panel-title {
        color: #FFF !important;
    }

    .bloco-add {
        border-right: 1px solid #f5f5f5
    }

    .img-thumbnail {
        max-height: 120px;
        max-width: 120px;
    }

    .buttons {
        margin-top: 20px;
    }

    .group-operadoras {
        display: flex;
        flex-wrap: wrap;
    }

    .before-card {
        /* width: 10%; */
        /* margin: 0.71%; */
        text-decoration: none;
        text-align: center;
    }

    .card {
        border: 1px solid #ccc;
        border-radius: 5px;
        overflow: hidden;
    }

    .card:hover .actions {
        visibility: visible;
    }

    .card-header {
        position: relative;
    }

    .card-header img {
        width: 100%;
    }

    .card-body {
        padding: 5px;
        font-size: 14px;
        color: #595959
    }

    .btn-vendas-active,
    .btn-vendas-active:active,
    .btn-vendas-active:hover,
    .btn-vendas-active:focus {
        background-color: #003656;
        color: #FFF;
    }

    .btnModalVideos {
        color: #d9534f;
        border: 0;
    }

    .btnModalVideos:hover {
        color: #d9534f;
    }

    .btnManuais {
        color: #d9534f;
        border: 0;
    }

    .btnManuais:hover {
        color: #d9534f;
    }

    .btnManuais:focus {
        color: #d9534f;
        background-color: #FFF;
    }

    .btnMaterial {
        color: #d9534f;
        border: 0;
    }

    .btnMaterial:hover {
        color: #d9534f;
    }

    .btnMaterial:focus {
        color: #d9534f;
        background-color: #FFF;
    }

    @media(max-width: 991px) {
        .before-card {
            width: 23%;
            margin: 1%;
        }

    }

    @media(max-width: 600px) {
        .before-card {
            width: 46%;
            margin: 2%;
        }

    }

    .oculta {
        opacity: 0.2;
    }

    #add-buttton {
        display: none;
    }

    .actions {
        visibility: hidden;
        position: absolute;
        left: 3px;
        right: 3px;
        bottom: 3px;
        display: flex;
        justify-content: space-between;
        flex-direction: column;
    }

    .estados {
        position: absolute;
        top: 50;
        left: 0;
        border: 1px solid #ccc;
    }
</style>
<section id="AppMidiasCompartilhadas">

    <div class="col-xs-12">
        <div class="col-xs-12 centralizada" style="margin-bottom: 10px !important;">
            <div class="btn-group">
                <?php foreach ($midiasCompartilhadas as $k => $midiasCompartilhada) : ?>
                    <a href="<?= "#tab-" . $midiasCompartilhada->id ?>" aria-controls="<?= $midiasCompartilhada->id ?>" role="tab" data-toggle="tab" class="btn btn-sm btn-default btn-vendas <?= $k == 0 ? 'active' : '' ?>">
                        <?= $midiasCompartilhada->titulo ?></br> <?= $midiasCompartilhada->menu_linha1 ?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
    <?php

    ?>
    <div class="tab-content">
        <?php foreach ($midiasCompartilhadas as $k => $midiasCompartilhada) { ?>

            <div role="tabpanel" class="tab-pane <?= $k == 0 ? 'active' : '' ?>" id="tab-<?= $midiasCompartilhada->id ?>">
                <div class="row cards group-operadoras">
                    <?php foreach ($midiasCompartilhada->midias_compartilhadas_operadoras as $value) { ?>
                        <div class="col-xs-12 col-md-2 col-sm-2 col-lg-2 before-card text-center">
                            <div class="card">
                                <?php $class = !empty($this->getRequest()->getSession()->read('Auth')) ? 'btnCompartilharMidia' : '' ?>
                                <?php $href = empty($this->getRequest()->getSession()->read('Auth')) ? '#modal-login' : 'javascript:void(0);' ?>
                                <?php $attr = empty($this->getRequest()->getSession()->read('Auth')) ? 'data-toggle="modal"' : '#' ?>
                                <a href="<?= $href ?>" <?= $attr ?> class="<?= $class ?>" midia_compartilhada_id="<?= $value->id ?>">
                                    <div class="card-header">
                                        <div class="imagens-preview" style="display: none;" midia="<?= $value->id ?>" path="<?= $this->request->webroot . "uploads/midias_compartilhadas/" . $value->imagem ?>">
                                            <?= $this->Html->image('/uploads/midias_compartilhadas/' . $value->imagem) ?>
                                        </div>
                                        <div class="w-100 text-center" style="margin: 10px;">
                                            <canvas class="canva-preview" id="canvas-preview-<?= $value->id ?>" midia_id="<?= $value->id ?>" style="width: 100%; height: 100%;" height="280"></canvas>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php if(!empty($value->linha1) || !empty($value->linha2)) : ?>
                            <div class="card-body">
                                <?php if (!empty($value->linha1)) : ?>
                                <span><?= $value->linha1 ?></span>
                                <br />
                                <?php endif; ?>
                                <?php if (!empty($value->linha2)) : ?>
                                <span><?= $value->linha2 ?></span>
                                <?php endif; ?>
                            </div>
                            <?php endif; ?>
                            <?php if (!empty($value->imagem)) : ?>
                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-share']) . ' Compartilhar', $href, ['class' => "btn btn-sm btn-default btnMaterial $class", 'escape' => false, 'midia_compartilhada_id' => $value->id, 'data-toggle' => $attr !== '#' ? 'modal' : '#']) ?>
                            <?php endif; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>

    <?= $this->Html->image('previa.png', ['style' => 'display: none;', 'id' => 'imgPrevia']) ?>
    <?= $this->element('modal-login') ?>
</section>