<?= $this->Form->create($midiasCompartilhada, ['class' => 'form form-validate', 'role' => 'form']); ?>
<?= $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Editar Categoria']); ?>
<?= $this->Form->hidden('id'); ?>
<?= $this->Form->hidden('nome', ['id' => 'nome']); ?>
<?= $this->Form->hidden('path', ['id' => 'path']); ?>
<!-- <?= debug($midiasCompartilhada) ?> -->
<div class="card-body">
    <div class="row">
        <div class="col-sm-8 col-lg-8 col-xs-12 col-md-8">
            <div class="form-group">
                <?= $this->Form->input('titulo', ['label' => 'Título', 'required']) ?>
            </div>
        </div>
        <div class="col-sm-4 col-lg-4 col-xs-12 col-md-4">
            <div class="form-group">
                <?= $this->Form->input('exibir_midias_compartilhadas', ['type' => 'select', 'required', 'label' => 'Exibir em menu Material Divulgação', 'options' => [1 => 'Sim', 0 => 'Não']]); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-lg-6 col-xs-12 col-md-6">
            <div class="form-group">
                <?= $this->Form->input('descricao', ['label' => 'Descrição', 'required', 'type' => 'text']) ?>
            </div>
        </div>
        <div class="col-sm-2 col-lg-2 col-xs-12 col-md-2">
            <div class="form-group">
                <?= $this->Form->input('ordem', ['required', 'label' => 'Odem']); ?>
            </div>
        </div>
        <div class="col-sm-2 col-lg-2 col-xs-12 col-md-2">
            <div class="form-group">
                <?= $this->Form->input('validade', ['required', 'label' => 'Validade', 'type' => 'text', 'class' => 'data']); ?>
            </div>
        </div>
        <div class="col-sm-2 col-lg-2 col-xs-12 col-md-2">
            <div class="form-group">
                <?= $this->Form->input('active', ['type' => 'select', 'required', 'label' => 'Ativo', 'options' => [1 => 'Sim', 0 => 'Não']]); ?>
            </div>
        </div>
    </div>
    <!-- titulo -->
    <div class="row">
        <div class="col-sm-2 col-lg-2 col-xs-12 col-md-2">
            <div class="form-group">
                <?php echo $this->Form->input('exibir_menu', ['type' => 'select', 'label' => 'Exibir Menu', 'options' => [1 => 'Sim', 0 => 'Não']]); ?>
            </div>
        </div>
        <div class="col-sm-10 col-lg-10 col-xs-12 col-md-10">
            <div class="form-group">
                <?= $this->Form->input('menu_linha1', ['label' => 'Menu Linha 1', 'class' => 'input_exibe_men/u']) ?>
            </div>
        </div>
        <!-- <div class="col-sm-5 col-lg-5 col-xs-12 col-md-5">
            <div class="form-group">
                <?= $this->Form->input('menu_linha2', ['label' => 'Menu Linha 2', 'class' => 'input_exibe_menu']) ?>
            </div>
        </div> -->
    </div>

    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
            <div class="form-group">
                <?= $this->Form->input('ramo', ['options' => $ramos, 'empty' => 'Selecione Ramo']) ?>
            </div>
        </div>
        <!-- <div class="col-sm-6 col-lg-6 col-xs-6 col-md-6">
            <div class="form-group" id="operadoras-field">
                <?= $this->Form->input('operadora_id', ['id' => 'operadora', 'empty' => 'Selecione Operadora', 'default' => $operadoras]) ?>
            </div>
        </div> -->
    </div>

    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#menu1">Midia</a></li>
                <li><a data-toggle="tab" href="#menu2">Atualizar Imagem</a></li>
            </ul>

            <div class="tab-content text-center" style="padding: 20px">
                <div id="menu1" class="tab-pane fade in active text-center">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
                            <?php if (!empty($midiasCompartilhada->nome)) { ?>
                                <?= $this->Html->image('/uploads/midias_compartilhadas/' . $midiasCompartilhada->nome) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12 fallback">
                            <div id="imagem" class="dropzone"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<?php echo $this->element('forms/buttons') ?>
<?php echo $this->Form->end(); ?>
