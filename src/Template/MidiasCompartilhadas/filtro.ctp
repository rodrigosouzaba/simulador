<?php foreach ($midiasCompartilhadas as $midiasCompartilhada) : ?>
    <div id="imagem-<?= $midiasCompartilhada->id ?>" class="imagens" midia="<?= $midiasCompartilhada->id ?>" path="<?= $this->request->webroot . "uploads/midias_compartilhadas/" . $midiasCompartilhada->nome ?>">
        <a href="#pre-visualization-<?= $midiasCompartilhada->id ?>" data-toggle="modal">
            <img src="<?= $this->request->webroot . 'uploads/midias_compartilhadas/' . $midiasCompartilhada->nome ?>" />
        </a>

        <?= $this->Html->link('Download', '#', [
            "class" => "btn btn-sm btn-default download mt-1",
            "nome" => $midiasCompartilhada->nome,
            "midia" => $midiasCompartilhada->id
        ]) ?>
    </div>

    <div class="modal fade" id="pre-visualization-<?= $midiasCompartilhada->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <canvas class="canva-preview" id="canvas-<?= $midiasCompartilhada->id ?>" width="810" height="810"></canvas>
            <div class="w-100 text-center" style="margin-top: 10px;">
                <?= $this->Html->link('Download', '#', [
                    "class" => "btn btn-sm btn-default download",
                    "nome" => $midiasCompartilhada->nome,
                    "midia" => $midiasCompartilhada->id,
                ]) ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var midia = $('#imagem-<?= $midiasCompartilhada->id ?>').attr("midia");
        var canvas = document.getElementById("canvas-" + midia);
        var context = canvas.getContext("2d");
        var path = $('#imagem-<?= $midiasCompartilhada->id ?>').attr("path");
        var logo = new Image();
        logo.src = '<?= $this->request->webroot ?>uploads/imagens/usuarios/logos/<?= $user['logo'] ?>';

        var img = new Image();
        img.src = path;
        img.crossOrigin = "anonymous";
        img.onload = function() {
            context.drawImage(
                this, //imagem a ser utilizada
                0, //X do inicio do recorte da imagem
                0, //Y do inicio do recorte da imagem
                this.naturalWidth, //Até onde vai o corte da imagem em X
                this.naturalHeight, //Até onde vai o corte da imagem em Y
                0, //coordenada onde começa o espaço da imagem em X
                0, //coordenada onde começa o espaço da imagem em Y
                810,
                810
            );
            context.drawImage(logo, 40, 730, logo.width * (70 / logo.height), 70)
            context.lineWidth = 1;
            context.fillStyle = "#000";
            context.lineStyle = "#000";
            context.font = "bold 13px sans-serif";
            addNome(context);
            context.font = "13px sans-serif";
            addCelular(context);
            addWhatsapp(context);
            addEmail(context);
        };
    </script>
<?php endforeach; ?>