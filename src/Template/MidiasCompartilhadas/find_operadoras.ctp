<?= $this->Form->input('operadora_id', ['label' => !is_null($action) ? false : 'Operadora', 'options' => $operadoras, 'empty' => 'Selecione Operadora']) ?>
<?php if (!is_null($action)) : ?>
    <script>
        $('#operadora-id').change(function() {
            var ramo = $('#ramo').val();
            var operadora = $(this).val();
            $.get(baseUrl + `midiasCompartilhadas/filtro/${ramo}/${operadora}`, function(data) {
                $('#midias-compartilhadas').empty()
                $('#midias-compartilhadas').append(data)
            })
        });
    </script>
<?php endif; ?>