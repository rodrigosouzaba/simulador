<style>
    .heading-color {
        background-color: #003656 !important;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 10px 10px 5px 10px;
    }

    .heading-color .panel-title {
        color: #FFF !important;
    }

    .bloco-add {
        border-right: 1px solid #f5f5f5
    }

    .img-thumbnail {
        max-height: 120px;
        max-width: 120px;
    }

    .buttons {
        margin-top: 20px;
    }

    .group-operadoras {
        display: flex;
        flex-wrap: wrap;
    }

    .before-card {
        width: 10%;
        margin: 0.71%;
        text-decoration: none;
        text-align: center;
    }

    .card {
        border: 1px solid #ccc;
        border-radius: 5px;
        overflow: hidden;
    }

    .card:hover .actions {
        visibility: visible;
    }

    .card-header {
        position: relative;
    }

    .card-header img {
        width: 100%;
    }

    .card-body {
        padding: 5px;
        font-size: 14px;
        color: #595959
    }

    .btn-vendas-active,
    .btn-vendas-active:active,
    .btn-vendas-active:hover,
    .btn-vendas-active:focus {
        background-color: #003656;
        color: #FFF;
    }

    .btnModalVideos {
        color: #d9534f;
        border: 0;
    }

    .btnModalVideos:hover {
        color: #d9534f;
    }

    .btnManuais {
        color: #d9534f;
        border: 0;
    }

    .btnManuais:hover {
        color: #d9534f;
    }

    .btnManuais:focus {
        color: #d9534f;
        background-color: #FFF;
    }

    .btnMaterial {
        color: #d9534f;
        border: 0;
    }

    .btnMaterial:hover {
        color: #d9534f;
    }

    .btnMaterial:focus {
        color: #d9534f;
        background-color: #FFF;
    }

    @media(max-width: 991px) {
        .before-card {
            width: 23%;
            margin: 1%;
        }

    }

    @media(max-width: 600px) {
        .before-card {
            width: 46%;
            margin: 2%;
        }

    }

    .oculta {
        opacity: 0.2;
    }

    #add-buttton {
        display: none;
    }

    .actions {
        visibility: hidden;
        position: absolute;
        left: 3px;
        right: 3px;
        bottom: 3px;
        display: flex;
        justify-content: space-between;
        flex-direction: column;
    }

    .estados {
        position: absolute;
        top: 50;
        left: 0;
        border: 1px solid #ccc;
    }
</style>
<section id="AppMidiasCompartilhadas">

    <div class="modulo">
        <div class="col-md-12">
            CONFIGURAR MIDIAS COMPARTILHADAS
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <ul class="nav nav-pills nav-justified">
                <?php foreach ($midiasCompartilhadas as $k => $midiasCompartilhada) { ?>
                    <li role="presentation" class="<?= $k == '0' ? "active" : '' ?>">
                        <a data-toggle="tab" href="#tab-<?= $midiasCompartilhada->id ?>" class="btn btn-sm btn-vendas">
                            <?= $midiasCompartilhada->titulo ?></br> <?= $midiasCompartilhada->menu_linha1 ?>
                        </a>
                    </li>

                <?php } ?>
                <li role="presentation">
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . '<br/>' . ' Novo Botão', 'javascript:void(0);', ['class' => 'btn btn-sm btn-vendas', 'data-toggle' => "tab", 'escape' => false, 's-id' => 'midiasCompartilhadas.add', 'id' => 'addMidiasCompartilhadas']) ?>
                </li>
            </ul>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="tab-content col-sm-12">
            <?php foreach ($midiasCompartilhadas as $k => $midiasCompartilhada) { ?>

                <div class="tab-pane fade in <?= $k == 0 ? 'active' : '' ?>" id="tab-<?= $midiasCompartilhada->id ?>">
                    <div class="row">
                        <div class="col-md-12 fontReduzida text-center">
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Midia', 'javascript:void(0);', ['midia_compartilhada_id' => $midiasCompartilhada->id, 'class' => 'btn btn-sm btn-primary addMidiaForOperadora', 'role' => 'button', 'escape' => false]); ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-edit', 'aria-hidden' => 'true']) . ' Editar Categoria', 'javascript:void(0);', ['midia_compartilhada_id' => $midiasCompartilhada->id, 'class' => 'btn btn-sm btn-warning btnEditarMidiaCompartilhada', 'role' => 'button', 'escape' => false]); ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']) . ' Excluir Categoria', 'javascript:void(0);', ['midia_compartilhada_id' => $midiasCompartilhada->id, 'class' => 'btn btn-sm btn-danger btnDeleteCategoria', 'role' => 'button', 'escape' => false]); ?>
                        </div>
                    </div>

                    <div class="col-xs-12 cards group-operadoras">
                        <?php foreach ($midiasCompartilhada->midias_compartilhadas_operadoras as $value) : ?>
                            <div class="before-card text-center">
                                <div class="card">
                                    <div class="card-header">
                                        <?= $this->Html->image('/uploads/midias_compartilhadas/' . $value->imagem) ?>
                                        <div class="actions">
                                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-edit']) . ' Editar', 'javascript:void(0);', ['class' => 'btn btn-sm btn-default btnEditarMidiaOperadoraEstado', 'id' => $value->id, 'midia_compartilhada_id' => $midiasCompartilhada->id,  'escape' => false, 'si-d' => 'midiasCompartilhadasOperadoras.edit']) ?>
                                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-trash']) . ' Excluir', 'javascript:void(0);', ['class' => 'btn btn-sm btn-danger btnDeleteMidiaOperadoraEstado', 'id' => $value->id, 'midia_compartilhada_id' => $midiasCompartilhada->id,  'escape' => false, 'si-d' => 'midiasCompartilhadasOperadoras.delete']) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <span><?= $value->titulo ?></span>
                                    <br />
                                    <span><?= $value->linha1 ?></span>
                                </div>

                                <?php if (!empty($value->imagem)) : ?>
                                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'far fa-folder']) . ' Material de divulgação',  '', ['class' => 'btn btn-sm btn-default btnMaterial btnMaterialDivulgacao', 'target' => '_blank', 'escape' => false, 'id' =>  $value->id]) ?>
                                <?php endif; ?>

                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</section>