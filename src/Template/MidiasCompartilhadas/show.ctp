<style>
    .imagens {
        max-width: 10%;
        margin: 0 10px;
        justify-content: center;
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .imagens a {
        justify-content: center;
    }

    .download {
        margin-top: 10px;
    }

    .canva-preview {
        width: 600px;
        height: 600px;
    }

    .download-link {
        margin-top: 10px;
        font-size: 22px;
        color: #FFFFFF;
    }

    .close {
        top: 29px;
        right: 10px;
        font-size: 22px;
    }

    @media(max-width: 880px) {
        .canva-preview {
            width: 100%;
            height: auto;
        }

        .imagens {
            max-width: calc(50% - 20px);
        }
    }
</style>
<div class="modulo">Material de divulgação</div>
<div class="row">
    <div class="col-md-2 col-md-offset-4">
        <?php $ramos = ['SPF' => 'Saúde Pessoa Física', 'SPJ' => 'Saúde Pessoa Jurídica', 'OPF' => 'Odonto Pessoa Física', 'OPJ' => 'Odonto Pessoa Jurídica']; ?>
        <?= $this->Form->input('ramo', ['label' => false, 'empty' => 'SELECIONE RAMO', 'options' => $ramos]) ?>
    </div>
    <div id="operadoras-field" class="col-md-2">
        <?= $this->Form->input('operadora_id', ['label' => false, 'empty' => 'SELECIONE OPERADORA', 'options' => '', 'disabled']) ?>
    </div>
</div>
<div class="spacer-md">&nbsp</div>

<div id="midias-compartilhadas" class="container-fluid flex">
    <?php foreach ($midiasCompartilhadas as $midiasCompartilhada) : ?>
        <div class="imagens" midia="<?= $midiasCompartilhada->id ?>" path="<?= $this->request->webroot . "uploads/midias_compartilhadas/" . $midiasCompartilhada->nome ?>">
            <a href="#pre-visualization-<?= $midiasCompartilhada->id ?>" data-toggle="modal">
                <img src="<?= $this->request->webroot . 'uploads/midias_compartilhadas/' . $midiasCompartilhada->nome ?>" />
            </a>

            <?= $this->Html->link('Download', '#', [
                "class" => "btn btn-sm btn-default download",
                "nome" => $midiasCompartilhada->nome,
                "midia" => $midiasCompartilhada->id
            ]) ?>
        </div>

        <div class="modal fade" id="pre-visualization-<?= $midiasCompartilhada->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="w-100 text-center" style="margin-top: 10px;">
                    <canvas class="canva-preview" id="canvas-<?= $midiasCompartilhada->id ?>" width="810" height="810"></canvas>
                    <div class="col-xs-12 flex" style="justify-content: center;">
                        <?= $this->Html->link('Download', '#', [
                            "class" => "btn btn-sm btn-default download",
                            "nome" => $midiasCompartilhada->nome,
                            "midia" => $midiasCompartilhada->id,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<script type="text/javascript">
    function addNome(context) {
        context.fillText("<?= $user['nome'] ?>", 230, 740);
    }

    function addCelular(context) {
        context.fillText("Celular: <?= $user['celular'] ?>", 230, 757);
    }

    function addWhatsapp(context) {
        context.fillText("Whatsapp: <?= $user['whatsapp'] ?>", 230, 777);
    }

    function addEmail(context) {
        context.fillText("Email: <?= $user['email'] ?>", 230, 797);
    }

    $('.download').click(function() {
        let nome = $(this).attr('nome');
        let midia = $(this).attr('midia');
        this.href = document.getElementById("canvas-" + midia).toDataURL("image/png");
        this.download = nome;
    });
    $(document).ready(function() {
        $('.imagens').each((index, obj) => {
            var midia = $(obj).attr("midia");
            var canvas = document.getElementById("canvas-" + midia);
            var context = canvas.getContext("2d");
            var path = $(obj).attr("path");
            var logo = new Image();
            logo.src = '<?= $this->request->webroot ?>uploads/imagens/usuarios/logos/<?= $user['logo'] ?>';

            var img = new Image();
            img.src = path;
            img.crossOrigin = "anonymous";
            img.onload = function() {
                context.drawImage(
                    this, //imagem a ser utilizada
                    0, //X do inicio do recorte da imagem
                    0, //Y do inicio do recorte da imagem
                    this.naturalWidth, //Até onde vai o corte da imagem em X
                    this.naturalHeight, //Até onde vai o corte da imagem em Y
                    0, //coordenada onde começa o espaço da imagem em X
                    0, //coordenada onde começa o espaço da imagem em Y
                    810,
                    810
                );
                context.drawImage(logo, 40, 730, logo.width * (70 / logo.height), 70)
                context.lineWidth = 1;
                context.fillStyle = "#000";
                context.lineStyle = "#000";
                context.font = "bold 13px sans-serif";
                addNome(context);
                context.font = "13px sans-serif";
                addCelular(context);
                addWhatsapp(context);
                addEmail(context);
            };
            // img.width = 300
        })
    })
    $('#ramo').change(function() {
        var ramo = $(this).val();
        $.get(baseUrl + `midiasCompartilhadas/findOperadoras/${ramo}/filtro`, function(data) {
            $('#operadoras-field').empty();
            $('#operadoras-field').append(data);

            $('.download').click(function() {
                let nome = $(this).attr('nome');
                let midia = $(this).attr('midia');
                this.href = document.getElementById("canvas-" + midia).toDataURL("image/png");
                this.download = nome;
            });
            $('.imagens').each((index, obj) => {
                var midia = $(obj).attr("midia");
                var canvas = document.getElementById("canvas-" + midia);
                var context = canvas.getContext("2d");
                var path = $(obj).attr("path");
                var logo = new Image();
                logo.src = '<?= $this->request->webroot ?>uploads/imagens/usuarios/logos/<?= $user['logo'] ?>';

                var img = new Image();
                img.src = path;
                img.crossOrigin = "anonymous";
                img.onload = function() {
                    context.drawImage(
                        this, //imagem a ser utilizada
                        0, //X do inicio do recorte da imagem
                        0, //Y do inicio do recorte da imagem
                        this.naturalWidth, //Até onde vai o corte da imagem em X
                        this.naturalHeight, //Até onde vai o corte da imagem em Y
                        0, //coordenada onde começa o espaço da imagem em X
                        0, //coordenada onde começa o espaço da imagem em Y
                        810,
                        810
                    );
                    context.drawImage(logo, 40, 730, logo.width * (70 / logo.height), 70)
                    context.lineWidth = 1;
                    context.fillStyle = "#000";
                    context.lineStyle = "#000";
                    context.font = "bold 13px sans-serif";
                    addNome(context);
                    context.font = "13px sans-serif";
                    addCelular(context);
                    addWhatsapp(context);
                    addEmail(context);
                };
            })
        })
    })
</script>