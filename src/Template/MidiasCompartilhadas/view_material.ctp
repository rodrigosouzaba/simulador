<?php echo $this->element('forms/title', ['title' => 'Material de Divulgação']); ?>
<div class="card-body" style="margin-top: 10px;margin-bottom: 10px">
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-6 col-md-12">
            <?= $this->Html->image('/uploads/midias_compartilhadas/' . $midia->imagem) ?>
        </div>
    </div>
</div>