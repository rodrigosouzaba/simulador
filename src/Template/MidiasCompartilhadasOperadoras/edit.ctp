<?php echo $this->Form->create($midiasCompartilhadasOperadoras, ['class' => 'form form-validate', 'role' => 'form']); ?>
<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Editar Midia Compartilhada']); ?>
<?php echo $this->Form->hidden('id'); ?>
<?php echo $this->Form->hidden('imagem', ['id' => 'nome']); ?>
<?php echo $this->Form->hidden('path', ['id' => 'path']); ?>

<div class="card-body">
    <div class="row">
        <div class="col-sm-10 col-lg-10 col-xs-12 col-md-10">
            <div class="form-group">
                <?= $this->Form->input('titulo', ['label' => 'Título', 'required']) ?>
            </div>
        </div>
        <div class="col-sm-2 col-lg-2 col-xs-12 col-md-2">
            <div class="form-group">
                <?php echo $this->Form->input('active', ['type' => 'select', 'required', 'label' => 'Ativo', 'options' => [1 => 'Sim', 0 => 'Não']]); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10 col-lg-10 col-xs-12 col-md-10">
            <div class="form-group">
                <?= $this->Form->input('linha1', ['label' => 'Linha 1']) ?>
            </div>
        </div>
        <!-- <div class="col-sm-5 col-lg-5 col-xs-12 col-md-5">
            <div class="form-group">
                <?= $this->Form->input('linha2', ['label' => 'Linha 2']) ?>
            </div>
        </div> -->
        <div class="col-sm-2 col-lg-2 col-xs-12 col-md-2">
            <div class="form-group">
                <?= $this->Form->input('prioridade', ['required', 'label' => 'Prioridade', 'required']); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-5 col-lg-5 col-xs-12 col-md-5">
            <div class="form-group">
                <?= $this->Form->input('ramo', ['options' => $ramos, 'empty' => 'Selecione Ramo', 'required']) ?>
            </div>
        </div>
        <div class="col-sm-5 col-lg-5 col-xs-12 col-md-5">
            <div class="form-group" id="operadoras-field">
                <?= $this->Form->input('operadora_id', ['id' => 'operadora', 'required', 'options' => $operadoras, 'empty' => 'Selecione Operadora']) ?>
            </div>
        </div>
        <div class="col-sm-2 col-lg-2 col-xs-12 col-md-2">
            <div class="form-group">
                <?= $this->Form->input('validade', ['required', 'type' => 'text', 'label' => 'Validade', 'class' => 'data', 'required']); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
            <div class="form-group">
                <?php echo $this->Form->input('estados', [
                    'label' => 'Estados',
                    'multiple' => 'true',
                    'required',
                    'type' => 'select',
                    'options' => $estados,
                    'default' => $estadosSelecionados,
                    'class' => 'chosen-select'
                ]); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
            &nbsp;
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#menu1">Midia</a></li>
                <li><a data-toggle="tab" href="#menu2">Atualizar Imagem</a></li>
            </ul>

            <div class="tab-content" style="padding: 20px">
                <div id="menu1" class="tab-pane fade in active">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
                            <?php if (!empty($midiasCompartilhadasOperadoras->imagem)) { ?>
                                <?= $this->Html->image('/uploads/midias_compartilhadas/' . $midiasCompartilhadasOperadoras->imagem) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12 fallback">
                            <div id="imagem" class="dropzone"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<?php echo $this->element('forms/buttons') ?>
<?php echo $this->Form->end(); ?>
