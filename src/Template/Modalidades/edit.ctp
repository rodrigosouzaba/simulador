<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $modalidade->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $modalidade->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Modalidades'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Carencias'), ['controller' => 'Carencias', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Carencia'), ['controller' => 'Carencias', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Informacoes'), ['controller' => 'Informacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Informaco'), ['controller' => 'Informacoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Observacoes'), ['controller' => 'Observacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Observaco'), ['controller' => 'Observacoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Opcionais'), ['controller' => 'Opcionais', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Opcionai'), ['controller' => 'Opcionais', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Redes'), ['controller' => 'Redes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Rede'), ['controller' => 'Redes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Reembolsos'), ['controller' => 'Reembolsos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reembolso'), ['controller' => 'Reembolsos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="modalidades form large-9 medium-8 columns content">
    <?= $this->Form->create($modalidade) ?>
    <fieldset>
        <legend><?= __('Edit Modalidade') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('descricao');
            echo $this->Form->input('carencia_id', ['options' => $carencias]);
            echo $this->Form->input('informacao_id', ['options' => $informacoes]);
            echo $this->Form->input('observacao_id', ['options' => $observacoes]);
            echo $this->Form->input('opcional_id', ['options' => $opcionais]);
            echo $this->Form->input('rede_id', ['options' => $redes]);
            echo $this->Form->input('reembolso_id', ['options' => $reembolsos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
