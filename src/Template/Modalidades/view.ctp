<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Modalidade'), ['action' => 'edit', $modalidade->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Modalidade'), ['action' => 'delete', $modalidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $modalidade->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Modalidades'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Modalidade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Carencias'), ['controller' => 'Carencias', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Carencia'), ['controller' => 'Carencias', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Informacoes'), ['controller' => 'Informacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Informaco'), ['controller' => 'Informacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Observacoes'), ['controller' => 'Observacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Observaco'), ['controller' => 'Observacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Opcionais'), ['controller' => 'Opcionais', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Opcionai'), ['controller' => 'Opcionais', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Redes'), ['controller' => 'Redes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rede'), ['controller' => 'Redes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Reembolsos'), ['controller' => 'Reembolsos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reembolso'), ['controller' => 'Reembolsos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="modalidades view large-9 medium-8 columns content">
    <h3><?= h($modalidade->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($modalidade->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($modalidade->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Carencia') ?></th>
            <td><?= $modalidade->has('carencia') ? $this->Html->link($modalidade->carencia->id, ['controller' => 'Carencias', 'action' => 'view', $modalidade->carencia->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Informaco') ?></th>
            <td><?= $modalidade->has('informaco') ? $this->Html->link($modalidade->informaco->id, ['controller' => 'Informacoes', 'action' => 'view', $modalidade->informaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Observaco') ?></th>
            <td><?= $modalidade->has('observaco') ? $this->Html->link($modalidade->observaco->id, ['controller' => 'Observacoes', 'action' => 'view', $modalidade->observaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Opcionai') ?></th>
            <td><?= $modalidade->has('opcionai') ? $this->Html->link($modalidade->opcionai->id, ['controller' => 'Opcionais', 'action' => 'view', $modalidade->opcionai->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Rede') ?></th>
            <td><?= $modalidade->has('rede') ? $this->Html->link($modalidade->rede->id, ['controller' => 'Redes', 'action' => 'view', $modalidade->rede->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Reembolso') ?></th>
            <td><?= $modalidade->has('reembolso') ? $this->Html->link($modalidade->reembolso->id, ['controller' => 'Reembolsos', 'action' => 'view', $modalidade->reembolso->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($modalidade->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Tabelas') ?></h4>
        <?php if (!empty($modalidade->tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Vigencia') ?></th>
                <th><?= __('Faixa1') ?></th>
                <th><?= __('Faixa2') ?></th>
                <th><?= __('Faixa3') ?></th>
                <th><?= __('Faixa4') ?></th>
                <th><?= __('Faixa5') ?></th>
                <th><?= __('Faixa6') ?></th>
                <th><?= __('Faixa7') ?></th>
                <th><?= __('Faixa8') ?></th>
                <th><?= __('Faixa9') ?></th>
                <th><?= __('Faixa10') ?></th>
                <th><?= __('Produto Id') ?></th>
                <th><?= __('Operadora Id') ?></th>
                <th><?= __('Ramo Id') ?></th>
                <th><?= __('Abrangencia Id') ?></th>
                <th><?= __('Tipo Id') ?></th>
                <th><?= __('Modalidade Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($modalidade->tabelas as $tabelas): ?>
            <tr>
                <td><?= h($tabelas->id) ?></td>
                <td><?= h($tabelas->nome) ?></td>
                <td><?= h($tabelas->descricao) ?></td>
                <td><?= h($tabelas->vigencia) ?></td>
                <td><?= h($tabelas->faixa1) ?></td>
                <td><?= h($tabelas->faixa2) ?></td>
                <td><?= h($tabelas->faixa3) ?></td>
                <td><?= h($tabelas->faixa4) ?></td>
                <td><?= h($tabelas->faixa5) ?></td>
                <td><?= h($tabelas->faixa6) ?></td>
                <td><?= h($tabelas->faixa7) ?></td>
                <td><?= h($tabelas->faixa8) ?></td>
                <td><?= h($tabelas->faixa9) ?></td>
                <td><?= h($tabelas->faixa10) ?></td>
                <td><?= h($tabelas->produto_id) ?></td>
                <td><?= h($tabelas->operadora_id) ?></td>
                <td><?= h($tabelas->ramo_id) ?></td>
                <td><?= h($tabelas->abrangencia_id) ?></td>
                <td><?= h($tabelas->tipo_id) ?></td>
                <td><?= h($tabelas->modalidade_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Tabelas', 'action' => 'view', $tabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Tabelas', 'action' => 'edit', $tabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tabelas', 'action' => 'delete', $tabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
