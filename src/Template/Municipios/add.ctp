<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes'), ['controller' => 'PfComercializacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Comercializaco'), ['controller' => 'PfComercializacoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="municipios form large-9 medium-8 columns content">
    <?= $this->Form->create($municipio) ?>
    <fieldset>
        <legend><?= __('Add Municipio') ?></legend>
        <?php
            echo $this->Form->input('codigo');
            echo $this->Form->input('nome');
            echo $this->Form->input('estado_id', ['options' => $estados]);
            echo $this->Form->input('pf_comercializacoes._ids', ['options' => $pfComercializacoes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
