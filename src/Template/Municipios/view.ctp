<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Municipio'), ['action' => 'edit', $municipio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Municipio'), ['action' => 'delete', $municipio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes'), ['controller' => 'PfComercializacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Comercializaco'), ['controller' => 'PfComercializacoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="municipios view large-9 medium-8 columns content">
    <h3><?= h($municipio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($municipio->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Estado') ?></th>
            <td><?= $municipio->has('estado') ? $this->Html->link($municipio->estado->id, ['controller' => 'Estados', 'action' => 'view', $municipio->estado->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($municipio->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Codigo') ?></th>
            <td><?= $this->Number->format($municipio->codigo) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pf Comercializacoes') ?></h4>
        <?php if (!empty($municipio->pf_comercializacoes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($municipio->pf_comercializacoes as $pfComercializacoes): ?>
            <tr>
                <td><?= h($pfComercializacoes->id) ?></td>
                <td><?= h($pfComercializacoes->nome) ?></td>
                <td><?= h($pfComercializacoes->estado_id) ?></td>
                <td><?= h($pfComercializacoes->pf_operadora_id) ?></td>
                <td><?= h($pfComercializacoes->descricao) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfComercializacoes', 'action' => 'view', $pfComercializacoes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfComercializacoes', 'action' => 'edit', $pfComercializacoes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfComercializacoes', 'action' => 'delete', $pfComercializacoes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfComercializacoes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
