<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $municipiosOdontoComercializaco->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $municipiosOdontoComercializaco->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Municipios Odonto Comercializacoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Comercializacoes'), ['controller' => 'OdontoComercializacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Comercializaco'), ['controller' => 'OdontoComercializacoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="municipiosOdontoComercializacoes form large-9 medium-8 columns content">
    <?= $this->Form->create($municipiosOdontoComercializaco) ?>
    <fieldset>
        <legend><?= __('Edit Municipios Odonto Comercializaco') ?></legend>
        <?php
            echo $this->Form->input('municipio_id', ['options' => $municipios]);
            echo $this->Form->input('odonto_comercializacao_id', ['options' => $odontoComercializacoes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
