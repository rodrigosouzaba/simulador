<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Municipios Odonto Comercializaco'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Comercializacoes'), ['controller' => 'OdontoComercializacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Comercializaco'), ['controller' => 'OdontoComercializacoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="municipiosOdontoComercializacoes index large-9 medium-8 columns content">
    <h3><?= __('Municipios Odonto Comercializacoes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('municipio_id') ?></th>
                <th><?= $this->Paginator->sort('odonto_comercializacao_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($municipiosOdontoComercializacoes as $municipiosOdontoComercializaco): ?>
            <tr>
                <td><?= $this->Number->format($municipiosOdontoComercializaco->id) ?></td>
                <td><?= $municipiosOdontoComercializaco->has('municipio') ? $this->Html->link($municipiosOdontoComercializaco->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $municipiosOdontoComercializaco->municipio->id]) : '' ?></td>
                <td><?= $municipiosOdontoComercializaco->has('odonto_comercializaco') ? $this->Html->link($municipiosOdontoComercializaco->odonto_comercializaco->id, ['controller' => 'OdontoComercializacoes', 'action' => 'view', $municipiosOdontoComercializaco->odonto_comercializaco->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $municipiosOdontoComercializaco->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $municipiosOdontoComercializaco->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $municipiosOdontoComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipiosOdontoComercializaco->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
