<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Municipios Odonto Comercializaco'), ['action' => 'edit', $municipiosOdontoComercializaco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Municipios Odonto Comercializaco'), ['action' => 'delete', $municipiosOdontoComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipiosOdontoComercializaco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Municipios Odonto Comercializacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipios Odonto Comercializaco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Comercializacoes'), ['controller' => 'OdontoComercializacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Comercializaco'), ['controller' => 'OdontoComercializacoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="municipiosOdontoComercializacoes view large-9 medium-8 columns content">
    <h3><?= h($municipiosOdontoComercializaco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Municipio') ?></th>
            <td><?= $municipiosOdontoComercializaco->has('municipio') ? $this->Html->link($municipiosOdontoComercializaco->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $municipiosOdontoComercializaco->municipio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Comercializaco') ?></th>
            <td><?= $municipiosOdontoComercializaco->has('odonto_comercializaco') ? $this->Html->link($municipiosOdontoComercializaco->odonto_comercializaco->id, ['controller' => 'OdontoComercializacoes', 'action' => 'view', $municipiosOdontoComercializaco->odonto_comercializaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($municipiosOdontoComercializaco->id) ?></td>
        </tr>
    </table>
</div>
