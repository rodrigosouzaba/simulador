<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Municipios Pf Comercializacoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes'), ['controller' => 'PfComercializacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Comercializaco'), ['controller' => 'PfComercializacoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="municipiosPfComercializacoes form large-9 medium-8 columns content">
    <?= $this->Form->create($municipiosPfComercializaco) ?>
    <fieldset>
        <legend><?= __('Add Municipios Pf Comercializaco') ?></legend>
        <?php
            echo $this->Form->input('pf_comercializacao_id', ['options' => $pfComercializacoes]);
            echo $this->Form->input('municipio_id', ['options' => $municipios]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
