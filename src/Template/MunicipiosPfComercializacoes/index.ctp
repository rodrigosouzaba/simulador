<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Municipios Pf Comercializaco'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes'), ['controller' => 'PfComercializacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Comercializaco'), ['controller' => 'PfComercializacoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="municipiosPfComercializacoes index large-9 medium-8 columns content">
    <h3><?= __('Municipios Pf Comercializacoes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_comercializacao_id') ?></th>
                <th><?= $this->Paginator->sort('municipio_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($municipiosPfComercializacoes as $municipiosPfComercializaco): ?>
            <tr>
                <td><?= $this->Number->format($municipiosPfComercializaco->id) ?></td>
                <td><?= $municipiosPfComercializaco->has('pf_comercializaco') ? $this->Html->link($municipiosPfComercializaco->pf_comercializaco->id, ['controller' => 'PfComercializacoes', 'action' => 'view', $municipiosPfComercializaco->pf_comercializaco->id]) : '' ?></td>
                <td><?= $municipiosPfComercializaco->has('municipio') ? $this->Html->link($municipiosPfComercializaco->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $municipiosPfComercializaco->municipio->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $municipiosPfComercializaco->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $municipiosPfComercializaco->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $municipiosPfComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipiosPfComercializaco->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
