<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Municipios Pf Comercializaco'), ['action' => 'edit', $municipiosPfComercializaco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Municipios Pf Comercializaco'), ['action' => 'delete', $municipiosPfComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipiosPfComercializaco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Municipios Pf Comercializacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipios Pf Comercializaco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes'), ['controller' => 'PfComercializacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Comercializaco'), ['controller' => 'PfComercializacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="municipiosPfComercializacoes view large-9 medium-8 columns content">
    <h3><?= h($municipiosPfComercializaco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Comercializaco') ?></th>
            <td><?= $municipiosPfComercializaco->has('pf_comercializaco') ? $this->Html->link($municipiosPfComercializaco->pf_comercializaco->id, ['controller' => 'PfComercializacoes', 'action' => 'view', $municipiosPfComercializaco->pf_comercializaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Municipio') ?></th>
            <td><?= $municipiosPfComercializaco->has('municipio') ? $this->Html->link($municipiosPfComercializaco->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $municipiosPfComercializaco->municipio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($municipiosPfComercializaco->id) ?></td>
        </tr>
    </table>
</div>
