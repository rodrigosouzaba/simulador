<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Municipios Regioes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="municipiosRegioes form large-9 medium-8 columns content">
    <?= $this->Form->create($municipiosRegio) ?>
    <fieldset>
        <legend><?= __('Add Municipios Regio') ?></legend>
        <?php
            echo $this->Form->input('municipio_id', ['options' => $municipios]);
            echo $this->Form->input('regiao_id', ['options' => $regioes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
