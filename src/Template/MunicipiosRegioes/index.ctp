<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Municipios Regio'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="municipiosRegioes index large-9 medium-8 columns content">
    <h3><?= __('Municipios Regioes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('municipio_id') ?></th>
                <th><?= $this->Paginator->sort('regiao_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($municipiosRegioes as $municipiosRegio): ?>
            <tr>
                <td><?= $this->Number->format($municipiosRegio->id) ?></td>
                <td><?= $municipiosRegio->has('municipio') ? $this->Html->link($municipiosRegio->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $municipiosRegio->municipio->id]) : '' ?></td>
                <td><?= $municipiosRegio->has('regio') ? $this->Html->link($municipiosRegio->regio->id, ['controller' => 'Regioes', 'action' => 'view', $municipiosRegio->regio->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $municipiosRegio->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $municipiosRegio->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $municipiosRegio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipiosRegio->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
