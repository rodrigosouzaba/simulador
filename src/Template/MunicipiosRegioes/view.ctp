<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Municipios Regio'), ['action' => 'edit', $municipiosRegio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Municipios Regio'), ['action' => 'delete', $municipiosRegio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipiosRegio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Municipios Regioes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipios Regio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="municipiosRegioes view large-9 medium-8 columns content">
    <h3><?= h($municipiosRegio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Municipio') ?></th>
            <td><?= $municipiosRegio->has('municipio') ? $this->Html->link($municipiosRegio->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $municipiosRegio->municipio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Regio') ?></th>
            <td><?= $municipiosRegio->has('regio') ? $this->Html->link($municipiosRegio->regio->id, ['controller' => 'Regioes', 'action' => 'view', $municipiosRegio->regio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($municipiosRegio->id) ?></td>
        </tr>
    </table>
</div>
