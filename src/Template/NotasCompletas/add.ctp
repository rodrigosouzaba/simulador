<?= $this->Form->create($notasCompleta, ['class' => "form-inline", 'type' => 'file', 'id' => 'addNF']); ?>
<div class="col-md-6 fonteReduzida">
    <?= $this->Form->input('arquivo', ['type' => 'file', 'accept' => '.xml']); ?>
</div>
<div class="col-md-6 fonteReduzida">
    <?= $this->Form->hidden('user_id', ['value' => $sessao['id']]); ?>
</div>
<div class="col-md-12 fonteReduzida centralizada">
    <div class="spacer-lg"></div>
    <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-save', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => 'btn btn-md btn-primary', 'id' => 'salvar-nota']) ?>
</div>
<?= $this->Form->end() ?>
<script type="text/javascript">
    //Pegando view do Add e jogando no Modal
    $("#salvar-nota").click(function(){
        $("#close-add-nota").click();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot?>notas-completas/add",
            data: $("#addNF").serialize(),
            beforeSend: function () {
                //Aqui adicionas o loader
                $("#loader").click();
            },     

            complete: function (data) {
                $("#fechar").click();
                    //$("#formAdicionar").empty();
                    //$("#formAdicionar").append(data);

                }
            });
    });
</script>