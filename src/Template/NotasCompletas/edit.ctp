<div class="col-md-12">
    <?= $this->Form->create($notasCompleta) ?>
    <fieldset>
        <h3 class="fonte-reduzida centralizada"><?= ('Editar Nota Completa') ?></h3>
        <?php
            echo $this->Form->input('caminho');
            echo $this->Form->input('nome_arquivo');
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar'),['class' => 'btn btn-md btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
