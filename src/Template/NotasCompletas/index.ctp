<div class="col-md-2 fonteReduzida">
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . 'Adicionar NF', "#", ['class' => 'btn btn-sm btn-primary', 'role' => 'button', 'escape' => false, 'id' => 'modal', "data-toggle" => "modal", "data-target" => "#adicionar"]);
    ?>
</div>

<div id="listaProdutos">

    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= 'ID' ?></th>
                <th><?= 'Nome do Arquivo' ?></th>
                <th><?= 'Caminho' ?></th>
                <th><?= 'Created' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($notasCompletas as $notasCompleta) : ?>
                <tr>
                    <td><?= $this->Number->format($notasCompleta->id) ?></td>
                    <td><?= h($notasCompleta->nome_arquivo) ?></td>
                    <td><?= h($notasCompleta->caminho) ?></td>
                    <td><?= h($notasCompleta->created) ?></td>
                    <td class="actions">
                        <!--
                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-edit', 'aria-hidden' => 'true']), ['action' => 'edit', $notasCompleta->id], ['class' => 'btn btn-xs btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar notasCompleta']) ?>
                                <?= $this->Form->postLink('', ['action' => 'delete', $notasCompleta->id], ['confirm' => __('Confirma exclusão do notasCompleta?'), 'title' => __('Deletar'), 'class' => 'btn btn-xs btn-danger glyphicon glyphicon-trash p']); ?>
                            -->
                        <?= $this->Form->postLink('', ['action' => 'delete', $notasCompleta->id], ['title' => 'Excluir', 'class' => 'btn btn-xs btn-danger glyphicon glyphicon-trash delete', 'value' => $notasCompleta->id]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="clearfix">&nbsp;</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="adicionar">
    <div class="modal-dialog" role="document" style="z-index: 9999;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-add-nota"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adicionar Nota</h4>
            </div>
            <div class="modal-body">
                <div id="loading"></div>
                <div id="formAdicionar"></div>
            </div>
            <div class="modal-footer" style="border-top:0px;">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?= $this->element("processando") ?>

<script type="text/javascript">
    //Pegando view do Add e jogando no Modal
    $("#modal").click(function() {
        $.ajax({
            type: "GET",
            url: "<?php echo $this->request->webroot ?>notas-completas/add",
            success: function(data) {
                $("#formAdicionar").empty();
                $("#formAdicionar").append(data);
                //  $(".close").click();
            }
        });
    });
</script>