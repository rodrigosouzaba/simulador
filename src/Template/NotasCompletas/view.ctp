
<div class="col-md-12">
    <h3><?= h($notasCompleta->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Caminho') ?></th>
            <td><?= h($notasCompleta->caminho) ?></td>
        </tr>
        <tr>
            <th><?= __('Nome Arquivo') ?></th>
            <td><?= h($notasCompleta->nome_arquivo) ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $notasCompleta->has('user') ? $this->Html->link($notasCompleta->user->id, ['controller' => 'Users', 'action' => 'view', $notasCompleta->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($notasCompleta->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($notasCompleta->created) ?></td>
        </tr>
    </table>
</div>
