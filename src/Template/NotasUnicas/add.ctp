<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Notas Unicas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Notas Completas'), ['controller' => 'NotasCompletas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Notas Completa'), ['controller' => 'NotasCompletas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="notasUnicas form large-9 medium-8 columns content">
    <?= $this->Form->create($notasUnica) ?>
    <fieldset>
        <legend><?= __('Add Notas Unica') ?></legend>
        <?php
            echo $this->Form->input('nota_completa_id', ['options' => $notasCompletas, 'empty' => true]);
            echo $this->Form->input('caminho');
            echo $this->Form->input('nome_arquivo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
