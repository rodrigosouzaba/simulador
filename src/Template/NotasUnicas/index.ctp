<div class="col-md-12">
    <h3 class="centralizada"><?= __('Notas Unicas') ?></h3>
    <table cellpadding="0" cellspacing="0" class="table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('created',['label' => 'Data']) ?></th>
                <th><?= $this->Paginator->sort('nota_completa_id') ?></th>
                <th><?= $this->Paginator->sort('nome_arquivo') ?></th>
                <th><?= $this->Paginator->sort('caminho') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($notasUnicas as $notasUnica): ?>
                <tr>
                 <td><?= h($notasUnica->created) ?></td>
                 <td><?= $notasUnica->notas_completa->nome_arquivo ?></td>
                 <td><?= h($notasUnica->nome_arquivo) ?></td>
                 <td><?= h($notasUnica->caminho) ?></td>
             </tr>
         <?php endforeach; ?>
     </tbody>
 </table>
 <div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
</div>
