<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Notas Unica'), ['action' => 'edit', $notasUnica->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Notas Unica'), ['action' => 'delete', $notasUnica->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notasUnica->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Notas Unicas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Notas Unica'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Notas Completas'), ['controller' => 'NotasCompletas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Notas Completa'), ['controller' => 'NotasCompletas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="notasUnicas view large-9 medium-8 columns content">
    <h3><?= h($notasUnica->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Notas Completa') ?></th>
            <td><?= $notasUnica->has('notas_completa') ? $this->Html->link($notasUnica->notas_completa->id, ['controller' => 'NotasCompletas', 'action' => 'view', $notasUnica->notas_completa->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($notasUnica->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= $this->Number->format($notasUnica->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Caminho') ?></th>
            <td><?= $this->Number->format($notasUnica->caminho) ?></td>
        </tr>
        <tr>
            <th><?= __('Nome Arquivo') ?></th>
            <td><?= $this->Number->format($notasUnica->nome_arquivo) ?></td>
        </tr>
    </table>
</div>
