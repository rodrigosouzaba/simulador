<?php $this->assign('title', $title); ?>
<?= $this->Form->create($observaco, ['class' => 'fonteReduzida']) ?>
<div class="col-md-12">
    <?= $this->Form->input('nome') ?>
</div>
<div class="col-md-6">
    <?= $this->Form->input('nova', ['options' => [1 => 'Sim', 0 => 'Não'], 'label' => 'Nova', 'empty' => 'Nova']) ?>
</div>
<div id="field-operadoras" class="col-md-6">
    <?= $this->Form->input('operadora_id', ['options' => '', 'label' => 'Operadora', 'empty' => 'SELECIONE OPERADORA', 'disabled' => 'disabled']) ?>
</div>
<div class="col-md-12">

    <?= $this->Form->input('descricao', ['label' => 'Descrição']) ?>
</div>
<?= $this->element('botoesAdd') ?>
<?= $this->Form->end() ?>
<script>
    $("#nova").change(function() {
        var action = '<?= $this->request->getParam('controller') ?>'.charAt(0).toLowerCase() + '<?= $this->request->getParam('controller') ?>'.slice(1);
        $.get(baseUrl + action + '/filtroNova/' + $(this).val(), function(data) {
            $("#field-operadoras").empty();
            $("#field-operadoras").append(data);
        })
    })
</script>