<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $observaco->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $observaco->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Observacoes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="observacoes form large-9 medium-8 columns content">
    <?= $this->Form->create($observaco) ?>
    <fieldset>
        <legend><?= __('Edit Observaco') ?></legend>
        <?php
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
