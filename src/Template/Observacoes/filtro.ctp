<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Observação', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'observacoes.add']);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['value' => $estado, 'options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?php if (empty($operadoras)) : ?>
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
    <?php else : ?>
        <?= $this->Form->input('operadora_id', ['value' =>  $operadora, 'options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
    <?php endif; ?>
</div>

<?= $this->Form->create('selecaoTabelas', ['id' => 'selecaoTabelas']); ?>
<div class="col-xs-12">
    <?php if (isset($observacoes)) {
        if (!empty($observacoes)) { ?>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?= 'Nome' ?></th>
                        <th><?= 'Descrição' ?></th>
                        <th><?= 'Operadora' ?></th>
                        <th class="actions" style="width: 12%;"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($observacoes as $observacao) : ?>
                        <tr>

                            <td><?= h($observacao->nome) ?></td>
                            <td><?= h($observacao->descricao) ?></td>
                            <td>
                                <?php
                                //                        debug($observacao);die();
                                if (isset($observacao->operadora->imagen->caminho)) {
                                    echo $this->Html->image("../" . $observacao->operadora->imagen->caminho . "/" . $observacao->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $observacao->operadora->detalhe;
                                } else {
                                    echo $observacao->operadora->nome . " - " . $observacao->operadora->detalhe;
                                }
                                ?>
                            </td>


                            <td class="actions">
                                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $observacao->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar observacao', 'sid' => 'observacoes.edit']) ?>
                                <?php echo $this->Form->postLink('', ['action' => 'delete', $observacao->id], ['confirm' => __('Confirma exclusão do observacao?', $observacao->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'observacoes.delete']); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php
        } else {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-md-12 text-danger centralizada"> NENHUM PRODUTO ENCONTRADO</div>
    <?php
        }
    }
    ?>


    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = baseUrl + "/observacoes/filtro/" + $(this).val();
    });

    $("#operadora-id").change(function() {
        window.location.href = baseUrl + "/observacoes/filtro/" + $("#estados").val() + "/" + $(this).val();
    });
</script>