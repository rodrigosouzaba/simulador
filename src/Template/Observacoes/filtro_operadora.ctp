<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('id') ?></th>-->
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Detalhes' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions" style="width: 12%;"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($observacoes as $obs): ?>
            <tr>
                <td><?= $obs->nome ?></td>
                <td ><?= $obs->descricao ?></td>
                <td>
                <?php
//                        debug($produto);die();
                        if (isset($obs->operadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $obs->operadora->imagen->caminho . "/" . $obs->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $obs->operadora->detalhe;
                        } else {
                            echo $obs->operadora->nome . " - " . $obs->operadora->detalhe;
                        }
                        ?></td>
                <td class="actions">
                    <?php
                        echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $observacao->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar observacao', 'sid' => 'observacoes.edit']);
                        echo $this->Form->postLink('', ['action' => 'delete', $observacao->id], ['confirm' => __('Confirma exclusão do observacao?', $observacao->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'observacoes.delete']);
                    ?>     
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
