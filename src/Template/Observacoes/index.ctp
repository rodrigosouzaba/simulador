<div class="col-md-3 fonteReduzida">
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Observação', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'observacoes.add']) ?>
</div>

<?= $this->element('filtro_interno') ?>

<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('descricao', ['label' => 'Descrição']) ?></th>
            <th><?= $this->Paginator->sort('operadora_id', ['label' => 'Operadora']) ?></th>
            <th class="actions" style="width: 12%;"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($observacoes as $observacao) : ?>
            <tr>
                <td><?= h($observacao->nome) ?></td>
                <td><?= h($observacao->descricao) ?></td>
                <td>
                    <?php if (isset($observacao->operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $observacao->operadora->imagen->caminho . "/" . $observacao->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $observacao->operadora->detalhe;
                    } else {
                        echo $observacao->operadora->nome . " - " . $observacao->operadora->detalhe;
                    }
                    ?>
                </td>
                <td class="actions">
                    <?php
                    echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $observacao->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar observacao', 'sid' => 'observacoes.edit']);
                    echo $this->Form->postLink('', ['action' => 'delete', $observacao->id], ['confirm' => __('Confirma exclusão do observacao?', $observacao->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'observacoes.delete']);
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>