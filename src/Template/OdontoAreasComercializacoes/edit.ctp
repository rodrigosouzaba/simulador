<style>
    .estados {
        border: 1px solid #ccc;
        padding: 0px;
    }

    .select-estado {
        display: flex;
        align-items: center;
        padding: 0 15px;
    }

    .select-estado:hover {
        background-color: #DDD;
    }

    .select-estado span {
        padding: 5px 0;
    }

    .active {
        background: #08c;
        color: #FFF;
    }
</style>
<div class="container fonteReduzida">
    <legend>Nova Area de Comercialização</legend>
    <?= $this->Form->create($odontoAreasComercializaco) ?>
    <fieldset>
        <div class="col-xs-6">
            <?= $this->Form->control('nome') ?>
        </div>
        <div class="col-xs-6">
            <?= $this->Form->control('odonto_operadora_id', ['empty' => 'SELECIONE OPERADORA', 'options' => $odontoOperadoras]) ?>
        </div>
        <div class="col-xs-12">
            <?= $this->Form->control('descricao') ?>
        </div>

        <!-- METROPOLES -->
        <div class="col-md-6" style="margin-top: 1rem;">
            <label>Estados</label>
            <div class="estados">
                <?php foreach ($estados as $estado) :
                    if (!empty($estado->metropoles)) : ?>
                        <div class="select-estado select-estado-metropoles" id="select-estado-<?= $estado->id ?>" estado-id=<?= $estado->id ?>>
                            <span><?= $estado->nome ?></span>
                        </div>
                <?php endif;
                endforeach; ?>
            </div>
        </div>
        <div class="col-md-6 tab-content" style="margin-top: 1rem;">
            <label>Metropoles</label>
            <?php foreach ($estados as $estado) : ?>
                <div class="metropoles tab-pane" id="tab-metropoles-<?= $estado->id ?>" estado="<?= $estado->id ?>">
                    <div class="flex" style="justify-content: space-between;">
                        <a href='#' id='select-all-<?= $estado->id ?>'>Seleiconar Tudo</a>
                        <a href='#' id='deselect-all-<?= $estado->id ?>'>Limpar</a>
                    </div>
                    <select multiple='multiple' id="multiple-estado-metropoles-<?= $estado->id ?>" class="multiselect" estado-id="<?= $estado->id ?>" name='<?= "metropoles[_ids][]" ?>'>
                        <?php foreach ($estado->metropoles as $metropole) : ?>
                            <option value="<?= $metropole->id; ?>"><?= $metropole->nome; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <script>
                    $(document).ready(function() {
                        var estado = $("#multiple-estado-metropoles-<?= $estado->id ?>").attr("estado-id");

                        $("#multiple-estado-metropoles-<?= $estado->id ?>").multiSelect({
                            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                            afterInit: function(ms) {
                                var that = this,
                                    $selectableSearch = that.$selectableUl.prev(),
                                    $selectionSearch = that.$selectionUl.prev(),
                                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                                    .on('keydown', function(e) {
                                        if (e.which === 40) {
                                            that.$selectableUl.focus();
                                            return false;
                                        }
                                    });

                                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                                    .on('keydown', function(e) {
                                        if (e.which == 40) {
                                            that.$selectionUl.focus();
                                            return false;
                                        }
                                    });
                            },
                            afterSelect: function(values) {
                                this.qs1.cache();
                                this.qs2.cache();
                                var selecionados = $('#multiple-estado-metropoles-<?= $estado->id ?> option:selected').length;
                                $("#select-estado-" + estado).addClass("active");
                            },
                            afterDeselect: function(values) {
                                this.qs1.cache();
                                this.qs2.cache();
                                var selecionados = $('#multiple-estado-metropoles-<?= $estado->id ?> option:selected').length;
                                if (selecionados == 0) {
                                    $("#select-estado-" + estado).removeClass("active");
                                }
                            }
                        })
                    })
                    $("#select-all-<?= $estado->id ?>").click(function() {
                        $("#multiple-estado-metropoles-<?= $estado->id ?>").multiSelect('select_all')
                    });
                    $("#deselect-all-<?= $estado->id ?>").click(function() {
                        $("#multiple-estado-metropoles-<?= $estado->id ?>").multiSelect('deselect_all')
                    });
                </script>
            <?php endforeach; ?>
        </div>
        <div class="clearfix">&nbsp;</div>

        <!-- ESTADO -->
        <div class="col-md-6" style="margin-top: 1rem;">
            <label>Estados</label>
            <div class="estados">
                <?php foreach ($estados as $estado) : ?>
                    <div class="select-estado <?= in_array($estado->id, $locais_salvos) ? "active" : "" ?>" id="select-estado-<?= $estado->id ?>" estado-id=<?= $estado->id ?>>
                        <span><?= $estado->nome ?></span>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="col-md-6 tab-content" style="margin-top: 1rem;">
            <label>Municipios</label>
            <?php foreach ($estados as $estado) : ?>
                <div class="municipios tab-pane" id="tab-<?= $estado->id ?>" estado="<?= $estado->id ?>">
                    <div class="flex" style="justify-content: space-between;">
                        <a href='#' id='select-all-<?= $estado->id ?>'>Seleiconar Tudo</a>
                        <a href='#' id='deselect-all-<?= $estado->id ?>'>Limpar</a>
                    </div>

                    <select multiple='multiple' id="multiple-estado-<?= $estado->id ?>" class="multiselect" estado-id="<?= $estado->id ?>" name='<?= "municipios[]" ?>'>
                        <?php foreach ($estado->municipios as $municipio) : ?>
                            <option value="<?= $municipio->id; ?>" <?= array_key_exists($municipio->id, $locais_salvos) ? "selected='selected'" : "" ?>><?= $municipio->nome; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <script>
                    $(document).ready(function() {
                        var estado = $("#multiple-estado-<?= $estado->id ?>").attr("estado-id");

                        $("#multiple-estado-<?= $estado->id ?>").multiSelect({
                            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                            afterInit: function(ms) {
                                var that = this,
                                    $selectableSearch = that.$selectableUl.prev(),
                                    $selectionSearch = that.$selectionUl.prev(),
                                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                                    .on('keydown', function(e) {
                                        if (e.which === 40) {
                                            that.$selectableUl.focus();
                                            return false;
                                        }
                                    });

                                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                                    .on('keydown', function(e) {
                                        if (e.which == 40) {
                                            that.$selectionUl.focus();
                                            return false;
                                        }
                                    });
                            },
                            afterSelect: function(values) {
                                this.qs1.cache();
                                this.qs2.cache();
                                var selecionados = $('#multiple-estado-<?= $estado->id ?> option:selected').length;
                                $("#select-estado-" + estado).addClass("active");
                            },
                            afterDeselect: function(values) {
                                this.qs1.cache();
                                this.qs2.cache();
                                var selecionados = $('#multiple-estado-<?= $estado->id ?> option:selected').length;
                                if (selecionados == 0) {
                                    $("#select-estado-" + estado).removeClass("active");
                                }
                            }
                        })
                    })
                    $("#select-all-<?= $estado->id ?>").click(function() {
                        $("#multiple-estado-<?= $estado->id ?>").multiSelect('select_all')
                    });
                    $("#deselect-all-<?= $estado->id ?>").click(function() {
                        $("#multiple-estado-<?= $estado->id ?>").multiSelect('deselect_all')
                    });
                </script>
            <?php endforeach; ?>
        </div>


    </fieldset>
    <?= $this->element('botoesAdd'); ?>
    <?= $this->Form->end() ?>
</div>
<script>
    $(".select-estado").click(function() {
        var estado = $(this).attr('estado-id');
        $('.tab-pane').hide();
        $('#tab-' + estado).show();
    });
</script>