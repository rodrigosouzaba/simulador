<div class="col-xs-12">
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Area Comercialização', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addAbrangencia']); ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('descricao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('odonto_operadora_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($odontoAreasComercializacoes as $odontoAreasComercializaco) : ?>
                <tr>
                    <td><?= h($odontoAreasComercializaco->nome) ?></td>
                    <td><?= h($odontoAreasComercializaco->descricao) ?></td>
                    <td><?= $odontoAreasComercializaco->has('odonto_operadora') ? $this->Html->link($odontoAreasComercializaco->odonto_operadora->nome, ['controller' => 'OdontoOperadoras', 'action' => 'view', $odontoAreasComercializaco->odonto_operadora->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $odontoAreasComercializaco->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $odontoAreasComercializaco->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $odontoAreasComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoAreasComercializaco->id)]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>