<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OdontoAreasComercializaco $odontoAreasComercializaco
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Areas Comercializaco'), ['action' => 'edit', $odontoAreasComercializaco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Areas Comercializaco'), ['action' => 'delete', $odontoAreasComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoAreasComercializaco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Areas Comercializacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Areas Comercializaco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Operadoras'), ['controller' => 'OdontoOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Operadora'), ['controller' => 'OdontoOperadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoAreasComercializacoes view large-9 medium-8 columns content">
    <h3><?= h($odontoAreasComercializaco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Descricao') ?></th>
            <td><?= h($odontoAreasComercializaco->descricao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($odontoAreasComercializaco->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Odonto Operadora') ?></th>
            <td><?= $odontoAreasComercializaco->has('odonto_operadora') ? $this->Html->link($odontoAreasComercializaco->odonto_operadora->id, ['controller' => 'OdontoOperadoras', 'action' => 'view', $odontoAreasComercializaco->odonto_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoAreasComercializaco->id) ?></td>
        </tr>
    </table>
</div>
