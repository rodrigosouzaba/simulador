<?= $this->Form->create($odontoAtendimento, ['class' => "form-inline"]) ?>

<div class="col-md-12 fonteReduzida">
    <?php
    echo $this->Form->input('nome');
    echo $this->Form->input('descricao', ['label' => 'Observação']);
    ?>
</div>
<?= $this->element('botoesAdd'); ?>
<?= $this->Form->end() ?>
