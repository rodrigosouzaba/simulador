<?php
foreach ($estados as $id => $estado) :
    if ($sessao['estado_id'] == $id) : ?>
        <script type="text/javascript">
            var estado_id = '<?= $sessao["estado_id"] ?>';
            $("estado_id").val(estado_id);
        </script>
<?php endif;
endforeach;
if ($tipo_pessoa == 'pf' || $tipo_pessoa == 'PF') {
    $tipo_pessoa = 'PF';
    $titulo = 'Pessoa Física';
}
if ($tipo_pessoa == 'pj' || $tipo_pessoa == 'PJ') {
    $tipo_pessoa = 'PJ';
    $titulo = 'PME';
}
?>
<div class="clearfix">&nbsp;</div>
<div class="col-xs-12" id="pagina">
    <!--<h4 style="padding: 10px 0" class="centralizada">Novo Cálculo Odonto <?= $titulo ?> </h4>-->
    <div class="clearfix">&nbsp;</div>
    <?= $this->Form->create($odontoCalculo, ['id' => 'odonto-calculo', 'url' => '/odontoCalculos/cotar']) ?>

    <?= $this->Form->hidden('tipo_pessoa', ['value' => $tipo_pessoa]) ?>
    <?= $this->Form->hidden('user_id', ['value' => $sessao['id']]) ?>

    <div class="col-xs-12 ">

        <div class="tituloField centralizada">
            Dados do Cliente
        </div>
        <div class="col-xs-12 col-md-3">
            <?= $this->Form->input('estado_id', ['label' => false, 'empty' => 'ESTADO DE COMERCIALIZAÇÃO', 'options' => $estados, 'required' => true]); ?>
        </div>

        <div class="col-xs-12 col-md-3">
            <?= $this->Form->input('nome', ['label' => false, "placeholder" => "Nome (opcional)"]); ?>
        </div>

        <div class="col-xs-12 col-md-3">
            <?= $this->Form->input('email', ['label' => false, "placeholder" => "Email (opcional)"]); ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?= $this->Form->input('telefone', ['label' => false, "placeholder" => "Telefone (opcional)"]); ?>
        </div>
    </div>
    <div class="col-xs-12 ">
        <div class="clearfix">&nbsp;</div>

        <div class="tituloField centralizada" id="tituloVidasOdonto">
            <?php
            if ($tipo_pessoa === "PF") {
                echo "Máximo de 5 dependentes por titular";
            } else {
                echo "Quantidade de vidas";
            }
            ?>
        </div>
        <?php if ($tipo_pessoa === "PF") { ?>

            <!--<div class="col-xs-12 col-md-2">-->
            <?= $this->Form->input('titularesPF', ['type' => 'hidden', 'label' => 'Titular', 'value' => '1', 'style' => 'margin-bottom: 2px !important']); ?>
            <div id="respostatitularpf" style="color: red; font-weight: bold; font-size: 70%"></div>
            <!--</div>-->

            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="col-md-6" style="padding-left: 0">
                    <?= $this->Form->input("titular", ["value" => "1 Titular", "label" => false, "disabled" => "disabled"]); ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->input('dependentesPF', ['type' => 'number', 'label' => false, "placeholder" => "Quantos dependentes?", 'style' => 'margin-bottom: 2px !important', 'min' => 0]); ?>
                </div>
                <div id="respostaPF" style="color: red; font-weight: bold; font-size: 70%"></div>
            </div>


        <?php } ?>

        <?php if ($tipo_pessoa === "PJ") { ?>
            <div class="col-md-4 col-xs-12 col-md-offset-4">
                <div class="col-xs-12 col-md-6" style="padding-left: 0 ">
                    <?= $this->Form->input('titularesPJ', ['min' => 1, 'type' => 'number', "label" => false, 'placeholder' => 'Titular(es)', 'required' => true]); ?>
                </div>

                <div class="col-xs-12 col-md-6" style="padding-right: 0 !important;">
                    <?= $this->Form->input('dependentesPJ', ['min' => 0, 'type' => 'number', "label" => false, 'placeholder' => 'Dependente(s)', 'style' => 'margin-bottom: 2px !important']); ?>
                    <div id="resposta" style="color: red; font-weight: bold; font-size: 70%"></div>
                </div>
            </div>


        <?php } ?>

        <div class="col-xs-12 col-md-4">
            <?= $this->Form->input('quantidade_vidas', ['label' => 'Total de vidas', 'class' => 'disabled', 'id' => 'quantidade_vidas', 'style' => 'margin-bottom: 2px !important', 'type' => 'hidden']); ?>
            <div id="respostaTotal" style="color: red; font-weight: bold; font-size: 70%"></div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>


    <div class="col-md-12 centralizada">
        <!-- <?=
                $this->Html->link($this->Html->tag('span', '', [
                    'class' => 'fa fa-file-text-o',
                    'aria-hidden' => 'true'
                ]) . ' Calcular', '#', [
                    'class' => 'btn btn-md btn-primary',
                    'role' => 'button',
                    'escape' => false,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                    'id' => 'calcular'
                ])
                ?>
-->

        <?=
        $this->Form->button('Simular', [
            'class' => 'btn btn-md btn-primary',
            'role' => 'button',
            'escape' => false,
            'data-toggle' => 'tooltip',
            'data-placement' => 'top',
            'title' => 'Gerar Cálculo', 'id' => 'enviar'
        ])
        ?>
        <?= $this->Form->end() ?>
    </div>

</div>

<?= $this->element("processando"); ?>

<script type="text/javascript">
    $("#titularespf").blur(function() {
        var titulares = parseInt($(this).val());

        if (titulares > 1) {
            $(this).val("1");
            $("#respostatitularpf").empty();
            $("#respostatitularpf").append('Máximo de titulares é 1 para planos individuais');
        } else {
            $("#respostatitularpf").empty();
        }
    });

    $("#calcular").click(function() {

        $.ajax({
            type: "post",
            data: $("#odonto-calculo").serialize(),
            url: "<?php echo $this->request->webroot ?>odonto-calculos/encontrarTabelas/",
            beforeSend: function() {
                $('#loader').on('click');
            },
            success: function(data) {
                $("#pagina").empty();
                $("#pagina").append(data);
                $('#close').trigger('click');
            }
        });
    });

    $(".botaoSalvar").hide();
</script>