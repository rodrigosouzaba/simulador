<style type="text/css">
    .jumbotron {
        background-color: #eee;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 10px;
        line-height: 1.5;
        width: 100%;
        /*width: 97%;*/
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }


    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
        font-size: 60%;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .totalTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
        font-weight: bold;
    }

    .fonteReduzida {
        font-size: 10px;
        text-align: justify !important;
        text-align: justify;
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        line-height: 1.3;
        margin-bottom: 0;
        cursor: default;
        display: block;
        font-style: normal;
        font-variant-caps: normal;
        vertical-align: baseline;
        white-space: normal;

    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .nobreak {
        page-break-inside: avoid;
    }

    .info {
        color: #a9a9a9;
        font-weight: lighter;
        font-style: italic;
        text-align: center;
        width: 100%;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .verde {
        color: #128c7e;
    }

    .pagina {
        padding: 20px;
        margin-top: 85px;
    }

    @media(max-width: 767px) {
        .pagina {
            margin-top: 106px;
        }
    }

    @media(min-width: 768px) and (max-width: 895px) {
        .pagina {
            margin-top: 191px;
        }
    }

    @media(min-width: 896px) and (max-width: 1218px) {
        .pagina {
            margin-top: 141px;
        }
    }
</style>
<?php
$texto = "Visualize sua cotação aqui: https://corretorparceiro.com.br/app/odonto-calculos/cotacao/";
$linkWhats = 'https://wa.me?text=' . $texto . $filtro->id;
?>
<?= $this->Form->create('dadosPDF', ['id' => 'dadosPDF', 'url' => ['action' => 'pdfCotacao', $filtro->id]]); ?>
<?= $this->Form->input("calculo_id", ["type" => "hidden", "value" => $cotacao['id']]); ?>
<?= $this->Form->input("observacao", ["type" => "hidden", "value" => $cotacao['observaco']]); ?>
<?= $this->Form->input("rede", ["type" => "hidden", "value" => $cotacao['rede']]); ?>
<?= $this->Form->input("reembolso", ["type" => "hidden", "value" => $cotacao['reembolso']]); ?>
<?= $this->Form->input("carencia", ["type" => "hidden", "value" => $cotacao['carencia']]); ?>
<?= $this->Form->input("documento", ["type" => "hidden", "value" => $cotacao['documento']]); ?>
<?php
foreach ($findTabelas as $key => $dadoPDF) {
    if ($key != "simulacao_id" && $key != "pf_atendimento_id" && $key != "pf_acomodacao_id" && $key != "tipo_produto_id" && $key != "coparticipacao" && $key != "filtroreembolso" && substr($key, 0, 5) != "check" && $key != "observacao" && $key != "rede" && $key != "reembolso" && $key != "carencia" && $key != "documento") {
        if ($dadoPDF != 0) {
            echo $this->Form->input($key, ["type" => "hidden", "value" => $key]);
        }
    }
}
?>
<!-- HIDDEN FIELD COM O TIPO DE AÇÃO REFERENTE AO PDF (DOWNLOAD ou GERAÇÃO NO BROWSER) VIA jQuery -->
<?= $this->Form->input("tipopdf", ["type" => "hidden", "value" => ""]); ?>
<?= $this->Form->end(); ?>
<?php
if (isset($sessao)) : ?>
    <?= $this->element('new_acoes'); ?>
<?php else : ?>
    <div class="centralizada well well-sm" id="acoesgerador">
        <!-- DOWNLOAD DO PDF -->
        <?= $this->Form->button($this->Html->tag('span', '', ['class' => 'fa fa-download', 'aria-hidden' => 'true']) . " Download", ['class' => ' btn btn-lg btn-default', "id" => "pdfdownload", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "bottom", "title" => "Download do PDF"]); ?>
    </div>
<?php endif; ?>

<div class="container">
    <div id="header">
        <?php echo $this->element('cabecalho_cotacao'); ?>
    </div>
    <?php foreach ($tabelasOrdenadas as $operadora_id => $operadora) { ?>

        <table>
            <tr>
                <td style="text-align: justify; line-height: 16px;">
                    À <b><?php echo $cotacao['nome'] ?></b>, A/C: <b><?php echo $cotacao['contato'] ?></b>.<br />
                    Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.<br />
                    Informamos que os valores, regras de comercialização e condições contratuais são determinadas pelas seguradoras/operadoras dos planos<br />
                    e podem ser alterados pelas mesmas a qualquer momento.
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table style="width: 100%" cellspacing="0">
            <tr>
                <td style="width: 15%; text-align: left; font-weight:bold;">
                    <?= $this->Html->image('/webroot/' . $operadora['operadora']['imagen']['caminho'] . $operadora['operadora']['imagen']['nome'], ['fullBase' => true, 'height' => 40]) ?>
                </td>
                <td style="text-align: left; font-weight:bold;">
                    <?= $operadora['operadora']['nome'] . '<br/><small style="font-weight: 100">' . $operadora['operadora']['detalhe'] . "</small>" ?>
                </td>
            </tr>
        </table>

        <br />
        <table class="table table-condensed">
            <?php foreach ($operadora['tabelas'] as $tabela) : ?>
                <tr style="background-color: #ccc;">
                    <td class="beneficiariosTransparente centralizadaVertical centralizada" style="width: 100%;padding: 5px 0; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a" colspan="3">
                        <?= '<strong>' . $tabela->nome . '</strong> | ' . $tabela->descricao . ' ' .    $tabela->minimo_vidas . ' à ' . $tabela->maximo_vidas ?>
                    </td>
                </tr>

                <tr style="border: solid 1px #ddd">
                    <td class='beneficiariosTransparente centralizadaVertical'>
                        <?php if ($tabela->preco_vida == 0) : ?>
                            ***
                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                            <strong>Tabela em Atualização de Preço</strong>
                        <?php else : ?>
                            <?= $this->Number->format($tabela->preco_vida, ['places' => 2, "before" => "R$ "]) ?>
                            <br />
                            <span style="font-size: 100%;">
                                por vida
                            </span>
                        <?php endif ?>
                    </td>

                    <td class="beneficiariosTransparente  centralizada centralizadaVertical">
                        <?php if ($tabela->preco_vida == 0) : ?>
                            ***
                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                            <strong>Tabela em Atualização de Preço</strong>
                        <?php else : ?>
                            <?= $cotacao->quantidade_vidas ?>
                            <br />
                            <span style="font-size: 100%;">
                                vida(s)
                            </span>
                        <?php endif ?>
                    </td>

                    <td class="beneficiariosTransparente  centralizada centralizadaVertical">
                        <?php if ($tabela->preco_vida == 0) : ?>
                            ***
                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                            <strong>Tabela em Atualização de Preço</strong>
                        <?php else : ?>
                            <strong>
                                <?= $this->Number->format($cotacao->quantidade_vidas * $tabela->preco_vida, ['places' => 2, "before" => "R$ "]) ?>
                                <br />
                                <span style="font-size: 100%;">
                                    Total
                                </span>
                            </strong>
                        <?php endif ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

        <div class="nobreak">
            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                OBSERVAÇÕES IMPORTANTES
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong>
                <br><br>
                <?php foreach ($operadora['observacoes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= $value['data'] ?>
                        <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
        </div>

        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                REDE REFERENCIADA (Resumo) <span><?= (isset($operadora['operadora']['url_rede']) && !is_null($operadora['operadora']['url_rede'])) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['operadora']['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php foreach ($operadora['redes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= $value['data'] ?>
                        <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>

        </div>

        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">

            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
            </div>

            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php foreach ($operadora['reembolsos'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= $value['data'] ?>
                        <?= count($operadora['reembolsos']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>

        </div>

        <div class="clearfix">&nbsp;</div>

        <div class="nobreak">
            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
            </div>

            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <strong>Areas de Atendimento</strong>
                <br>
                <?php foreach ($operadora['atendimentos'] as $tipo => $tabelas) {
                    echo '<strong>' . $tipo  . '</strong>: ' . $tabelas . '<br>';
                } ?>
                <br>
                <strong>Areas de Comercialização</strong>
                <br>
                <?php foreach ($operadora['areas_comercializacoes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= substr($value['data'], 0, -2) . '.' ?>
                        <?= count($operadora['areas_comercializacoes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>

        </div>

        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">

            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                CARÊNCIAS (Resumo)
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php foreach ($operadora['carencias'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= $value['data'] ?>
                        <?= count($operadora['carencias']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
        </div>


        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                DEPENDENTES
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php foreach ($operadora['dependentes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= $value['data'] ?>
                        <?= count($operadora['dependentes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
        </div>

        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                DOCUMENTOS NECESSÁRIOS
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php foreach ($operadora['documentos'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= $value['data'] ?>
                        <?= count($operadora['documentos']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                FORMAS DE PAGAMENTO
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php foreach ($operadora['pagamentos'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= $value['data'] ?>
                        <?= count($operadora['pagamentos']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
        </div>

    <?php
    }
    ?>
    <div class="rodape-home col-xs-12" style="text-align: center;">
        <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
            <span style="text-decoration: none !important; font-size: 16px"> corretorparceiro.com.br</span>
        </a>
        <br>
        <span style="color: #004057; font-size: 16px">Assessoria de Apoio a Corretores</span>
    </div>
</div>
<?= $this->element('modal-login'); ?>
<?= $this->element('cotacao-email'); ?>
<script>
    var action = '<?= $this->request->params["action"] ?>';
    if (action === 'filtro') {
        var serialize = 'operadorasFiltro';
    } else {
        var serialize = 'operadoras';
    }
    $("#pdfdownload").click(function(event) {
        event.preventDefault();
        $("#tipopdf").val("D");
        $("#dadosPDF").attr("target", "");
        $("#dadosPDF").submit();
    });
    $("#whatsapp").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        let link = "<?= $linkWhats ?>";
        if (sessao != "") {
            window.open(link);
        } else {
            $("#modal-login").modal("show");
        }

    });
    $("#btn-email").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        if (sessao != "") {
            $("#modal-compartilhamento").modal("show");
        } else {
            $("#modal-login").modal("show");
        }
    });
</script>