<?php
$tipo_pessoa = $odontoCalculo['tipo_pessoa'];
if ($tipo_pessoa == 'pf' || $tipo_pessoa == 'PF') {
    $tipo_pessoa = 'PF';
    $titulo = 'Pessoa Física';
}
if ($tipo_pessoa == 'pj' || $tipo_pessoa == 'PJ') {
    $tipo_pessoa = 'PJ';
    $titulo = 'PME';
}
?>

    <?= $this->Form->create($odontoCalculo, ['id' => 'odonto-calculo','class' => 'form form-validate', 'url' => '/odontoCalculos/encontrarTabelas']) ?>
    <?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Editar Cálculo']); ?>
    <div class="card-body">
    <div class="row">
    <div class="col-md-4">
        <?= $this->Form->input('nome'); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('email'); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('telefone'); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('estado_id', ['label' => 'Estado', 'empty' => 'SELECIONE', 'value' => 5]); ?>
    </div>
    <div class="col-md-4">
        <?php if ($tipo_pessoa == "PF") : ?>
            <?= $this->Form->input('titularesPF', ['label' => 'Titulares', 'type' => 'number']); ?>
        <?php else : ?>
            <?= $this->Form->input('titularesPJ', ['label' => 'Titulares', 'type' => 'number']); ?>
        <?php endif ?>
    </div>
    <div class="col-md-4">
        <?php if ($tipo_pessoa == "PF") : ?>
            <?= $this->Form->input('dependentesPF', ['label' => 'Dependentes', 'type' => 'number']); ?>
        <?php else : ?>
            <?= $this->Form->input('dependentesPJ', ['label' => 'Dependentes', 'type' => 'number']); ?>
        <?php endif ?>
    </div>

    <?= $this->Form->input('calculo_id', ['type' => 'hidden', 'value' => $odontoCalculo['id']]); ?>
    <?= $this->Form->input('data', ['type' => 'hidden']); ?>
    <?= $this->Form->input('user_id', ['type' => 'hidden', 'value' => $odontoCalculo["user_id"]]); ?>
    <?= $this->Form->input('calculoremoto', ['type' => 'hidden', 'value' => $odontoCalculo['calculoremoto']]); ?>
    <?= $this->Form->input('tipo_pessoa', ['type' => 'hidden', 'value' => $odontoCalculo['tipo_pessoa']]); ?>

    </div>
    </div>
    <?php echo $this->element('forms/buttons') ?>
    <?= $this->Form->end() ?>