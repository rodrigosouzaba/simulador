<?= $this->Form->create('operadoras', ['id' => 'operadoras', 'url' => ['action' => 'pdf']]); ?>
<div id="resultado">
    <div class="alert alert-info centralizada" role="alert" style="margin-top:20px;">
        Foram filtrados <strong> <?= $totalTabelas ?></strong> cálculos de <b><?= $totalsemfiltro ?></b> opções.

        <?= $this->Html->link($this->Html->tag('span', 'Editar Cálculo Original', ['class' => '', 'aria-hidden' => 'true']), ['action' => 'view', $odonto_calculo['id']], ['class' => 'btn btn-sm btn-primary', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Cálculo Original', 'style' => 'margin-left: 5px;']); ?>
        <?= $this->Html->link($this->Html->tag('span', 'Reenviar PDF', ['class' => '', 'aria-hidden' => 'true']), ['action' => 'reenviarpdf', $filtro['id']], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Reenviar PDF', 'style' => 'margin-left: 5px;']); ?>


        <!--<a href="#collapseFiltro" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="collapseFiltro" style="margin-left: 20px;">Deseja filtrar os resultados? </a>-->

    </div>
    <div class="clearfix">&nbsp;</div>


    <?php if ($totalTabelas > 0) { ?>
        <?php
        $idOperadora = null;
        foreach ($tabelasOrdenadas as $chave => $tabelas) {
            //                debug($chave);
            //                debug($tabelas);
            //                die();
            $numeroTabelas = 0;
            foreach ($tabelas as $p => $t) {
                $numeroTabelas = $numeroTabelas + count($t);
            }
        ?>

            <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">

                <div class="panel panel-default col-xs-12" style="margin: 5px 0;">
                    <div class="panel-heading" role="tab" id="heading<?= $chave ?>" style="min-height: 100px; background-color: #fff !important;">
                        <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $chave ?>" aria-expanded="true" aria-controls="collapse<?= $chave ?>">
                            <div class="col-md-3 vermais">

                                <i class="fa fa-chevron-circle-up" aria-hidden="true" style="font-size: 110% !important;"></i> Mostar <?= $numeroTabelas ?> cálculos
                        </a>
                        <!--                        <label for="checkAll<?= $chave ?>" style="color: #337ab7; display: inline !important">
                            <?= $this->Form->checkbox('published', ['hiddenField' => false, 'id' => 'checkAll' . $chave, 'class' => 'todas', 'name' => 'checkAll' . $chave, 'value' => $chave, 'style' => 'margin-left: 15px; color: #23527c']); ?> Exibir todos no PDF
                        </label>  -->

                    </div>
                    </a>
                    <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $chave ?>" aria-expanded="true" aria-controls="collapse<?= $chave ?>">

                        <h4 class="panel-title centralizada col-md-6">
                            <?php
                            foreach ($operadoras as $operadora) {
                                if ($operadora['id'] == $chave) {
                                    echo $this->Html->image("../" . $operadora->imagen->caminho . "/" . $operadora->imagen->nome, ['class' => 'logoSimulacao']);
                                    echo "<p class='detalheOperadora'>" . $operadora['detalhe'] . "</p>";
                                }
                            }
                            ?>
                        </h4>
                    </a>
                    <div class="col-md-3" style="margin: 2.5% 0;">
                        <?php
                        //                            $linkcp = '';
                        foreach ($operadoras as $operadora) {
                            if ($operadora['id'] == $chave) {

                                $linkcp = $operadora['url'];
                        ?>
                                <a class="controle" href="<?php echo $linkcp ?>" target="_blank"> Ver Material de Apoio</a>
                        <?php
                                break;
                            }
                        }
                        ?>

                    </div>
                </div>
                <div id="collapse<?= $chave ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $chave ?>">
                    <div class="panel-body" style="border-top: none !important">



                        <table class="table table-condensed">

                            <?php
                            foreach ($tabelas as $produto) {

                                foreach ($produto as $produto) {
                                    //                                        debug($produto);
                                    //                                        debug($odonto_calculo);
                                    //                                        die();
                            ?>

                                    <tr style="background-color: #eee; border: solid 1px #ddd">
                                        <!--                                        <td class="centralizada beneficiarios" style="width: 4%">
                                            Exibir<br>no PDF
                                        </td>-->
                                        <td class="beneficiariosTransparente centralizadaVertical centralizada" colspan="3" style="font-size: 90% !important">


                                            <b><?= $produto['odonto_produto']['nome'] . "</b> | " . $produto['nome'] . "  " . $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas" ?>

                                        </td>
                                    </tr>
                                    <tr style="border: solid 1px #ddd">
                                        <!--                                        <td class="centralizada beneficiariosTransparente">
                                            <?= $this->Form->input($produto['id'], ['type' => 'checkbox', 'id' => 'imprimir' . $produto['id'], 'value' => $produto['id'], 'class' => 'noMarginBottom ' . $chave, 'title' => 'Selecionar Para Impresão', 'label' => '']); ?>
                                        </td>-->
                                        <td class="beneficiariosTransparente  centralizada centralizadaVertical" style="font-size: 90% !important">
                                            <?= $this->Number->currency($produto['preco_vida'], "R$ ") ?>
                                            <br>
                                            por vida
                                        </td>
                                        <td class="beneficiariosTransparente  centralizada centralizadaVertical" style="font-size: 90% !important">
                                            <?= $odonto_calculo['quantidade_vidas'] ?>
                                            <br>
                                            vida(s)
                                        </td>
                                        <td class="beneficiariosTransparente centralizada centralizadaVertical negrito" style="font-size: 90% !important">
                                            <?= $this->Number->currency($odonto_calculo['quantidade_vidas'] * $produto['preco_vida'], "R$ ") ?>
                                            <br>
                                            <b>Total</b>
                                        </td>


                                    </tr>
                            <?php
                                }
                            }
                            ?>
                            </tr>


                        </table>




                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            OBSERVAÇÕES IMPORTANTES
                        </div>



                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            $redes = null;
                            $teste = null;
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    //                                    debug($produto);die();
                                    $redes[$produto['odonto_produto']['descricao']] = $produto['odonto_produto']['odonto_observacao']['descricao'];
                                }
                            }
                            foreach ($redes as $chave => $valor) {
                                if ($valor != '') {
                                    echo nl2br($valor);
                                    $teste = 1;
                                    break;
                                }
                            }
                            echo ($teste != 1) ? "<div class='info'>Consulte Operadora</div>" : '';
                            ?>
                        </div>




                        <div class="clearfix">&nbsp;</div>

                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
                        </div>
                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            $redes = null;
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    //                                    debug($produto);die();
                                    $redes[$produto['odonto_produto']['nome']] = $produto['odonto_comercializaco']['descricao'] . " / " . $produto['odonto_atendimento']['descricao'];
                                }
                            }
                            ?>
                            <table class="table table-condensed ">
                                <tr>
                                    <td style="border: 0 !important"></td>
                                    <td style="border: 0 !important;text-align: center;">Comercialização </td>
                                    <td style='border: 0 !important;text-align: center;'>Atendimento</td>
                                </tr>
                                <?php
                                foreach ($redes as $chave => $valor) {
                                    echo "<tr><td style='border: 0 !important;vertical-align: middle;'><b class='fonteReduzida'>" . $chave . "</b> </td>";
                                    $arr = explode("/", $valor, 2);
                                    echo ($arr[0] != ' ') ? "<td style='border: 0 !important;vertical-align: middle;text-align: center;text-transform: uppercase'><span class='fonteReduzida'>" . nl2br($arr[0]) . "</span></td>" : "<td style='border: 0 !important;vertical-align: middle;text-align: center;'><span class='fonteReduzida'>CONSULTE OPERADORA</span></td>";
                                    echo ($arr[1] != ' ') ? "<td style='border: 0 !important;vertical-align: middle;text-align: center;text-transform: uppercase'><span class='fonteReduzida'>" . nl2br($arr[1]) . "</span></td></tr>" : "<td style='border: 0 !important;vertical-align: middle;text-align: center;'><span class='fonteReduzida'>CONSULTE OPERADORA</span></td></tr>";
                                }
                                ?>
                            </table>

                        </div>
                        <div class="clearfix">&nbsp;</div>
                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            REDE REFERENCIADA (Resumo)
                        </div>



                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            $redes = null;
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    //                                    debug($produto);die();
                                    $redes[$produto['odonto_produto']['nome']] = $produto['odonto_produto']['odonto_rede']['descricao'];
                                }
                            }
                            foreach ($redes as $chave => $valor) {
                                echo "<b>" . $chave . ":</b> ";
                                echo ($valor != '') ? nl2br($valor) : "<span class='info'>Consulte Operadora</span>";
                                echo "<div class='clearfix'>&nbsp;</div>";
                            }
                            ?>
                        </div>

                        <div class="clearfix">&nbsp;</div>


                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            COBERTURAS, REEMBOLSOS E DIFERENCIAIS (Resumo)
                        </div>



                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            $redes = null;
                            $teste = null;
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    $redes[$produto['odonto_produto']['nome']] = $produto['odonto_produto']['odonto_reembolso']['descricao'];
                                }
                            }
                            foreach ($redes as $chave => $valor) {
                                if ($valor != '') {
                                    echo nl2br($valor);
                                    $teste = 1;
                                    break;
                                }
                            }
                            echo ($teste != 1) ? "<div class='info'>Consulte Operadora</div>" : '';
                            ?>
                        </div>




                        <div class="clearfix">&nbsp;</div>
                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            CARÊNCIAS (Resumo)
                        </div>



                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            $redes = null;
                            $teste = null;
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    $redes[$produto['odonto_produto']['descricao']] = $produto['odonto_produto']['odonto_carencia']['descricao'];
                                }
                            }
                            foreach ($redes as $chave => $valor) {
                                if ($valor != '') {
                                    echo nl2br($valor);
                                    $teste = 1;
                                    break;
                                }
                            }
                            echo ($teste != 1) ? "<div class='info'>Consulte Operadora</div>" : '';
                            ?>
                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            DOCUMENTOS NECESSÁRIOS
                        </div>



                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            //                                                        debug($produto['odonto_produto']);
                            //                                                        die();
                            $redes = null;
                            $teste = null;
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    $redes[$produto['odonto_produto']['descricao']] = $produto['odonto_produto']['odonto_documento']['descricao'];
                                }
                            }
                            foreach ($redes as $chave => $valor) {
                                if ($valor != '') {
                                    echo nl2br($valor);
                                    $teste = 1;
                                    break;
                                }
                            }
                            echo ($teste != 1) ? "<div class='info'>Consulte Operadora</div>" : '';
                            ?>
                        </div>



                    </div>
                </div>
            </div>
</div>



<?php
        }
        echo $this->Form->end();
?>
<div class="clearfix">&nbsp;</div>


<div class="centralizada">
    <?php
        foreach ($operadoras as $operadora) {
            if (!in_array($operadora['id'], array_keys($tabelasOrdenadas))) {
                $vetor[$this->Html->image("../" . $operadora['imagen']['caminho'] . $operadora['imagen']['nome'], ['class' => 'logoSimulacaoReduzido'])] = $operadora['url'];
            }
        }
        if (isset($vetor)) {
            foreach ($vetor as $img => $valor) {
                //                    debug($img);
                //                    debug($valor);
                //                    die();
    ?>
            <div class="well well-sm col-xs-12">
                <div class="col-xs-3" style="font-size: 12px;text-align: left; padding-left: 1%; margin-top: 1.2%;">
                    <i class="fa fa-minus-circle" aria-hidden="true" style="font-size: 110% !important;"></i>
                    0 cálculos <br /><span class="text-danger">Operadora não disponibiliza cálculos para este perfil de cliente.</span>
                </div>
                <div class="col-xs-6">
                    <?= $img; ?>

                </div>
                <div class="col-xs-3" style="font-size: 12px;text-align: left; padding-left: 1%; margin-top: 1.8%;">
                    <a class="controle" href="<?= $valor ?>" target="_blank"> Ver Material de Apoio</a>
                </div>
            </div>
    <?php
            }
        }
        //            debug(array_keys($tabelasOrdenadas));
    ?>
</div>




<?php
    }

    //debug($filtro);
?>
<div class="col-md-12 centralizada" style="background-color: #ddd; margin-top: 20px;clear: both;">
    <!--    <div class="clearfix">&nbsp;</div>

        <div class="col-md-12 centralizada" style="background-color: #ddd; font-weight: bold">
            EXIBIDO NO PDF
        </div>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-12 centralizada">
            <strong>
    <?php
    echo ($filtro['observacao'] == 1) ? "Observações Importantes<br/>" : "";
    echo ($filtro['rede'] == 1) ? "Rede Credenciada (Resumo)<br/>" : "";
    echo ($filtro['reembolso'] == 1) ? "Observações Importantes<br/>" : "";
    echo ($filtro['carencia'] == 1) ? "Carência (Resumo)<br/>" : "";
    echo ($filtro['informacao'] == 1) ? "Documentos Necessários" : "";
    ?>
            </strong>

            <div class="clearfix">&nbsp;</div>

                <div class="col-md-12 centralizada">

    <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-file-pdf-o', 'aria-hidden' => 'true']) . ' Gerar PDF'), ['class' => 'btn btn-md btn-primary', 'id' => 'pdf']) ?>



    <?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', $btnCancelar, ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Retornar', 'id' => 'retornar']); ?>

                </div>
            <div class="clearfix">&nbsp;</div>
            <div id="salvarSimulacao"></div>
        </div>-->


    <?php
    echo $this->Form->end();
    ?>

</div>
<?= $this->element('processando'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('.todas').click(function() {
            $('.' + (this).value).prop('checked', this.checked);
        });
    });

    $("#pdf").click(function() {

        $.ajax({
            type: "post",
            //            data: $("#operadoras").serialize(),
            //            url: "<?php echo $this->request->webroot ?>simulacoes/pdf/",
            beforeSend: function() {
                $('#loader').trigger('click');
            },
            success: function(data) {
                $('#fechar').trigger('click');
            }
        });
    });
    $("#filtrar").click(function() {
        $.ajax({
            type: "post",
            data: $("#operadoras").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/filtro/",
            beforeSend: function() {
                $('#loader').trigger('click');
            },
            success: function(data) {
                $('#fechar').trigger('click');
                $('#resultado').empty();
                $('#resultado').append(data);
                $("body").removeClass("modal-open");
                $('.modal-backdrop').remove()
            }
        });
    });
</script>
