<div class="col-xs-12">
    <h4 style="padding: 10px 0" class="centralizada"><?= $titulo ?></h4>
    <div class="clearfix">&nbsp;</div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr >
                <th><?= $this->Paginator->sort('id', ['label' => 'Nº Cálculo']) ?></th>
                <th><?= $this->Paginator->sort('data') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('quantidade_vidas', ['label' => 'Vidas']) ?></th>
                <th>Selecione Status</th>

                <th class="actions" style='width: 10%'><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($odontoCalculos as $odontoCalculo): ?>
                <tr>
                    <td><?= $odontoCalculo->id ?></td>
                    <td><?= h($odontoCalculo->data) ?></td>
                    <td>
                        <span data-toggle="popover" data-html="true" data-placement="top" data-trigger="click" title="Dados do Cliente" 
                              data-content="<div style='text-align:left;'><b>Telefone: </b><?= $odontoCalculo['telefone'] ?> <br> <b>Email: </b><?= $odontoCalculo['email'] ?> </div>" >
                                  <?=
                                  $this->Html->link('', '#historico' . $odontoCalculo['id'], [
                                      'class' => 'fa fa-info-circle listaracompanhamentos',
                                      'style' => 'margin: 0 5px;']);
                                  ?>
                        </span>
                        <?= h($odontoCalculo->nome) ?></td>

                    <td><?= $this->Number->format($odontoCalculo->quantidade_vidas) ?></td>

                    <td>
                        <?= $this->Form->input('status_id', ['label' => '', 'value' => $odontoCalculo->status_id, 'empty' => 'SEM STATUS', 'options' => $status, 'odonto_calculo_id' => $odontoCalculo->id, 'class' => 'change_status']) ?>


                    </td>
                    <td class="actions">

                        <span data-toggle='tooltip' data-placement='top' title='Acompanhamento '>

                            <?=
                            $this->Html->link('', '#historico' . $odontoCalculo->id, [
                                'class' => 'btn btn-sm btn-default fa fa-list listarpdf',
                                'role' => 'button',
                                'value' => $odontoCalculo->id,
                                'data-toggle' => 'modal',
//                                data-toggle="modal" data-target="#modalFiltro"
                                'data-target' => '#modalFiltro',
                                'aria-expanded' => "false"
                            ])
                            ?>
                        </span>
                        <?=
                        $this->Html->link('', ['action' => 'edit', $odontoCalculo->id], [
                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Cálculo',
                            'class' => 'btn btn-sm btn-default glyphicon glyphicon-edit'])
                        ?>
                        <?=
                        $this->Html->link('', ['action' => 'view', $odontoCalculo->id], [
                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Visualizar Cálculo',
                            'title' => __('Visualizar Cálculo'),
                            'class' => 'btn btn-sm btn-default glyphicon glyphicon-eye-open'])
                        ?>


                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<!-- Modal FILTRO-->
<div class="modal fade" id="modalFiltro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="conteudoModal">

        </div>
    </div>
</div>

<script type="text/javascript">
    $("#busca").hide();
    $("#tipo-busca").change(function () {
        $("#busca").show();
    });
    $('[data-toggle="popover"]').popover();

    $(".change_status").change(function () {
        $.ajax({
            type: "POST",
            url: "<?= $this->request->webroot ?>odonto-calculos/alterar-status/" + $(this).attr("odonto_calculo_id") + "/" + (this).value,
            success: function (data) {
                location.reload();
            }
        });
    });
    $(".listarpdf").click(function () {

        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>odonto-calculos/listarpdfs/" + $(this).attr("value"),

            success: function (data) {
                $("#conteudoModal").empty();
                $("#conteudoModal").append(data);
            }
        });
    });
    $(".reenvio").click(function () {
        $.ajax({
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#close').trigger('click');
            }
        });



    });
</script>