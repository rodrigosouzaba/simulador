<!-- Button trigger modal -->
<button type="button" id="loader" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#modalReenvio">
</button>
<!-- Modal -->
<div class="modal fade" id="modalReenvio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span style="margin-right: 20px !important;">Processando </span><img src='../../img/spinner.gif' style='max-height: 60px;margin:-10px;' /> </h4> 
            </div>

        </div>
    </div>
</div>
<div class="modal-header centralizada">
                <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Cálculo Nº <b><?= $calculo['id'] ?></b> </h4> 
            </div>
            <div class="modal-body col-md-12" >
                
<h4 class="centralizada" style="border-bottom: 0.5px solid #e5e5e5;
    padding: 0 0 20px;">PDFs gerados e enviados por email:
      
</h4>


<?php
if (isset($filtros) && !empty($filtros)) {
    foreach ($filtros as $filtro) {
        ?>
        <div class="col-md-6 centralizada"><?= $filtro['created']; ?></div>
        <div class="col-md-6 centralizada">
            <?php
            echo $this->Html->link('', ['action' => 'filtropdf', $filtro['id']], [
                'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Visualizar dados do PDF',
                'class' => 'btn btn-sm btn-primary fa fa-file-pdf-o',
                'style' => 'margin: 3px 0;']);

            echo $this->Html->link('', ['action' => 'reenviarpdf', $filtro['id']], [
                'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Reenviar PDF',
                'class' => 'reenvio btn btn-sm btn-primary fa fa-envelope',
                'style' => 'margin: 3px 5px;'])
            ?>

        </div>
        <?php
    }
} else {
    ?>
    <div class="well well-sm centralizada" style="font-size: 80%; color: #666">
        Nenhum PDF gerado ainda para o cálculo selecionado
    </div>
<?php }
?>
<div id="processo">
    <h4 class="centralizada" style="color: #666"> 
        Processando 
    </h4>
    <div class="centralizada">
        <?= $this->Html->image("spinner.gif", array("style" => "max-height: 60px;")) ?>
    </div>
    <!--    echo $this->Html->image("logo.png", array(
        'url' => $url,
        'class' => 'logo'
        ));-->

</div> 

            </div>
            <div class="modal-footer">
<!--                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>-->
            </div>





<script type="text/javascript">
    $('#processo').hide('click');
    $(".reenvio").click(function () {
        $.ajax({
            beforeSend: function () {
                $('#processo').show('click');
            },
            success: function (data) {
                $('#close').trigger('click');
            }
        });
    });
</script>