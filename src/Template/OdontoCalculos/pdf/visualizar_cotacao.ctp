<style type="text/css">
    .titulos {
        background-color: #ccc;
        text-align: center;
        font-weight: bold;
        padding: 5px;
        font-size: 90%;
    }

    .conteudo {
        padding: 5px;
        border: 0.1mm solid #ccc;
    }

    .introducao {
        font-size: 16px;
        text-align: justify;
        line-height: 16px;
        width: 100%;
        padding-right: 50px;
    }

    .jumbotron {
        background-color: #eee;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 10px;
        line-height: 1.5;
        width: 100%;
        /*width: 97%;*/
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }


    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
        font-size: 60%;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .totalTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
        font-weight: bold;
    }

    .fonteReduzida {
        font-size: 10px;
        text-align: justify !important;
        text-align: justify;
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        line-height: 1.3;
        margin-bottom: 0;
        cursor: default;
        display: block;
        font-style: normal;
        font-variant-caps: normal;
        vertical-align: baseline;
        white-space: normal;

    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .nobreak {
        page-break-inside: avoid;
    }

    .info {
        color: #a9a9a9;
        font-weight: lighter;
        font-style: italic;
        text-align: center;
        width: 100%;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .verde {
        color: #128c7e;
    }

    .pagina {
        padding: 20px;
        margin-top: 85px;
    }

    @media(max-width: 767px) {
        .pagina {
            margin-top: 106px;
        }
    }

    @media(min-width: 768px) and (max-width: 895px) {
        .pagina {
            margin-top: 191px;
        }
    }

    @media(min-width: 896px) and (max-width: 1218px) {
        .pagina {
            margin-top: 141px;
        }
    }
</style>
<div class="container">
    <?php foreach ($tabelas_ordenadas as $operadora_id => $operadora) { ?>
        <div id="header">
            <?php echo $this->element('cabecalho_pdf'); ?>
        </div>
        <table>
            <tr>
                <td class="introducao">
                    À <b><?= $simulacao['nome'] ?></b>, A/C: <b><?= $simulacao['contato'] ?></b>.<br />
                    Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.<br />
                    Informamos que os valores, regras de comercialização e condições contratuais são determinadas pelas seguradoras/operadoras dos planos<br />
                    e podem ser alterados pelas mesmas a qualquer momento.
                </td>
            </tr>
        </table>
        <?php $tamanhoFonte = 'font-size:95%'; ?>
        <br /> <br />

        <table style="width: 100%" cellspacing="0">
            <tr>
                <td style="width: 15%; text-align: left; font-weight:bold;">
                    <img src='<?= WWW_ROOT . 'uploads/imagens/' . $operadora['dados']['imagen']['nome']; ?>' height="40" /><br />
                </td>
                <td style="width: 85%; text-align: left; font-weight:bold;">
                    <?= $operadora['dados']['nome'] . '<br/><small style="font-weight: 100">' . $operadora['dados']['detalhe'] . "</small>" ?>
                </td>
            </tr>
        </table>

        <br />
        <?php $corfundo = '#eee'; ?>
        <table style="width: 100%" cellspacing="0">
            <?php
            foreach ($operadora['tabelas'] as $produto) {
            ?>
                <tr style="background-color: #ccc;">
                    <td class="centralizada" colspan="3" style="text-align:center; width: 100%;padding: 5px 0; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a">
                        <strong><?= $produto['nome'] ?> </strong> <?= $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas" ?>
                    </td>
                </tr>
                <tr style="background-color: #fff;border-left: solid 0.1mm #5a5a5a; text-align: center;">

                    <td class="beneficiariosTransparente  centralizada centralizadaVertical" style="font-size: 90% !important; padding: 5px 0; border-left: 0.1mm solid #ccc;border-bottom: 0.1mm solid #ccc;">
                        <?= $this->Number->currency($produto['preco_vida']) ?>
                        <br>
                        por vida
                    </td>
                    <td class="beneficiariosTransparente  centralizada centralizadaVertical" style="font-size: 90% !important; border-bottom: 0.1mm solid #ccc;">
                        <?= $simulacao['quantidade_vidas'] ?>
                        <br>
                        vida(s)
                    </td>
                    <td class="beneficiariosTransparente centralizada centralizadaVertical negrito" style="font-size: 90% !important; border-right: 0.1mm solid #ccc;border-bottom: 0.1mm solid #ccc;">
                        <?= $this->Number->currency($simulacao['quantidade_vidas'] * $produto['preco_vida']) ?>
                        <br>
                        <b>Total</b>
                    </td>
                </tr>
            <?php
            }
            ?>
        </table>

        <div class="clearfix">&nbsp;</div>
        <div class="titulos">
            FORMAS DE PAGAMENTO
        </div>
        <div class="fonteReduzida conteudo">
            <?= ($operadora['dados']['odonto_formas_pagamentos'] != null) ? nl2br($operadora['dados']['odonto_formas_pagamentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
        </div>

        <?php if ($simulacao['observaco'] == 1) { ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div class="titulos">
                    OBSERVAÇÕES IMPORTANTES
                </div>

                <div class="fonteReduzida conteudo">
                    <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
                    <?= $operadora['dados']['odonto_observacaos'] != null ? nl2br($operadora['dados']['odonto_observacaos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                </div>
            </div>
        <?php } ?>

        <div class="clearfix">&nbsp;</div>
        <div class="titulos">
            DEPENDENTES
        </div>
        <div class="fonteReduzida conteudo">
            <?= $operadora['dados']['odonto_dependentes'] != null ? nl2br($operadora['dados']['odonto_dependentes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
        </div>
        ,
        <?php if ($simulacao['rede'] == 1) {; ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div class="titulos">
                    REDE REFERENCIADA (Resumo) <span><?= !empty($operadora['dados']['url_rede']) && !is_null($operadora['dados']['url_rede']) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['dados']['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
                </div>
                <div class="fonteReduzida conteudo">
                    <?= ($operadora['dados']['odonto_redes'] != null) ? nl2br($operadora['dados']['odonto_redes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>"; ?>
                </div>
            </div>
        <?php } ?>

        <?php if ($simulacao['reembolso'] == 1) { ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div class="titulos">
                    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS (Resumo)
                </div>
                <div class="fonteReduzida conteudo">
                    <?= $operadora['dados']['odonto_reembolsos'] != null ? nl2br($operadora['dados']['odonto_reembolsos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>"; ?>
                </div>
            </div>
        <?php } ?>

        <?php if ($simulacao['carencia'] == 1) { ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div class="titulos">
                    CARÊNCIAS (Resumo)
                </div>
                <div class="fonteReduzida conteudo">
                    <?= $operadora['dados']['odonto_carencias'] != null ? nl2br($operadora['dados']['odonto_carencias'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>"; ?>
                </div>
            </div>
        <?php } ?>

        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div class="titulos">
                ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
            </div>

            <div class="fonteReduzida conteudo">
                <?php
                $areas = array();
                foreach ($operadora['tabelas'] as $tabela) {
                    foreach ($tabela['odonto_areas_comercializaco']['municipios'] as $municipio) {
                        $areas[$tabela['id']]['nome'] = $tabela['nome'] . ' - ' . $tabela['descricao'];
                        $areas[$tabela['id']]['atendimento'] = $tabela['odonto_atendimento']['nome'];
                        $areas[$tabela['id']]['estados'][$municipio['estado']['id']]['nome'] =  $municipio['estado']['nome'];
                        $areas[$tabela['id']]['estados'][$municipio['estado']['id']]['municipios'][] =  $municipio['nome'];
                    }
                }
                ?>
                <div class="fonteReduzida" style="margin-left: 10px;">
                    <?php foreach ($areas as $area) : ?>
                        <strong><?= $area['nome'] ?></strong>
                        <br>
                        <div style="margin-left: 10px; border-bottom: 1px solid #cccccc;">
                            <b>COMERCIALIZAÇÃO </b>
                            </br>
                            <?php foreach ($area['estados'] as $estado) : ?>
                                <strong><?= $estado['nome'] ?>:</strong>
                                <?php foreach ($estado['municipios'] as $municipio) : ?>
                                    <?= $municipio ?>,
                                <?php endforeach; ?>
                                </br>
                            <?php endforeach; ?>
                            <br>
                            <b>ATENDIMENTO:</b> <?= $area['atendimento'] ?>
                            </br>
                        </div>
                        </br>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <?php if ($simulacao['documento'] == 1) { ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div class="titulos">
                    DOCUMENTOS NECESSÁRIOS
                </div>
                <div class="fonteReduzida conteudo">
                    <?= $operadora['dados']['odonto_documentos'] != null ? nl2br($operadora['dados']['odonto_documentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>"; ?>
                </div>
            </div>
        <?php } ?>


    <?php } ?>
    <div class="rodape-home col-xs-12" style="text-align: center;">
        <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
            <span style="text-decoration: none !important; font-size: 16px"> corretorparceiro.com.br</span>
        </a>
        <br>
        <span style="color: #004057; font-size: 16px">Assessoria de Apoio a Corretores</span>
    </div>
</div>