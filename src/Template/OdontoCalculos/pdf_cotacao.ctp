<?= $this->element('estilo_cotacao_pdf') ?>
<page backtop="7mm" backbottom="7mm" backleft="5mm" backright="5mm" style="font-size: 11px;">

    <page_footer>
        <div style="font-size: 15px; margin-top: 20px;">
            <div class="rodape-home col-xs-12" style="text-align: center;">
                <span style="color: #004057">Assessoria de Apoio para Corretores.</span><br>
                <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
                    <?= '<img src="https://corretorparceiro.com.br/app/img/ciranda.png" style="height: 15px; margin-left: 5px;" />' ?>
                    <span style="text-decoration: none !important"> corretorparceiro.com.br</span>
                </a>
            </div>
        </div>
    </page_footer>


    <?php foreach ($tabelas_ordenadas as $chave => $operadora) :
        $faixas = (new Cake\Collection\Collection($simulacao->toArray()))
            ->filter(function ($value, $key) {
                return str_starts_with($key, 'faixa') && $value > 0;
            })
            ->count();

    ?>
        <div class="spacer-md">&nbsp;</div>

        <table style="margin-top: 40px;">
            <tr>
                <td style="text-align: justify; line-height: 16px;">
                    À <b><?php echo $simulacao['nome'] ?></b>, A/C: <b><?php echo $simulacao['contato'] ?></b>.<br />
                    Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.<br />
                    Informamos que os valores, regras de comercialização e condições contratuais são determinadas pelas seguradoras/operadoras dos planos<br />
                    e podem ser alterados pelas mesmas a qualquer momento.
                </td>
            </tr>
        </table>

        <br />
        <br />

        <table style="width: 100%" cellspacing="0">
            <tr>
                <td style="width: 15%; text-align: left; font-weight:bold;">
                    <?php
                    $imagem =  !empty($operadora['operadora']['imagen']['caminho'])  && !empty($operadora['operadora']['imagen']['nome']) ?  $operadora['operadora']['imagen']['caminho'] . $operadora['operadora']['imagen']['nome'] : '';
                    $this->Html->image('/webroot/' . $imagem, ['fullBase' => true, 'height' => 40]) ?>
                </td>
                <td style="text-align: left; font-weight:bold;">
                </td>
            </tr>
        </table>

        <br />
        <br />
        <table style="width: 100%; border:  solid 0.1mm #ddd;" cellspacing="0">
            <?php foreach ($operadora['tabelas'] as $tabela) : ?>
                <tr style="width: 100% !important; background-color: #ccc; text-align: center;">
                    <td class="centralizada" style="font-size: 10px; padding: 5px 0 5px 3px; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a; width: 100%;" colspan="3">
                        <?= '<strong>' . $tabela->nome . '</strong> | ' . $tabela->descricao . ' ' .    $tabela->minimo_vidas . ' à ' . $tabela->maximo_vidas ?>
                    </td>
                </tr>

                <tr style="background-color: #fff;border-left: solid 0.1mm #5a5a5a;  ">
                    <td style="border-left: solid 0.1mm #5a5a5a; text-align:center; border-bottom: solid 0.1mm #5a5a5a; padding: 10px 0; vertical-align: middle">
                        <?php if ($tabela->preco_vida == 0) : ?>
                            ***
                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                            <strong>Tabela em Atualização de Preço</strong>
                        <?php else : ?>
                            <?= $this->Number->format($tabela->preco_vida, ['places' => 2, "before" => "R$ "]) ?>
                            <br />
                            <span style="font-size: 100%;">
                                por vida
                            </span>
                        <?php endif ?>
                    </td>

                    <td style="border-left: solid 0.1mm #dddddd; text-align:center; border-bottom: solid 0.1mm #5a5a5a; padding: 10px 0; vertical-align: middle">
                        <?php if ($tabela->preco_vida == 0) : ?>
                            ***
                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                            <strong>Tabela em Atualização de Preço</strong>
                        <?php else : ?>
                            <?= $cotacao->quantidade_vidas ?>
                            <br />
                            <span style="font-size: 100%;">
                                vida(s)
                            </span>
                        <?php endif ?>
                    </td>

                    <td style="font-weight: bold; text-align: center;border-left:solid 0.1mm #dddddd;border-bottom: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a;">
                        <?php if ($tabela->preco_vida == 0) : ?>
                            ***
                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                            <strong>Tabela em Atualização de Preço</strong>
                        <?php else : ?>
                            <strong>
                                <?= $this->Number->format($cotacao->quantidade_vidas * $tabela->preco_vida, ['places' => 2, "before" => "R$ "]) ?>
                                <br />
                                <span style="font-size: 100%;">
                                    Total
                                </span>
                            </strong>
                        <?php endif ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <div class="clearfix">&nbsp;</div>

        <div class="nobreak">
            <!-- ENTIDADES E PROFISSÕES -->
            <?php if (!empty($operadora['grupo_entidades'])) : ?>
                <div class="nobreak">
                    <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 100%;max-width: 100% !important">
                        ENTIDADES E PROFISSÕES
                    </div>
                    <div style="padding: 5px; max-width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                        <?php foreach ($operadora['grupo_entidades'] as $key => $value) :
                            if (!empty($key)) : ?>
                                <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                </br></br>
                                <?= $value['data'] ?>
                                <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                            <?php else : ?>
                                <div class='info'>Consulte Operadora</div>
                        <?php endif;
                        endforeach; ?>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            <?php endif; ?>
        </div>

        <div class="nobreak">
            <!-- FORMAS DE PAGAMENTOS -->
            <div style="max-width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                FORMAS DE PAGAMENTO
            </div>
            <div style="padding: 5px; max-width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php foreach ($operadora['pagamentos'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        <br><br>
                        <?= $value['data'] ?>
                        <?= count($operadora['pagamentos']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <!-- /FORMAS DE PAGAMENTOS -->


        <!-- OBSERVAÇÕES IMPORTANTES -->
        <div class="nobreak">
            <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; max-width: 100%;max-width: 100% !important">
                OBSERVAÇÕES IMPORTANTES
            </div>
            <div class="fonteReduzida corpo-gerador-tabelas">
                <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong>
                <br><br>
                <?php foreach ($operadora['observacoes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        <br><br>
                        <?= $value['data'] ?>
                        <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>
        <!-- OBSERVAÇÕES IMPORTANTES  -->


        <div class="nobreak">
            <!-- DEPENDENTES  -->
            <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; max-width: 100%;max-width: 100% !important">
                DEPENDENTES
            </div>
            <div class="fonteReduzida corpo-gerador-tabelas">
                <?php foreach ($operadora['dependentes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        <br><br>
                        <?= $value['data'] ?>
                        <?= count($operadora['dependentes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>
        <!-- /DEPENDENTES  -->

        <!-- REDE  -->
        <?php if ($simulacao['rede'] == 1) : ?>
            <div class="nobreak">
                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; max-width: 100%;max-width: 100% !important">
                    REDE REFERENCIADA (Resumo) <span><?= (isset($operadora['url_rede']) && !is_null($operadora['url_rede'])) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
                </div>
                <div class="fonteReduzida corpo-gerador-tabelas">
                    <?php foreach ($operadora['redes'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            <br><br>
                            <?= $value['data'] ?>
                            <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
        <?php endif; ?>
        <!-- /REDE  -->

        <!-- OPCIONAIS  -->
        <?php if ($simulacao['reembolso'] == 1) : ?>
            <div class="nobreak">
                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; max-width: 100%;max-width: 100% !important">
                    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                </div>
                <div class="fonteReduzida corpo-gerador-tabelas">
                    <?php foreach ($operadora['redes'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            <br><br>
                            <?= $value['data'] ?>
                            <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
        <?php endif; ?>
        <!-- /OPCIONAIS  -->

        <!-- CARENCIAS -->
        <?php
        if ($simulacao['carencia'] == 1) {
        ?>
            <div class="nobreak">
                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; max-width: 100%;max-width: 100% !important">
                    CARÊNCIAS (Resumo)
                </div>
                <div class="fonteReduzida corpo-gerador-tabelas">
                    <?php foreach ($operadora['carencias'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            <br><br>
                            <?= $value['data'] ?>
                            <?= count($operadora['carencias']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
        <?php } ?>
        <!-- /CARENCIAS -->


        <!-- COMERCIALIZACAO -->
        <div class="nobreak">
            <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; max-width: 100%;max-width: 100% !important">
                ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
            </div>
            <div class="fonteReduzida corpo-gerador-tabelas">
                <?php foreach ($operadora['areas_comercializacoes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        <br><br>
                        <?= !empty($value['data']) ? $value['data'] : '' ?>
                        <?= count($operadora['areas_comercializacoes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>
        <!-- /COMERCIALIZACAO -->


        <!-- DOCUMENTOS -->
        <?php if ($simulacao['documento'] == 1) { ?>
            <div class="nobreak">
                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; max-width: 100%;max-width: 100% !important">
                    DOCUMENTOS NECESSÁRIOS
                </div>
                <div class="fonteReduzida corpo-gerador-tabelas">
                    <?php foreach ($operadora['documentos'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            <br><br>
                            <?= $value['data'] ?>
                            <?= count($operadora['documentos']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
        <?php } ?>
        <!-- /DOCUMENTOS -->

    <?php endforeach; ?>
</page>