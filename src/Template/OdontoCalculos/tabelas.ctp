<style>
    .alert-secondary {
        background-color: #f5f5f5;
    }
</style>
<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Cálculos']); ?>
<div class="card-body">
    <div class="row">
        <div class="col-sm-12">

            <?php if (isset($tabelas_ordenadas)) { ?>
                <?= $this->Form->create('operadoras', ['id' => 'operadoras', 'url' => ['action' => 'salvaFiltros']]); ?>
                <?= $this->Form->hidden('calculo_id', ['value' => $odonto_calculo['id']]); ?>
                <div id="resultado" class="total col-xs-12">
                    <div id="resposta">

                        <div class="well well-sm col-xs-12" style="padding: 5px; min-height: 75px;margin: 5px 0 !important;">

                            <?php
                            $x = 1;
                            $vidas = 0;
                            for ($x = 1; $x <= 12; $x++) {
                                $vidas = $vidas + $odonto_calculo['faixa' . $x];
                            }
                            //                echo "Total de vidas:<b>" . $vidas . "</b>";
                            if (isset($odonto_calculo['nome_titular'])) {
                                $titular = "Nome Titular: " . $odonto_calculo['nome_titular'];
                                $classe = "col-md-3";
                            } else {
                                $classe = "col-md-4";
                            }
                            ?>
                            <div class="col-md-4 col-xs-12" style="text-align: left;padding-left: 0 !important;">
                                <div class="col-xs-12">
                                    Cálculo Nº: <a href="#" data-target="#editModal" data-toggle="modal" odonto_id="<?= $odonto_calculo['id'] ?>" id="btnModalEdit"><span><?= $odonto_calculo['id'] . "   " ?> - <u>editar</u> <u>cotação</u> </span></a>
                                </div>
                                <div class="col-xs-12">
                                    Total de vidas: <b><?= $odonto_calculo["quantidade_vidas"] ?></b>
                                </div>
                                <div class="col-xs-12">
                                    Empresa: <b><?= $odonto_calculo['nome'] ?></b>
                                </div>
                            </div>

                            <div class="col-md-8 col-xs-12" style="text-align: left;">
                                <div class="col-xs-12" style="padding-left: 0 !important;">
                                    <?php
                                    $total = $odonto_calculo['quantidade_vidas'] * $menor_preco['preco_vida'];
                                    ?>
                                    <b class="tabulacao"><?= $this->Number->currency($total, "BRL") ?></b>Menor Preço <small> (<?= $menor_preco["odonto_produto"]["odonto_operadora"]['nome'] ?>)</small>
                                </div>

                                <div class="col-xs-12" style="padding-left: 0 !important;">

                                    <?php
                                    $inter = $odonto_calculo['quantidade_vidas'] * $intermediario['preco_vida'];
                                    ?>
                                    <b class="tabulacao"><?= $this->Number->currency($inter, "BRL") ?></b>Preço Intermediário <small> (<?= $intermediario["odonto_produto"]["odonto_operadora"]['nome'] ?>)</small>
                                </div>
                                <div class="col-xs-12" style="padding-left: 0 !important;">
                                    <b class="tabulacao"><?= $this->Number->currency($odonto_calculo['quantidade_vidas'] * $maior_preco['preco_vida'], "BRL") ?></b><span>Maior Preço <small> (<?= $maior_preco["odonto_produto"]["odonto_operadora"]['nome'] ?>)</small></span>
                                </div>
                            </div>
                        </div>
                        <?php
                        $idOperadora = null;
                        foreach ($tabelas_ordenadas as $operadora_id => $operadora) {
                        ?>
                            <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default col-xs-12" style="margin: 5px 0;">
                                    <div class="panel-heading" role="tab" id="heading<?= $operadora_id ?>" style="min-height: 100px; background-color: #fff !important;">
                                        <div class="col-md-6 vermais" style="font-size: 14px !important;">

                                            <!-- ABRIR CALCULOS -->
                                            <div class="col-xs-12 col-md-4" style="padding-right: 0 !important;padding-left: 0 !important;">
                                                <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $operadora_id ?>" aria-expanded="true" aria-controls="collapse<?= $operadora_id ?>">
                                                    <i class="fas fa-chevron-down" aria-hidden="true" style="font-size: 110% !important;"></i> <span style="font-size: 14px !important;">Abrir <?= count($operadora['tabelas']) ?> cálculo(s)</span>
                                                </a>
                                            </div>

                                            <!-- EXIBIR NO CALCULO -->
                                            <div class="col-xs-12 col-md-4" style="padding-right: 0 !important;padding-left: 0 !important;">
                                                <label for="checkAll<?= $operadora_id ?>" style="color: #337ab7; display: inline !important">
                                                    <?= $this->Form->checkbox('published', ['hiddenField' => false, 'id' => 'checkAll' . $operadora_id, 'class' => 'todas', 'name' => 'checkAll' . $operadora_id, 'value' => $operadora_id, 'style' => 'color: #23527c']); ?> <span style="font-size: 14px;">Exibir todos no cálculo</span>
                                                </label>
                                            </div>

                                            <!-- LOGO DA OPERADORA -->
                                            <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $operadora_id ?>" aria-expanded="true" aria-controls="collapse<?= $operadora_id ?>">
                                                <div class="col-xs-12 col-md-4" style="padding-right: 0 !important;padding-left: 0 !important;">
                                                    <a class="controle" href="<?php echo $operadora['dados']['url'] ?>" target="_blank" style="font-size: 14px !important;"> Ver Material de Vendas</a>
                                                </div>
                                            </a>
                                        </div>

                                        <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $operadora_id ?>" aria-expanded="true" aria-controls="collapse<?= $operadora_id ?>">
                                            <h4 class="panel-title centralizada col-md-6">
                                                <?= $this->Html->image("../" . $operadora['dados']['imagen']['caminho'] . "/" . $operadora['dados']['imagen']['nome'], ['class' => 'logoSimulacao']) . "<p class='detalheOperadora'>" . $operadora['dados']['detalhe'] . "</p>" ?>
                                            </h4>
                                        </a>
                                    </div>

                                    <div id="collapse<?= $operadora_id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $operadora_id ?>">
                                        <div class="panel-body" style="border-top: none !important">
                                            <table class="table table-condensed">

                                                <?php
                                                foreach ($operadora['tabelas'] as $produto) { ?>

                                                    <tr style="background-color: #eee; border: solid 1px #ddd">
                                                        <td class="centralizada beneficiarios" style="width: 4%">
                                                            <?= $this->Form->input($produto['id'], ['type' => 'checkbox', 'id' => 'imprimir' . $produto['id'], 'value' => $produto['id'], 'class' => 'noMarginBottom ' . $operadora_id, 'title' => 'Selecionar Para Impresão', 'label' => '']); ?>
                                                        </td>
                                                        <td class="beneficiariosTransparente centralizadaVertical centralizada" colspan="3" style="font-size: 90% !important">


                                                            <b><?= $produto['odonto_produto']['nome'] . "</b> | " . $produto['nome'] . "  " . $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas" ?>

                                                        </td>
                                                    </tr>
                                                    <tr style="border: solid 1px #ddd">
                                                        <td class="centralizada beneficiariosTransparente">
                                                        </td>
                                                        <td class="beneficiariosTransparente  centralizada centralizadaVertical" style="font-size: 90% !important">
                                                            <?php if ($produto['atualizacao'] == 1) : ?>
                                                                <strong>Tabela em Atualização de Preço</strong>
                                                            <?php else : ?>
                                                                <?= $this->Number->currency($produto['preco_vida'], "BRL") ?>
                                                                <br>
                                                                por vida
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="beneficiariosTransparente  centralizada centralizadaVertical" style="font-size: 90% !important">
                                                            <?= $odonto_calculo['quantidade_vidas'] ?>
                                                            <br>
                                                            vida(s)
                                                        </td>
                                                        <td class="beneficiariosTransparente centralizada centralizadaVertical negrito" style="font-size: 90% !important">
                                                            <?php if ($produto['atualizacao'] == 1) : ?>
                                                                <strong>Tabela em Atualização de Preço</strong>
                                                            <?php else : ?>
                                                                <?= $this->Number->currency($odonto_calculo['quantidade_vidas'] * $produto['preco_vida'], "BRL") ?>
                                                                <br>
                                                                <b>Total</b>
                                                            <?php endif; ?>
                                                        </td>


                                                    </tr>
                                                <?php
                                                }
                                                ?>
                                                </tr>


                                            </table>
                                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                                FORMAS DE PAGAMENTOS
                                            </div>
                                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                                <?= ($operadora['dados']['odonto_formas_pagamentos'] != null) ? nl2br($operadora['dados']['odonto_formas_pagamentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                                            </div>
                                            <div class="clearfix">&nbsp;</div>
                                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                                OBSERVAÇÕES IMPORTANTES
                                            </div>

                                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                                <?= ($operadora['dados']['odonto_observacaos'] != null) ? nl2br($operadora['dados']['odonto_observacaos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                                            </div>
                                            <div class="clearfix">&nbsp;</div>
                                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                                DEPENDENTES
                                            </div>

                                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                                <?= ($operadora['dados']['odonto_dependentes'] != null) ? nl2br($operadora['dados']['odonto_dependentes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                                            </div>
                                            <div class="clearfix">&nbsp;</div>

                                            <?php
                                            if (isset($operadora['dados']['url_rede']) && $operadora['dados']['url_rede'] <> null) {
                                                $linkRedeCompleta = "- Para rede completa " . $this->Html->link('clique aqui', $operadora['dados']['url_rede'], ['target' => "_blank"]) . ".";
                                            } else {
                                                $linkRedeCompleta = "";
                                            }
                                            ?>
                                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                                REDE REFERENCIADA (Resumo) <span><?= $linkRedeCompleta ?></span>
                                            </div>

                                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                                <?= ($operadora['dados']['odonto_redes'] != null) ? nl2br($operadora['dados']['odonto_redes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                                            </div>

                                            <div class="clearfix">&nbsp;</div>

                                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                                OPCIONAIS, DIFERENCIAIS E REEMBOLSOS (Resumo)
                                            </div>
                                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                                <?= ($operadora['dados']['odonto_reembolsos'] != null) ? nl2br($operadora['dados']['odonto_reembolsos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                                            </div>
                                            <div class="clearfix">&nbsp;</div>
                                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                                CARÊNCIAS (Resumo)
                                            </div>
                                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                                <?= ($operadora['dados']['odonto_carencias'] != null) ? nl2br($operadora['dados']['odonto_carencias'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                                            </div>
                                            <div class="clearfix">&nbsp;</div>

                                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                                ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
                                            </div>
                                            <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 100%;max-width: 100% !important">
                                                <?php
                                                $areas = array();
                                                foreach ($operadora['tabelas'] as $tabela) {
                                                    foreach ($tabela['odonto_areas_comercializaco']['municipios'] as $municipio) {
                                                        $areas[$tabela['id']]['nome'] = $tabela['nome'] . ' - ' . $tabela['descricao'];
                                                        $areas[$tabela['id']]['atendimento'] = $tabela['odonto_atendimento']['nome'];
                                                        $areas[$tabela['id']]['estados'][$municipio['estado']['id']]['nome'] =  $municipio['estado']['nome'];
                                                        $areas[$tabela['id']]['estados'][$municipio['estado']['id']]['municipios'][] =  $municipio['nome'];
                                                    }
                                                }
                                                ?>
                                                <div class="fonteReduzida" style="margin-left: 10px;">
                                                    <?php foreach ($areas as $area) : ?>
                                                        <strong><?= $area['nome'] ?></strong>
                                                        <br>
                                                        <div style="margin-left: 10px; border-bottom: 1px solid #cccccc;">
                                                            <b>COMERCIALIZAÇÃO </b>
                                                            </br>
                                                            <?php foreach ($area['estados'] as $estado) : ?>
                                                                <strong><?= $estado['nome'] ?>:</strong>
                                                                <?php foreach ($estado['municipios'] as $municipio) : ?>
                                                                    <?= $municipio ?>,
                                                                <?php endforeach; ?>
                                                                </br>
                                                            <?php endforeach; ?>
                                                            <br>
                                                            <b>ATENDIMENTO:</b> <?= $area['atendimento'] ?>
                                                            </br>
                                                        </div>
                                                        </br>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>

                                            <div class="clearfix">&nbsp;</div>
                                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                                DOCUMENTOS NECESSÁRIOS
                                            </div>
                                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                                <?= ($operadora['dados']['odonto_documentos'] != null) ? nl2br($operadora['dados']['odonto_documentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        <?php
                        }
                        echo $this->Form->end();
                        ?>


                        <div class="centralizada">
                            <?php
                            foreach ($operadoras as $operadora) {
                                if (!in_array($operadora['id'], array_keys($tabelas_ordenadas))) {
                                    if ($operadora["status"] != "OCULTA" && $operadora["status"] != "INATIVA") {
                            ?>
                                        <div class="col-xs-12 alert alert-secondary" style="margin: 5px 0; border: 1px solid #ddd;">
                                            <div class="col-md-4 col-xs-12 vermais" style="font-size: 12px;text-align: left; padding-left: 50px !important;">
                                                <b>
                                                    <?php if ($operadora_id === 'BRADESCO') { ?>
                                                        <span class="text-danger">OPERADORA EM PROCESSO DE ATUALIZAÇÃO</span>

                                                    <?php
                                                    } else {
                                                        switch ($operadora["status"]) {
                                                            case 'EM ATUALIZAÇÃO':
                                                                $msg = "OPERADORA EM PROCESSO DE ATUALIZAÇÃO";
                                                                break;
                                                            default:
                                                                $msg = "SEM OPÇÕES PARA ESTE PERFIL DE CLIENTE";
                                                                break;
                                                        }
                                                    ?>
                                                        <span class="text-danger"><?= $msg ?></span>
                                                    <?php } ?>
                                                </b>
                                            </div>

                                            <div class="col-md-2 col-xs-12 vermais" style="font-size: 14px; text-align: center; padding-left: 1%;">
                                                <a class="controle" href="<?= $operadora['url'] ?>" target="_blank"> Ver Material de Vendas</a>
                                            </div>

                                            <div class="col-md-6 col-xs-12" operadora-id='<?= $operadora['id'] ?>'>
                                                <?= $this->Html->image("../" . $operadora['imagen']['caminho'] . $operadora['imagen']['nome'], ['class' => 'logoSimulacaoReduzido']); ?>
                                            </div>
                                        </div>
                            <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                        <?= $this->element("link-cotacao"); ?>
                    </div>
                </div>

            <?php } else {
            ?>
                <div class="clearfix">&nbsp;</div>
                <div class="alert alert-danger centralizada" role="alert"> Nenhuma tabela encontrada dentro dos Filtros selecionados</div>

            <?php } ?>
        </div>

        <?= $this->element('avisos'); ?>
        <div id="aviso" class="col-xs-12 centralizada" style="display: none;">
            <div class="text-danger">Selecione os cálculos que deseja exibir na cotação</div>
        </div>
    </div>
</div>
<?php echo $this->element('forms/buttonsFechar') ?>
<script>
    $(".todas").click(function() {
        $('.' + (this).value).prop('checked', this.checked);
    });
</script>