<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Cálculos']); ?>
<?= $this->Form->create('operadoras', ['id' => 'operadoras', 'url' => ['action' => 'salvaFiltros']]); ?>
<?= $this->Form->hidden('calculo_id', ['value' => $odonto_calculo['id']]); ?>
<div id="resultado">
    <?php if (isset($tabelas_ordenadas) && !empty($tabelas_ordenadas)) : ?>
        <!-- INICIO CABEÇALHO -->
        <div class="well well-sm col-xs-12" style="padding: 5px; min-height: 75px;margin: 5px 0 !important;" id="dadosCalculo">
            <?php
            if (isset($cotacao['nome_titular'])) {
                $titular = "Nome Titular: " . $cotacao['nome_titular'];
                $classe = "col-md-3";
            } else {
                $classe = "col-md-4";
            }
            ?>
            <div class="col-md-4 col-xs-12" style="text-align: left;padding-left: 0 !important;">
                <div class="col-xs-12">
                    Cálculo Nº <a href="#" id="btnModalEdit" simulacao_id="<?= $cotacao['id'] ?>"><span><?= $cotacao['id'] . "   " ?> - <u>EDITAR SIMULAÇÃO</u></span></a>
                </div>
                <div class="col-xs-12">
                    Total de vidas: <b><?= $cotacao['quantidade_vidas'] ?></b>
                </div>
                <div class="col-xs-12">
                    Cliente: <b><?= $cotacao['nome'] ?></b>
                </div>
            </div>

            <div class="col-md-8 col-xs-12" style="text-align: left;">
                <div class="col-xs-12" style="padding-left: 0 !important;">
                    <b class="tabulacao"><?= $this->Number->currency($cotacao->quantidade_vidas * $menor_preco->preco_vida) ?></b>Menor Preço <small>
                        (<?= $menor_preco['odonto_areas_comercializaco']['odonto_operadora']['nome'] ?>)</small>
                </div>

                <div class="col-xs-12" style="padding-left: 0 !important;">
                    <strong class="tabulacao"><?= $this->Number->currency($cotacao->quantidade_vidas * $intermediario->preco_vida) ?></strong>
                    Preço Intermediário <small>(<?= $intermediario['odonto_areas_comercializaco']['odonto_operadora']['nome'] ?>)</small>
                </div>

                <div class="col-xs-12" style="padding-left: 0 !important;">
                    <b class="tabulacao"><?= $this->Number->currency($cotacao->quantidade_vidas * $maior_preco->preco_vida) ?></b><span>Maior Preço <small>
                            (<?= $maior_preco['odonto_areas_comercializaco']['odonto_operadora']['nome'] ?>)</small></span>
                </div>
            </div>
        </div>
        <!-- FIM CABEÇALHO -->

        <!-- INICIO OPERADORAS E TABELAS -->
        <div class="panel-group listaCalculos" role="tablist" aria-multiselectable="true">
            <?php foreach ($tabelas_ordenadas as $operadora_id => $operadora) :
                $numero_tabelas = count($operadora['tabelas']); ?>

                <div class="panel panel-default col-xs-12" style="margin: 5px 0;">
                    <div class="panel-heading" role="tab" id="heading<?= $operadora_id ?>" style="min-height: 100px; background-color: #fff !important;">
                        <!-- ABRIR CALCULOS -->
                        <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                            <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $operadora_id ?>" aria-expanded="true" aria-controls="collapse<?= $operadora_id ?>">
                                <i class="fas fa-chevron-down" aria-hidden="true" style="font-size: 110% !important;"></i>
                                <span style="font-size: 14px !important;">Abrir <?= $numero_tabelas ?> cálculo(s)</span>
                            </a>
                        </div>

                        <!-- EXIBIR TODOS NO PDF -->
                        <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                            <label for="checkAll<?= $operadora_id ?>" style="color: #337ab7; display: inline !important">
                                <?= $this->Form->checkbox('published', ['hiddenField' => false, 'id' => 'checkAll' . $operadora_id, 'class' => 'todas', 'name' => 'checkAll' . $operadora_id, 'value' => $operadora_id, 'style' => 'color: #23527c']); ?> <span style="font-size: 14px;">Exibir todos no cálculo</span>
                            </label>
                        </div>

                        <!-- MATERIAL DE VENDAS -->
                        <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                            <a class="controle" href="<?= $operadora['operadora']['url'] ?>" target="_blank" style="font-size: 14px !important;"> Ver Material de Vendas</a>
                        </div>


                        <!-- LOGO OPERADORA -->
                        <div class="col-xs-12 col-md-6 centralizada">
                            <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $operadora_id ?>" aria-expanded="true" aria-controls="collapse<?= $operadora_id ?>">
                                <?= !is_null($operadora['operadora']['imagen']) ? $this->Html->image("../" . $operadora['operadora']['imagen']['caminho'] . "/" . $operadora['operadora']['imagen']['nome'], ['class' => 'logoSimulacao']) : $operadora['operadora']['nome'] ?>
                                <p class='detalheOperadora'><?= $operadora['operadora']['detalhe'] ?></p>
                            </a>
                        </div>
                    </div> <!-- /PANEL HEADING -->

                    <div id="collapse<?= $operadora_id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $operadora_id ?>">
                        <div class="panel-body" style="border-top: none !important">
                            <table class="table table-condensed">
                                <?php foreach ($operadora['tabelas'] as $tabela) : ?>
                                    <tr style="background-color: #ccc; border: solid 1px #ddd;">
                                        <td class="centralizada" style="width: 4%;">
                                            <?= $this->Form->input($tabela['id'], ['type' => 'checkbox', 'id' => 'imprimir' . $tabela['id'], 'value' => $tabela['id'], 'class' => 'noMarginBottom ' . $operadora_id, 'title' => 'Selecionar Para Impresão', 'label' => '']); ?>
                                        </td>
                                        <td class="beneficiariosTransparente centralizadaVertical centralizada" colspan="3">
                                            <?= '<strong>' . $tabela->nome . '</strong> | ' . $tabela->descricao . ' ' .    $tabela->minimo_vidas . ' à ' . $tabela->maximo_vidas ?>
                                        </td>
                                    </tr>

                                    <tr style="border: solid 1px #ddd">
                                        <td>&nbsp;</td>
                                        <td class='beneficiariosTransparente centralizadaVertical'>
                                            <?php if ($tabela->preco_vida == 0) : ?>
                                                ***
                                            <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                                <strong>Tabela em Atualização de Preço</strong>
                                            <?php else : ?>
                                                <?= $this->Number->format($tabela->preco_vida, ['places' => 2, "before" => "R$ "]) ?>
                                                <br />
                                                <span style="font-size: 100%;">
                                                    por vida
                                                </span>
                                            <?php endif ?>
                                        </td>

                                        <td class="beneficiariosTransparente  centralizada centralizadaVertical">
                                            <?php if ($tabela->preco_vida == 0) : ?>
                                                ***
                                            <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                                <strong>Tabela em Atualização de Preço</strong>
                                            <?php else : ?>
                                                <?= $cotacao->quantidade_vidas ?>
                                                <br />
                                                <span style="font-size: 100%;">
                                                    vida(s)
                                                </span>
                                            <?php endif ?>
                                        </td>

                                        <td class="beneficiariosTransparente  centralizada centralizadaVertical">
                                            <?php if ($tabela->preco_vida == 0) : ?>
                                                ***
                                            <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                                <strong>Tabela em Atualização de Preço</strong>
                                            <?php else : ?>
                                                <strong>
                                                    <?= $this->Number->format($cotacao->quantidade_vidas * $tabela->preco_vida, ['places' => 2, "before" => "R$ "]) ?>
                                                    <br />
                                                    <span style="font-size: 100%;">
                                                        Total
                                                    </span>
                                                </strong>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </table>
                            <?php if (!empty($operadora['grupo_entidades'])) : ?>
                                <div class="nobreak">
                                    <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                        ENTIDADES E PROFISSÕES
                                    </div>
                                    <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                                        <?php foreach ($operadora['grupo_entidades'] as $key => $value) :
                                            if (!empty($value)) : ?>
                                                <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                                </br></br>
                                                <?= !empty($value['data']) ? $value['data'] : "<div class='info'>Consulte Operadora</div>" ?>
                                                <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                                            <?php else : ?>
                                                <div class='info'>Consulte Operadora</div>
                                        <?php endif;
                                        endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="clearfix">&nbsp;</div>
                            <div class="nobreak">
                                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                    OBSERVAÇÕES IMPORTANTES
                                </div>
                                <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                                    <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong>
                                    <br><br>
                                    <?php foreach ($operadora['observacoes'] as $key => $value) :
                                        if (!empty($key)) : ?>
                                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                            </br></br>
                                            <?= $value['data'] ?>
                                            <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                                        <?php else : ?>
                                            <div class='info'>Consulte Operadora</div>
                                    <?php endif;
                                    endforeach; ?>
                                </div>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="nobreak">
                                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                    REDE REFERENCIADA (Resumo) <span><?= (isset($operadora['operadora']['url_rede']) && !is_null($operadora['operadora']['url_rede'])) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['operadora']['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
                                </div>
                                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                    <?php foreach ($operadora['redes'] as $key => $value) :
                                        if (!empty($key)) : ?>
                                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                            </br></br>
                                            <?= $value['data'] ?>
                                            <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                                        <?php else : ?>
                                            <div class='info'>Consulte Operadora</div>
                                    <?php endif;
                                    endforeach; ?>
                                </div>

                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="nobreak">

                                <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                                </div>

                                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                    <?php foreach ($operadora['reembolsos'] as $key => $value) :
                                        if (!empty($key)) : ?>
                                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                            </br></br>
                                            <?= $value['data'] ?>
                                            <?= count($operadora['reembolsos']) > 1 ? '<hr>' : '' ?>
                                        <?php else : ?>
                                            <div class='info'>Consulte Operadora</div>
                                    <?php endif;
                                    endforeach; ?>
                                </div>

                            </div>

                            <div class="clearfix">&nbsp;</div>

                            <div class="nobreak">
                                <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                    ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
                                </div>

                                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                    <strong>Areas de Atendimento</strong>
                                    <br>
                                    <?php foreach ($operadora['atendimentos'] as $tipo => $tabelas) {
                                        echo '<strong>' . $tipo  . '</strong>: ' . $tabelas . '<br>';
                                    } ?>
                                    <br>
                                    <strong>Areas de Comercialização</strong>
                                    <br>
                                    <?php foreach ($operadora['areas_comercializacoes'] as $key => $value) :
                                        if (!empty($key)) : ?>
                                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                            </br></br>
                                            <?= substr($value['data'], 0, -2) . '.' ?>
                                            <?= count($operadora['areas_comercializacoes']) > 1 ? '<hr>' : '' ?>
                                        <?php else : ?>
                                            <div class='info'>Consulte Operadora</div>
                                    <?php endif;
                                    endforeach; ?>
                                </div>

                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="nobreak">

                                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;">
                                    CARÊNCIAS (Resumo)
                                </div>
                                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                    <?php foreach ($operadora['carencias'] as $key => $value) :
                                        if (!empty($key)) : ?>
                                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                            </br></br>
                                            <?= $value['data'] ?>
                                            <?= count($operadora['carencias']) > 1 ? '<hr>' : '' ?>
                                        <?php else : ?>
                                            <div class='info'>Consulte Operadora</div>
                                    <?php endif;
                                    endforeach; ?>
                                </div>
                            </div>


                            <div class="clearfix">&nbsp;</div>
                            <div class="nobreak">
                                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                    DEPENDENTES
                                </div>
                                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                    <?php foreach ($operadora['dependentes'] as $key => $value) :
                                        if (!empty($key)) : ?>
                                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                            </br></br>
                                            <?= $value['data'] ?>
                                            <?= count($operadora['dependentes']) > 1 ? '<hr>' : '' ?>
                                        <?php else : ?>
                                            <div class='info'>Consulte Operadora</div>
                                    <?php endif;
                                    endforeach; ?>
                                </div>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="nobreak">
                                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                                    DOCUMENTOS NECESSÁRIOS
                                </div>
                                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                    <?php foreach ($operadora['documentos'] as $key => $value) :
                                        if (!empty($key)) : ?>
                                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                            </br></br>
                                            <?= $value['data'] ?>
                                            <?= count($operadora['documentos']) > 1 ? '<hr>' : '' ?>
                                        <?php else : ?>
                                            <div class='info'>Consulte Operadora</div>
                                    <?php endif;
                                    endforeach; ?>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="nobreak">
                                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                    FORMAS DE PAGAMENTO
                                </div>
                                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                    <?php foreach ($operadora['pagamentos'] as $key => $value) :
                                        if (!empty($key)) : ?>
                                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                            </br></br>
                                            <?= $value['data'] ?>
                                            <?= count($operadora['pagamentos']) > 1 ? '<hr>' : '' ?>
                                        <?php else : ?>
                                            <div class='info'>Consulte Operadora</div>
                                    <?php endif;
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php foreach ($operadoras as $operadora) :
                if (!in_array($operadora->id, array_keys($tabelas_ordenadas))) : ?>
                    <div class="col-xs-12 alert alert-secondary" style="margin: 5px 0; border: 1px solid #ddd;">
                        <div class="col-md-4 col-xs-12 vermais" style="font-size: 12px;text-align: left; padding-left: 50px !important;">
                            <span class="text-danger"><?= $operadora->status == 'EM ATUALIZAÇÃO' ? 'OPERADORA EM PROCESSO DE ATUALIZAÇÃO' : 'SEM OPÇÕES PARA ESTE PERFIL DE CLIENTE' ?></span>
                        </div>
                        <div class="col-md-2 col-xs-12 vermais" style="font-size: 14px; text-align: center; padding-left: 1%;">
                            <a class="controle" href="<?= $operadora->url ?>" target="_blank"> Ver Material de Vendas</a>
                        </div>
                        <div class="col-md-6 col-xs-12 text-center" operadora-id='<?= $operadora->id ?>'>
                            <?= !is_null($operadora['imagen']) ? $this->Html->image("../" . $operadora['imagen']['caminho'] . "/" . $operadora['imagen']['nome'], ['class' => 'logoSimulacao']) : $operadora['nome'] ?>
                        </div>
                    </div>
            <?php endif;
            endforeach; ?>
        </div>
        <?= $this->element("link-cotacao") ?>

    <?php else : ?>
        <div class="clearfix">&nbsp;</div>
        <div class="alert alert-danger centralizada" role="alert"> Nenhuma tabela encontrada dentro dos Filtros selecionados</div>

    <?php endif; ?>
    <div class="spacer-md">&nbsp;</div>
</div>
<?= $this->Form->end(); ?>
<?php echo $this->element('forms/buttonsFechar') ?>

<script>
    $(".todas").click(function() {
        $('.' + (this).value).prop('checked', this.checked);
    });
</script>