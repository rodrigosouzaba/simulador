<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Odonto Calculos Tabelas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Calculos'), ['controller' => 'OdontoCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Calculo'), ['controller' => 'OdontoCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoCalculosTabelas form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoCalculosTabela) ?>
    <fieldset>
        <legend><?= __('Add Odonto Calculos Tabela') ?></legend>
        <?php
            echo $this->Form->input('odonto_calculo_id', ['options' => $odontoCalculos, 'empty' => true]);
            echo $this->Form->input('odonto_tabela_id', ['options' => $odontoTabelas, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
