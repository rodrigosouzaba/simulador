<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Odonto Calculos Tabela'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Calculos'), ['controller' => 'OdontoCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Calculo'), ['controller' => 'OdontoCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoCalculosTabelas index large-9 medium-8 columns content">
    <h3><?= __('Odonto Calculos Tabelas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('odonto_calculo_id') ?></th>
                <th><?= $this->Paginator->sort('odonto_tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($odontoCalculosTabelas as $odontoCalculosTabela): ?>
            <tr>
                <td><?= $this->Number->format($odontoCalculosTabela->id) ?></td>
                <td><?= $odontoCalculosTabela->has('odonto_calculo') ? $this->Html->link($odontoCalculosTabela->odonto_calculo->id, ['controller' => 'OdontoCalculos', 'action' => 'view', $odontoCalculosTabela->odonto_calculo->id]) : '' ?></td>
                <td><?= $odontoCalculosTabela->has('odonto_tabela') ? $this->Html->link($odontoCalculosTabela->odonto_tabela->id, ['controller' => 'OdontoTabelas', 'action' => 'view', $odontoCalculosTabela->odonto_tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $odontoCalculosTabela->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $odontoCalculosTabela->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $odontoCalculosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoCalculosTabela->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
