<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Calculos Tabela'), ['action' => 'edit', $odontoCalculosTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Calculos Tabela'), ['action' => 'delete', $odontoCalculosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoCalculosTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Calculos Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Calculos Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Calculos'), ['controller' => 'OdontoCalculos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Calculo'), ['controller' => 'OdontoCalculos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoCalculosTabelas view large-9 medium-8 columns content">
    <h3><?= h($odontoCalculosTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Odonto Calculo') ?></th>
            <td><?= $odontoCalculosTabela->has('odonto_calculo') ? $this->Html->link($odontoCalculosTabela->odonto_calculo->id, ['controller' => 'OdontoCalculos', 'action' => 'view', $odontoCalculosTabela->odonto_calculo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Tabela') ?></th>
            <td><?= $odontoCalculosTabela->has('odonto_tabela') ? $this->Html->link($odontoCalculosTabela->odonto_tabela->id, ['controller' => 'OdontoTabelas', 'action' => 'view', $odontoCalculosTabela->odonto_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoCalculosTabela->id) ?></td>
        </tr>
    </table>
</div>
