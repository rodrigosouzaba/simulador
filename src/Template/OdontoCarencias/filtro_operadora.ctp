<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('id') ?></th>-->
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($odonto_carencias as $carencia) : ?>
            <tr>
                <td><?= $carencia->nome ?></td>
                <td><?= $carencia->descricao ?></td>
                <td>
                    <?php
                    if (isset($carencia->odonto_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $carencia->odonto_operadora->imagen->caminho . "/" . $carencia->odonto_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $carencia->odonto_operadora->detalhe;
                    } else {
                        echo $carencia->odonto_operadora->nome . " - " . $carencia->odonto_operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?php if ($sessao['role'] == 'admin') { ?>
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/odontoCarencias/edit/$carencia->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Carência', 'sid' => 'odontoCarencias.edit']) ?>
                        <?= $this->Form->postLink('', "/odontoCarencias/delete/$carencia->id", ['confirm' => __('Confirma exclusão da Carência?', $carencia->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'odontoCarencias.delete'])
                        ?>
                    <?php } ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
