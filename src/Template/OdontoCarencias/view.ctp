<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Carencia'), ['action' => 'edit', $odontoCarencia->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Carencia'), ['action' => 'delete', $odontoCarencia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoCarencia->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Carencias'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Carencia'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Operadoras'), ['controller' => 'OdontoOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Operadora'), ['controller' => 'OdontoOperadoras', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Produtos'), ['controller' => 'OdontoProdutos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Produto'), ['controller' => 'OdontoProdutos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoCarencias view large-9 medium-8 columns content">
    <h3><?= h($odontoCarencia->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($odontoCarencia->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Operadora') ?></th>
            <td><?= $odontoCarencia->has('odonto_operadora') ? $this->Html->link($odontoCarencia->odonto_operadora->id, ['controller' => 'OdontoOperadoras', 'action' => 'view', $odontoCarencia->odonto_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoCarencia->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($odontoCarencia->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Odonto Produtos') ?></h4>
        <?php if (!empty($odontoCarencia->odonto_produtos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Odonto Operadora Id') ?></th>
                <th><?= __('Odonto Carencia Id') ?></th>
                <th><?= __('Odonto Rede Id') ?></th>
                <th><?= __('Odonto Reembolso Id') ?></th>
                <th><?= __('Odonto Documento Id') ?></th>
                <th><?= __('Odonto Dependente Id') ?></th>
                <th><?= __('Odonto Observacao Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($odontoCarencia->odonto_produtos as $odontoProdutos): ?>
            <tr>
                <td><?= h($odontoProdutos->id) ?></td>
                <td><?= h($odontoProdutos->nome) ?></td>
                <td><?= h($odontoProdutos->descricao) ?></td>
                <td><?= h($odontoProdutos->odonto_operadora_id) ?></td>
                <td><?= h($odontoProdutos->odonto_carencia_id) ?></td>
                <td><?= h($odontoProdutos->odonto_rede_id) ?></td>
                <td><?= h($odontoProdutos->odonto_reembolso_id) ?></td>
                <td><?= h($odontoProdutos->odonto_documento_id) ?></td>
                <td><?= h($odontoProdutos->odonto_dependente_id) ?></td>
                <td><?= h($odontoProdutos->odonto_observacao_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OdontoProdutos', 'action' => 'view', $odontoProdutos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OdontoProdutos', 'action' => 'edit', $odontoProdutos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OdontoProdutos', 'action' => 'delete', $odontoProdutos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoProdutos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
