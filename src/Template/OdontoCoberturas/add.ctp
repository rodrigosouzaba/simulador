<div class="col-xs-12">
    <?= $this->Form->create($odontoCobertura) ?>
    <fieldset>
        <legend><?= __('Add Odonto Cobertura') ?></legend>
        <?php
        echo $this->Form->input('nome');
        echo $this->Form->input('prioridade');
        ?>
    </fieldset>
    <?= $this->Form->button('Enviar', ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
