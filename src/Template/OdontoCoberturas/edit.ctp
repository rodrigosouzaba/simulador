<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $odontoCobertura->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $odontoCobertura->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Odonto Coberturas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="odontoCoberturas form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoCobertura) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Cobertura') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('prioridade');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
