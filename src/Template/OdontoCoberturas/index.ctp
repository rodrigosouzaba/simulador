<div class="col-md-3 fonteReduzida">
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Cobertura', '/odontoCoberturas/add', ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]); ?>
</div>
<div class="col-xs-12">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('prioridade') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($odontoCoberturas as $odontoCobertura) : ?>
                <tr>
                    <td><?= $this->Number->format($odontoCobertura->id) ?></td>
                    <td><?= h($odontoCobertura->nome) ?></td>
                    <td><?= $this->Number->format($odontoCobertura->prioridade) ?></td>
                    <td class="actions" style="width: 5%">
                        <div class="btn-group" >
                            <button type="button" class="btn btn-default btn-icon-toggle dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i></button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><?= $this->Html->link('<i class="fa fa-edit"></i> Edit', '/odontoCoberturas/edit/'.$odontoCobertura->id, ['sid' => 'odontoCoberturas.edit', 'escape' => false]) ?></li>
                                <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> Delete', '/odontoCoberturas/delete/',  ['escape' => false,'confirm' => __('Are you sure you want to delete # {0}?', $odontoCobertura->id), 'sid' => 'odontoCoberturas.delete']) ?></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
