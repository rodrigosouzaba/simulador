<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Cobertura'), ['action' => 'edit', $odontoCobertura->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Cobertura'), ['action' => 'delete', $odontoCobertura->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoCobertura->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Coberturas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Cobertura'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoCoberturas view large-9 medium-8 columns content">
    <h3><?= h($odontoCobertura->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($odontoCobertura->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoCobertura->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prioridade') ?></th>
            <td><?= $this->Number->format($odontoCobertura->prioridade) ?></td>
        </tr>
    </table>
</div>
