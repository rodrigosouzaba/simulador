<?php $bloqueador = $this->request->action == "add" ? true : false; ?>

<div class="col-xs-12">
    <?= $this->Form->create($odontoComercializaco) ?>


    <div class="col-xs-12 fonteReduzida">
        <?= $this->Form->input('nome'); ?>
    </div>
    <div class="col-xs-4 fonteReduzida">
        <?= $this->Form->input('estado_id', ['options' => $estados, 'empty' => 'SELECIONE']); ?>
    </div>
    <div class="col-xs-4 fonteReduzida">
        <?= $this->Form->input('tipo_pessoa', ['options' => ['PF' => 'Saúde Pessoa Física', 'PJ' => 'Saúde Pessoa Jurídica'], 'empty' => 'SELECIONE', 'disabled' => $bloqueador]); ?>
    </div>
    <div class="col-md-4 fonteReduzida" id="operadoras">
        <?= $this->Form->input('odonto_operadora_id', ['options' => $odontoOperadoras, 'empty' => 'SELECIONE', 'label' => 'Operadora Odonto', 'disabled' => $bloqueador]); ?>
    </div>
    <div class="clearfix">&nbsp</div>

    <div id="local_municipios" class="col-md-offset-2 col-md-8 fonteReduzida">
        <select multiple='multiple' class="multiselect" name='lista_municipios_escolhidos'>
            <?php foreach ($municipios as $municipio) : ?>
                <option <?= (isset($municipios_tabela) ? (key_exists($municipio->id, $municipios_tabela) ? "selected" : "") : "") ?> value="<?= $municipio->id; ?>"><?= $municipio->nome; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<div class="clearfix">&nbsp</div>
<div class="col-xs-12 fonteReduzida">
    <?= $this->Form->input('descricao', ['label' => 'Observação']); ?>
</div>

<?= $this->element('botoesAdd') ?>
<?= $this->Form->end() ?>
</div>
<script>
    $("#estado-id").change(function() {
        let estado = $(this).val();

        $("#tipo-pessoa").prop("disabled", false);

        //GET MUNICIPIOS
        $.ajax({
            type: "GET",
            url: baseUrl + "odontoComercializacoes/getMunicipios/" + estado,
            success: function(data) {
                $("#local_municipios").empty();
                $("#local_municipios").append(data);
                $("#local_municipios").show();
            }
        });
    });

    $("#tipo-pessoa").change(function() {
        var estado = $("#estado-id").val();

        //GET OPERADORAS
        $.ajax({
            type: 'POST',
            url: baseUrl + "odontoComercializacoes/filtroOperadoras/" + estado + "/" + $(this).val(),
            success: function(data) {
                $("#operadoras").empty();
                $("#operadoras").append(data);
            }
        })
    });
</script>