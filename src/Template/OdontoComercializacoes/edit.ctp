<div class="odontoComercializacoes form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoComercializaco) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Comercializaco') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('estado_id', ['options' => $estados]);
            echo $this->Form->input('odonto_operadora_id', ['options' => $odontoOperadoras, 'empty' => true]);
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
