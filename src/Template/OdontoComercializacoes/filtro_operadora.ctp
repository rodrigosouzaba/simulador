<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Nome' ?></th>
            <th><?= 'Estado' ?></th>
            <th><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($odonto_comercializacoes as $comercializacao) : ?>
            <tr>
                <td><?= $comercializacao->nome ?></td>
                <td><?= $comercializacao->estado->nome ?></td>
                <td><?= $comercializacao->descricao ?></td>
                <td>
                    <?php
                    if (isset($comercializacao->odonto_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $comercializacao->odonto_operadora->imagen->caminho . "/" . $comercializacao->odonto_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $comercializacao->odonto_operadora->detalhe;
                    } else {
                        echo $comercializacao->odonto_operadora->nome . " - " . $comercializacao->odonto_operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/odontoComercializacoes/edit/$comercializacao->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Carência', 'sid' => 'odontoComercializacoes.edit']) ?>
                    <?= $this->Form->postLink('', "/odontoComercializacoes/delete/$comercializacao->id", ['confirm' => __('Confirma exclusão?', $comercializacao->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'odontoComercializacoes.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
