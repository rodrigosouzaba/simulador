<select multiple='multiple' class="multiselect" name='lista_municipios_escolhidos[]'>
        <?php foreach ($municipios as $key => $municipio) : ?>
                <option value="<?= $key; ?>"><?= $municipio; ?></option>
        <?php endforeach; ?>
</select>
<script>
        $('.multiselect').multiSelect({
                keepOrder: false,
                selectableHeader: "<input type='text' class='search-input form-control' style='margin-bottom:2px;' autocomplete='off' placeholder='Digite para filtrar'>",
                selectionHeader: "<input type='text' class='search-input form-control' style='margin-bottom:2px;' autocomplete='off' placeholder='Digite para filtrar'>",
                afterInit: function(ms) {
                        var that = this,
                                $selectableSearch = that.$selectableUl.prev(),
                                $selectionSearch = that.$selectionUl.prev(),
                                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                                .on('keydown', function(e) {
                                        if (e.which === 40) {
                                                that.$selectableUl.focus();
                                                return false;
                                        }
                                });

                        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                                .on('keydown', function(e) {
                                        if (e.which == 40) {
                                                that.$selectionUl.focus();
                                                return false;
                                        }
                                });
                },
                afterSelect: function() {
                        this.qs1.cache();
                        this.qs2.cache();
                },
                afterDeselect: function() {
                        this.qs1.cache();
                        this.qs2.cache();
                }
        });
</script>