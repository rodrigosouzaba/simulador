<?php
$tipo_pessoa = ['PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica'];
?>
<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Área de Comercialização', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
    ?>
</div>

<div class="col-md-2 fonteReduzida">
    <?= $this->Form->input('estados', ['label' => '', 'options' => $estados, 'empty' => 'Selecione ESTADO']); ?>
</div>

<div class="col-md-2 fonteReduzida">
    <?= $this->Form->input('tipo_pessoa', ['label' => '', 'options' => $tipo_pessoa, 'empty' => 'Selecione TIPO PESSOA', 'disabled' => 'true']); ?>
</div>

<div id="operadoras" class="col-md-2">
    <div class="fonteReduzida">
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
    </div>
</div>

<div id="respostaFiltroOperadora"></div>

<script type="text/javascript">
    $("#estados").change(function() {
        let estado = $(this).val();
        if (estado != '') {
            $("#tipo-pessoa").removeAttr('disabled');
        }
    })
    $("#tipo-pessoa").change(function() {
        $.ajax({
            type: "GET",
            url: baseUrl + "odontoComercializacoes/filtroIndex/" + $("#estados").val() + "/" + $("#tipo-pessoa").val(),
            beforeSend: function() {
                $("#loader").click();
            },
            success: function(data) {
                $("#operadoras").empty();
                $("#operadoras").append(data);
                $("#fechar").click();
                $(".modal-backdrop").hide();
            }
        });
    });
</script>
