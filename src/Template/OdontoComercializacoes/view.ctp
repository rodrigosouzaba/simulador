<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Comercializaco'), ['action' => 'edit', $odontoComercializaco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Comercializaco'), ['action' => 'delete', $odontoComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoComercializaco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Comercializacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Comercializaco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Operadoras'), ['controller' => 'OdontoOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Operadora'), ['controller' => 'OdontoOperadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoComercializacoes view large-9 medium-8 columns content">
    <h3><?= h($odontoComercializaco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($odontoComercializaco->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Estado') ?></th>
            <td><?= $odontoComercializaco->has('estado') ? $this->Html->link($odontoComercializaco->estado->id, ['controller' => 'Estados', 'action' => 'view', $odontoComercializaco->estado->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Operadora') ?></th>
            <td><?= $odontoComercializaco->has('odonto_operadora') ? $this->Html->link($odontoComercializaco->odonto_operadora->id, ['controller' => 'OdontoOperadoras', 'action' => 'view', $odontoComercializaco->odonto_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoComercializaco->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($odontoComercializaco->descricao)); ?>
    </div>
</div>
