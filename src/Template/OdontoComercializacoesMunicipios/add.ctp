<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Odonto Comercializacoes Municipios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoComercializacoesMunicipios form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoComercializacoesMunicipio) ?>
    <fieldset>
        <legend><?= __('Add Odonto Comercializacoes Municipio') ?></legend>
        <?php
            echo $this->Form->input('municipio_id', ['options' => $municipios, 'empty' => true]);
            echo $this->Form->input('odonto_tabela_id', ['options' => $odontoTabelas, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
