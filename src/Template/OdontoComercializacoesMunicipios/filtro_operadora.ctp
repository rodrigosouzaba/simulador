 <table cellpadding="0" cellspacing="0" class="col-xs-12">
     <thead>
         <tr>
             <th><?= $this->Paginator->sort('nome') ?></th>
             <!-- <th><?= $this->Paginator->sort('odonto_operadora_id', ['label' => 'Operadora']) ?></th> -->
             <th><?= 'municipios' ?></th>
             <th class="actions"><?= __('Actions') ?></th>
         </tr>
     </thead>
     <tbody>
         <?php foreach ($tabelas as $tabela) : ?>
             <tr>
                 <td><?= h($tabela->nome) ?></td>
                 <td>
                     <span style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap; display: block;">
                         <?php foreach ($tabela->municipios as $municipio) {
                                echo $municipio['nome'] .  ", ";
                            } ?>
                     </span>
                 </td>
                 <td class="actions">
                     <?=
                            $this->Html->link('', ['action' => 'edit', $tabela->id], [
                                'title' => __('Editar'),
                                'class' => 'btn btn-default btn-sm fa fa-pencil',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Editar'
                            ])
                        ?>
                 </td>
             </tr>
         <?php endforeach; ?>
     </tbody>
 </table>