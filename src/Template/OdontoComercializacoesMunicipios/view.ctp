<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Comercializacoes Municipio'), ['action' => 'edit', $odontoComercializacoesMunicipio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Comercializacoes Municipio'), ['action' => 'delete', $odontoComercializacoesMunicipio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoComercializacoesMunicipio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Comercializacoes Municipios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Comercializacoes Municipio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoComercializacoesMunicipios view large-9 medium-8 columns content">
    <h3><?= h($odontoComercializacoesMunicipio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Municipio') ?></th>
            <td><?= $odontoComercializacoesMunicipio->has('municipio') ? $this->Html->link($odontoComercializacoesMunicipio->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $odontoComercializacoesMunicipio->municipio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Tabela') ?></th>
            <td><?= $odontoComercializacoesMunicipio->has('odonto_tabela') ? $this->Html->link($odontoComercializacoesMunicipio->odonto_tabela->id, ['controller' => 'OdontoTabelas', 'action' => 'view', $odontoComercializacoesMunicipio->odonto_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoComercializacoesMunicipio->id) ?></td>
        </tr>
    </table>
</div>
