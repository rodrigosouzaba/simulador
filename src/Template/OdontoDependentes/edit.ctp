<div class="odontoDependentes form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoDependente) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Dependente') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('odonto_operadora_id', ['options' => $odontoOperadoras, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
