<?php if (isset($estados)) : ?>
    <?= $this->Form->input('estados', ['label' => '', 'empty' => 'Selecione ESTADO']); ?>
    <script>
        $("#estados").change(function() {
            let estado = $(this).val();
            let nova = $("#nova").val();
            let tipo = $("#tipo-pessoa").val();
            filtro('estado', {
                nova,
                tipo,
                estado
            })
        })
    </script>
<?php elseif (isset($operadoras)) : ?>
    <?= $this->Form->input('operadora_id', ['label' => '', 'empty' => 'Selecione OPERADORA']); ?>
    <script>
        $("#operadora-id").change(function() {
            let operadora = $(this).val();
            let estado = $("#operadora-id").val();
            let nova = $("#nova").val();
            let tipo = $("#tipo-pessoa").val();
            filtro('operadora', {
                operadora
            })
        })
    </script>
<?php endif; ?>