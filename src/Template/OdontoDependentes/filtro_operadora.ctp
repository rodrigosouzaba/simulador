<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($odonto_dependentes as $dependente) : ?>
            <tr>
                <td><?= $dependente->nome ?></td>
                <td><?= $dependente->descricao ?></td>
                <td>
                    <?php
                    if (isset($dependente->odonto_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $dependente->odonto_operadora->imagen->caminho . "/" . $dependente->odonto_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $dependente->odonto_operadora->detalhe;
                    } else {
                        echo $dependente->odonto_operadora->nome . " - " . $dependente->odonto_operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/odontoDependentes/edit/$dependente->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Carência', 'sid' => 'odontoDependentes.edit']) ?>
                    <?= $this->Form->postLink('', "/odontoDependentes/delete/$dependente->id", ['confirm' => __('Confirma exclusão?', $dependente->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'odontoDependentes.delete' ]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
