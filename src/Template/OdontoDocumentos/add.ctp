<?php $bloqueador = $this->request->action == "add" ? true : false; ?>

<div class="col-xs-12 fonteReduzida">
    <?= $this->Form->create($odontoDocumento) ?>

    <div class="col-xs-12">
        <?= $this->Form->input('nome'); ?>
    </div>
    <div class="col-xs-4" id="estados-field">
        <?= $this->Form->input('nova', ['options' => ['Não', 'Sim'], 'empty' => 'SELECIONE', 'label' => 'Nova']); ?>
    </div>
    <div class="col-xs-4" id="tipo-pessoa-field">
        <?= $this->Form->input('tipo_pessoa', ['options' => ['PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica'], 'empty' => 'SELECIONE', 'label' => 'Tipo Pessoa', 'disabled' => $bloqueador]); ?>
    </div>
    <div class="col-xs-4" id="operadoras-field">
        <?= $this->Form->input('odonto_operadora_id', ['empty' => 'SELECIONE', 'label' => 'Operadora Odonto', 'disabled' => $bloqueador]); ?>
    </div>
    <div class="clearfix">&nbsp</div>
    <div class="col-xs-12">
        <?= $this->Form->input('descricao', ['label' => 'Observação']); ?>
    </div>
    <?= $this->element('botoesAdd') ?>
    <?= $this->Form->end() ?>
</div>
<script>
    $("#nova").change(function() {
        $("#tipo-pessoa").removeAttr("disabled");
    });

    $("#tipo-pessoa").change(function() {
        let estado = $("#estado-id").val();
        let nova = $("#nova").val();
        let tipo = $(this).val();

        $.ajax({
            type: 'GET',
            url: '<?= $this->request->webroot . $this->request->controller ?>/get-operadoras/' + nova + '/' + tipo,
            success: function(data) {
                $("#operadoras-field").empty();
                $("#operadoras-field").append(data);
            }
        });
    });
</script>