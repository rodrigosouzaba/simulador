<div class="odontoDocumentos form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoDocumento) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Documento') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('odonto_operadora_id', ['options' => $odontoOperadoras, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
