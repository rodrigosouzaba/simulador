<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($odonto_documentos as $documento) : ?>
            <tr>
                <td><?= $documento->nome ?></td>
                <td><?= $documento->descricao ?></td>
                <td>
                    <?php
                    if (isset($documento->odonto_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $documento->odonto_operadora->imagen->caminho . "/" . $documento->odonto_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $documento->odonto_operadora->detalhe;
                    } else {
                        echo $documento->odonto_operadora->nome . " - " . $documento->odonto_operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/odontoDocumentos/edit/$documento->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Carência', 'sid' => 'odontoDocumentos.edit']) ?>
                    <?= $this->Form->postLink('', "/odontoDocumentos/delete/$documento->id", ['confirm' => __('Confirma exclusão?', $documento->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'odontoDocumentos.delete'])?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
