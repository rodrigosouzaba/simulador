<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Odonto Filtros'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Calculos'), ['controller' => 'OdontoCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Calculo'), ['controller' => 'OdontoCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoFiltros form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoFiltro) ?>
    <fieldset>
        <legend><?= __('Add Odonto Filtro') ?></legend>
        <?php
            echo $this->Form->input('odonto_calculo_id', ['options' => $odontoCalculos]);
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('tabelas._ids', ['options' => $tabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
