<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Odonto Filtro'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Calculos'), ['controller' => 'OdontoCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Calculo'), ['controller' => 'OdontoCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoFiltros index large-9 medium-8 columns content">
    <h3><?= __('Odonto Filtros') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('odonto_calculo_id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($odontoFiltros as $odontoFiltro): ?>
            <tr>
                <td><?= $this->Number->format($odontoFiltro->id) ?></td>
                <td><?= h($odontoFiltro->created) ?></td>
                <td><?= $odontoFiltro->has('odonto_calculo') ? $this->Html->link($odontoFiltro->odonto_calculo->id, ['controller' => 'OdontoCalculos', 'action' => 'view', $odontoFiltro->odonto_calculo->id]) : '' ?></td>
                <td><?= $odontoFiltro->has('user') ? $this->Html->link($odontoFiltro->user->id, ['controller' => 'Users', 'action' => 'view', $odontoFiltro->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $odontoFiltro->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $odontoFiltro->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $odontoFiltro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoFiltro->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
