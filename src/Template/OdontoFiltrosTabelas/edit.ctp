<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $odontoFiltrosTabela->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $odontoFiltrosTabela->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Odonto Filtros Tabelas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Filtros'), ['controller' => 'OdontoFiltros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Filtro'), ['controller' => 'OdontoFiltros', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoFiltrosTabelas form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoFiltrosTabela) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Filtros Tabela') ?></legend>
        <?php
            echo $this->Form->input('odonto_filtro_id', ['options' => $odontoFiltros]);
            echo $this->Form->input('odonto_tabela_id', ['options' => $odontoTabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
