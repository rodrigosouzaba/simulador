<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Odonto Filtros Tabela'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Filtros'), ['controller' => 'OdontoFiltros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Filtro'), ['controller' => 'OdontoFiltros', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoFiltrosTabelas index large-9 medium-8 columns content">
    <h3><?= __('Odonto Filtros Tabelas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('odonto_filtro_id') ?></th>
                <th><?= $this->Paginator->sort('odonto_tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($odontoFiltrosTabelas as $odontoFiltrosTabela): ?>
            <tr>
                <td><?= $this->Number->format($odontoFiltrosTabela->id) ?></td>
                <td><?= $odontoFiltrosTabela->has('odonto_filtro') ? $this->Html->link($odontoFiltrosTabela->odonto_filtro->id, ['controller' => 'OdontoFiltros', 'action' => 'view', $odontoFiltrosTabela->odonto_filtro->id]) : '' ?></td>
                <td><?= $odontoFiltrosTabela->has('odonto_tabela') ? $this->Html->link($odontoFiltrosTabela->odonto_tabela->id, ['controller' => 'OdontoTabelas', 'action' => 'view', $odontoFiltrosTabela->odonto_tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $odontoFiltrosTabela->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $odontoFiltrosTabela->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $odontoFiltrosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoFiltrosTabela->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
