<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Filtros Tabela'), ['action' => 'edit', $odontoFiltrosTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Filtros Tabela'), ['action' => 'delete', $odontoFiltrosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoFiltrosTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Filtros Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Filtros Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Filtros'), ['controller' => 'OdontoFiltros', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Filtro'), ['controller' => 'OdontoFiltros', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoFiltrosTabelas view large-9 medium-8 columns content">
    <h3><?= h($odontoFiltrosTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Odonto Filtro') ?></th>
            <td><?= $odontoFiltrosTabela->has('odonto_filtro') ? $this->Html->link($odontoFiltrosTabela->odonto_filtro->id, ['controller' => 'OdontoFiltros', 'action' => 'view', $odontoFiltrosTabela->odonto_filtro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Tabela') ?></th>
            <td><?= $odontoFiltrosTabela->has('odonto_tabela') ? $this->Html->link($odontoFiltrosTabela->odonto_tabela->id, ['controller' => 'OdontoTabelas', 'action' => 'view', $odontoFiltrosTabela->odonto_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoFiltrosTabela->id) ?></td>
        </tr>
    </table>
</div>
