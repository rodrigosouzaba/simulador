<div class="odontoFormasPagamentos form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoFormasPagamento) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Formas Pagamento') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('odonto_operadora_id', ['options' => $odontoOperadoras]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
