<?= $this->Form->input('odonto_operadora_id', ['options' => $operadoras, 'label' => '', 'empty' => 'Selecione a OPERADORA', 'id' => 'odonto-operadora-id']); ?>
<div>

</div>
<script type="text/javascript">
	$(document).ready(function () {
		$("#odonto-operadora-id").change(function () {
			$.ajax({
				type: "POST",
				data: $("#operadora-id").serialize(),
				url: baseUrl + "odontoFormasPagamento/filtroOperadora/" + $("#odonto-operadora-id").val(),
				beforeSend: function () {
					$("#loader").click();
				},
				success: function (data) {
					$("#respostaFiltroOperadora").empty();
					$("#respostaFiltroOperadora").append(data);
					$("#fechar").click();
				}
			});

		});

	});
</script>