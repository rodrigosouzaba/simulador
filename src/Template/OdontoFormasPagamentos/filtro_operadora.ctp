<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($odonto_formas_pagamentos as $formaPagamento) : ?>
            <tr>
                <td><?= $formaPagamento->nome ?></td>
                <td><?= $formaPagamento->descricao ?></td>
                <td>
                    <?php
                    if (isset($formaPagamento->odonto_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $formaPagamento->odonto_operadora->imagen->caminho . "/" . $formaPagamento->odonto_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $formaPagamento->odonto_operadora->detalhe;
                    } else {
                        echo $formaPagamento->odonto_operadora->nome . " - " . $formaPagamento->odonto_operadora->detalhe;
                    }
                    ?>
                </td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/odontoFormasPagamentos/edit/$formaPagamento->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'odontoFormasPagamentos.edit']) ?>
                    <?= $this->Form->postLink('', "/odontoFormasPagamentos/delete/$formaPagamento->id", ['confirm' => __('Confirma exclusão?', $formaPagamento->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'odontoFormasPagamentos.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>