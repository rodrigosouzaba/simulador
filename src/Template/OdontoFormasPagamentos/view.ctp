<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Formas Pagamento'), ['action' => 'edit', $odontoFormasPagamento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Formas Pagamento'), ['action' => 'delete', $odontoFormasPagamento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoFormasPagamento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Formas Pagamentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Formas Pagamento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Operadoras'), ['controller' => 'OdontoOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Operadora'), ['controller' => 'OdontoOperadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoFormasPagamentos view large-9 medium-8 columns content">
    <h3><?= h($odontoFormasPagamento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($odontoFormasPagamento->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Operadora') ?></th>
            <td><?= $odontoFormasPagamento->has('odonto_operadora') ? $this->Html->link($odontoFormasPagamento->odonto_operadora->id, ['controller' => 'OdontoOperadoras', 'action' => 'view', $odontoFormasPagamento->odonto_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoFormasPagamento->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($odontoFormasPagamento->descricao)); ?>
    </div>
</div>
