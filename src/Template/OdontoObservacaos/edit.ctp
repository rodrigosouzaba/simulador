<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $odontoObservacao->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $odontoObservacao->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Odonto Observacaos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Operadoras'), ['controller' => 'OdontoOperadoras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Operadora'), ['controller' => 'OdontoOperadoras', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Produtos'), ['controller' => 'OdontoProdutos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Produto'), ['controller' => 'OdontoProdutos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoObservacaos form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoObservacao) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Observacao') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('odonto_operadora_id', ['options' => $odontoOperadoras, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
