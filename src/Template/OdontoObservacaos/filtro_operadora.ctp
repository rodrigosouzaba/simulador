<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('id') ?></th>-->
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($odonto_observacoes as $observacao) : ?>
            <tr>
                <td><?= $observacao->nome ?></td>
                <td><?= $observacao->descricao ?></td>
                <td>
                    <?php
                    //                        debug($produto);die();
                    if (isset($observacao->odonto_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $observacao->odonto_operadora->imagen->caminho . "/" . $observacao->odonto_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $observacao->odonto_operadora->detalhe;
                    } else {
                        echo $observacao->odonto_operadora->nome . " - " . $observacao->odonto_operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/odontoObservacaos/edit/$observacao->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Carência', 'sid' => 'odontoObservacaos.edit']) ?>
                        <?= $this->Form->postLink('', "/odontoObservacaos/delete/$observacao->id", ['confirm' => __('Confirma exclusão?', $observacao->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'odontoObservacaos.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
