<?php
$tipo_pessoa = ['PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica'];
?>
<div class="col-md-2 fonteReduzida">
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Observação', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
    ?>
</div>

<div class="col-md-2 fonteReduzida">
    <?= $this->Form->input('nova', ['label' => '', 'options' => ['Não', 'Sim'], 'empty' => 'Nova']); ?>
</div>

<div class="col-md-2 fonteReduzida">
    <?= $this->Form->input('tipo_pessoa', ['label' => '', 'options' => $tipo_pessoa, 'empty' => 'Selecione TIPO PESSOA', 'disabled' => 'true']); ?>
</div>

<div id="estados-field" class="col-md-2 fonteReduzida">
    <?= $this->Form->input('estados', ['label' => '', 'options' => [], 'empty' => 'Selecione ESTADO', 'disabled' => true]); ?>
</div>

<div id="operadoras-field" class="col-md-2 fonteReduzida">
    <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
</div>

<div id="respostaFiltroOperadora"></div>

<script type="text/javascript">
    $("#nova").change(function() {
        let nova = $(this).val();
        if (nova != '')
            $("#tipo-pessoa").removeAttr('disabled');
    })
    $("#tipo-pessoa").change(function() {
        var nova = $("#nova").val();
        let tipo = $(this).val();
        filtro('tipo', {
            nova,
            tipo
        })
    });

    function filtro(origem, object) {
        let {
            nova,
            tipo,
            estado,
            operadora
        } = object;

        let parameters = '';
        parameters = nova ? parameters + '/' + nova : parameters;
        parameters = tipo ? parameters + '/' + tipo : parameters;
        parameters = estado ? parameters + '/' + estado : parameters;
        parameters = operadora ? parameters + '/' + operadora : parameters;

        if (origem == 'tipo') {
            var url = 'filtroIndex';
            var campo = "#estados-field";
        } else if (origem == 'estado') {
            var url = 'filtroIndex';
            var campo = "#operadoras-field";

        } else if (origem == 'operadora') {
            var url = 'filtroOperadora';
            var campo = "#respostaFiltroOperadora";
        }

        $.ajax(baseUrl + "odontoObservacaos/" + url + parameters)
            .then(function(data) {
                $(campo).empty()
                $(campo).append(data)
            });
    }
</script>