<?= $this->Form->create($odontoOperadora) ?>
<div class="col-xs-12">

    <?php
    echo $this->Form->input('nome');
    echo $this->Form->input('detalhe');
    echo $this->Form->input('prioridade');
    echo $this->Form->input('tipo_pessoa', ['options' => array('PF' => 'PF', 'PJ' => 'PJ'), 'empty' => 'SELECIONE']);
    echo $this->Form->input('url', ['label' => 'Link Corretor Parceiro']);
    echo $this->Form->input('url_rede', ['label' => 'URL Rede Credenciada Completa']);
    echo $this->Form->input('estado_id', ['options' => $estados, 'empty' => 'SELECIONE']);
    ?>

</div>
<?= $this->element('botoesAdd');?>