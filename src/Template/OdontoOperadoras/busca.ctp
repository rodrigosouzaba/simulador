<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('id') ?></th>-->
            <th><?= $this->Html->link('Prioridade', '#', ['onclick' => 'order("prioridade", "ajax")']) ?></th>
            <th><?= $this->Html->link('Estado', '#', ['onclick' => 'order("estado_id", "ajax")']) ?></th>
            <th><?= $this->Html->link('Nome', '#', ['onclick' => 'order("nome", "ajax")']) ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($operadoras as $operadora) : ?>
            <tr>
                <td> <?= $operadora->prioridade ?></td>
                <td> <?= $operadora["estado"]["nome"] ?></td>
                <td>
                    <?php
                        if (isset($operadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $operadora->imagen->caminho . "/" . $operadora->imagen->nome, ['class' => 'logoMiniatura']) . " " . $operadora->nome . "  " . $operadora->detalhe;
                        } else {
                            ?>
                        <?= $operadora->nome . " - " . $operadora->detalhe ?>
                </td>


            <?php }
                ?>
            <td class="actions">
                <?=
                        $this->Html->link('', ['action' => 'duplicar', $operadora->id], [
                            'class' => 'btn btn-sm btn-default fa fa-copy',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Duplicar Operadora'
                        ])
                    ?>

                <?=
                        $this->Html->link('', ['action' => 'addImagem', $operadora->id], [
                            'title' => __('Adicionar Logomarca'),
                            'class' => 'btn btn-sm btn-default fa fa-image',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Logomarca'
                        ])
                    ?>
                <?=
                        $this->Html->link('', ['action' => 'edit', $operadora->id], [
                            'title' => __('Editar'),
                            'class' => 'btn btn-sm btn-default fa fa-pencil',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Editar'
                        ])
                    ?>
                <span data-toggle="tooltip" data-placement="top" data-original-title="Prioridade" title="Definir Prioridade">
                    <?=
                            $this->Html->link('', '#', [
                                'title' => __('Prioridade'),
                                'class' => 'btn btn-sm btn-default fa fa-list alterarPrioridade',
                                'role' => 'button',
                                'escape' => false,
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Alterar Prioridade',
                                'id' => 'reajustarVigencia',
                                'data-toggle' => "modal",
                                'data-target' => "#modalPrioridade",
                                'value' => $operadora->id
                            ])
                        ?>
                </span>
                <!-- <?=
                                $this->Form->postLink('', ['action' => 'delete', $operadora->id], [
                                    'confirm' => __('Tem certeza que deseja excluir Operadora {0}?', $operadora->nome . "  " . $operadora->detalhes), 'title' => __('Deletar'),
                                    'class' => 'btn btn-sm btn-default fa fa-trash',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'title' => 'Deletar'
                                ])
                            ?> -->

                <?php switch ($operadora->status):
                        case 'OCULTA': ?>

                        <?=
                                    $this->Html->link('', ['action' => 'ocultar', $operadora->id], [
                                        'class' => 'btn btn-sm btn-danger fa fa-eye',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Reexibir Operadora'
                                    ])
                                ?>
                        <?=
                                    $this->Html->link('', ['action' => 'atualizacao', $operadora->id], [
                                        'class' => 'btn btn-sm btn-default fa fa-refresh',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Colocar em Atualização de Preços'
                                    ])
                                ?>
                        <?=
                                    $this->Html->link('', ['action' => 'ativacao', $operadora->id], [
                                        'class' => 'btn btn-sm btn-default fa fa-check',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Inativar Operadora'
                                    ])
                                ?>
                    <?php break;

                        case 'EM ATUALIZAÇÃO': ?>
                        <?=
                                    $this->Html->link('', ['action' => 'ocultar', $operadora->id], [
                                        'class' => 'btn btn-sm btn-default fa fa-eye',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Ocultar Operadora'
                                    ])
                                ?>
                        <?=
                                    $this->Html->link('', ['action' => 'atualizacao', $operadora->id], [
                                        'class' => 'btn btn-sm btn-danger fa fa-refresh',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Remover Atualização de Preços'
                                    ])
                                ?>
                        <?=
                                    $this->Html->link('', ['action' => 'ativacao', $operadora->id], [
                                        'class' => 'btn btn-sm btn-default fa fa-check',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Inativar Operadora'
                                    ])
                                ?>
                    <?php break;

                        case 'INATIVA': ?>
                        <?=
                                    $this->Html->link('', ['action' => 'ocultar', $operadora->id], [
                                        'class' => 'btn btn-sm btn-default fa fa-eye',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Ocultar Operadora'
                                    ])
                                ?>
                        <?=
                                    $this->Html->link('', ['action' => 'atualizacao', $operadora->id], [
                                        'class' => 'btn btn-sm btn-default fa fa-refresh',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Colocar em Atualização de Preços'
                                    ])
                                ?>
                        <?=
                                    $this->Html->link('', ['action' => 'ativacao', $operadora->id], [
                                        'class' => 'btn btn-sm btn-danger fa fa-close',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Ativar Operadora'
                                    ])
                                ?>
                    <?php break;

                        default: ?>
                        <?=
                                    $this->Html->link('', ['action' => 'ocultar', $operadora->id], [
                                        'class' => 'btn btn-sm btn-default fa fa-eye',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Ocultar Operadora'
                                    ])
                                ?>
                        <?=
                                    $this->Html->link('', ['action' => 'atualizacao', $operadora->id], [
                                        'class' => 'btn btn-sm btn-default fa fa-refresh',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Colocar em Atualização de Preços'
                                    ])
                                ?>
                        <?=
                                    $this->Html->link('', ['action' => 'ativacao', $operadora->id], [
                                        'class' => 'btn btn-sm btn-default fa fa-check',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Inativar Operadora'
                                    ])
                                ?>
                    <?php break;
                    endswitch;
                    ?>
            </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
