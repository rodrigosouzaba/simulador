<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $odontoOperadora->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $odontoOperadora->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Odonto Operadoras'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Imagens'), ['controller' => 'Imagens', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Imagen'), ['controller' => 'Imagens', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Produtos'), ['controller' => 'OdontoProdutos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Produto'), ['controller' => 'OdontoProdutos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoOperadoras form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoOperadora) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Operadora') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('imagem_id', ['options' => $imagens, 'empty' => true]);
            echo $this->Form->input('detalhe');
            echo $this->Form->input('prioridade');
            echo $this->Form->input('url');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
