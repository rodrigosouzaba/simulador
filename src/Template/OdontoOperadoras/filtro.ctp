<?php
if (!empty($operadoras)) { ?>
    <?= $this->element('order') ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Html->link('Prioridade', '#', ['onclick' => 'order("prioridade", "operadoras-odonto")']) ?></th>
                <?php if (!$new) : ?>
                    <th><?= $this->Html->link('Estado', '#', ['onclick' => 'order("estado_id", "operadoras-odonto")']) ?></th>
                <?php endif; ?>
                <th><?= $this->Html->link('Nome', '#', ['onclick' => 'order("nome", "operadoras-odonto")']) ?></th>
                <th class="centralizada"><?= $this->Html->link('Tipo Pessoa', '#', ['onclick' => 'order("tipo_pessoa", "operadoras-odonto")']) ?></th>

                <th class="actions" style="width: 27%"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <?php foreach ($operadoras as $operadora) : ?>
            <tr>
                <td><?= $operadora->prioridade ?></td>
                <?php if (!$new) : ?>
                    <td> <?= $operadora["estado"]["nome"] ?></td>
                <?php endif; ?>
                <td>
                    <?= isset($operadora->imagen->caminho) ? $this->Html->image("../" . $operadora->imagen->caminho . "/" . $operadora->imagen->nome, ['class' => 'logoMiniatura']) . " " . $operadora->nome . "  " . $operadora->detalhe : $operadora->nome . " - " . $operadora->detalhe; ?>
                </td>
                <td class="centralizada"> <?= $operadora->tipo_pessoa ?></td>

                <td class="actions">
                    <?=
                    $this->Html->link('', ['action' => 'duplicar', $operadora->id], [
                        'class' => 'btn btn-default fa fa-copy',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Duplicar Operadora'
                    ])
                    ?>
                    <?=
                    $this->Html->link('', ['action' => 'addImagem', $operadora->id], [
                        'title' => __('Adicionar Logomarca'),
                        'class' => 'btn btn-default fa fa-image',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Logomarca'
                    ])
                    ?>
                    <?=
                    $this->Html->link('', ['action' => 'edit', $operadora->id], [
                        'title' => __('Editar'),
                        'class' => 'btn btn-default fa fa-pencil',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Editar'
                    ])
                    ?>
                    <span data-toggle="tooltip" data-placement="top" data-original-title="Prioridade" title="Definir Prioridade">
                        <?=
                        $this->Html->link('', '#', [
                            'title' => __('Prioridade'),
                            'class' => 'btn btn-default fa fa-list alterarPrioridade',
                            'role' => 'button',
                            'escape' => false,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Alterar Prioridade',
                            'id' => 'reajustarVigencia',
                            'data-toggle' => "modal",
                            'data-target' => "#modalPrioridade",
                            'value' => $operadora->id
                        ])
                        ?>
                    </span>
                    <!-- <?=
                            $this->Form->postLink('', ['action' => 'delete', $operadora->id], [
                                'confirm' => __('Tem certeza que deseja excluir Operadora {0}?', $operadora->nome . "  " . $operadora->detalhes), 'title' => __('Deletar'),
                                'class' => 'btn btn-default fa fa-trash',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Deletar'
                            ])
                            ?> -->
                    <?php switch ($operadora->status):
                        case 'OCULTA': ?>

                            <?=
                            $this->Html->link('', ['action' => 'ocultar', $operadora->id], [
                                'class' => 'btn btn-sm btn-danger fa fa-eye',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Reexibir Operadora'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'atualizacao', $operadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-refresh',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Colocar em Atualização de Preços'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'ativacao', $operadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-check',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Inativar Operadora'
                            ])
                            ?>
                        <?php break;

                        case 'EM ATUALIZAÇÃO': ?>
                            <?=
                            $this->Html->link('', ['action' => 'ocultar', $operadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-eye',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Ocultar Operadora'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'atualizacao', $operadora->id], [
                                'class' => 'btn btn-sm btn-danger fa fa-refresh',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Remover Atualização de Preços'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'ativacao', $operadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-check',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Inativar Operadora'
                            ])
                            ?>
                        <?php break;

                        case 'INATIVA': ?>
                            <?=
                            $this->Html->link('', ['action' => 'ocultar', $operadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-eye',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Ocultar Operadora'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'atualizacao', $operadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-refresh',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Colocar em Atualização de Preços'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'ativacao', $operadora->id], [
                                'class' => 'btn btn-sm btn-danger fa fa-close',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Ativar Operadora'
                            ])
                            ?>
                        <?php break;

                        default: ?>
                            <?=
                            $this->Html->link('', ['action' => 'ocultar', $operadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-eye',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Ocultar Operadora'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'atualizacao', $operadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-refresh',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Colocar em Atualização de Preços'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'ativacao', $operadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-check',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Inativar Operadora'
                            ])
                            ?>
                    <?php break;
                    endswitch;
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php } else { ?>
    <div class="col-xs-12 text-danger centralizada">NENHUMA OPERADORA NESTE ESTADO</div>
<?php } ?>
<!-- Modal -->
<div class="modal fade" id="modalPrioridade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Alterar Ordem de Prioridade da Operadora</h4>
                <small> As Operadoras serão exibidas nas simulações de acordo com sua <b>Ordem de
                        Prioridade</b>.</small>
            </div>
            <div class="modal-body" id="prioridade" style="min-height: 150px !important;">
                <div class="clearfix"> &nbsp;</div>
                &nbsp;
            </div>

        </div>
    </div>
</div>