<div id="ajax">
    <div class="col-xs-12" style="padding-top: 15px;">
        <div class="col-xs-12 col-md-3">
            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Operadora Odonto', '/odontoOperadoras/newAdd', ['class' => 'btn btn-sm btn-primary botaoSalvar', 'sid' => 'odontoOperadoras.add', 'role' => 'button', 'escape' => false, 'id' => 'addOperadora']); ?>
        </div>
        <div class="col-xs-2 flex">
            <?= $this->Form->input('search', ['placeholder' => 'Digite a operadora', 'label' => false, 'list' => 'lista-operadoras', 'style' => 'width: 90%;', 'templates' => ['inputContainer' => '{{content}}']]) ?>
            <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'fa fa-search']), ['id' => 'search-button', 'class' => 'search-button', 'style' => 'width: 20%']); ?>
            <datalist id="lista-operadoras">
                <?php foreach ($operadoras as $operadora) : ?>
                    <option value="<?= $operadora->nome ?>">
                    <?php endforeach; ?>
            </datalist>
        </div>
        <div class="col-xs-2">
            <?= $this->Form->input('status', ['options' => ['ATIVA' => 'ATIVAS', 'EM ATUALIZACAO' => 'ATUALIZAÇÃO', 'INATIVA' => 'INATIVAS', 'OCULTA' => 'OCULTAS',], "class" => "fonteReduzida", 'label' => false, 'empty' => 'Selecione STATUS']); ?>
        </div>
    </div>
    <?= $this->element('order') ?>
    <div class="col-xs-12" id="operadoras">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?= $this->Html->link('Prioridade', '#', ['onclick' => 'order("prioridade", "ajax")']) ?></th>
                    <th><?= $this->Html->link('Nome', '#', ['onclick' => 'order("nome", "ajax")']) ?></th>
                    <th class="centralizada"><?= $this->Html->link('Tipo Pessoa', '#', ['onclick' => 'order("tipo_pessoa", "ajax")']) ?></th>
                    <th class="actions" style="width: 27%"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($operadoras as $operadora) : ?>
                    <tr>
                        <td><?= $this->Number->format($operadora->prioridade) ?></td>
                        <td>
                            <?php
                            if (isset($operadora->imagen->caminho)) {
                                echo $this->Html->image("../" . $operadora->imagen->caminho . "/" . $operadora->imagen->nome, ['class' => 'logoMiniatura']) . " " . $operadora->nome . "  " . $operadora->detalhe;
                            } else {
                            ?>
                                <?= $operadora->nome . " - " . $operadora->detalhe ?>
                            <?php }
                            ?>

                        </td>
                        <td class="centralizada"><?= $operadora->tipo_pessoa ?></td>
                        <td class="actions">
                            <?=
                            $this->Html->link('', '/odontoOperadoras/duplicar/' . $operadora->id, [
                                'class' => 'btn btn-default fa fa-copy',
                                'data-toggle' => 'tooltip',
                                'sid' => 'odontoOperadoras.add',
                                'data-placement' => 'top',
                                'title' => 'Duplicar Operadora'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', '/odontoOperadoras/addImagem/' . $operadora->id, [
                                'title' => __('Adicionar Logomarca'),
                                'class' => 'btn btn-default fa fa-image',
                                'data-toggle' => 'tooltip',
                                'sid' => 'odontoOperadoras.addImagem',
                                'data-placement' => 'top',
                                'title' => 'Logomarca'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', '/odontoOperadoras/edit/' . $operadora->id . '/1', [
                                'title' => __('Editar'),
                                'class' => 'btn btn-default fa fa-pencil',
                                'data-toggle' => 'tooltip',
                                'sid' => 'odontoOperadoras.edit',
                                'data-placement' => 'top',
                                'title' => 'Editar'
                            ])
                            ?>
                            <span data-toggle="tooltip" data-placement="top" data-original-title="Prioridade" title="Definir Prioridade">
                                <?=
                                $this->Html->link('', '#', [
                                    'title' => __('Prioridade'),
                                    'class' => 'btn btn-default fa fa-list alterarPrioridade',
                                    'role' => 'button',
                                    'escape' => false,
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'odontoOperadoras.prioridade',
                                    'data-placement' => 'top',
                                    'title' => 'Alterar Prioridade',
                                    'id' => 'reajustarVigencia',
                                    'data-toggle' => "modal",
                                    'data-target' => "#modalPrioridade",
                                    'value' => $operadora->id
                                ])
                                ?>
                            </span>
                            <!-- <?=
                                    $this->Form->postLink('', '/odontoOperadoras/delete/' . $operadora->id, [
                                        'confirm' => __('Tem certeza que deseja excluir Operadora {0}?', $operadora->nome . "  " . $operadora->detalhes), 'title' => __('Deletar'),
                                        'class' => 'btn btn-default fa fa-trash',
                                        'sid' => 'odontoOperadoras.delete',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Deletar'
                                    ])
                                    ?> -->
                            <?php switch ($operadora->status):
                                case 'OCULTA': ?>

                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/ocultar/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-danger fa fa-eye',
                                        'sid' => 'odontoOperadoras.ocultar',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Reexibir Operadora'
                                    ])
                                    ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/atualizacao/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-default fa fa-refresh',
                                        'sid' => 'odontoOperadoras.atualizacao',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Colocar em Atualização de Preços'
                                    ])
                                    ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/ativacao/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-default fa fa-check',
                                        'sid' => 'odontoOperadoras.ativacao',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Inativar Operadora'
                                    ])
                                    ?>
                                <?php break;

                                case 'EM ATUALIZAÇÃO': ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/ocultar/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-default fa fa-eye',
                                        'sid' => 'odontoOperadoras.ocultar',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Ocultar Operadora'
                                    ])
                                    ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/atualizacao/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-danger fa fa-refresh',
                                        'sid' => 'odontoOperadoras.atualizacao',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Remover Atualização de Preços'
                                    ])
                                    ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/ativacao/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-default fa fa-check',
                                        'sid' => 'odontoOperadoras.ativacao',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Inativar Operadora'
                                    ])
                                    ?>
                                <?php break;

                                case 'INATIVA': ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/ocultar/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-default fa fa-eye',
                                        'sid' => 'odontoOperadoras.ocultar',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Ocultar Operadora'
                                    ])
                                    ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/atualizacao/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-default fa fa-refresh',
                                        'sid' => 'odontoOperadoras.atualizacao',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Colocar em Atualização de Preços'
                                    ])
                                    ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/ativacao/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-danger fa fa-close',
                                        'sid' => 'odontoOperadoras.ativacao',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Ativar Operadora'
                                    ])
                                    ?>
                                <?php break;

                                default: ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/ocultar/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-default fa fa-eye',
                                        'sid' => 'odontoOperadoras.ocultar',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Ocultar Operadora'
                                    ])
                                    ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/atualizacao/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-default fa fa-refresh',
                                        'sid' => 'odontoOperadoras.atualizacao',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Colocar em Atualização de Preços'
                                    ])
                                    ?>
                                    <?=
                                    $this->Html->link('', '/odontoOperadoras/ativacao/' . $operadora->id, [
                                        'class' => 'btn btn-sm btn-default fa fa-check',
                                        'sid' => 'odontoOperadoras.ativacao',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Inativar Operadora'
                                    ])
                                    ?>
                            <?php break;
                            endswitch;
                            ?>
                        </td>


                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modalPrioridade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Alterar Ordem de Prioridade da Operadora</h4>
                        <small> As Operadoras serão exibidas nas simulações de acordo com sua <b>Ordem de Prioridade</b>.</small>
                    </div>
                    <div class="modal-body" id="prioridade" style="min-height: 150px !important;">
                        <div class="form-inline col-xs-6 col-xs-offset-1 text-center">
                            <?= $this->Form->create(null, ['id' => 'form-prioridade', 'url' => ['action' => 'prioridade']]) ?>
                            <?= $this->Form->hidden('operadora_id', ['value' => '', 'label' => false, 'id' => 'operadora-id']) ?>
                            <div class="form-group">
                                <?= $this->Form->input('prioridade', ['label' => 'Ordem de Prioridade', 'label' => false, 'type' => 'number', 'style' => 'margin: 0;']) ?>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <div class="col-xs-3">
                            <?= $this->Form->button('Validar', ['class' => 'btn btn-sm btn-default col-xs-12', 'id' => 'btn-validar', 'style' => 'height: 37px']) ?>
                        </div>
                        <span id="text-prioridade" class="text-danger small col-xs-8 col-xs-offset-2 text-center"></span>
                        <?= $this->Form->button('Salvar', ['class' => 'btn btn-success col-xs-4 col-xs-offset-4', 'id' => 'btn-priorizar', 'disabled' => true, 'style' => 'margin-top: 15px;']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?= $this->Form->end() ?>
<?= $this->element("processando") ?>
<script type="text/javascript">
    function filtrar() {
        let texto = $("#search").val();
        let estado = $("#estado-id").val();
        let status = $("#status").val();
        if (texto !== "" || estado !== "" || status !== "") {
            $.ajax({
                type: 'POST',
                url: '<?= $this->request->webroot . 'odontoOperadoras/filtro/1' ?>',
                data: {
                    'search': texto,
                    'estado': estado,
                    'status': status
                },
                success: function(data) {
                    $("#operadoras").empty();
                    $("#operadoras").append(data);
                }
            });
        }
    }

    $("#search-button").click(function() {
        filtrar()
    });

    $("#search").keyup(function(e) {
        if (e.which == 13) {
            filtrar()
        }
    });

    $("#estado-id").change(function() {
        filtrar()
    });

    $("#status").change(function() {
        filtrar()
    });


    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('#modalPrioridade').on('shown.bs.modal', function() {
            $('#myInput').focus()
        });
    });

    $(".alterarPrioridade").click(function() {
        let operadora_id = $(this).attr('value');
        $("#form-prioridade > #operadora-id").empty();
        $("#form-prioridade > #operadora-id").val(operadora_id);
    });

    $("#btn-validar").click(function() {
        let form = $("#form-prioridade").serialize();
        $.ajax({
            type: 'POST',
            data: form,
            url: baseUrl + '/odontoOperadoras/validacaoPrioridade',
            beforeSend: function() {
                $("#text-prioridade").empty();
                $("#text-prioridade").append('Validando...');
            },
            success: function(data) {
                if (data.result) {
                    $("#btn-priorizar").removeAttr('disabled');
                    $("#text-prioridade").empty();
                    $("#text-prioridade").append('Não há operadora com esta prioridade.');
                } else {
                    $("#btn-priorizar").attr('disabled', 'disabled');
                    $("#text-prioridade").empty();
                    $("#text-prioridade").append('Esta prioridade já está em uso.');
                }
            }
        });
    });

    $("#btn-priorizar").click(function() {
        $("#form-prioridade").submit()
    });

    $(".ocultar").click(function() {
        $("#loader").click();
        $.ajax({
            type: "post",
            url: baseUrl + '/odontoOperadoras/ocultar/' + $(this).attr("value"),
            success: function(data) {
                alert("Operadora Oculta com sucesso");
                location.reload();
            }
        });
    });
    $(".exibir").click(function() {
        $("#loader").click();
        $.ajax({
            type: "post",
            url: baseUrl + '/odontoOperadoras/exibir/' + $(this).attr("value"),
            success: function(data) {
                alert("Operadora Reexibida com sucesso");
                location.reload();
            }
        });
    });

    $(".atualizacao").click(function() {
        $("#loader").click();
        $.ajax({
            type: "post",
            url: baseUrl + '/odontoOperadoras/atualizacao/' + $(this).attr("value"),
            success: function(data) {
                alert("Operadora EM ATUALIZAÇÃO com sucesso");
                location.reload();
            }
        });
    });

    $(".remover-atualizacao").click(function() {
        $("#loader").click();
        $.ajax({
            type: "post",
            url: baseUrl + '/odontoOperadoras/removeratualizacao/' + $(this).attr("value"),
            success: function(data) {
                alert("Operadora REMOVIDA DE ATUALIZAÇÃO com sucesso");
                location.reload();
            }
        });
    });
</script>