<?= $this->Form->create($odontoOperadora) ?>
<div class="container fonteReduzida">
    <legend>Nova Operadora</legend>

    <?= $this->Form->hidden('nova', ['value' => 1]) ?>
    <div class="col-xs-4">
        <?= $this->Form->input('nome') ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('detalhe') ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('prioridade') ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('tipo_pessoa', ['options' => array('PF' => 'PF', 'PJ' => 'PJ'), 'empty' => 'SELECIONE']) ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('url', ['label' => 'Link Corretor Parceiro']) ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('url_rede', ['label' => 'URL Rede Credenciada Completa']) ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('atendimento_cliente', ['label' => 'Atendimento ao Cliente']) ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('atendimento_corretor', ['label' => 'Atendimento ao Corretor']) ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('site', ['label' => 'Site']) ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('via_boleto', ['label' => 'Via para Boleto']) ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('app_android', ['label' => 'Link App Android']) ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('app_ios', ['label' => 'Link App IOS']) ?>
    </div>
</div>
<?= $this->element('botoesAdd'); ?>