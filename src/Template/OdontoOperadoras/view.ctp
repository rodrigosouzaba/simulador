<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Operadora'), ['action' => 'edit', $odontoOperadora->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Operadora'), ['action' => 'delete', $odontoOperadora->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoOperadora->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Operadoras'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Operadora'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Imagens'), ['controller' => 'Imagens', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Imagen'), ['controller' => 'Imagens', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Produtos'), ['controller' => 'OdontoProdutos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Produto'), ['controller' => 'OdontoProdutos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoOperadoras view large-9 medium-8 columns content">
    <h3><?= h($odontoOperadora->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($odontoOperadora->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Imagen') ?></th>
            <td><?= $odontoOperadora->has('imagen') ? $this->Html->link($odontoOperadora->imagen->id, ['controller' => 'Imagens', 'action' => 'view', $odontoOperadora->imagen->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Detalhe') ?></th>
            <td><?= h($odontoOperadora->detalhe) ?></td>
        </tr>
        <tr>
            <th><?= __('Url') ?></th>
            <td><?= h($odontoOperadora->url) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoOperadora->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prioridade') ?></th>
            <td><?= $this->Number->format($odontoOperadora->prioridade) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Odonto Produtos') ?></h4>
        <?php if (!empty($odontoOperadora->odonto_produtos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Odonto Operadora Id') ?></th>
                <th><?= __('Carencia Id') ?></th>
                <th><?= __('Rede Id') ?></th>
                <th><?= __('Reembolso Id') ?></th>
                <th><?= __('Opcional Id') ?></th>
                <th><?= __('Informacao Id') ?></th>
                <th><?= __('Observacao Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($odontoOperadora->odonto_produtos as $odontoProdutos): ?>
            <tr>
                <td><?= h($odontoProdutos->id) ?></td>
                <td><?= h($odontoProdutos->nome) ?></td>
                <td><?= h($odontoProdutos->descricao) ?></td>
                <td><?= h($odontoProdutos->odonto_operadora_id) ?></td>
                <td><?= h($odontoProdutos->carencia_id) ?></td>
                <td><?= h($odontoProdutos->rede_id) ?></td>
                <td><?= h($odontoProdutos->reembolso_id) ?></td>
                <td><?= h($odontoProdutos->opcional_id) ?></td>
                <td><?= h($odontoProdutos->informacao_id) ?></td>
                <td><?= h($odontoProdutos->observacao_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OdontoProdutos', 'action' => 'view', $odontoProdutos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OdontoProdutos', 'action' => 'edit', $odontoProdutos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OdontoProdutos', 'action' => 'delete', $odontoProdutos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoProdutos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
