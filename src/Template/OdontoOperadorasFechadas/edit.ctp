<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $odontoOperadorasFechada->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $odontoOperadorasFechada->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Odonto Operadoras Fechadas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoOperadorasFechadas form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoOperadorasFechada) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Operadoras Fechada') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('arquivos._ids', ['options' => $arquivos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
