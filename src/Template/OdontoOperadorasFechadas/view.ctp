<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Operadoras Fechada'), ['action' => 'edit', $odontoOperadorasFechada->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Operadoras Fechada'), ['action' => 'delete', $odontoOperadorasFechada->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoOperadorasFechada->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Operadoras Fechadas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Operadoras Fechada'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoOperadorasFechadas view large-9 medium-8 columns content">
    <h3><?= h($odontoOperadorasFechada->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($odontoOperadorasFechada->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoOperadorasFechada->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($odontoOperadorasFechada->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Arquivos') ?></h4>
        <?php if (!empty($odontoOperadorasFechada->arquivos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Caminho') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Tipo') ?></th>
                <th><?= __('Exibicao Nome') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($odontoOperadorasFechada->arquivos as $arquivos): ?>
            <tr>
                <td><?= h($arquivos->id) ?></td>
                <td><?= h($arquivos->nome) ?></td>
                <td><?= h($arquivos->caminho) ?></td>
                <td><?= h($arquivos->created) ?></td>
                <td><?= h($arquivos->modified) ?></td>
                <td><?= h($arquivos->tipo) ?></td>
                <td><?= h($arquivos->exibicao_nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Arquivos', 'action' => 'view', $arquivos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Arquivos', 'action' => 'edit', $arquivos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Arquivos', 'action' => 'delete', $arquivos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $arquivos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
