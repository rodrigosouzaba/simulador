<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $odontoOperadorasFechadasArquivo->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $odontoOperadorasFechadasArquivo->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Odonto Operadoras Fechadas Arquivos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Operadoras Fechadas'), ['controller' => 'OdontoOperadorasFechadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Operadoras Fechada'), ['controller' => 'OdontoOperadorasFechadas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoOperadorasFechadasArquivos form large-9 medium-8 columns content">
    <?= $this->Form->create($odontoOperadorasFechadasArquivo) ?>
    <fieldset>
        <legend><?= __('Edit Odonto Operadoras Fechadas Arquivo') ?></legend>
        <?php
            echo $this->Form->input('odonto_operadora_fechada_id', ['options' => $odontoOperadorasFechadas]);
            echo $this->Form->input('arquivo_id', ['options' => $arquivos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
