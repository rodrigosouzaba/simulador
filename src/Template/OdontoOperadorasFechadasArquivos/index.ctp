<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Odonto Operadoras Fechadas Arquivo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Odonto Operadoras Fechadas'), ['controller' => 'OdontoOperadorasFechadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Odonto Operadoras Fechada'), ['controller' => 'OdontoOperadorasFechadas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="odontoOperadorasFechadasArquivos index large-9 medium-8 columns content">
    <h3><?= __('Odonto Operadoras Fechadas Arquivos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('odonto_operadora_fechada_id') ?></th>
                <th><?= $this->Paginator->sort('arquivo_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($odontoOperadorasFechadasArquivos as $odontoOperadorasFechadasArquivo): ?>
            <tr>
                <td><?= $this->Number->format($odontoOperadorasFechadasArquivo->id) ?></td>
                <td><?= $odontoOperadorasFechadasArquivo->has('odonto_operadoras_fechada') ? $this->Html->link($odontoOperadorasFechadasArquivo->odonto_operadoras_fechada->id, ['controller' => 'OdontoOperadorasFechadas', 'action' => 'view', $odontoOperadorasFechadasArquivo->odonto_operadoras_fechada->id]) : '' ?></td>
                <td><?= $odontoOperadorasFechadasArquivo->has('arquivo') ? $this->Html->link($odontoOperadorasFechadasArquivo->arquivo->id, ['controller' => 'Arquivos', 'action' => 'view', $odontoOperadorasFechadasArquivo->arquivo->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $odontoOperadorasFechadasArquivo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $odontoOperadorasFechadasArquivo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $odontoOperadorasFechadasArquivo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoOperadorasFechadasArquivo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
