<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Operadoras Fechadas Arquivo'), ['action' => 'edit', $odontoOperadorasFechadasArquivo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Operadoras Fechadas Arquivo'), ['action' => 'delete', $odontoOperadorasFechadasArquivo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoOperadorasFechadasArquivo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Operadoras Fechadas Arquivos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Operadoras Fechadas Arquivo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Operadoras Fechadas'), ['controller' => 'OdontoOperadorasFechadas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Operadoras Fechada'), ['controller' => 'OdontoOperadorasFechadas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoOperadorasFechadasArquivos view large-9 medium-8 columns content">
    <h3><?= h($odontoOperadorasFechadasArquivo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Odonto Operadoras Fechada') ?></th>
            <td><?= $odontoOperadorasFechadasArquivo->has('odonto_operadoras_fechada') ? $this->Html->link($odontoOperadorasFechadasArquivo->odonto_operadoras_fechada->id, ['controller' => 'OdontoOperadorasFechadas', 'action' => 'view', $odontoOperadorasFechadasArquivo->odonto_operadoras_fechada->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Arquivo') ?></th>
            <td><?= $odontoOperadorasFechadasArquivo->has('arquivo') ? $this->Html->link($odontoOperadorasFechadasArquivo->arquivo->id, ['controller' => 'Arquivos', 'action' => 'view', $odontoOperadorasFechadasArquivo->arquivo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoOperadorasFechadasArquivo->id) ?></td>
        </tr>
    </table>
</div>
