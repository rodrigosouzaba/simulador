<div class="col-xs-12 fonteReduzida">

    <?= $this->Form->create($odontoProduto) ?>

    <div class="col-xs-12">
        <?= $this->Form->input('nome'); ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('estado_id', ['options' => $estados, 'empty' => 'SELECIONE', 'label' => 'Estados']); ?>
    </div>
    <div class="col-xs-4">
        <?= $this->Form->input('tipo_pessoa', ['options' => ['PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica'], 'empty' => 'SELECIONE', 'label' => 'Tipo Pessoa', 'disabled' => true]); ?>
    </div>
    <div class="col-xs-4" id="operadora-field">
        <?= $this->Form->input('odonto_operadora_id', ['options' => '', 'empty' => 'SELECIONE', 'disabled' => true]); ?>
    </div>

    <div id="infoAdicional"></div>

    <div class="col-xs-12">
        <?= $this->Form->input('descricao', ['label' => 'Observação']) ?>
    </div>

    <?= $this->element('botoesAdd'); ?>

    <?= $this->Form->end() ?>

</div>

<script type="text/javascript">
    $("#estado-id").change(function() {
        $("#tipo-pessoa").removeAttr("disabled");
    });

    $("#tipo-pessoa").change(function() {
        let estado = $("#estado-id").val();
        let tipo = $(this).val();
        $.ajax({
            type: 'POST',
            url: baseUrl + 'odontoProdutos/getOperadoras/' + estado + '/' + tipo,
            success: function(data) {
                $("#operadora-field").empty();
                $("#operadora-field").append(data);
            }
        });
    });
</script>
