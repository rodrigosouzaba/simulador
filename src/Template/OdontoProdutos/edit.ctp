<?= $this->Form->create($odontoProduto, ['class' => "form-inline"]) ?>

<div class="col-md-4 fonteReduzida">
    <?= $this->Form->input('nome') ?>
</div>
<div class="col-xs-4 fonteReduzida">
    <?= $this->Form->input('tipo_pessoa', ['options' => ['PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica'], 'empty' => 'SELECIONE', 'label' => 'Tipo Pessoa']); ?>
</div>
<div class="col-md-4 fonteReduzida">
    <?= $this->Form->input('odonto_operadora_id', ['empty' => 'SELECIONE', 'required' => 'required']); ?>
</div>

<div class="fonteReduzida" id="infoAdicional">
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-4 fonteReduzida">

        <div class="tituloField ">
            Carências
        </div>
        <?= $this->Form->input('odonto_carencia_id', ['empty' => 'SELECIONE', 'label' => '']); ?>
    </div>
    <div class="col-md-4 fonteReduzida">

        <div class="tituloField">
            Reembolsos
        </div>
        <?= $this->Form->input('odonto_reembolso_id', ['empty' => 'SELECIONE', 'label' => '']); ?>
    </div>
    <div class="col-md-4 fonteReduzida">

        <div class="tituloField">
            Rede Credenciada
        </div>
        <?= $this->Form->input('odonto_rede_id', ['empty' => 'SELECIONE', 'label' => '']); ?>

    </div>

    <div class="clearfix">&nbsp;</div>
    <div class="col-md-4 fonteReduzida">

        <div class="tituloField">
            Dependentes
        </div>
        <?= $this->Form->input('odonto_dependente_id', ['empty' => 'SELECIONE', 'label' => '']); ?>

    </div>
    <div class="col-md-4 fonteReduzida">

        <div class="tituloField">
            Documentos Necessários
        </div>
        <?= $this->Form->input('odonto_documento_id', ['empty' => 'SELECIONE', 'label' => '']); ?>

    </div>
    <div class="col-md-4 fonteReduzida">
        <div class="tituloField">
            Observações
        </div>
        <?= $this->Form->input('odonto_observacao_id', ['options' => $odontoObservacoes, 'empty' => 'SELECIONE', 'label' => '']); ?>
    </div>

    <div class="clearfix">&nbsp;</div>

</div>
<div class="col-xs-12 fonteReduzida">
    <?= $this->Form->input('descricao', ['label' => 'Observação']) ?>
</div>
<?= $this->element('botoesAdd'); ?>

<?= $this->Form->end() ?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#acoes").hide();

        $("#operadora-id").change(function() {
            if ($("#operadora-id").val() !== 0 || $("#operadora-id").val() !== null) {

                $.ajax({
                    type: "POST",
                    url: baseUrl + "produtos/findInfo/" + $("#operadora-id").val(),
                    data: $("#operadora-id").serialize(),
                    success: function(data) {
                        $("#infoAdicional").empty();
                        $("#infoAdicional").append(data);
                    }
                });
            } else {
                $("#acoes").hide();
            }
        });

        $("#operadora-id").change(function() {
            $("#acoes").show();
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelas/findProdutos/" + (this).value,
                data: $("#operadora-id").serialize(),
                success: function(data) {
                    $("#produtos").empty();
                    $("#produtos").append(data);
                }
            });
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelas/findRegioes/" + (this).value + "/" + $("#estado-id").val(),
                data: $("#operadora-id").serialize(),
                success: function(data) {
                    $("#regiao").empty();
                    $("#regiao").append(data);
                }
            });



        });


    });

    //    $('.collapse').collapse({aria - expanded = "false"});
</script>
