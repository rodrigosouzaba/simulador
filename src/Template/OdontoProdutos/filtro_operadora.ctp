<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<?= $this->Form->create('selecaoTabelas', ['id' => 'selecaoTabelas']); ?>

<?php
//debug($produtos);

if (!empty($produtos)) {
?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th class="centralizada" style="width:10%"><small><?= 'Selecionar' ?></small></th>
                <th><?= 'Nome' ?></th>
                <th><?= 'Observação' ?></th>
                <th><?= 'Operadora' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($produtos as $produto) : ?>
                <tr>
                    <td class="centralizada"><?= $this->Form->input($produto->id, ['type' => 'checkbox', 'id' => 'imprimir' . $produto->id, 'value' => $produto->id, 'class' => 'noMarginBottom', 'title' => 'Selecionar', 'label' => '']); ?></td>

                    <td><?= h($produto->nome) ?></td>
                    <td><?= h($produto->descricao) ?></td>
                    <td>
                        <?php
                        if (isset($produto->odonto_odonto_operadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $produto->odonto_operadora->imagen->caminho . "/" . $produto->odonto_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $produto->odonto_operadora->detalhe;
                        } else {
                            echo $produto->odonto_operadora->nome . " - " . $produto->odonto_operadora->detalhe;
                        }
                        ?>
                    </td>


                    <td class="actions">
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/odontoProdutos/edit/$produto->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Produto', 'sid' => 'odontoProdutos.edit']) ?>
                        <?= $this->Form->postLink('', "/odontoProdutos/delete/$produto->id", ['confirm' => __('Confirma exclusão do Produto?', $produto->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'odontoProdutos.delete']);?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="col-md-12">
        <?=
            $this->Html->link($this->Html->tag('span', '', [
                'class' => 'fa fa-trash',
                'aria-hidden' => 'true'
            ]) . ' Deletar Produtos', '#', [
                'class' => 'btn btn-sm btn-danger',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Deletar Produtos',
                'id' => 'deleteLote'
            ])
        ?>
    </div>
<?php
} else {
?>
    <div class="clearfix">&nbsp;</div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12 text-danger centralizada"> NENHUM PRODUTO ENCONTRADO</div>
<?php
}
?>


<?= $this->Form->end() ?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#odonto_operadora-id").change(function() {
            $.ajax({
                type: "POST",
                url: baseUrl + "produtos/filtroOdontoOperadora/" + $("#odonto_operadora-id").val(),
                data: $("#odonto_operadora-id").serialize(),
                success: function(data) {
                    $("#listaProdutos").empty();
                    $("#listaProdutos").append(data);
                }
            });

        });

    });
    $("#deleteLote").click(function() {
        $.ajax({
            type: "post",
            data: $("#selecaoTabelas").serialize(),
            url: baseUrl + "produtos/deleteLote/",
            success: function(data) {
                window.location = baseUrl + 'produtos/';

            }
        });
    });
</script>
