<?= $this->Form->input('odonto_operadora_id', ['options' => $operadoras, 'empty' => 'SELECIONE', 'label' => 'Operadoras Odonto']); ?>
<script>
    $("#odonto-operadora-id").change(function() {
        if ($("#odonto-operadora-id").val() !== 0 || $("#odonto-operadora-id").val() !== null) {
            $.ajax({
                type: "POST",
                url: baseUrl + "odontoProdutos/findInfo/" + $("#odonto-operadora-id").val(),
                data: $("#operadora-id").serialize(),
                success: function(data) {
                    $("#infoAdicional").empty();
                    $("#infoAdicional").append(data);
                }
            });
        }
    });
</script>
