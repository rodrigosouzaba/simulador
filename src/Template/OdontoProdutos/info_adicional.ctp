<div class="clearfix">&nbsp;</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField ">
        Carências
    </div>
    <?= $this->Form->input('odonto_carencia_id', ['options' => $odontoCarencias, 'empty' => 'SELECIONE', 'label' => '']); ?>
</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField">
        Reembolsos
    </div>
    <?= $this->Form->input('odonto_reembolso_id', ['options' => $odontoReembolsos, 'empty' => 'SELECIONE', 'label' => '']); ?>
</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField">
        Rede Credenciada
    </div>
    <?= $this->Form->input('odonto_rede_id', ['options' => $odontoRedes, 'empty' => 'SELECIONE', 'label' => '']); ?>

</div>

<div class="clearfix">&nbsp;</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField">
        Dependentes
    </div>
    <?= $this->Form->input('odonto_dependente_id', ['options' => $odontoDependentes, 'empty' => 'SELECIONE', 'label' => '']); ?>

</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField">
        Documentos Necessários
    </div>
    <?= $this->Form->input('odonto_documento_id', ['options' => $odontoDocumentos, 'empty' => 'SELECIONE', 'label' => '']); ?>

</div>
<div class="col-md-4 fonteReduzida">
    <div class="tituloField">
        Observações
    </div>
    <?= $this->Form->input('odonto_observacao_id', ['options' => $odontoObservacoes, 'empty' => 'SELECIONE', 'label' => '']); ?>
</div>

<div class="clearfix">&nbsp;</div>
