<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Produto'), ['action' => 'edit', $odontoProduto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Produto'), ['action' => 'delete', $odontoProduto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoProduto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Produtos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Produto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Operadoras'), ['controller' => 'OdontoOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Operadora'), ['controller' => 'OdontoOperadoras', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Carencias'), ['controller' => 'Carencias', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Carencia'), ['controller' => 'Carencias', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Redes'), ['controller' => 'Redes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rede'), ['controller' => 'Redes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Reembolsos'), ['controller' => 'Reembolsos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reembolso'), ['controller' => 'Reembolsos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Opcionais'), ['controller' => 'Opcionais', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Opcionai'), ['controller' => 'Opcionais', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Informacoes'), ['controller' => 'Informacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Informaco'), ['controller' => 'Informacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Observacoes'), ['controller' => 'Observacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Observaco'), ['controller' => 'Observacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['controller' => 'OdontoTabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoProdutos view large-9 medium-8 columns content">
    <h3><?= h($odontoProduto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($odontoProduto->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Operadora') ?></th>
            <td><?= $odontoProduto->has('odonto_operadora') ? $this->Html->link($odontoProduto->odonto_operadora->id, ['controller' => 'OdontoOperadoras', 'action' => 'view', $odontoProduto->odonto_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Carencia') ?></th>
            <td><?= $odontoProduto->has('carencia') ? $this->Html->link($odontoProduto->carencia->id, ['controller' => 'Carencias', 'action' => 'view', $odontoProduto->carencia->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Rede') ?></th>
            <td><?= $odontoProduto->has('rede') ? $this->Html->link($odontoProduto->rede->id, ['controller' => 'Redes', 'action' => 'view', $odontoProduto->rede->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Reembolso') ?></th>
            <td><?= $odontoProduto->has('reembolso') ? $this->Html->link($odontoProduto->reembolso->id, ['controller' => 'Reembolsos', 'action' => 'view', $odontoProduto->reembolso->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Opcionai') ?></th>
            <td><?= $odontoProduto->has('opcionai') ? $this->Html->link($odontoProduto->opcionai->id, ['controller' => 'Opcionais', 'action' => 'view', $odontoProduto->opcionai->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Informaco') ?></th>
            <td><?= $odontoProduto->has('informaco') ? $this->Html->link($odontoProduto->informaco->id, ['controller' => 'Informacoes', 'action' => 'view', $odontoProduto->informaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Observaco') ?></th>
            <td><?= $odontoProduto->has('observaco') ? $this->Html->link($odontoProduto->observaco->id, ['controller' => 'Observacoes', 'action' => 'view', $odontoProduto->observaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoProduto->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($odontoProduto->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Odonto Tabelas') ?></h4>
        <?php if (!empty($odontoProduto->odonto_tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Vigencia') ?></th>
                <th><?= __('Preco Vida') ?></th>
                <th><?= __('Odonto Produto Id') ?></th>
                <th><?= __('Abrangencia Id') ?></th>
                <th><?= __('Validade') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Regiao Id') ?></th>
                <th><?= __('Minimo Vidas') ?></th>
                <th><?= __('Maximo Vidas') ?></th>
                <th><?= __('Titulares') ?></th>
                <th><?= __('Prioridade') ?></th>
                <th><?= __('Reembolso') ?></th>
                <th><?= __('Tipo Pessoa') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($odontoProduto->odonto_tabelas as $odontoTabelas): ?>
            <tr>
                <td><?= h($odontoTabelas->id) ?></td>
                <td><?= h($odontoTabelas->nome) ?></td>
                <td><?= h($odontoTabelas->descricao) ?></td>
                <td><?= h($odontoTabelas->vigencia) ?></td>
                <td><?= h($odontoTabelas->preco_vida) ?></td>
                <td><?= h($odontoTabelas->odonto_produto_id) ?></td>
                <td><?= h($odontoTabelas->abrangencia_id) ?></td>
                <td><?= h($odontoTabelas->validade) ?></td>
                <td><?= h($odontoTabelas->estado_id) ?></td>
                <td><?= h($odontoTabelas->regiao_id) ?></td>
                <td><?= h($odontoTabelas->minimo_vidas) ?></td>
                <td><?= h($odontoTabelas->maximo_vidas) ?></td>
                <td><?= h($odontoTabelas->titulares) ?></td>
                <td><?= h($odontoTabelas->prioridade) ?></td>
                <td><?= h($odontoTabelas->reembolso) ?></td>
                <td><?= h($odontoTabelas->tipo_pessoa) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OdontoTabelas', 'action' => 'view', $odontoTabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OdontoTabelas', 'action' => 'edit', $odontoTabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OdontoTabelas', 'action' => 'delete', $odontoTabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoTabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
