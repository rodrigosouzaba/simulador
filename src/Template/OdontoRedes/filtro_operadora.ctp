<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('id') ?></th>-->
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($odonto_redes as $rede) : ?>
            <tr>
                <td><?= $rede->nome ?></td>
                <td><?= $rede->descricao ?></td>
                <td>
                    <?php
                    if (isset($rede->odonto_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $rede->odonto_operadora->imagen->caminho . "/" . $rede->odonto_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $rede->odonto_operadora->detalhe;
                    } else {
                        echo $rede->odonto_operadora->nome . " - " . $rede->odonto_operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/odontoRedes/edit/$rede->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Carência', 'sid' => 'odontoRedes.edit']) ?>
                    <?= $this->Form->postLink('', "/odontoRedes/delete/$rede->id", ['confirm' => __('Confirma exclusão?', $rede->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'odontoRedes.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
