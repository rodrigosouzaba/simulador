<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($odonto_reembolsos as $reembolso) : ?>
            <tr>
                <td><?= $reembolso->nome ?></td>
                <td><?= $reembolso->descricao ?></td>
                <td>
                    <?php
                    if (isset($reembolso->odonto_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $reembolso->odonto_operadora->imagen->caminho . "/" . $reembolso->odonto_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $reembolso->odonto_operadora->detalhe;
                    } else {
                        echo $reembolso->odonto_operadora->nome . " - " . $reembolso->odonto_operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/odontoReembolsos/edit/$reembolso->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Carência', 'sid' => 'odontoReembolsos.edit']) ?>
                    <?= $this->Form->postLink('', "/odontoReembolsos/delete/$reembolso->id", ['confirm' => __('Confirma exclusão?', $reembolso->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'odontoReembolsos.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
