<style>
    #modalidades {
        display: none;
    }
</style>
<div class="col-xs-12 fonteReduzida">
    <?= $this->Form->create($odontoTabela) ?>
    <div class="col-xs-12 col-md-9">
        <?= $this->Form->input('nome') ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('vigencia', ['Válida até']) ?>
    </div>
    <div class="col-xs-12">
        <?= $this->Form->input('descricao', ['label' => 'Observação']); ?>
    </div>
    <div class="clearfix">&nbsp;</div>

    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('titulares', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('minimo_vidas', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('maximo_vidas', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?php $tipo_pessoas = array('PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica') ?>
        <?= $this->Form->input('tipo_pessoa', ['options' => $tipo_pessoas, 'empty' => 'SELECIONE', 'required' => 'required']) ?>
    </div>

    <div class="clearfix">&nbsp;</div>
    <div class="col-xs-12 col-md-3">

        <?= $this->Form->input('estado_id', ['empty' => 'SELECIONE', 'label' => 'Estado', 'required' => 'required']); ?>
    </div>
    <div class="col-xs-12 col-md-3" id="operadoras">
        <?= $this->Form->input('odonto_operadora_id', ['options' => '', 'empty' => 'SELECIONE', 'label' => 'Operadora', 'required' => 'required', 'disabled' => 'true']); ?>
    </div>
    <div class="col-xs-12 col-md-3" id="odonto-produto">
        <?= $this->Form->input('odonto_produto_id', ['options' => $odontoProdutos, 'label' => 'Produto', 'empty' => 'SELECIONE', 'required' => 'required', 'disabled' => 'true']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('odonto_atendimento_id', ['options' => $odontoAtendimentos, 'empty' => 'SELECIONE', 'label' => 'Área de Atendimento', 'required' => 'required']); ?>
    </div>
    <div class="clearfix">&nbsp;</div>

    <div class="col-xs-12 col-md-3" id="odonto-comercializacao">
        <?= $this->Form->input('odonto_comercializacao_id', ['options' => '', 'empty' => 'SELECIONE', 'label' => 'Área de Comercialização', 'required' => 'required', 'disabled' => 'true']); ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('preco_vida', ['label' => 'Preço por Vida', 'required' => 'required', 'step' => "0.01"]) ?>
    </div>

    <div class="col-xs-12 col-md-3" id="modalidades">
        <?= $this->Form->input('modalidade', ['label' => 'Modalidade', 'options' => ['INDIVIDUAL' => 'INDIVIDUAL', 'ADESÃO' => 'ADESÃO'], 'empty' => 'SELECIONE']) ?>
    </div>

    <div class="col-xs-12 col-md-3">
        <?php $reembolsos = array('S' => 'SIM', 'N' => 'NÃO') ?>
        <?= $this->Form->input('reembolso', ['options' => $reembolsos, 'empty' => 'SELECIONE', 'label' => 'Possue reembolso?', 'required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        &nbsp;
    </div>
    <?= $this->element('botoesAdd'); ?>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    $("#estado-id").change(function() {
        if ($("#tipo-pessoa").val() != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>odonto-tabelas/getOperadora/" + $(this).val() + "/" + $("#tipo-pessoa").val(),
                success: function(data) {
                    $("#operadoras").empty();
                    $("#operadoras").append(data);
                }
            });
        }
    });

    $("#tipo-pessoa").change(function() {
        let pessoa = $(this).val();
        if (pessoa == "PF") {
            $("#modalidades").show()
            $("#modalidade").attr('required')
        } else {
            $("#modalidades").hide()
            $("#modalidade").removeAttr('required')
        }
    })
</script>
