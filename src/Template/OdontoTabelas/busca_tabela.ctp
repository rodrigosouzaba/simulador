<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<?= $this->Form->create('editarPercentual', ['id' => 'editarPercentual']); ?>
<div class="clearfix">&nbsp;</div>
<?php if ($odonto_tabelas) { ?> 
    <div class="col-md-4">
        <?= $this->Form->input('tabelaMatriz_id', ['options' => $odonto_tabelas, 'empty' => 'SELECIONE']); ?>

    </div>
    <div class="col-md-4">
        <?= $this->Form->input('tabelasSecundaria_id', ['options' => $odonto_tabelas, 'empty' => 'SELECIONE']); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('percentual', ['placeholder' => 'Percentual aplicado']); ?>
        <span id="helpBlock" class="help-block">Não há necessidade de digitar (%)</span>
    </div>
    <div class="col-md-12 centralizada">
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar', '#', ['class' => 'btn btn-md btn-primary ', 'role' => 'button', 'escape' => false, 'id' => 'editar', 'type' => 'submit']);
        ?>
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['action' => 'index'], ['class' => 'btn btn-md btn-default ', 'role' => 'button', 'escape' => false]);
        ?>
    </div>
    <?php
} else {
    ?>
    <div class="clearfix">&nbsp;</div>
    <p class="bg-danger centralizada">Nenhuma tabela encontrada</p>
<?php } ?>

<?= $this->Form->end() ?>

<script type="text/javascript">
    $("#tabelassecundaria-id").change(function () {
//            alert($("#tabelassecundaria-id").value() +" - "+$("#tabelamatriz-id").value());
        if ($("#tabelassecundaria-id").val() == $("#tabelamatriz-id").val()) {
            alert('Tabelas devem ser diferentes.');
            $("#percentual").prop('disabled', true);
        } else {
            $("#percentual").removeAttr('disabled');
        }

    });





    $("#editar").click(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>odonto-tabelas/editar_percentual/" + $("#editarPercentual").serialize(),
            data: $("#editarPercentual").serialize(),
//            beforeSend: function () {
//                $('#loadingMatriz').html("<img src='<?= $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
//            },
            success: function (data) {
//              $("#resposta").append(data);  
                window.location = "<?php echo $this->request->webroot ?>odonto-tabelas/";
            }
        });

    });

</script>