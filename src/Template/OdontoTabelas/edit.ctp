<style>
    #modalidades {
        display: none;
    }
</style>
<div class="col-xs-12 fonteReduzida">
    <?= $this->Form->create($odontoTabela) ?>
    <div class="col-xs-12 col-md-9">
        <?= $this->Form->input('nome', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('vigencia', ['Válida até', 'required' => 'required']) ?>
    </div>
    <div class="col-xs-12">
        <?= $this->Form->input('descricao', ['label' => 'Observação', 'required' => 'required']); ?>
    </div>
    <div class="clearfix">&nbsp;</div>

    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('titulares', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('minimo_vidas', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('maximo_vidas', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?php $tipo_pessoas = array('PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica') ?>
        <?= $this->Form->input('tipo_pessoa', ['options' => $tipo_pessoas, 'empty' => 'SELECIONE', 'required' => 'required']) ?>
    </div>

    <div class="clearfix">&nbsp;</div>
    <div class="col-xs-12 col-md-3">

        <?= $this->Form->input('estado_id', ['empty' => 'SELECIONE', 'label' => 'Estado', 'required' => 'required']); ?>
    </div>
    <div class="col-xs-12 col-md-3" id="operadoras">
        <?= $this->Form->input('odonto_operadora_id', ['options' => $odonto_operadoras, 'empty' => 'SELECIONE', 'label' => 'Operadora', 'value' => $odontoTabela['odonto_produto']['odonto_operadora']['id'], 'required' => 'required']); ?>
    </div>
    <div class="col-xs-12 col-md-3" id="odonto-produto">
        <?= $this->Form->input('odonto_produto_id', ['options' => $odontoProdutos, 'label' => 'Produto', 'empty' => 'SELECIONE', 'required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('odonto_atendimento_id', ['options' => $odontoAtendimentos, 'empty' => 'SELECIONE', 'label' => 'Área de Atendimento', 'required' => 'required']); ?>
    </div>
    <div class="clearfix">&nbsp;</div>

    <div class="col-xs-12 col-md-3" id="odonto-comercializacao">
        <?= $this->Form->input('odonto_comercializacao_id', ['options' => $odontoComercializacoes, 'empty' => 'SELECIONE', 'label' => 'Área de Comercialização', 'required' => 'required']); ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('preco_vida', ['label' => 'Preço por Vida', 'required' => 'required']) ?>
    </div>

    <div class="col-xs-12 col-md-3" id="modalidades">
        <?= $this->Form->input('modalidade', ['label' => 'Modalidade', 'options' => ['INDIVIDUAL' => 'INDIVIDUAL', 'ADESÃO' => 'ADESÃO'], 'empty' => 'SELECIONE']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?php $reembolsos = array('S' => 'SIM', 'N' => 'NÃO') ?>
        <?= $this->Form->input('reembolso', ['options' => $reembolsos, 'empty' => 'SELECIONE', 'label' => 'Possue reembolso?', 'required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">&nbsp;</div>

    <?= $this->element('botoesAdd'); ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        let tipo_pessoa = $("#tipo-pessoa").val();
        if (tipo_pessoa == "PF") {
            $("#modalidades").show();
            $("#modalidade").attr("required", "required");
        } else {
            $("#modalidades").hide()
            $("#modalidade").removeAttr("required");
        }
        $("#odonto-operadora-id").change(function() {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>odonto-tabelas/find_produtos/" + (this)
                    .value,
                data: $("#odonto-operadora-id").serialize(),
                beforeSend: function() {
                    $("#modalProcessando").modal('show')
                },
                success: function(data) {
                    $("#odonto-produto").empty();
                    $("#odonto-produto").append(data);
                    $("#modalProcessando").modal('hide')
                }
            });
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>odonto-tabelas/find_comercializacao/" + (
                    this).value + "/" + $("#estado-id").val(),
                data: $("#odonto-operadora-id").serialize(),
                beforeSend: function() {
                    $("#modalProcessando").modal('show')
                },
                success: function(data) {
                    $("#odonto-comercializacao").empty();
                    $("#odonto-comercializacao").append(data);
                    $("#modalProcessando").modal('hide')
                }
            });
        });
    });

    $("#tipo-pessoa").change(function() {
        let pessoa = $(this).val();
        if (pessoa == "PF") {
            $("#modalidades").show();
            $("#modalidade").attr('required', 'true');
        } else {
            $("#modalidade").removeAttr('required');
            $("#modalidades").hide();
        }
        $("#odonto-operadora-id").val('');
        $("#odonto-operadora-id").attr('disabled', 'true');
        $("#odonto-produto-id").val('');
        $("#odonto-produto-id").attr('disabled', 'true');
        $("#odonto-comercializacao-id").val('');
        $("#odonto-comercializacao-id").attr('disabled', 'true');
    });
    $("#estado-id").change(function() {
        if ($("#tipo-pessoa").val() != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>odonto-tabelas/getOperadora/" + $(this).val() + "/" + $("#tipo-pessoa").val(),
                success: function(data) {
                    $("#operadoras").empty();
                    $("#operadoras").append(data);
                }
            });
        }
    });
</script>
