<div class="col-md-3">
    <?= $this->Form->control('odonto_rede_id', ['empty' => 'SELECIONE REDE', 'options' => $odonto_redes]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('odonto_observacao_id', ['empty' => 'SELECIONE OBSERVAÇÃO', 'options' => $odonto_observacoes]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('odonto_reembolso_id', ['empty' => 'SELECIONE REEMBOLSO', 'options' => $odonto_reembolsos]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('odonto_carencia_id', ['empty' => 'SELECIONE CARENCIA', 'options' => $odonto_carencias]) ?>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-3">
    <?= $this->Form->control('odonto_dependente_id', ['empty' => 'SELECIONE DEPENDENTE', 'options' => $odonto_dependentes]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('odonto_documento_id', ['empty' => 'SELECIONE DOCUMENTO', 'options' => $odonto_documentos]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('odonto_formas_pagamento_id', ['empty' => 'SELECIONE FORMAS PAGAMENTO', 'options' => $odonto_formas_pagamentos]) ?>
</div>