<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
echo $this->Form->create('selecaoTabelas', ['id' => 'selecaoTabelas']);
if ($odonto_tabelas->count()) {
?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <td class="tituloTabela" style="width: 2%"><?= 'Sel.' ?></td>

                <td class="tituloTabela" style="width: 30%"><?= 'Produto' ?></td>
                <td class="tituloTabela">Preço por vida</td>
                <td class="tituloTabela"><?= 'Vidas' ?></td>
                <td class="tituloTabela"><?= 'Vigência' ?></td>
                <td class="tituloTabela text-center"><?= 'Prioridade' ?></td>

                <td class="actions tituloTabela"><?= __('Actions') ?></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($odonto_tabelas as $tabela) : ?>
                <tr>

                    <td style="width: 2%"><?= $this->Form->input('tabela' . $tabela->id, ['type' => 'checkbox', 'value' => $tabela->id, 'label' => '', 'class' => 'checkHabilita']) ?></td>

                    <td style="width: 40%">
                        <?php
                        if ((int)$tabela->new) {
                            echo "<b>" . $tabela->odonto_areas_comercializaco->odonto_operadora->nome . "</b><br/><small>" . $tabela->nome . "</small>";
                        } else {
                            echo "<b>" . $tabela->odonto_produto->nome . "</b><br/><small>" . $tabela->nome . "</small>";
                        }
                        ?>
                    </td>
                    <td>
                        <?= $this->Number->currency($tabela->preco_vida, "R$ ") ?>
                    </td>
                    <td><?= $tabela->minimo_vidas . " a " . $tabela->maximo_vidas ?></td>
                    <td><?= h($tabela->vigencia) ?></td>
                    <td class="text-center"><?= h($tabela->prioridade) ?></td>

                    <td class="actions" style="font-size: 90% !important">
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), '/odontoTabelas/edit/' . $tabela->id . ((int) $tabela->new ? '/1' : ''), ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar / Preços', 'sid' => 'odontoTabelas.edit']) ?>
                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-copy', 'aria-hidden' => 'true']), '/odontoTabelas/duplicar/' . $tabela->id . ((int) $tabela->new ? '/1' : ''), ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Duplicar', 'sid' => 'odontoTabelas.duplicar']) ?>

                        <?php
                        if ($sessao['role'] == 'admin') :
                            if ($tabela->validade == null || $tabela->validade == 0) {

                                echo $this->Html->link(
                                    $this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']),
                                    '#',
                                    [
                                        'class' => 'btn btn-sm btn-danger margemMinima validar',
                                        'escape' => false,
                                        'value' => $tabela->id,
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'SUSPENSA'
                                    ]
                                );
                            } else {

                                echo $this->Html->link(
                                    $this->Html->tag('span', '', ['class' => 'fa fa-check', 'aria-hidden' => 'true']),
                                    '#',
                                    array(
                                        'class' => 'btn btn-sm btn-success margemMinima invalidar',
                                        'escape' => false,
                                        'value' => $tabela->id,
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'ATIVA'
                                    )
                                );
                            }

                            echo $this->Html->link($this->Html->tag('span', '', [
                                'class' => 'fa fa-list',
                                'aria-hidden' => 'true'
                            ]), '#', [
                                'class' => 'btn btn-sm btn-default margemMinima alterarPrioridade',
                                'escape' => false,
                                'value' => $tabela->id,
                                'data-placement' => 'top',
                                'title' => 'Definir Prioridade',
                                'data-toggle' => "modal",
                                'data-target' => "#modalPrioridade"
                            ]);

                            if ($tabela->atualizacao == null || $tabela->atualizacao == 0) :
                                echo
                                $this->Html->link('', '#', [
                                    'class' => 'btn btn-sm btn-default fa fa-refresh colocarAtlz',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'value' => $tabela->id,
                                    'title' => 'Colocar em Atualização de Preços'
                                ]);
                            else :
                                echo $this->Html->link('', '#', [
                                    'class' => 'btn btn-sm btn-danger fa fa-refresh removerAtlz',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'value' => $tabela->id,
                                    'title' => 'Remover Atualização de Preços'
                                ]);
                            endif;


                            echo $this->Form->postLink(
                                $this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']),
                                array(
                                    'action' => 'delete', $tabela->id
                                ),
                                array(
                                    'class' => 'btn btn-sm btn-danger margemMinima',
                                    'escape' => false,
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'title' => 'Remover',
                                    'confirm' => 'Confirma exclusão da Tabela?'
                                )
                            );

                        endif;
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>

    <div class="col-md-12">
        <?=
        $this->Html->link($this->Html->tag('span', '', [
            'class' => 'fa fa-trash',
            'aria-hidden' => 'true'
        ]) . ' Deletar Tabelas', '#', [
            'class' => 'btn btn-sm btn-danger',
            'role' => 'button',
            'escape' => false,
            'data-toggle' => 'tooltip',
            'data-placement' => 'top',
            'title' => 'Deletar tabelas',
            'id' => 'deleteLote'
        ])
        ?>

        <?=
        $this->Html->link($this->Html->tag('span', '', [
            'class' => 'fa fa-percent',
            'aria-hidden' => 'true'
        ]) . ' Reajustar Tabelas', '#', [
            'class' => 'btn btn-sm btn-default',
            'role' => 'button',
            'escape' => false,
            'data-toggle' => 'tooltip',
            'data-placement' => 'top',
            'title' => 'Reajustar Tabelas',
            'id' => 'reajustarLote',
            'data-toggle' => "modal",
            'data-target' => "#modalTabelas"
        ])
        ?>
        <?=
        $this->Html->link($this->Html->tag('span', '', [
            'class' => 'fa fa-calendar',
            'aria-hidden' => 'true'
        ]) . ' Alterar Vigência', '#', [
            'class' => 'btn btn-sm btn-default',
            'role' => 'button',
            'escape' => false,
            'data-toggle' => 'tooltip',
            'data-placement' => 'top',
            'title' => 'Alterar Vigências',
            'id' => 'reajustarVigencia',
            'data-toggle' => "modal",
            'data-target' => "#modalTabelas"
        ])
        ?>
        <?= $this->Form->hidden('tabelas'); ?>

        <div id="pdf"></div>
    </div>
<?php } else {
?>
    <div class="clearfix">&nbsp;</div>
    <div class="col-sm-12">
        <p class="well well-sm centralizada">Nenhuma tabela encontrada</p>
    </div>
<?php } ?>


<!-- Modal -->
<div class="modal fade" id="modalTabelas" tabindex="-1" role="dialog" aria-labelledby="modalTabelasLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalTabelasLabel">Reajustar Tabelas</h4>
                <small> Formato 99.99 - Usar "." como separador de decimais. Não Há necessidade de digitar "%".</small>
            </div>
            <div class="modal-body" id="percentual" style="min-height: 150px !important;">
                <div class="clearfix"> &nbsp;</div>
                &nbsp;
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalPrioridade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Alterar Ordem de Prioridade da Tabela</h4>
                <small> As tabelas serão exibidas nas simulações de acordo com sua <b>Ordem de Prioridade</b> definida</small>
            </div>
            <div class="modal-body" id="prioridade" style="min-height: 150px !important;">
                <div class="clearfix"> &nbsp;</div>
                &nbsp;
            </div>

        </div>
    </div>
</div>
<?= $this->Form->end() ?>

<?= $this->element('processando'); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#deleteLote").hide();
        $("#reajustarLote").hide();

        $('#modalTabelas').on('shown.bs.modal', function() {
            $('#myInput').focus()
        });
        $('#modalPrioridade').on('shown.bs.modal', function() {
            $('#myInput').focus()
        });
        $("#deleteLote").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "odontoTabelas/deleteLote/" + $("#selecaoTabelas").serialize(),
                success: function(data) {}
            });
        });
        $(".validar").click(function() {
            $.ajax({
                type: "post",
                url: baseUrl + "odontoTabelas/validar/" + $(this).attr("value"),
                success: function(data) {
                    $("#operadora-id").trigger('change');

                }
            });
        });
        $(".invalidar").click(function() {
            $.ajax({
                type: "post",
                url: baseUrl + "odontoTabelas/invalidar/" + $(this).attr("value"),
                success: function(data) {
                    $("#operadora-id").trigger('change');

                }
            });
        });
        $("#reajustarLote").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "odontoTabelas/reajusteLote/" + $("#selecaoTabelas").serialize(),
                success: function(data) {
                    $("#percentual").empty();
                    $("#percentual").append(data);

                }
            });
        });
        $("#reajustarVigencia").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "odontoTabelas/reajusteVigencia/" + $("#selecaoTabelas").serialize(),
                success: function(data) {
                    $("#percentual").empty();
                    $("#percentual").append(data);

                }
            });
        });
        $(".alterarPrioridade").click(function() {
            $.ajax({
                //                type: "post",
                data: $("#prioridadeForm").serialize(),
                url: baseUrl + "odontoTabelas/prioridade/" + $(this).attr("value"),
                success: function(data) {
                    $("#prioridade").empty();
                    $("#prioridade").append(data);

                }
            });
        });


    });

    $(".colocarAtlz").click(function() {
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>odontoTabelas/colocarAtlz/" + $(this).attr(
                'value'),
            beforeSend: function() {
                $("#loader").click();
            },
            success: function(data) {
                $('#operadora-id').trigger('change');

            }
        });
    });
    $(".removerAtlz").click(function() {
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>odontoTabelas/removerAtlz/" + $(this).attr(
                'value'),
            beforeSend: function() {
                $("#loader").click();
            },
            success: function(data) {
                $('#operadora-id').trigger('change');

            }
        });
    });

    $(".checkHabilita").click(function() {
        $("#deleteLote").show();
        $("#reajustarLote").show();
    });
</script>