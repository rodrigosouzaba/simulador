<?= $this->Form->input('odonto_operadora_id', ['options' => $odonto_operadoras, 'label' => '', 'empty' => 'Selecione a OPERADORA']); ?>
<script type="text/javascript">
$("#odonto-operadora-id").change(function () {
    if($(this).val()){
	    $.ajax({
	        type: "POST",
	        url: "<?php echo $this->request->webroot ?>odonto-tabelas/filtro_operadora/",
            data: $("#odonto-operadora-id").serialize(),
			beforeSend: function () {
		        $('#loader').click();
			},
			success: function (data) {
			    $("#listaTabelas").empty();
			    $("#listaTabelas").append(data);
			    $("#loading").empty();
			    $("#fechar").click();
			}
		});
	}else{
		$("#listaTabelas").empty();
	}
});
</script>
<?= $this->element("processando");?>
