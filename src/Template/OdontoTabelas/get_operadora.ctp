<?= $this->Form->input('odonto_operadora_id', ['options' => $operadoras, 'empty' => 'SELECIONE', 'label' => 'Operadora', 'required' => 'required']); ?>

<script>
    $("#odonto-operadora-id").change(function() {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>odonto-tabelas/find_produtos/" + (this)
                .value,
            data: $("#odonto-operadora-id").serialize(),
            success: function(data) {
                $("#odonto-produto").empty();
                $("#odonto-produto").append(data);
            }
        });
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>odonto-tabelas/find_comercializacao/" + (
                this).value + "/" + $("#estado-id").val(),
            data: $("#odonto-operadora-id").serialize(),
            success: function(data) {
                $("#odonto-comercializacao").empty();
                $("#odonto-comercializacao").append(data);
            }
        });
    });
</script>
