<?php
$tipo_pessoa = ['PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica'];
?>
<div class="col-md-3 fonteReduzida">
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Tabela Odonto', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
    ?>
</div>

<div class="col-md-2 fonteReduzida">
    <?= $this->Form->input('estados', ['label' => '', 'options' => $estados, 'empty' => 'Selecione ESTADO']); ?>
</div>

<div class="col-md-2 fonteReduzida">
    <?= $this->Form->input('tipo_pessoa', ['label' => '', 'options' => $tipo_pessoa, 'empty' => 'Selecione TIPO PESSOA', 'disabled' => 'true']); ?>
</div>

<div id="operadoras" class="col-md-2">
    <div class="fonteReduzida">
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
    </div>
</div>

<div id="respostaFiltroOperadora">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <td class="tituloTabela" style="width: 2%"><?= 'Sel.' ?></td>

                <td class="tituloTabela" style="width: 30%"><?= 'Produto' ?></td>
                <td class="tituloTabela">Preço por vida</td>
                <td class="tituloTabela"><?= 'Vidas' ?></td>
                <td class="tituloTabela"><?= 'Vigência' ?></td>
                <td class="tituloTabela text-center"><?= 'Prioridade' ?></td>

                <td class="actions tituloTabela"><?= __('Actions') ?></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($odonto_tabelas as $tabela) : ?>
                <tr>

                    <td style="width: 2%"><?= $this->Form->input('tabela' . $tabela->id, ['type' => 'checkbox', 'value' => $tabela->id, 'label' => '', 'class' => 'checkHabilita']) ?></td>

                    <td style="width: 40%"><?= "<b>" . $tabela->nome . "</b>" ?></td>
                    <td> <?= $this->Number->currency($tabela->preco_vida, "R$ ") ?></td>
                    <td><?= $tabela->minimo_vidas . " a " . $tabela->maximo_vidas ?></td>
                    <td><?= h($tabela->vigencia) ?></td>
                    <td class="text-center"><?= h($tabela->prioridade) ?></td>

                    <td class="actions" style="font-size: 90% !important">
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), '/odontoTabelas/edit/' . $tabela->id, ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar / Preços', 'sid' => 'odontoTabelas.edit']) ?>
                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-copy', 'aria-hidden' => 'true']), '/odontoTabelas/duplicar/' . $tabela->id, ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Duplicar', 'sid' => 'odontoTabelas.duplicar']) ?>

                        <?php
                        if ($tabela->validade == null || $tabela->validade == 0) {

                            echo $this->Html->link(
                                $this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']),
                                '#',
                                [
                                    'class' => 'btn btn-sm btn-danger margemMinima validar',
                                    'escape' => false,
                                    'value' => $tabela->id,
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'title' => 'SUSPENSA'
                                ]
                            );
                        } else {

                            echo $this->Html->link(
                                $this->Html->tag('span', '', ['class' => 'fa fa-check', 'aria-hidden' => 'true']),
                                '#',
                                array(
                                    'class' => 'btn btn-sm btn-success margemMinima invalidar',
                                    'escape' => false,
                                    'value' => $tabela->id,
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'title' => 'ATIVA'
                                )
                            );
                        }

                        echo $this->Html->link($this->Html->tag('span', '', [
                            'class' => 'fa fa-list',
                            'aria-hidden' => 'true'
                        ]), '#', [
                            'class' => 'btn btn-sm btn-default margemMinima alterarPrioridade',
                            'escape' => false,
                            'value' => $tabela->id,
                            'data-placement' => 'top',
                            'title' => 'Definir Prioridade',
                            'data-toggle' => "modal",
                            'data-target' => "#modalPrioridade"
                        ]);

                        if ($tabela->atualizacao == null || $tabela->atualizacao == 0) :
                            echo
                            $this->Html->link('', '#', [
                                'class' => 'btn btn-sm btn-default fa fa-refresh colocarAtlz',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'value' => $tabela->id,
                                'title' => 'Colocar em Atualização de Preços'
                            ]);
                        else :
                            echo $this->Html->link('', '#', [
                                'class' => 'btn btn-sm btn-danger fa fa-refresh removerAtlz',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'value' => $tabela->id,
                                'title' => 'Remover Atualização de Preços'
                            ]);
                        endif;


                        echo $this->Form->postLink(
                            $this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']),
                            array(
                                'action' => 'delete', $tabela->id
                            ),
                            array(
                                'class' => 'btn btn-sm btn-danger margemMinima',
                                'escape' => false,
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Remover',
                                'confirm' => 'Confirma exclusão da Tabela?'
                            )
                        );

                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
<script type="text/javascript">
    $("#estados").change(function() {
        let estado = $(this).val();
        if (estado != '') {
            $("#tipo-pessoa").removeAttr('disabled');
        }
    })
    $("#tipo-pessoa").change(function() {
        $.ajax({
            type: "GET",
            url: baseUrl + "/odontoTabelas/filtroIndex/" + $("#estados").val() + "/" + $("#tipo-pessoa").val(),
            success: function(data) {
                $("#operadoras").empty();
                $("#operadoras").append(data);
            }
        });
    });
</script>