<style>
    #modalidades {
        display: none;
    }
</style>
<div class="container fonteReduzida">
    <?= $this->Form->create($odontoTabela) ?>
    <div class="col-xs-12 col-md-9">
        <?= $this->Form->input('nome') ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <label>Válida até</label>
        <?= $this->Form->input('vigencia', ['label' => false]) ?>
    </div>
    <div class="col-xs-12">
        <?= $this->Form->input('odonto_area_comercializacao_id', ['label' => 'Áreas de Comercialização', 'options' => $areas_comercializacoes, 'empty' => 'SELECIONE']); ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="spacer-xs">&nbsp;</div>
    <div class="col-xs-12">
        <?= $this->Form->input('descricao', ['label' => 'Observação']); ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="spacer-xs">&nbsp;</div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('titulares', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('minimo_vidas', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('maximo_vidas', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?php $tipo_pessoas = array('PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica') ?>
        <?= $this->Form->input('tipo_pessoa', ['options' => $tipo_pessoas, 'empty' => 'SELECIONE', 'required' => 'required']) ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('odonto_atendimento_id', ['options' => $odontoAtendimentos, 'empty' => 'SELECIONE', 'label' => 'Área de Atendimento', 'required' => 'required']); ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?= $this->Form->input('preco_vida', ['label' => 'Preço por Vida', 'required' => 'required', 'step' => "0.01"]) ?>
    </div>
    <div class="col-xs-12 col-md-3" id="modalidades">
        <?= $this->Form->input('modalidade', ['label' => 'Modalidade', 'options' => ['INDIVIDUAL' => 'INDIVIDUAL', 'ADESÃO' => 'ADESÃO'], 'empty' => 'SELECIONE']) ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?php $reembolsos = array('S' => 'SIM', 'N' => 'NÃO') ?>
        <?= $this->Form->input('reembolso', ['options' => $reembolsos, 'empty' => 'SELECIONE', 'label' => 'Possue reembolso?', 'required' => 'required']) ?>
    </div>

    <!-- NOVOS RELACIONAMENTOS -->
    <div class="spacer-md clearfix">&nbsp;</div>
    <h4 class="centralizada">Novas Associações</h4>
    <div class="col-md-12 fonteReduzida" id="field-associacoes">
        <div class="col-md-3">
            <?= $this->Form->control('odonto_rede_id', ['empty' => 'SELECIONE REDE', 'options' => isset($odonto_redes) && !empty($odonto_redes) ? $odonto_redes : '']) ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->control('odonto_observacao_id', ['empty' => 'SELECIONE OBSERVACAO', 'options' => isset($odonto_observacoes) && !empty($odonto_observacoes) ? $odonto_observacoes : '']) ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->control('odonto_reembolso_id', ['empty' => 'SELECIONE REEMBOLSO', 'options' => isset($odonto_reembolsos) && !empty($odonto_reembolsos) ? $odonto_reembolsos : '']) ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->control('odonto_carencia_id', ['empty' => 'SELECIONE CARENCIA', 'options' => isset($odonto_carencias) && !empty($odonto_carencias) ? $odonto_carencias : '']) ?>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="col-md-3">
            <?= $this->Form->control('odonto_dependente_id', ['empty' => 'SELECIONE DEPENDENTE', 'options' => isset($odonto_dependentes) && !empty($odonto_dependentes) ? $odonto_dependentes : '']) ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->control('odonto_documento_id', ['empty' => 'SELECIONE DOCUMENTO', 'options' => isset($odonto_documentos) && !empty($odonto_documentos) ? $odonto_documentos : '']) ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->control('odonto_formas_pagamento_id', ['empty' => 'SELECIONE FORMAS PAGAMENTO', 'options' => isset($odonto_formas_pagamentos) && !empty($odonto_formas_pagamentos) ? $odonto_formas_pagamentos : '']) ?>
        </div>
    </div>
    <!-- NOVOS RELACIONAMENTOS -->

    <div class="spacer-md">&nbsp;</div>
    <?= $this->element('botoesAdd'); ?>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    $("#odonto-area-comercializacao-id").change(function() {
        var comercializacao = $(this).val();
        $.ajax(`${baseUrl}odontoTabelas/filtroAssociacoes/${comercializacao}`)
            .then(function(data) {
                $("#field-associacoes").empty();
                $("#field-associacoes").append(data);
            })
    })

    $("#estado-id").change(function() {
        if ($("#tipo-pessoa").val() != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>odonto-tabelas/getOperadora/" + $(this).val() + "/" + $("#tipo-pessoa").val(),
                success: function(data) {
                    $("#operadoras").empty();
                    $("#operadoras").append(data);
                }
            });
        }
    });

    $("#tipo-pessoa").change(function() {
        let pessoa = $(this).val();
        if (pessoa == "PF") {
            $("#modalidades").show()
            $("#modalidade").attr('required')
        } else {
            $("#modalidades").hide()
            $("#modalidade").removeAttr('required')
        }
    })
</script>