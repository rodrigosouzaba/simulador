<?= $this->Form->create('prioridade', ['id' => 'prioridadeForm']); ?>
<div class="col-md-8 fonteReduzida">
    <?= $this->Form->input('odonto_tabela_id', ['type' => 'hidden', 'value' => $odonto_tabela_id]) ?>
    <?= $this->Form->input('prioridade', ['label' => 'Ordem de Prioridade']); ?>
</div>
<div class="col-md-4 fonteReduzida centralizada" style="margin-top:30px;">
    <?=
        $this->Html->link($this->Html->tag('span', '', [
            'class' => 'fa fa-floppy-o',
            'aria-hidden' => 'true'
        ]) . ' Salvar', '#', [
            'class' => 'btn btn-primary btn-sm',
            'role' => 'button',
            'id' => 'salvarPrioridade',
            'escape' => false
        ]);
    ?>

    <button type="button" id="fecharModal" class="btn btn-sm btn-default" data-dismiss="modal"> <span class="fa fa-remove" aria-hidden="true"></span> Cancelar</button>
</div>


<div class="col-md-12 fonteReduzida">
    <div class="fonteReduzida text-center centralizada" id="loadingModal">&nbsp;</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#salvarPrioridade").click(function() {
            $.ajax({
                type: "POST",
                data: $("#prioridadeForm").serialize(),
                url: "<?php echo $this->request->webroot ?>odonto-tabelas/prioridade/" + $("#prioridadeForm").serialize(),
                beforeSend: function() {
                    $('#loadingModal').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
                },
                success: function(data) {
                    $('#fecharModal').trigger('click');
                    $("#operadora-id").trigger('change');
                }
            });
        });
    });
</script>
