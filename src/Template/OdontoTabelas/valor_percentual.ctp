<div class="col-md-4 col-md-offset-2 fonteReduzida">    
    <h4 style="float: left;">Estado </h4> <div id="loadingMatriz" style="float: right;"></div>
    <?= $this->Form->input('estado_id', ['options' => $estados, 'label' => '', 'empty' => 'Selecione o ESTADO']); ?>
</div>
<div class="col-md-4 fonteReduzida">    
    <h4 style="float: left;">Operadora </h4> <div id="loadingMatriz" style="float: right;"></div>
    <div id="operadoras">
        <?= $this->Form->input('odonto_operadora_id_fake', ['options' => '', 'label' => '', 'empty' => 'Selecione a OPERADORA', 'disabled' => true]); ?>
    </div>
</div>
<div id="tabelas" class="fonteReduzida"></div>

<div class="spacer-sm">&nbsp;</div>
<div class="spacer-sm">&nbsp;</div>
<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>



<script type="text/javascript">
    $("#estado-id").change(function() {
        $.ajax({
            type:"POST",
            url: "<?php echo $this->request->webroot ?>odonto-tabelas/filtro-estado",
            data: $("#estado-id").serialize(),
            success: function(data){
                $("#operadoras").empty();
                $("#operadoras").append(data);
            }
        });
    })
    $("#odonto-operadora-id").change(function () {

        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>odonto-tabelas/busca_tabela/" + $("#odonto-operadora-id").val(),
            data: $("#operadora-id").serialize(),
            beforeSend: function () {
                $('#loadingMatriz').html("<img src='<?= $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                $("#tabelas").empty();
                $("#tabelas").append(data);
                $("#loadingMatriz").empty();
            }
        });


    });

</script>


