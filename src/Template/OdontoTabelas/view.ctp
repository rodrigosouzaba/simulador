<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Odonto Tabela'), ['action' => 'edit', $odontoTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Odonto Tabela'), ['action' => 'delete', $odontoTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $odontoTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Comercializacoes'), ['controller' => 'OdontoComercializacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Comercializaco'), ['controller' => 'OdontoComercializacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Atendimentos'), ['controller' => 'OdontoAtendimentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Atendimento'), ['controller' => 'OdontoAtendimentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Odonto Produtos'), ['controller' => 'OdontoProdutos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Odonto Produto'), ['controller' => 'OdontoProdutos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="odontoTabelas view large-9 medium-8 columns content">
    <h3><?= h($odontoTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($odontoTabela->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Validade') ?></th>
            <td><?= h($odontoTabela->validade) ?></td>
        </tr>
        <tr>
            <th><?= __('Reembolso') ?></th>
            <td><?= h($odontoTabela->reembolso) ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo Pessoa') ?></th>
            <td><?= h($odontoTabela->tipo_pessoa) ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Comercializaco') ?></th>
            <td><?= $odontoTabela->has('odonto_comercializaco') ? $this->Html->link($odontoTabela->odonto_comercializaco->id, ['controller' => 'OdontoComercializacoes', 'action' => 'view', $odontoTabela->odonto_comercializaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Atendimento') ?></th>
            <td><?= $odontoTabela->has('odonto_atendimento') ? $this->Html->link($odontoTabela->odonto_atendimento->id, ['controller' => 'OdontoAtendimentos', 'action' => 'view', $odontoTabela->odonto_atendimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Odonto Produto') ?></th>
            <td><?= $odontoTabela->has('odonto_produto') ? $this->Html->link($odontoTabela->odonto_produto->id, ['controller' => 'OdontoProdutos', 'action' => 'view', $odontoTabela->odonto_produto->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Estado') ?></th>
            <td><?= $odontoTabela->has('estado') ? $this->Html->link($odontoTabela->estado->id, ['controller' => 'Estados', 'action' => 'view', $odontoTabela->estado->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($odontoTabela->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Preco Vida') ?></th>
            <td><?= $this->Number->format($odontoTabela->preco_vida) ?></td>
        </tr>
        <tr>
            <th><?= __('Minimo Vidas') ?></th>
            <td><?= $this->Number->format($odontoTabela->minimo_vidas) ?></td>
        </tr>
        <tr>
            <th><?= __('Maximo Vidas') ?></th>
            <td><?= $this->Number->format($odontoTabela->maximo_vidas) ?></td>
        </tr>
        <tr>
            <th><?= __('Titulares') ?></th>
            <td><?= $this->Number->format($odontoTabela->titulares) ?></td>
        </tr>
        <tr>
            <th><?= __('Prioridade') ?></th>
            <td><?= $this->Number->format($odontoTabela->prioridade) ?></td>
        </tr>
        <tr>
            <th><?= __('Vigencia') ?></th>
            <td><?= h($odontoTabela->vigencia) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($odontoTabela->descricao)); ?>
    </div>
</div>
