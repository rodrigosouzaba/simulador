<?php $this->assign('title', $title); ?>
<div class="container fonteReduzida">
    <?= $this->Form->create($opcionai) ?>
    <div class="col-xs-12">
        <?= $this->Form->input('nome') ?>
    </div>
    <div class="col-md-6">
        <?= $this->Form->input('nova', ['options' => [1 => 'Sim', 0 => 'Não'], 'label' => 'Nova', 'empty' => 'Nova']) ?>
    </div>
    <div id="field-operadoras" class="col-xs-6">
        <?= $this->Form->input('operadora_id', ['options' => $operadoras, 'empty' => 'SELECIONE']) ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-xs-12">
        <?= $this->Form->input('descricao', ['label' => 'Observação']) ?>
    </div>

    <?= $this->element('botoesAdd') ?>
    <?= $this->Form->end() ?>
</div>
<script>
    $("#nova").change(function() {
        var action = '<?= $this->request->getParam('controller') ?>'.charAt(0).toLowerCase() + '<?= $this->request->getParam('controller') ?>'.slice(1);
        $.get(baseUrl + action + '/filtroNova/' + $(this).val(), function(data) {
            $("#field-operadoras").empty();
            $("#field-operadoras").append(data);
        })
    })
</script>