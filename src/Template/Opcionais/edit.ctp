<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $opcionai->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $opcionai->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Opcionais'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="opcionais form large-9 medium-8 columns content">
    <?= $this->Form->create($opcionai) ?>
    <fieldset>
        <legend><?= __('Edit Opcionai') ?></legend>
        <?php
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
