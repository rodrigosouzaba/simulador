<div class="col-md-3 fonteReduzida">
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Dados de Dependentes', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addRamo', 'sid' => 'opcionais.add']);
    ?>
</div>

<?= $this->element('filtro_interno') ?>

<div class="col-md-1 centralizada" id="loading"></div>
<div id="respostaFiltroOperadora"></div>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('id') ?></th>-->
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($opcionais as $opcional) : ?>
            <tr>
                <td><?= $opcional->nome ?></td>
                <td><?= $opcional->descricao ?></td>
                <td>
                    <?php
                    //                        debug($produto);die();
                    if (isset($opcional->operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $opcional->operadora->imagen->caminho . "/" . $opcional->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $opcional->operadora->detalhe;
                    } else {
                        echo $opcional->operadora->nome . " - " . $opcional->operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $opcional->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'opcionais.edit']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $opcional->id], ['confirm' => __('Confirma exclusão?', $opcional->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'opcionais.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>