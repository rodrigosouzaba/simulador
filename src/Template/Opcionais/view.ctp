<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Opcionai'), ['action' => 'edit', $opcionai->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Opcionai'), ['action' => 'delete', $opcionai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $opcionai->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Opcionais'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Opcionai'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="opcionais view large-9 medium-8 columns content">
    <h3><?= h($opcionai->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($opcionai->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($opcionai->descricao)); ?>
    </div>
</div>
