<div class="container">
    <?php echo $this->Form->create($operadora, ['class' => "form-inline"]) ?>
    <div class="col-xs-12 col-md-4">
        <?php echo $this->Form->input('nome'); ?>
    </div>
    <div class="col-xs-12 <?= $new ? 'col-md-8' : 'col-md-4' ?>">
        <?php echo $this->Form->input('detalhe'); ?>
    </div>
    <?php if (!$new) : ?>
        <div class="col-xs-12 col-md-4">
            <?= $this->Form->input('estado_id'); ?>
        </div>
        <div class="clearfix">&nbsp;</div>
    <?php endif; ?>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('prioridade'); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('url', ['label' => 'URL Corretor Parceiro - Link Material de Apoio']); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('url_rede', ['label' => 'Url rede credenciada (site operadora)']); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('atendimento_cliente', ['label' => 'Telefone de Atendimento ao Cliente', 'placeholder' => "(71) 3333-3333 ou (71) 9 8888-8888"]); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('atendimento_corretor', ['label' => 'Telefone de Atendimento ao Corretor', 'placeholder' => "(71) 3333-3333 ou (71) 9 8888-8888"]); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('site', ['label' => 'Site Operadora']); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('via_boleto', ['label' => 'Url Segunda Via do Boleto']); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('app_android', ['label' => 'Url Aplicativo Android']); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('app_ios', ['label' => 'Url Aplicativo IOS']); ?>
    </div>


    <?= $this->element('botoesAdd') ?>

    <?= $this->Form->end() ?>
</div>