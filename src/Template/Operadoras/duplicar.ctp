<?= $this->Form->create($operadora, ['class' => "form-inline"]) ?>
<div class="col-xs-12 col-md-6">
	<?= $this->Form->input('nome');?>
</div>
<div class="col-xs-12 col-md-6">
	<?= $this->Form->input('detalhe'); ?>
</div>
<div class="col-xs-12 col-md-6">
	<?= $this->Form->input('estado_id', ['empty' => 'SELECIONE']); ?>
</div>
<div class="col-xs-12 col-md-6">
	<?= $this->Form->input('prioridade'); ?>
</div>
<div class="col-xs-12 col-md-6">
	<?= $this->Form->input('url', ['label' => 'URL Corretor Parceiro - Link Material de Apoio']);?>
</div>
<div class="col-xs-12 col-md-6">
	<?= $this->Form->input('url_rede', ['label' => 'Url rede credenciada (site operadora)']);?>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-12 centralizada">
	<?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-save', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => 'btn btn-md btn-primary', 'id' => 'duplicarSalvar']) ?>
	<?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true',]) . ' Cancelar',["action" => "index"], ['class' => 'btn btn-md btn-default', 'role' => 'button', "escape" =>false]) ?>
</div>
<?= $this->element('processando');?>

<?= $this->Form->end() ?>
<script type="text/javascript">
	$(document).ready(function(){
		
		$("#duplicarSalvar").click(function() {
			$("#loader").click();
		});
	});
</script>