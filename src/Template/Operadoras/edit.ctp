<?php echo $this->Form->create($operadora, ['class' => "form-inline"]) ?>
<div class="col-xs-12 col-md-6">
    <?php echo $this->Form->input('nome'); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?php echo $this->Form->input('detalhe'); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?php echo $this->Form->input('estado_id', ['empty' => 'SELECIONE']); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('prioridade'); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('url', ['label' => 'URL Corretor Parceiro - Link Material de Apoio']); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('url_rede', ['label' => 'Url rede credenciada (site operadora)']); ?>
</div>
<?= $this->element('botoesAdd') ?>




<?= $this->Form->end() ?>