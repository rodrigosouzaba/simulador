<div id="ajax">
    <div class="col-xs-12" style="padding-top: 15px;">
        <div class="col-xs-3">
            <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Operadora', ['controller' => 'operadoras', 'action' => 'add', $this->request->getParam('action') == 'new' ? 1 : ''], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addOperadora', 'sid' => 'operadoras.add']);
            ?>
        </div>
        <div class="col-xs-2 flex">
            <?= $this->Form->input('search', ['placeholder' => 'Digite a operadora', 'label' => false, 'list' => 'lista-operadoras', 'style' => 'width: 80%;', 'templates' => ['inputContainer' => '{{content}}']]) ?>
            <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'fa fa-search']), ['id' => 'search-button', 'class' => 'search-button', 'style' => 'width: 20%']); ?>
            <datalist id="lista-operadoras">
                <?php foreach ($operadoras as $operadora) : ?>
                    <option value="<?= $operadora->nome ?>">
                    <?php endforeach; ?>
            </datalist>
        </div>
        <div class="col-xs-2">
            <?= $this->Form->input('estado_id', ['options' => $estados, "class" => "fonteReduzida", 'label' => false, 'empty' => 'Selecione ESTADO']); ?>
        </div>
        <div class="col-xs-2">
            <?= $this->Form->input('status', ['options' => ['ATIVA' => 'ATIVAS', 'EM ATUALIZACAO' => 'ATUALIZAÇÃO', 'INATIVA' => 'INATIVAS', 'OCULTA' => 'OCULTAS'], "class" => "fonteReduzida", 'label' => false, 'empty' => 'Selecione STATUS']); ?>
        </div>
    </div>
    <?= $this->element('order') ?>
    <div id="operadoras">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?= $this->Html->link('Prioridade', '#', ['onclick' => 'order("prioridade", "ajax")']) ?></th>
                    <?php if ($this->request->action != 'new') : ?>
                        <th><?= $this->Html->link('Estado', '#', ['onclick' => 'order("estado_id", "operadoras")']) ?></th>
                    <?php endif; ?>
                    <th><?= $this->Html->link('Nome', '#', ['onclick' => 'order("nome", "ajax")']) ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($operadoras as $operadora) : ?>
                    <tr>
                        <td> <?= $operadora->prioridade ?></td>
                        <?php if ($this->request->action != 'new') : ?>
                            <td><?= $operadora->estado->nome ?></td>
                        <?php endif; ?>
                        <td>
                            <?php
                            if (isset($operadora->imagen->caminho)) {
                                echo $this->Html->image("../" . $operadora->imagen->caminho . "/" . $operadora->imagen->nome, ['class' => 'logoMiniatura']) . " " . $operadora->nome . "  " . $operadora->detalhe;
                            } else {
                            ?>
                                <?= $operadora->nome . " - " . $operadora->detalhe ?>
                        </td>


                    <?php }
                    ?>
                    <td class="actions">
                        <?=
                        $this->Html->link('', '/operadoras/duplicar/' . $operadora->id, [
                            'class' => 'btn btn-sm btn-default fa fa-copy',
                            'sid' => 'operadoras.duplicar',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Duplicar Operadora'
                        ])
                        ?>

                        <?=
                        $this->Html->link('', '/operadoras/addImagem/' . $operadora->id, [
                            'title' => __('Adicionar Logomarca'),
                            'sid' => 'operadoras.addImagem',
                            'class' => 'btn btn-sm btn-default fa fa-image',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Logomarca'
                        ])
                        ?>
                        <?=
                        $this->Html->link('', ($this->request->action != 'new' ? '/operadoras/edit/' . $operadora->id : '/operadoras/edit/' . $operadora->id . '/1'), [
                            'title' => __('Editar'),
                            'sid' => 'operadoras.edit',
                            'class' => 'btn btn-sm btn-default fa fa-pencil',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Editar'
                        ])
                        ?>
                        <span data-toggle="tooltip" data-placement="top" data-original-title="Prioridade" title="Definir Prioridade">
                            <?=
                            $this->Html->link('', '#', [
                                'title' => __('Prioridade'),
                                'class' => 'btn btn-sm btn-default fa fa-list alterarPrioridade',
                                'role' => 'button',
                                'sid' => 'operadoras.prioridade',
                                'escape' => false,
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Alterar Prioridade',
                                'data-toggle' => "modal",
                                'data-target' => "#modalPrioridade",
                                'value' => $operadora->id
                            ])
                            ?>
                        </span>
                        <!-- <?=
                                $this->Form->postLink('', '/operadoras/delete/' . $operadora->id, [
                                    'confirm' => __('Tem certeza que deseja excluir Operadora {0}?', $operadora->nome . "  " . $operadora->detalhes), 'title' => __('Deletar'),
                                    'class' => 'btn btn-sm btn-default fa fa-trash',
                                    'sid' => 'operadoras.delete',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'title' => 'Deletar'
                                ])
                                ?> -->

                        <?php switch ($operadora->status):
                            case 'OCULTA': ?>

                                <?=
                                $this->Html->link('',  '/operadoras/ocultar/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-danger fa fa-eye',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.ocultar',
                                    'data-placement' => 'top',
                                    'title' => 'Reexibir Operadora'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/operadoras/atualizacao/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-refresh',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.atualizacao',
                                    'data-placement' => 'top',
                                    'title' => 'Colocar em Atualização de Preços'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/operadoras/ativacao/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-check',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.ativacao',
                                    'data-placement' => 'top',
                                    'title' => 'Inativar Operadora'
                                ])
                                ?>
                            <?php break;

                            case 'EM ATUALIZAÇÃO': ?>
                                <?=
                                $this->Html->link('', '/operadoras/ocultar/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-eye',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.ocultar',
                                    'data-placement' => 'top',
                                    'title' => 'Ocultar Operadora'
                                ])
                                ?>
                                <?=
                                $this->Html->link('',  '/operadoras/atualizacao/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-danger fa fa-refresh',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.atualizacao',
                                    'data-placement' => 'top',
                                    'title' => 'Remover Atualização de Preços'
                                ])
                                ?>
                                <?=
                                $this->Html->link('',  '/operadoras/ativacao/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-check',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.ativacao',
                                    'data-placement' => 'top',
                                    'title' => 'Inativar Operadora'
                                ])
                                ?>
                            <?php break;

                            case 'INATIVA': ?>
                                <?=
                                $this->Html->link('', '/operadoras/ocultar/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-eye',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.ocultar',
                                    'data-placement' => 'top',
                                    'title' => 'Ocultar Operadora'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/operadoras/atualizacao/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-refresh',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.atualizacao',
                                    'data-placement' => 'top',
                                    'title' => 'Colocar em Atualização de Preços'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/operadoras/ativacao/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-danger fa fa-close',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.ativacao',
                                    'data-placement' => 'top',
                                    'title' => 'Ativar Operadora'
                                ])
                                ?>
                            <?php break;

                            default: ?>
                                <?=
                                $this->Html->link('', '/operadoras/ocultar/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-eye',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.ocultar',
                                    'data-placement' => 'top',
                                    'title' => 'Ocultar Operadora'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/operadoras/atualizacao/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-refresh',
                                    'data-toggle' => 'tooltip',
                                    'sid' => 'operadoras.atualizacao',
                                    'data-placement' => 'top',
                                    'title' => 'Colocar em Atualização de Preços'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/operadoras/ativacao/' . $operadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-check',
                                    'sid' => 'operadoras.ativacao',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'title' => 'Inativar Operadora'
                                ])
                                ?>
                        <?php break;
                        endswitch;
                        ?>
                    </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalPrioridade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Alterar Ordem de Prioridade da Operadora</h4>
                    <small> As Operadoras serão exibidas nas simulações de acordo com sua <b>Ordem de Prioridade</b>.</small>
                </div>
                <div class="modal-body" id="prioridade" style="min-height: 150px !important;">
                    <div class="form-inline col-xs-6 col-xs-offset-1 text-center">
                        <?= $this->Form->create(null, ['id' => 'form-prioridade', 'url' => ['action' => 'prioridade']]) ?>
                        <?= $this->Form->hidden('operadora_id', ['value' => '', 'label' => false, 'id' => 'operadora-id']) ?>
                        <div class="form-group">
                            <?= $this->Form->input('prioridade', ['label' => 'Ordem de Prioridade', 'label' => false, 'type' => 'number', 'style' => 'margin: 0;']) ?>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                    <div class="col-xs-3">
                        <?= $this->Form->button('Validar', ['class' => 'btn btn-sm btn-default col-xs-12', 'id' => 'btn-validar', 'style' => 'height: 37px']) ?>
                    </div>
                    <span id="text-prioridade" class="text-danger small col-xs-8 col-xs-offset-2 text-center"></span>
                    <?= $this->Form->button('Salvar', ['class' => 'btn btn-success col-xs-4 col-xs-offset-4', 'id' => 'btn-priorizar', 'disabled' => true, 'style' => 'margin-top: 15px;']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<?= $this->element('processando'); ?>
<script type="text/javascript">
    function filtrar() {
        let texto = $("#search").val();
        let estado = $("#estado-id").val();
        let status = $("#status").val();
        if (texto !== "" || estado !== "" || status !== "") {
            $.ajax({
                type: 'POST',
                url: baseUrl + '/operadoras/filtro/',
                data: {
                    'search': texto,
                    'estado': estado,
                    'status': status
                },
                success: function(data) {
                    $("#operadoras").empty();
                    $("#operadoras").append(data);
                }
            });
        }
    }

    $("#search-button").click(function() {
        filtrar()
    });

    $("#search").keyup(function(e) {
        if (e.which == 13) {
            filtrar()
        }
    });

    $("#estado-id").change(function() {
        filtrar()
    });

    $("#status").change(function() {
        filtrar()
    });

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('#modalPrioridade').on('shown.bs.modal', function() {
            $('#myInput').focus()
        });
    });
    $(".alterarPrioridade").click(function() {
        let operadora_id = $(this).attr('value');
        $("#form-prioridade > #operadora-id").empty();
        $("#form-prioridade > #operadora-id").val(operadora_id);
    });

    $("#btn-validar").click(function() {
        let form = $("#form-prioridade").serialize();
        $.ajax({
            type: 'POST',
            data: form,
            url: baseUrl + 'operadoras/validacaoPrioridade',
            beforeSend: function() {
                $("#text-prioridade").empty();
                $("#text-prioridade").append('Validando...');
            },
            success: function(data) {
                if (data.result) {
                    $("#btn-priorizar").removeAttr('disabled');
                    $("#text-prioridade").empty();
                    $("#text-prioridade").append('Não há operadora com esta prioridade.');
                } else {
                    $("#btn-priorizar").attr('disabled', 'disabled');
                    $("#text-prioridade").empty();
                    $("#text-prioridade").append('Esta prioridade já está em uso.');
                }
            }
        });
    });

    $("#btn-priorizar").click(function() {
        $("#form-prioridade").submit()
    });
</script>