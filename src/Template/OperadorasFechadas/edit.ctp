<div class="col-xs-12">
    <?= $this->Form->create($operadorasFechada) ?>
    <fieldset>
        <?php
        echo $this->Form->input('nome');
        echo $this->Form->input('arquivos._ids', ['type' => 'select', 'options' => $arquivos]);
        ?>
    </fieldset>
    <?= $this->element('botoesAdd'); ?>
    <?= $this->Form->end() ?>
</div>
