<?=
$this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Operadora Fechada', [ 'action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addOperadora']);
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($operadorasFechadas as $operadorasFechada): ?>
            <tr>
                <td><?= h($operadorasFechada->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Arquivos'), ['action' => 'arquivos', $operadorasFechada->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $operadorasFechada->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $operadorasFechada->id], ['confirm' => __('Are you sure you want to delete # {0}?', $operadorasFechada->id)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
</div>
