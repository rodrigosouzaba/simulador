<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Operadoras Fechada'), ['action' => 'edit', $operadorasFechada->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Operadoras Fechada'), ['action' => 'delete', $operadorasFechada->id], ['confirm' => __('Are you sure you want to delete # {0}?', $operadorasFechada->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Operadoras Fechadas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Operadoras Fechada'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="operadorasFechadas view large-9 medium-8 columns content">
    <h3><?= h($operadorasFechada->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($operadorasFechada->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($operadorasFechada->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= $this->Number->format($operadorasFechada->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Arquivos') ?></h4>
        <?php if (!empty($operadorasFechada->arquivos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Caminho') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Tipo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($operadorasFechada->arquivos as $arquivos): ?>
            <tr>
                <td><?= h($arquivos->id) ?></td>
                <td><?= h($arquivos->nome) ?></td>
                <td><?= h($arquivos->caminho) ?></td>
                <td><?= h($arquivos->created) ?></td>
                <td><?= h($arquivos->modified) ?></td>
                <td><?= h($arquivos->tipo) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Arquivos', 'action' => 'view', $arquivos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Arquivos', 'action' => 'edit', $arquivos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Arquivos', 'action' => 'delete', $arquivos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $arquivos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
