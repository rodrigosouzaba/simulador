<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $operadorasFechadasArquivo->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $operadorasFechadasArquivo->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Operadoras Fechadas Arquivos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Operadoras Fechadas'), ['controller' => 'OperadorasFechadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Operadoras Fechada'), ['controller' => 'OperadorasFechadas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="operadorasFechadasArquivos form large-9 medium-8 columns content">
    <?= $this->Form->create($operadorasFechadasArquivo) ?>
    <fieldset>
        <legend><?= __('Edit Operadoras Fechadas Arquivo') ?></legend>
        <?php
            echo $this->Form->input('operadora_fechada_id', ['options' => $operadorasFechadas]);
            echo $this->Form->input('arquivo_id', ['options' => $arquivos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
