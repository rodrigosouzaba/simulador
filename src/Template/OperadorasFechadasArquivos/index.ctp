<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Operadoras Fechadas Arquivo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Operadoras Fechadas'), ['controller' => 'OperadorasFechadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Operadoras Fechada'), ['controller' => 'OperadorasFechadas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="operadorasFechadasArquivos index large-9 medium-8 columns content">
    <h3><?= __('Operadoras Fechadas Arquivos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('operadora_fechada_id') ?></th>
                <th><?= $this->Paginator->sort('arquivo_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($operadorasFechadasArquivos as $operadorasFechadasArquivo): ?>
            <tr>
                <td><?= $this->Number->format($operadorasFechadasArquivo->id) ?></td>
                <td><?= $operadorasFechadasArquivo->has('operadoras_fechada') ? $this->Html->link($operadorasFechadasArquivo->operadoras_fechada->id, ['controller' => 'OperadorasFechadas', 'action' => 'view', $operadorasFechadasArquivo->operadoras_fechada->id]) : '' ?></td>
                <td><?= $operadorasFechadasArquivo->has('arquivo') ? $this->Html->link($operadorasFechadasArquivo->arquivo->id, ['controller' => 'Arquivos', 'action' => 'view', $operadorasFechadasArquivo->arquivo->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $operadorasFechadasArquivo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $operadorasFechadasArquivo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $operadorasFechadasArquivo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $operadorasFechadasArquivo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
