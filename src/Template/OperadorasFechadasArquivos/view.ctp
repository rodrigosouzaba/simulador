<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Operadoras Fechadas Arquivo'), ['action' => 'edit', $operadorasFechadasArquivo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Operadoras Fechadas Arquivo'), ['action' => 'delete', $operadorasFechadasArquivo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $operadorasFechadasArquivo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Operadoras Fechadas Arquivos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Operadoras Fechadas Arquivo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Operadoras Fechadas'), ['controller' => 'OperadorasFechadas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Operadoras Fechada'), ['controller' => 'OperadorasFechadas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="operadorasFechadasArquivos view large-9 medium-8 columns content">
    <h3><?= h($operadorasFechadasArquivo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Operadoras Fechada') ?></th>
            <td><?= $operadorasFechadasArquivo->has('operadoras_fechada') ? $this->Html->link($operadorasFechadasArquivo->operadoras_fechada->id, ['controller' => 'OperadorasFechadas', 'action' => 'view', $operadorasFechadasArquivo->operadoras_fechada->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Arquivo') ?></th>
            <td><?= $operadorasFechadasArquivo->has('arquivo') ? $this->Html->link($operadorasFechadasArquivo->arquivo->id, ['controller' => 'Arquivos', 'action' => 'view', $operadorasFechadasArquivo->arquivo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($operadorasFechadasArquivo->id) ?></td>
        </tr>
    </table>
</div>
