<?= $this->element('menu'); ?>
<?= $this->element('modal-login'); ?>
<?= $this->element('modal_avaliacao'); ?>
<?= $this->element('modal_fale_conosco'); ?>
<?= $this->element('modal_fale_conosco_visitante'); ?>
<?= $this->element('modal_fale_conosco', ['id' => 'modal-leads', 'message' => 'Essa funcionalidade só está disponivel para parceiros autorizados']); ?>
<div class="modal fade" id="modal-alerta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body centralizada">
                <h4>Esta funcionalidade não está habilitada para o seu usuário.</h4>
            </div>
            <div class="modal-footer" style="text-align: center !important;">
                <button type="button" class="btn btn-default centralizada" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script>
    var baseURL = baseUrl = baseSub = "<?php echo $this->request->webroot; ?>";
    var userPermissions = <?= json_encode($sessao['permissions']); ?>;
</script>
<?= $this->Html->script('basics.js') . "\n"; ?>