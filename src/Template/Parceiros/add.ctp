
<div class="col-xs-12">    
    <?= $this->Form->create($parceiro) ?>
    <div class="col-md-9">
        <?= $this->Form->input('nome'); ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->input('data_nascimento', ['minYear' => date('Y') - 90, 'maxYear' => date('Y')]); ?>
    </div>
    <div class="col-md-12">
        <?= $this->Form->input('email'); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('uf', ['options' => $estados, 'required' => 'required', 'empty' => 'SELECIONE', 'label' => 'UF']); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('cidade', ['required' => 'required']); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('ddd', ['required' => 'required']); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('telefone', ['required' => 'required']); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('rg'); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('cpf'); ?>
    </div>

    <div class="col-md-4">
        <?= $this->Form->input('situacao', ['options' => array(1 => 'ATIVO', 2 => 'DESLIGADO'), 'empty' => 'SELECIONE', 'label' => 'Situação', 'required' => 'required']); ?>
    </div>
    <div class="col-md-4" id="data_desligamento">
        <?= $this->Form->input('desligamento', ['label' => 'Data do Desligamento']); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('perfil', ['options' => array(1 => 'CORRETOR PARCEIRO', 2 => 'GERENCIAL PARCEIRO'), 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('unidade', ['label' => 'Unidade de Negócio', 'required' => 'required']); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('comissionado', ['label' => 'Código do Comissionado', 'required' => 'required']); ?>
    </div>
    <div class="col-md-4">
        <?= $this->Form->input('login', ['label' => 'Identificação de LOGIN (3 letras)', 'required' => 'required']); ?>
    </div>


    <?php
    echo $this->Form->hidden('cnpj_corpar', ['value' => '18254042000114']);
    echo $this->Form->hidden('razao_social', ['value' => 'CORPAR ASSESSORIA E CONSULTORIA COMERCIAL LTDA']);
    ?>
    <?= $this->element('botoesAdd') ?>
    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
    $("#data_desligamento").hide();
    $("#situacao").change(function () {
        if ($("#situacao").val() === '2') {
            $("#data_desligamento").show();
        } else {
            $("#data_desligamento").hide();

        }

    });

</script>

