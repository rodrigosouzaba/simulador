<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $parceiro->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $parceiro->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Parceiros'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="parceiros form large-9 medium-8 columns content">
    <?= $this->Form->create($parceiro) ?>
    <fieldset>
        <legend><?= __('Edit Parceiro') ?></legend>
        <?php
            echo $this->Form->input('rg');
            echo $this->Form->input('nome');
            echo $this->Form->input('cpf');
            echo $this->Form->input('email');
            echo $this->Form->input('data_nascimento');
            echo $this->Form->input('cnpj_corpar');
            echo $this->Form->input('razao_social');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
