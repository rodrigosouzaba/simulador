<div class="col-md-3" >
    <h3><?= __('Parceiros') ?></h3>
</div>
<div class="col-md-6" >
    <div class="col-md-8" >

        <?= $this->Form->create('encontrarCalculo', ['id' => 'encontrarCalculo']) ?>
        <?= $this->Form->input('cpf_busca', ['placeholder' => 'Buscar por CPF', 'label' => '']); ?>
        <?php $this->Form->end(); ?>
    </div>
    <div class="col-md-4" >
    </div>
</div>
<div class="col-md-3 fonteReduzida " >
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Parceiro', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar direita', 'role' => 'button', 'escape' => false, 'id' => 'salvar', 'style' => 'margin-top: 20px;']);
    ?>
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-file', 'aria-hidden' => 'true']) . ' Gerar Arquivo TXT', ['action' => 'txt'], ['class' => 'btn btn-sm btn-default botaoSalvar direita', 'role' => 'button', 'escape' => false, 'id' => 'salvar', 'style' => 'margin-top: 20px;margin-right: 5px']);
    ?>
</div>
<table cellpadding="0" cellspacing="0" class="table table-condensed table-striped table-hover">
    <thead>
        <tr>
            <th style="width: 2%"><?= $this->Paginator->sort('id', ['label' => 'Cód']) ?></th>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('rg') ?></th>
            <th><?= $this->Paginator->sort('cpf') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th style="text-align: right;"><?= $this->Paginator->sort('data_nascimento') ?></th>
            <th><?= $this->Paginator->sort('created', ['label' => 'Cadastro']) ?></th>
<!--            <th><?= $this->Paginator->sort('cnpj_corpar') ?></th>
            <th><?= $this->Paginator->sort('razao_social') ?></th>-->
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($parceiros as $parceiro): ?>
            <tr >
                <td ><?= $this->Number->format($parceiro->id) ?></td>
                <td><?= h($parceiro->nome) ?></td>
                <td><?= h($parceiro->rg) ?></td>
                <td><?= h($parceiro->cpf) ?></td>
                <td><?= h($parceiro->email) ?></td>
                <td style="text-align: right;"><?= h($parceiro->data_nascimento) ?></td>
                <td><?= h($parceiro->created) ?></td>
    <!--                <td><?= h($parceiro->cnpj_corpar) ?></td>
                <td><?= h($parceiro->razao_social) ?></td>-->
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $parceiro->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $parceiro->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $parceiro->id], ['confirm' => __('Confirma exclusão de  # {0}?', $parceiro->nome)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
<!--</div>-->
