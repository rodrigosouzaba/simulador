<div class="col-md-3" >
    <h3 style="color:red;"><?= h($parceiro->nome) ?></h3>
</div>
<div class="col-md-9 fonteReduzida " >
    <?=
    $this->Html->link('Voltar', ['action' => 'index'], ['class' => 'btn btn-sm btn-primary botaoSalvar direita', 'role' => 'button', 'escape' => false, 'style' => 'margin-top: 20px;']);
    ?>
    <?=
    $this->Html->link('Editar', ['action' => 'edit', $parceiro->id], ['class' => 'btn btn-sm btn-default botaoSalvar direita', 'role' => 'button', 'escape' => false, 'style' => 'margin-top: 20px;margin-right:5px;']);
    ?>
</div>
<div class="col-md-12">
    <table class="table table-condensed">
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'Código'?></td>
            <td><?= h($parceiro->id) ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= __('Rg') ?></td>
            <td><?= h($parceiro->rg) ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= __('Cpf') ?></td>
            <td><?= h($parceiro->cpf) ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= __('Email') ?></td>
            <td><?= h($parceiro->email) ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= __('Cnpj Corpar') ?></td>
            <td><?= h($parceiro->cnpj_corpar) ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= __('Razao Social') ?></td>
            <td><?= h($parceiro->razao_social) ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= __('Data Nascimento') ?></td>
            <td><?= h($parceiro->data_nascimento) ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'Data de Cadastro'?></td>
            <td><?= h($parceiro->created) ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'Situação'?></td>
            <td><?= h($parceiro->situacao === '1' ? 'ATIVO' : 'DESLIGADO: '.$parceiro->desligamento) ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'Perfil'?></td>
            <td><?= h($parceiro->perfil === '1' ? 'CORRETOR PARCEIRO' : 'GERENCIAL PARCEIRO') ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'UF'?></td>
            <td><?= $parceiro->uf ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'Cidade'?></td>
            <td><?= $parceiro->cidade ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'Telefone'?></td>
            <td><?= $parceiro->ddd." ".$parceiro->telefone ?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'Unidade'?></td>
            <td><?= $parceiro->unidade?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'Comissionado'?></td>
            <td><?= $parceiro->comissionado?></td>
        </tr>
        <tr>
            <td style="width: 20%; font-weight: bold;"><?= 'Login'?></td>
            <td><?= $parceiro->login?></td>
        </tr>
    </table>
</div>
