AQUIadasd<?php if (isset($tabelasOrdenadas)) { ?>
    <div class="jumbotron fonteReduzida">

        À <b><?= $simulacao['nome'] ?></b> - AC: <b><?= $simulacao['contato'] ?></b>.<br/>
        Primeiramente, agradecemos pelo seu contato.<br/>
        Informamos que os custos e as condições abaixo são determinadas por suas respectivas operadoras.<br/>
        Estamos apresentando proposta(s) da(s) seguintes operadora(s) para sua apreciação:<br/>
        <ul style="display: flex; margin-left: 0 !important;">
            <?php
            foreach ($tabelasOrdenadas as $chave => $tabela) {
                echo "<b><li class='fonteReduzida' style='list-style: none; margin: 0 10px 0 0'>" . $chave . "</li></b>";
            }
            ?>
        </ul>
        <?php //debug($simulacao)?>
    </div>
    <?php
    $idOperadora = null;
    foreach ($tabelasOrdenadas as $chave => $tabela) {
//    debug($chave);
//    debug(array_values($tabela)[0]);
//    die();
//    $idOperadora = $tabela['operadora']['id'];
        ?>

        <div class="panel panel-primary tabela">
            <div class="panel-heading"> <span class="panel-title"><?= $chave ?></h3> </div>
            <div class="panel-body">


                <ul class="list-group semMargem">
                    <li class="list-group-item informacao">
                        <h5><?= array_values($tabela)[0]['ramo']['nome'] . " | " . array_values($tabela)[0]['modalidade']['nome'] ?> </h5>
                    </li>

                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <td class="beneficiarios">Faixa Etária</td>
                                <?php foreach ($tabela as $produto) { ?>
                                    <td  class="beneficiarios"><?= $produto['produto']['nome'] ?></td>
                                    <?php
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($simulacao['faixa1']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>0 à 18 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa1'] * $produto['faixa1']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php if ($simulacao['faixa2']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>19 à 23 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa2'] * $produto['faixa2']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php if ($simulacao['faixa3']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>24 à 28 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa3'] * $produto['faixa3']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php if ($simulacao['faixa4']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>29 à 33 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa4'] * $produto['faixa4']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php if ($simulacao['faixa5']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>34 à 38 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa5'] * $produto['faixa5']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php if ($simulacao['faixa6']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>39 à 43 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa6'] * $produto['faixa6']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php if ($simulacao['faixa7']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>44 à 48 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa7'] * $produto['faixa7']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php if ($simulacao['faixa8']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>49 à 53 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa8'] * $produto['faixa8']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php if ($simulacao['faixa9']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>54 à 58 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa9'] * $produto['faixa9']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php if ($simulacao['faixa10']) { ?>
                                <tr>
                                    <td class='beneficiariosTransparente'>+ de 59 anos</td>
                                    <?php
                                    foreach ($tabela as $produto) {
                                        ?>
                                        <td  class='beneficiariosTransparente centralizada'>
                                            <?= $this->Number->currency($simulacao['faixa10'] * $produto['faixa10']) ?>
                                        </td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>



                            <tr>
                                <td class="beneficiariosTransparente negrito">Total</td>
                                <?php foreach ($tabela as $produto) { ?>
                                    <td class="beneficiariosTransparente negrito centralizada">
                                        <?= $this->Number->currency($simulacao['faixa1'] * $produto['faixa1'] + $simulacao['faixa2'] * $produto['faixa2'] + $simulacao['faixa3'] * $produto['faixa3'] + $simulacao['faixa4'] * $produto['faixa4'] + $simulacao['faixa5'] * $produto['faixa5'] + $simulacao['faixa6'] * $produto['faixa6'] + $simulacao['faixa7'] * $produto['faixa7'] + $simulacao['faixa8'] * $produto['faixa8'] + $simulacao['faixa9'] * $produto['faixa9'] + $simulacao['faixa10'] * $produto['faixa10']) ?>
                                    </td>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>    


                </ul>
                <?php
                if ($simulacao['rede'] == 1) {
                    ?>

                    <div class="tituloField negrito"> Rede Credenciada</div>
                    <div class="fonteReduzida">
                        <?php
                        foreach ($tabela as $produto) {
                            echo $produto['produto']['nome'] . ": ";
                            echo $produto['modalidade']['rede']['descricao'];
                            echo "<div class='clearfix'>&nbsp;</div>";
                        }
                        ?>

                    </div>

                    <?php
                }
                if ($simulacao['carencia'] == 1) {
                    ?>
                    <div class="clearfix">&nbsp;</div>
                    <div class="tituloField negrito"> Carência</div>
                    <div class="fonteReduzida">
                        <?php
                        foreach ($tabela as $produto) {
                            echo $produto['produto']['nome'] . ": ";
                            echo $produto['modalidade']['carencia']['descricao'];
                            echo "<div class='clearfix'>&nbsp;</div>";
                        }
                        ?>
                    </div>

                    <?php
                }
                if ($simulacao['reembolso'] == 1) {
                    ?>
                    <div class="clearfix">&nbsp;</div>
                    <div class="tituloField negrito"> Reembolso</div>
                    <div class="fonteReduzida">

                        <?php
                        foreach ($tabela as $produto) {
                            echo $produto['produto']['nome'] . ": ";
                            echo $produto['modalidade']['reembolso']['descricao'];
                            echo "<div class='clearfix'>&nbsp;</div>";
                        }
                        ?>

                    </div>

                    <?php
                }
                if ($simulacao['opcionais'] == 1) {
                    ?>
                    <div class="clearfix">&nbsp;</div>
                    <div class="tituloField negrito"> Opcionais</div>
                    <div class="fonteReduzida">
                        <?php
                        foreach ($tabela as $produto) {
                            echo $produto['produto']['nome'] . ": ";
                            echo $produto['modalidade']['opcionai']['descricao'];
                            echo "<div class='clearfix'>&nbsp;</div>";
                        }
                        ?>               

                    </div>

                    <?php
                }
                if ($simulacao['info'] == 1) {
                    ?>
                    <div class="clearfix">&nbsp;</div>
                    <div class="tituloField negrito"> Outras Informações</div>
                    <div class="fonteReduzida">

                        <?php
                        foreach ($tabela as $produto) {
                            echo $produto['produto']['nome'] . ": ";
                            echo $produto['modalidade']['informaco']['descricao'];
                            echo "<div class='clearfix'>&nbsp;</div>";
                        }
                        ?>   
                    </div>

                    <?php
                }
                ?>

            </div>
        </div>    
        <?php
    }
//debug($simulacao);
    ?>
    <div class="col-md-12 centralizada">

        <?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-save', 'aria-hidden' => 'true']) . ' Salvar', '#', ['class' => 'btn btn-lg btn-primary', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Salvar Simulação', 'id' => 'salvar']); ?>
        <?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', $btnCancelar, ['class' => 'btn btn-lg btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Retornar', 'id' => 'retornar']); ?>

    </div>
<div id="salvarSimulacao"></div>
<?php } else {
    ?>
    <div class="clearfix">&nbsp;</div>
    <div class="alert alert-danger centralizada" role="alert"> Nenhuma tabela encontrada dentro dos Filtros selecionados</div>

    <?php //debug($simulacao) ?>
<?php } ?>
    
    
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    
     $("#salvar").click(function () {
        $.ajax({
            type: "post",
            data: $("#simulacao").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/add/" + $("#simulacao").serialize(),
            success: function (data) {
                $("#salvarSimulacao").empty();
                $("#salvarSimulacao").append(data);

            }
        });
    });
</script>
<?php
 $CakePdf = new \CakePdf\Pdf\CakePdf();
    $CakePdf->template('download', 'pdf/pdf');
    $CakePdf->viewVars($this->viewVars);
    // Get the PDF string returned
//    $pdf = $CakePdf->output();
    // Or write it to file directly
    $pdf = $CakePdf->write(APP . 'files' . DS . 'newsletter.pdf');
        ?>