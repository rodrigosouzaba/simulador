<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pdffiltros'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Abrangencias'), ['controller' => 'Abrangencias', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Abrangencia'), ['controller' => 'Abrangencias', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['controller' => 'TiposProdutos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['controller' => 'TiposProdutos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pdffiltros form large-9 medium-8 columns content">
    <?= $this->Form->create($pdffiltro) ?>
    <fieldset>
        <legend><?= __('Add Pdffiltro') ?></legend>
        <?php
            echo $this->Form->input('simulacao_id');
            echo $this->Form->input('abrangencia_id', ['options' => $abrangencias, 'empty' => true]);
            echo $this->Form->input('tipo_id', ['options' => $tipos, 'empty' => true]);
            echo $this->Form->input('tipo_produto_id', ['options' => $tiposProdutos, 'empty' => true]);
            echo $this->Form->input('coparticipacao');
            echo $this->Form->input('tipo_contratacao');
            echo $this->Form->input('filtroreembolso');
            echo $this->Form->input('simulacoes._ids', ['options' => $simulacoes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
