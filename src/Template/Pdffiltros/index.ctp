<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pdffiltro'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Abrangencias'), ['controller' => 'Abrangencias', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Abrangencia'), ['controller' => 'Abrangencias', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['controller' => 'TiposProdutos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['controller' => 'TiposProdutos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pdffiltros index large-9 medium-8 columns content">
    <h3><?= __('Pdffiltros') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('simulacao_id') ?></th>
                <th><?= $this->Paginator->sort('abrangencia_id') ?></th>
                <th><?= $this->Paginator->sort('tipo_id') ?></th>
                <th><?= $this->Paginator->sort('tipo_produto_id') ?></th>
                <th><?= $this->Paginator->sort('coparticipacao') ?></th>
                <th><?= $this->Paginator->sort('tipo_contratacao') ?></th>
                <th><?= $this->Paginator->sort('filtroreembolso') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pdffiltros as $pdffiltro): ?>
            <tr>
                <td><?= $this->Number->format($pdffiltro->id) ?></td>
                <td><?= $this->Number->format($pdffiltro->simulacao_id) ?></td>
                <td><?= $pdffiltro->has('abrangencia') ? $this->Html->link($pdffiltro->abrangencia->id, ['controller' => 'Abrangencias', 'action' => 'view', $pdffiltro->abrangencia->id]) : '' ?></td>
                <td><?= $pdffiltro->has('tipo') ? $this->Html->link($pdffiltro->tipo->id, ['controller' => 'Tipos', 'action' => 'view', $pdffiltro->tipo->id]) : '' ?></td>
                <td><?= $pdffiltro->has('tipos_produto') ? $this->Html->link($pdffiltro->tipos_produto->id, ['controller' => 'TiposProdutos', 'action' => 'view', $pdffiltro->tipos_produto->id]) : '' ?></td>
                <td><?= h($pdffiltro->coparticipacao) ?></td>
                <td><?= $this->Number->format($pdffiltro->tipo_contratacao) ?></td>
                <td><?= h($pdffiltro->filtroreembolso) ?></td>
                <td><?= h($pdffiltro->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pdffiltro->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pdffiltro->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pdffiltro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pdffiltro->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
