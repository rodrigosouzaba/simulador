<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>

<div class="col-md-12 centralizada">
    <h3>Gerencie seus perfis empresariais de maneira prática e inteligente</h3>
    <div class="clearfix">&nbsp;</div>
    <!--    <div class="col-md-6 col-md-offset-3 centralizada" style="padding: 20px;">
            <ul class="nav nav-pills centralizada nav-justified">
                <li><a data-toggle="pill" href="#individual">Digitar cada data de nascimento</a></li>
                <li><a data-toggle="pill" href="#arquivo">Copiar de algum arquivo</a><small>Excel, Word, PDF</small></li>
            </ul> 
        </div>-->
    <!--<div class="tab-content">-->
    <!--        <div id="individual" class="tab-pane fade">
                <div class="col-md-12 centralizada well well-sm">
                    <h3 style="margin-top: 0 !important;"> Qual o total de vidas da Empresa? </h3>
                    &nbsp;
                    <div class="col-xs-12 centralizada">
    <?= $this->Form->create('detalhes', ['id' => 'detalhes']) ?>
    
                        <div class="col-xs-12 col-md-2 col-md-offset-2 centralizada">
    <?= $this->Form->input('titulares', ['class' => 'centralizada']) ?> 
                        </div>
                        <div class="col-xs-12 col-md-2 centralizada">
    <?= $this->Form->input('dependentes', ['class' => 'centralizada']) ?>        
                        </div>
                        <div class="col-xs-12 col-md-2 centralizada">
    <?= $this->Form->input('agregados', ['class' => 'centralizada']) ?>        
                        </div>
                        <div class="col-xs-12 col-md-2 centralizada">
    <?= $this->Form->input('afastados', ['class' => 'centralizada']) ?>        
                        </div>
                    </div>
                    <div class="col-md-12 centralizada">
    <?=
    $this->Html->link($this->Html->tag('span', '', [
                'class' => '',
                'aria-hidden' => 'true']) . ' Detalhar', '#', [
        'class' => 'btn btn-md btn-primary',
        'role' => 'button',
        'escape' => false,
        'data-toggle' => 'tooltip',
        'data-placement' => 'top',
        'id' => 'detalhar'])
    ?>
                    </div>  
    <?= $this->Form->end() ?>
                </div>
                <div class="col-xs-12 fonteReduzida centralizada">
    
                    <div class="col-xs-12 centralizada" id="formvidas" >
                    </div>
    
                </div>
    
    
            </div>-->
    <!--<div id="arquivo" class="tab-pane fade">-->
    <div class="col-md-8 col-md-offset-2 centralizada well well-sm">
        <h3>Digite as datas de nascimento em suas respectivas categorias</h3>
        <?= $this->Form->create('datas', ['id' => 'datas']) ?>
        <?= $this->Form->hidden('user_id', ['value' => $sessao['id'], 'type' => 'hidden']) ?>
        <?= $this->Form->hidden('status', ['value' => 'Criado', 'type' => 'hidden']) ?>
        <table class="table table-condensed">
            <tr>
                <td colspan="3" class="centralizada">
                    <?= $this->Form->input('nome', ['label' => 'Nome da Empresa', 'class' => 'centralizada']) ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="centralizada">Masculino</td>
                <td class="centralizada">Feminino</td>
            </tr>
            <tr>
                <td class="centralizada centralizadaVertical"><h4>Titulares</h4></td>
                <td>                            
                    <?= $this->Form->input('Vidas.Titulares.m', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea', 'placeholder' => 'Digite ou cole as datas aqui no formato 01/01/2000']) ?>
                </td>
                <td>
                    <?= $this->Form->input('Vidas.Titulares.f', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea', 'placeholder' => 'Digite ou cole as datas aqui no formato 01/01/2000']) ?>
                </td>
            </tr>
            <tr>
                <td class="centralizada centralizadaVertical"><h4>Dependentes</h4></td>
                <td>                            
                    <?= $this->Form->input('Vidas.Dependentes.m', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea', 'placeholder' => 'Digite ou cole as datas aqui no formato 01/01/2000']) ?>
                </td>
                <td>
                    <?= $this->Form->input('Vidas.Dependentes.f', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea', 'placeholder' => 'Digite ou cole as datas aqui no formato 01/01/2000']) ?>
                </td>
            </tr>
            <tr>
                <td class="centralizada centralizadaVertical"><h4>Agregados</h4></td>
                <td>                            
                    <?= $this->Form->input('Vidas.Agregados.m', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea', 'placeholder' => 'Digite ou cole as datas aqui no formato 01/01/2000']) ?>
                </td>
                <td>
                    <?= $this->Form->input('Vidas.Agregados.f', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea', 'placeholder' => 'Digite ou cole as datas aqui no formato 01/01/2000']) ?>
                </td>
            </tr>
            <tr>
                <td class="centralizada centralizadaVertical"><h4>Afastados</h4></td>
                <td>                            
                    <?= $this->Form->input('Vidas.Afastados.m', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea', 'placeholder' => 'Digite ou cole as datas aqui no formato 01/01/2000']) ?>
                </td>
                <td>
                    <?= $this->Form->input('Vidas.Afastados.f', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea', 'placeholder' => 'Digite ou cole as datas aqui no formato 01/01/2000']) ?>
                </td>
            </tr>
        </table>

        <div class="col-md-12">
            <div class="clearfix">&nbsp;</div>
            <div class="col-md-12 centralizada">
                <?= $this->Form->button( 'Calcular Faixas Etárias', ['class' => 'btn btn-md btn-primary ', 'role' => 'button', 'escape' => false]); ?>    
<!--
                <?=
                $this->Html->button($this->Html->tag('span', '', [
                            'class' => '',
                            'aria-hidden' => 'true']) . ' Calcular Faixas Etárias', '#', [
                    'class' => 'btn btn-md btn-primary',
                    'role' => 'button',
                    'escape' => false,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                    'id' => 'enviar'])
                ?>
-->
            </div>  
            <div id="resposta" style="text-align: left;"></div>
        </div>
        <?= $this->Form->end(); ?>




    </div>
    <!--</div>-->
</div>

<script type="text/javascript">
    //    $("#detalhes").hide();

    $("#enviar").click(function () {
//            alert();
        $.ajax({
            type: "post",
            data: $("#datas").serialize(),
            url: "<?php echo $this->request->webroot ?>perfis-empresariais/add/",
            success: function (data) {
                $("#resposta").empty();
                $("#resposta").append(data);

            }
        });
    });

</script>