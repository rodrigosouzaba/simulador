<h4 class="centralizada"><?= __('Perfis Empresariais') ?></h4>

<div class="content col-xs-12" style="padding: 0 !important">
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Perfil Empresarial', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'salvar']);
    ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id', ['label' => 'Código']) ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('created', ['label' => 'Data']) ?></th>
                <th class="centralizada"><?= 'Total de vidas' ?></th>
                <th class="centralizada"> <?= $this->Paginator->sort('status') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($perfisEmpresariais as $perfisEmpresariai): ?>

                <tr>
                    <td><?= $this->Number->format($perfisEmpresariai->id) ?></td>
                    <td><?= $perfisEmpresariai->nome ?></td>
                    <td><?= h($perfisEmpresariai->created) ?></td>
                    <td class="centralizada"><?= count($perfisEmpresariai['detalhes_perfis_empresariais']) ?></td>
                    <td class="centralizada"><?= h($perfisEmpresariai->status) ?></td>
                    <td class="actions">
                        <?=
                        $this->Html->link('', ['action' => 'view', $perfisEmpresariai->id], [
                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Visualizar Perfil',
                            'class' => 'btn btn-sm btn-default glyphicon glyphicon-eye-open'])
                        ?>
                      
                        <?=
                        $this->Form->postLink(
                                __('<i class="glyphicon glyphicon-trash"></i>'), [
                            'action' => 'delete',
                            $perfisEmpresariai->id
                                ], [
                            'escape' => false,
                            'class' => 'btn btn-sm btn-danger',
                            'confirm' => __('Confirma exclusão?')
                                ]
                        )
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
