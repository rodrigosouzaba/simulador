<div class="col-md-12">
    <h3 class="centralizada"><?= $perfisEmpresariai->nome?></h3>
    <h5 class="centralizada">Distribuição das quantidades de vidas por faixa Etária</h5>
    <table class="table table-condensed table-striped table-hover" >
        <tbody>
            <tr>
                <td class="centralizada centralizadaVertical" rowspan="2"> Faixas</td>
                <?php if (($titularesM + $titularesF) > 0) {
                    if($titularesM>0 && $titularesF>0){
                        $colspanT = 3;
                    }else{
                        $colspanT = 2;
                    }
                    if($dependentesM>0 && $dependentesF>0){
                        $colspanD = 3;
                    }else{
                        $colspanD = 2;
                    }
                    if($agregadosM>0 && $agregadosF>0){
                        $colspanAg = 3;
                    }else{
                        $colspanAg = 2;
                    }
                    if($afastadosM>0 && $afastadosF>0){
                        $colspanAf = 3;
                    }else{
                        $colspanAf = 2;
                    }
                    ?>
                    <td class="centralizada" colspan="<?= $colspanT?>"> Titulares</td>
                <?php } ?>
                <?php if (($dependentesM + $dependentesF) > 0) { ?>
                    <td class="centralizada" colspan="<?= $colspanD?>"> Dependentes</td>
                <?php } ?>
                <?php if (($agregadosM + $agregadosF) > 0) { ?>
                    <td class="centralizada" colspan="<?= $colspanAg?>"> Agregados</td>
                <?php } ?>
                <?php if (($afastadosM + $afastadosF) > 0) { ?>
                    <td class="centralizada" colspan="<?= $colspanAf?>"> Afastados</td>
                <?php } ?>
                </b>
            </tr>
            <tr>
                <?php if ($titularesM > 0) { ?>
                    <td class="centralizada" > Masculino</td>
                <?php } ?>

                <?php if ($titularesF > 0) { ?>
                    <td class="centralizada" > Feminino</td>
                <?php } ?>

                <?php if (($titularesM + $titularesF) > 0) { ?>
                    <td class="centralizada" > <b>Total</b></td>
                <?php } ?>

                <?php if ($dependentesM > 0) { ?>
                    <td class="centralizada" > Masculino</td>
                <?php } ?>
                <?php if ($dependentesF > 0) { ?>
                    <td class="centralizada" > Feminino</td>
                <?php } ?>
                <?php if (($dependentesM + $dependentesF) > 0) { ?>
                    <td class="centralizada" > <b>Total</b></td>
                <?php } ?>


                <?php if ($agregadosM > 0) { ?>
                    <td class="centralizada" > Masculino</td>
                <?php } ?>
                <?php if ($agregadosF > 0) { ?>
                    <td class="centralizada" > Feminino</td>
                <?php } ?>
                <?php if (($agregadosM + $agregadosF) > 0) { ?>
                    <td class="centralizada" > <b>Total</b></td>
                <?php } ?>


                <?php if ($afastadosM > 0) { ?>
                    <td class="centralizada" > Masculino</td>
                <?php } ?>
                <?php if ($afastadosF > 0) { ?>
                    <td class="centralizada" > Feminino</td>
                <?php } ?>
                <?php if (($afastadosM + $afastadosF) > 0) { ?>
                    <td class="centralizada" > <b>Total</b></td>
                <?php } ?>
            </tr>
            <?php
            $faixas = array('00 a 18', '19 a 23', '24 a 28', '29 a 33', '34 a 38', '39 a 43', '44 a 48', '49 a 53', '54 a 58', '+ de 59');
            $tiposPessoas = array('Titulares', 'Dependentes', 'Agregados', 'Afastados');
            $sexos = array('m' => 'Masculino', 'f' => 'Feminino');
            $i = 1;
            foreach ($faixas as $faixa) {
                ?>
                <tr>
                    <td class="centralizada"><?= $faixa ?></td>

                    <?php
                    foreach ($tiposPessoas as $tipoPessoa) {
                        foreach ($sexos as $chave => $valor) {
                            if ($geralPorFaixa[$tipoPessoa][$chave] != null) {
                                ?>
                                <td class="centralizada"><?= (isset($geralPorFaixa[$tipoPessoa][$chave]['faixa' . $i]) ? $geralPorFaixa[$tipoPessoa][$chave]['faixa' . $i] : 0) ?></td>
                                <?php
                            }
                        }

                        if ($geralPorFaixa[$tipoPessoa]['m'] != null || $geralPorFaixa[$tipoPessoa]['f'] != null) {
                            ?>
                            <td class="centralizada"><?= (isset($geralPorFaixa[$tipoPessoa]['m']['faixa' . $i]) ? $geralPorFaixa[$tipoPessoa]['m']['faixa' . $i] : 0) + (isset($geralPorFaixa[$tipoPessoa]['f']['faixa' . $i]) ? $geralPorFaixa[$tipoPessoa]['f']['faixa' . $i] : 0) ?></td>
                            <?php
                        }
                    }
                    ?>
                </tr>
                <?php
                $i = $i + 1;
            }
            ?>
<!--            <tr>
            <td class="centralizada">00 a 18</td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa1']) ? $geralPorFaixa['Titulares']['m']['faixa1'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa1']) ? $geralPorFaixa['Titulares']['f']['faixa1'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa1']) ? $geralPorFaixa['Titulares']['m']['faixa1'] : 0) + (isset($geralPorFaixa['Titulares']['f']['faixa1']) ? $geralPorFaixa['Titulares']['f']['faixa1'] : 0) ?></td>
            <?php } ?>


            <?php if ($geralPorFaixa['Dependentes']['m'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa1']) ? $geralPorFaixa['Dependentes']['m']['faixa1'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa1']) ? $geralPorFaixa['Dependentes']['f']['faixa1'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa1']) ? $geralPorFaixa['Dependentes']['m']['faixa1'] : 0) + (isset($geralPorFaixa['Dependentes']['f']['faixa1']) ? $geralPorFaixa['Dependentes']['f']['faixa1'] : 0) ?></td>
            <?php } ?>


            <?php if ($geralPorFaixa['Agregados']['m'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa1']) ? $geralPorFaixa['Agregados']['m']['faixa1'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa1']) ? $geralPorFaixa['Agregados']['f']['faixa1'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa1']) ? $geralPorFaixa['Agregados']['m']['faixa1'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa1']) ? $geralPorFaixa['Agregados']['f']['faixa1'] : 0) ?></td>
            <?php } ?>


            <?php if ($geralPorFaixa['Afastados']['m'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa1']) ? $geralPorFaixa['Afastados']['m']['faixa1'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa1']) ? $geralPorFaixa['Afastados']['f']['faixa1'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa1']) ? $geralPorFaixa['Afastados']['m']['faixa1'] : 0) + (isset($geralPorFaixa['Afastados']['f']['faixa1']) ? $geralPorFaixa['Afastados']['f']['faixa1'] : 0) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td class="centralizada">19 a 23</td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa2']) ? $geralPorFaixa['Titulares']['m']['faixa2'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa2']) ? $geralPorFaixa['Titulares']['f']['faixa2'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa2']) ? $geralPorFaixa['Titulares']['m']['faixa2'] : 0) + (isset($geralPorFaixa['Titulares']['f']['faixa2']) ? $geralPorFaixa['Titulares']['f']['faixa2'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa2']) ? $geralPorFaixa['Dependentes']['m']['faixa2'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa2']) ? $geralPorFaixa['Dependentes']['f']['faixa2'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa2']) ? $geralPorFaixa['Dependentes']['m']['faixa2'] : 0) + (isset($geralPorFaixa['Dependentes']['f']['faixa2']) ? $geralPorFaixa['Dependentes']['f']['faixa2'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa2']) ? $geralPorFaixa['Agregados']['m']['faixa2'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa2']) ? $geralPorFaixa['Agregados']['f']['faixa2'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa2']) ? $geralPorFaixa['Agregados']['m']['faixa2'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa2']) ? $geralPorFaixa['Agregados']['f']['faixa2'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa2']) ? $geralPorFaixa['Afastados']['m']['faixa2'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa2']) ? $geralPorFaixa['Afastados']['f']['faixa2'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa2']) ? $geralPorFaixa['Afastados']['m']['faixa2'] : 0) + (isset($geralPorFaixa['Afastados']['f']['faixa2']) ? $geralPorFaixa['Afastados']['f']['faixa2'] : 0) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td class="centralizada">24 a 28</td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa3']) ? $geralPorFaixa['Titulares']['m']['faixa3'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa3']) ? $geralPorFaixa['Titulares']['f']['faixa3'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa3']) ? $geralPorFaixa['Titulares']['m']['faixa3'] : 0) + (isset($geralPorFaixa['Titulares']['f']['faixa3']) ? $geralPorFaixa['Titulares']['f']['faixa3'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa3']) ? $geralPorFaixa['Dependentes']['m']['faixa3'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa3']) ? $geralPorFaixa['Dependentes']['f']['faixa3'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa3']) ? $geralPorFaixa['Dependentes']['m']['faixa3'] : 0) + (isset($geralPorFaixa['Dependentes']['f']['faixa3']) ? $geralPorFaixa['Dependentes']['f']['faixa3'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa3']) ? $geralPorFaixa['Agregados']['m']['faixa3'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa3']) ? $geralPorFaixa['Agregados']['f']['faixa3'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa3']) ? $geralPorFaixa['Agregados']['m']['faixa3'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa3']) ? $geralPorFaixa['Agregados']['f']['faixa3'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa3']) ? $geralPorFaixa['Afastados']['m']['faixa3'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa3']) ? $geralPorFaixa['Afastados']['f']['faixa3'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa3']) ? $geralPorFaixa['Afastados']['m']['faixa3'] : 0) + (isset($geralPorFaixa['Afastados']['f']['faixa3']) ? $geralPorFaixa['Afastados']['f']['faixa3'] : 0) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td class="centralizada">29 a 33</td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa4']) ? $geralPorFaixa['Titulares']['m']['faixa4'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa4']) ? $geralPorFaixa['Titulares']['f']['faixa4'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa4']) ? $geralPorFaixa['Titulares']['m']['faixa4'] : 0) + (isset($geralPorFaixa['Titulares']['f']['faixa4']) ? $geralPorFaixa['Titulares']['f']['faixa4'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa4']) ? $geralPorFaixa['Dependentes']['m']['faixa4'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa4']) ? $geralPorFaixa['Dependentes']['f']['faixa4'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa4']) ? $geralPorFaixa['Dependentes']['m']['faixa4'] : 0) + (isset($geralPorFaixa['Dependentes']['f']['faixa4']) ? $geralPorFaixa['Dependentes']['f']['faixa4'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa4']) ? $geralPorFaixa['Agregados']['m']['faixa4'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa4']) ? $geralPorFaixa['Agregados']['f']['faixa4'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa4']) ? $geralPorFaixa['Agregados']['m']['faixa4'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa4']) ? $geralPorFaixa['Agregados']['f']['faixa4'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa4']) ? $geralPorFaixa['Afastados']['m']['faixa4'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa4']) ? $geralPorFaixa['Afastados']['f']['faixa4'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa4']) ? $geralPorFaixa['Afastados']['m']['faixa4'] : 0) + (isset($geralPorFaixa['Afastados']['f']['faixa4']) ? $geralPorFaixa['Afastados']['f']['faixa4'] : 0) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td class="centralizada">34 a 38</td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa5']) ? $geralPorFaixa['Titulares']['m']['faixa5'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa5']) ? $geralPorFaixa['Titulares']['f']['faixa5'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa5']) ? $geralPorFaixa['Titulares']['m']['faixa5'] : 0) + (isset($geralPorFaixa['Titulares']['f']['faixa5']) ? $geralPorFaixa['Titulares']['f']['faixa5'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa5']) ? $geralPorFaixa['Dependentes']['m']['faixa5'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa5']) ? $geralPorFaixa['Dependentes']['f']['faixa5'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa5']) ? $geralPorFaixa['Dependentes']['m']['faixa5'] : 0) + (isset($geralPorFaixa['Dependentes']['f']['faixa5']) ? $geralPorFaixa['Dependentes']['f']['faixa5'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa5']) ? $geralPorFaixa['Agregados']['m']['faixa5'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa5']) ? $geralPorFaixa['Agregados']['f']['faixa5'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa5']) ? $geralPorFaixa['Agregados']['m']['faixa5'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa5']) ? $geralPorFaixa['Agregados']['f']['faixa5'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa5']) ? $geralPorFaixa['Afastados']['m']['faixa5'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa5']) ? $geralPorFaixa['Afastados']['f']['faixa5'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa5']) ? $geralPorFaixa['Afastados']['m']['faixa5'] : 0) + (isset($geralPorFaixa['Afastados']['f']['faixa5']) ? $geralPorFaixa['Afastados']['f']['faixa5'] : 0) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td class="centralizada">39 a 43</td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa6']) ? $geralPorFaixa['Titulares']['m']['faixa6'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa6']) ? $geralPorFaixa['Titulares']['f']['faixa6'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa6']) ? $geralPorFaixa['Titulares']['m']['faixa6'] : 0) + (isset($geralPorFaixa['Titulares']['f']['faixa6']) ? $geralPorFaixa['Titulares']['f']['faixa6'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa6']) ? $geralPorFaixa['Dependentes']['m']['faixa6'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa6']) ? $geralPorFaixa['Dependentes']['f']['faixa6'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa6']) ? $geralPorFaixa['Dependentes']['m']['faixa6'] : 0 ) + (isset($geralPorFaixa['Dependentes']['f']['faixa6']) ? $geralPorFaixa['Dependentes']['f']['faixa6'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa6']) ? $geralPorFaixa['Agregados']['m']['faixa6'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa6']) ? $geralPorFaixa['Agregados']['f']['faixa6'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa6']) ? $geralPorFaixa['Agregados']['m']['faixa6'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa6']) ? $geralPorFaixa['Agregados']['f']['faixa6'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa6']) ? $geralPorFaixa['Afastados']['m']['faixa6'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa6']) ? $geralPorFaixa['Afastados']['f']['faixa6'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa6']) ? $geralPorFaixa['Afastados']['m']['faixa6'] : 0 ) + (isset($geralPorFaixa['Afastados']['f']['faixa6']) ? $geralPorFaixa['Afastados']['f']['faixa6'] : 0) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td class="centralizada">44 a 48</td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa7']) ? $geralPorFaixa['Titulares']['m']['faixa7'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa7']) ? $geralPorFaixa['Titulares']['f']['faixa7'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa7']) ? $geralPorFaixa['Titulares']['m']['faixa7'] : 0) + (isset($geralPorFaixa['Titulares']['f']['faixa7']) ? $geralPorFaixa['Titulares']['f']['faixa7'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa7']) ? $geralPorFaixa['Dependentes']['m']['faixa7'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa7']) ? $geralPorFaixa['Dependentes']['f']['faixa7'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa7']) ? $geralPorFaixa['Dependentes']['m']['faixa7'] : 0) + (isset($geralPorFaixa['Dependentes']['f']['faixa7']) ? $geralPorFaixa['Dependentes']['f']['faixa7'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa7']) ? $geralPorFaixa['Agregados']['m']['faixa7'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa7']) ? $geralPorFaixa['Agregados']['f']['faixa7'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa7']) ? $geralPorFaixa['Agregados']['m']['faixa7'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa7']) ? $geralPorFaixa['Agregados']['f']['faixa7'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa7']) ? $geralPorFaixa['Afastados']['m']['faixa7'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa7']) ? $geralPorFaixa['Afastados']['f']['faixa7'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa7']) ? $geralPorFaixa['Afastados']['m']['faixa7'] : 0) + (isset($geralPorFaixa['Afastados']['f']['faixa7']) ? $geralPorFaixa['Afastados']['f']['faixa7'] : 0) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td class="centralizada">49 a 53</td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa8']) ? $geralPorFaixa['Titulares']['m']['faixa8'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa8']) ? $geralPorFaixa['Titulares']['f']['faixa8'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa8']) ? $geralPorFaixa['Titulares']['m']['faixa8'] : 0) + (isset($geralPorFaixa['Titulares']['f']['faixa8']) ? $geralPorFaixa['Titulares']['f']['faixa8'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa8']) ? $geralPorFaixa['Dependentes']['m']['faixa8'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa8']) ? $geralPorFaixa['Dependentes']['f']['faixa8'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa8']) ? $geralPorFaixa['Dependentes']['m']['faixa8'] : 0) + (isset($geralPorFaixa['Dependentes']['f']['faixa8']) ? $geralPorFaixa['Dependentes']['f']['faixa8'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa8']) ? $geralPorFaixa['Agregados']['m']['faixa8'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa8']) ? $geralPorFaixa['Agregados']['f']['faixa8'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa8']) ? $geralPorFaixa['Agregados']['m']['faixa8'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa8']) ? $geralPorFaixa['Agregados']['f']['faixa8'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa8']) ? $geralPorFaixa['Afastados']['m']['faixa8'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa8']) ? $geralPorFaixa['Afastados']['f']['faixa8'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa8']) ? $geralPorFaixa['Afastados']['m']['faixa8'] : 0) + (isset($geralPorFaixa['Afastados']['f']['faixa8']) ? $geralPorFaixa['Afastados']['f']['faixa8'] : 0) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td class="centralizada">54 a 58</td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa9']) ? $geralPorFaixa['Titulares']['m']['faixa9'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa9']) ? $geralPorFaixa['Titulares']['f']['faixa9'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa9']) ? $geralPorFaixa['Titulares']['m']['faixa9'] : 0) + (isset($geralPorFaixa['Titulares']['f']['faixa9']) ? $geralPorFaixa['Titulares']['f']['faixa9'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa9']) ? $geralPorFaixa['Dependentes']['m']['faixa9'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa9']) ? $geralPorFaixa['Dependentes']['f']['faixa9'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa9']) ? $geralPorFaixa['Dependentes']['m']['faixa9'] : 0) + (isset($geralPorFaixa['Dependentes']['f']['faixa9']) ? $geralPorFaixa['Dependentes']['f']['faixa9'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa9']) ? $geralPorFaixa['Agregados']['m']['faixa9'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa9']) ? $geralPorFaixa['Agregados']['f']['faixa9'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa9']) ? $geralPorFaixa['Agregados']['m']['faixa9'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa9']) ? $geralPorFaixa['Agregados']['f']['faixa9'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa9']) ? $geralPorFaixa['Afastados']['m']['faixa9'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa9']) ? $geralPorFaixa['Afastados']['f']['faixa9'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa9']) ? $geralPorFaixa['Afastados']['m']['faixa9'] : 0) + (isset($geralPorFaixa['Afastados']['f']['faixa9']) ? $geralPorFaixa['Afastados']['f']['faixa9'] : 0) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td class="centralizada">+ de 59 </td>
            <?php if ($geralPorFaixa['Titulares']['m'] != null || $geralPorFaixa['Titulares']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa10']) ? $geralPorFaixa['Titulares']['m']['faixa10'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['f']['faixa10']) ? $geralPorFaixa['Titulares']['f']['faixa10'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Titulares']['m']['faixa10']) ? $geralPorFaixa['Titulares']['m']['faixa10'] : 0 ) + (isset($geralPorFaixa['Titulares']['f']['faixa10']) ? $geralPorFaixa['Titulares']['f']['faixa10'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Dependentes']['m'] != null || $geralPorFaixa['Dependentes']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa10']) ? $geralPorFaixa['Dependentes']['m']['faixa10'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['f']['faixa10']) ? $geralPorFaixa['Dependentes']['f']['faixa10'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Dependentes']['m']['faixa10']) ? $geralPorFaixa['Dependentes']['m']['faixa10'] : 0 ) + (isset($geralPorFaixa['Dependentes']['f']['faixa10']) ? $geralPorFaixa['Dependentes']['f']['faixa10'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Agregados']['m'] != null || $geralPorFaixa['Agregados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa10']) ? $geralPorFaixa['Agregados']['m']['faixa10'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['f']['faixa10']) ? $geralPorFaixa['Agregados']['f']['faixa10'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Agregados']['m']['faixa10']) ? $geralPorFaixa['Agregados']['m']['faixa10'] : 0) + (isset($geralPorFaixa['Agregados']['f']['faixa10']) ? $geralPorFaixa['Agregados']['f']['faixa10'] : 0) ?></td>
            <?php } ?>
            <?php if ($geralPorFaixa['Afastados']['m'] != null || $geralPorFaixa['Afastados']['f'] != null) { ?>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa10']) ? $geralPorFaixa['Afastados']['m']['faixa10'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['f']['faixa10']) ? $geralPorFaixa['Afastados']['f']['faixa10'] : 0) ?></td>
                    <td class="centralizada"><?= (isset($geralPorFaixa['Afastados']['m']['faixa10']) ? $geralPorFaixa['Afastados']['m']['faixa10'] : 0) + (isset($geralPorFaixa['Afastados']['f']['faixa10']) ? $geralPorFaixa['Afastados']['f']['faixa10'] : 0) ?></td>
            <?php } ?>
        </tr>-->
            <tr class="centralizada" style="font-weight: bold !important">
                <td class="centralizada">Totais</td>
                <?php if ($titularesM > 0) { ?>
                    <td class="centralizada"><?= $titularesM ?></td>
                <?php } ?>
                <?php if ($titularesF > 0) { ?>
                    <td class="centralizada"><?= ($titularesF ? $titularesF : 0) ?></td>
                <?php } ?>
                <?php if ($titularesF + $titularesM > 0) { ?>
                    <td class="centralizada"><?= $titularesM + $titularesF ?></td>
                <?php } ?>
                <?php if ($dependentesM > 0) { ?>
                    <td class="centralizada"><?= ($dependentesM ? $dependentesM : 0) ?></td>
                <?php } ?>
                <?php if ($dependentesF > 0) { ?>
                    <td class="centralizada"><?= ($dependentesF ? $dependentesF : 0) ?></td>
                <?php } ?>
                <?php if ($dependentesF + $dependentesM > 0) { ?>
                    <td class="centralizada"><?= $dependentesM + $dependentesF ?></td>
                <?php } ?>
                <?php if ($agregadosM > 0) { ?>
                    <td class="centralizada"><?= ($agregadosM ? $agregadosM : 0) ?></td>
                <?php } ?>
                <?php if ($agregadosF > 0) { ?>
                    <td class="centralizada"><?= ($agregadosF ? $agregadosF : 0) ?></td>
                <?php } ?>
                <?php if ($agregadosF + $agregadosM > 0) { ?>
                    <td class="centralizada"><?= $agregadosM + $agregadosF ?></td>
                <?php } ?>
                <?php if ($afastadosM > 0) { ?>
                    <td class="centralizada"><?= ($afastadosM ? $afastadosM : 0) ?></td>
                <?php } ?>
                <?php if ($afastadosF > 0) { ?>
                    <td class="centralizada"> <?= ($afastadosF ? $afastadosF : 0) ?></td>
                <?php } ?>
                <?php if ($afastadosF + $afastadosM > 0) { ?>
                    <td class="centralizada"> <?= $afastadosM + $afastadosF ?></td>
                <?php } ?>
            </tr>
        </tbody>
    </table>
    <h4 class="centralizada"> <strong>Total Geral neste estudo: <?= $titularesF + $titularesM + $dependentesF + $dependentesM + $agregadosF + $agregadosM + $afastadosF + $afastadosM ?></strong></h4>

</div>
