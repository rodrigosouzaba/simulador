<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfAcomodaco->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfAcomodaco->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Acomodacoes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="pfAcomodacoes form large-9 medium-8 columns content">
    <?= $this->Form->create($pfAcomodaco) ?>
    <fieldset>
        <legend><?= __('Edit Pf Acomodaco') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
