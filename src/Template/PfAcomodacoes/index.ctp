<div class="col-xs-12">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= 'id' ?></th>
                <th><?= 'nome' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfAcomodacoes as $pfAcomodaco): ?>
                <tr>
                    <td><?= $this->Number->format($pfAcomodaco->id) ?></td>
                    <td><?= h($pfAcomodaco->nome) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $pfAcomodaco->id]) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $pfAcomodaco->id]) ?>
                        <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $pfAcomodaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfAcomodaco->id)]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Próxima') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
