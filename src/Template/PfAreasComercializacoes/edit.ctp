<style>
    .estados {
        border: 1px solid #ccc;
        padding: 0px;
        max-height: 300px;
        overflow: auto;
    }

    .select-estado {
        display: flex;
        align-items: center;
        padding: 0 15px;
    }

    .select-estado:hover {
        background-color: #DDD;
    }

    .select-estado span {
        padding: 5px 0;
    }

    .active {
        background: #08c;
        color: #FFF;
    }
</style>
<div class="container fonteReduzida">
    <?= $this->Form->create($pfAreasComercializaco) ?>
    <fieldset>
        <legend><?= __('Nova Area Comercialização') ?></legend>
        <div class="spacer-md">&nbsp</div>
        <div class="col-md-6">
            <?= $this->Form->input('nome') ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('pf_operadora_id', ['options' => $pfOperadoras]) ?>
        </div>
        <div class="col-md-12">
            <?= $this->Form->input('descricao', ['type' => 'textarea', 'label' => 'Descrição']) ?>
        </div>
    </fieldset>

    <div class="col-md-6" style="margin-top: 1rem;">
        <label>Estados</label>
        <div class="estados">
            <?php foreach ($estados as $estado) :
                if (!empty($estado->metropoles)) : ?>
                    <div class="select-estado select-estado-metropoles <?= in_array($estado->id, $locaisSalvos['metropoles']) ? "active" : "" ?>" id="select-estado-metropoles-<?= $estado->id ?>" estado-id=<?= $estado->id ?>>
                        <span><?= $estado->nome ?></span>
                    </div>
            <?php endif;
            endforeach; ?>
        </div>
    </div>
    <div class="col-md-6 tab-content" style="margin-top: 1rem;">
        <label>Metropoles</label>
        <?php foreach ($estados as $estado) : ?>
            <div class="metropoles tab-pane tab-pane-metropoles" id="tab-metropoles-<?= $estado->id ?>" estado="<?= $estado->id ?>">
                <div class="flex" style="justify-content: space-between;">
                    <a href='#' id='select-all-metropoles-<?= $estado->id ?>'>Seleiconar Tudo</a>
                    <a href='#' id='deselect-all-metropoles-<?= $estado->id ?>'>Limpar</a>
                </div>
                <select multiple='multiple' id="multiple-estado-metropoles-<?= $estado->id ?>" class="multiselect" estado-id="<?= $estado->id ?>" name='<?= "metropoles[$estado->id][]" ?>'>
                    <?php foreach ($estado->metropoles as $metropole) : ?>
                        <option value="<?= $metropole->id; ?>" <?= array_key_exists($metropole->id, $locaisSalvos['metropoles']) ? "selected='selected'" : "" ?>><?= $metropole->nome; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <script>
                $(document).ready(function() {
                    var estado = $("#multiple-estado-metropoles-<?= $estado->id ?>").attr("estado-id");

                    $("#multiple-estado-metropoles-<?= $estado->id ?>").multiSelect({
                        selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                        selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                        afterInit: function(ms) {
                            var that = this,
                                $selectableSearch = that.$selectableUl.prev(),
                                $selectionSearch = that.$selectionUl.prev(),
                                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                                .on('keydown', function(e) {
                                    if (e.which === 40) {
                                        that.$selectableUl.focus();
                                        return false;
                                    }
                                });

                            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                                .on('keydown', function(e) {
                                    if (e.which == 40) {
                                        that.$selectionUl.focus();
                                        return false;
                                    }
                                });
                        },
                        afterSelect: function(values) {
                            this.qs1.cache();
                            this.qs2.cache();
                            var selecionados = $('#multiple-estado-metropoles-<?= $estado->id ?> option:selected').length;
                            $("#select-estado-metropoles-" + estado).addClass("active");
                        },
                        afterDeselect: function(values) {
                            this.qs1.cache();
                            this.qs2.cache();
                            var selecionados = $('#multiple-estado-metropoles-<?= $estado->id ?> option:selected').length;
                            if (selecionados == 0) {
                                $("#select-estado-metropoles-" + estado).removeClass("active");
                            }
                        }
                    })
                })
                $("#select-all-metropoles-<?= $estado->id ?>").click(function() {
                    $("#multiple-estado-metropoles-<?= $estado->id ?>").multiSelect('select_all')
                });
                $("#deselect-all-metropoles-<?= $estado->id ?>").click(function() {
                    $("#multiple-estado-metropoles-<?= $estado->id ?>").multiSelect('deselect_all')
                });
            </script>
        <?php endforeach; ?>
    </div>
    <div class="clearfix">&nbsp;</div>

    <div class="col-md-6" style="margin-top: 1rem;">
        <label>Estados</label>
        <div class="estados">
            <?php foreach ($estados as $estado) : ?>
                <div class="select-estado select-estado-municipios <?= in_array($estado->id, $locaisSalvos['municipios']) ? "active" : "" ?>" id="select-estado-municipios-<?= $estado->id ?>" estado-id=<?= $estado->id ?>>
                    <span><?= $estado->nome ?></span>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="col-md-6 tab-content" style="margin-top: 1rem;">
        <label>Municipios</label>
        <?php foreach ($estados as $estado) : ?>
            <div class="municipios tab-pane tab-pane-municipios" id="tab-municipios-<?= $estado->id ?>" estado="<?= $estado->id ?>">
                <div class="flex" style="justify-content: space-between;">
                    <a href='#' id='select-all-municipios-<?= $estado->id ?>'>Seleiconar Tudo</a>
                    <a href='#' id='deselect-all-municipios-<?= $estado->id ?>'>Limpar</a>
                </div>
                <select multiple='multiple' id="multiple-estado-municipios-<?= $estado->id ?>" class="multiselect" estado-id="<?= $estado->id ?>" name='<?= "municipio[$estado->id][]" ?>'>
                    <?php foreach ($estado->municipios as $municipio) : ?>
                        <option value="<?= $municipio->id; ?>" <?= array_key_exists($municipio->id, $locaisSalvos['municipios']) ? "selected='selected'" : "" ?>><?= $municipio->nome; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <script>
                $(document).ready(function() {
                    var estado = $("#multiple-estado-municipios-<?= $estado->id ?>").attr("estado-id");

                    $("#multiple-estado-municipios-<?= $estado->id ?>").multiSelect({
                        selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                        selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
                        afterInit: function(ms) {
                            var that = this,
                                $selectableSearch = that.$selectableUl.prev(),
                                $selectionSearch = that.$selectionUl.prev(),
                                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                                .on('keydown', function(e) {
                                    if (e.which === 40) {
                                        that.$selectableUl.focus();
                                        return false;
                                    }
                                });

                            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                                .on('keydown', function(e) {
                                    if (e.which == 40) {
                                        that.$selectionUl.focus();
                                        return false;
                                    }
                                });
                        },
                        afterSelect: function(values) {
                            this.qs1.cache();
                            this.qs2.cache();
                            var selecionados = $('#multiple-estado-municipios-<?= $estado->id ?> option:selected').length;
                            $("#select-estado-municipios-" + estado).addClass("active");
                        },
                        afterDeselect: function(values) {
                            this.qs1.cache();
                            this.qs2.cache();
                            var selecionados = $('#multiple-estado-municipios-<?= $estado->id ?> option:selected').length;
                            if (selecionados == 0) {
                                $("#select-estado-municipios-" + estado).removeClass("active");
                            }
                        }
                    })
                })
                $("#select-all-municipios-<?= $estado->id ?>").click(function() {
                    $("#multiple-estado-municipios-<?= $estado->id ?>").multiSelect('select_all')
                });
                $("#deselect-all-municipios-<?= $estado->id ?>").click(function() {
                    $("#multiple-estado-municipios-<?= $estado->id ?>").multiSelect('deselect_all')
                });
            </script>
        <?php endforeach; ?>
    </div>
</div>

<div class="spacer-md">&nbsp</div>
<div class="col-md-12 text-center">
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
</div>
<?= $this->Form->end() ?>

<script>
    $(".select-estado-metropoles").click(function() {
        var estado = $(this).attr('estado-id');
        $('.tab-pane-metropoles').hide();
        $('#tab-metropoles-' + estado).show();
    });

    $(".select-estado-municipios").click(function() {
        var estado = $(this).attr('estado-id');
        $('.tab-pane-municipios').hide();
        $('#tab-municipios-' + estado).show();
    });
</script>