<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PfpfAreasComercializaco[]|\Cake\Collection\CollectionInterface $pfpfAreasComercializacoes
 */
?>
<div class="col-xs-12">
    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' Nova Área Comercialização', '/pfAreasComercializacoes/add', ['class' => 'btn btn-sm btn-primary', 'escape' => false, 'sid' => 'pfAreasComercializacoes.add']) ?>
</div>
<div class="col-xs-12">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>#</th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('operadora_id') ?></th>
                <th><?= $this->Paginator->sort('descricao') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfAreasComercializacoes as $pfAreasComercializaco) : ?>
                <tr>
                    <td><?= (int) $pfAreasComercializaco->pf_operadora->nova ? '' : '<i class="fas fa-exclamation-circle text-danger"></i>' ?></td>
                    <td><?= h($pfAreasComercializaco->nome) ?></td>
                    <td><?= $pfAreasComercializaco->has('pf_operadora') ? $this->Html->link($pfAreasComercializaco->pf_operadora->nome . ' - ' . $pfAreasComercializaco->pf_operadora->detalhe, ['controller' => 'Operadoras', 'action' => 'view', $pfAreasComercializaco->pf_operadora->id]) : '' ?></td>
                    <td><?= $pfAreasComercializaco->descricao ?></td>
                    <td class="actions">
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm dropdown-toggle btn-success " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </button>
                            <ul class="dropdown-menu" style="right: 0; left: auto;">
                                <li><?= $this->Html->link(__('Edit'), '/pfAreasComercializacoes/edit/' . $pfAreasComercializaco->id, ['sid' => 'pfAreasComercializacoes.edit']) ?></li>
                                <li><?= $this->Form->postLink(__('Duplicar'), '/pfAreasComercializacoes/duplicar/' . $pfAreasComercializaco->id, ['sid' => 'pfAreasComercializacoes.duplicar', 'confirm' => __('Tem certeza que quer duplicar a area de comercialização #{0}?', $pfAreasComercializaco->nome)]) ?></li>
                                <li><?= $this->Form->postLink(__('Delete'), '/pfAreasComercializacoes/delete/' . $pfAreasComercializaco->id, ['sid' => 'pfAreasComercializacoes.delete', 'confirm' => __('Are you sure you want to delete # {0}?', $pfAreasComercializaco->id)]) ?></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>