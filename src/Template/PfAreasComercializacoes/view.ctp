<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PfAreasComercializaco $pfAreasComercializaco
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Areas Comercializaco'), ['action' => 'edit', $pfAreasComercializaco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Areas Comercializaco'), ['action' => 'delete', $pfAreasComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfAreasComercializaco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Areas Comercializacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Areas Comercializaco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['controller' => 'PfAtendimentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Atendimento'), ['controller' => 'PfAtendimentos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfAreasComercializacoes view large-9 medium-8 columns content">
    <h3><?= h($pfAreasComercializaco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Descricao') ?></th>
            <td><?= h($pfAreasComercializaco->descricao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($pfAreasComercializaco->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pf Operadora') ?></th>
            <td><?= $pfAreasComercializaco->has('pf_operadora') ? $this->Html->link($pfAreasComercializaco->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfAreasComercializaco->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pf Atendimento') ?></th>
            <td><?= $pfAreasComercializaco->has('pf_atendimento') ? $this->Html->link($pfAreasComercializaco->pf_atendimento->id, ['controller' => 'PfAtendimentos', 'action' => 'view', $pfAreasComercializaco->pf_atendimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfAreasComercializaco->id) ?></td>
        </tr>
    </table>
</div>
