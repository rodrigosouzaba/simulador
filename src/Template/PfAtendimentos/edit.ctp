<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfAtendimento->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfAtendimento->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfAtendimentos form large-9 medium-8 columns content">
    <?= $this->Form->create($pfAtendimento) ?>
    <fieldset>
        <legend><?= __('Edit Pf Atendimento') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
