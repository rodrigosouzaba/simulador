<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Atendimento'), ['action' => 'edit', $pfAtendimento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Atendimento'), ['action' => 'delete', $pfAtendimento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfAtendimento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Atendimento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfAtendimentos view large-9 medium-8 columns content">
    <h3><?= h($pfAtendimento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($pfAtendimento->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfAtendimento->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($pfAtendimento->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Tabelas') ?></h4>
        <?php if (!empty($pfAtendimento->pf_tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Vigencia') ?></th>
                <th><?= __('Faixa1') ?></th>
                <th><?= __('Faixa2') ?></th>
                <th><?= __('Faixa3') ?></th>
                <th><?= __('Faixa4') ?></th>
                <th><?= __('Faixa5') ?></th>
                <th><?= __('Faixa6') ?></th>
                <th><?= __('Faixa7') ?></th>
                <th><?= __('Faixa8') ?></th>
                <th><?= __('Faixa9') ?></th>
                <th><?= __('Faixa10') ?></th>
                <th><?= __('Faixa11') ?></th>
                <th><?= __('Faixa12') ?></th>
                <th><?= __('Pf Produto Id') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th><?= __('Pf Atendimento Id') ?></th>
                <th><?= __('Pf Acomodacao Id') ?></th>
                <th><?= __('Validade') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Pf Comercializacao Id') ?></th>
                <th><?= __('Cod Ans') ?></th>
                <th><?= __('Minimo Vidas') ?></th>
                <th><?= __('Maximo Vidas') ?></th>
                <th><?= __('Prioridade') ?></th>
                <th><?= __('Coparticipacao') ?></th>
                <th><?= __('Detalhe Coparticipacao') ?></th>
                <th><?= __('Modalidade') ?></th>
                <th><?= __('Reembolso') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfAtendimento->pf_tabelas as $pfTabelas): ?>
            <tr>
                <td><?= h($pfTabelas->id) ?></td>
                <td><?= h($pfTabelas->nome) ?></td>
                <td><?= h($pfTabelas->descricao) ?></td>
                <td><?= h($pfTabelas->vigencia) ?></td>
                <td><?= h($pfTabelas->faixa1) ?></td>
                <td><?= h($pfTabelas->faixa2) ?></td>
                <td><?= h($pfTabelas->faixa3) ?></td>
                <td><?= h($pfTabelas->faixa4) ?></td>
                <td><?= h($pfTabelas->faixa5) ?></td>
                <td><?= h($pfTabelas->faixa6) ?></td>
                <td><?= h($pfTabelas->faixa7) ?></td>
                <td><?= h($pfTabelas->faixa8) ?></td>
                <td><?= h($pfTabelas->faixa9) ?></td>
                <td><?= h($pfTabelas->faixa10) ?></td>
                <td><?= h($pfTabelas->faixa11) ?></td>
                <td><?= h($pfTabelas->faixa12) ?></td>
                <td><?= h($pfTabelas->pf_produto_id) ?></td>
                <td><?= h($pfTabelas->pf_operadora_id) ?></td>
                <td><?= h($pfTabelas->pf_atendimento_id) ?></td>
                <td><?= h($pfTabelas->pf_acomodacao_id) ?></td>
                <td><?= h($pfTabelas->validade) ?></td>
                <td><?= h($pfTabelas->estado_id) ?></td>
                <td><?= h($pfTabelas->pf_comercializacao_id) ?></td>
                <td><?= h($pfTabelas->cod_ans) ?></td>
                <td><?= h($pfTabelas->minimo_vidas) ?></td>
                <td><?= h($pfTabelas->maximo_vidas) ?></td>
                <td><?= h($pfTabelas->prioridade) ?></td>
                <td><?= h($pfTabelas->coparticipacao) ?></td>
                <td><?= h($pfTabelas->detalhe_coparticipacao) ?></td>
                <td><?= h($pfTabelas->modalidade) ?></td>
                <td><?= h($pfTabelas->reembolso) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfTabelas', 'action' => 'view', $pfTabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfTabelas', 'action' => 'edit', $pfTabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfTabelas', 'action' => 'delete', $pfTabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfTabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
