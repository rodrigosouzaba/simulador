<?php
$estados = $estados->toArray();
foreach ($estados as $id => $estado) :
    if ($sessao['estado_id'] == $id) : ?>
        <script type="text/javascript">
            var sessionEstado = <?= $sessao["estado_id"] ?>;
            $("#estado-id").val(sessionEstado);
        </script>
<?php endif;
endforeach;
?>
<?= $this->Form->create($pfCalculo, ['id' => 'calculo', 'url' => '/pfCalculos/cotar', 'class' => 'form form-validate']) ?>
<div class="clearfix">&nbsp;</div>
<div id="pagina">

    <div class="col-md-12 fonteReduzida">
        <div class="tituloField centralizada">
            Dados do Cliente
        </div>

        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('estado_id', ['options' => $estados, 'label' => false, 'empty' => 'ESTADO DE COMERCIALIZAÇÃO', "required" => "required"]); ?>
        </div>
        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('nome', ['label' => false, 'placeholder' => 'Nome (opcional)']); ?>
        </div>
        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('email', ['label' => false, "type" => "text", 'placeholder' => 'E-mail (opcional)']); ?>
        </div>
        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('telefone', ['label' => false, 'placeholder' => 'Telefone (opcional)']); ?>
        </div>

        <div class="clearfix">&nbsp;</div>
        <div class="col-md-12">
            <?= $this->element('calculadora_idade'); ?>
        </div>

        <div class="col-md-3">
            <?php
            $especiais = ['138' => "TODAS AS PROFISSÕES", '86' => 'SEM PROFISSÃO'];
            $pfProfissoes = $especiais + $pfProfissoes;
            ?>
            <?= $this->Form->input('pf_profissao_id', ['label' => 'Profissão', 'options' => $pfProfissoes, 'empty' => 'SELECIONE', 'required' => 'required']); ?>
        </div>
        <div class="hidden-md hidden-lg">
            <div class="clearfix">&nbsp;</div>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('idade_titular', ['label' => 'Titular', 'placeholder' => 'Idade', 'required' => 'required']); ?>
        </div>
        <div class="col-md-1">
            <?= $this->Form->input('Dependentes.dependente1', ['label' => 'Dependente 1', 'placeholder' => 'Idade']); ?>
        </div>
        <div class="col-md-1">
            <?= $this->Form->input('Dependentes.dependente2', ['label' => 'Dependente 2', 'placeholder' => 'Idade']); ?>
        </div>
        <div class="col-md-1">
            <?= $this->Form->input('Dependentes.dependente3', ['label' => 'Dependente 3', 'placeholder' => 'Idade']); ?>
        </div>
        <div class="col-md-1">
            <?= $this->Form->input('Dependentes.dependente4', ['label' => 'Dependente 4', 'placeholder' => 'Idade']); ?>
        </div>
        <div class="col-md-1">
            <?= $this->Form->input('Dependentes.dependente5', ['label' => 'Dependente 5', 'placeholder' => 'Idade']); ?>
        </div>
        <div class="col-md-1">
            <?= $this->Form->input('Dependentes.dependente6', ['label' => 'Dependente 6', 'placeholder' => 'Idade']); ?>
        </div>


    </div>

    <div class="col-md-12">
        <div class="clearfix">&nbsp;</div>
        <div class="col-md-12 centralizada">
            <?=
            $this->Form->button('Simular', [
                'class' => 'btn btn-md btn-primary',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Gerar Cálculo', 'id' => 'enviar'
            ])
            ?>
        </div>
    </div>
</div>

<?= $this->Form->end() ?>