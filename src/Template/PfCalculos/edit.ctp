<?php
$estados = $estados->toArray();
foreach ($estados as $id => $estado) :
    if ($sessao['estado_id'] == $id) : ?>
        <script type="text/javascript">
            var sessionEstado = <?= $sessao["estado_id"] ?>;
            $("#estado-id").val(sessionEstado);
        </script>
    <?php endif;
endforeach;
// debug($pfCalculo);
if ($pfCalculo->edicoes < 3) :
    ?>
    <?= $this->Form->create($pfCalculo, ['id' => 'calculo', 'class' => 'form form-validate', 'role' => 'form','url' => '/pfCalculos/encontrarTabelas' ]); ?>
    <?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Editar Cálculo']); ?>
    <div class="clearfix">&nbsp;</div>
    <div id="pagina" >
        
    
        <div class="col-md-12 ">
            <div class="tituloField centralizada">
                Dados do Cliente
            </div>
            <div class="col-md-3 fonteReduzida">
                <?= $this->Form->input('estado_id', ['options' => $estados, 'label' => false, 'empty' => 'ESTADO DE COMERCIALIZAÇÃO', "required" => "required"]); ?>
            </div>
            <div class="col-md-3 fonteReduzida">
                <?= $this->Form->input('nome', ['label' => false, 'placeholder' => 'Nome (opcional)']); ?>
            </div>
            <div class="col-md-3 fonteReduzida">
                <?= $this->Form->input('email', ['label' => false, "type" => "text", 'placeholder' => 'E-mail (opcional)']); ?>
            </div>
            <div class="col-md-3 fonteReduzida">
                <?= $this->Form->input('telefone', ['label' => false, 'placeholder' => 'Telefone (opcional)']); ?>
            </div>
        </div>
        
        <div class="clearfix">&nbsp;</div>

        <?= $this->element('calculadora_idade'); ?>

        <div class="col-md-12 ">

            <div class="col-md-12 fonteReduzida">
                <div class="col-xs-6" style="padding-left: 0 !important;">
                    <?php
                    $pfProfissoes = $pfProfissoes->toArray();
                    $especiais = ['138' => "TODAS AS PROFISSÕES", '86' => 'SEM PROFISSÃO'];
                    $pfProfissoes = $especiais + $pfProfissoes;
                    ?>
                    <?= $this->Form->input('pf_profissao_id', ['label' => 'Profissão', 'options' => $pfProfissoes, 'empty' => 'SELECIONE', 'required' => 'required']); ?>

                </div>
                <div class="col-xs-6" style="padding-right: 0 !important;">
                    <?= $this->Form->input('idade_titular', ['label' => 'Titular', 'placeholder' => 'Idade', 'required' => 'required']); ?>
                </div>
            </div>

            <div class="col-md-4 fonteReduzida">
                <div class="col-md-6" style="padding-left: 0 !important;">
                    Dependente 1
                    <?= $this->Form->input('Dependentes.dependente1', ['label' => '', 'placeholder' => 'Idade']); ?>
                </div>
                <div class="col-md-6" style="padding-right: 0 !important;">
                    Dependente 2
                    <?= $this->Form->input('Dependentes.dependente2', ['label' => '', 'placeholder' => 'Idade']); ?>
                </div>
            </div>
            <div class="col-md-4 fonteReduzida">
                <div class="col-md-6" style="padding-left: 0 !important;">
                    Dependente 3
                    <?= $this->Form->input('Dependentes.dependente3', ['label' => '', 'placeholder' => 'Idade']); ?>
                </div>
                <div class="col-md-6" style="padding-right: 0 !important;">
                    Dependente 4
                    <?= $this->Form->input('Dependentes.dependente4', ['label' => '', 'placeholder' => 'Idade']); ?>
                </div>
            </div>
            <div class="col-md-4 fonteReduzida">
                <div class="col-md-6" style="padding-left: 0 !important;">
                    Dependente 5
                    <?= $this->Form->input('Dependentes.dependente5', ['label' => '', 'placeholder' => 'Idade']); ?>
                </div>
                <div class="col-md-6" style="padding-right: 0 !important;">
                    Dependente 6
                    <?= $this->Form->input('Dependentes.dependente6', ['label' => '', 'placeholder' => 'Idade']); ?>
                </div>
            </div>


        </div>
    
        <div class="clearfix">&nbsp;</div>
    <?php echo $this->element('forms/buttons') ?>
    <?= $this->Form->end() ?>

<?php else : ?>
    <?= $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Editar Cálculo']); ?>
        <div class="text-danger text-center">
            Quantidade de edições excedida. Faça nova Simulação.
        </div>
    <?= $this->element('forms/buttonsFechar') ?>
<?php endif; ?>