<?php if (isset($tabelas_ordenadas) && count($tabelas_ordenadas) > 0) : ?>
    <div class="well well-sm col-xs-12" style="padding: 5px; min-height: 75px;margin: 5px 0 !important;" id="dadosCalculo">
        <?php
        if (isset($cotacao['nome_titular'])) {
            $titular = "Nome Titular: " . $cotacao['nome_titular'];
            $classe = "col-md-3";
        } else {
            $classe = "col-md-4";
        }
        ?>
        <div class="col-md-4 col-xs-12" style="text-align: left;padding-left: 0 !important;">
            <div class="col-xs-12">
                Cálculo Nº <a href="#" id="btnModalEdit" simulacao_id="<?= $cotacao['id'] ?>"><span><?= $cotacao['id'] . "   " ?> - <u>EDITAR SIMULAÇÃO</u></span></a>
            </div>
            <div class="col-xs-12">
                Total de vidas: <b><?= $total_vidas ?></b>
            </div>
            <div class="col-xs-12">
                Cliente: <b><?= $cotacao['nome'] ?></b>
            </div>
        </div>

        <div class="col-md-8 col-xs-12" style="text-align: left;">
            <div class="col-xs-12" style="padding-left: 0 !important;">
                <?php
                $total = 0;
                for ($i = 1; $i <= 12; $i++) {
                    $total = $total + ($cotacao['faixa' . $i] * $menor_preco['faixa' . $i]);
                }
                ?>
                <b class="tabulacao"><?= $this->Number->currency($total) ?></b>Menor Preço <small>
                    (<?= $menor_preco['pf_areas_comercializaco']['pf_operadora']['nome'] ?>)</small>
            </div>

            <div class="col-xs-12" style="padding-left: 0 !important;">

                <?php
                $total = 0;
                for ($i = 1; $i <= 12; $i++) {
                    $total = $total + ($cotacao['faixa' . $i] * $intermediario['faixa' . $i]);
                }
                ?>
                <b class="tabulacao"><?= $this->Number->currency($total) ?></b>Preço Intermediário <small>
                    (<?= $intermediario['pf_areas_comercializaco']['pf_operadora']['nome'] ?>)</small>
            </div>

            <div class="col-xs-12" style="padding-left: 0 !important;">
                <?php
                $total = 0;
                for ($i = 1; $i <= 12; $i++) {
                    $total = $total + ($cotacao['faixa' . $i] * $maior_preco['faixa' . $i]);
                }
                ?>
                <b class="tabulacao"><?= $this->Number->currency($total) ?></b><span>Maior Preço <small>
                        (<?= $maior_preco['pf_areas_comercializaco']['pf_operadora']['nome'] ?>)</small></span>
            </div>
        </div>
    </div>
    <!-- FIM CABEÇALHO -->


    <!-- INICIO OPERADORAS E TABELAS -->
    <div class="panel-group listaCalculos" role="tablist" aria-multiselectable="true">
        <?php foreach ($tabelas_ordenadas as $operadora_id => $operadora) :
            $numero_tabelas = count($operadora['tabelas']); ?>

            <div class="panel panel-default col-xs-12" style="margin: 5px 0;">
                <div class="panel-heading" role="tab" id="heading<?= $operadora_id ?>" style="min-height: 100px; background-color: #fff !important;">
                    <!-- ABRIR CALCULOS -->
                    <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                        <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $operadora_id ?>" aria-expanded="true" aria-controls="collapse<?= $operadora_id ?>">
                            <i class="fas fa-chevron-down" aria-hidden="true" style="font-size: 110% !important;"></i>
                            <span style="font-size: 14px !important;">Abrir <?= $numero_tabelas ?> cálculo(s)</span>
                        </a>
                    </div>

                    <!-- EXIBIR TODOS NO PDF -->
                    <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                        <label for="checkAll<?= $operadora_id ?>" style="color: #337ab7; display: inline !important">
                            <?= $this->Form->checkbox('published', ['hiddenField' => false, 'id' => 'checkAll' . $operadora_id, 'class' => 'todas', 'name' => 'checkAll' . $operadora_id, 'value' => $operadora_id, 'style' => 'color: #23527c']); ?> <span style="font-size: 14px;">Exibir todos no cálculo</span>
                        </label>
                    </div>

                    <!-- MATERIAL DE VENDAS -->
                    <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                        <a class="controle" href="<?= $operadora['operadora']['url'] ?>" target="_blank" style="font-size: 14px !important;"> Ver Material de Vendas</a>
                    </div>


                    <!-- LOGO OPERADORA -->
                    <div class="col-xs-12 col-md-6 centralizada">
                        <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $operadora_id ?>" aria-expanded="true" aria-controls="collapse<?= $operadora_id ?>">
                            <?= !is_null($operadora['operadora']['imagen']) ? $this->Html->image("../" . $operadora['operadora']['imagen']['caminho'] . "/" . $operadora['operadora']['imagen']['nome'], ['class' => 'logoSimulacao']) : $operadora['operadora']['nome'] ?>
                            <p class='detalheOperadora'><?= $operadora['operadora']['detalhe'] ?></p>
                        </a>
                    </div>
                </div> <!-- /PANEL HEADING -->

                <div id="collapse<?= $operadora_id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $operadora_id ?>">
                    <div class="panel-body" style="border-top: none !important">
                        <table class="table table-condensed">
                            <tr>
                                <td class="beneficiarios" style="width: 4%;">
                                    Exibir<br>Cálculo
                                </td>
                                <?php
                                for ($i = 1; $i <= 10; $i++) {
                                    if ($simulacao['faixa' . $i] > 0) {
                                ?>
                                        <td class='beneficiarios negrito centralizadaVertical' style="border: solid 1px #ddd;">
                                            <?php
                                            switch ($i) {
                                                case 1:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                                    break;
                                                case 2:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                                    break;
                                                case 3:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                                    break;
                                                case 4:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                                    break;
                                                case 5:
                                                    echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                                    break;
                                                case 6:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                                    break;
                                                case 7:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                                    break;
                                                case 8:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                                    break;
                                                case 9:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                                    break;
                                                case 10:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 ou +";
                                                    break;
                                            } ?>

                                        </td>

                                <?php }
                                } ?>
                                <td class="beneficiarios negrito">
                                    <?php
                                    $vidas = (new Cake\Collection\Collection($simulacao->toArray()))->filter(function ($value, $key) {
                                        return str_starts_with($key, 'faixa');
                                    });
                                    echo "Total:<br>" . $vidas->count() . " Vida(s)";
                                    ?>
                                </td>
                            </tr>

                            <?php foreach ($operadora['tabelas'] as $tabela) :
                                $faixas = (new Cake\Collection\Collection($simulacao->toArray()))->filter(function ($value, $key) {
                                    return str_starts_with($key, 'faixa') && $value > 0;
                                })->count();
                            ?>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr style="background-color: #eee; border: solid 1px #ddd">
                                    <td class="centralizada beneficiarios">
                                        <?= $this->Form->input($tabela['id'], ['type' => 'checkbox', 'id' => 'imprimir' . $tabela['id'], 'value' => $tabela['id'], 'class' => 'noMarginBottom ' . $operadora_id, 'title' => 'Selecionar Para Impresão', 'label' => '']); ?>
                                    </td>
                                    <td class="beneficiariosTransparente centralizadaVertical centralizada" colspan="<?= $faixas + 1 ?>">

                                        <?php
                                        if ($tabela['coparticipacao'] === 's') {
                                            $co = "C/COPARTICIPAÇÃO " . $tabela['detalhe_coparticipacao'];
                                        } else {
                                            $co = "S/COPARTICIPAÇÃO ";
                                        }
                                        ?>

                                        <?= "<b>" . $tabela['nome'] . " - " . $tabela['descricao'] . " - "  . $tabela['acomodacao']['nome'] . " - "  . $co . " - " . $tabela["cobertura"]["nome"] . "</b> <br> " . $tabela['minimo_vidas'] . " à " . $tabela['maximo_vidas'] . " vidas - " . $tabela['acomodacao']['nome'] ?>
                                    </td>
                                </tr>

                                <tr style="border: solid 1px #ddd">
                                    <td>&nbsp;</td>
                                    <?php
                                    (new Cake\Collection\Collection($simulacao->toArray()))->each(function ($value, $key) use ($tabela) {
                                        if (str_starts_with($key, 'faixa') && $value > 0) {
                                    ?>
                                            <td class='beneficiariosTransparente centralizadaVertical'>
                                                <?php if ($tabela[$key] == 0) : ?>
                                                    ***
                                                <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                                    <strong>Tabela em Atualização de Preço</strong>
                                                <?php else : ?>
                                                    <?= $this->Number->currency($value * $tabela[$key], "R$ ") ?>
                                                    <br />
                                                    <span style="font-size: 100%;">
                                                        <?= number_format($tabela[$key], 2, ",", ".") ?> p/ vida
                                                    </span>
                                                <?php endif ?>
                                            </td>
                                    <?php }
                                    }); ?>

                                    <td class="beneficiariosTransparente  centralizada centralizadaVertical">
                                        <?php if ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Tabela em Atualização de Preço</strong>
                                        <?php else : ?>
                                            <?php
                                            $totalPorProduto = 0;
                                            for ($i = 1; $i <= 10; $i++) {
                                                $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * $tabela['faixa' . $i]);
                                            } ?>

                                            <b><?= $this->Number->currency($totalPorProduto) ?></b>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </table>

                        <div class="nobreak">
                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                OBSERVAÇÕES IMPORTANTES
                            </div>
                            <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                                <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong>
                                <br><br>
                                <?php foreach ($operadora['observacoes'] as $key => $value) :
                                    if (!empty($key)) : ?>
                                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                        </br></br>
                                        <?= $value['data'] ?>
                                        <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                                    <?php else : ?>
                                        <div class='info'>Consulte Operadora</div>
                                <?php endif;
                                endforeach; ?>
                            </div>
                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <div class="nobreak">
                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                REDE REFERENCIADA (Resumo) <span><?= (isset($operadora['operadora']['url_rede']) && !is_null($operadora['operadora']['url_rede'])) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['operadora']['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
                            </div>
                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php foreach ($operadora['redes'] as $key => $value) :
                                    if (!empty($key)) : ?>
                                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                        </br></br>
                                        <?= $value['data'] ?>
                                        <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                                    <?php else : ?>
                                        <div class='info'>Consulte Operadora</div>
                                <?php endif;
                                endforeach; ?>
                            </div>

                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <div class="nobreak">

                            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                            </div>

                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php foreach ($operadora['reembolsos'] as $key => $value) :
                                    if (!empty($key)) : ?>
                                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                        </br></br>
                                        <?= $value['data'] ?>
                                        <?= count($operadora['reembolsos']) > 1 ? '<hr>' : '' ?>
                                    <?php else : ?>
                                        <div class='info'>Consulte Operadora</div>
                                <?php endif;
                                endforeach; ?>
                            </div>

                        </div>

                        <div class="clearfix">&nbsp;</div>

                        <div class="nobreak">
                            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
                            </div>

                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <strong>Areas de Atendimento</strong>
                                <br>
                                <?php foreach ($operadora['atendimentos'] as $tipo => $tabelas) {
                                    echo '<strong>' . $tipo  . '</strong>: ' . $tabelas . '<br>';
                                } ?>
                                <br>
                                <strong>Areas de Comercialização</strong>
                                <br>
                                <?php foreach ($operadora['areas_comercializacoes'] as $key => $value) :
                                    if (!empty($key)) : ?>
                                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                        </br></br>
                                        <?= substr($value['data'], 0, -2) . '.' ?>
                                        <?= count($operadora['areas_comercializacoes']) > 1 ? '<hr>' : '' ?>
                                    <?php else : ?>
                                        <div class='info'>Consulte Operadora</div>
                                <?php endif;
                                endforeach; ?>
                            </div>

                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <div class="nobreak">

                            <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;">
                                CARÊNCIAS (Resumo)
                            </div>
                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php foreach ($operadora['carencias'] as $key => $value) :
                                    if (!empty($key)) : ?>
                                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                        </br></br>
                                        <?= $value['data'] ?>
                                        <?= count($operadora['carencias']) > 1 ? '<hr>' : '' ?>
                                    <?php else : ?>
                                        <div class='info'>Consulte Operadora</div>
                                <?php endif;
                                endforeach; ?>
                            </div>
                        </div>


                        <div class="clearfix">&nbsp;</div>
                        <div class="nobreak">
                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                DEPENDENTES
                            </div>
                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php foreach ($operadora['dependentes'] as $key => $value) :
                                    if (!empty($key)) : ?>
                                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                        </br></br>
                                        <?= $value['data'] ?>
                                        <?= count($operadora['dependentes']) > 1 ? '<hr>' : '' ?>
                                    <?php else : ?>
                                        <div class='info'>Consulte Operadora</div>
                                <?php endif;
                                endforeach; ?>
                            </div>
                            <div class="clearfix">&nbsp;</div>

                        </div>

                        <div class="clearfix">&nbsp;</div>
                        <div class="nobreak">
                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                                DOCUMENTOS NECESSÁRIOS
                            </div>
                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php foreach ($operadora['documentos'] as $key => $value) :
                                    if (!empty($key)) : ?>
                                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                        </br></br>
                                        <?= $value['data'] ?>
                                        <?= count($operadora['documentos']) > 1 ? '<hr>' : '' ?>
                                    <?php else : ?>
                                        <div class='info'>Consulte Operadora</div>
                                <?php endif;
                                endforeach; ?>
                            </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="nobreak">
                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                FORMAS DE PAGAMENTO
                            </div>
                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php foreach ($operadora['pagamentos'] as $key => $value) :
                                    if (!empty($key)) : ?>
                                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                        </br></br>
                                        <?= $value['data'] ?>
                                        <?= count($operadora['pagamentos']) > 1 ? '<hr>' : '' ?>
                                    <?php else : ?>
                                        <div class='info'>Consulte Operadora</div>
                                <?php endif;
                                endforeach; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <?php foreach ($operadoras as $operadora) :
            if (!in_array($operadora->id, array_keys($tabelas_ordenadas))) : ?>
                <div class="col-xs-12 alert alert-secondary" style="margin: 5px 0; border: 1px solid #ddd;">
                    <div class="col-md-4 col-xs-12 vermais" style="font-size: 12px;text-align: left; padding-left: 50px !important;">
                        <span class="text-danger"><?= $operadora->status == 'EM ATUALIZAÇÃO' ? 'OPERADORA EM PROCESSO DE ATUALIZAÇÃO' : 'SEM OPÇÕES PARA ESTE PERFIL DE CLIENTE' ?></span>
                    </div>
                    <div class="col-md-2 col-xs-12 vermais" style="font-size: 14px; text-align: center; padding-left: 1%;">
                        <a class="controle" href="<?= $operadora->url ?>" target="_blank"> Ver Material de Vendas</a>
                    </div>
                    <div class="col-md-6 col-xs-12 text-center" operadora-id='<?= $operadora->id ?>'>
                        <?= !is_null($operadora['imagen']) ? $this->Html->image("../" . $operadora['imagen']['caminho'] . "/" . $operadora['imagen']['nome'], ['class' => 'logoSimulacao']) : $operadora['nome'] ?>
                    </div>
                </div>
        <?php endif;
        endforeach; ?>
    </div>
    <?= $this->element("link-cotacao") ?>
<?php else : ?>
    <div class="clearfix">&nbsp;</div>
    <div class="alert alert-danger centralizada" role="alert"> Nenhuma tabela encontrada dentro dos Filtros selecionados</div>
<?php endif; ?>
<div class="spacer-md">&nbsp;</div>