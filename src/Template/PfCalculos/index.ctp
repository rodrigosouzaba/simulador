<div class="col-md-12" >
    <h4 style="padding: 10px 0" class="centralizada">Cálculos Saúde Pessoa Física</h4>
    <table cellpadding="0" cellspacing="0" style="font-size: 90%;" >
        <thead>
            <tr>
                <th class="centralizada col-md-1" ><?= $this->Paginator->sort('id', ['label' => 'Nº Cálculo']) ?></th>
                <th class="centralizada col-md-1"><?= $this->Paginator->sort('created', ['label' => 'Data']) ?></th>
                <th class="col-md-2"><?= $this->Paginator->sort('nome') ?></th>
                <th class="centralizada col-md-1">Vidas</th>
                <th class="centralizada col-md-1"><?= $this->Paginator->sort('pf_profissao_id', ['label' => 'Profissão']) ?></th>
                <th class="centralizada col-md-1">Selecione Status</th>
                <th class="actions col-md-2" ><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfCalculos as $pfCalculo): ?>
                <tr style="font-size: 90% !important;">

                    <td class="centralizada"><?= $pfCalculo->id ?></td>
                    <td class="centralizada"><?= h($pfCalculo->created) ?></td>
                    <td>
                        <span data-toggle="popover" data-html="true" data-placement="top" data-trigger="click" title="Dados do Cliente" 
                              data-content="<div style='text-align:left;'><b>Telefone: </b><?= $pfCalculo['telefone'] ?> <br> <b>Email: </b><?= $pfCalculo['email'] ?> </div>" >
                            <?=
                            $this->Html->link('', '#historico' . $pfCalculo['id'], [
                                'class' => 'fa fa-info-circle listaracompanhamentos',
                                'style' => 'margin: 0 5px;']);
                            ?>
                        </span>
                        <?= $pfCalculo->nome ?>
                    </td>

                    <td class="centralizada"><?= $pfCalculo->has('pf_calculos_dependentes') ? count($pfCalculo->pf_calculos_dependentes) + 1 : 1 ?></td>
                    <td class="centralizada"><?= $pfCalculo->has('pf_profisso') ? $pfCalculo->pf_profisso->nome : '' ?></td>
                    <td class="centralizada">
                        <?= $this->Form->input('status_id', ['label' => '', 'value' => $pfCalculo->status_id, 'empty' => 'SEM STATUS', 'options' => $status, 'pfcalculo_id' => $pfCalculo->id, 'class' => 'change_status']) ?>

                    </td>
                    <td class="actions">

                        <?=
                        $this->Html->link('', '#historico' . $pfCalculo->id, [
                            'class' => 'btn btn-sm btn-default fa fa-list listarpdf',
                            'role' => 'button',
                            'value' => $pfCalculo->id,
                            'data-toggle' => 'modal',
                            'data-target' => '#modalFiltro',
                            'aria-expanded' => "false"
                        ])
                        ?>
                        </span>
                        <?=
                        $this->Html->link('', ['action' => 'edit', $pfCalculo->id], [
                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Cálculo',
                            'class' => 'btn btn-sm btn-default glyphicon glyphicon-edit'])
                        ?>

                        <?=
                        $this->Html->link('', ['action' => 'view', $pfCalculo->id], [
                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Visualizar Cálculo',
                            'title' => __('Visualizar Cálculo'),
                            'class' => 'btn btn-sm btn-default glyphicon glyphicon-eye-open'])
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>





<!-- Button trigger modal -->
<button type="button" id="loader" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#modalReenvio">
</button>
<!-- Modal -->
<div class="modal fade" id="modalReenvio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span style="margin-right: 20px !important;">Processando </span><img src='../../img/spinner.gif' style='max-height: 60px;margin:-10px;' /> </h4> 
            </div>

        </div>
    </div>
</div>

<!-- Modal FILTRO-->
<div class="modal fade" id="modalFiltro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="conteudoModal">

        </div>
    </div>
</div>


<script type="text/javascript">
    $('[data-toggle="popover"]').popover();

    $("#busca").hide();
    $("#tipo-busca").change(function () {
        $("#busca").show();
//       

    });
    
    $(".change_status").change(function () {
//    console.log();
        $.ajax({
            type: "POST",
            url: "<?= $this->request->webroot ?>pf-calculos/alterar-status/" + $(this).attr("pfcalculo_id") + "/" + (this).value,
            success: function (data) {
                location.reload();
            }
        });
    });
//    $('.collapse').collapse()
    
    $(".listarpdf").click(function () {
//        alert();
//         var id = document.getElementsByTagName("H1")[0].getAttribute("class"); 
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>pf-calculos/listarpdfs/" + $(this).attr("value"),
//            beforeSend: function () {
//                $('#loading').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
//            },
            success: function (data) {
                $("#conteudoModal").empty();
                $("#conteudoModal").append(data);
//                $("#loading").empty();
            }
        });
    });

    $(".reenvio").click(function () {
        $.ajax({
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#close').trigger('click');
            }
        });



    });
</script>