<style type="text/css">
    .jumbotron {
        background-color: #eee;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 10px;
        line-height: 1.5;
        width: 100%;
        /*width: 97%;*/
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }


    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
        font-size: 60%;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .totalTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
        font-weight: bold;
    }

    .fonteReduzida {
        font-size: 10px;
        text-align: justify !important;
        text-align: justify;
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        line-height: 1.3;
        margin-bottom: 0;
        cursor: default;
        display: block;
        font-style: normal;
        font-variant-caps: normal;
        vertical-align: baseline;
        white-space: normal;

    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .nobreak {
        page-break-inside: avoid;
    }

    .info {
        color: #a9a9a9;
        font-weight: lighter;
        font-style: italic;
        text-align: center;
        width: 100%;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .verde {
        color: #128c7e;
    }

    .pagina {
        padding: 20px;
        margin-top: 170px;
    }

    @media(max-width: 767px) {
        .pagina {
            margin-top: 160px;
        }
    }

    @media(min-width: 768px) and (max-width: 895px) {
        .pagina {
            margin-top: 250px;
        }
    }

    @media(min-width: 896px) and (max-width: 1218px) {
        .pagina {
            margin-top: 200px;
        }
    }
</style>

<?php
$texto = "Visualize sua cotação aqui: https://corretorparceiro.com.br/app/pf-calculos/cotacao/";
$linkWhats = 'https://wa.me?text=' . $texto . $filtro->id;
?>
<?= $this->Form->create('dadosPDF', ['id' => 'dadosPDF', 'url' => ['action' => 'pdfNewCotacao', $filtro->id]]); ?>
<?= $this->Form->input("simulacao_id", ["type" => "hidden", "value" => $simulacao['id']]); ?>
<?= $this->Form->input("observacao", ["type" => "hidden", "value" => $simulacao['observaco']]); ?>
<?= $this->Form->input("rede", ["type" => "hidden", "value" => $simulacao['rede']]); ?>
<?= $this->Form->input("reembolso", ["type" => "hidden", "value" => $simulacao['reembolso']]); ?>
<?= $this->Form->input("carencia", ["type" => "hidden", "value" => $simulacao['carencia']]); ?>
<?= $this->Form->input("documento", ["type" => "hidden", "value" => $simulacao['documento']]); ?>
<?php
if (isset($filtroPDF)) :
?>
    <?= $this->Form->input("pf_atendimento_id", ["type" => "hidden", "value" => $filtroPDF['pf_atendimento_id']]); ?>
    <?= $this->Form->input("pf_acomodacao_id", ["type" => "hidden", "value" => $filtroPDF['pf_acomodacao_id']]); ?>
    <?= $this->Form->input("tipo_produto_id", ["type" => "hidden", "value" => $filtroPDF['tipo_produto_id']]); ?>
    <?= $this->Form->input("coparticipacao", ["type" => "hidden", "value" => $filtroPDF['coparticipacao']]); ?>
    <?= $this->Form->input("filtroreembolso", ["type" => "hidden", "value" => $filtroPDF['filtroreembolso']]); ?>
<?php
endif;
foreach ($findTabelas as $key => $dadoPDF) {
    if ($key != "simulacao_id" && $key != "pf_atendimento_id" && $key != "pf_acomodacao_id" && $key != "tipo_produto_id" && $key != "coparticipacao" && $key != "filtroreembolso" && substr($key, 0, 5) != "check" && $key != "observacao" && $key != "rede" && $key != "reembolso" && $key != "carencia" && $key != "documento") {
        if ($dadoPDF != 0) {
            echo $this->Form->input($key, ["type" => "hidden", "value" => $key]);
        }
    }
}
?>
<!-- HIDDEN FIELD COM O TIPO DE AÇÃO REFERENTE AO PDF (DOWNLOAD ou GERAÇÃO NO BROWSER) VIA jQuery -->
<?= $this->Form->input("tipopdf", ["type" => "hidden", "value" => ""]); ?>
<?= $this->Form->end(); ?>

<?php if (isset($sessao)) : ?>
    <?= $this->element('new_acoes'); ?>
<?php else : ?>
    <div class="centralizada well well-sm" id="acoesgerador">
        <!-- DOWNLOAD DO PDF -->
        <?= $this->Form->button($this->Html->tag('span', '', ['class' => 'fa fa-download', 'aria-hidden' => 'true']) . " Download", ['class' => ' btn btn-lg btn-default', "id" => "pdfdownload", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "bottom", "title" => "Baixar em PDF"]); ?>
    </div>
<?php endif; ?>
<div class="container">
    <div id="header">
        <?php echo $this->element('cabecalho_cotacao'); ?>
    </div>

    <?php foreach ($tabelas_ordenadas as $chave => $operadora) : ?>
        <div class="spacer-md">&nbsp</div>
        <table style="margin-top: 40px;">
            <tr>
                <td style="text-align: justify; line-height: 16px;">
                    À <b><?php echo $simulacao['nome'] ?></b>, A/C: <b><?php echo $simulacao['contato'] ?></b>.<br />
                    Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.<br />
                    Informamos que os valores, regras de comercialização e condições contratuais são determinadas pelas seguradoras/operadoras dos planos<br />
                    e podem ser alterados pelas mesmas a qualquer momento.
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table style="width: 100%" cellspacing="0">
            <tr>
                <td style="width: 15%; text-align: left; font-weight:bold;">
                    <?= $this->Html->image('/webroot/' . $operadora['operadora']['imagen']['caminho'] . $operadora['operadora']['imagen']['nome'], ['fullBase' => true, 'height' => 40]) ?>
                </td>
                <td style="text-align: left; font-weight:bold;">
                    <?= $operadora['operadora']['nome'] . '<br/><small style="font-weight: 100">' . $operadora['operadora']['detalhe'] . "</small>" ?>
                </td>
            </tr>
        </table>

        <br />
        <?php
        $corfundo = '#eee';
        $tamanhoFonte = 'font-size:95%';
        if (!in_array($operadora['operadora']['id'], $ids_vitalmed)) :
            $faixas = (new Cake\Collection\Collection($simulacao->toArray()))->filter(function ($value, $key) {
                return str_starts_with($key, 'faixa')  && !str_starts_with($key, 'faixa_extra') && !str_starts_with($key, 'faixa_promo') && $value > 0;
            })->count();
            $largura = 100 / ($faixas + 1);
        ?>
            <table style="width: 100%; border:  solid 0.1mm #ddd;<?php echo $tamanhoFonte ?>" cellspacing="0">
                <tr style="width: 100% !important; ">
                    <?php
                    $fim = 10;
                    for ($i = 1; $i <= $fim; $i++) {
                        if ($simulacao['faixa' . $i] > 0) {
                    ?>
                            <td style="border: solid 0.1mm #dddddd;text-align:center; width: <?php echo $largura ?>%;<?php echo $tamanhoFonte ?>">
                                <?php
                                switch ($i) {
                                    case 1:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                        break;
                                    case 2:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                        break;
                                    case 3:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                        break;
                                    case 4:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                        break;
                                    case 5:
                                        echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                        break;
                                    case 6:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                        break;
                                    case 7:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                        break;
                                    case 8:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                        break;
                                    case 9:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                        break;
                                    case 10:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 ou +";
                                        break;
                                }
                                ?>

                            </td>

                    <?php
                        }
                    }
                    ?>
                    <td class="negrito" style=" border:solid 0.1mm #dddddd;width: <?php echo $largura ?>%;text-align:center;<?php echo $tamanhoFonte ?>">
                        <?php
                        $x = 1;
                        $vidas = 0;
                        for ($x = 1; $x <= 10; $x++) {
                            $vidas = $vidas + $simulacao['faixa' . $x];
                        }
                        echo "Total:<br>" . $vidas . " Vida(s)";
                        ?>
                    </td>
                </tr>
                <?php foreach ($operadora['tabelas'] as $produto) { ?>
                    <tr>
                        <td></td>
                    </tr>
                    <tr style="background-color: #ccc; text-align: center;">
                        <td class="centralizada" style="padding: 5px 0 5px 3px; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a" colspan="<?php echo ($faixas + 1) ?>">
                            <?php
                            if ($produto['coparticipacao'] == 's') {
                                $co = "C/COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'];
                            } else {
                                $co = "S/COPARTICIPAÇÃO ";
                            }

                            if ($produto['tipo_contratacao'] == '0') {
                                $tipo_contratacao = "OPCIONAL";
                            } else {
                                $tipo_contratacao = "COMPULSÓRIO";
                            }
                            ?>
                            <?= "<b>" . $produto['nome'] . " - " . $produto['pf_areas_comercializaco']['nome'] . " - " . $produto["pf_cobertura"]["nome"] . "</b> <br> " .
                                $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas. " . $produto['pf_acomodaco']['nome'] . ' - ' . $co  ?>

                        </td>
                    </tr>
                    <tr style="background-color: #fff;border-left: solid 0.1mm #5a5a5a;  ">
                        <?php
                        for ($i = 1; $i <= 10; $i++) {

                            if ($simulacao['faixa' . $i] > 0) {
                                if ($i == 1) {
                                    $bordaEsquerda = "border-left: solid 0.1mm #5a5a5a;";
                                } else {
                                    $bordaEsquerda = "border-left:solid 0.1mm #dddddd;";
                                }
                        ?>
                                <td style="width: <?php echo $largura ?>%;<?php echo $bordaEsquerda ?>; text-align:center; border-bottom: solid 0.1mm #5a5a5a; padding: 10px 0; vertical-align: middle">
                                    <?php if ($produto['faixa' . $i] == 0) : ?>
                                        ***
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Tabela em Atualização de Preço</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format($simulacao['faixa' . $i] * ($produto['faixa' . $i] + $produto['odonto_valor']), ["before" => "R$ ", "places" => 2]) ?>
                                        <br />

                                        <small style="font-size:75%"> <?= $this->Number->format(($produto['faixa' . $i] + $produto['odonto_valor']), ["before" => "R$ ", "places" => 2]) ?> p/ vida</small>
                                    <?php endif ?>
                                </td>
                        <?php
                            }
                        }
                        ?>
                        <td style="font-weight: bold;width: <?php echo $largura ?>%; text-align: center;border-left:solid 0.1mm #dddddd;border-bottom: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a;<?php echo $tamanhoFonte ?>">
                            <?php if ($produto['atualizacao'] == 1) : ?>
                                <strong>Tabela em Atualização de Preço</strong>
                            <?php else :
                                $totalPorProduto = 0;
                                for ($i = 1; $i <= 10; $i++) {
                                    $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * ($produto['faixa' . $i] + $produto['odonto_valor']));
                                }
                            ?>
                                <b><?= $this->Number->format($totalPorProduto, ["before" => "R$ ", "places" => 2]) ?></b>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>

        <?php else : ?>
            <table style="width: 100%; border:  solid 0.1mm #ddd;<?php echo $tamanhoFonte ?>" cellspacing="0">
                <?php foreach ($operadora['tabelas'] as $produto) {
                    $faixas = (new Cake\Collection\Collection($simulacao->toArray()))->filter(function ($value, $key) use ($produto) {
                        if ($produto->tipo_faixa == '3')
                            return str_starts_with($key, 'faixa_promo_') && $value > 0;
                        return str_starts_with($key, 'faixa_extra_') && $value > 0;
                    })->count();
                    $largura = 100 / ($faixas + 1);
                ?>
                    <tr style="width: 100% !important; ">
                        <?php
                        $fim = $produto->tipo_faixa == '3' ? 3 : 6;
                        for ($i = 1; $i <= $fim; $i++) {
                            if (($fim == 3 && $simulacao['faixa_promo_' . $i] > 0) || ($fim == 6 && $simulacao['faixa_extra_' . $i] > 0)) { ?>
                                <td style="border: solid 0.1mm #dddddd;text-align:center; width: <?php echo $largura ?>%;<?php echo $tamanhoFonte ?>">
                                    <?php
                                    switch ($i) {
                                        case 1:
                                            echo $produto->tipo_faixa == '3' ?  $simulacao['faixa_promo_' . $i] .  " vida(s)<br/> 18 à 39 anos" : $simulacao['faixa_extra_' . $i] .  " vida(s)<br/> 0 à 14";
                                            break;
                                        case 2:
                                            echo $produto->tipo_faixa == '3' ?  $simulacao['faixa_promo_' . $i] .  " vida(s)<br/> 40 à 58 anos" : $simulacao['faixa_extra_' . $i] .  " vida(s)<br/> 15 à 29";
                                            break;
                                        case 3:
                                            echo $produto->tipo_faixa == '3' ?  $simulacao['faixa_promo_' . $i] .  " vida(s)<br/> + de 59 anos" : $simulacao['faixa_extra_' . $i] .  " vida(s)<br/> 30 à 39";
                                            break;
                                        case 4:
                                            echo $produto->tipo_faixa == '3' ? "" : $simulacao['faixa_extra_' . $i] .  " vida(s)<br/> 40 à 49 ";
                                            break;
                                        case 5:
                                            echo $produto->tipo_faixa == '3' ?  "" : $simulacao['faixa_extra_' . $i] .  " vida(s)<br/> 50 à 58";
                                            break;
                                        case 6:
                                            echo $produto->tipo_faixa == '3' ? "" : $simulacao['faixa_extra_' . $i] .  " vida(s)<br/> 59 ou +";
                                            break;
                                    } ?>

                                </td>

                        <?php }
                        }
                        ?>
                        <td class="negrito" style=" border:solid 0.1mm #dddddd;width: <?php echo $largura ?>%;text-align:center;<?php echo $tamanhoFonte ?>">
                            <?php
                            $vidas = (new Cake\Collection\Collection($simulacao->toArray()))->filter(function ($value, $key) use ($produto) {
                                if ($produto->tipo_faixa == '3')
                                    return str_starts_with($key, 'faixa_promo_') && $value > 0;
                                return str_starts_with($key, 'faixa_extra_') && $value > 0;
                            })->toList();
                            echo "Total:<br>" . array_sum($vidas) . " Vida(s)";
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr style="background-color: #ccc; text-align: center;">
                        <td class="centralizada" style="padding: 5px 0 5px 3px; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a" colspan="<?php echo ($faixas + 1) ?>">
                            <?php
                            $co = $produto['coparticipacao'] == 's' ? "C/COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'] : "S/COPARTICIPAÇÃO ";
                            $tipo_contratacao = $produto['tipo_contratacao'] == '0' ? "OPCIONAL" : "COMPULSÓRIO";
                            echo "<b>" . $produto['nome'] . " - " . $produto['pf_areas_comercializaco']['nome'] . " - " . $produto["pf_cobertura"]["nome"] . "</b> <br> " .
                                $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas. " . $produto['pf_acomodaco']['nome'] . ' - ' . $co  ?>

                        </td>
                    </tr>
                    <tr style="background-color: #fff;border-left: solid 0.1mm #5a5a5a;  ">
                        <?php
                        (new Cake\Collection\Collection($simulacao->toArray()))->each(function ($value, $key) use ($produto, $largura) {
                            if (str_starts_with($key, 'faixa_extra') && $value > 0) {
                                $bordaEsquerda = (int) substr($key, 12) == 1 ? "border-left: solid 0.1mm #5a5a5a;" : "border-left:solid 0.1mm #dddddd;";
                        ?>
                                <td style="width: <?= $largura ?>%;<?= $bordaEsquerda ?>; text-align:center; border-bottom: solid 0.1mm #5a5a5a; padding: 10px 0; vertical-align: middle">
                                    <?php if ($produto[$key] == 0) : ?>
                                        ***
                                    <?php elseif ($produto['atualizacao'] == 1) : ?>
                                        <strong>Tabela em Atualização de Preço</strong>
                                    <?php else : ?>
                                        <?= $this->Number->format($value * ($produto[$key] + $produto['odonto_valor']), ["before" => "R$ ", "places" => 2]) ?>
                                        <br />
                                        <span style="font-size: 100%;">
                                            <?= $this->Number->format(($produto[$key] + $produto['odonto_valor']), ["before" => "R$ ", "places" => 2]) ?> p/ vida
                                        </span>
                                    <?php endif ?>
                                </td>
                        <?php }
                        }); ?>
                        <td class="beneficiariosTransparente  centralizada centralizadaVertical" style="border-bottom: solid 0.1mm #5a5a5a;">
                            <?php if ($produto['atualizacao'] == 1) : ?>
                                <strong>Tabela em Atualização de Preço</strong>
                            <?php else : ?>
                                <?php
                                $totalPorProduto = 0;
                                for ($i = 1; $i <= 6; $i++) {
                                    $nome_faixa = ($produto->tipo_faixa == '3' ? 'faixa_promo_' : 'faixa_extra_') . $i;
                                    $totalPorProduto = $totalPorProduto + ($simulacao[$nome_faixa] * ($produto[$nome_faixa] + $produto['odonto_valor']));
                                } ?>
                                <b><?= $this->Number->format($totalPorProduto, ["before" => "R$ ", "places" => 2]) ?></b>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="<?= $faixas + 1 ?>">&nbsp;</td>
                    </tr>
                <?php } ?>
            </table>
        <?php endif; ?>
        <div class="clearfix">&nbsp;</div>

        <?php if (!empty($operadora['grupo_entidades'])) : ?>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    ENTIDADES E PROFISSÕES
                </div>
                <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                    <?php foreach ($operadora['grupo_entidades'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
        <?php endif; ?>

        <!-- FORMAS DE PAGAMENTOS -->
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            FORMAS DE PAGAMENTO
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php foreach ($operadora['pagamentos'] as $key => $value) :
                if (!empty($key)) : ?>
                    <strong>Produtos: </strong><?= $value['tabelas'] ?>
                    </br></br>
                    <?= $value['data'] ?>
                    <?= count($operadora['pagamentos']) > 1 ? '<hr>' : '' ?>
                <?php else : ?>
                    <div class='info'>Consulte Operadora</div>
            <?php endif;
            endforeach; ?>
        </div>
        <div class="clearfix">&nbsp;</div>
        <!-- /FORMAS DE PAGAMENTOS -->

        <!-- OBSERVAÇÕES IMPORTANTES -->
        <div class="nobreak">
            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                OBSERVAÇÕES IMPORTANTES
            </div>
            <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong>
                <br><br>
                <?php foreach ($operadora['observacoes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= $value['data'] ?>
                        <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>

        </div>
        <div class="clearfix">&nbsp;</div>
        <!-- OBSERVAÇÕES IMPORTANTES  -->


        <!-- DEPENDENTES  -->
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            DEPENDENTES
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php foreach ($operadora['dependentes'] as $key => $value) :
                if (!empty($key)) : ?>
                    <strong>Produtos: </strong><?= $value['tabelas'] ?>
                    </br></br>
                    <?= $value['data'] ?>
                    <?= count($operadora['dependentes']) > 1 ? '<hr>' : '' ?>
                <?php else : ?>
                    <div class='info'>Consulte Operadora</div>
            <?php endif;
            endforeach; ?>
        </div>
        <div class="clearfix">&nbsp;</div>
        <!-- /DEPENDENTES  -->


        <!-- REDE  -->
        <?php if ($simulacao['rede'] == 1) : ?>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    REDE REFERENCIADA (Resumo) <span><?= (isset($operadora['url_rede']) && !is_null($operadora['url_rede'])) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php foreach ($operadora['redes'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
        <?php endif; ?>
        <!-- /REDE  -->


        <!-- OPCIONAIS  -->
        <?php if ($simulacao['reembolso'] == 1) : ?>
            <div class="nobreak">
                <div style="width: 100%;max-width: 100%;  background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                </div>
                <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 100%; max-width: 100%;">
                    <?php foreach ($operadora['redes'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
        <?php endif; ?>
        <!-- /OPCIONAIS  -->


        <!-- CARENCIAS -->
        <?php
        if ($simulacao['carencia'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                    CARÊNCIAS (Resumo)
                </div>
                <div class="fonteReduzida" style="padding: 5px;width: 100%; border: 0.1mm solid #ccc;max-width: 100% !important">
                    <?php foreach ($operadora['carencias'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['carencias']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
        <?php } ?>
        <!-- /CARENCIAS -->



        <!-- COMERCIALIZACAO -->
        <div class="nobreak">
            <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; width: 100%;max-width: 100% !important">
                ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
            </div>
            <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 100%;max-width: 100% !important">
                <strong>Areas de Atendimento</strong>
                <br>
                <?php foreach ($operadora['atendimentos'] as $tipo => $tabelas) {
                    echo '<strong>' . $tipo  . '</strong>: ' . $tabelas . '<br>';
                } ?>
                <br>
                <strong>Areas de Comercialização</strong>
                <br>
                <?php foreach ($operadora['areas_comercializacoes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        </br></br>
                        <?= substr($value['data'], 0, -2) . '.' ?>
                        <?= count($operadora['areas_comercializacoes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>
        <!-- /COMERCIALIZACAO -->

        <!-- DOCUMENTOS -->
        <?php
        if ($simulacao['documento'] == 1) {
        ?>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                    DOCUMENTOS NECESSÁRIOS
                </div>
                <div class="fonteReduzida" style="padding: 5px;width: 100%; border: 0.1mm solid #ccc;max-width: 100% !important">
                    <?php foreach ($operadora['documentos'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            </br></br>
                            <?= $value['data'] ?>
                            <?= count($operadora['documentos']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
        <?php } ?>
        <!-- /DOCUMENTOS -->



    <?php endforeach; ?>

    <div id="footer" style="font-size: 15px;">
        <div class="rodape-home col-xs-12" style="text-align: center;">
            <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
                <span style="text-decoration: none !important; ; font-size: 16px;"> corretorparceiro.com.br</span>
            </a>
            <br>
            <span style="color: #004057; font-size: 16px;">Assessoria de Apoio a Corretores</span>
        </div>
    </div>
</div>
<?= $this->element('modal-login'); ?>
<?= $this->element('cotacao-email'); ?>

<script>
    var action = '<?= $this->request->params["action"] ?>';
    if (action === 'filtro') {
        var serialize = 'operadorasFiltro';
    } else {
        var serialize = 'operadoras';
    }

    $("#pdfdownload").click(function(event) {
        event.preventDefault();
        $("#tipopdf").val("D");
        $("#dadosPDF").attr("target", "");
        $("#dadosPDF").submit();
    });
    $("#whatsapp").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        let link = "<?= $linkWhats ?>";
        if (sessao != "") {
            window.open(link);
        } else {
            $("#modal-login").modal("show");
        }

    });
    $("#btn-email").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        if (sessao != "") {
            $("#modal-compartilhamento").modal("show");
        } else {
            $("#modal-login").modal("show");
        }
    });
</script>