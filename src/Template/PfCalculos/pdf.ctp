<style type="text/css">
    .jumbotron {
        background-color: #eee;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 10px;
        line-height: 1.5;
        width: 100%;
        /*width: 97%;*/
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }


    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
        font-size: 60%;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .totalTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
        font-weight: bold;
    }

    .fonteReduzida {
        font-size: 10px;
        text-align: justify !important;
        text-align: justify;
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        line-height: 1.3;
        margin-bottom: 0;
        cursor: default;
        display: block;
        font-style: normal;
        font-variant-caps: normal;
        vertical-align: baseline;
        white-space: normal;

    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .nobreak {
        page-break-inside: avoid;
    }

    .info {
        color: #a9a9a9;
        font-weight: lighter;
        font-style: italic;
        text-align: center;
        width: 100%;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }
</style>
<?php foreach ($tabelasOrdenadas as $chave => $tabelas) { ?>
    <page backtop="32mm" backbottom="7mm" backleft="5mm" backright="5mm" style="font-size: 11px">

        <page_header style="margin-right: 20px;margin-left: 20px; margin-top: 20px; width: 100%; margin-bottom: 300px !important;">
            <?php echo $this->element('cabecalho_pdf'); ?>
        </page_header>

        <page_footer style="font-size: 15px;">
            <div class="rodape-home col-xs-12" style="text-align: center;">
                <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
                    <span style="text-decoration: none !important"> corretorparceiro.com.br</span>
                </a>
                <br>
                <span style="color: #004057">Assessoria de Apoio a Corretores</span>
            </div>
        </page_footer>
        <table>
            <tr>
                <td style="text-align: justify; line-height: 16px;">
                    À <b><?php echo $simulacao['nome'] ?></b>, A/C: <b><?php echo $simulacao['contato'] ?></b>.<br />
                    Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.<br />
                    Informamos que os valores, regras de comercialização e condições contratuais são determinadas pelas seguradoras/operadoras dos planos<br />
                    e podem ser alterados pelas mesmas a qualquer momento.
                </td>
            </tr>
        </table>

        <?php
        $faixas = null;
        $i = 1;
        for ($i = 1; $i <= 10; $i++) {
            if ($simulacao['faixa' . $i] > 0) {
                $faixas = $faixas + 1;
            }
        }
        $largura = 100 / ($faixas + 1);
        $tamanhoFonte = 'font-size:95%';
        ?>

        <br /> <br />


        <table style="width: 100%" cellspacing="0">
            <tr>
                <td style="width: 15%; text-align: left; font-weight:bold;">
                    <img src='<?= './' . $operadoras[$chave]['imagen']['caminho'] . $operadoras[$chave]['imagen']['nome']; ?>' height="40" /><br />
                </td>
                <td style="text-align: left; font-weight:bold;">
                    <?= $operadoras[$chave]['nome'] . '<br/><small style="font-weight: 100">' . $operadoras[$chave]['detalhe'] . "</small>" ?>
                </td>
            </tr>
        </table>

        <br />
        <?php $corfundo = '#eee'; ?>
        <table style="width: 100%; border:  solid 0.1mm #ddd;<?php echo $tamanhoFonte ?>" cellspacing="0">
            <tr style="width: 100% !important; ">
                <?php
                $i = 1;
                for ($i = 1; $i <= 10; $i++) {

                    if ($simulacao['faixa' . $i] > 0) {
                ?>
                        <td style="border: solid 0.1mm #dddddd;text-align:center; width: <?php echo $largura ?>%;<?php echo $tamanhoFonte ?>">
                            <?php
                            switch ($i) {
                                case 1:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                    break;
                                case 2:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                    break;
                                case 3:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                    break;
                                case 4:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                    break;
                                case 5:
                                    echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                    break;
                                case 6:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                    break;
                                case 7:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                    break;
                                case 8:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                    break;
                                case 9:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                    break;
                                case 10:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 ou +";
                                    break;
                            }
                            ?>

                        </td>

                <?php
                    }
                }
                ?>
                <td class="negrito" style=" border:solid 0.1mm #dddddd;width: <?php echo $largura ?>%;text-align:center;<?php echo $tamanhoFonte ?>">
                    <?php
                    $x = 1;
                    $vidas = 0;
                    for ($x = 1; $x <= 10; $x++) {
                        $vidas = $vidas + $simulacao['faixa' . $x];
                    }
                    echo "Total:<br>" . $vidas . " Vida(s)";
                    ?>
                </td>
            </tr>
            <?php
            foreach ($tabelas as $produto) {

                foreach ($produto as $produto) {
            ?>
                    <tr>
                        <td></td>
                    </tr>
                    <tr style="background-color: #ccc; text-align: center;">
                        <td style="padding: 5px 0 5px 3px; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a" colspan="<?php echo $faixas + 1 ?>">
                            <?php
                            if ($produto['coparticipacao'] == 's') {
                                $co = "C/COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'];
                            } else {
                                $co = "S/COPARTICIPAÇÃO ";
                            }
                            ?>


                            <?php
                            if ($produto['tipo_contratacao'] == '0') {
                                $tipo_contratacao = "OPCIONAL";
                            } else {
                                $tipo_contratacao = "COMPULSÓRIO";
                            }
                            ?>
                            <?= "<b>" . $produto['descricao'] . " - " . $co . " - " . $produto["pf_produto"]["tipos_produto"]["nome"] . "</b> <br> " .
                                $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas - " . $produto['pf_acomodaco']['nome'] ?>

                        </td>
                    </tr>
                    <tr style="background-color: #fff;border-left: solid 0.1mm #5a5a5a;  ">
                        <?php
                        $i = 1;


                        for ($i = 1; $i <= 10; $i++) {

                            if ($simulacao['faixa' . $i] > 0) {
                                if ($i == 1) {
                                    $bordaEsquerda = "border-left: solid 0.1mm #5a5a5a;";
                                } else {
                                    $bordaEsquerda = "border-left:solid 0.1mm #dddddd;";
                                }
                        ?>
                                <td style="width: <?php echo $largura ?>%;<?php echo $bordaEsquerda ?>; text-align:center; border-bottom: solid 0.1mm #5a5a5a; padding: 10px 0; vertical-align: middle">
                                    <?php if ($produto['faixa' . $i] == 0) : ?>
                                        ***
                                    <?php else : ?>
                                        <?php echo $this->Number->currency($simulacao['faixa' . $i] * $produto['faixa' . $i]) ?>
                                        <br />

                                        <small style="font-size:75%"> <?php echo number_format($produto['faixa' . $i], 2, ",", ".") ?> p/ vida</small>
                                    <?php endif ?>
                                </td>
                        <?php
                            }
                        }
                        ?>
                        <td style="font-weight: bold;width: <?php echo $largura ?>%; text-align: center;border-left:solid 0.1mm #dddddd;border-bottom: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a;<?php echo $tamanhoFonte ?>">
                            <?php
                            $i = 1;
                            $totalPorProduto = 0;
                            for ($i = 1; $i <= 10; $i++) {
                                $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * $produto['faixa' . $i]);
                            }
                            $operadorascomiof = array(1, 28, 29, 30, 31, 16);
                            if (in_array($produto['operadora']['id'], $operadorascomiof)) {
                            ?>
                                <?php echo $this->Number->currency($totalPorProduto) ?>
                                <?php
                                echo
                                    "<br>
                                + IOF: " . $this->Number->currency($totalPorProduto * 0.0238, "R$ ") . "<br>"
                                        . "<b>" . $this->Number->currency($totalPorProduto * 1.0238, "R$ ") . "</b>"
                                ?>
                            <?php } else { ?>

                                <b><?php echo $this->Number->currency($totalPorProduto) ?></b>
                            <?php } ?>
                        </td>

                    </tr>
            <?php
                }
            }
            ?>
        </table>


        <!-- ENTIDADES DE CLASSE -->
        <div class="clearfix">&nbsp;</div>


        <div class="nobreak">
            <?php
            foreach ($operadoras as $operadora) {
                if (($operadora['id'] == $chave) && ($produto['modalidade'] <> 'INDIVIDUAL')) {
            ?>

                    <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                        ENTIDADES E PROFISSÕES
                    </div>
                    <div class="fonteReduzida" style="font-size: 8px;padding: 5px;width: 100%; border: 0.1mm solid #ccc; text-align: justify !important">
                        <?php
                        if (isset($entidadesOrdenadas[$produto["pf_produto"]["pf_operadora"]["id"]])) {

                            foreach ($entidadesOrdenadas[$produto["pf_produto"]["pf_operadora"]["id"]] as $entidade => $profissoes) {
                                $indice = 1;
                                foreach ($profissoes as $profissao) {
                                    if ($indice === 1) {
                                        echo "<b>" . $entidade . ":</b> ";
                                    }
                                    if ($indice === count($profissoes)) {
                                        echo $profissao . ". ";
                                    } else {
                                        echo $profissao . ", ";
                                    }
                                    $indice++;
                                }
                            }
                        }

                        ?>

                    </div>
            <?php
                }
            }
            ?>
        </div>
        <!-- /ENTIDADES DE CLASSE -->

        <!-- FORMAS DE PAGAMENTOS -->

        <div class="clearfix">&nbsp;</div>
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            FORMAS DE PAGAMENTO
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php
            foreach ($tabelas as $produto) {
                foreach ($produto as $produto) {
                    //                                            debug($produto);
                    //                                            die();
                    echo ($produto['pf_produto']['pf_operadora']['pf_formas_pagamentos'] != null) ? nl2br($produto['pf_produto']['pf_operadora']['pf_formas_pagamentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";

                    //                    echo ($produto['pf_operadora']['pf_formas_pagamentos'] != null) ? $produto['pf_operadora']['pf_formas_pagamentos'][0]['descricao'] : "<div class='info'>Consulte Operadora</div>";
                    break;
                }
                break;
            }
            ?>
        </div>
        <!-- /FORMAS DE PAGAMENTOS -->

        <!-- OBSERVAÇÕES IMPORTANTES -->


        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">

            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                OBSERVAÇÕES IMPORTANTES
            </div>



            <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
                <?php
                foreach ($tabelas as $produto) {
                    foreach ($produto as $produto) {
                        echo ($produto['pf_produto']['pf_operadora']['pf_observacoes'] != null) ? nl2br($produto['pf_produto']['pf_operadora']['pf_observacoes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                        break;
                    }
                    break;
                }
                ?>
            </div>

        </div>
        <!-- /OBSERVAÇÕES IMPORTANTES  -->

        <!-- DEPENDENTES  -->
        <div class="clearfix">&nbsp;</div>
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            DEPENDENTES
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php
            foreach ($tabelas as $produto) {
                foreach ($produto as $produto) {
                    echo ($produto['pf_produto']['pf_operadora']['pf_dependentes'] != null) ? nl2br($produto['pf_produto']['pf_operadora']['pf_dependentes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                    break;
                }
                break;
            }
            ?>
        </div>
        <!-- /DEPENDENTES  -->

        <!-- REDE  -->
        <?php if ($simulacao['rede'] == 1) { ?>


            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <?php
                foreach ($tabelas as $produto) {
                    foreach ($produto as $produto) {
                        //                                            debug($produto['pf_produto']['pf_operadora']);die();
                        if (isset($produto['pf_produto']['pf_operadora']['url_rede']) && $produto['pf_produto']['pf_operadora']['url_rede'] <> null) {
                            $linkRedeCompleta = "- Para rede completa " . $this->Html->link('clique aqui', $produto['pf_produto']['pf_operadora']['url_rede'], ['target' => "_blank"]) . ".";
                        } else {
                            $linkRedeCompleta = "";
                        }
                        break;
                    }
                    break;
                }
                ?>
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    REDE REFERENCIADA (Resumo) <span><?= $linkRedeCompleta ?></span>
                </div>

                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            if ($produto['pf_produto']['pf_operadora']['pf_redes'] != null) {
                                echo nl2br($produto['pf_produto']['pf_operadora']['pf_redes'][0]['descricao']);
                            } else {
                                echo "<div class='info'>Consulte Operadora</div>";
                            }
                            break;
                        }
                        break;
                    }
                    ?>
                </div>


            </div>

        <?php } ?>
        <!-- /REDE  -->
        <!-- OPCIONAIS  -->
        <?php
        if ($simulacao['reembolso'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">

                <div style="width: 100%;max-width: 100%;  background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                </div>


                <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 100%; max-width: 100%;">
                    <?php
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            echo ($produto['pf_produto']['pf_operadora']['pf_reembolsos'] != null) ? nl2br($produto['pf_produto']['pf_operadora']['pf_reembolsos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                            break;
                        }
                        break;
                    }
                    ?>
                </div>

            </div>


        <?php
        }
        ?>
        <!-- /OPCIONAIS  -->

        <!-- CARENCIAS -->
        <?php
        if ($simulacao['carencia'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                    CARÊNCIAS (Resumo)
                </div>
                <div class="fonteReduzida" style="padding: 5px;width: 100%; border: 0.1mm solid #ccc;max-width: 100% !important">
                    <?php
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            echo ($produto['pf_produto']['pf_operadora']['pf_carencias'] != null) ? nl2br($produto['pf_produto']['pf_operadora']['pf_carencias'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                            break;
                        }
                        break;
                    }
                    ?>
                </div>
            </div>
        <?php }
        ?>
        <!-- /CARENCIAS -->

        <!-- COMERCIALIZACAO -->
        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">

            <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; width: 100%;max-width: 100% !important">
                ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
            </div>


            <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 98.63%;max-width: 100% !important">
                <?php
                foreach ($tabelas as $produto) {
                    foreach ($produto as $produto) {
                        echo ($produto['pf_produto']['pf_operadora']['pf_comercializacoes'] != null) ? "<div style='width: 100%; max-width:100%;'><b>COMERCIALIZAÇÃO: </b>" . nl2br($produto['pf_produto']['pf_operadora']['pf_comercializacoes'][0]['descricao']) . "</div>" : "<div><b>COMERCIALIZAÇÃO: </b><span class='info'>Consulte Operadora</span></div>";
                        break;
                    }
                    break;
                }
                ?>
                <br>
                <div class="fonteReduzida" style="width: 100%; max-width: 100%;">
                    <b>ATENDIMENTO </b>
                </div>
                <div class="fonteReduzida" style="width: 100%; max-width: 100% !important;">
                    <?php
                    $redes = null;
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            $redes[$produto['pf_produto']['descricao']] = $produto['pf_produto']['pf_operadora']['pf_comercializaco']['descricao'] . " / " . $produto['pf_atendimento']['descricao'];
                        }
                    }

                    foreach ($redes as $chave => $valor) {
                        $arr = explode("/", $valor, 2);
                        echo "<span><b>" . nl2br($chave) . ":</b> " . $arr[1] . "</span><br>";
                    }
                    ?>
                </div>
            </div>

        </div>
        <!-- /COMERCIALIZACAO -->

        <!-- DOCUMENTOS -->
        <?php
        if ($simulacao['documento'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                    DOCUMENTOS NECESSÁRIOS
                </div>
                <div class="fonteReduzida" style="padding: 5px;width: 100%; border: 0.1mm solid #ccc;max-width: 100% !important">
                    <?php
                    $redes = null;
                    $teste = null;
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            $redes[$produto['pf_produto']['descricao']] = $produto['pf_produto']['pf_operadora']['pf_documentos'][0]['descricao'];
                        }
                    }
                    foreach ($redes as $chave => $valor) {
                        if ($valor != '') {
                            echo nl2br($valor);
                            $teste = 1;
                            break;
                        }
                    }
                    echo ($teste != 1) ? "Sob Consulta" : '';
                    ?>
                </div>
            </div>
        <?php } ?>
        <!-- /DOCUMENTOS -->
    </page>
<?php
}
// die();
?>
