<style type="text/css">
    .titulos {
        background-color: #ccc;
        text-align: center;
        font-weight: bold;
        padding: 5px;
        font-size: 90%;
    }

    .conteudo {
        padding: 5px;
        border: 0.1mm solid #ccc;
    }

    .introducao {
        font-size: 16px;
        text-align: justify;
        line-height: 16px;
        width: 100%;
        padding-right: 50px;
    }

    .jumbotron {
        background-color: #eee;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 10px;
        line-height: 1.5;
        width: 100%;
        /*width: 97%;*/
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }


    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
        font-size: 60%;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .totalTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
        font-weight: bold;
    }

    .fonteReduzida {
        font-size: 10px;
        text-align: justify !important;
        text-align: justify;
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        line-height: 1.3;
        margin-bottom: 0;
        cursor: default;
        display: block;
        font-style: normal;
        font-variant-caps: normal;
        vertical-align: baseline;
        white-space: normal;

    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .nobreak {
        page-break-inside: avoid;
    }

    .info {
        color: #a9a9a9;
        font-weight: lighter;
        font-style: italic;
        text-align: center;
        width: 100%;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .verde {
        color: #128c7e;
    }

    .pagina {
        padding: 20px;
        margin-top: 170px;
    }

    @media(max-width: 767px) {
        .pagina {
            margin-top: 160px;
        }
    }

    @media(min-width: 768px) and (max-width: 895px) {
        .pagina {
            margin-top: 250px;
        }
    }

    @media(min-width: 896px) and (max-width: 1218px) {
        .pagina {
            margin-top: 200px;
        }
    }
</style>

<page backtop="32mm" backbottom="7mm" backleft="5mm" backright="5mm" style="font-size: 11px">
    <div class="container">
        <?php foreach ($tabelas_ordenadas as $chave => $operadora) : ?>
            <page_header style="margin-right: 20px;margin-left: 20px; margin-top: 20px; width: 100%; margin-bottom: 300px !important;">

                <?php echo $this->element('cabecalho_pdf'); ?>

            </page_header>
            </br>
            <table>
                <tr>
                    <td class="introducao">
                        À <b><?php echo $simulacao['nome'] ?></b>, A/C: <b><?php echo $simulacao['contato'] ?></b>.<br />
                        Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.
                        Informamos que os valores, regras de comercialização e condições contratuais são determinadas pelas seguradoras/operadoras dos planos
                        e podem ser alterados pelas mesmas a qualquer momento.
                    </td>
                </tr>
            </table>

            <?php
            $faixas = null;
            $i = 1;
            for ($i = 1; $i <= 10; $i++) {
                if ($simulacao['faixa' . $i] > 0) {
                    $faixas = $faixas + 1;
                }
            }
            $largura = 100 / ($faixas + 1);
            $tamanhoFonte = 'font-size:95%';
            ?>
            <br />
            <br />
            <table style="width: 100%" cellspacing="0">
                <tr>
                    <td style="width: 15%; text-align: left; font-weight:bold;">
                        <img src="<?= WWW_ROOT . 'uploads/imagens/' . $operadora['dados']['imagen']['nome'] ?>" style="width: 200px;">
                    </td>
                    <td style="text-align: left; font-weight:bold;">
                        <?= $operadora['dados']['nome'] . '<br/><small style="font-weight: 100">' . $operadora['dados']['detalhe'] . "</small>" ?>
                    </td>
                </tr>
            </table>

            <br />
            <?php $corfundo = '#eee'; ?>
            <table style="width: 100%; border:  solid 0.1mm #ddd;<?php echo $tamanhoFonte ?>" cellspacing="0">
                <tr style="width: 100% !important; ">
                    <?php
                    $i = 1;
                    for ($i = 1; $i <= 10; $i++) {
                        if ($simulacao['faixa' . $i] > 0) {
                    ?>
                            <td style="border: solid 0.1mm #dddddd;text-align:center; width: <?php echo $largura ?>%;<?php echo $tamanhoFonte ?>">
                                <?php
                                switch ($i) {
                                    case 1:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                        break;
                                    case 2:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                        break;
                                    case 3:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                        break;
                                    case 4:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                        break;
                                    case 5:
                                        echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                        break;
                                    case 6:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                        break;
                                    case 7:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                        break;
                                    case 8:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                        break;
                                    case 9:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                        break;
                                    case 10:
                                        echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 ou +";
                                        break;
                                }
                                ?>

                            </td>

                    <?php
                        }
                    }
                    ?>
                    <td class="negrito" style=" border:solid 0.1mm #dddddd;width: <?php echo $largura ?>%;text-align:center;<?php echo $tamanhoFonte ?>">
                        <?php
                        $x = 1;
                        $vidas = 0;
                        for ($x = 1; $x <= 10; $x++) {
                            $vidas = $vidas + $simulacao['faixa' . $x];
                        }
                        echo "Total:<br>" . $vidas . " Vida(s)";
                        ?>
                    </td>
                </tr>
                <?php foreach ($operadora['tabelas'] as $produto) { ?>
                    <tr>
                        <td></td>
                    </tr>
                    <tr style="background-color: #ccc; text-align: center;">
                        <td class="centralizada" style="padding: 5px 0 5px 3px; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a" colspan="<?php echo $faixas + 1 ?>">
                            <?php
                            if ($produto['coparticipacao'] == 's') {
                                $co = "C/COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'];
                            } else {
                                $co = "S/COPARTICIPAÇÃO ";
                            }

                            if ($produto['tipo_contratacao'] == '0') {
                                $tipo_contratacao = "OPCIONAL";
                            } else {
                                $tipo_contratacao = "COMPULSÓRIO";
                            }
                            ?>
                            <?= "<b>" . $produto['descricao'] . " - " . $co . " - " . $produto["pf_cobertura"]["nome"] . "</b> <br> " .
                                $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas - " . $produto['pf_acomodaco']['nome'] ?>

                        </td>
                    </tr>
                    <tr style="background-color: #fff;border-left: solid 0.1mm #5a5a5a;  ">
                        <?php
                        for ($i = 1; $i <= 10; $i++) {

                            if ($simulacao['faixa' . $i] > 0) {
                                if ($i == 1) {
                                    $bordaEsquerda = "border-left: solid 0.1mm #5a5a5a;";
                                } else {
                                    $bordaEsquerda = "border-left:solid 0.1mm #dddddd;";
                                }
                        ?>
                                <td style="width: <?php echo $largura ?>%;<?php echo $bordaEsquerda ?>; text-align:center; border-bottom: solid 0.1mm #5a5a5a; padding: 10px 0; vertical-align: middle">
                                    <?php if ($produto['faixa' . $i] == 0) : ?>
                                        ***
                                    <?php else : ?>
                                        <?php echo $this->Number->currency($simulacao['faixa' . $i] * $produto['faixa' . $i]) ?>
                                        <br />

                                        <small style="font-size:75%"> <?php echo number_format($produto['faixa' . $i], 2, ",", ".") ?> p/ vida</small>
                                    <?php endif ?>
                                </td>
                        <?php
                            }
                        }
                        ?>
                        <td style="font-weight: bold;width: <?php echo $largura ?>%; text-align: center;border-left:solid 0.1mm #dddddd;border-bottom: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a;<?php echo $tamanhoFonte ?>">
                            <?php
                            $totalPorProduto = 0;
                            for ($i = 1; $i <= 10; $i++) {
                                $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * $produto['faixa' . $i]);
                            }
                            ?>
                            <b><?php echo $this->Number->currency($totalPorProduto) ?></b>
                        </td>

                    </tr>
                <?php } ?>
            </table>


            <!-- ENTIDADES DE CLASSE -->
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <?php
                foreach ($operadora['tabelas'] as $tabela) {
                    if ($tabela['modalidade'] <> 'INDIVIDUAL') {
                ?>
                        <div class="titulos">
                            ENTIDADES E PROFISSÕES
                        </div>
                        <div class="fonteReduzida conteudo">
                            <?php
                            if (!empty($tabela['pf_entidades'])) {
                                foreach ($tabela['pf_entidades'] as $entidade) {
                                    echo $entidade['nome'] . ': ';
                                    $indice = 1;
                                    foreach ($entidade['pf_profissoes'] as $profissao) {
                                        if ($indice === count($entidade['pf_profissoes'])) {
                                            echo $profissao['nome'] . ". ";
                                        } else {
                                            echo $profissao['nome'] . ", ";
                                        }

                                        $indice++;
                                    }
                                }
                            }
                            ?>

                        </div>
                <?php
                    }
                }
                ?>
            </div>
            <!-- /ENTIDADES DE CLASSE -->


            <!-- FORMAS DE PAGAMENTOS -->
            <div class="clearfix">&nbsp;</div>
            <div class="titulos">
                FORMAS DE PAGAMENTO
            </div>
            <div class="conteudo">
                <?= ($operadora['dados']['pf_formas_pagamentos'] != null) ? nl2br($operadora['dados']['pf_formas_pagamentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>"; ?>
            </div>
            <!-- /FORMAS DE PAGAMENTOS -->


            <!-- OBSERVAÇÕES IMPORTANTES -->
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div class="titulos">
                    OBSERVAÇÕES IMPORTANTES
                </div>
                <div class="fonteReduzida conteudo">
                    <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
                    <?= ($operadora['dados']['pf_observacoes'] != null) ? nl2br($operadora['dados']['pf_observacoes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                </div>

            </div>
            <!-- OBSERVAÇÕES IMPORTANTES  -->


            <!-- DEPENDENTES  -->
            <div class="clearfix">&nbsp;</div>
            <div class="titulos">
                DEPENDENTES
            </div>
            <div class="fonteReduzida conteudo">
                <?= ($operadora['dados']['pf_dependentes'] != null) ? nl2br($operadora['dados']['pf_dependentes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
            </div>
            <!-- /DEPENDENTES  -->


            <!-- REDE  -->
            <?php if ($simulacao['rede'] == 1) : ?>
                <div class="clearfix">&nbsp;</div>
                <div class="nobreak">
                    <div class="titulos">
                        REDE REFERENCIADA (Resumo) <span><?= (isset($operadora['dados']['url_rede']) && !is_null($operadora['dados']['url_rede'])) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['dados']['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
                    </div>
                    <div class="fonteReduzida conteudo">
                        <?= !is_null($operadora['dados']['pf_redes']) ? nl2br($operadora['dados']['pf_redes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                    </div>
                </div>
            <?php endif; ?>
            <!-- /REDE  -->


            <!-- OPCIONAIS  -->
            <?php if ($simulacao['reembolso'] == 1) : ?>
                <div class="clearfix">&nbsp;</div>
                <div class="nobreak">
                    <div class="titulos">
                        OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                    </div>
                    <div class="fonteReduzida conteudo">
                        <?= !is_null($operadora['dados']['pf_reembolsos']) ? nl2br($operadora['dados']['pf_reembolsos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                    </div>
                </div>
            <?php endif; ?>
            <!-- /OPCIONAIS  -->


            <!-- CARENCIAS -->
            <?php if ($simulacao['carencia'] == 1) { ?>
                <div class="clearfix">&nbsp;</div>
                <div class="nobreak">
                    <div class="titulos">
                        CARÊNCIAS (Resumo)
                    </div>
                    <div class="fonteReduzida conteudo">
                        <?= !is_null($operadora['dados']['pf_carencias']) ? nl2br($operadora['dados']['pf_carencias'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                    </div>
                </div>
            <?php } ?>
            <!-- /CARENCIAS -->


            <!-- COMERCIALIZACAO -->
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div class="titulos">
                    ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
                </div>
                <div class="fonteReduzida conteudo">
                    <?php
                    $areas = array();
                    foreach ($operadora['tabelas'] as $tabela) {
                        foreach ($tabela['pf_areas_comercializaco']['municipios'] as $municipio) {
                            $areas[$tabela['id']]['nome'] = $tabela['nome'] . ' - ' . $tabela['descricao'];
                            $areas[$tabela['id']]['atendimento'] = $tabela['pf_atendimento']['nome'];
                            $areas[$tabela['id']]['estados'][$municipio['estado']['id']]['nome'] =  $municipio['estado']['nome'];
                            $areas[$tabela['id']]['estados'][$municipio['estado']['id']]['municipios'][] =  $municipio['nome'];
                        }
                    }
                    ?>
                    <div class="fonteReduzida" style="margin-left: 10px;">
                        <?php foreach ($areas as $area) : ?>
                            <strong><?= $area['nome'] ?></strong>
                            <br>
                            <div style="margin-left: 10px; border-bottom: 1px solid #cccccc;">
                                <b>COMERCIALIZAÇÃO </b>
                                </br>
                                <?php foreach ($area['estados'] as $estado) : ?>
                                    <strong><?= $estado['nome'] ?>:</strong>
                                    <?php foreach ($estado['municipios'] as $municipio) : ?>
                                        <?= $municipio ?>,
                                    <?php endforeach; ?>
                                    </br>
                                <?php endforeach; ?>
                                <br>
                                <b>ATENDIMENTO:</b> <?= $area['atendimento'] ?>
                                </br>
                            </div>
                            </br>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
            <!-- /COMERCIALIZACAO -->


            <!-- DOCUMENTOS -->
            <?php if ($simulacao['documento'] == 1) { ?>
                <div class="nobreak">
                    <div class="titulos">
                        DOCUMENTOS NECESSÁRIOS
                    </div>
                    <div class="fonteReduzida conteudo">
                        <?php
                        $redes = null;
                        $teste = null;
                        foreach ($operadora as $produto) {
                            foreach ($produto as $produto) {
                                $redes[$operadora['dados']['descricao']] = $operadora['dados']['pf_documentos'][0]['descricao'];
                            }
                        }
                        foreach ($redes as $chave => $valor) {
                            if ($valor != '') {
                                echo nl2br($valor);
                                $teste = 1;
                                break;
                            }
                        }
                        echo ($teste != 1) ? "Sob Consulta" : '';
                        ?>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            <?php } ?>
            <!-- /DOCUMENTOS -->

        <?php endforeach; ?>

        <div id="footer" style="font-size: 15px;">
            <div class="rodape-home col-xs-12" style="text-align: center;">
                <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
                    <span style="text-decoration: none !important; ; font-size: 16px;"> corretorparceiro.com.br</span>
                </a>
                <br>
                <span style="color: #004057; font-size: 16px;">Assessoria de Apoio a Corretores</span>
            </div>
        </div>
    </div>
</page>