<?= $this->element('estilo_cotacao_pdf') ?>
<page backtop="7mm" backbottom="7mm" backleft="5mm" backright="5mm" style="font-size: 11px;">

    <page_footer>
        <div style="font-size: 15px; margin-top: 20px;">
            <div class="rodape-home col-xs-12" style="text-align: center;">
                <span style="color: #004057">Assessoria de Apoio para Corretores.</span><br>
                <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
                    <?= '<img src="https://corretorparceiro.com.br/app/img/ciranda.png" style="height: 15px; margin-left: 5px;" />' ?>
                    <span style="text-decoration: none !important"> corretorparceiro.com.br</span>
                </a>
            </div>
        </div>
    </page_footer>

    <div style="width: 100%; margin-bottom: 300px !important;">
        <?php
        echo $this->element('cabecalho_cotacao'); ?>
    </div>
    <?php foreach ($tabelas_ordenadas as $chave => $operadora) :
        $faixas = (new Cake\Collection\Collection($simulacao->toArray()))->filter(function ($value, $key) {
            return str_starts_with($key, 'faixa')  && !str_starts_with($key, 'faixa_extra') && $value > 0;
        })->count();
    ?>
        <div class="spacer-md">&nbsp;</div>

        <table style="margin-top: 40px;">
            <tr>
                <td style="text-align: justify; line-height: 16px;">
                    À <b><?php echo $simulacao['nome'] ?></b>, A/C: <b><?php echo $simulacao['contato'] ?></b>.<br />
                    Agradecemos pela oportunidade. Segue abaixo os cálculos dos planos para análise.<br />
                    Informamos que os valores, regras de comercialização e condições contratuais são determinadas pelas seguradoras/operadoras dos planos<br />
                    e podem ser alterados pelas mesmas a qualquer momento.
                </td>
            </tr>
        </table>


        <br />
        <br />


        <table style="width: 100%" cellspacing="0">
            <tr>
                <td style="width: 15%; text-align: left; font-weight:bold;">
                    <?= $this->Html->image('/webroot/' . $operadora['operadora']['imagen']['caminho'] . $operadora['operadora']['imagen']['nome'], ['fullBase' => true, 'height' => 40]) ?>
                </td>
                <td style="text-align: left; font-weight:bold;">
                </td>
            </tr>
        </table>

        <br />
        <br />

        <table style="width: 100%; border:  solid 0.1mm #ddd;" cellspacing="0">
            <tr style="width: 100% !important;">
                <?php
                for ($i = 1; $i <= 10; $i++) {
                    if ($simulacao['faixa' . $i] > 0) {
                ?>
                        <td style="border: solid 0.1mm #dddddd;text-align:center; width: <?= 100 / ($faixas + 1)  ?>%">
                            <?php
                            switch ($i) {
                                case 1:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                    break;
                                case 2:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                    break;
                                case 3:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                    break;
                                case 4:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                    break;
                                case 5:
                                    echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                    break;
                                case 6:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                    break;
                                case 7:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                    break;
                                case 8:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                    break;
                                case 9:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                    break;
                                case 10:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 ou +";
                                    break;
                            }
                            ?>

                        </td>

                <?php
                    }
                }
                ?>
                <td style=" border:solid 0.1mm #dddddd;text-align:center; width: <?= 100 / ($faixas + 1) ?>%">
                    <?php
                    $vidas = 0;
                    for ($x = 1; $x <= 10; $x++) {
                        $vidas = $vidas + $simulacao['faixa' . $x];
                    }
                    echo "Total:<br>" . $vidas . " Vida(s)";
                    ?>
                </td>
            </tr>

            <?php foreach ($operadora['tabelas'] as $produto) { ?>
                <tr>
                    <td></td>
                </tr>
                <tr style="background-color: #ccc; text-align: center;">
                    <td class="centralizada" style="font-size: 10px; padding: 5px 0 5px 3px; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a" colspan="<?= ($faixas + 1) ?>">
                        <?php
                        if ($produto['coparticipacao'] == 's') {
                            $co = "C/COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'];
                        } else {
                            $co = "S/COPARTICIPAÇÃO ";
                        }

                        if ($produto['tipo_contratacao'] == '0') {
                            $tipo_contratacao = "OPCIONAL";
                        } else {
                            $tipo_contratacao = "COMPULSÓRIO";
                        }
                        ?>
                        <?= "<b>" . $produto['nome'] . " - " . $produto['area_comercializacao'] . " - " . $produto["pf_cobertura"]["nome"] . "</b> <br> " .
                            $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas. " . $produto['pf_acomodaco']['nome'] . ' - ' . $co  ?>

                    </td>
                </tr>
                <tr style="background-color: #fff;border-left: solid 0.1mm #5a5a5a;  ">
                    <?php
                    for ($i = 1; $i <= 10; $i++) {

                        if ($simulacao['faixa' . $i] > 0) {
                            if ($i == 1) {
                                $bordaEsquerda = "border-left: solid 0.1mm #5a5a5a;";
                            } else {
                                $bordaEsquerda = "border-left:solid 0.1mm #dddddd;";
                            }
                    ?>
                            <td style="<?php echo $bordaEsquerda ?>; text-align:center; border-bottom: solid 0.1mm #5a5a5a; padding: 10px 0; vertical-align: middle">
                                <?php if ($produto['faixa' . $i] == 0) : ?>
                                    ***
                                <?php elseif ($produto['atualizacao'] == 1) : ?>
                                    <strong>Tabela em Atualização de Preço</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($simulacao['faixa' . $i] * ($produto['faixa' . $i] + $produto['odonto_valor']), ["before" => "R$ ", "places" => 2]) ?>
                                    <br />

                                    <small style="font-size:75%"> <?= $this->Number->format(($produto['faixa' . $i] + $produto['odonto_valor']), ["before" => "R$ ", "places" => 2]) ?> p/ vida</small>
                                <?php endif ?>
                            </td>
                    <?php
                        }
                    }
                    ?>
                    <td style="font-weight: bold; text-align: center;border-left:solid 0.1mm #dddddd;border-bottom: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a;">
                        <?php if ($produto['atualizacao'] == 1) : ?>
                            <strong>Tabela em Atualização de Preço</strong>
                        <?php else :
                            $totalPorProduto = 0;
                            for ($i = 1; $i <= 10; $i++) {
                                $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * ($produto['faixa' . $i] + $produto['odonto_valor']));
                            }
                        ?>
                            <b><?= $this->Number->format($totalPorProduto, ["before" => "R$ ", "places" => 2]) ?></b>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <div class="clearfix">&nbsp;</div>

        <div class="nobreak">
            <!-- ENTIDADES E PROFISSÕES -->
            <?php if (!empty($operadora['grupo_entidades'])) : ?>
                <div class="nobreak">
                    <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                        ENTIDADES E PROFISSÕES
                    </div>
                    <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                        <?php foreach ($operadora['grupo_entidades'] as $key => $value) :
                            if (!empty($key)) : ?>
                                <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                </br></br>
                                <?= $value['data'] ?>
                                <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                            <?php else : ?>
                                <div class='info'>Consulte Operadora</div>
                        <?php endif;
                        endforeach; ?>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            <?php endif; ?>
        </div>

        <div class="nobreak">
            <!-- FORMAS DE PAGAMENTOS -->
            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                FORMAS DE PAGAMENTO
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php foreach ($operadora['pagamentos'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        <br><br>
                        <?= $value['data'] ?>
                        <?= count($operadora['pagamentos']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <!-- /FORMAS DE PAGAMENTOS -->


        <!-- OBSERVAÇÕES IMPORTANTES -->
        <div class="nobreak">
            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                OBSERVAÇÕES IMPORTANTES
            </div>
            <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong>
                <br><br>
                <?php foreach ($operadora['observacoes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        <br><br>
                        <?= $value['data'] ?>
                        <?= count($operadora['observacoes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>

        </div>
        <div class="clearfix">&nbsp;</div>
        <!-- OBSERVAÇÕES IMPORTANTES  -->

        <div class="nobreak">
            <!-- DEPENDENTES  -->
            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                DEPENDENTES
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php foreach ($operadora['dependentes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        <br><br>
                        <?= $value['data'] ?>
                        <?= count($operadora['dependentes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <!-- /DEPENDENTES  -->

        <div class="nobreak">
            <!-- REDE  -->
            <?php if ($simulacao['rede'] == 1) : ?>
                <div class="nobreak">
                    <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                        REDE REFERENCIADA (Resumo) <span><?= (isset($operadora['url_rede']) && !is_null($operadora['url_rede'])) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
                    </div>
                    <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                        <?php foreach ($operadora['redes'] as $key => $value) :
                            if (!empty($key)) : ?>
                                <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                <br><br>
                                <?= $value['data'] ?>
                                <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                            <?php else : ?>
                                <div class='info'>Consulte Operadora</div>
                        <?php endif;
                        endforeach; ?>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            <?php endif; ?>
        </div>
        <!-- /REDE  -->

        <div class="nobreak">
            <!-- OPCIONAIS  -->
            <?php if ($simulacao['reembolso'] == 1) : ?>
                <div class="nobreak">
                    <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                        OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                    </div>
                    <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                        <?php foreach ($operadora['redes'] as $key => $value) :
                            if (!empty($key)) : ?>
                                <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                <br><br>
                                <?= $value['data'] ?>
                                <?= count($operadora['redes']) > 1 ? '<hr>' : '' ?>
                            <?php else : ?>
                                <div class='info'>Consulte Operadora</div>
                        <?php endif;
                        endforeach; ?>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            <?php endif; ?>
        </div>
        <!-- /OPCIONAIS  -->

        <div class="nobreak">
            <!-- CARENCIAS -->
            <?php
            if ($simulacao['carencia'] == 1) {
            ?>
                <div class="nobreak">
                    <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                        CARÊNCIAS (Resumo)
                    </div>
                    <div class="fonteReduzida" style="padding: 5px;width: 100%; border: 0.1mm solid #ccc;max-width: 100% !important">
                        <?php foreach ($operadora['carencias'] as $key => $value) :
                            if (!empty($key)) : ?>
                                <strong>Produtos: </strong><?= $value['tabelas'] ?>
                                <br><br>
                                <?= $value['data'] ?>
                                <?= count($operadora['carencias']) > 1 ? '<hr>' : '' ?>
                            <?php else : ?>
                                <div class='info'>Consulte Operadora</div>
                        <?php endif;
                        endforeach; ?>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            <?php } ?>
        </div>
        <!-- /CARENCIAS -->


        <!-- COMERCIALIZACAO -->
        <div class="nobreak">
            <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; width: 100%;max-width: 100% !important">
                ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
            </div>
            <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 100%;max-width: 100% !important">
                <?php foreach ($operadora['areas_comercializacoes'] as $key => $value) :
                    if (!empty($key)) : ?>
                        <strong>Produtos: </strong><?= $value['tabelas'] ?>
                        <br><br>
                        <?= $value['data'] ?>
                        <?= count($operadora['areas_comercializacoes']) > 1 ? '<hr>' : '' ?>
                    <?php else : ?>
                        <div class='info'>Consulte Operadora</div>
                <?php endif;
                endforeach; ?>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>
        <!-- /COMERCIALIZACAO -->


        <!-- DOCUMENTOS -->
        <?php if ($simulacao['documento'] == 1) { ?>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                    DOCUMENTOS NECESSÁRIOS
                </div>
                <div class="fonteReduzida" style="padding: 5px;width: 100%; border: 0.1mm solid #ccc;max-width: 100% !important">
                    <?php foreach ($operadora['documentos'] as $key => $value) :
                        if (!empty($key)) : ?>
                            <strong>Produtos: </strong><?= $value['tabelas'] ?>
                            <br><br>
                            <?= $value['data'] ?>
                            <?= count($operadora['documentos']) > 1 ? '<hr>' : '' ?>
                        <?php else : ?>
                            <div class='info'>Consulte Operadora</div>
                    <?php endif;
                    endforeach; ?>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
        <?php } ?>
        <!-- /DOCUMENTOS -->

    <?php endforeach; ?>
</page>