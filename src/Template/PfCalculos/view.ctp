<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Cálculos']); ?>
<div class="card-body">
    <div class="row">
        <div class="col-sm-12">
            <style>
                .alert-secondary {
                    background-color: #f5f5f5;
                }
            </style>
            <?php
            $session = $this->request->session();
            $sessao = $session->read('Auth.User');
            $error = $session->read('erro');
            $session->delete('erro');
            ?>
            <div id="resposta">

                <?= $this->Form->create('operadoras', ['id' => 'operadoras', 'url' => ['controller' => 'pf-calculos', 'action' => 'salvaFiltros']]); ?>
                <?= $this->element('filtro_simulacao_pf'); ?>
                <div id="resultado">
                    <?= $this->element('body_pf_calculos'); ?>
                </div>
                <?= $this->Form->end(); ?>
            </div>


            <?= $this->element('avisos'); ?>
            <div id="aviso" class="col-xs-12 centralizada" style="display: none;">
                <div class="text-danger">Selecione os cálculos que deseja exibir na cotação</div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {

                    var atendimento = '<?= $filtro["pf_atendimento_id"] ?>';
                    var acomodacao = '<?= $filtro["pf_acomodacao_id"] ?>';
                    var tipo_produto = '<?= $filtro["tipo_produto_id"] ?>';
                    var coparticipacao = '<?= $filtro["coparticipacao"] ?>';
                    var filtroreembolso = '<?= $filtro["filtroreembolso"] ?>';

                    if (atendimento || acomodacao || tipo_produto || coparticipacao || filtroreembolso) {
                        $("#pf-atendimento-id").val(atendimento);
                        $("#pf-acomodacao-id").val(acomodacao);
                        $("#tipo-produto-id").val(tipo_produto);
                        $("#coparticipacao").val(coparticipacao);
                        $("#filtroreembolso").val(filtroreembolso);
                        $("#filtroreembolso").change();


                    } else {
                        if ('<?= $error ?>') {
                            $("#aviso").appendTo("#conteudoAviso");
                            $("#modalAviso").modal("show");
                            $("#aviso").show();
                        }
                    }

                });
            </script>
        </div>
    </div>
</div>
<?php echo $this->element('forms/buttonsFechar') ?>