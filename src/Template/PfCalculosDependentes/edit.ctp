<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfCalculosDependente->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfCalculosDependente->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos Dependentes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfCalculosDependentes form large-9 medium-8 columns content">
    <?= $this->Form->create($pfCalculosDependente) ?>
    <fieldset>
        <legend><?= __('Edit Pf Calculos Dependente') ?></legend>
        <?php
            echo $this->Form->input('pf_calculo_id', ['options' => $pfCalculos]);
            echo $this->Form->input('idade_dependente');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
