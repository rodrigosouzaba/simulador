<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pf Calculos Dependente'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfCalculosDependentes index large-9 medium-8 columns content">
    <h3><?= __('Pf Calculos Dependentes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_calculo_id') ?></th>
                <th><?= $this->Paginator->sort('idade_dependente') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfCalculosDependentes as $pfCalculosDependente): ?>
            <tr>
                <td><?= $this->Number->format($pfCalculosDependente->id) ?></td>
                <td><?= $pfCalculosDependente->has('pf_calculo') ? $this->Html->link($pfCalculosDependente->pf_calculo->id, ['controller' => 'PfCalculos', 'action' => 'view', $pfCalculosDependente->pf_calculo->id]) : '' ?></td>
                <td><?= $this->Number->format($pfCalculosDependente->idade_dependente) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pfCalculosDependente->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfCalculosDependente->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfCalculosDependente->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfCalculosDependente->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
