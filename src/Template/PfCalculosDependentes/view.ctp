<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Calculos Dependente'), ['action' => 'edit', $pfCalculosDependente->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Calculos Dependente'), ['action' => 'delete', $pfCalculosDependente->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfCalculosDependente->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Calculos Dependentes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Calculos Dependente'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfCalculosDependentes view large-9 medium-8 columns content">
    <h3><?= h($pfCalculosDependente->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Calculo') ?></th>
            <td><?= $pfCalculosDependente->has('pf_calculo') ? $this->Html->link($pfCalculosDependente->pf_calculo->id, ['controller' => 'PfCalculos', 'action' => 'view', $pfCalculosDependente->pf_calculo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfCalculosDependente->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Idade Dependente') ?></th>
            <td><?= $this->Number->format($pfCalculosDependente->idade_dependente) ?></td>
        </tr>
    </table>
</div>
