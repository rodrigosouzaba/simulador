<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos Tabelas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfCalculosTabelas form large-9 medium-8 columns content">
    <?= $this->Form->create($pfCalculosTabela) ?>
    <fieldset>
        <legend><?= __('Add Pf Calculos Tabela') ?></legend>
        <?php
            echo $this->Form->input('pf_calculo_id', ['options' => $pfCalculos]);
            echo $this->Form->input('pf_tabela_id', ['options' => $pfTabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
