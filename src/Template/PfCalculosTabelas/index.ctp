<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pf Calculos Tabela'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfCalculosTabelas index large-9 medium-8 columns content">
    <h3><?= __('Pf Calculos Tabelas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_calculo_id') ?></th>
                <th><?= $this->Paginator->sort('pf_tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfCalculosTabelas as $pfCalculosTabela): ?>
            <tr>
                <td><?= $this->Number->format($pfCalculosTabela->id) ?></td>
                <td><?= $pfCalculosTabela->has('pf_calculo') ? $this->Html->link($pfCalculosTabela->pf_calculo->id, ['controller' => 'PfCalculos', 'action' => 'view', $pfCalculosTabela->pf_calculo->id]) : '' ?></td>
                <td><?= $pfCalculosTabela->has('pf_tabela') ? $this->Html->link($pfCalculosTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $pfCalculosTabela->pf_tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pfCalculosTabela->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfCalculosTabela->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfCalculosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfCalculosTabela->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
