<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Calculos Tabela'), ['action' => 'edit', $pfCalculosTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Calculos Tabela'), ['action' => 'delete', $pfCalculosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfCalculosTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Calculos Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Calculos Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfCalculosTabelas view large-9 medium-8 columns content">
    <h3><?= h($pfCalculosTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Calculo') ?></th>
            <td><?= $pfCalculosTabela->has('pf_calculo') ? $this->Html->link($pfCalculosTabela->pf_calculo->id, ['controller' => 'PfCalculos', 'action' => 'view', $pfCalculosTabela->pf_calculo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Tabela') ?></th>
            <td><?= $pfCalculosTabela->has('pf_tabela') ? $this->Html->link($pfCalculosTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $pfCalculosTabela->pf_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfCalculosTabela->id) ?></td>
        </tr>
    </table>
</div>
