<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($carencias as $carencia) : ?>
            <tr>
                <td><?= $carencia->nome ?></td>
                <td><?= $carencia->descricao ?></td>
                <td>
                    <?php
                    //                        debug($produto);die();
                    if (isset($carencia->pf_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $carencia->pf_operadora->imagen->caminho . "/" . $carencia->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $carencia->pf_operadora->detalhe;
                    } else {
                        echo $carencia->pf_operadora->nome . " - " . $carencia->pf_operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/pfCarencias/edit/$carencia->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'pfCarencias.edit']) ?>
                    <?= $this->Form->postLink('',  "/pfCarencias/delete/$carencia->id", ['confirm' => __('Confirma exclusão da Carência?', $carencia->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'pfCarencias.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
