<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Carencia'), ['action' => 'edit', $pfCarencia->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Carencia'), ['action' => 'delete', $pfCarencia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfCarencia->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Carencias'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Carencia'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Produtos'), ['controller' => 'PfProdutos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Produto'), ['controller' => 'PfProdutos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfCarencias view large-9 medium-8 columns content">
    <h3><?= h($pfCarencia->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($pfCarencia->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Operadora') ?></th>
            <td><?= $pfCarencia->has('pf_operadora') ? $this->Html->link($pfCarencia->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfCarencia->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfCarencia->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($pfCarencia->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Produtos') ?></h4>
        <?php if (!empty($pfCarencia->pf_produtos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th><?= __('Pf Carencia Id') ?></th>
                <th><?= __('Pf Rede Id') ?></th>
                <th><?= __('Pf Reembolso Id') ?></th>
                <th><?= __('Pf Documento Id') ?></th>
                <th><?= __('Pf Dependente Id') ?></th>
                <th><?= __('Pf Observacao Id') ?></th>
                <th><?= __('Tipo Produto Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfCarencia->pf_produtos as $pfProdutos): ?>
            <tr>
                <td><?= h($pfProdutos->id) ?></td>
                <td><?= h($pfProdutos->nome) ?></td>
                <td><?= h($pfProdutos->descricao) ?></td>
                <td><?= h($pfProdutos->pf_operadora_id) ?></td>
                <td><?= h($pfProdutos->pf_carencia_id) ?></td>
                <td><?= h($pfProdutos->pf_rede_id) ?></td>
                <td><?= h($pfProdutos->pf_reembolso_id) ?></td>
                <td><?= h($pfProdutos->pf_documento_id) ?></td>
                <td><?= h($pfProdutos->pf_dependente_id) ?></td>
                <td><?= h($pfProdutos->pf_observacao_id) ?></td>
                <td><?= h($pfProdutos->tipo_produto_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfProdutos', 'action' => 'view', $pfProdutos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfProdutos', 'action' => 'edit', $pfProdutos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfProdutos', 'action' => 'delete', $pfProdutos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfProdutos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
