<div class="pfCoberturas form large-9 medium-8 columns content">
    <?= $this->Form->create($pfCobertura) ?>
    <fieldset>
        <legend><?= __('Edit Pf Cobertura') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('prioridade');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
