<div class="col-md-3 fonteReduzida">
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Cobertura', '/pfCoberturas/add', ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'pfCoberturas.add']); ?>
</div>
<div class="col-xs-12">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('prioridade') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfCoberturas as $pfCobertura) : ?>
                <tr>
                    <td><?= $this->Number->format($pfCobertura->id) ?></td>
                    <td><?= h($pfCobertura->nome) ?></td>
                    <td><?= $this->Number->format($pfCobertura->prioridade) ?></td>
                    <td class="actions">
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm dropdown-toggle btn-success " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </button>
                            <ul class="dropdown-menu" style="right: 0; left: auto;">
                                <li><?= $this->Html->link(__('Edit'), '/pfCoberturas/edit/'. $pfCobertura->id, ['sid' => 'pfCoberturas.edit']) ?></li>
                                <li><?= $this->Form->postLink(__('Delete'), '/pfCoberturas/delete/'. $pfCobertura->id, ['sid' => 'pfCoberturas.delete', 'confirm' => __('Are you sure you want to delete # {0}?', $pfCobertura->id)]) ?></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
