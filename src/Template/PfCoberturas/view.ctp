<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Cobertura'), ['action' => 'edit', $pfCobertura->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Cobertura'), ['action' => 'delete', $pfCobertura->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfCobertura->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Coberturas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Cobertura'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfCoberturas view large-9 medium-8 columns content">
    <h3><?= h($pfCobertura->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($pfCobertura->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfCobertura->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prioridade') ?></th>
            <td><?= $this->Number->format($pfCobertura->prioridade) ?></td>
        </tr>
    </table>
</div>
