<?= $this->Form->create($pfComercializaco) ?>
<div class="col-md-12 fonteReduzida">
    <?= $this->Form->input('nome', ['required' => 'required']) ?>
</div>
<div class="col-md-6 fonteReduzida">
    <?= $this->Form->input('estado_id', ['empty' => 'SELECIONE', 'required' => 'required']) ?>
</div>
<div class="col-md-6 fonteReduzida" id="operadoras">
    <?php if ($this->request->action == 'edit') : ?>
        <?= $this->Form->input('pf_operadora_id', ['empty' => 'SELECIONE', 'label' => 'Operadora', 'disabled' => 'false', 'required' => 'required']) ?>
    <?php else : ?>
        <?= $this->Form->input('pf_operadora_id', ['empty' => 'SELECIONE', 'label' => 'Operadora', 'disabled' => 'true', 'required' => 'required']) ?>
    <?php endif; ?>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-offset-3 col-md-6" id="local_municipios">
    <?= $this->Form->input('municipios._ids', ['options' => $municipios, 'multiple' => 'multiple', 'label' => 'Municípios da Área de Comercialização', 'style' => 'margin-bottom: 0 !important', 'required' => 'required']); ?>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-12 fonteReduzida">
    <?= $this->Form->input('descricao', ['label' => 'Observação']) ?>
</div>
<?= $this->element('botoesAdd') ?>
<?= $this->Form->end() ?>
</div>
<script>
    $("#estado-id").change(function() {
        estado = $(this).val();

        //GET OPERADORAS
        $.ajax({
            type: 'POST',
            url: "<?= $this->request->webroot . $this->request->controller ?>/filtroOperadoras/" + estado,
            success: function(data) {
                $("#operadoras").empty();
                $("#operadoras").append(data);
            }
        })

        //GET MUNICIPIOS
        $.ajax({
            type: "GET",
            url: "<?= $this->request->webroot . $this->request->controller ?>/getMunicipios/" + estado,
            success: function(data) {
                $("#local_municipios").empty();
                $("#local_municipios").append(data);
                $("#local_municipios").show();

            }
        });
    });
</script>
