<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfComercializaco->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfComercializaco->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfComercializacoes form large-9 medium-8 columns content">
    <?= $this->Form->create($pfComercializaco) ?>
    <fieldset>
        <legend><?= __('Edit Pf Comercializaco') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('estado_id', ['options' => $estados]);
            echo $this->Form->input('pf_operadora_id', ['options' => $pfOperadoras, 'empty' => true]);
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
