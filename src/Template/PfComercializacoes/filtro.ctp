<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Produto PF',  '/pfComercializacoes/add', ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'pfComercializacoes.add']);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['value' => $estado, 'options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?php if (empty($operadoras)) : ?>
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
    <?php else : ?>
        <?= $this->Form->input('operadora_id', ['value' =>  $operadora, 'options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
    <?php endif; ?>
</div>

<?php if (isset($filtradas)) : ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= 'Título' ?></th>
                <th><?= 'Observação' ?></th>
                <th><?= 'Municípios' ?></th>
                <th><?= 'Operadora' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($filtradas as $item) : ?>
                <tr>
                    <td><?= $item->nome ?></td>
                    <td><?= $item->descricao ?></td>
                    <td>
                        <span style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap; display: block;">
                            <?php foreach ($item->municipios as $municipio) {
                                echo $municipio['nome'] . ', ';
                            } ?>
                        </span>
                    </td>
                    <td>
                        <?php
                        if (isset($item->pf_operadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $item->pf_operadora->imagen->caminho . "/" . $item->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $item->pf_operadora->detalhe;
                        } else {
                            echo $item->pf_operadora->nome . " - " . $item->pf_operadora->detalhe;
                        }
                        ?></td>
                    <td class="actions">
                        <?php if ($sessao['role'] == 'admin') { ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/pfComercializacoes/edit/$item->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'pfComercializacoes.edit']) ?>

                            <?= $this->Form->postLink('', "/pfComercializacoes/delete/$item->id", ['confirm' => __('Confirma exclusão da Carência?', $item->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'pfComercializacoes.delete'])
                            ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = baseUrl + "pfComercializacoes/filtro/" + $(this).val();
    });

    $("#operadora-id").change(function() {
        window.location.href = baseUrl + "pfComercializacoes/filtro/" + $("#estados").val() + "/" + $(this).val();
    });
</script>