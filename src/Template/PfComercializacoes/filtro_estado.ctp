 <table cellpadding="0" cellspacing="0" class="col-xs-12">
     <thead>
         <tr>
             <th><?= $this->Paginator->sort('nome') ?></th>
             <th><?= $this->Paginator->sort('estado_id') ?></th>
             <th><?= $this->Paginator->sort('pf_operadora_id', ['label' => 'Operadora']) ?></th>
             <th class="actions"><?= __('Actions') ?></th>
         </tr>
     </thead>
     <tbody>
         <?php foreach ($pfComercializacoes as $pfComercializaco): ?>
         <tr>
             <td><?= h($pfComercializaco->nome) ?></td>
             <td><?= $pfComercializaco->estado->nome ?></td>
             <td><?= $pfComercializaco->pf_operadora->nome ?></td>
             <td class="actions">
                 <?=
                    $this->Html->link('', ['action' => 'edit', $pfComercializaco->id], ['title' => __('Editar'),
                                'class' => 'btn btn-default btn-sm fa fa-pencil',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Editar'])
                            ?>
                 <?=
                        $this->Form->postLink('', ['action' => 'delete', $pfComercializaco->id], ['confirm' => __('Tem certeza que deseja excluir Profissão {0}?', $pfComercializaco->nome . "  " . $pfComercializaco->detalhes), 'title' => __('Deletar'),
                            'class' => 'btn btn-default btn-sm fa fa-trash',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Deletar'])
                        ?>
             </td>
         </tr>
         <?php endforeach; ?>
     </tbody>
 </table>