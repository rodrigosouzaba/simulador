<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Rede PF', '/pfComercializacoes/add', ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'pfComercializacoes.add']);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>

<div id="operadoras" class="col-md-3 fonteReduzida">
    <?= $this->Form->input('pf_operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => 'disabled']); ?>
</div>

<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = baseUrl + "pfComercializacoes/filtro/" + $(this).val()
    });
</script>