<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Comercializaco'), ['action' => 'edit', $pfComercializaco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Comercializaco'), ['action' => 'delete', $pfComercializaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfComercializaco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Comercializaco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfComercializacoes view large-9 medium-8 columns content">
    <h3><?= h($pfComercializaco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($pfComercializaco->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Estado') ?></th>
            <td><?= $pfComercializaco->has('estado') ? $this->Html->link($pfComercializaco->estado->id, ['controller' => 'Estados', 'action' => 'view', $pfComercializaco->estado->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Operadora') ?></th>
            <td><?= $pfComercializaco->has('pf_operadora') ? $this->Html->link($pfComercializaco->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfComercializaco->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfComercializaco->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($pfComercializaco->descricao)); ?>
    </div>
</div>
