<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes Municipios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfComercializacoesMunicipios form large-9 medium-8 columns content">
    <?= $this->Form->create($pfComercializacoesMunicipio) ?>
    <fieldset>
        <legend><?= __('Add Pf Comercializacoes Municipio') ?></legend>
        <?php
            echo $this->Form->input('pf_tabela_id', ['options' => $pfTabelas]);
            echo $this->Form->input('municipio_id', ['options' => $municipios]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
