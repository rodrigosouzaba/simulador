<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Produto PF', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['value' => $estado, 'options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?php if (empty($operadoras)) : ?>
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
    <?php else : ?>
        <?= $this->Form->input('operadora_id', ['value' =>  $operadora, 'options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
    <?php endif; ?>
</div>

<?php if (isset($tabelas)) : ?>
    <table cellpadding="0" cellspacing="0" class="col-xs-12">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('pf_operadora_id', ['label' => 'Operadora']) ?></th>
                <th><?= 'municipios' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tabelas as $tabela) : ?>
                <tr>
                    <td><?= h($tabela->nome) ?></td>
                    <td><?= $tabela->pf_operadora->nome ?></td>
                    <td>
                        <span style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap; display: block;">
                            <?php foreach ($tabela->municipios as $municipio) {
                                echo $municipio['nome'] .  ", ";
                            } ?>
                        </span>
                    </td>
                    <td class="actions">
                        <?=
                            $this->Html->link('', ['action' => 'edit', $tabela->id], [
                                'title' => __('Editar'),
                                'class' => 'btn btn-default btn-sm fa fa-pencil',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Editar'
                            ])
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = "<?= $this->request->webroot . $this->request->controller ?>/filtro/" + $(this).val();
    });

    $("#operadora-id").change(function() {
        window.location.href = "<?= $this->request->webroot . $this->request->controller ?>/filtro/" + $("#estados").val() + "/" + $(this).val();
    });
</script>