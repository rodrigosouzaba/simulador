<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Rede PF', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>

<div id="operadoras" class="col-md-3 fonteReduzida">
    <?= $this->Form->input('pf_operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => 'disabled']); ?>
</div>

<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = "<?= $this->request->controller ?>/filtro/" + $(this).val()
    });
</script>