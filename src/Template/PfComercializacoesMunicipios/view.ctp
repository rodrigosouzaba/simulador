<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Comercializacoes Municipio'), ['action' => 'edit', $pfComercializacoesMunicipio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Comercializacoes Municipio'), ['action' => 'delete', $pfComercializacoesMunicipio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfComercializacoesMunicipio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes Municipios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Comercializacoes Municipio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfComercializacoesMunicipios view large-9 medium-8 columns content">
    <h3><?= h($pfComercializacoesMunicipio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Tabela') ?></th>
            <td><?= $pfComercializacoesMunicipio->has('pf_tabela') ? $this->Html->link($pfComercializacoesMunicipio->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $pfComercializacoesMunicipio->pf_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Municipio') ?></th>
            <td><?= $pfComercializacoesMunicipio->has('municipio') ? $this->Html->link($pfComercializacoesMunicipio->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $pfComercializacoesMunicipio->municipio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfComercializacoesMunicipio->id) ?></td>
        </tr>
    </table>
</div>
