<div class="col-md-3 fonteReduzida">
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Rede PF', "/pfDocumentos/add", ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'pfDocumentos.add']);
    ?>
</div>

<?= $this->element('filtro_interno') ?>

<?php if (isset($documentos)) : ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= 'Título' ?></th>
                <th style="width: 50% !important"><?= 'Observação' ?></th>
                <th><?= 'Operadora' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($documentos as $item) : ?>
                <tr>
                    <td><?= $item->nome ?></td>
                    <td><?= $item->descricao ?></td>
                    <td>
                        <?php
                        if (isset($item->pf_operadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $item->pf_operadora->imagen->caminho . "/" . $item->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $item->pf_operadora->detalhe;
                        } else {
                            echo $item->pf_operadora->nome . " - " . $item->pf_operadora->detalhe;
                        }
                        ?></td>
                    <td class="actions">
                        <?php if ($sessao['role'] == 'admin') { ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/pfDocumentos/edit/$item->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'pfDocumentos.edit']) ?>
                            <?= $this->Form->postLink('', "/pfDocumentos/delete/$item->id", ['confirm' => __('Confirma exclusão da Carência?', $item->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'pfDocumentos.delete']) ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
<?php endif; ?>