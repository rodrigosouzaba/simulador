<div class="col-md-12">
    <?= $this->Form->create($pfEntidade) ?>
    <div class="col-md-12 col-xs-12">
        <?= $this->Form->input('nome'); ?>
    </div>

    <div class="col-md-6 col-xs-12">
        <?= $this->Form->input('idade_min', ['label' => 'Idade Mínima']); ?>
    </div>
    <div class="col-md-6 col-xs-12">
        <?= $this->Form->input('idade_max', ['label' => 'Idade Máxima']); ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">

        <?= $this->Form->input('lista_profissoes_escolhidas', ['options' => $profissoes, 'default' => array_keys($profissoes_tabela),  'multiple' => 'multiple', 'label' => 'Profissões da Entidade', 'style' => 'margin-bottom: 0 !important']); ?>
        <div class="clearfix">&nbsp;</div>
        <?= $this->Form->input('descricao', ['label' => 'Observação']); ?>

        <div class="clearfix">&nbsp;</div>
        <?= $this->element('botoesAdd') ?>
    </div>
    <?= $this->Form->end() ?>
</div>