<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfEntidade->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidade->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades Profissoes'), ['controller' => 'PfEntidadesProfissoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Entidades Profisso'), ['controller' => 'PfEntidadesProfissoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfEntidades form large-9 medium-8 columns content">
    <?= $this->Form->create($pfEntidade) ?>
    <fieldset>
        <legend><?= __('Edit Pf Entidade') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('tabelas._ids', ['options' => $tabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
