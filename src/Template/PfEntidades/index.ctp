<div class="col-md-12">
    <div class="col-md-4 fonteReduzida">
        <?= $this->Html->link('Grupos de Entidades', '/pfEntidadesGrupos/index', ['id' => 'linkGruposEntidades', 'class' => 'btn btn-sm  btn-default linkAjax', 'role' => 'button', 'escape' => false, 'sid' => 'pfEntidadesGrupos.index']); ?>
        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Entidade', '/pfEntidades/add', ['class' => 'btn btn-sm btn-sm btn-sm btn-primary', 'role' => 'button', 'escape' => false, 'sid' => 'pfEntidades.add']); ?>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <input type="text" name="info_busca" class="form-control" placeholder="Pesquisar Entidade" id="info-busca">
            <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="sugestao" style="display: none;"></button>
            <style type="text/css">
                #sugestoes {
                    display: block;
                    margin-left: 15px;
                    margin-top: -17px;
                    padding: 0px;
                    border-top-right-radius: 0px;
                    border-top-left-radius: 0px;
                    background-color: white;
                    width: 312.4px;
                }

                #sugestoes li {
                    display: none;
                    padding-left: 10px;
                }

                #sugestoes li:hover {
                    background-color: #6659591a;
                    color: black;
                    cursor: context-menu;
                }
            </style>
            <ul class="dropdown-menu" id="sugestoes">
            </ul>
        </div>
    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfEntidades as $pfEntidade) : ?>
                <tr>
                    <td><?= h($pfEntidade->nome) ?></td>
                    <td class="actions">
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm dropdown-toggle btn-success " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-cogs"></i>
                            </button>
                            <ul class="dropdown-menu" style="right: 0; left: auto;">
                                <li>
                                    <?= $this->Html->link(__(' Editar'), "/pfEntidades/edit/$pfEntidade->id", ['title' => __('Editar'), 'sid' => 'pfEntidades.edit', 'title' => 'Editar']) ?>
                                </li>
                                <li>
                                    <?= $this->Form->postLink(__(' Excluir'), "/pfEntidades/delete/$pfEntidade->id", ['confirm' => __('Tem certeza que deseja excluir Entidade {0}?', $pfEntidade->nome . "  " . $pfEntidade->detalhes), 'title' => __('Deletar'), 'sid' => 'pfEntidades.delete', 'title' => 'Excluir']) ?>
                                </li>
                            </ul>
                        </div>

                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<script type="text/javascript">
    $("#linkGruposEntidades").click(function(e) {
        e.preventDefault();
        $.get(baseUrl + 'PfEntidadesGrupos')
            .then(data => {
                $("#gridPf").empty()
                $("#gridPf").append(data)
            })
    })
    $(document).ready(function() {
        $(".linkEntidade").click(function() {
            $.ajax({
                type: "GET",
                url: baseUrl + "pfEntidades/edit/" + $(this).attr('entidade'),
                success: function(data) {
                    $("#pagina").empty();
                    $("#pagina").append(data);
                }
            });
        });
    });
    $("#info-busca").keyup(function() {
        var value = $(this).val().toLowerCase();
        $("#sugestoes li").filter(function() {
            var lista = $(this).text();
            lista = lista.toLowerCase();
            if (value != '') {
                if (lista.indexOf(value) == 0) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            } else {
                $(this).hide();
            }
        });
    });
    $(document).ready(function() {
        $.ajax({
            type: "GET",
            url: baseUrl + "pfEntidades/search",
            success: function(data) {
                $("#sugestoes").empty();
                $("#sugestoes").append(data);
            }
        });
    });
    $(window).click(function() {
        $("#sugestoes li").hide();
    });
</script>