<div class="container">
    <?= $this->Form->create($pfEntidadesGrupo) ?>
    <fieldset>
        <legend><?= __('Novo grupo de Entidades') ?></legend>
        <div class="spacer-md">&nbsp;</div>

        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->control('nome'); ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('pf_operadora_id', ['options' => $pfOperadoras, 'label' => 'Operadoras', 'empty' => 'Selecione']); ?>
            </div>
            <div class="col-md-12">
                <?= $this->Form->control('pf_entidades._ids', ['options' => $pfEntidades, 'label' => 'Entidades']); ?>
            </div>
        </div>

    </fieldset>
    <div class="spacer-md">&nbsp;</div>
    <div class="text-center">
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>