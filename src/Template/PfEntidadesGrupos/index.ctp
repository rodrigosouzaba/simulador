<div class="container">
    <h3>Grupos de Entidades</h3>
    <?= $this->Html->link('Entidades', '/pfEntidadesGrupos/index', ['id' => 'linkEntidades', 'class' => 'btn btn-sm  btn-default linkAjax', 'role' => 'button', 'escape' => false, 'sid' => 'pfEntidadesGrupos.index']); ?>
    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']) . ' Novo', ['action' => 'add'], ['class' => 'btn btn-primary', 'escape' => false]) ?>

    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pf_operadora_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfEntidadesGrupos as $pfEntidadesGrupo) : ?>
                <tr>
                    <td><?= h($pfEntidadesGrupo->nome) ?></td>
                    <td><?= $pfEntidadesGrupo->has('pf_operadora') ? $this->Html->link($pfEntidadesGrupo->pf_operadora->nome, ['controller' => 'PfOperadoras', 'action' => 'view', $pfEntidadesGrupo->pf_operadora->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfEntidadesGrupo->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfEntidadesGrupo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidadesGrupo->id)]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#linkEntidades").click(function(e) {
            e.preventDefault();
            $.get(baseUrl + 'PfEntidades')
                .then(data => {
                    $("#gridPf").empty()
                    $("#gridPf").append(data)
                })
        })
    })
</script>