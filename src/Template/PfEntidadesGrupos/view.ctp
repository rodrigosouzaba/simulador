<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PfEntidadesGrupo $pfEntidadesGrupo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Entidades Grupo'), ['action' => 'edit', $pfEntidadesGrupo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Entidades Grupo'), ['action' => 'delete', $pfEntidadesGrupo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidadesGrupo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Entidades Grupos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Entidades Grupo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Entidade'), ['controller' => 'PfEntidades', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfEntidadesGrupos view large-9 medium-8 columns content">
    <h3><?= h($pfEntidadesGrupo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($pfEntidadesGrupo->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pf Operadora') ?></th>
            <td><?= $pfEntidadesGrupo->has('pf_operadora') ? $this->Html->link($pfEntidadesGrupo->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfEntidadesGrupo->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfEntidadesGrupo->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pf Entidades') ?></h4>
        <?php if (!empty($pfEntidadesGrupo->pf_entidades)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Descricao') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Idade Min') ?></th>
                <th scope="col"><?= __('Idade Max') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfEntidadesGrupo->pf_entidades as $pfEntidades): ?>
            <tr>
                <td><?= h($pfEntidades->id) ?></td>
                <td><?= h($pfEntidades->descricao) ?></td>
                <td><?= h($pfEntidades->nome) ?></td>
                <td><?= h($pfEntidades->idade_min) ?></td>
                <td><?= h($pfEntidades->idade_max) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfEntidades', 'action' => 'view', $pfEntidades->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfEntidades', 'action' => 'edit', $pfEntidades->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfEntidades', 'action' => 'delete', $pfEntidades->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidades->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
