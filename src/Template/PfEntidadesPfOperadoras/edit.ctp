<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfEntidadesPfOperadora->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidadesPfOperadora->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades Pf Operadoras'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Entidade'), ['controller' => 'PfEntidades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfEntidadesPfOperadoras form large-9 medium-8 columns content">
    <?= $this->Form->create($pfEntidadesPfOperadora) ?>
    <fieldset>
        <legend><?= __('Edit Pf Entidades Pf Operadora') ?></legend>
        <?php
            echo $this->Form->input('pf_entidade_id', ['options' => $pfEntidades]);
            echo $this->Form->input('pf_operadora_id', ['options' => $pfOperadoras]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
