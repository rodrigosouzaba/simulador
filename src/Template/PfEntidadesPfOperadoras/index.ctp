<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pf Entidades Pf Operadora'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Entidade'), ['controller' => 'PfEntidades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfEntidadesPfOperadoras index large-9 medium-8 columns content">
    <h3><?= __('Pf Entidades Pf Operadoras') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_entidade_id') ?></th>
                <th><?= $this->Paginator->sort('pf_operadora_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfEntidadesPfOperadoras as $pfEntidadesPfOperadora): ?>
            <tr>
                <td><?= $this->Number->format($pfEntidadesPfOperadora->id) ?></td>
                <td><?= $pfEntidadesPfOperadora->has('pf_entidade') ? $this->Html->link($pfEntidadesPfOperadora->pf_entidade->id, ['controller' => 'PfEntidades', 'action' => 'view', $pfEntidadesPfOperadora->pf_entidade->id]) : '' ?></td>
                <td><?= $pfEntidadesPfOperadora->has('pf_operadora') ? $this->Html->link($pfEntidadesPfOperadora->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfEntidadesPfOperadora->pf_operadora->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pfEntidadesPfOperadora->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfEntidadesPfOperadora->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfEntidadesPfOperadora->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidadesPfOperadora->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
