<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Entidades Pf Operadora'), ['action' => 'edit', $pfEntidadesPfOperadora->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Entidades Pf Operadora'), ['action' => 'delete', $pfEntidadesPfOperadora->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidadesPfOperadora->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Entidades Pf Operadoras'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Entidades Pf Operadora'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Entidade'), ['controller' => 'PfEntidades', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfEntidadesPfOperadoras view large-9 medium-8 columns content">
    <h3><?= h($pfEntidadesPfOperadora->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Entidade') ?></th>
            <td><?= $pfEntidadesPfOperadora->has('pf_entidade') ? $this->Html->link($pfEntidadesPfOperadora->pf_entidade->id, ['controller' => 'PfEntidades', 'action' => 'view', $pfEntidadesPfOperadora->pf_entidade->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Operadora') ?></th>
            <td><?= $pfEntidadesPfOperadora->has('pf_operadora') ? $this->Html->link($pfEntidadesPfOperadora->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfEntidadesPfOperadora->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfEntidadesPfOperadora->id) ?></td>
        </tr>
    </table>
</div>
