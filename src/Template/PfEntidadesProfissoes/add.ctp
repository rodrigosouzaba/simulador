<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades Profissoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Entidade'), ['controller' => 'PfEntidades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Profissoes'), ['controller' => 'PfProfissoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Profisso'), ['controller' => 'PfProfissoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfEntidadesProfissoes form large-9 medium-8 columns content">
    <?= $this->Form->create($pfEntidadesProfisso) ?>
    <fieldset>
        <legend><?= __('Add Pf Entidades Profisso') ?></legend>
        <?php
            echo $this->Form->input('pf_entidade_id', ['options' => $pfEntidades]);
            echo $this->Form->input('pf_profissao_id', ['options' => $pfProfissoes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
