<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Entidades Profisso'), ['action' => 'edit', $pfEntidadesProfisso->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Entidades Profisso'), ['action' => 'delete', $pfEntidadesProfisso->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidadesProfisso->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Entidades Profissoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Entidades Profisso'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Entidade'), ['controller' => 'PfEntidades', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Profissoes'), ['controller' => 'PfProfissoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Profisso'), ['controller' => 'PfProfissoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfEntidadesProfissoes view large-9 medium-8 columns content">
    <h3><?= h($pfEntidadesProfisso->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Entidade') ?></th>
            <td><?= $pfEntidadesProfisso->has('pf_entidade') ? $this->Html->link($pfEntidadesProfisso->pf_entidade->id, ['controller' => 'PfEntidades', 'action' => 'view', $pfEntidadesProfisso->pf_entidade->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Profisso') ?></th>
            <td><?= $pfEntidadesProfisso->has('pf_profisso') ? $this->Html->link($pfEntidadesProfisso->pf_profisso->id, ['controller' => 'PfProfissoes', 'action' => 'view', $pfEntidadesProfisso->pf_profisso->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfEntidadesProfisso->id) ?></td>
        </tr>
    </table>
</div>
