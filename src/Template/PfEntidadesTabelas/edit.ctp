<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfEntidadesTabela->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidadesTabela->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades Tabelas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Entidade'), ['controller' => 'PfEntidades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfEntidadesTabelas form large-9 medium-8 columns content">
    <?= $this->Form->create($pfEntidadesTabela) ?>
    <fieldset>
        <legend><?= __('Edit Pf Entidades Tabela') ?></legend>
        <?php
            echo $this->Form->input('pf_entidade_id', ['options' => $pfEntidades]);
            echo $this->Form->input('pf_tabela_id', ['options' => $pfTabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
