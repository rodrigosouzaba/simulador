<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pf Entidades Tabela'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Entidade'), ['controller' => 'PfEntidades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfEntidadesTabelas index large-9 medium-8 columns content">
    <h3><?= __('Pf Entidades Tabelas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_entidade_id') ?></th>
                <th><?= $this->Paginator->sort('pf_tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfEntidadesTabelas as $pfEntidadesTabela): ?>
            <tr>
                <td><?= $this->Number->format($pfEntidadesTabela->id) ?></td>
                <td><?= $pfEntidadesTabela->has('pf_entidade') ? $this->Html->link($pfEntidadesTabela->pf_entidade->id, ['controller' => 'PfEntidades', 'action' => 'view', $pfEntidadesTabela->pf_entidade->id]) : '' ?></td>
                <td><?= $pfEntidadesTabela->has('pf_tabela') ? $this->Html->link($pfEntidadesTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $pfEntidadesTabela->pf_tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pfEntidadesTabela->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfEntidadesTabela->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfEntidadesTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidadesTabela->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
