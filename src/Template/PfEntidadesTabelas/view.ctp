<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Entidades Tabela'), ['action' => 'edit', $pfEntidadesTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Entidades Tabela'), ['action' => 'delete', $pfEntidadesTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfEntidadesTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Entidades Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Entidades Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Entidade'), ['controller' => 'PfEntidades', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfEntidadesTabelas view large-9 medium-8 columns content">
    <h3><?= h($pfEntidadesTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Entidade') ?></th>
            <td><?= $pfEntidadesTabela->has('pf_entidade') ? $this->Html->link($pfEntidadesTabela->pf_entidade->id, ['controller' => 'PfEntidades', 'action' => 'view', $pfEntidadesTabela->pf_entidade->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Tabela') ?></th>
            <td><?= $pfEntidadesTabela->has('pf_tabela') ? $this->Html->link($pfEntidadesTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $pfEntidadesTabela->pf_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfEntidadesTabela->id) ?></td>
        </tr>
    </table>
</div>
