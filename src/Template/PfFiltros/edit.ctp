<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfFiltro->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfFiltro->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Filtros'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['controller' => 'PfAtendimentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Atendimento'), ['controller' => 'PfAtendimentos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Acomodacoes'), ['controller' => 'PfAcomodacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Acomodaco'), ['controller' => 'PfAcomodacoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['controller' => 'TiposProdutos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['controller' => 'TiposProdutos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfFiltros form large-9 medium-8 columns content">
    <?= $this->Form->create($pfFiltro) ?>
    <fieldset>
        <legend><?= __('Edit Pf Filtro') ?></legend>
        <?php
            echo $this->Form->input('pf_calculo_id', ['options' => $pfCalculos]);
            echo $this->Form->input('pf_atendimento_id', ['options' => $pfAtendimentos, 'empty' => true]);
            echo $this->Form->input('coparticipacao');
            echo $this->Form->input('pf_acomodacao_id', ['options' => $pfAcomodacoes, 'empty' => true]);
            echo $this->Form->input('tipo_produto_id', ['options' => $tiposProdutos, 'empty' => true]);
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('carencia');
            echo $this->Form->input('reembolso');
            echo $this->Form->input('rede');
            echo $this->Form->input('documento');
            echo $this->Form->input('dependente');
            echo $this->Form->input('observacao');
            echo $this->Form->input('tabelas._ids', ['options' => $tabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
