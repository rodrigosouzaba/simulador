<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pf Filtro'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['controller' => 'PfAtendimentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Atendimento'), ['controller' => 'PfAtendimentos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Acomodacoes'), ['controller' => 'PfAcomodacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Acomodaco'), ['controller' => 'PfAcomodacoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['controller' => 'TiposProdutos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['controller' => 'TiposProdutos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfFiltros index large-9 medium-8 columns content">
    <h3><?= __('Pf Filtros') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_calculo_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('pf_atendimento_id') ?></th>
                <th><?= $this->Paginator->sort('coparticipacao') ?></th>
                <th><?= $this->Paginator->sort('pf_acomodacao_id') ?></th>
                <th><?= $this->Paginator->sort('tipo_produto_id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('carencia') ?></th>
                <th><?= $this->Paginator->sort('reembolso') ?></th>
                <th><?= $this->Paginator->sort('rede') ?></th>
                <th><?= $this->Paginator->sort('documento') ?></th>
                <th><?= $this->Paginator->sort('dependente') ?></th>
                <th><?= $this->Paginator->sort('observacao') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfFiltros as $pfFiltro): ?>
            <tr>
                <td><?= $this->Number->format($pfFiltro->id) ?></td>
                <td><?= $pfFiltro->has('pf_calculo') ? $this->Html->link($pfFiltro->pf_calculo->id, ['controller' => 'PfCalculos', 'action' => 'view', $pfFiltro->pf_calculo->id]) : '' ?></td>
                <td><?= h($pfFiltro->created) ?></td>
                <td><?= $pfFiltro->has('pf_atendimento') ? $this->Html->link($pfFiltro->pf_atendimento->id, ['controller' => 'PfAtendimentos', 'action' => 'view', $pfFiltro->pf_atendimento->id]) : '' ?></td>
                <td><?= h($pfFiltro->coparticipacao) ?></td>
                <td><?= $pfFiltro->has('pf_acomodaco') ? $this->Html->link($pfFiltro->pf_acomodaco->id, ['controller' => 'PfAcomodacoes', 'action' => 'view', $pfFiltro->pf_acomodaco->id]) : '' ?></td>
                <td><?= $pfFiltro->has('tipos_produto') ? $this->Html->link($pfFiltro->tipos_produto->id, ['controller' => 'TiposProdutos', 'action' => 'view', $pfFiltro->tipos_produto->id]) : '' ?></td>
                <td><?= $pfFiltro->has('user') ? $this->Html->link($pfFiltro->user->id, ['controller' => 'Users', 'action' => 'view', $pfFiltro->user->id]) : '' ?></td>
                <td><?= $this->Number->format($pfFiltro->carencia) ?></td>
                <td><?= $this->Number->format($pfFiltro->reembolso) ?></td>
                <td><?= $this->Number->format($pfFiltro->rede) ?></td>
                <td><?= $this->Number->format($pfFiltro->documento) ?></td>
                <td><?= $this->Number->format($pfFiltro->dependente) ?></td>
                <td><?= $this->Number->format($pfFiltro->observacao) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pfFiltro->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfFiltro->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfFiltro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfFiltro->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
