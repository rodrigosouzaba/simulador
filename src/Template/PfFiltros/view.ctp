<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Filtro'), ['action' => 'edit', $pfFiltro->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Filtro'), ['action' => 'delete', $pfFiltro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfFiltro->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Filtros'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Filtro'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['controller' => 'PfAtendimentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Atendimento'), ['controller' => 'PfAtendimentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Acomodacoes'), ['controller' => 'PfAcomodacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Acomodaco'), ['controller' => 'PfAcomodacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['controller' => 'TiposProdutos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['controller' => 'TiposProdutos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfFiltros view large-9 medium-8 columns content">
    <h3><?= h($pfFiltro->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Calculo') ?></th>
            <td><?= $pfFiltro->has('pf_calculo') ? $this->Html->link($pfFiltro->pf_calculo->id, ['controller' => 'PfCalculos', 'action' => 'view', $pfFiltro->pf_calculo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Atendimento') ?></th>
            <td><?= $pfFiltro->has('pf_atendimento') ? $this->Html->link($pfFiltro->pf_atendimento->id, ['controller' => 'PfAtendimentos', 'action' => 'view', $pfFiltro->pf_atendimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Coparticipacao') ?></th>
            <td><?= h($pfFiltro->coparticipacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Acomodaco') ?></th>
            <td><?= $pfFiltro->has('pf_acomodaco') ? $this->Html->link($pfFiltro->pf_acomodaco->id, ['controller' => 'PfAcomodacoes', 'action' => 'view', $pfFiltro->pf_acomodaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tipos Produto') ?></th>
            <td><?= $pfFiltro->has('tipos_produto') ? $this->Html->link($pfFiltro->tipos_produto->id, ['controller' => 'TiposProdutos', 'action' => 'view', $pfFiltro->tipos_produto->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $pfFiltro->has('user') ? $this->Html->link($pfFiltro->user->id, ['controller' => 'Users', 'action' => 'view', $pfFiltro->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfFiltro->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Carencia') ?></th>
            <td><?= $this->Number->format($pfFiltro->carencia) ?></td>
        </tr>
        <tr>
            <th><?= __('Reembolso') ?></th>
            <td><?= $this->Number->format($pfFiltro->reembolso) ?></td>
        </tr>
        <tr>
            <th><?= __('Rede') ?></th>
            <td><?= $this->Number->format($pfFiltro->rede) ?></td>
        </tr>
        <tr>
            <th><?= __('Documento') ?></th>
            <td><?= $this->Number->format($pfFiltro->documento) ?></td>
        </tr>
        <tr>
            <th><?= __('Dependente') ?></th>
            <td><?= $this->Number->format($pfFiltro->dependente) ?></td>
        </tr>
        <tr>
            <th><?= __('Observacao') ?></th>
            <td><?= $this->Number->format($pfFiltro->observacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($pfFiltro->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Tabelas') ?></h4>
        <?php if (!empty($pfFiltro->tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Vigencia') ?></th>
                <th><?= __('Faixa1') ?></th>
                <th><?= __('Faixa2') ?></th>
                <th><?= __('Faixa3') ?></th>
                <th><?= __('Faixa4') ?></th>
                <th><?= __('Faixa5') ?></th>
                <th><?= __('Faixa6') ?></th>
                <th><?= __('Faixa7') ?></th>
                <th><?= __('Faixa8') ?></th>
                <th><?= __('Faixa9') ?></th>
                <th><?= __('Faixa10') ?></th>
                <th><?= __('Faixa11') ?></th>
                <th><?= __('Faixa12') ?></th>
                <th><?= __('Produto Id') ?></th>
                <th><?= __('Operadora Id') ?></th>
                <th><?= __('Ramo Id') ?></th>
                <th><?= __('Abrangencia Id') ?></th>
                <th><?= __('Tipo Id') ?></th>
                <th><?= __('Modalidade Id') ?></th>
                <th><?= __('Validade') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Regiao Id') ?></th>
                <th><?= __('Cod Ans') ?></th>
                <th><?= __('Minimo Vidas') ?></th>
                <th><?= __('Maximo Vidas') ?></th>
                <th><?= __('Titulares') ?></th>
                <th><?= __('Prioridade') ?></th>
                <th><?= __('Tipo Cnpj') ?></th>
                <th><?= __('Coparticipacao') ?></th>
                <th><?= __('Detalhe Coparticipacao') ?></th>
                <th><?= __('Tipo Contratacao') ?></th>
                <th><?= __('Reembolso') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfFiltro->tabelas as $tabelas): ?>
            <tr>
                <td><?= h($tabelas->id) ?></td>
                <td><?= h($tabelas->nome) ?></td>
                <td><?= h($tabelas->descricao) ?></td>
                <td><?= h($tabelas->vigencia) ?></td>
                <td><?= h($tabelas->faixa1) ?></td>
                <td><?= h($tabelas->faixa2) ?></td>
                <td><?= h($tabelas->faixa3) ?></td>
                <td><?= h($tabelas->faixa4) ?></td>
                <td><?= h($tabelas->faixa5) ?></td>
                <td><?= h($tabelas->faixa6) ?></td>
                <td><?= h($tabelas->faixa7) ?></td>
                <td><?= h($tabelas->faixa8) ?></td>
                <td><?= h($tabelas->faixa9) ?></td>
                <td><?= h($tabelas->faixa10) ?></td>
                <td><?= h($tabelas->faixa11) ?></td>
                <td><?= h($tabelas->faixa12) ?></td>
                <td><?= h($tabelas->produto_id) ?></td>
                <td><?= h($tabelas->operadora_id) ?></td>
                <td><?= h($tabelas->ramo_id) ?></td>
                <td><?= h($tabelas->abrangencia_id) ?></td>
                <td><?= h($tabelas->tipo_id) ?></td>
                <td><?= h($tabelas->modalidade_id) ?></td>
                <td><?= h($tabelas->validade) ?></td>
                <td><?= h($tabelas->estado_id) ?></td>
                <td><?= h($tabelas->regiao_id) ?></td>
                <td><?= h($tabelas->cod_ans) ?></td>
                <td><?= h($tabelas->minimo_vidas) ?></td>
                <td><?= h($tabelas->maximo_vidas) ?></td>
                <td><?= h($tabelas->titulares) ?></td>
                <td><?= h($tabelas->prioridade) ?></td>
                <td><?= h($tabelas->tipo_cnpj) ?></td>
                <td><?= h($tabelas->coparticipacao) ?></td>
                <td><?= h($tabelas->detalhe_coparticipacao) ?></td>
                <td><?= h($tabelas->tipo_contratacao) ?></td>
                <td><?= h($tabelas->reembolso) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Tabelas', 'action' => 'view', $tabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Tabelas', 'action' => 'edit', $tabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tabelas', 'action' => 'delete', $tabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
