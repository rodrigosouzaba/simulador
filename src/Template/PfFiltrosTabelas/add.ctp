<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pf Filtros Tabelas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Filtros'), ['controller' => 'PfFiltros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Filtro'), ['controller' => 'PfFiltros', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfFiltrosTabelas form large-9 medium-8 columns content">
    <?= $this->Form->create($pfFiltrosTabela) ?>
    <fieldset>
        <legend><?= __('Add Pf Filtros Tabela') ?></legend>
        <?php
            echo $this->Form->input('pf_filtro_id', ['options' => $pfFiltros]);
            echo $this->Form->input('pf_tabela_id', ['options' => $pfTabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
