<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pf Filtros Tabela'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Filtros'), ['controller' => 'PfFiltros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Filtro'), ['controller' => 'PfFiltros', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfFiltrosTabelas index large-9 medium-8 columns content">
    <h3><?= __('Pf Filtros Tabelas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_filtro_id') ?></th>
                <th><?= $this->Paginator->sort('pf_tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfFiltrosTabelas as $pfFiltrosTabela): ?>
            <tr>
                <td><?= $this->Number->format($pfFiltrosTabela->id) ?></td>
                <td><?= $pfFiltrosTabela->has('pf_filtro') ? $this->Html->link($pfFiltrosTabela->pf_filtro->id, ['controller' => 'PfFiltros', 'action' => 'view', $pfFiltrosTabela->pf_filtro->id]) : '' ?></td>
                <td><?= $pfFiltrosTabela->has('pf_tabela') ? $this->Html->link($pfFiltrosTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $pfFiltrosTabela->pf_tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pfFiltrosTabela->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfFiltrosTabela->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfFiltrosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfFiltrosTabela->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
