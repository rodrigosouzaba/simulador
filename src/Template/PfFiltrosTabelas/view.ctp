<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Filtros Tabela'), ['action' => 'edit', $pfFiltrosTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Filtros Tabela'), ['action' => 'delete', $pfFiltrosTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfFiltrosTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Filtros Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Filtros Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Filtros'), ['controller' => 'PfFiltros', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Filtro'), ['controller' => 'PfFiltros', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfFiltrosTabelas view large-9 medium-8 columns content">
    <h3><?= h($pfFiltrosTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Filtro') ?></th>
            <td><?= $pfFiltrosTabela->has('pf_filtro') ? $this->Html->link($pfFiltrosTabela->pf_filtro->id, ['controller' => 'PfFiltros', 'action' => 'view', $pfFiltrosTabela->pf_filtro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Tabela') ?></th>
            <td><?= $pfFiltrosTabela->has('pf_tabela') ? $this->Html->link($pfFiltrosTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $pfFiltrosTabela->pf_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfFiltrosTabela->id) ?></td>
        </tr>
    </table>
</div>
