<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfFormasPagamento->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfFormasPagamento->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Formas Pagamentos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfFormasPagamentos form large-9 medium-8 columns content">
    <?= $this->Form->create($pfFormasPagamento) ?>
    <fieldset>
        <legend><?= __('Edit Pf Formas Pagamento') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('pf_operadora_id', ['options' => $pfOperadoras]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
