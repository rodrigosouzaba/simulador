<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('id') ?></th>-->
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($pfFormasPagamentos as $formaPagamento) : ?>
            <tr>
                <td><?= $formaPagamento->nome ?></td>
                <td><?= $formaPagamento->descricao ?></td>
                <td>
                    <?php
                    if (isset($formaPagamento->pf_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $formaPagamento->pf_operadora->imagen->caminho . "/" . $formaPagamento->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $formaPagamento->pf_operadora->detalhe;
                    } else {
                        echo $formaPagamento->pf_operadora->nome . " - " . $formaPagamento->pf_operadora->detalhe;
                    }
                    ?>
                </td>
                <td class="actions">
                    <?php if ($sessao['role'] == 'admin') { ?>

                        <?=
                            $this->Html->link('', ['action' => 'edit', $formaPagamento->id], [
                                'title' => __('Editar'),
                                'class' => 'btn btn-default btn-sm fa fa-pencil',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Editar'
                            ])
                        ?>
                        <?=
                            $this->Form->postLink('', ['action' => 'delete', $formaPagamento->id], [
                                'confirm' => __('Tem certeza que deseja excluir Profissão {0}?', $formaPagamento->nome . "  " . $formaPagamento->detalhes), 'title' => __('Deletar'),
                                'class' => 'btn btn-danger btn-sm fa fa-trash',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Deletar'
                            ])
                        ?>
                    <?php } ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
