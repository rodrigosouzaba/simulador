<div class="col-md-3 fonteReduzida">
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Rede PF', "/pfFormasPagamentos/add", ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'pfFormasPagamentos.add']);
    ?>
</div>
<?= $this->element('filtro_interno') ?>

<?php if (isset($formasPagamentos)) : ?> <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= 'Título' ?></th>
                <th style="width: 50% !important"><?= 'Observação' ?></th>
                <th><?= 'Operadora' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($formasPagamentos as $item) : ?>
                <tr>
                    <td><?= $item->nome ?></td>
                    <td><?= $item->descricao ?></td>
                    <td>
                        <?php
                        if (isset($item->pf_operadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $item->pf_operadora->imagen->caminho . "/" . $item->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $item->pf_operadora->detalhe;
                        } else {
                            echo $item->pf_operadora->nome . " - " . $item->pf_operadora->detalhe;
                        }
                        ?></td>
                    <td class="actions">
                        <?php if ($sessao['role'] == 'admin') { ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/pfFormasPagamentos/edit/$item->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'pfFormasPagamentos.edit']) ?>

                            <?= $this->Form->postLink('', "/pfFormasPagamentos/delete/$item->id", ['confirm' => __('Confirma exclusão da Carência?', $item->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'pfFormasPagamentos.delete'])
                            ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>