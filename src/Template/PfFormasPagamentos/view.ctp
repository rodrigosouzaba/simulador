<div class="pfFormasPagamentos view large-9 medium-8 columns content">
    <h3><?= h($pfFormasPagamento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($pfFormasPagamento->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Operadora') ?></th>
            <td><?= $pfFormasPagamento->has('pf_operadora') ? $this->Html->link($pfFormasPagamento->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfFormasPagamento->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfFormasPagamento->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($pfFormasPagamento->descricao)); ?>
    </div>
</div>
