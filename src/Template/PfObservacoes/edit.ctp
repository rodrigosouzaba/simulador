
<div class="pfObservacoes form large-9 medium-8 columns content">
    <?= $this->Form->create($pfObservaco) ?>
    <fieldset>
        <legend><?= __('Edit Pf Observaco') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('pf_operadora_id', ['options' => $pfOperadoras, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
