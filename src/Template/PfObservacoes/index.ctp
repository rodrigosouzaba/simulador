<div class="col-md-3 fonteReduzida">
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Observações PF',  '/pfObservacoes/add', ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'pfObservacoes.add']); ?>
</div>

<?= $this->element('filtro_interno') ?>

<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('descricao', ['label' => 'Descrição']) ?></th>
            <th><?= $this->Paginator->sort('pf_operadora_id', ['label' => 'Operadora']) ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($observacoes as $observacao) : ?>
            <tr>
                <td><?= h($observacao->nome) ?></td>
                <td><?= h($observacao->descricao) ?></td>
                <td>
                    <?php if (isset($observacao->pf_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $observacao->pf_operadora->imagen->caminho . "/" . $observacao->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $observacao->pf_operadora->detalhe;
                    } else {
                        echo $observacao->pf_operadora->nome . " - " . $observacao->pf_operadora->detalhe;
                    }
                    ?>
                </td>
                <td class="actions">
                    <?php
                    if ($sessao['role'] == 'admin') {
                        echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), "/pfObservacoes/edit/$observacao->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar observacao']);
                        echo $this->Form->postLink('', "/pfObservacoes/delete/$observacao->id", ['confirm' => __('Confirma exclusão do observacao?', $observacao->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash']);
                    }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>