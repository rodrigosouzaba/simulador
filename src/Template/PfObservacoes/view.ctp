<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Observaco'), ['action' => 'edit', $pfObservaco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Observaco'), ['action' => 'delete', $pfObservaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfObservaco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Observacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Observaco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfObservacoes view large-9 medium-8 columns content">
    <h3><?= h($pfObservaco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($pfObservaco->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Operadora') ?></th>
            <td><?= $pfObservaco->has('pf_operadora') ? $this->Html->link($pfObservaco->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfObservaco->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfObservaco->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($pfObservaco->descricao)); ?>
    </div>
</div>
