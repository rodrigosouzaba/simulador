
<?= $this->Form->create($pfOperadora) ?>
<div class="col-md-12">
    <?php
    echo $this->Form->input('nome');
    echo $this->Form->input('detalhe');
    echo $this->Form->input('prioridade');
    echo $this->Form->input('url', ['label' => 'Link Corretor Parceiro']);
    echo $this->Form->input('url_rede', ['label' => 'URL Rede Credenciada Completa']);
    echo $this->Form->input('estado_id',['options' => $estados, 'empty' => 'SELECIONE']);
    ?>
</div>
<div class="col-xs-12  fonteReduzida" id="entidades">
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-5">
        <?= $this->Form->input('lista_entidades', ['options' => $pf_entidades, 'multiple' => 'multiple', 'label' => 'Entidades Cadastradas', 'style' => 'margin-bottom: 0 !important']); ?>
    </div>

    <div class="col-md-2 centralizada">
        <div class="clearfix">&nbsp;</div>
        <div class="clearfix">&nbsp;</div>
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-arrow-right', 'aria-hidden' => 'true']), '#sel_prof', ['class' => 'add btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'title' => 'Adicionar']);
        ?>
        <br/>
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-arrow-left', 'aria-hidden' => 'true']), '#sel_prof', ['class' => 'remove btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'title' => 'Remover']);
        ?>

    </div>


    <div class="col-md-5">
        <?= $this->Form->input('lista_entidades_escolhidas', ['options' => '', 'multiple' => 'multiple', 'label' => 'Entidades Escolhidas', 'style' => 'margin-bottom: 0 !important']); ?>
    </div>
    <div id="sel_prof" style="clear:both"></div>
</div>

<div class="clearfix">&nbsp;</div>
<div class="col-md-12 centralizada">
    <?= $this->element('botoesAdd'); ?>
</div>
<?= $this->Form->end() ?>
<script type="text/javascript">
	 $('.add').click(function () {
            $('#lista-entidades option:selected').appendTo('#lista-entidades-escolhidas');
        });
        $('.remove').click(function () {
            $('#lista-entidades-escolhidas option:selected').appendTo('#lista-entidades');
        });
        $('.add-all').click(function () {
            $('#lista-entidades option').appendTo('#lista-entidades-escolhidas');
        });
        $('.remove-all').click(function () {
            $('#lista-entidades-escolhidas option').appendTo('#lista-entidades');
        });

	</script>