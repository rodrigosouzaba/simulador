<?= $this->Form->create($pfOperadora) ?>
<?= $this->Form->hidden('nova', ['value' => 1]); ?>
<div class="container">
    <div class="col-md-4 col-xs-12">
        <?= $this->Form->input('nome') ?>
    </div>
    <div class="col-md-8 col-xs-12">
        <?= $this->Form->input('detalhe') ?>
    </div>
    <div class="col-md-4 col-xs-12">
        <?= $this->Form->input('prioridade') ?>
    </div>
    <div class="col-md-4 col-xs-12">
        <?= $this->Form->input('url', ['label' => 'Link Corretor Parceiro']) ?>
    </div>
    <div class="col-md-4 col-xs-12">
        <?= $this->Form->input('url_rede', ['label' => 'URL Rede Credenciada Completa']) ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('atendimento_cliente', ['label' => 'Telefone de Atendimento ao Cliente', 'placeholder' => "(71) 3333-3333 ou (71) 9 8888-8888"]); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('atendimento_corretor', ['label' => 'Telefone de Atendimento ao Corretor', 'placeholder' => "(71) 3333-3333 ou (71) 9 8888-8888"]); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('site', ['label' => 'Site Operadora']); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('via_boleto', ['label' => 'Url Segunda Via do Boleto']); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('app_android', ['label' => 'Url Aplicativo Android']); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?= $this->Form->input('app_ios', ['label' => 'Url Aplicativo IOS']); ?>
    </div>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-12 centralizada">
    <?= $this->element('botoesAdd'); ?>
</div>
<?= $this->Form->end() ?>