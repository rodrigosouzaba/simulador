<?= $this->Form->create($pfOperadora) ?>
<div class="col-md-12">
    <?php
    echo $this->Form->input('nome');
    echo $this->Form->input('detalhe');
    echo $this->Form->input('prioridade');
    echo $this->Form->input('url', ['label' => 'Link Corretor Parceiro']);
    echo $this->Form->input('url_rede', ['label' => 'URL Rede Credenciada Completa']);
    echo $this->Form->input('estado_id', ['options' => $estados, 'empty' => 'SELECIONE']);
    ?>
</div>
<div class="spacer-md">&nbsp</div>
<div class="col-xs-12  fonteReduzida" id="entidades">
    <?= $this->Form->select('lista_entidades_escolhidas',  $pf_entidades, ['value' => $pf_entidades_associadas->toArray(), 'multiple' => true, 'label' => 'Entidades Escolhidas', 'style' => 'margin-bottom: 0 !important']); ?>
</div>

<div class="clearfix">&nbsp;</div>
<div class="col-md-12 centralizada">
    <?= $this->element('botoesAdd') ?>
</div>
<?= $this->Form->end() ?>