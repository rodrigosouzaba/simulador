<?php
if (!empty($pfOperadoras->toArray())) { ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Html->link('Prioridade', '#', ['onclick' => 'order("prioridade", "operadoras")']) ?></th>
                <?php if (!$new) : ?>
                    <th><?= $this->Html->link('Estado', '#', ['onclick' => 'order("estado_id", "operadoras")']) ?></th>
                <?php endif; ?>

                <th><?= $this->Html->link('Nome', '#', ['onclick' => 'order("nome", "operadoras")']) ?></th>

                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfOperadoras as $pfOperadora) : ?>
                <tr>
                    <td> <?= $pfOperadora->prioridade ?></td>
                    <?php if (!$new) : ?>
                        <td><?= $pfOperadora->estado->nome ?></td>
                    <?php endif; ?>
                    <td>
                        <?php
                        if (isset($pfOperadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $pfOperadora->imagen->caminho . "/" . $pfOperadora->imagen->nome, ['class' => 'logoMiniatura']) . " " . $pfOperadora->nome . "  " . $pfOperadora->detalhe;
                        } else {
                        ?>
                            <?= $pfOperadora->nome . " - " . $pfOperadora->detalhe ?>
                    </td>


                <?php }
                ?>
                <td class="actions">
                    <?=
                    $this->Html->link('', ['action' => 'duplicar', $pfOperadora->id], [
                        'class' => 'btn-sm btn btn-default fa fa-copy',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Duplicar Operadora'
                    ])
                    ?>
                    <?=
                    $this->Html->link('', ['action' => 'addImagem', $pfOperadora->id], [
                        'title' => __('Adicionar Logomarca'),
                        'class' => 'btn-sm btn btn-default fa fa-image',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Logomarca'
                    ])
                    ?>
                    <?=
                    $this->Html->link('', ['action' => 'edit', $pfOperadora->id], [
                        'title' => __('Editar'),
                        'class' => 'btn-sm btn btn-default fa fa-pencil',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Editar'
                    ])
                    ?>
                    <span data-toggle="tooltip" data-placement="top" data-original-title="Prioridade" title="Definir Prioridade">
                        <?=
                        $this->Html->link('', '#', [
                            'title' => __('Prioridade'),
                            'class' => 'btn-sm btn btn-default fa fa-list alterarPrioridade',
                            'role' => 'button',
                            'escape' => false,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Alterar Prioridade',
                            'id' => 'reajustarVigencia',
                            'data-toggle' => "modal",
                            'data-target' => "#modalPrioridade",
                            'value' => $pfOperadora->id
                        ])
                        ?>
                    </span>
                    <!-- <?=
                            $this->Form->postLink('', ['action' => 'delete', $pfOperadora->id], [
                                'confirm' => __('Tem certeza que deseja excluir Operadora {0}?', $pfOperadora->nome . "  " . $pfOperadora->detalhes), 'title' => __('Deletar'),
                                'class' => 'btn-sm btn btn-default fa fa-trash',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Deletar'
                            ])
                            ?> -->
                    <?php switch ($pfOperadora->status):
                        case 'OCULTA': ?>

                            <?=
                            $this->Html->link('', ['action' => 'ocultar', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-danger fa fa-eye',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Reexibir Operadora'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'atualizacao', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-refresh',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Colocar em Atualização de Preços'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'ativacao', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-check',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Inativar Operadora'
                            ])
                            ?>
                        <?php break;

                        case 'EM ATUALIZAÇÃO': ?>
                            <?=
                            $this->Html->link('', ['action' => 'ocultar', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-eye',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Ocultar Operadora'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'atualizacao', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-danger fa fa-refresh',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Remover Atualização de Preços'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'ativacao', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-check',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Inativar Operadora'
                            ])
                            ?>
                        <?php break;

                        case 'INATIVA': ?>
                            <?=
                            $this->Html->link('', ['action' => 'ocultar', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-eye',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Ocultar Operadora'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'atualizacao', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-refresh',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Colocar em Atualização de Preços'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'ativacao', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-danger fa fa-close',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Ativar Operadora'
                            ])
                            ?>
                        <?php break;

                        default: ?>
                            <?=
                            $this->Html->link('', ['action' => 'ocultar', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-eye',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Ocultar Operadora'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'atualizacao', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-refresh',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Colocar em Atualização de Preços'
                            ])
                            ?>
                            <?=
                            $this->Html->link('', ['action' => 'ativacao', $pfOperadora->id], [
                                'class' => 'btn btn-sm btn-default fa fa-check',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Inativar Operadora'
                            ])
                            ?>
                    <?php break;
                    endswitch;
                    ?>
                </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php } else { ?>
    <div class="col-xs-12 text-danger centralizada">NENHUMA OPERADORA NESTE ESTADO</div>
<?php } ?>