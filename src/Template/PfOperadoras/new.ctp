<div id="ajax">

    <div id="fake-form" class="col-xs-12" style="padding-top: 15px;">
        <div class="col-xs-3">
            <?php if ($this->request->action) : ?>
                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Operadora', ['action' => 'add', 1], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addOperadora']); ?>
            <?php else : ?>
                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Operadora', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addOperadora']); ?>
            <?php endif; ?>
        </div>
        <div class="col-xs-2 flex">
            <?= $this->Form->input('search', ['placeholder' => 'Digite a operadora', 'label' => false, 'list' => 'lista-operadoras', 'style' => 'width: 90%;', 'templates' => ['inputContainer' => '{{content}}']]) ?>
            <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'fa fa-search']), ['id' => 'search-button', 'class' => 'search-button', 'style' => 'width: 20%']); ?>
            <datalist id="lista-operadoras">
                <?php foreach ($operadoras as $operadora) : ?>
                    <option value="<?= $operadora->nome ?>">
                    <?php endforeach; ?>
            </datalist>
        </div>
        <div class="col-xs-2">
            <?= $this->Form->input('estado_id', ['options' => $estados, "class" => "fonteReduzida", 'label' => false, 'empty' => 'Selecione ESTADO']); ?>
        </div>
        <div class="col-xs-2">
            <?= $this->Form->input('status', ['options' => ['ATIVA' => 'ATIVAS', 'EM ATUALIZACAO' => 'ATUALIZAÇÃO', 'INATIVA' => 'INATIVAS', 'OCULTA' => 'OCULTAS',], "class" => "fonteReduzida", 'label' => false, 'empty' => 'Selecione STATUS']); ?>
        </div>
    </div>
    <?= $this->element('order') ?>
    <div id="operadoras">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?= $this->Html->link('Prioridade', '#', ['onclick' => 'order("prioridade", "operadoras")']) ?></th>
                    <?php if ($this->request->action != 'new') : ?>
                        <th><?= $this->Html->link('Estado', '#', ['onclick' => 'order("estado_id", "operadoras")']) ?></th>
                    <?php endif; ?>
                    <th><?= $this->Html->link('Nome', '#', ['onclick' => 'order("nome", "operadoras")']) ?></th>
                    <th class="actions"><?= 'Ações' ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($pfOperadoras as $pfOperadora) : ?>
                    <tr>
                        <td><?= $this->Number->format($pfOperadora->prioridade) ?></td>
                        <?php if ($this->request->action != 'new') : ?>
                            <td><?= $pfOperadora->estado->nome ?></td>
                        <?php endif; ?>
                        <td>
                            <?php
                            if (isset($pfOperadora->imagen->caminho)) {
                                echo $this->Html->image("../" . $pfOperadora->imagen->caminho . "/" . $pfOperadora->imagen->nome, ['class' => 'logoMiniatura']) . " " . $pfOperadora->nome . "  " . $pfOperadora->detalhe;
                            } else {
                            ?>
                                <?= $pfOperadora->nome . "  " . $pfOperadora->detalhe ?>
                        </td>


                    <?php }
                    ?>
                    <td class="actions">
                        <?=
                        $this->Html->link('', '/pfOperadoras/duplicar/' . $pfOperadora->id, [
                            'class' => 'btn-sm btn btn-default fa fa-copy',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'sid' => 'pfOperadoras.duplicar',
                            'title' => 'Duplicar Operadora'
                        ])
                        ?>
                        <?=
                        $this->Html->link('', '/pfOperadoras/addImagem/' . $pfOperadora->id, [
                            'title' => __('Adicionar Logomarca'),
                            'class' => 'btn-sm btn btn-default fa fa-image',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'sid' => 'pfOperadoras.addImagem',
                            'title' => 'Logomarca'
                        ])
                        ?>
                        <?=
                        $this->Html->link('', '/pfOperadoras/edit/' . $pfOperadora->id . '/1', [
                            'title' => __('Editar'),
                            'class' => 'btn-sm btn btn-default fa fa-pencil',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'sid' => 'pfOperadoras.edit',
                            'title' => 'Editar'
                        ])
                        ?>
                        <span data-toggle="tooltip" data-placement="top" data-original-title="Prioridade" title="Definir Prioridade">
                            <?=
                            $this->Html->link('', '#', [
                                'title' => __('Prioridade'),
                                'class' => 'btn-sm btn btn-default fa fa-list alterarPrioridade',
                                'role' => 'button',
                                'escape' => false,
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'title' => 'Alterar Prioridade',
                                'id' => 'reajustarVigencia',
                                'data-toggle' => "modal",
                                'data-target' => "#modalPrioridade",
                                'value' => $pfOperadora->id,
                                'sid' => 'pfOperadoras.prioridade',
                            ])
                            ?>
                        </span>
                        <!-- <?=
                                $this->Form->postLink('', '/pfOperadoras/delete/' . $pfOperadora->id, [
                                    'confirm' => __('Tem certeza que deseja excluir Operadora {0}?', $pfOperadora->nome . "  " . $pfOperadora->detalhes), 'title' => __('Deletar'),
                                    'class' => 'btn-sm btn btn-default fa fa-trash',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'title' => 'Deletar',
                                    'sid' => 'pfOperadoras.delete',
                                ])
                                ?> -->
                        <?php switch ($pfOperadora->status):
                            case 'OCULTA': ?>

                                <?=
                                $this->Html->link('', '/pfOperadoras/ocultar/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-danger fa fa-eye',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.ocultar',
                                    'title' => 'Reexibir Operadora'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/atualizacao/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-refresh',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.atualizacao',
                                    'title' => 'Colocar em Atualização de Preços'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/ativacao/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-check',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.ativacao',
                                    'title' => 'Inativar Operadora'
                                ])
                                ?>
                            <?php break;

                            case 'EM ATUALIZAÇÃO': ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/ocultar/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-eye',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.ocultar',
                                    'title' => 'Ocultar Operadora'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/atualizacao/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-danger fa fa-refresh',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.atualizacao',
                                    'title' => 'Remover Atualização de Preços'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/ativacao/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-check',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.ativacao',
                                    'title' => 'Inativar Operadora'
                                ])
                                ?>
                            <?php break;

                            case 'INATIVA': ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/ocultar/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-eye',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.ocultar',
                                    'title' => 'Ocultar Operadora'
                                ])
                                ?>
                                <?=
                                $this->Html->link('',  '/pfOperadoras/atualizacao/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-refresh',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.atualizacao',
                                    'title' => 'Colocar em Atualização de Preços'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/ativacao/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-danger fa fa-close',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.ativacao',
                                    'title' => 'Ativar Operadora'
                                ])
                                ?>
                            <?php break;

                            default: ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/ocultar/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-eye',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.ocultar',
                                    'title' => 'Ocultar Operadora'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/atualizacao/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-refresh',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.atualizacao',
                                    'title' => 'Colocar em Atualização de Preços'
                                ])
                                ?>
                                <?=
                                $this->Html->link('', '/pfOperadoras/ativacao/' . $pfOperadora->id, [
                                    'class' => 'btn btn-sm btn-default fa fa-check',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'sid' => 'pfOperadoras.ativacao',
                                    'title' => 'Inativar Operadora'
                                ])
                                ?>
                        <?php break;
                        endswitch;
                        ?>
                    </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalPrioridade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Alterar Ordem de Prioridade da Operadora</h4>
                <small> As Operadoras serão exibidas nas simulações de acordo com sua <b>Ordem de Prioridade</b>.</small>
            </div>
            <div class="modal-body" id="prioridade" style="min-height: 150px !important;">
                <div class="form-inline col-xs-6 col-xs-offset-1 text-center">
                    <?= $this->Form->create(null, ['id' => 'form-prioridade', 'url' => ['action' => 'prioridade']]) ?>
                    <?= $this->Form->hidden('operadora_id', ['value' => '', 'label' => false, 'id' => 'operadora-id']) ?>
                    <div class="form-group">
                        <?= $this->Form->input('prioridade', ['label' => 'Ordem de Prioridade', 'label' => false, 'type' => 'number', 'style' => 'margin: 0;']) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <div class="col-xs-3">
                    <?= $this->Form->button('Validar', ['class' => 'btn btn-sm btn-default col-xs-12', 'id' => 'btn-validar', 'style' => 'height: 37px']) ?>
                </div>
                <span id="text-prioridade" class="text-danger small col-xs-8 col-xs-offset-2 text-center"></span>
                <?= $this->Form->button('Salvar', ['class' => 'btn btn-success col-xs-4 col-xs-offset-4', 'id' => 'btn-priorizar', 'disabled' => true, 'style' => 'margin-top: 15px;']) ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<script type="text/javascript">
    function filtrar() {
        let texto = $("#search").val();
        let estado = $("#estado-id").val();
        let status = $("#status").val();
        if (texto !== "" || estado !== "" || status !== "") {
            $.ajax({
                type: 'POST',
                url: baseUrl + '/pfOperadoras/filtro/1',
                data: {
                    'search': texto,
                    'estado': estado,
                    'status': status
                },
                success: function(data) {
                    $("#operadoras").empty();
                    $("#operadoras").append(data);
                }
            });
        }
    }

    $("#search-button").click(function() {
        filtrar()
    });

    $("#search").keyup(function(e) {
        if (e.which == 13) {
            filtrar()
        }
    });

    $("#estado-id").change(function() {
        filtrar()
    });

    $("#status").change(function() {
        filtrar()
    });

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('#modalPrioridade').on('shown.bs.modal', function() {
            $('#myInput').focus()
        });
    });

    $(".alterarPrioridade").click(function() {
        let operadora_id = $(this).attr('value');
        $("#form-prioridade > #operadora-id").empty();
        $("#form-prioridade > #operadora-id").val(operadora_id);
    });

    $("#btn-validar").click(function() {
        let form = $("#form-prioridade").serialize();
        $.ajax({
            type: 'POST',
            data: form,
            url: baseUrl + '/pfOperadoras/validacaoPrioridade',
            beforeSend: function() {
                $("#text-prioridade").empty();
                $("#text-prioridade").append('Validando...');
            },
            success: function(data) {
                if (data.result) {
                    $("#btn-priorizar").removeAttr('disabled');
                    $("#text-prioridade").empty();
                    $("#text-prioridade").append('Não há operadora com esta prioridade.');
                } else {
                    $("#btn-priorizar").attr('disabled', 'disabled');
                    $("#text-prioridade").empty();
                    $("#text-prioridade").append('Esta prioridade já está em uso.');
                }
            }
        });
    });

    $("#btn-priorizar").click(function() {
        $("#form-prioridade").submit()
    });
</script>