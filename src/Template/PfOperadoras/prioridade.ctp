<?= $this->Form->create('operadora', ['id' => 'operadoraForm']); ?>
<div class="col-md-8 fonteReduzida">    
    <?= $this->Form->input('operadora_id', ['type' => 'hidden', 'value' => $operadora_id]) ?>
    <?= $this->Form->input('prioridade', ['label' => 'Ordem de Prioridade', "id" => "valor_prioridade"]); ?>
</div>
<div class="col-md-4 fonteReduzida centralizada" style="margin-top:30px;">
    <?=
    $this->Html->link($this->Html->tag('span', '', [
                'class' => 'fa fa-floppy-o',
                'aria-hidden' => 'true']) . ' Salvar', '#', [
        'class' => 'btn btn-primary btn-sm',
        'role' => 'button',
        'id' => 'salvarPrioridade',
        'escape' => false
    ]);
    ?>

    <button type="button" id="fecharModal" class="btn btn-sm btn-default" data-dismiss="modal"> 
        <span class="fa fa-remove" aria-hidden="true"></span> Cancelar
    </button>
</div>

<?= $this->element("processando");?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#salvarPrioridade").click(function () {
	        $("#loader").click();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>pf-operadoras/prioridade/" + $("#operadora-id").val() +"/"+$("#valor_prioridade").val(),
                beforeSend: function () {
                    $('#salvarPrioridade').attr("disabled", true);
                },
                success: function (data) {
                    location.reload();
                    $('#fechar').trigger('click');
                    $('#fecharModal').trigger('click');
                }
            });
        });
    });
</script>