<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Operadora'), ['action' => 'edit', $pfOperadora->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Operadora'), ['action' => 'delete', $pfOperadora->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfOperadora->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Imagens'), ['controller' => 'Imagens', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Imagen'), ['controller' => 'Imagens', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Carencias'), ['controller' => 'PfCarencias', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Carencia'), ['controller' => 'PfCarencias', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Comercializacoes'), ['controller' => 'PfComercializacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Comercializaco'), ['controller' => 'PfComercializacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Dependentes'), ['controller' => 'PfDependentes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Dependente'), ['controller' => 'PfDependentes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Documentos'), ['controller' => 'PfDocumentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Documento'), ['controller' => 'PfDocumentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Observacoes'), ['controller' => 'PfObservacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Observaco'), ['controller' => 'PfObservacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Produtos'), ['controller' => 'PfProdutos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Produto'), ['controller' => 'PfProdutos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Redes'), ['controller' => 'PfRedes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Rede'), ['controller' => 'PfRedes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Reembolsos'), ['controller' => 'PfReembolsos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Reembolso'), ['controller' => 'PfReembolsos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfOperadoras view large-9 medium-8 columns content">
    <h3><?= h($pfOperadora->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($pfOperadora->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Imagen') ?></th>
            <td><?= $pfOperadora->has('imagen') ? $this->Html->link($pfOperadora->imagen->id, ['controller' => 'Imagens', 'action' => 'view', $pfOperadora->imagen->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Detalhe') ?></th>
            <td><?= h($pfOperadora->detalhe) ?></td>
        </tr>
        <tr>
            <th><?= __('Url') ?></th>
            <td><?= h($pfOperadora->url) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfOperadora->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prioridade') ?></th>
            <td><?= $this->Number->format($pfOperadora->prioridade) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pf Carencias') ?></h4>
        <?php if (!empty($pfOperadora->pf_carencias)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfOperadora->pf_carencias as $pfCarencias): ?>
            <tr>
                <td><?= h($pfCarencias->id) ?></td>
                <td><?= h($pfCarencias->descricao) ?></td>
                <td><?= h($pfCarencias->nome) ?></td>
                <td><?= h($pfCarencias->pf_operadora_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfCarencias', 'action' => 'view', $pfCarencias->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfCarencias', 'action' => 'edit', $pfCarencias->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfCarencias', 'action' => 'delete', $pfCarencias->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfCarencias->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Comercializacoes') ?></h4>
        <?php if (!empty($pfOperadora->pf_comercializacoes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfOperadora->pf_comercializacoes as $pfComercializacoes): ?>
            <tr>
                <td><?= h($pfComercializacoes->id) ?></td>
                <td><?= h($pfComercializacoes->nome) ?></td>
                <td><?= h($pfComercializacoes->estado_id) ?></td>
                <td><?= h($pfComercializacoes->pf_operadora_id) ?></td>
                <td><?= h($pfComercializacoes->descricao) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfComercializacoes', 'action' => 'view', $pfComercializacoes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfComercializacoes', 'action' => 'edit', $pfComercializacoes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfComercializacoes', 'action' => 'delete', $pfComercializacoes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfComercializacoes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Dependentes') ?></h4>
        <?php if (!empty($pfOperadora->pf_dependentes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfOperadora->pf_dependentes as $pfDependentes): ?>
            <tr>
                <td><?= h($pfDependentes->id) ?></td>
                <td><?= h($pfDependentes->descricao) ?></td>
                <td><?= h($pfDependentes->nome) ?></td>
                <td><?= h($pfDependentes->pf_operadora_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfDependentes', 'action' => 'view', $pfDependentes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfDependentes', 'action' => 'edit', $pfDependentes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfDependentes', 'action' => 'delete', $pfDependentes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfDependentes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Documentos') ?></h4>
        <?php if (!empty($pfOperadora->pf_documentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfOperadora->pf_documentos as $pfDocumentos): ?>
            <tr>
                <td><?= h($pfDocumentos->id) ?></td>
                <td><?= h($pfDocumentos->descricao) ?></td>
                <td><?= h($pfDocumentos->nome) ?></td>
                <td><?= h($pfDocumentos->pf_operadora_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfDocumentos', 'action' => 'view', $pfDocumentos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfDocumentos', 'action' => 'edit', $pfDocumentos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfDocumentos', 'action' => 'delete', $pfDocumentos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfDocumentos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Observacoes') ?></h4>
        <?php if (!empty($pfOperadora->pf_observacoes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfOperadora->pf_observacoes as $pfObservacoes): ?>
            <tr>
                <td><?= h($pfObservacoes->id) ?></td>
                <td><?= h($pfObservacoes->descricao) ?></td>
                <td><?= h($pfObservacoes->nome) ?></td>
                <td><?= h($pfObservacoes->pf_operadora_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfObservacoes', 'action' => 'view', $pfObservacoes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfObservacoes', 'action' => 'edit', $pfObservacoes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfObservacoes', 'action' => 'delete', $pfObservacoes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfObservacoes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Produtos') ?></h4>
        <?php if (!empty($pfOperadora->pf_produtos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th><?= __('Pf Carencia Id') ?></th>
                <th><?= __('Pf Rede Id') ?></th>
                <th><?= __('Pf Reembolso Id') ?></th>
                <th><?= __('Pf Documento Id') ?></th>
                <th><?= __('Pf Dependente Id') ?></th>
                <th><?= __('Pf Observacao Id') ?></th>
                <th><?= __('Tipo Produto Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfOperadora->pf_produtos as $pfProdutos): ?>
            <tr>
                <td><?= h($pfProdutos->id) ?></td>
                <td><?= h($pfProdutos->nome) ?></td>
                <td><?= h($pfProdutos->descricao) ?></td>
                <td><?= h($pfProdutos->pf_operadora_id) ?></td>
                <td><?= h($pfProdutos->pf_carencia_id) ?></td>
                <td><?= h($pfProdutos->pf_rede_id) ?></td>
                <td><?= h($pfProdutos->pf_reembolso_id) ?></td>
                <td><?= h($pfProdutos->pf_documento_id) ?></td>
                <td><?= h($pfProdutos->pf_dependente_id) ?></td>
                <td><?= h($pfProdutos->pf_observacao_id) ?></td>
                <td><?= h($pfProdutos->tipo_produto_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfProdutos', 'action' => 'view', $pfProdutos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfProdutos', 'action' => 'edit', $pfProdutos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfProdutos', 'action' => 'delete', $pfProdutos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfProdutos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Redes') ?></h4>
        <?php if (!empty($pfOperadora->pf_redes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfOperadora->pf_redes as $pfRedes): ?>
            <tr>
                <td><?= h($pfRedes->id) ?></td>
                <td><?= h($pfRedes->descricao) ?></td>
                <td><?= h($pfRedes->nome) ?></td>
                <td><?= h($pfRedes->pf_operadora_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfRedes', 'action' => 'view', $pfRedes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfRedes', 'action' => 'edit', $pfRedes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfRedes', 'action' => 'delete', $pfRedes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfRedes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Reembolsos') ?></h4>
        <?php if (!empty($pfOperadora->pf_reembolsos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfOperadora->pf_reembolsos as $pfReembolsos): ?>
            <tr>
                <td><?= h($pfReembolsos->id) ?></td>
                <td><?= h($pfReembolsos->descricao) ?></td>
                <td><?= h($pfReembolsos->nome) ?></td>
                <td><?= h($pfReembolsos->pf_operadora_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfReembolsos', 'action' => 'view', $pfReembolsos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfReembolsos', 'action' => 'edit', $pfReembolsos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfReembolsos', 'action' => 'delete', $pfReembolsos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfReembolsos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Tabelas') ?></h4>
        <?php if (!empty($pfOperadora->pf_tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Vigencia') ?></th>
                <th><?= __('Faixa1') ?></th>
                <th><?= __('Faixa2') ?></th>
                <th><?= __('Faixa3') ?></th>
                <th><?= __('Faixa4') ?></th>
                <th><?= __('Faixa5') ?></th>
                <th><?= __('Faixa6') ?></th>
                <th><?= __('Faixa7') ?></th>
                <th><?= __('Faixa8') ?></th>
                <th><?= __('Faixa9') ?></th>
                <th><?= __('Faixa10') ?></th>
                <th><?= __('Faixa11') ?></th>
                <th><?= __('Faixa12') ?></th>
                <th><?= __('Pf Produto Id') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th><?= __('Pf Atendimento Id') ?></th>
                <th><?= __('Pf Acomodacao Id') ?></th>
                <th><?= __('Validade') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Pf Comercializacao Id') ?></th>
                <th><?= __('Cod Ans') ?></th>
                <th><?= __('Minimo Vidas') ?></th>
                <th><?= __('Maximo Vidas') ?></th>
                <th><?= __('Prioridade') ?></th>
                <th><?= __('Coparticipacao') ?></th>
                <th><?= __('Detalhe Coparticipacao') ?></th>
                <th><?= __('Modalidade') ?></th>
                <th><?= __('Reembolso') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfOperadora->pf_tabelas as $pfTabelas): ?>
            <tr>
                <td><?= h($pfTabelas->id) ?></td>
                <td><?= h($pfTabelas->nome) ?></td>
                <td><?= h($pfTabelas->descricao) ?></td>
                <td><?= h($pfTabelas->vigencia) ?></td>
                <td><?= h($pfTabelas->faixa1) ?></td>
                <td><?= h($pfTabelas->faixa2) ?></td>
                <td><?= h($pfTabelas->faixa3) ?></td>
                <td><?= h($pfTabelas->faixa4) ?></td>
                <td><?= h($pfTabelas->faixa5) ?></td>
                <td><?= h($pfTabelas->faixa6) ?></td>
                <td><?= h($pfTabelas->faixa7) ?></td>
                <td><?= h($pfTabelas->faixa8) ?></td>
                <td><?= h($pfTabelas->faixa9) ?></td>
                <td><?= h($pfTabelas->faixa10) ?></td>
                <td><?= h($pfTabelas->faixa11) ?></td>
                <td><?= h($pfTabelas->faixa12) ?></td>
                <td><?= h($pfTabelas->pf_produto_id) ?></td>
                <td><?= h($pfTabelas->pf_operadora_id) ?></td>
                <td><?= h($pfTabelas->pf_atendimento_id) ?></td>
                <td><?= h($pfTabelas->pf_acomodacao_id) ?></td>
                <td><?= h($pfTabelas->validade) ?></td>
                <td><?= h($pfTabelas->estado_id) ?></td>
                <td><?= h($pfTabelas->pf_comercializacao_id) ?></td>
                <td><?= h($pfTabelas->cod_ans) ?></td>
                <td><?= h($pfTabelas->minimo_vidas) ?></td>
                <td><?= h($pfTabelas->maximo_vidas) ?></td>
                <td><?= h($pfTabelas->prioridade) ?></td>
                <td><?= h($pfTabelas->coparticipacao) ?></td>
                <td><?= h($pfTabelas->detalhe_coparticipacao) ?></td>
                <td><?= h($pfTabelas->modalidade) ?></td>
                <td><?= h($pfTabelas->reembolso) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfTabelas', 'action' => 'view', $pfTabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfTabelas', 'action' => 'edit', $pfTabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfTabelas', 'action' => 'delete', $pfTabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfTabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
