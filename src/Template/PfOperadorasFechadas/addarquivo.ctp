<div class="modal-body">
	  	<?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>
        <?php echo $this->Form->input('operadora_fechada_id', ['type' => 'hidden', 'value' => $operadora_fechada['id']]); ?>
        
        
        <div class="upload-frm col-xs-12">
        
        <?php echo $this->Form->input('file', ['type' => 'file', 'label' => '', 'class' => 'centralizada']); ?>
        </div>
        
        <div class="col-xs-12">
	        <?= $this->Form->input("tipo"); ?>
        </div>
        
        <div class="upload-frm centralizada">
        <?php echo $this->Form->button(__('Enviar'), ['type' => 'submit', 'class' => 'btn btn-md btn-primary']); ?>
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ["#"], [
        'class' => 'btn btn-md btn-default', 
        'data-dismiss'=>"modal", 
        'role' => 'button', 
        'escape' => false]);
        ?>
 		</div>
        <?php echo $this->Form->end(); ?>
       </div>
<!--
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar</button>
      </div>
-->