    <div class="col-xs-12">
        <h3 class="centralizada well well-sm col-xs-12"><?php echo $pf_operadora_fechada['nome'] ?></h3>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="panel panel-default">
            <h4 class="panel-heading col-xs-12">Arquivos Associados</h4>

            <div class="panel-body">

                <?php if (isset($pf_operadora_fechada['arquivos']) && $pf_operadora_fechada['arquivos'] <> null) { ?>
	                <div class="col-md-2 negrito"> Tipo do Arquivo</div>
	            <div class="col-md-3 negrito"> Nome botão</div>
	            <div class="col-md-4 negrito"> Nome arquivo</div>
	            <div class="col-md-3 negrito" > Data Upload</div>
	            <div class="col-md-12" style="border-bottom: 1px solid #ddd; margin: 2px;"></div>


                <?php    foreach($pf_operadora_fechada['arquivos'] as $arquivo){ ?>

                <div class="col-md-2">
                    <?php echo ucwords($arquivo['tipo']);?>

                </div>
				<div class="col-md-3">
					<?php echo $arquivo['exibicao_nome'];?>
				</div>
                <div class="col-md-4">
                    <?php echo $this->Html->link($arquivo['nome'], "/".$arquivo["caminho"].$arquivo['nome'], ['target' => "_blank"]);?>
                </div>

                <div class="col-md-3">
                    <?php echo $arquivo['created']." ";?><?php echo $this->Html->link("Excluir", "#", ["class" => "removerArquivo", "value" => $arquivo['id']]);?>
                </div>
                <?php
	}
} else { ?>

                <div class="col-xs-12 aviso">
                    Nenhum arquivo Associado
                </div><?php } ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="panel panel-default">
            <h4 class="panel-heading col-xs-12">Associar novos Arquivos</h4>

            <div class="panel-body">
                <?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>

                <div class="col-xs-12">
                    <?php echo $this->Form->input('pf_operadora_fechada_id', ['type' => 'hidden', 'value' => $pf_operadora_fechada['id']]); ?>
                </div>

                <div class="col-xs-6">
                    <?php echo $this->Form->input('tipo', ['options' => $tipos, 'empty' => "SELECIONE", "label" => "Tipo de Arquivo"]); ?>
                </div>

                <div class="col-xs-6 centralizada">
                    <b>Selecione o Arquivo</b> <?php echo $this->Form->input('file', ['type' => 'file', 'label' => '', 'class' => 'centralizada', "style" => "margin-top: 10px;"]); ?>
                </div>

                <div class="clearfix">
                    &nbsp;
                </div>
                <div class="col-xs-12">
                	<?php echo $this->Form->input('exibicao_nome', ["label" => "Nome a exibido do arquivo no botão de Download"]); ?>
                </div>

                <div class="centralizada col-xs-12">
                    <?php echo $this->Form->button("Adicionar Arquivo", ['type' => 'submit', 'class' => 'btn btn-sm btn-primary']); ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
<script type="text/javascript">
$(".removerArquivo").click(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>pf-operadoras-fechadas/remover_arquivo/" + $(this).attr('value'),
                success: function (data) {
    /*
                $("#renderView").empty();
                $("#renderView").append(data);
    */
                window.location = '<?php echo $this->request->webroot."pf-operadoras-fechadas/arquivos/".$pf_operadora_fechada['id']?>';
            }

        });
    });

</script>