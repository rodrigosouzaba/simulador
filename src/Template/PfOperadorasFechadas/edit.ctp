<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfOperadorasFechada->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfOperadorasFechada->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Operadoras Fechadas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfOperadorasFechadas form large-9 medium-8 columns content">
    <?= $this->Form->create($pfOperadorasFechada) ?>
    <fieldset>
        <legend><?= __('Edit Pf Operadoras Fechada') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('arquivos._ids', ['options' => $arquivos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
