<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pf Operadoras Fechadas Arquivos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Operadoras Fechadas'), ['controller' => 'PfOperadorasFechadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Operadoras Fechada'), ['controller' => 'PfOperadorasFechadas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfOperadorasFechadasArquivos form large-9 medium-8 columns content">
    <?= $this->Form->create($pfOperadorasFechadasArquivo) ?>
    <fieldset>
        <legend><?= __('Add Pf Operadoras Fechadas Arquivo') ?></legend>
        <?php
            echo $this->Form->input('pf_operadora_fechada_id', ['options' => $pfOperadorasFechadas]);
            echo $this->Form->input('arquivo_id', ['options' => $arquivos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
