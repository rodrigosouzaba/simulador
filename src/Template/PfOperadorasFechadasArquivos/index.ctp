<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pf Operadoras Fechadas Arquivo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Operadoras Fechadas'), ['controller' => 'PfOperadorasFechadas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Operadoras Fechada'), ['controller' => 'PfOperadorasFechadas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfOperadorasFechadasArquivos index large-9 medium-8 columns content">
    <h3><?= __('Pf Operadoras Fechadas Arquivos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_operadora_fechada_id') ?></th>
                <th><?= $this->Paginator->sort('arquivo_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfOperadorasFechadasArquivos as $pfOperadorasFechadasArquivo): ?>
            <tr>
                <td><?= $this->Number->format($pfOperadorasFechadasArquivo->id) ?></td>
                <td><?= $pfOperadorasFechadasArquivo->has('pf_operadoras_fechada') ? $this->Html->link($pfOperadorasFechadasArquivo->pf_operadoras_fechada->id, ['controller' => 'PfOperadorasFechadas', 'action' => 'view', $pfOperadorasFechadasArquivo->pf_operadoras_fechada->id]) : '' ?></td>
                <td><?= $pfOperadorasFechadasArquivo->has('arquivo') ? $this->Html->link($pfOperadorasFechadasArquivo->arquivo->id, ['controller' => 'Arquivos', 'action' => 'view', $pfOperadorasFechadasArquivo->arquivo->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pfOperadorasFechadasArquivo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfOperadorasFechadasArquivo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfOperadorasFechadasArquivo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfOperadorasFechadasArquivo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
