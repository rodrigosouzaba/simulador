<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Operadoras Fechadas Arquivo'), ['action' => 'edit', $pfOperadorasFechadasArquivo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Operadoras Fechadas Arquivo'), ['action' => 'delete', $pfOperadorasFechadasArquivo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfOperadorasFechadasArquivo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras Fechadas Arquivos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadoras Fechadas Arquivo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras Fechadas'), ['controller' => 'PfOperadorasFechadas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadoras Fechada'), ['controller' => 'PfOperadorasFechadas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Arquivos'), ['controller' => 'Arquivos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Arquivo'), ['controller' => 'Arquivos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfOperadorasFechadasArquivos view large-9 medium-8 columns content">
    <h3><?= h($pfOperadorasFechadasArquivo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Operadoras Fechada') ?></th>
            <td><?= $pfOperadorasFechadasArquivo->has('pf_operadoras_fechada') ? $this->Html->link($pfOperadorasFechadasArquivo->pf_operadoras_fechada->id, ['controller' => 'PfOperadorasFechadas', 'action' => 'view', $pfOperadorasFechadasArquivo->pf_operadoras_fechada->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Arquivo') ?></th>
            <td><?= $pfOperadorasFechadasArquivo->has('arquivo') ? $this->Html->link($pfOperadorasFechadasArquivo->arquivo->id, ['controller' => 'Arquivos', 'action' => 'view', $pfOperadorasFechadasArquivo->arquivo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfOperadorasFechadasArquivo->id) ?></td>
        </tr>
    </table>
</div>
