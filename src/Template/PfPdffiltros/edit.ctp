<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfPdffiltro->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfPdffiltro->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Pdffiltros'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['controller' => 'PfAtendimentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Atendimento'), ['controller' => 'PfAtendimentos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfPdffiltros form large-9 medium-8 columns content">
    <?= $this->Form->create($pfPdffiltro) ?>
    <fieldset>
        <legend><?= __('Edit Pf Pdffiltro') ?></legend>
        <?php
            echo $this->Form->input('pf_calculo_id', ['options' => $pfCalculos]);
            echo $this->Form->input('pf_atendimento_id', ['options' => $pfAtendimentos, 'empty' => true]);
            echo $this->Form->input('pf_acomodacao_id');
            echo $this->Form->input('tipo_produto_id');
            echo $this->Form->input('coparticipacao');
            echo $this->Form->input('filtroreembolso');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
