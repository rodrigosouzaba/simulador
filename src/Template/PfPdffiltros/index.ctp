<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pf Pdffiltro'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['controller' => 'PfAtendimentos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Atendimento'), ['controller' => 'PfAtendimentos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfPdffiltros index large-9 medium-8 columns content">
    <h3><?= __('Pf Pdffiltros') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_calculo_id') ?></th>
                <th><?= $this->Paginator->sort('pf_atendimento_id') ?></th>
                <th><?= $this->Paginator->sort('pf_acomodacao_id') ?></th>
                <th><?= $this->Paginator->sort('tipo_produto_id') ?></th>
                <th><?= $this->Paginator->sort('coparticipacao') ?></th>
                <th><?= $this->Paginator->sort('filtroreembolso') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfPdffiltros as $pfPdffiltro): ?>
            <tr>
                <td><?= $this->Number->format($pfPdffiltro->id) ?></td>
                <td><?= $pfPdffiltro->has('pf_calculo') ? $this->Html->link($pfPdffiltro->pf_calculo->id, ['controller' => 'PfCalculos', 'action' => 'view', $pfPdffiltro->pf_calculo->id]) : '' ?></td>
                <td><?= $pfPdffiltro->has('pf_atendimento') ? $this->Html->link($pfPdffiltro->pf_atendimento->id, ['controller' => 'PfAtendimentos', 'action' => 'view', $pfPdffiltro->pf_atendimento->id]) : '' ?></td>
                <td><?= $this->Number->format($pfPdffiltro->pf_acomodacao_id) ?></td>
                <td><?= $this->Number->format($pfPdffiltro->tipo_produto_id) ?></td>
                <td><?= h($pfPdffiltro->coparticipacao) ?></td>
                <td><?= h($pfPdffiltro->filtroreembolso) ?></td>
                <td><?= h($pfPdffiltro->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pfPdffiltro->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfPdffiltro->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfPdffiltro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfPdffiltro->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
