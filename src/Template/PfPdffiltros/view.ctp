<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Pdffiltro'), ['action' => 'edit', $pfPdffiltro->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Pdffiltro'), ['action' => 'delete', $pfPdffiltro->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfPdffiltro->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Pdffiltros'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Pdffiltro'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Calculos'), ['controller' => 'PfCalculos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Calculo'), ['controller' => 'PfCalculos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['controller' => 'PfAtendimentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Atendimento'), ['controller' => 'PfAtendimentos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfPdffiltros view large-9 medium-8 columns content">
    <h3><?= h($pfPdffiltro->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Calculo') ?></th>
            <td><?= $pfPdffiltro->has('pf_calculo') ? $this->Html->link($pfPdffiltro->pf_calculo->id, ['controller' => 'PfCalculos', 'action' => 'view', $pfPdffiltro->pf_calculo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Atendimento') ?></th>
            <td><?= $pfPdffiltro->has('pf_atendimento') ? $this->Html->link($pfPdffiltro->pf_atendimento->id, ['controller' => 'PfAtendimentos', 'action' => 'view', $pfPdffiltro->pf_atendimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Coparticipacao') ?></th>
            <td><?= h($pfPdffiltro->coparticipacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Filtroreembolso') ?></th>
            <td><?= h($pfPdffiltro->filtroreembolso) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfPdffiltro->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Acomodacao Id') ?></th>
            <td><?= $this->Number->format($pfPdffiltro->pf_acomodacao_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo Produto Id') ?></th>
            <td><?= $this->Number->format($pfPdffiltro->tipo_produto_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($pfPdffiltro->created) ?></td>
        </tr>
    </table>
</div>
