<?= $this->Form->create($pfProduto) ?>

<div class="col-xs-12 fonteReduzida">
    <div class="col-xs-6 ">
        <?= $this->Form->input('nome') ?>
    </div>
    <div class="col-xs-6 ">
        <?= $this->Form->input('pf_operadora_id', ['options' => $pfOperadoras, 'empty' => 'SELECIONE', 'label' => 'Operadora', 'required' => 'required']) ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-xs-12 ">
        <?= $this->Form->input('descricao', ['label' => 'Observação']) ?>
    </div>
    <?= $this->element('botoesAdd') ?>
</div>
<?= $this->Form->end() ?>
