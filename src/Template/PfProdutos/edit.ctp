<div class="col-xs-12">
    <?= $this->Form->create($pfProduto) ?>

    <div class="col-xs-6 fonteReduzida">
        <?= $this->Form->create($pfProduto) ?>
        <?= $this->Form->input('nome', ['required' => 'required']) ?>
    </div>
    <div class="col-xs-6 fonteReduzida">
        <?= $this->Form->input('pf_operadora_id', ['options' => $pfOperadoras, 'empty' => 'SELECIONE', 'label' => 'Operadora', 'required' => 'required']) ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-xs-12 fonteReduzida">
        <?= $this->Form->input('descricao', ['label' => 'Observção']) ?>
    </div>

    <?= $this->element('botoesAdd') ?>
    <?= $this->Form->end() ?>
</div>
