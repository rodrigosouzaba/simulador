<div class="col-md-3 fonteReduzida">
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Produto PF', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
</div>

<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('descricao', ['label' => 'Descrição']) ?></th>
            <th><?= $this->Paginator->sort('pf_operadora_id', ['label' => 'Operadora']) ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($produtos as $produto) : ?>
            <tr>
                <td><?= h($produto->nome) ?></td>
                <td><?= h($produto->descricao) ?></td>
                <td>
                    <?php if (isset($produto->pf_operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $produto->pf_operadora->imagen->caminho . "/" . $produto->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $produto->pf_operadora->detalhe;
                    } else {
                        echo $produto->pf_operadora->nome . " - " . $produto->pf_operadora->detalhe;
                    }
                    ?>
                </td>
                <td class="actions">
                    <?php
                    if ($sessao['role'] == 'admin') {
                        echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $produto->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Produto']);
                        echo $this->Form->postLink('', ['action' => 'delete', $produto->id], ['confirm' => __('Confirma exclusão do Produto?', $produto->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash']);
                    }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>


<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = "<?= $this->request->webroot .  $this->request->controller ?>/filtro/" + $(this).val()
    });
</script>