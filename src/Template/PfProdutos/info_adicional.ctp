<div class="clearfix">&nbsp;</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField ">
        Carências
    </div>
    <?= $this->Form->input('pf_carencia_id', ['options' => $pfCarencias, 'empty' => 'SELECIONE', 'label' => '']); ?>            
</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField">
        Reembolsos
    </div>
    <?= $this->Form->input('pf_reembolso_id', ['options' => $pfReembolsos, 'empty' => 'SELECIONE', 'label' => '']); ?>
</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField">
        Rede Credenciada
    </div>
    <?= $this->Form->input('pf_rede_id', ['options' => $pfRedes, 'empty' => 'SELECIONE', 'label' => '']); ?>

</div>

<div class="clearfix">&nbsp;</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField">
        Dependentes
    </div>
    <?= $this->Form->input('pf_documento_id', ['options' => $pfDocumentos, 'empty' => 'SELECIONE', 'label' => '']); ?>

</div>
<div class="col-md-4 fonteReduzida">

    <div class="tituloField">
        Documentos Necessários
    </div>
    <?= $this->Form->input('pf_dependente_id', ['options' => $pfDependentes, 'empty' => 'SELECIONE', 'label' => '']); ?>

</div>
<div class="col-md-4 fonteReduzida">
    <div class="tituloField">
        Observações
    </div>
    <?= $this->Form->input('pf_observacao_id', ['options' => $pfObservacoes, 'empty' => 'SELECIONE', 'label' => '']); ?>
</div>

<div class="clearfix">&nbsp;</div>


