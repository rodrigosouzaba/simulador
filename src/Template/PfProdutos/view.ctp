<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Produto'), ['action' => 'edit', $pfProduto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Produto'), ['action' => 'delete', $pfProduto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfProduto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Produtos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Produto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Carencias'), ['controller' => 'PfCarencias', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Carencia'), ['controller' => 'PfCarencias', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Redes'), ['controller' => 'PfRedes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Rede'), ['controller' => 'PfRedes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Reembolsos'), ['controller' => 'PfReembolsos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Reembolso'), ['controller' => 'PfReembolsos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Documentos'), ['controller' => 'PfDocumentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Documento'), ['controller' => 'PfDocumentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Dependentes'), ['controller' => 'PfDependentes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Dependente'), ['controller' => 'PfDependentes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Observacoes'), ['controller' => 'PfObservacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Observaco'), ['controller' => 'PfObservacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['controller' => 'TiposProdutos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['controller' => 'TiposProdutos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfProdutos view large-9 medium-8 columns content">
    <h3><?= h($pfProduto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($pfProduto->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Operadora') ?></th>
            <td><?= $pfProduto->has('pf_operadora') ? $this->Html->link($pfProduto->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfProduto->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Carencia') ?></th>
            <td><?= $pfProduto->has('pf_carencia') ? $this->Html->link($pfProduto->pf_carencia->id, ['controller' => 'PfCarencias', 'action' => 'view', $pfProduto->pf_carencia->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Rede') ?></th>
            <td><?= $pfProduto->has('pf_rede') ? $this->Html->link($pfProduto->pf_rede->id, ['controller' => 'PfRedes', 'action' => 'view', $pfProduto->pf_rede->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Reembolso') ?></th>
            <td><?= $pfProduto->has('pf_reembolso') ? $this->Html->link($pfProduto->pf_reembolso->id, ['controller' => 'PfReembolsos', 'action' => 'view', $pfProduto->pf_reembolso->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Documento') ?></th>
            <td><?= $pfProduto->has('pf_documento') ? $this->Html->link($pfProduto->pf_documento->id, ['controller' => 'PfDocumentos', 'action' => 'view', $pfProduto->pf_documento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Dependente') ?></th>
            <td><?= $pfProduto->has('pf_dependente') ? $this->Html->link($pfProduto->pf_dependente->id, ['controller' => 'PfDependentes', 'action' => 'view', $pfProduto->pf_dependente->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Observaco') ?></th>
            <td><?= $pfProduto->has('pf_observaco') ? $this->Html->link($pfProduto->pf_observaco->id, ['controller' => 'PfObservacoes', 'action' => 'view', $pfProduto->pf_observaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tipos Produto') ?></th>
            <td><?= $pfProduto->has('tipos_produto') ? $this->Html->link($pfProduto->tipos_produto->id, ['controller' => 'TiposProdutos', 'action' => 'view', $pfProduto->tipos_produto->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfProduto->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($pfProduto->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Tabelas') ?></h4>
        <?php if (!empty($pfProduto->pf_tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Vigencia') ?></th>
                <th><?= __('Faixa1') ?></th>
                <th><?= __('Faixa2') ?></th>
                <th><?= __('Faixa3') ?></th>
                <th><?= __('Faixa4') ?></th>
                <th><?= __('Faixa5') ?></th>
                <th><?= __('Faixa6') ?></th>
                <th><?= __('Faixa7') ?></th>
                <th><?= __('Faixa8') ?></th>
                <th><?= __('Faixa9') ?></th>
                <th><?= __('Faixa10') ?></th>
                <th><?= __('Faixa11') ?></th>
                <th><?= __('Faixa12') ?></th>
                <th><?= __('Pf Produto Id') ?></th>
                <th><?= __('Pf Operadora Id') ?></th>
                <th><?= __('Pf Atendimento Id') ?></th>
                <th><?= __('Pf Acomodacao Id') ?></th>
                <th><?= __('Validade') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Pf Comercializacao Id') ?></th>
                <th><?= __('Cod Ans') ?></th>
                <th><?= __('Minimo Vidas') ?></th>
                <th><?= __('Maximo Vidas') ?></th>
                <th><?= __('Prioridade') ?></th>
                <th><?= __('Coparticipacao') ?></th>
                <th><?= __('Detalhe Coparticipacao') ?></th>
                <th><?= __('Modalidade') ?></th>
                <th><?= __('Reembolso') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfProduto->pf_tabelas as $pfTabelas): ?>
            <tr>
                <td><?= h($pfTabelas->id) ?></td>
                <td><?= h($pfTabelas->nome) ?></td>
                <td><?= h($pfTabelas->descricao) ?></td>
                <td><?= h($pfTabelas->vigencia) ?></td>
                <td><?= h($pfTabelas->faixa1) ?></td>
                <td><?= h($pfTabelas->faixa2) ?></td>
                <td><?= h($pfTabelas->faixa3) ?></td>
                <td><?= h($pfTabelas->faixa4) ?></td>
                <td><?= h($pfTabelas->faixa5) ?></td>
                <td><?= h($pfTabelas->faixa6) ?></td>
                <td><?= h($pfTabelas->faixa7) ?></td>
                <td><?= h($pfTabelas->faixa8) ?></td>
                <td><?= h($pfTabelas->faixa9) ?></td>
                <td><?= h($pfTabelas->faixa10) ?></td>
                <td><?= h($pfTabelas->faixa11) ?></td>
                <td><?= h($pfTabelas->faixa12) ?></td>
                <td><?= h($pfTabelas->pf_produto_id) ?></td>
                <td><?= h($pfTabelas->pf_operadora_id) ?></td>
                <td><?= h($pfTabelas->pf_atendimento_id) ?></td>
                <td><?= h($pfTabelas->pf_acomodacao_id) ?></td>
                <td><?= h($pfTabelas->validade) ?></td>
                <td><?= h($pfTabelas->estado_id) ?></td>
                <td><?= h($pfTabelas->pf_comercializacao_id) ?></td>
                <td><?= h($pfTabelas->cod_ans) ?></td>
                <td><?= h($pfTabelas->minimo_vidas) ?></td>
                <td><?= h($pfTabelas->maximo_vidas) ?></td>
                <td><?= h($pfTabelas->prioridade) ?></td>
                <td><?= h($pfTabelas->coparticipacao) ?></td>
                <td><?= h($pfTabelas->detalhe_coparticipacao) ?></td>
                <td><?= h($pfTabelas->modalidade) ?></td>
                <td><?= h($pfTabelas->reembolso) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfTabelas', 'action' => 'view', $pfTabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfTabelas', 'action' => 'edit', $pfTabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfTabelas', 'action' => 'delete', $pfTabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfTabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
