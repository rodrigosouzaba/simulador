<div class="container">
    <?= $this->Form->create($pfProdutosGrupo) ?>
    <fieldset>
        <legend><?= __('Novo Grupo - PF') ?></legend>
        <div class="space-md">&nbsp;</div>
        <div class="col-md-6">
            <?= $this->Form->control('nome') ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->control('pf_operadora_id', ['label' => 'Operadora', 'options' => $pfOperadoras]) ?>
        </div>
        <div id="resposta-produtos" class="col-md-12">
            <?= $this->Form->input('pf_tabelas._ids', ['id' => 'pf-tabelas', 'label' => 'Produtos', 'class' => 'multiselect', 'multiple']) ?>
        </div>
    </fieldset>
    <div class="space-md">&nbsp;</div>
    <div class="text-center">
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
<script>
    $(document).ready(function() {
        $("#pf-tabelas").multiSelect()
    })
    $("#pf-operadora-id").change(function() {
        var operadora = $(this).val();
        $.ajax(baseUrl + 'pf-produtos-grupos/get-produtos/' + operadora)
            .then(function(data) {
                $("#resposta-produtos").empty();
                $("#resposta-produtos").append(data);
            })
    })
</script>