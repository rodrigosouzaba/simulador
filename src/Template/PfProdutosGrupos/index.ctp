<div class="container">
    <h3><?= __('Grupos de Produtos') ?></h3>
    <?= $this->Html->link('Novo Grupo', ['action' => 'add'], ['class' => 'btn btn-primary right']) ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pf_operadora_id', ['label' => 'Operadora']) ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfProdutosGrupos as $pfProdutosGrupo) : ?>
                <tr>
                    <td><?= h($pfProdutosGrupo->nome) ?></td>
                    <td><?= $pfProdutosGrupo->has('pf_operadora') ? $this->Html->link($pfProdutosGrupo->pf_operadora->nome, ['controller' => 'PfOperadoras', 'action' => 'view', $pfProdutosGrupo->pf_operadora->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfProdutosGrupo->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfProdutosGrupo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfProdutosGrupo->id)]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>