<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PfProdutosGrupo $pfProdutosGrupo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Produtos Grupo'), ['action' => 'edit', $pfProdutosGrupo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Produtos Grupo'), ['action' => 'delete', $pfProdutosGrupo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfProdutosGrupo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Produtos Grupos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Produtos Grupo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfProdutosGrupos view large-9 medium-8 columns content">
    <h3><?= h($pfProdutosGrupo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($pfProdutosGrupo->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pf Operadora') ?></th>
            <td><?= $pfProdutosGrupo->has('pf_operadora') ? $this->Html->link($pfProdutosGrupo->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfProdutosGrupo->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfProdutosGrupo->id) ?></td>
        </tr>
    </table>
</div>
