<div class="container">
    <?= $this->Form->create($pfProfisso) ?>
    <div class="col-md-4">
        <?= $this->Form->input('nome'); ?>
    </div>
    <div class="col-md-4"><?= $this->Form->input('idade_min', ['label' => 'Idade Mínima']); ?></div>
    <div class="col-md-4"><?= $this->Form->input('idade_max', ['label' => 'Idade Máxima']); ?></div>
    <?php if ($this->request->action == 'edit') : ?>
        <div class="col-md-12">
            <?= $this->Form->select('pf_entidades', $entidades, ['default' => $entidades_assoc, 'multiple' => 'true']) ?>
        </div>
    <?php endif; ?>
    <div class="col-md-12">
        <?= $this->Form->input('descricao', ['label' => 'Observação']) ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div id="alias" class="col-xs-12">
        <label>Outras nomenclaturas</label>
        <div class="btn-group right">
            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-plus']), '#', ['class' => 'btn btn-default add-input', 'escape' => false]) ?>
            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-minus']), '#', ['class' => 'btn btn-danger del-input', 'escape' => false]) ?>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div id="inputs">
            <?php if ($this->request->action == 'add') : ?>
                <?= $this->Form->input('pf_profissoes_alias[].nome', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Adicionar Apelido']); ?>
                <?php else :
                foreach ($pfProfisso->pf_profissoes_alias as $key => $alias) : ?>
                    <?= $this->Form->input('pf_profissoes_alias[' . $key . '].id', ['value' => $alias->id, 'type' => 'hidden']); ?>
                    <?= $this->Form->input('pf_profissoes_alias[' . $key . '].nome', ['value' => $alias->nome, 'label' => false, 'class' => 'form-control', 'placeholder' => 'Adicionar Apelido']); ?>
            <?php endforeach;
            endif; ?>
        </div>
    </div>
    <?= $this->element('botoesAdd') ?>
    <?= $this->Form->end() ?>

</div>
<script>
    $('.add-input').click(function(e) {
        e.preventDefault();
        var length = $('.dinamic').length;
        $('#inputs').append('<div class="input text dinamic"><input type = "text" name="pf_profissoes_alias[][nome]" class="form-control" placeholder="Adicionar Apelido"></div>');
    });
    $('.del-input').click(function(e) {
        e.preventDefault();
        var length = $('.dinamic').length;

        if (length > 0) {
            $(".dinamic").last().remove()
        }
    });
</script>