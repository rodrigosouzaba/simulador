<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfProfisso->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfProfisso->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Profissoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfProfissoes form large-9 medium-8 columns content">
    <?= $this->Form->create($pfProfisso) ?>
    <fieldset>
        <legend><?= __('Edit Pf Profisso') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('tabelas._ids', ['options' => $tabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
