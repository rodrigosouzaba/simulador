<div class="col-xs-12">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Profissão PF', "/pfProfissoes/add", ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'salvar']);
    ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('Observação') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfProfissoes as $pfProfisso) : ?>
                <tr>
                    <td><?= h($pfProfisso->nome) ?></td>
                    <td><?= h($pfProfisso->descricao) ?></td>
                    <td class="actions">
                        <?=
                            $this->Html->link('', "/pfProfissoes/edit/$pfProfisso->id", [
                                'title' => __('Editar'),
                                'class' => 'btn btn-default btn-sm fa fa-pencil',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'sid' => 'pfProfissoes.edit',
                                'title' => 'Editar'
                            ]);
                        ?>
                        <?=
                            $this->Form->postLink('', "/pfProfissoes/delete/$pfProfisso->id", [
                                'confirm' => __('Tem certeza que deseja excluir Profissão {0}?', $pfProfisso->nome . "  " . $pfProfisso->detalhes), 'title' => __('Deletar'),
                                'class' => 'btn btn-default btn-sm fa fa-trash',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                                'sid' => 'pfProfissoes.delete',
                                'title' => 'Deletar'
                            ]);
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
