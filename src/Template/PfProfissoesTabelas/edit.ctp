<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfProfissoesTabela->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfProfissoesTabela->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Profissoes Tabelas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Profissoes'), ['controller' => 'PfProfissoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Profisso'), ['controller' => 'PfProfissoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfProfissoesTabelas form large-9 medium-8 columns content">
    <?= $this->Form->create($pfProfissoesTabela) ?>
    <fieldset>
        <legend><?= __('Edit Pf Profissoes Tabela') ?></legend>
        <?php
            echo $this->Form->input('pf_profissao_id', ['options' => $pfProfissoes]);
            echo $this->Form->input('pf_tabela_id', ['options' => $pfTabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
