<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pf Profissoes Tabela'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Profissoes'), ['controller' => 'PfProfissoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Profisso'), ['controller' => 'PfProfissoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfProfissoesTabelas index large-9 medium-8 columns content">
    <h3><?= __('Pf Profissoes Tabelas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('pf_profissao_id') ?></th>
                <th><?= $this->Paginator->sort('pf_tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pfProfissoesTabelas as $pfProfissoesTabela): ?>
            <tr>
                <td><?= $this->Number->format($pfProfissoesTabela->id) ?></td>
                <td><?= $pfProfissoesTabela->has('pf_profisso') ? $this->Html->link($pfProfissoesTabela->pf_profisso->id, ['controller' => 'PfProfissoes', 'action' => 'view', $pfProfissoesTabela->pf_profisso->id]) : '' ?></td>
                <td><?= $pfProfissoesTabela->has('pf_tabela') ? $this->Html->link($pfProfissoesTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $pfProfissoesTabela->pf_tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pfProfissoesTabela->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pfProfissoesTabela->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pfProfissoesTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfProfissoesTabela->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
