<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Profissoes Tabela'), ['action' => 'edit', $pfProfissoesTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Profissoes Tabela'), ['action' => 'delete', $pfProfissoesTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfProfissoesTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Profissoes Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Profissoes Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Profissoes'), ['controller' => 'PfProfissoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Profisso'), ['controller' => 'PfProfissoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['controller' => 'PfTabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfProfissoesTabelas view large-9 medium-8 columns content">
    <h3><?= h($pfProfissoesTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Pf Profisso') ?></th>
            <td><?= $pfProfissoesTabela->has('pf_profisso') ? $this->Html->link($pfProfissoesTabela->pf_profisso->id, ['controller' => 'PfProfissoes', 'action' => 'view', $pfProfissoesTabela->pf_profisso->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Tabela') ?></th>
            <td><?= $pfProfissoesTabela->has('pf_tabela') ? $this->Html->link($pfProfissoesTabela->pf_tabela->id, ['controller' => 'PfTabelas', 'action' => 'view', $pfProfissoesTabela->pf_tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfProfissoesTabela->id) ?></td>
        </tr>
    </table>
</div>
