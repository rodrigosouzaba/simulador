<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pfRede->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pfRede->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pf Redes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pf Produtos'), ['controller' => 'PfProdutos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pf Produto'), ['controller' => 'PfProdutos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pfRedes form large-9 medium-8 columns content">
    <?= $this->Form->create($pfRede) ?>
    <fieldset>
        <legend><?= __('Edit Pf Rede') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('pf_operadora_id', ['options' => $pfOperadoras, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
