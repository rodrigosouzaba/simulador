<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Rede', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['value' => $estado, 'options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?php if (empty($operadoras)) : ?>
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
    <?php else : ?>
        <?= $this->Form->input('operadora_id', ['value' =>  $operadora, 'options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
    <?php endif; ?>
</div>

<?php if ($redes) : ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= 'Título' ?></th>
                <th style="width: 50% !important"><?= 'Observação' ?></th>
                <th><?= 'Operadora' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($redes as $rede) : ?>
                <tr>
                    <td><?= $rede->nome ?></td>
                    <td><?= $rede->descricao ?></td>
                    <td>
                        <?php
                        if (isset($rede->pf_operadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $rede->pf_operadora->imagen->caminho . "/" . $rede->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $rede->pf_operadora->detalhe;
                        } else {
                            echo $rede->pf_operadora->nome . " - " . $rede->pf_operadora->detalhe;
                        }
                        ?></td>
                    <td class="actions">
                        <?php if ($sessao['role'] == 'admin') { ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $rede->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar']) ?>

                            <?= $this->Form->postLink('', ['action' => 'delete', $rede->id], ['confirm' => __('Confirma exclusão da Carência?', $rede->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash'])
                            ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="col-md-12">
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>
<?php endif; ?>
<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = baseUrl + "pfRedes/filtro/" + $(this).val();
    });

    $("#operadora-id").change(function() {
        window.location.href = baseUrl + "pfRedes/filtro/" + $("#estados").val() + "/" + $(this).val();
    });
</script>