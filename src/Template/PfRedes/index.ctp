<section id="AppPfRedes">
    <div class="col-md-3 fonteReduzida">
        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Rede PF', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]) ?>
    </div>

    <?= $this->element('filtro_interno') ?>
    <div class="spacer-md">&nbsp;</div>
    <div id="gridPfRedes">
        <?php if (isset($redes)) : ?>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?= 'Título' ?></th>
                        <th style="width: 50% !important"><?= 'Observação' ?></th>
                        <th><?= 'Operadora' ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($redes as $rede) : ?>
                        <tr>
                            <td><?= $rede->nome ?></td>
                            <td><?= $rede->descricao ?></td>
                            <td>
                                <?php
                                if (isset($rede->pf_operadora->imagen->caminho)) {
                                    echo $this->Html->image("../" . $rede->pf_operadora->imagen->caminho . "/" . $rede->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $rede->pf_operadora->detalhe;
                                } else {
                                    echo $rede->pf_operadora->nome . " - " . $rede->pf_operadora->detalhe;
                                }
                                ?></td>
                            <td class="actions">
                                <?php if ($sessao['role'] == 'admin') { ?>
                                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $rede->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar']) ?>

                                    <?= $this->Form->postLink('', ['action' => 'delete', $rede->id], ['confirm' => __('Confirma exclusão da Carência?', $rede->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash'])
                                    ?>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
    </div>
</section>
<?php endif; ?>