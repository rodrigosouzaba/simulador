<div class="col-md-3 fonteReduzida">
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Reembolso', '/pfReembolsos/add', ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'pfReembolsos.add']);
    ?>
</div>

<?= $this->element('filtro_interno') ?>


<?php if (isset($reembolsos)) : ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= 'Título' ?></th>
                <th style="width: 50% !important"><?= 'Observação' ?></th>
                <th><?= 'Operadora' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($reembolsos as $reembolso) : ?>
                <tr>
                    <td><?= $reembolso->nome ?></td>
                    <td><?= $reembolso->descricao ?></td>
                    <td>
                        <?php
                        if (isset($reembolso->pf_operadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $reembolso->pf_operadora->imagen->caminho . "/" . $reembolso->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $reembolso->pf_operadora->detalhe;
                        } else {
                            echo $reembolso->pf_operadora->nome . " - " . $reembolso->pf_operadora->detalhe;
                        }
                        ?></td>
                    <td class="actions">
                        <?php if ($sessao['role'] == 'admin') { ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']),  "/pfReembolsos/edit/$reembolso->id", ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'pfReembolsos.edit']) ?>

                            <?= $this->Form->postLink('', "/pfReembolsos/delete/$reembolso->id", ['confirm' => __('Confirma exclusão da Carência?', $reembolso->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'pfReembolsos.delete'])
                            ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="col-md-12">
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>
<?php endif; ?>