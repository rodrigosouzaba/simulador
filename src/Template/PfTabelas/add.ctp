<style>
    #spinner {
        display: show;
    }
</style>
<?= $this->Form->create($pfTabela) ?>
<div class="col-xs-12">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-6 fonteReduzida">
        <?= $this->Form->input('nome', ['required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('cod_ans', ['label' => 'Código ANS']); ?>
    </div>

    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('vigencia', ['label' => 'Válida até', 'required' => 'required']); ?>
    </div>
</div>
<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-12 fonteReduzida">
        <?= $this->Form->input('descricao', ["label" => "Observação", 'required' => 'required']); ?>
    </div>
</div>
<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('minimo_vidas', ['label' => 'Mínimo de Vidas', 'required' => 'required', "style" => "margin-bottom: 0 !important", 'min' => 1]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('maximo_vidas', ['label' => 'Máximo de Vidas', 'required' => 'required', "style" => "margin-bottom: 0 !important", 'min' => 1]); ?>
    </div>

    <div class="col-md-3 fonteReduzida" id="subproduto">
        <?= $this->Form->input('estado_id', ['options' => $estados, 'label' => 'Estado', 'empty' => 'SELECIONE', 'required' => 'required', "style" => "margin-bottom: 0 !important"]); ?>
    </div>
    <div class="col-md-3 fonteReduzida" id="operadoras">
        <?= $this->Form->input('pf_operadora_id', ['label' => 'Operadoras', 'empty' => 'SELECIONE', 'required' => 'required', "style" => "margin-bottom: 0 !important", 'disabled' => 'disabled']); ?>
    </div>
</div>
<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-3 fonteReduzida required" id="produtos">
        <?= $this->Form->input('pf_produto_id', ['label' => 'Produtos', 'empty' => 'SELECIONE', 'required' => 'required', 'disabled' => 'disabled']); ?>
    </div>

    <div class="col-md-3 fonteReduzida" id="tipoProduto">
        <?= $this->Form->input('pf_cobertura_id', ['label' => 'Tipo de Cobertura', 'style' => 'margin-bottom: 0 !important', 'options' => $pfCoberturas, 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>


    <div class="col-md-3 fonteReduzida" id="regiao">
        <?= $this->Form->input('pf_comercializacao_id', ['options' => '', 'label' => 'Área de Comercialização', 'empty' => 'SELECIONE', 'required' => 'required', 'disabled' => 'disabled']); ?>
    </div>

    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('pf_acomodacao_id', ['options' => $pfAcomodacoes, 'label' => 'Tipo de Acomodação', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
</div>
<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('pf_atendimento_id', ['label' => 'Área de Atendimento', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?php $modalidades = array('INDIVIDUAL' => 'INDIVIDUAL', 'ADESAO' => 'ADESÃO'); ?>
        <?= $this->Form->input('modalidade', ['options' => $modalidades, 'label' => 'Modalidade', 'empty' => 'SELECIONE', 'required' => 'required', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">

        <?php $filtroreembolso = array('N' => 'SEM REEMBOLSO', 'S' => 'COM REEMBOLSO'); ?>
        <?= $this->Form->input('reembolso', ['options' => $filtroreembolso, 'label' => 'Possui Reembolso', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>

    <div class="col-md-3 fonteReduzida">
        <?php $coparticipacao = array('s' => 'Sim', 'n' => 'Não'); ?>
        <?= $this->Form->input('coparticipacao', ['options' => $coparticipacao, 'empty' => 'SELECIONE', 'label' => 'Possui Coparticipação?', 'required' => 'required']); ?>
    </div>
</div>
<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-3 fonteReduzida" id="detalheCoparticipacao">
        <?= $this->Form->input('detalhe_coparticipacao', ['label' => 'Detalhes Coparticipação. Exemplo ("10%", "FN10", etc..) ', 'style' => 'margin-bottom: 0 !important']); ?>
    </div>
    <div id="odontoValor" style="display: none;">
        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('odonto_operadora', ['options' => $odonto_operadoras, 'empty' => 'Selecione Operadora']); ?>
        </div>
        <div class="col-md-3 fonteReduzida">
            <label for="odonto_valor" id="label-odonto">Preço Odonto por Vida </label>
            <?= $this->Form->input('odonto_valor', ['label' => false, 'style' => 'margin-bottom: 0 !important', 'step' => '0.01']); ?>
        </div>
    </div>
</div>
<!-- PREÇOS -->
<div class="col-xs-12">
    <div class="col-md-12 centralizada fonteReduzida ">
        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa1', ['label' => '0 à 18 anos', 'type' => 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa1]); ?>
            <label for="check1">
                <?= $this->Form->checkbox('check1', ["id" => "check1", "class" => "consulta", "campo" => "faixa1", "tabindex" => "1"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa2', ['label' => '19 à 23 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa2]); ?>
            <label for="check2">
                <?= $this->Form->checkbox('check2', ["id" => "check2", "class" => "consulta", "campo" => "faixa2", "tabindex" => "2"]); ?>
                So consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa3', ['label' => '24 à 28 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa3]); ?>
            <label for="check3">
                <?= $this->Form->checkbox('check3', ["id" => "check3", "class" => "consulta", "campo" => "faixa3", "tabindex" => "3"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa4', ['label' => '29 à 33 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa4]); ?>
            <label for="check4">
                <?= $this->Form->checkbox('check4', ["id" => "check4", "class" => "consulta", "campo" => "faixa4", "tabindex" => "4"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa5', ['label' => '34 à 38 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa5]); ?>
            <label for="check5">
                <?= $this->Form->checkbox('check5', ["id" => "check5", "class" => "consulta", "campo" => "faixa5", "tabindex" => "5"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa6', ['label' => '39 à 43 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa6]); ?>
            <label for="check6">
                <?= $this->Form->checkbox('check6', ["id" => "check6", "class" => "consulta", "campo" => "faixa6", "tabindex" => "6"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa7', ['label' => '44 à 48 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa7]); ?>
            <label for="check7">
                <?= $this->Form->checkbox('check7', ["id" => "check7", "class" => "consulta", "campo" => "faixa7", "tabindex" => "7"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa8', ['label' => '49 à 53 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa8]); ?>
            <label for="check8">
                <?= $this->Form->checkbox('check8', ["id" => "check8", "class" => "consulta", "campo" => "faixa8", "tabindex" => "8"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa9', ['label' => '54 à 58 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa9]); ?>
            <label for="check9">
                <?= $this->Form->checkbox('check9', ["id" => "check9", "class" => "consulta", "campo" => "faixa9", "tabindex" => "9"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa10', ['label' => '59 à 64 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa10]); ?>
            <label for="check10">
                <?= $this->Form->checkbox('check10', ["id" => "check10", "class" => "consulta", "campo" => "faixa10", "tabindex" => "10"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa11', ['label' => '65 à 80 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa11]); ?>
            <label for="check11">

                <?= $this->Form->checkbox('check11', ["id" => "check11", "class" => "consulta", "campo" => "faixa11", "tabindex" => "11"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa12', ['label' => '+ de 81 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa12]); ?>
            <label for="check12">
                <?= $this->Form->checkbox('check12', ["id" => "check12", "class" => "consulta", "campo" => "faixa12", "tabindex" => "12"]); ?>
                Sob consulta
            </label>
        </div>
    </div>
</div>
<!-- ENTIDADES -->
<div class="col-xs-12 clearfix">&nbsp;</div>
<div class="col-xs-12">
    <label>Entidades</label>
    <?= $this->Form->input('pf_entidades._ids', ['label' => false, 'options' => $entidades, 'multiple' => true]) ?>
</div>
<div class="col-xs-12 clearfix">&nbsp;</div>
<?= $this->element('botoesAdd') ?>

<?= $this->Form->end() ?>




<script type="text/javascript">
    $(document).ready(function() {
        $("#acoes").hide();
        $("#entidades").hide();
        $("#detalheCoparticipacao").hide();
        $("#coparticipacao").change(function() {
            //           alert((this).value);
            if ((this).value === 's') {
                $("#detalheCoparticipacao").show();
            } else {
                $("#detalheCoparticipacao").hide();
            }

        });
    });
    $("#pf-cobertura-id").change(function() {
        if ($(this).val() == 4 || $(this).val() == 5 || $(this).val() == 6) {
            $("#odontoValor").show();
            $("#odonto-valor").prop('required', true);
            $("#odonto-operadora").prop('required', true);
        } else {
            $("#odontoValor").hide();
            $("#odonto-valor").removeAttr("required");
            $("#odonto-operadora").removeAttr("required");
        }
    });

    $("#estado-id").change(function() {
        let estado = $(this).val();
        var element = $("#pf-operadora-id");
        $.ajax({
            type: 'POST',
            data: {
                'estado': estado
            },
            url: '<?= $this->request->webroot ?>pfTabelas/filtroEstado',
            success: function(response) {
                element.prop("disabled", true).empty();
                element.append(`<option value="" selected="selected"> Selecione </option>`);
                Object.entries(response).forEach(([key, value]) => {
                    element.append(`<option value="${key}">  ${value} </option>`);
                });
                element.prop("disabled", false);
            }
        });
    });

    $("#pf-operadora-id").change(function() {
        $("#acoes").show();
        if ((this).value != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>pf-tabelas/find_produtos/" + (
                    this).value,
                data: $("#pf-operadora-id").serialize(),
                beforeSend: () => {
                    $("#modalProcessando").modal("show");
                },
                success: function(data) {
                    $("#produtos").empty();
                    $("#produtos").append(data);
                    $("#modalProcessando").modal("hide");
                }
            });
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>pf-tabelas/find_regioes/" + (
                    this).value + "/" + $("#estado-id").val(),
                data: $("#pf-operadora-id").serialize(),
                beforeSend: () => {
                    $("#modalProcessando").modal("show");
                },
                success: function(data) {
                    $("#regiao").empty();
                    $("#regiao").append(data);
                    $("#modalProcessando").modal("hide");
                }
            });
        }
    });
</script>