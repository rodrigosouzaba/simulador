<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<div class="clearfix">&nbsp;</div>

<?= $this->Form->create('editarPercentual', ['id' => 'editarPercentual', 'url' => 'pf_tabelas/editar_percentual']); ?>


<?php if ($pftabelas) { ?> 
	<div class="col-md-4">
		<?= $this->Form->input('percentual', ['placeholder' => 'Percentual de Reajuste', 'label' => 'Percentual de Reajuste']); ?>
		<div class="well well-sm">
			<?php for ($i=2; $i <= 11; $i++): ?>	
				<?= $this->Form->input('faixa'.$i, ['label' => 'Variação Faixa '. $i, 'placeholder' => 'Variação relativa a Faixa' . ($i - 1)]); ?>
			<?php endfor; ?>
		</div>
		<span id="helpBlock" class="help-block">Não há necessidade de digitar (%)</span>
	</div>
	<div class="col-md-4">
		<h6>
			<b>Tabelas Secundárias</b>    
		</h6>
		<?php
		foreach ($pftabelas as $id => $nometabela) {
			echo $this->Form->checkbox('Tabelas[' . $nometabela . ']', ['label' => $nometabela, 'value' => $id, 'hiddenField' => 'false']) . " " . $nometabela . "<br/>";
		}
		?>
	</div>
	<div class="col-md-4">
	</div>
	<div class="col-md-12 centralizada">
		<div class="clearfix">&nbsp;</div>
		<?=
		$this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar', '#', ['class' => 'btn btn-md btn-primary ', 'role' => 'button', 'escape' => false, 'id' => 'editar', 'type' => 'submit']);
		?>
		<?=
		$this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['action' => 'index'], ['class' => 'btn btn-md btn-default ', 'role' => 'button', 'escape' => false]);
		?>
	</div>
	<?php
} else {
	?>
	<div class="clearfix">&nbsp;</div>
	<p class="bg-danger centralizada">Nenhuma tabela encontrada</p>
<?php } ?>


<?= $this->Form->end() ?>
<div id="aaaa"></div>

<script type="text/javascript">
	$("#tabelassecundaria-id").change(function () {
		if ($("#tabelassecundaria-id").val() == $("#tabelamatriz-id").val()) {
			alert('Tabelas devem ser diferentes.');
			$("#percentual").prop('disabled', true);
		} else {
			$("#percentual").removeAttr('disabled');
		}

	});

	$("#editar").click(function () {
		$.ajax({
			type: "POST",
			url: "<?php echo $this->request->webroot ?>pf-tabelas/editar_percentual",
			data: $("#editarPercentual").serialize(),
			success: function (data) {
				alert(data);
			}
		});

	});

</script>