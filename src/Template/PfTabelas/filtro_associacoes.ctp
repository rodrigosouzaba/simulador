<div class="col-md-3">
    <?= $this->Form->control('pf_rede_id', ['empty' => 'SELECIONE REDE', 'options' => $pf_redes]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('pf_observacao_id', ['empty' => 'SELECIONE OBSERVAÇÃO', 'options' => $pf_observacoes]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('pf_reembolso_id', ['empty' => 'SELECIONE REEMBOLSO', 'options' => $pf_reembolsos]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('pf_carencia_id', ['empty' => 'SELECIONE CARENCIA', 'options' => $pf_carencias]) ?>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-3">
    <?= $this->Form->control('pf_dependente_id', ['empty' => 'SELECIONE DEPENDENTE', 'options' => $pf_dependentes]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('pf_documento_id', ['empty' => 'SELECIONE DOCUMENTO', 'options' => $pf_documentos]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('pf_formas_pagamento_id', ['empty' => 'SELECIONE FORMAS PAGAMENTO', 'options' => $pf_formas_pagamentos]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('pf_entidades_grupo_id', ['empty' => 'SELECIONE GRUPOS ENTIDADES', 'options' => $pf_entidades_grupos]) ?>
</div>