<section id="AppPfTabelas">
    <?= $this->Form->hidden('old', ['id' => 'table', 'value' => 1]); ?>
    <div class="row">
        <?php echo $this->Form->create('search', array('id' => 'pesquisarPfCalculo', 'class' => 'form', 'role' => 'form')); ?>

        <div class="col-md-2 fonteReduzida">
            <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Tabela PF', '/pfTabelas/add', ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'salvar', 'sid' => 'pfTabelas.add']);
            ?>
        </div>
        <div class="col-md-2 fonteReduzida">
            <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Editar por Percentual', '/pfTabelas/valorPercentual', ['class' => 'btn btn-sm btn-default botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'pfTabelas.valorPercentual']);
            ?>
        </div>
        <div class="col-md-4 fonteReduzida">
            <?= $this->Form->input('estados', ['id' => 'estados_old', 'options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
        </div>
        <div class="col-md-4 fonteReduzida">
            <?= $this->Form->input('fakeOperadora', ['id' => 'operadora-id-old', 'options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
        </div>

        <?php echo $this->Form->end(); ?>
    </div>

    <div class="row">
        <div id="gridPfCalculo" clas="col-md-12" style="padding: 15px;">
            <form id="selecaoTabelas">
                <?php if ($tabelas) { ?>
                    <div class="col-md-12">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <td class="tituloTabela text-center" style="width: 2%"><?= 'Sel.' ?></td>
                                    <td class="tituloTabela text-center" style="width: 17%"><?= 'Operadora' ?></td>
                                    <td class="tituloTabela text-center" style="width: 17%"><?= 'Produto' ?></td>
                                    <td class="tituloTabela text-center">Tipo cobertura</td>
                                    <td class="tituloTabela text-center">Modalidade</td>
                                    <td class="tituloTabela text-center" style="width: 9%"><?= 'Vidas' ?></td>
                                    <td class="tituloTabela text-center"><?= 'Coparticipação' ?></td>
                                    <td class="tituloTabela text-center"><?= 'Acomodação' ?></td>
                                    <td class="tituloTabela text-center" style="width: 6%"><?= 'Vigência' ?></td>
                                    <td class="tituloTabela text-center" style="width: 7%"><?= 'Prioridade' ?></td>

                                    <td class="actions tituloTabela" style="width: max-content;"><?= __('Actions') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($tabelas as $tabela) : ?>
                                    <tr>
                                        <td class="text-center" style="width: 2%"><?= $this->Form->input('tabela' . $tabela->id, ['type' => 'checkbox', 'value' => $tabela->id, 'label' => '', 'class' => 'checkHabilita']) ?></td>
                                        <td style="font-size: 90% !important" class=" centralizada"><?= $this->Html->image("../" . $tabela->pf_produto->pf_operadora->imagen->caminho . "/" . $tabela->pf_produto->pf_operadora->imagen->nome, ['class' => 'logoMiniatura']) ?></td>
                                        <td class="text-center" style="width: 40%"><?= "<b>" . $tabela->pf_produto->nome . "</b><br/><small>" . $tabela->nome . "</small>" ?></td>
                                        <td class="text-center"><?= ($tabela->pf_cobertura_id ? $tabela->pf_cobertura->nome : '') ?></td>
                                        <td class="text-center"><?= $tabela->modalidade ?></td>
                                        <td class="text-center"><?= $tabela->minimo_vidas . " a " . $tabela->maximo_vidas ?></td>
                                        <td class="text-center"><?= ($tabela->coparticipacao == 'n') ? "S/ Coparticipação" : "C/ Coparticipação " . $tabela->detalhe_coparticipacao ?></td>
                                        <td class="text-center"><?= h($tabela->pf_acomodaco->nome) ?></td>
                                        <td class="text-center"><?= h($tabela->vigencia) ?></td>
                                        <td class="text-center" class="text-center"><?= h($tabela->prioridade) ?></td>

                                        <td class="actions" style="font-size: 90% !important;">
                                            <?php if ($sessao['role'] == 'admin') : ?>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-sm dropdown-toggle <?= $tabela->validade == null || $tabela->validade == 0 ? "btn-danger" : ($tabela->atualizacao != null || $tabela->atualizacao != 0 ? "btn-warning" : "btn-success")  ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-cogs"></i>
                                                    </button>
                                                    <ul class="dropdown-menu" style="right: 0; left: auto;">
                                                        <!-- <li><?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']) . " Editar", '/pfTabelas/edit/' . $tabela->id, ['escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Editar / Preços']) ?></li> -->
                                                        <li><?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']) . " Editar", 'javascript: void(0);', ['escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Editar / Preços', 'id' => $tabela->id, 'class' => 'btnEditOld']) ?></li>

                                                        <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-copy', 'aria-hidden' => 'true']) . " Duplicar", '/pfTabelas/duplicar/' . $tabela->id, ['escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Duplicar tabela']) ?></li>

                                                        <li>
                                                            <?php if ($tabela->validade == null || $tabela->validade == 0) {

                                                                echo $this->Html->link(
                                                                    $this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . " Produto Suspenso",
                                                                    '#',
                                                                    [
                                                                        'class' => 'btn-danger validar',
                                                                        'escape' => false,
                                                                        'value' => $tabela->id,
                                                                        'data-toggle' => 'tooltip',
                                                                        'data-placement' => 'left',
                                                                        'title' => 'SUSPENSA'
                                                                    ]
                                                                );
                                                            } else {

                                                                echo $this->Html->link(
                                                                    $this->Html->tag('span', '', ['class' => 'fa fa-check', 'aria-hidden' => 'true']) . " Produto Ativo",
                                                                    '#',
                                                                    [
                                                                        'class' => 'btn-success invalidar',
                                                                        'escape' => false,
                                                                        'value' => $tabela->id,
                                                                        'data-toggle' => 'tooltip',
                                                                        'data-placement' => 'left',
                                                                        'title' => 'ATIVA'
                                                                    ]
                                                                );
                                                            } ?>
                                                        </li>
                                                        <li>
                                                            <?= $this->Html->link($this->Html->tag('span', '', [
                                                                'class' => 'fa fa-list',
                                                                'aria-hidden' => 'true'
                                                            ]) . " Alterar Prioridade", '#', [
                                                                'class' => 'alterarPrioridade',
                                                                'escape' => false,
                                                                'value' => $tabela->id,
                                                                // 'data-placement' => 'left',
                                                                // 'title' => 'Definir Prioridade',
                                                                // 'data-toggle' => "modal",
                                                                // 'data-target' => "#modalPrioridade"
                                                            ]) ?>
                                                        </li>
                                                        <li>
                                                            <?php if ($tabela->atualizacao == null || $tabela->atualizacao == 0) :
                                                                echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-refresh']) . " Atualizado", '#', [
                                                                    'class' => 'btn-default colocarAtlz',
                                                                    'data-toggle' => 'tooltip',
                                                                    'data-placement' => 'left',
                                                                    'escape' => false,
                                                                    'value' => $tabela->id,
                                                                    'title' => 'Colocar em Atualização de Preços'
                                                                ]);
                                                            else :
                                                                echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-refresh']) . " Em Atualização", '#', [
                                                                    'class' => 'btn-warning removerAtlz',
                                                                    'data-toggle' => 'tooltip',
                                                                    'data-placement' => 'left',
                                                                    'escape' => false,
                                                                    'value' => $tabela->id,
                                                                    'title' => 'Remover Atualização de Preços'
                                                                ]);
                                                            endif; ?>
                                                        </li>
                                                        <li>
                                                            <?= $this->Html->link(
                                                                $this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']) . " Deletar Produto",
                                                                "/pfTabelas/delete/$tabela->id",
                                                                [
                                                                    'class' => 'btn-default',
                                                                    'escape' => false,
                                                                    'data-toggle' => 'tooltip',
                                                                    'data-placement' => 'left',
                                                                    'title' => 'Remover',
                                                                    'confirm' => 'Confirma exclusão da Tabela?',
                                                                    'sid' => 'pfTabelas.delete'
                                                                ]
                                                            ); ?>
                                                        </li>

                                                    <?php endif; ?>
                                                    </ul>
                                                </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">
                        <?=
                        $this->Html->link($this->Html->tag('span', '', [
                            'class' => 'fa fa-trash',
                            'aria-hidden' => 'true'
                        ]) . ' Deletar Tabelas', '#', [
                            'class' => 'btn btn-sm btn-danger',
                            'role' => 'button',
                            'escape' => false,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Deletar tabelas',
                            'id' => 'deleteLote'
                        ])
                        ?>

                        <?=
                        $this->Html->link($this->Html->tag('span', '', [
                            'class' => 'fa fa-percent',
                            'aria-hidden' => 'true'
                        ]) . ' Reajustar Tabelas', '#', [
                            'class' => 'btn btn-sm btn-default',
                            'role' => 'button',
                            'escape' => false,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Reajustar Tabelas',
                            'id' => 'reajustarLote',
                            'data-toggle' => "modal",
                            'data-target' => "#modalTabelas"
                        ])
                        ?>
                        <?=
                        $this->Html->link($this->Html->tag('span', '', [
                            'class' => 'fa fa-calendar',
                            'aria-hidden' => 'true'
                        ]) . ' Alterar Vigência', '#', [
                            'class' => 'btn btn-sm btn-default',
                            'role' => 'button',
                            'escape' => false,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Alterar Vigências',
                            'id' => 'reajustarVigencia',
                            'data-toggle' => "modal",
                            'data-target' => "#modalTabelas"
                        ])
                        ?>
                    </div>
                    <div class="col-md-12">
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                <?php }  ?>
            </form>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="modalTabelas" tabindex="-1" role="dialog" aria-labelledby="modalTabelasLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalTabelasLabel">Reajustar Tabelas</h4>
                        <small> Formato 99.99 - Usar "." como separador de decimais. Não Há necessidade de digitar "%".</small>
                    </div>
                    <div class="modal-body" id="percentual" style="min-height: 150px !important;">
                        <div class="clearfix"> &nbsp;</div>
                        &nbsp;
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $("#deleteLote").hide();
        $("#reajustarLote").hide();

        $('#modalTabelas').on('shown.bs.modal', function() {
            $('#myInput').focus()
        });


        $("#reajustarLote").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "pfTabelas/reajusteLote/" + $("#selecaoTabelas").serialize(),
                success: function(data) {
                    $("#percentual").empty();
                    $("#percentual").append(data);

                }
            });
        });
        $("#reajustarVigencia").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "pfTabelas/reajusteVigencia/" + $("#selecaoTabelas").serialize(),
                success: function(data) {
                    $("#percentual").empty();
                    $("#percentual").append(data);

                }
            });
        });

    });
    $("#gerarTabela").click(function() {
        $.ajax({
            type: "post",
            data: $('#selecaoTabelas').serialize(),
            url: "<?php echo $this->request->webroot ?>tabelas/gerarTabela/" + $('#selecaoTabelas').serialize(),
            beforeSend: function() {
                $('#loader').trigger('click');
            },
            success: function(data) {
                $('#fechar').trigger('click');
            }
        });
    });

    $("#deleteLote").click(function() {
        $.ajax({
            type: "post",
            data: $("#selecaoTabelas").serialize(),
            url: "<?php echo $this->request->webroot ?>pf-tabelas/deleteLote/" + $("#selecaoTabelas").serialize(),
            success: function(data) {
                $('#operadora-id').trigger('change');
                //                    window.location = '<?php echo $this->request->webroot ?>pf-tabelas/';

            }
        });
    });

    $(".checkHabilita").click(function() {
        $("#deleteLote").show();
        $("#reajustarLote").show();
    });

    $("#deleteLote").click(function() {
        $.ajax({
            type: "post",
            data: $("#selecaoTabelas").serialize(),
            url: "<?php echo $this->request->webroot ?>produtos/deleteLote/",
            success: function(data) {
                window.location = '<?php echo $this->request->webroot ?>produtos/';

            }
        });
    });
</script>