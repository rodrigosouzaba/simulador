<style>
    #faixa_3,
    #faixa_6 {
        display: none;
    }
</style>

<?= $this->Form->create($pfTabela) ?>
<!-- INICIO -->
<div class="col-xs-12">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-6 fonteReduzida">
        <?= $this->Form->input('nome', ['required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('cod_ans', ['label' => 'Código ANS']); ?>
    </div>

    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('vigencia', ['label' => 'Válida até', 'required' => 'required']); ?>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-12 fonteReduzida">
        <?= $this->Form->input('pf_area_comercializacao_id', ['empty' => 'SELECIONE', 'label' => ['class' => 'text-success'], 'required' => 'required', 'options' => $pf_areas_comercializacoes]); ?>
    </div>
</div>
<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-12 fonteReduzida">
        <?= $this->Form->input('descricao', ["label" => "Observação", 'required' => 'required']); ?>
    </div>
</div>
<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('minimo_vidas', ['label' => 'Mínimo de Vidas', 'required' => 'required', "style" => "margin-bottom: 0 !important", 'min' => 1]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('maximo_vidas', ['label' => 'Máximo de Vidas', 'required' => 'required', "style" => "margin-bottom: 0 !important", 'min' => 1]); ?>
    </div>
    <div class="col-md-3 fonteReduzida" id="tipoProduto">
        <?= $this->Form->input('pf_cobertura_id', ['label' => 'Tipo de Cobertura', 'style' => 'margin-bottom: 0 !important', 'options' => $pfCoberturas, 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('pf_acomodacao_id', ['options' => $pfAcomodacoes, 'label' => 'Tipo de Acomodação', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
</div>
<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('pf_atendimento_id', ['label' => 'Área de Atendimento', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?php $modalidades = array('INDIVIDUAL' => 'INDIVIDUAL', 'ADESAO' => 'ADESÃO'); ?>
        <?= $this->Form->input('modalidade', ['options' => $modalidades, 'label' => 'Modalidade', 'empty' => 'SELECIONE', 'required' => 'required', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">

        <?php $filtroreembolso = array('N' => 'SEM REEMBOLSO', 'S' => 'COM REEMBOLSO'); ?>
        <?= $this->Form->input('reembolso', ['options' => $filtroreembolso, 'label' => 'Possui Reembolso', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>

    <div class="col-md-3 fonteReduzida">
        <?php $coparticipacao = array('s' => 'Sim', 'n' => 'Não'); ?>
        <?= $this->Form->input('coparticipacao', ['options' => $coparticipacao, 'empty' => 'SELECIONE', 'label' => 'Possui Coparticipação?', 'required' => 'required']); ?>
    </div>
</div>
<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-3 fonteReduzida" id="detalheCoparticipacao">
        <?= $this->Form->input('detalhe_coparticipacao', ['label' => 'Detalhes Coparticipação. Exemplo ("10%", "FN10", etc..) ', 'style' => 'margin-bottom: 0 !important']); ?>
    </div>
    <div id="odontoValor" style="display: none;">
        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('odonto_operadora', ['options' => $odonto_operadoras, 'empty' => 'Selecione Operadora']); ?>
        </div>
        <div class="col-md-3 fonteReduzida">
            <label for="odonto_valor" id="label-odonto">Preço Odonto por Vida </label>
            <?= $this->Form->input('odonto_valor', ['label' => false, 'style' => 'margin-bottom: 0 !important', 'type' => 'number', 'step' => '0.01']); ?>
        </div>
    </div>
</div>
<!-- PREÇOS -->

<div class="spacer-md">&nbsp;</div>
<div id="faixas" class="col-md-12 centralizada fonteReduzida">
    <?php if (!$is_vitalmed) : ?>
        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa1', ['label' => '0 à 18 anos', 'type' => 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa1]); ?>
            <label for="check1">
                <?= $this->Form->checkbox('check1', ["id" => "check1", "class" => "consulta", "campo" => "faixa1", "tabindex" => "1"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa2', ['label' => '19 à 23 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa2]); ?>
            <label for="check2">
                <?= $this->Form->checkbox('check2', ["id" => "check2", "class" => "consulta", "campo" => "faixa2", "tabindex" => "2"]); ?>
                So consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa3', ['label' => '24 à 28 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa3]); ?>
            <label for="check3">
                <?= $this->Form->checkbox('check3', ["id" => "check3", "class" => "consulta", "campo" => "faixa3", "tabindex" => "3"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa4', ['label' => '29 à 33 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa4]); ?>
            <label for="check4">
                <?= $this->Form->checkbox('check4', ["id" => "check4", "class" => "consulta", "campo" => "faixa4", "tabindex" => "4"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa5', ['label' => '34 à 38 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa5]); ?>
            <label for="check5">
                <?= $this->Form->checkbox('check5', ["id" => "check5", "class" => "consulta", "campo" => "faixa5", "tabindex" => "5"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa6', ['label' => '39 à 43 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa6]); ?>
            <label for="check6">
                <?= $this->Form->checkbox('check6', ["id" => "check6", "class" => "consulta", "campo" => "faixa6", "tabindex" => "6"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa7', ['label' => '44 à 48 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa7]); ?>
            <label for="check7">
                <?= $this->Form->checkbox('check7', ["id" => "check7", "class" => "consulta", "campo" => "faixa7", "tabindex" => "7"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa8', ['label' => '49 à 53 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa8]); ?>
            <label for="check8">
                <?= $this->Form->checkbox('check8', ["id" => "check8", "class" => "consulta", "campo" => "faixa8", "tabindex" => "8"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa9', ['label' => '54 à 58 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa9]); ?>
            <label for="check9">
                <?= $this->Form->checkbox('check9', ["id" => "check9", "class" => "consulta", "campo" => "faixa9", "tabindex" => "9"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa10', ['label' => '59 à 64 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa10]); ?>
            <label for="check10">
                <?= $this->Form->checkbox('check10', ["id" => "check10", "class" => "consulta", "campo" => "faixa10", "tabindex" => "10"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa11', ['label' => '65 à 80 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa11]); ?>
            <label for="check11">

                <?= $this->Form->checkbox('check11', ["id" => "check11", "class" => "consulta", "campo" => "faixa11", "tabindex" => "11"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa12', ['label' => '+ de 81 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $pfTabela->faixa12]); ?>
            <label for="check12">
                <?= $this->Form->checkbox('check12', ["id" => "check12", "class" => "consulta", "campo" => "faixa12", "tabindex" => "12"]); ?>
                Sob consulta
            </label>
        </div>
    <?php else : ?>
        <div class="col-md-1">
            <?= $this->Form->control('tipo_faixa', ['label' => 'Alterar Faixas', 'options' => ['6' => 'Faixa com 6', '3' => 'Faixa com 3'], 'empty' => 'Selecione']) ?>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="spacer-md">&nbsp;</div>
        <div id="faixa_6">
            <div class="faixaEtaria col-md-2">
                <?= $this->Form->input('faixa_extra_1', ['label' => '0 à 14 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
                <label for="check_extra_1">
                    <?= $this->Form->checkbox('check_extra_1', ["id" => "check_extra_1", "class" => "consulta", "campo" => "faixa_extra_1", "tabindex" => "1", "value" => (isset($pfTabela) ? $pfTabela->faixa_extra_1 : '')]); ?>
                    Sob consulta
                </label>
            </div>

            <div class="faixaEtaria col-md-2">
                <?= $this->Form->input('faixa_extra_2', ['label' => '15 à 29 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
                <label for="check_extra_2">
                    <?= $this->Form->checkbox('check_extra_2', ["id" => "check_extra_2", "class" => "consulta", "campo" => "faixa_extra_2", "tabindex" => "2", "value" => (isset($pfTabela) ? $pfTabela->faixa_extra_2 : '')]); ?>
                    Sob consulta
                </label>
            </div>

            <div class="faixaEtaria col-md-2">
                <?= $this->Form->input('faixa_extra_3', ['label' => '30 à 39 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
                <label for="check_extra_3">
                    <?= $this->Form->checkbox('check_extra_3', ["id" => "check_extra_3", "class" => "consulta", "campo" => "faixa_extra_3", "tabindex" => "3", "value" => (isset($pfTabela) ? $pfTabela->faixa_extra_3 : '')]); ?>
                    Sob consulta
                </label>
            </div>

            <div class="faixaEtaria col-md-2">
                <?= $this->Form->input('faixa_extra_4', ['label' => '40 à 49 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
                <label for="check_extra_4">
                    <?= $this->Form->checkbox('check_extra_4', ["id" => "check_extra_4", "class" => "consulta", "campo" => "faixa_extra_4", "tabindex" => "4", "value" => (isset($pfTabela) ? $pfTabela->faixa_extra_4 : '')]); ?>
                    Sob consulta
                </label>
            </div>

            <div class="faixaEtaria col-md-2">
                <?= $this->Form->input('faixa_extra_5', ['label' => '50 à 58 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
                <label for="check_extra_5">
                    <?= $this->Form->checkbox('check_extra_5', ["id" => "check_extra_5", "class" => "consulta", "campo" => "faixa_extra_5", "tabindex" => "5", "value" => (isset($pfTabela) ? $pfTabela->faixa_extra_5 : '')]); ?>
                    Sob consulta
                </label>
            </div>

            <div class="faixaEtaria col-md-2">
                <?= $this->Form->input('faixa_extra_6', ['label' => '+ de 58 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
                <label for="check_extra_6">
                    <?= $this->Form->checkbox('check_extra_6', ["id" => "check_extra_6", "class" => "consulta", "campo" => "faixa_extra_6", "tabindex" => "6", "value" => (isset($pfTabela) ? $pfTabela->faixa_extra_6 : '')]); ?>
                    Sob consulta
                </label>
            </div>
        </div>
        <div id="faixa_3">
            <div class="faixaEtaria col-md-2 col-md-offset-3">
                <?= $this->Form->input('faixa_promo_1', ['label' => '18 à 39 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
                <label for="check_promo_1">
                    <?= $this->Form->checkbox('check_promo_1', ["id" => "check_promo_1", "class" => "consulta", "campo" => "faixa_promo_1", "tabindex" => "6", "value" => (isset($pfTabela) ? $pfTabela->faixa_promo_1 : '')]); ?>
                    Sob consulta
                </label>
            </div>
            <div class="faixaEtaria col-md-2">
                <?= $this->Form->input('faixa_promo_2', ['label' => '40 à 58 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
                <label for="check_promo_2">
                    <?= $this->Form->checkbox('check_promo_2', ["id" => "check_promo_2", "class" => "consulta", "campo" => "faixa_promo_2", "tabindex" => "6", "value" => (isset($pfTabela) ? $pfTabela->faixa_promo_2 : '')]); ?>
                    Sob consulta
                </label>
            </div>
            <div class="faixaEtaria col-md-2">
                <?= $this->Form->input('faixa_promo_3', ['label' => '+ de 59 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
                <label for="check_promo_3">
                    <?= $this->Form->checkbox('check_promo_3', ["id" => "check_promo_3", "class" => "consulta", "campo" => "faixa_promo_3", "tabindex" => "6", "value" => (isset($pfTabela) ? $pfTabela->faixa_promo_3 : '')]); ?>
                    Sob consulta
                </label>
            </div>
        </div>

    <?php endif; ?>
</div>
<!-- PREÇOS -->

<!-- NOVOS RELACIONAMENTOS -->
<div class="col-xs-12 clearfix">&nbsp;</div>
<h4 class="centralizada">Novas Associações</h4>
<div class="col-md-12 fonteReduzida" id="field-associacoes">
    <div class="col-md-3">
        <?= $this->Form->control('pf_rede_id', ['empty' => 'SELECIONE REDE', 'options' => isset($pf_redes) && !empty($pf_redes) ? $pf_redes : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('pf_observacao_id', ['empty' => 'SELECIONE OBSERVACAO', 'options' => isset($pf_observacoes) && !empty($pf_observacoes) ? $pf_observacoes : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('pf_reembolso_id', ['empty' => 'SELECIONE REEMBOLSO', 'options' => isset($pf_reembolsos) && !empty($pf_reembolsos) ? $pf_reembolsos : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('pf_carencia_id', ['empty' => 'SELECIONE CARENCIA', 'options' => isset($pf_carencias) && !empty($pf_carencias) ? $pf_carencias : '']) ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-3">
        <?= $this->Form->control('pf_dependente_id', ['empty' => 'SELECIONE DEPENDENTE', 'options' => isset($pf_dependentes) && !empty($pf_dependentes) ? $pf_dependentes : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('pf_documento_id', ['empty' => 'SELECIONE DOCUMENTO', 'options' => isset($pf_documentos) && !empty($pf_documentos) ? $pf_documentos : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('pf_formas_pagamento_id', ['empty' => 'SELECIONE FORMAS PAGAMENTO', 'options' => isset($pf_formas_pagamentos) && !empty($pf_formas_pagamentos) ? $pf_formas_pagamentos : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('pf_entidades_grupo_id', ['empty' => 'SELECIONE GRUPO ENTIDADE', 'options' => isset($pf_entidades_grupos) && !empty($pf_entidades_grupos) ? $pf_entidades_grupos : '']) ?>
    </div>
</div>
<!-- NOVOS RELACIONAMENTOS -->

<div class="col-xs-12 clearfix">&nbsp;</div>
<?= $this->element('botoesAdd') ?>
<div class="spacer-md">&nbsp;</div>

<?= $this->Form->end() ?>


<script type="text/javascript">
    $("#tipo-faixa").change(function() {
        changeFaixas($(this).val())
    })

    changeFaixas($("#tipo-faixa").val());

    function changeFaixas(value) {
        if (value) {
            if (value == '6') {
                $("#faixa_6").show();
                $("#faixa_3").hide();
            } else {
                $("#faixa_6").hide();
                $("#faixa_3").show();
            }
        }
    }
    $("#pf-area-comercializacao-id").change(function() {
        var comercializacao = $(this).val();
        $.ajax(`${baseUrl}pfTabelas/filtroAssociacoes/${comercializacao}`)
            .then(function(data) {
                $("#field-associacoes").empty();
                $("#field-associacoes").append(data);
            })
        $.ajax(`${baseUrl}pfTabelas/getFaixas/${comercializacao}`)
            .then(function(data) {
                $("#faixas").empty();
                $("#faixas").append(data);
            })
    })

    $(document).ready(function() {
        $("#acoes").hide();
        $("#entidades").hide();
        $("#detalheCoparticipacao").hide();
        $("#coparticipacao").change(function() {
            //           alert((this).value);
            if ((this).value === 's') {
                $("#detalheCoparticipacao").show();
            } else {
                $("#detalheCoparticipacao").hide();
            }
        });
    });
    $("#pf-cobertura-id").change(function() {
        if ($(this).val() == 4 || $(this).val() == 5 || $(this).val() == 6) {
            $("#odontoValor").show();
            $("#odonto-valor").prop('required', true);
            $("#odonto-operadora").prop('required', true);
        } else {
            $("#odontoValor").hide();
            $("#odonto-valor").removeAttr("required");
            $("#odonto-operadora").removeAttr("required");
        }
    });

    $("#estado-id").change(function() {
        let estado = $(this).val();
        $.ajax({
            type: 'POST',
            data: {
                'estado': estado
            },
            url: '<?= $this->request->webroot ?>pf-tabelas/selectOperadoras',
            success: function(data) {
                $("#operadoras").empty();
                $("#operadoras").append(data);
            }
        });
    });
</script>