<?= $this->Form->create('Tabelas', ['id' => 'prioridadeForm', 'class' => 'form form-validate', 'role' => 'form']); ?>
<?= $this->element('forms/title', ['title' => '<i class="fa fa-edit"></i> Alterar Ordem de Prioridade da Tabela']); ?>
<?= $this->Form->hidden('tabela_id', ['value' => $tabela_id]) ?>
<div class="card-body">

    <div class="row">
        <div class="col-md-12">
            <small> As tabelas serão exibidas nas simulações de acordo com sua <b>Ordem de Prioridade</b> definida</small>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= $this->Form->input('prioridade', ['label' => 'Ordem de Prioridade', "required", "type" => "number", "id" => "valorPrioridade", 'class' => 'form-control']); ?>
            </div>
        </div>
    </div>
</div>
<?= $this->element('forms/buttons') ?>
<?= $this->Form->end(); ?>