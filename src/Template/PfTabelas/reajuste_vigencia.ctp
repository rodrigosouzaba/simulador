<?= $this->Form->create('percentualReajuste', ['id' => 'percentualReajuste']); ?>

<div class="col-md-12 fonteReduzida">    
    <?php
    foreach ($tabelas as $tabela) {
        echo $this->Form->input('tabelas_id'.$tabela, ['type' => 'hidden', 'value' => $tabela]);
    }
    ?>
    <?= $this->Form->input('vigencia', ['label' => 'Vigência', 'type' => 'date']); ?>
</div>

<div class="col-md-12 fonteReduzida centralizada">
 <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => "btn btn-primary btn-md "]) ?>

   <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['controller' => 'Tabelas', 'action' => 'index'], ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false]);
    ?>   
</div>




