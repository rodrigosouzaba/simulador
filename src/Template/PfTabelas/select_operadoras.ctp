<div>
    <?= $this->Form->input('pf_operadora_id', ['options' => $operadoras, 'label' => 'Operadoras', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
</div>
<script>
    $("#pf-operadora-id").change(function() {
        $("#acoes").show();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>pf-tabelas/find_produtos/" + (this)
                .value,
            data: $("#pf-operadora-id").serialize(),
            success: function(data) {
                $("#produtos").empty();
                $("#produtos").append(data);
            }
        });
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>pf-tabelas/find_regioes/" + (this)
                .value + "/" + $("#estado-id").val(),
            data: $("#pf-operadora-id").serialize(),
            success: function(data) {
                $("#regiao").empty();
                $("#regiao").append(data);
            }
        });
    });
</script>