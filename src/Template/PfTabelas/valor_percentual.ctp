<?php echo $this->Form->create('buscarGrupo', ['class' => 'form form-validate', 'role' => 'form', 'id' => 'buscarGrupo']); ?>
<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Editar por pencentual']); ?>
<div class="card-body">
    <div class="row">

<!-- <?= $this->Form->create('buscarGrupo', ['id' => 'buscarGrupo']); ?> -->
<div class="col-md-4 col-md-offset-2">
    <h4 style="float: left;">Estado </h4> <div id="loadingMatriz" style="float: right;"></div>
    <?= $this->Form->input('estado_id', ['options' => $estados, 'label' => '', 'empty' => 'Selecione o ESTADO', 'disabled' => '', 'id' => 'estado-id']); ?>
</div>
<div class="col-md-4 fonteReduzida">    
    <h4 style="float: left;">Operadora </h4> <div id="loadingMatriz" style="float: right;"></div>
    <div id="operadoras">
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione a OPERADORA', 'disabled' => 'true']); ?>
    </div>
</div>
<div class="col-md-12 fonteReduzida" id="grupo">
    <div class="col-xs-12 centralizada">
        <h4>Defina o Grupo de Tabelas</h4>    
    </div>
    <div class="col-xs-2 col-xs-offset-1">
        <?php $contratacao = array('0' => 'OPCIONAL', '1' => 'COMPULSÓRIO', '2' => 'AMBOS') ?>
        <?= $this->Form->input('tipo_contratacao_id', ['options' => $contratacao, 'empty' => 'SELECIONE']); ?>
    </div>    
    <div class="col-xs-2">
        <?= $this->Form->input('pf_acomodacao_id', ['options' => $pfacomodacoes,'label' => 'Acomodação', 'empty' => 'SELECIONE']); ?>
    </div>    
    <div class="col-xs-2">
        <?= $this->Form->input('minimo_vidas', ['label' => 'Mínimo de Vidas', 'empty' => 'SELECIONE']); ?>

    </div>    
    <div class="col-xs-2">
        <?= $this->Form->input('maximo_vidas', ['label' => 'Máximo de Vidas', 'empty' => 'SELECIONE']); ?>
    </div>    
    <div class="col-xs-2">
        <?php $co = array('s' => 'SIM', 'n' => 'NÃO') ?>
        <?= $this->Form->input('coparticipacao', ['options' => $co, 'empty' => 'SELECIONE']); ?>
    </div>    
    <div class="col-xs-12 centralizada">
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-search', 'aria-hidden' => 'true']) . ' Buscar Tabelas', '#', ['class' => 'btn btn-md btn-primary ', 'role' => 'button', 'escape' => false, 'id' => 'buscar']);
        ?>
    </div>    
</div>
<!-- <?= $this->Form->end() ?> -->

<div id="mini-loader" class="centralizada col-xs-12" style="display: none; padding-top: 50px;">
    <?= $this->Html->image('ciranda.gif', ['alt' => 'Carregando...']);?>
</div>
<div id="tabelas" class="fonteReduzida col-xs-12">

</div>

</div>
<?php echo $this->element('forms/buttonsFechar') ?>
<?php echo $this->Form->end(); ?>

