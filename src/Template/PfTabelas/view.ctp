<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pf Tabela'), ['action' => 'edit', $pfTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pf Tabela'), ['action' => 'delete', $pfTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pf Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Produtos'), ['controller' => 'PfProdutos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Produto'), ['controller' => 'PfProdutos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Operadora'), ['controller' => 'PfOperadoras', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Atendimentos'), ['controller' => 'PfAtendimentos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Atendimento'), ['controller' => 'PfAtendimentos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Acomodacoes'), ['controller' => 'PfAcomodacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Acomodaco'), ['controller' => 'PfAcomodacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pf Profissoes Tabelas'), ['controller' => 'PfProfissoesTabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pf Profissoes Tabela'), ['controller' => 'PfProfissoesTabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pfTabelas view large-9 medium-8 columns content">
    <h3><?= h($pfTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($pfTabela->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Produto') ?></th>
            <td><?= $pfTabela->has('pf_produto') ? $this->Html->link($pfTabela->pf_produto->id, ['controller' => 'PfProdutos', 'action' => 'view', $pfTabela->pf_produto->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Operadora') ?></th>
            <td><?= $pfTabela->has('pf_operadora') ? $this->Html->link($pfTabela->pf_operadora->id, ['controller' => 'PfOperadoras', 'action' => 'view', $pfTabela->pf_operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Atendimento') ?></th>
            <td><?= $pfTabela->has('pf_atendimento') ? $this->Html->link($pfTabela->pf_atendimento->id, ['controller' => 'PfAtendimentos', 'action' => 'view', $pfTabela->pf_atendimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Pf Acomodaco') ?></th>
            <td><?= $pfTabela->has('pf_acomodaco') ? $this->Html->link($pfTabela->pf_acomodaco->id, ['controller' => 'PfAcomodacoes', 'action' => 'view', $pfTabela->pf_acomodaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Validade') ?></th>
            <td><?= h($pfTabela->validade) ?></td>
        </tr>
        <tr>
            <th><?= __('Estado') ?></th>
            <td><?= $pfTabela->has('estado') ? $this->Html->link($pfTabela->estado->id, ['controller' => 'Estados', 'action' => 'view', $pfTabela->estado->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Regio') ?></th>
            <td><?= $pfTabela->has('regio') ? $this->Html->link($pfTabela->regio->id, ['controller' => 'Regioes', 'action' => 'view', $pfTabela->regio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Cod Ans') ?></th>
            <td><?= h($pfTabela->cod_ans) ?></td>
        </tr>
        <tr>
            <th><?= __('Coparticipacao') ?></th>
            <td><?= h($pfTabela->coparticipacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Detalhe Coparticipacao') ?></th>
            <td><?= h($pfTabela->detalhe_coparticipacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Modalidade') ?></th>
            <td><?= h($pfTabela->modalidade) ?></td>
        </tr>
        <tr>
            <th><?= __('Reembolso') ?></th>
            <td><?= h($pfTabela->reembolso) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pfTabela->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa1') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa1) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa2') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa2) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa3') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa3) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa4') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa4) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa5') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa5) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa6') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa6) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa7') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa7) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa8') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa8) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa9') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa9) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa10') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa10) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa11') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa11) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa12') ?></th>
            <td><?= $this->Number->format($pfTabela->faixa12) ?></td>
        </tr>
        <tr>
            <th><?= __('Minimo Vidas') ?></th>
            <td><?= $this->Number->format($pfTabela->minimo_vidas) ?></td>
        </tr>
        <tr>
            <th><?= __('Maximo Vidas') ?></th>
            <td><?= $this->Number->format($pfTabela->maximo_vidas) ?></td>
        </tr>
        <tr>
            <th><?= __('Prioridade') ?></th>
            <td><?= $this->Number->format($pfTabela->prioridade) ?></td>
        </tr>
        <tr>
            <th><?= __('Vigencia') ?></th>
            <td><?= h($pfTabela->vigencia) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($pfTabela->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pf Profissoes Tabelas') ?></h4>
        <?php if (!empty($pfTabela->pf_profissoes_tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Pf Profissao Id') ?></th>
                <th><?= __('Pf Tabela Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pfTabela->pf_profissoes_tabelas as $pfProfissoesTabelas): ?>
            <tr>
                <td><?= h($pfProfissoesTabelas->id) ?></td>
                <td><?= h($pfProfissoesTabelas->pf_profissao_id) ?></td>
                <td><?= h($pfProfissoesTabelas->pf_tabela_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PfProfissoesTabelas', 'action' => 'view', $pfProfissoesTabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PfProfissoesTabelas', 'action' => 'edit', $pfProfissoesTabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PfProfissoesTabelas', 'action' => 'delete', $pfProfissoesTabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pfProfissoesTabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
