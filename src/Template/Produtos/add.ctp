<?= $this->Form->create($produto, ['class' => "form-inline"]) ?>
<div class="fonteReduzida col-xs-12">

    <div class="col-xs-6">
        <?= $this->Form->input('nome'); ?>
    </div>

    <div class="col-xs-6">
        <?= $this->Form->input('operadora_id', ['options' => $operadoras, 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-xs-12">
        <?= $this->Form->input('descricao', ['label' => 'Descrição']) ?>
    </div>
    <?= $this->element('botoesAdd') ?>
</div>
<?= $this->Form->end() ?>
