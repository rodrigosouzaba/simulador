<?= $this->Form->create($produto, ['class' => "form-inline"]) ?>

<div class="col-md-6 fonteReduzida">
    <?php
    echo $this->Form->input('nome');

    ?>
</div>
<div class="col-md-6 fonteReduzida">
    <?= $this->Form->input('operadora_id', ['options' => $operadoras, 'empty' => 'SELECIONE', 'required' => 'required']); ?>
</div>
<div class="col-md-12 fonteReduzida">
    <?= $this->Form->input('descricao') ?>
</div>
<?= $this->element('botoesAdd') ?>
<?= $this->Form->end() ?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#acoes").hide();


        $("#operadora-id").change(function() {
            ////            alert($("#operadora-id").val() !==);
            if ($("#operadora-id").val() !== 0 || $("#operadora-id").val() !== null) {
                $.ajax({
                    type: "POST",
                    url: baseUrl + "produtos/findInfo/" + $("#operadora-id").val(),
                    data: $("#operadora-id").serialize(),
                    success: function(data) {
                        $("#infoAdicional").empty();
                        $("#infoAdicional").append(data);
                    }
                });
            } else {
                $("#acoes").hide();
            }
        });
        $("#operadora-id").change(function() {

            $("#acoes").show();
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelas/findProdutos/" + (this).value,
                data: $("#operadora-id").serialize(),
                success: function(data) {
                    $("#produtos").empty();
                    $("#produtos").append(data);
                }
            });
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelas/findRegioes/" + (this).value +
                    "/" + $("#estado-id").val(),
                data: $("#operadora-id").serialize(),
                success: function(data) {
                    $("#regiao").empty();
                    $("#regiao").append(data);
                }
            });



        });


    });

    //    $('.collapse').collapse({aria - expanded = "false"});
</script>
