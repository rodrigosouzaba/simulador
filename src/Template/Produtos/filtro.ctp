<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Produto', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['value' => $estado, 'options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?php if (empty($operadoras)) : ?>
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
    <?php else : ?>
        <?= $this->Form->input('operadora_id', ['value' =>  $operadora, 'options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
    <?php endif; ?>
</div>

<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<?= $this->Form->create('selecaoTabelas', ['id' => 'selecaoTabelas']); ?>
<div class="col-xs-12">
    <?php if (isset($filtradas)) {
        if (!empty($filtradas)) { ?>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th class="centralizada" style="width:10%"><small><?= 'Selecionar' ?></small></th>
                        <th><?= 'Nome' ?></th>
                        <th><?= 'Descrição' ?></th>
                        <th><?= 'Operadora' ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($filtradas as $produto) : ?>
                        <tr>
                            <td class="centralizada"><?= $this->Form->input($produto->id, ['type' => 'checkbox', 'id' => 'imprimir' . $produto->id, 'value' => $produto->id, 'class' => 'noMarginBottom', 'title' => 'Selecionar', 'label' => '']); ?></td>

                            <td><?= h($produto->nome) ?></td>
                            <td><?= h($produto->descricao) ?></td>
                            <td>
                                <?php
                                //                        debug($produto);die();
                                if (isset($produto->operadora->imagen->caminho)) {
                                    echo $this->Html->image("../" . $produto->operadora->imagen->caminho . "/" . $produto->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $produto->operadora->detalhe;
                                } else {
                                    echo $produto->operadora->nome . " - " . $produto->operadora->detalhe;
                                }
                                ?>
                            </td>


                            <td class="actions">
                                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $produto->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Produto', 'sid' => 'produtos.edit',]) ?>
                                <?php
                                if ($sessao['role'] == 'admin') {
                                    echo $this->Form->postLink('', ['action' => 'delete', $produto->id], ['confirm' => __('Confirma exclusão do Produto?', $produto->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'produtos.delete',]);
                                }
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="col-md-12">
                <?=
                    $this->Html->link($this->Html->tag('span', '', [
                        'class' => 'fa fa-trash',
                        'aria-hidden' => 'true'
                    ]) . ' Deletar Produtos', '#', [
                        'class' => 'btn btn-sm btn-danger',
                        'role' => 'button',
                        'escape' => false,
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Deletar Produtos',
                        'sid' => 'produtos.deleteLote',
                        'id' => 'deleteLote'
                    ])
                ?>
            </div>
        <?php
        } else {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-md-12 text-danger centralizada"> NENHUM PRODUTO ENCONTRADO</div>
    <?php
        }
    }
    ?>


    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = "<?= $this->request->webroot . $this->request->controller ?>/filtro/" + $(this).val();
    });

    $("#operadora-id").change(function() {
        window.location.href = "<?= $this->request->webroot . $this->request->controller ?>/filtro/" + $("#estados").val() + "/" + $(this).val();
    });

    $("#deleteLote").click(function() {
        $.ajax({
            type: "post",
            data: $("#selecaoTabelas").serialize(),
            url: "<?php echo $this->request->webroot ?>produtos/deleteLote/",
            success: function(data) {
                window.location = '<?php echo $this->request->webroot ?>produtos/';

            }
        });
    });
</script>