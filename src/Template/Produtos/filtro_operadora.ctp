<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<?= $this->Form->create('selecaoTabelas', ['id' => 'selecaoTabelas']); ?>

<?php
//debug($produtos);

if (!empty($produtos)) {
    ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th class="centralizada" style="width:10%"><small><?= 'Selecionar' ?></small></th>
                <th><?= 'Nome' ?></th>
                <th><?= 'Descrição' ?></th>
                <th><?= 'Operadora' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($produtos as $produto): ?>
                <tr>
                    <td class="centralizada"><?= $this->Form->input($produto->id, ['type' => 'checkbox', 'id' => 'imprimir' . $produto->id, 'value' => $produto->id, 'class' => 'noMarginBottom', 'title' => 'Selecionar', 'label' => '']); ?></td>

                    <td><?= h($produto->nome) ?></td>
                    <td><?= h($produto->descricao) ?></td>
                    <td>
                        <?php
//                        debug($produto);die();
                        if (isset($produto->operadora->imagen->caminho)) {
                            echo $this->Html->image("../" . $produto->operadora->imagen->caminho . "/" . $produto->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $produto->operadora->detalhe;
                        } else {
                            echo $produto->operadora->nome . " - " . $produto->operadora->detalhe;
                        }
                        ?>
                    </td>


                    <td class="actions">
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $produto->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Produto']) ?>
                        <!--
                                <?= $this->Form->postLink('', ['action' => 'delete', $produto->id], ['confirm' => __('Confirma exclusão do Produto?'), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash p']); ?>
                        -->
                        <?= $this->Html->link('', '#', ['title' => 'Excluir', 'class' => 'btn btn-sm btn-danger fa fa-trash delete', 'value' => $produto->id]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="col-md-12">  
        <?=
        $this->Html->link($this->Html->tag('span', '', [
                    'class' => 'fa fa-trash',
                    'aria-hidden' => 'true']) . ' Deletar Produtos', '#', [
            'class' => 'btn btn-sm btn-danger',
            'role' => 'button',
            'escape' => false,
            'data-toggle' => 'tooltip',
            'data-placement' => 'top',
            'title' => 'Deletar Produtos',
            'id' => 'deleteLote'])
        ?>
    </div>
    <?php
} else {
    ?>
    <div class="clearfix">&nbsp;</div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12 text-danger centralizada"> NENHUM PRODUTO ENCONTRADO</div>
    <?php
}
?>


<?= $this->Form->end() ?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#operadora-id").change(function () {

            $.ajax({
                type: "POST",
                url: baseUrl + "produtos/filtroOperadora/" + $("#operadora-id").val(),
                data: $("#operadora-id").serialize(),
                success: function (data) {
                    $("#listaProdutos").empty();
                    $("#listaProdutos").append(data);
                }
            });

        });

    });
    $("#deleteLote").click(function () {
        $.ajax({
            type: "post",
            data: $("#selecaoTabelas").serialize(),
            url: "<?php echo $this->request->webroot ?>produtos/deleteLote/",
            success: function (data) {
                window.location = '<?php echo $this->request->webroot ?>produtos/';

            }
        });
    });
    $(".delete").click(function () {
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>produtos/delete/"+$(this).attr('value'),
            success: function (data) {
               $("#operadora-id").change();
               alert('Produto excluído com sucesso.');
            }
        });
    });
</script>
