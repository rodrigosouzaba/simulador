<div class="col-md-3 fonteReduzida">
    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Produto', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'produtos.add']);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
</div>

<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = baseUrl + "Produtos/filtro/" + $(this).val()
    });
</script>