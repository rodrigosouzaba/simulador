
<div class="produtos view large-9 medium-8 columns content">
    <h3><?= h($produto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($produto->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Operadora') ?></th>
            <td><?= $produto->has('operadora') ? $this->Html->link($produto->operadora->id, ['controller' => 'Operadoras', 'action' => 'view', $produto->operadora->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($produto->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($produto->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tabelas') ?></h4>
        <?php if (!empty($produto->tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Produto Id') ?></th>
                <th><?= __('Acomodacao') ?></th>
                <th><?= __('Vigencia') ?></th>
                <th><?= __('Faixa1') ?></th>
                <th><?= __('Faixa2') ?></th>
                <th><?= __('Faixa3') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($produto->tabelas as $tabelas): ?>
            <tr>
                <td><?= h($tabelas->id) ?></td>
                <td><?= h($tabelas->nome) ?></td>
                <td><?= h($tabelas->descricao) ?></td>
                <td><?= h($tabelas->produto_id) ?></td>
                <td><?= h($tabelas->acomodacao) ?></td>
                <td><?= h($tabelas->vigencia) ?></td>
                <td><?= h($tabelas->faixa1) ?></td>
                <td><?= h($tabelas->faixa2) ?></td>
                <td><?= h($tabelas->faixa3) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Tabelas', 'action' => 'view', $tabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Tabelas', 'action' => 'edit', $tabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tabelas', 'action' => 'delete', $tabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
