<style>
        .nav-tabs > li.active > a, .nav-tabs > li.active > a > span, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:focus > span, .nav-tabs > li.active > a:hover{color: #fff !important; background-color: #003C55 !important}
        .nav-tabs > li > a{border-radius: 4px !important; border: 1px solid #ddd !important }
        .nav-tabs > li > a:hover, .nav-tabs > li > a:hover > span{background-color: #c2c2c2 !important}
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover > span ,.nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{border-bottom-color: #ddd !important;background-color: #003C55 !important}
        .topo{
	        text-transform: uppercase;
	        font-weight: bold;
	        text-align: center;
        }
</style>
    
<?= $this->Form->create($prospect,["id" => "novo-prospect"]) ?>

	    <div class="clearfix">&nbsp;</div>
	    <div class="fonteReduzida">
			<div class="col-xs-12 col-md-6">
				<?= $this->Form->input('nome'); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<?= $this->Form->input('sobrenome'); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<?= $this->Form->input('email'); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				Tipo de Pessoa
				<?= $this->Form->radio('tipo_pessoa',["PF" => "Pessoa Física", "PJ" => "Pessoa Jurídica"],["class" => "radio-inline"]); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<?= $this->Form->input('celular'); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<?= $this->Form->input('telefone'); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<?= $this->Form->input('celular_secundario'); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<?= $this->Form->input('whatsapp'); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<?= $this->Form->input('cpf'); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				Possui SUSEP?
				<?= $this->Form->radio('susep',["S" => "Sim","N" => "Não"], ["class" => "pf"]); ?>
			</div>
			<div class="col-xs-6">
				<?= $this->Form->input('data_nascimento', ['empty' => true, "class" => "pf datepicker", "type" => "text"]); ?>
			</div>
		</div>



<div class="clearfix">&nbsp;</div>
<div class="clearfix centralizada">
    <?= $this->Html->link("Salvar", "#",["class" => "btn btn-primary btn-md", "id" => "salvar-prospect"]) ?>
    <?= $this->Html->link("Cancelar", "#", ["class" => "btn btn-default btn-md", "data-dismiss" => "modal", "id" => "fechar-novo-prospect"]) ?>
</div>
    <?= $this->Form->end() ?>
    
    
<script type="text/javascript">
$(".celular").mask("(99) 9 9999-9999");
$(".cpf").mask("999.999.999-99");
$(".cnpj").mask("99.999.999/9999-99");
$(".telefone").mask("(99) 9999-9999");

    
$(function() {
	$( ".datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true
	});
});

$("#pessoa-fisica").click(function () {
	$(".pj").val('');
});	
$("#pessoa-juridica").click(function () {
	$(".pf").val('');
});	
	
$("#salvar-prospect").click(function () {
    $.ajax({
        type: "POST",
        url: "<?php echo $this->request->webroot.$this->request->controller ?>/add",
        data: $("#novo-prospect").serialize(),
        success: function (data) {
			alert("Salvo com Sucesso");
            $("#fechar-novo-prospect").click();
            location.reload();
        }
    });
});
</script>