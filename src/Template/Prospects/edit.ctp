<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $prospect->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $prospect->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Prospects'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="prospects form large-9 medium-8 columns content">
    <?= $this->Form->create($prospect) ?>
    <fieldset>
        <legend><?= __('Edit Prospect') ?></legend>
        <?php
            echo $this->Form->input('email');
            echo $this->Form->input('celular');
            echo $this->Form->input('nome');
            echo $this->Form->input('sobrenome');
            echo $this->Form->input('data_nascimento', ['empty' => true]);
            echo $this->Form->input('cpf');
            echo $this->Form->input('cnpj');
            echo $this->Form->input('susep');
            echo $this->Form->input('telefone');
            echo $this->Form->input('celular_secundario');
            echo $this->Form->input('whatsapp');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
