<div class="col-xs-12 col-md-10">
	<h3 class="mobile"><?= __('Prospects') ?></h3>
</div>
<div class="col-xs-12 col-md-2 botao" style="margin-top: 20px;">
	<?= $this->Html->link('Novo Prospect', "#", ['class' => 'btn btn-danger btn-sm right mobile', "id" => "addProspect","data-target" => "#modal-add-prospect", "data-toggle" => "modal"]) ?>
</div>
    <table cellpadding="0" cellspacing="0" class="table table-condensed">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('created', ["label" => "Criado"]) ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th>Contatos</th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('data_nascimento') ?></th>
                <th><?= $this->Paginator->sort('cpf') ?></th>
                <th><?= $this->Paginator->sort('cnpj') ?></th>
                <th><?= $this->Paginator->sort('susep') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($prospects as $prospect): ?>
            <tr>
                <td><?= h($prospect->created) ?></td>
                <td></td>
                <td class="fonteReduzida" style="border-top: none">
	                <?= "Celular: ".$prospect->celular."<br>" ?>
	                <?= ($prospect->celular_secundario ? "Alternativo: ".$prospect->celular_secundario."<br>" : "") ?>
	                <?= ($prospect->telefone ? "Telefone: ".$prospect->telefone."<br>" : "") ?>
	                <?= ($prospect->whatsapp ? "Whatsapp: ".$prospect->whatsapp."<br>" : "") ?>
	                <?= ($prospect->email ? "E-mail: ".$prospect->email : "") ?>
                </td>
                <td><?= $prospect->nome." ".$prospect->sobrenome ?></td>
                <td><?= h($prospect->data_nascimento) ?></td>
                <td><?= h($prospect->cpf) ?></td>
                <td><?= h($prospect->cnpj) ?></td>
                <td><?= h($prospect->susep) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $prospect->id]) ?>
                    <?= $this->Html->link(__('Edit'), "#",['class' => 'editProspect', "value" => $prospect->id, "data-target" => "#modal-add-prospect", "data-toggle" => "modal"]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $prospect->id], ['confirm' => __('Are you sure you want to delete # {0}?', $prospect->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal-add-prospect" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Novo(a)</h4>
      </div>
      <div class="modal-body" id="modalResposta">
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">
if ($(window).width() <= 800) {
    $(".mobile").attr('style', 'text-align: center !important');
    $(".mobile").removeClass('right');
    $(".botao").attr('style', "margin-top: 0 !important;text-align: center");

}


	$("#addProspect").click(function () {
		$(".modal-title").text("Novo Prospect");
	    $.ajax({
	        type: "get",
	        url: "<?php echo $this->request->webroot.$this->request->controller ?>/add",
	        success: function (data) {
				$("#modalResposta").empty();
	            $("#modalResposta").append(data);
	        }
	    });

	});
	
	$(".editProspect").click(function () {
		$(".modal-title").text("Editar Prospect");
	    $.ajax({
	        type: "get",
	        url: "<?php echo $this->request->webroot.$this->request->controller ?>/edit/"+ $(this).attr("value"),
	        success: function (data) {
				$("#modalResposta").empty();
	            $("#modalResposta").append(data);
	        }
	    });

	});
</script>
