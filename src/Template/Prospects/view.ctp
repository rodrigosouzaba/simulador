<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Prospect'), ['action' => 'edit', $prospect->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Prospect'), ['action' => 'delete', $prospect->id], ['confirm' => __('Are you sure you want to delete # {0}?', $prospect->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Prospects'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Prospect'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="prospects view large-9 medium-8 columns content">
    <h3><?= h($prospect->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($prospect->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Celular') ?></th>
            <td><?= h($prospect->celular) ?></td>
        </tr>
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($prospect->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Sobrenome') ?></th>
            <td><?= h($prospect->sobrenome) ?></td>
        </tr>
        <tr>
            <th><?= __('Cpf') ?></th>
            <td><?= h($prospect->cpf) ?></td>
        </tr>
        <tr>
            <th><?= __('Cnpj') ?></th>
            <td><?= h($prospect->cnpj) ?></td>
        </tr>
        <tr>
            <th><?= __('Susep') ?></th>
            <td><?= h($prospect->susep) ?></td>
        </tr>
        <tr>
            <th><?= __('Telefone') ?></th>
            <td><?= h($prospect->telefone) ?></td>
        </tr>
        <tr>
            <th><?= __('Celular Secundario') ?></th>
            <td><?= h($prospect->celular_secundario) ?></td>
        </tr>
        <tr>
            <th><?= __('Whatsapp') ?></th>
            <td><?= h($prospect->whatsapp) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($prospect->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($prospect->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Data Nascimento') ?></th>
            <td><?= h($prospect->data_nascimento) ?></td>
        </tr>
    </table>
</div>
