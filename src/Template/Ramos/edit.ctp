<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ramo->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ramo->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ramos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ramos form large-9 medium-8 columns content">
    <?= $this->Form->create($ramo) ?>
    <fieldset>
        <legend><?= __('Edit Ramo') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
