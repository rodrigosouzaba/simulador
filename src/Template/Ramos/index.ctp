<?=
$this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Ramo', ['controller' => 'Ramos', 'action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addRamo']);
?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <!--<th><?= $this->Paginator->sort('Código') ?></th>-->
                <th><?= $this->Paginator->sort('nome') ?></th>
                <!--<th><?= $this->Paginator->sort('descricao') ?></th>-->
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ramos as $ramo): ?>
            <tr>
                <!--<td><?= $this->Number->format($ramo->id) ?></td>-->
                <td><?= h($ramo->nome) ?></td>
                <!--<td><?= h($ramo->descricao) ?></td>-->
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ramo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ramo->id], ['confirm' => __('Tem certeza que deseja excluir o Ramo Código # {0}?', $ramo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>

<!--

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ramo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ramos index large-9 medium-8 columns content">
    <h3><?= __('Ramos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ramos as $ramo): ?>
            <tr>
                <td><?= $this->Number->format($ramo->id) ?></td>
                <td><?= h($ramo->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ramo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ramo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ramo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ramo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>-->
