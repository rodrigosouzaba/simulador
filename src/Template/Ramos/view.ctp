<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ramo'), ['action' => 'edit', $ramo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ramo'), ['action' => 'delete', $ramo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ramo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ramos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ramo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ramos view large-9 medium-8 columns content">
    <h3><?= h($ramo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($ramo->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($ramo->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($ramo->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tabelas') ?></h4>
        <?php if (!empty($ramo->tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Vigencia') ?></th>
                <th><?= __('Faixa1') ?></th>
                <th><?= __('Faixa2') ?></th>
                <th><?= __('Faixa3') ?></th>
                <th><?= __('Faixa4') ?></th>
                <th><?= __('Faixa5') ?></th>
                <th><?= __('Faixa6') ?></th>
                <th><?= __('Faixa7') ?></th>
                <th><?= __('Faixa8') ?></th>
                <th><?= __('Faixa9') ?></th>
                <th><?= __('Faixa10') ?></th>
                <th><?= __('Produto Id') ?></th>
                <th><?= __('Operadora Id') ?></th>
                <th><?= __('Ramo Id') ?></th>
                <th><?= __('Abrangencia Id') ?></th>
                <th><?= __('Tipo Id') ?></th>
                <th><?= __('Modalidade Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($ramo->tabelas as $tabelas): ?>
            <tr>
                <td><?= h($tabelas->id) ?></td>
                <td><?= h($tabelas->nome) ?></td>
                <td><?= h($tabelas->descricao) ?></td>
                <td><?= h($tabelas->vigencia) ?></td>
                <td><?= h($tabelas->faixa1) ?></td>
                <td><?= h($tabelas->faixa2) ?></td>
                <td><?= h($tabelas->faixa3) ?></td>
                <td><?= h($tabelas->faixa4) ?></td>
                <td><?= h($tabelas->faixa5) ?></td>
                <td><?= h($tabelas->faixa6) ?></td>
                <td><?= h($tabelas->faixa7) ?></td>
                <td><?= h($tabelas->faixa8) ?></td>
                <td><?= h($tabelas->faixa9) ?></td>
                <td><?= h($tabelas->faixa10) ?></td>
                <td><?= h($tabelas->produto_id) ?></td>
                <td><?= h($tabelas->operadora_id) ?></td>
                <td><?= h($tabelas->ramo_id) ?></td>
                <td><?= h($tabelas->abrangencia_id) ?></td>
                <td><?= h($tabelas->tipo_id) ?></td>
                <td><?= h($tabelas->modalidade_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Tabelas', 'action' => 'view', $tabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Tabelas', 'action' => 'edit', $tabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tabelas', 'action' => 'delete', $tabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
