<div class="col-md-3 fonteReduzida">
    <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Observação', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'redes.add']);
    ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?= $this->Form->input('estados', ['value' => $estado, 'options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>
<div class="col-md-3 fonteReduzida">
    <?php if (empty($operadoras)) : ?>
        <?= $this->Form->input('operadora_id', ['options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
    <?php else : ?>
        <?= $this->Form->input('operadora_id', ['value' =>  $operadora, 'options' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
    <?php endif; ?>
</div>

<?= $this->Form->create('selecaoTabelas', ['id' => 'selecaoTabelas']); ?>
<div class="col-xs-12">
    <?php if (isset($redes)) {
        if (!empty($redes)) { ?>
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th><?= 'Nome' ?></th>
                        <th><?= 'Descrição' ?></th>
                        <th><?= 'Operadora' ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($redes as $rede) : ?>
                        <tr>

                            <td><?= h($rede->nome) ?></td>
                            <td><?= h($rede->descricao) ?></td>
                            <td>
                                <?php
                                //                        debug($rede);die();
                                if (isset($rede->operadora->imagen->caminho)) {
                                    echo $this->Html->image("../" . $rede->operadora->imagen->caminho . "/" . $rede->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $rede->operadora->detalhe;
                                } else {
                                    echo $rede->operadora->nome . " - " . $rede->operadora->detalhe;
                                }
                                ?>
                            </td>


                            <td class="actions">
                                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $rede->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'redes.edit']) ?>
                                <?= $this->Form->postLink('', ['action' => 'delete', $rede->id], ['confirm' => __('Confirma exclusão?', $rede->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'redes.delete'])?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php
        } else {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-md-12 text-danger centralizada"> NENHUM PRODUTO ENCONTRADO</div>
    <?php
        }
    }
    ?>


    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
    $("#estados").change(function() {
        window.location.href = "<?= $this->request->webroot . $this->request->controller ?>/filtro/" + $(this).val();
    });

    $("#operadora-id").change(function() {
        window.location.href = "<?= $this->request->webroot . $this->request->controller ?>/filtro/" + $("#estados").val() + "/" + $(this).val();
    });
</script>