<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('id') ?></th>-->
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($redes as $rede) : ?>
            <tr>
                <td><?= $rede->nome ?></td>
                <td><?= $rede->descricao ?></td>
                <td>
                    <?php
                    //                        debug($produto);die();
                    if (isset($rede->operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $rede->operadora->imagen->caminho . "/" . $rede->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $rede->operadora->detalhe;
                    } else {
                        echo $rede->operadora->nome . " - " . $rede->operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $rede->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'redes.edit']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $rede->id], ['confirm' => __('Confirma exclusão?', $rede->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid' => 'redes.delete'])?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
