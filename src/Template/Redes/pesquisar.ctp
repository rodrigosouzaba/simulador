<?php foreach ($redes as $key => $rede) : ?>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default col-xs-12" style="margin: 5px 0;">

            <!-- PANEL HEADING -->
            <div class="panel-heading" role="tab" id="heading<?= $key ?>" style="min-height: 100px; background-color: #fff !important;">
                <!-- ABRIR CALCULOS -->
                <div class="col-xs-12 col-md-2 vermais" style="padding-right: 0 !important;padding-left: 0 !important;">
                    <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $key ?>" aria-expanded="true" aria-controls="collapse<?= $key ?>">
                        <i class="fas fa-chevron-down" aria-hidden="true" style="font-size: 110% !important;"></i>
                        <span style="font-size: 14px !important;">Exibir Rede</span>
                    </a>
                </div>
                <div class="col-xs-12 col-md-offset-4 col-md-6 centralizada">
                    <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $key ?>" aria-expanded="true" aria-controls="collapse<?= $key ?>">
                        <?= $this->Html->image("../" . $rede->operadora->imagen->caminho . "/" . $rede->operadora->imagen->nome, ['class' => 'logoSimulacao']); ?>
                        <?= "<p class='detalheOperadora'>" . $rede->operadora->detalhe . "</p>"; ?>
                    </a>
                </div> <!-- /PANEL HEADING -->
            </div>
            <!-- REDE CREDENCIADA -->
            <div class="collapse col-xs-12" id="collapse<?= $key ?>">
                <div class="nobreak">
                    <div class="clearfix">&nbsp;</div>
                    <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                        REDE CREDENCIADA (Resumo) <span><?= "link" ?></span>
                    </div>
                    <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida descricao" id="descricao">
                        <?= $rede->descricao ?>
                    </div>
                </div>
                <!-- /REDE CREDENCIADA -->
            </div>
        </div>
    </div>
    </div>
<?php endforeach; ?>
<script>
    $(".descricao").each(function() {
        let palavra = "<?= $palavra; ?>";
        var text = $(this).text();
        regex = new RegExp(palavra, 'g');
        var text = text.replace(regex, "<strong style='text-transform: uppercase' font-size: 90% !important>" +
            palavra + "</strong>");
        $(this).empty();
        $(this).append(text);
    });
</script>