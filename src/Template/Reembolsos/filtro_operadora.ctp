<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <!--<th><?= $this->Paginator->sort('id') ?></th>-->
            <th><?= 'Título' ?></th>
            <th style="width: 50% !important"><?= 'Observação' ?></th>
            <th><?= 'Operadora' ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($reembolsos as $reembolso) : ?>
            <tr>
                <td><?= $reembolso->nome ?></td>
                <td><?= $reembolso->descricao ?></td>
                <td>
                    <?php
                    if (isset($reembolso->operadora->imagen->caminho)) {
                        echo $this->Html->image("../" . $reembolso->operadora->imagen->caminho . "/" . $reembolso->operadora->imagen->nome, ['class' => 'logoMiniatura']) . "  " . $reembolso->operadora->detalhe;
                    } else {
                        echo $reembolso->operadora->nome . " - " . $reembolso->operadora->detalhe;
                    }
                    ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $reembolso->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid'=>'reembolsos.edit']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $reembolso->id], ['confirm' => __('Confirma exclusão?', $reembolso->id), 'title' => __('Deletar'), 'class' => 'btn btn-sm btn-danger fa fa-trash', 'sid'=>'reembolsos.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>