<?php $this->assign('title', $title); ?>
<?= $this->Form->create($regio) ?>
<div class="col-md-12 fonteReduzida">
    <?= $this->Form->input('nome') ?>
</div>
<div class="col-md-6 fonteReduzida">
    <?= $this->Form->input('estado_id', ['empty' => 'SELECIONE']) ?>
</div>
<div class="col-md-6 fonteReduzida" id="operadoras">
    <?= $this->Form->input('operadora_id', ['empty' => 'SELECIONE', 'disabled' => 'true']) ?>
</div>
<div class="clearfix">&nbsp;</div>
<div id="local_municipios">
    <div class="col-md-5">
        <?= $this->Form->input('lista-municipios', ['options' => $municipios, 'multiple' => 'multiple']) ?>
    </div>
    <div class="col-md-2 centralizada">
        <div class="clearfix">&nbsp;</div>
        <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-arrow-right', 'aria-hidden' => 'true']), '#sel_prof', ['class' => 'add btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'title' => 'Adicionar']);
        ?>
        <br />
        <br />
        <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-arrow-left', 'aria-hidden' => 'true']), '#sel_prof', ['class' => 'remove btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'title' => 'Remover']);
        ?>
    </div>
    <div class="col-md-5">
        <?= $this->Form->input('lista_municipios_escolhidos', ['options' => $municipios_tabela, 'multiple' => 'multiple', 'label' => 'Municípios da Área de Comercialização', 'style' => 'margin-bottom: 0 !important']); ?>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#lista-municipios-escolhidos').children().prop("selected", true);
            $('#salvar').click(function() {
                $('#lista-municipios-escolhidos').children().prop("selected", true);
                $('#lista-municipios-escolhidos option').sort();
            });
            $('.add').click(function() {
                $('#lista-municipios option:selected').appendTo('#lista-municipios-escolhidos');
                ordenarSelect();
            });
            $('.remove').click(function() {
                $('#lista-municipios-escolhidos option:selected').appendTo('#lista-municipios');
                ordenarSelect();
            });
            $('.add-all').click(function() {
                $('#lista-municipios option').appendTo('#lista-municipios-escolhidos');
            });
            $('.remove-all').click(function() {
                $('#lista-municipios-escolhidos option').appendTo('#lista-municipios');
            });
        });

        function ordenarSelect() {
            $("#lista-municipios-escolhidos").html($("option", $("#lista-municipios-escolhidos")).sort(function(a, b) {
                return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
            }));
        }
    </script>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-12 fonteReduzida">
    <?= $this->Form->input('descricao', ['label' => 'Observação']) ?>
</div>
<?= $this->element('botoesAdd') ?>
<?= $this->Form->end() ?>
<script>
    $("#estado-id").change(function() {
        estado = $(this).val();

        //GET OPERADORAS
        $.ajax({
            type: 'POST',
            url: baseUrl + "regioes/filtroOperadoras/" + estado,
            success: function(data) {
                $("#operadoras").empty();
                $("#operadoras").append(data);
            }
        })

        //GET MUNICIPIOS
        $.ajax({
            type: "GET",
            url: baseUrl + "regioes/getMunicipios/" + estado,
            success: function(data) {
                $("#local_municipios").empty();
                $("#local_municipios").append(data);
                $("#local_municipios").show();

            }
        });
    });
</script>
