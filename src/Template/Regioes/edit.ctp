<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $regio->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $regio->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Regioes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="regioes form large-9 medium-8 columns content">
    <?= $this->Form->create($regio) ?>
    <fieldset>
        <legend><?= __('Edit Regio') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('estado_id', ['options' => $estados]);
            echo $this->Form->input('tabelas._ids', ['options' => $tabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
