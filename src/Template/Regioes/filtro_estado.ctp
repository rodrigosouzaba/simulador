<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('Estado') ?></th>
            <th><?= $this->Paginator->sort('Operadora') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($regioes as $regio): ?>
            <tr>
                <td><?= $regio->nome ?></td>
                <td><?= $regio->estado->nome?></td>
                <td><?= $regio->operadora->nome?></td>
                <td class="actions">
                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $regio->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'regioes.edit']) ?>                   
                 <?= $this->Form->postLink($this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']), ['action' => 'delete', $regio->id], ['title' => 'Deletar', 'class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false,'confirm' => __('Are you sure you want to delete # {0}?', $regio->id), 'sid' => 'regioes.delete']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>