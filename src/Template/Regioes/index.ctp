<section id="AppRegioes">
    <div class="row">
        <?php echo $this->Form->create('search', ['id' => 'pesquisarRegiao', 'class' => 'form', 'role' => 'form']); ?>
            <div class="col-md-3 fonteReduzida">
                <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Área de Comercialização', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'salvar', 'sid' => 'regioes.add']); ?>
            </div>

            <div class="col-md-3 fonteReduzida">
                <div class="form-group ">
                    <?= $this->Form->input('estados', ['options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
                </div>
            </div>
        <?php echo $this->Form->end(); ?>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div id="selecaoRegioes">
                <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('nome') ?></th>
                            <th><?= $this->Paginator->sort('Estado') ?></th>
                            <th><?= $this->Paginator->sort('Operadora') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($regioes as $regio): ?>
                        <tr>
                            <td><?= $regio->nome ?></td>
                            <td><?= $regio->estado->nome?></td>
                            <td><?= $regio->operadora->nome?></td>
                            <td class="actions">
                                <?= $this->Html->link($this->Html->tag('i','',['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $regio->id], ['role' => 'button', 'escape' => false, 'class' => 'btn btn-sm btn-default', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar', 'sid' => 'regioes.edit']) ?>
                                <?= $this->Form->postLink($this->Html->tag('i','',['class' => 'fa fa-trash', 'aria-hidden' => 'true']), ['action' => 'delete', $regio->id], ['title' => 'Deletar', 'role' => 'button', 'escape' => false, 'class' => 'btn btn-sm btn-default', 'confirm' => __('Are you sure you want to delete # {0}?', $regio->id), 'sid' => 'regioes.delete']) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                    <p><?= $this->Paginator->counter() ?></p>
                </div>
            </div>
        </div>
    </div>
</section>