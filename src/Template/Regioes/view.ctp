<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Regio'), ['action' => 'edit', $regio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Regio'), ['action' => 'delete', $regio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $regio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Regioes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Regio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="regioes view large-9 medium-8 columns content">
    <h3><?= h($regio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Estado') ?></th>
            <td><?= $regio->has('estado') ? $this->Html->link($regio->estado->id, ['controller' => 'Estados', 'action' => 'view', $regio->estado->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($regio->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Nome') ?></h4>
        <?= $this->Text->autoParagraph(h($regio->nome)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tabelas') ?></h4>
        <?php if (!empty($regio->tabelas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Produto Id') ?></th>
                <th><?= __('Acomodacao') ?></th>
                <th><?= __('Vigencia') ?></th>
                <th><?= __('Faixa1') ?></th>
                <th><?= __('Faixa2') ?></th>
                <th><?= __('Faixa3') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($regio->tabelas as $tabelas): ?>
            <tr>
                <td><?= h($tabelas->id) ?></td>
                <td><?= h($tabelas->nome) ?></td>
                <td><?= h($tabelas->descricao) ?></td>
                <td><?= h($tabelas->produto_id) ?></td>
                <td><?= h($tabelas->acomodacao) ?></td>
                <td><?= h($tabelas->vigencia) ?></td>
                <td><?= h($tabelas->faixa1) ?></td>
                <td><?= h($tabelas->faixa2) ?></td>
                <td><?= h($tabelas->faixa3) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Tabelas', 'action' => 'view', $tabelas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Tabelas', 'action' => 'edit', $tabelas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tabelas', 'action' => 'delete', $tabelas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
