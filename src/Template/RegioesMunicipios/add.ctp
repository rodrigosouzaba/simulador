<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Regioes Municipios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="regioesMunicipios form large-9 medium-8 columns content">
    <?= $this->Form->create($regioesMunicipio) ?>
    <fieldset>
        <legend><?= __('Add Regioes Municipio') ?></legend>
        <?php
            echo $this->Form->input('municipio_id', ['options' => $municipios]);
            echo $this->Form->input('tabela_id', ['options' => $tabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
