<?= $this->Form->create($regioesMunicipio) ?>
<?= $this->Form->input('pf_tabela_id', ['type' => 'hidden', 'value' => $tabela->id]) ?>
<div id="local_municipios">
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-5">
        <?= $this->Form->input('lista-municipios', ['options' => $municipios, 'multiple' => 'multiple']) ?>
    </div>
    <div class="col-md-2 centralizada">
        <div class="clearfix">&nbsp;</div>
        <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-arrow-right', 'aria-hidden' => 'true']), '#sel_prof', ['class' => 'add btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'title' => 'Adicionar']);
        ?>
        <br />
        <br />
        <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-arrow-left', 'aria-hidden' => 'true']), '#sel_prof', ['class' => 'remove btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'title' => 'Remover']);
        ?>
    </div>
    <div class="col-md-5">
        <?= $this->Form->input('lista_municipios_escolhidos', ['options' => $municipios_tabela, 'multiple' => 'multiple', 'label' => 'Municípios da Área de Comercialização', 'style' => 'margin-bottom: 0 !important', 'required' => 'required']); ?>
    </div>
</div>

<div class="spacer-md">&nbsp</div>
<?= $this->element('botoesAdd') ?>

<?= $this->Form->end() ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#lista-municipios-escolhidos').children().prop("selected", true);
        $('#salvar').click(function() {
            $('#lista-municipios-escolhidos').children().prop("selected", true);
            $('#lista-municipios-escolhidos option').sort();
        });
        $('.add').click(function() {
            $('#lista-municipios option:selected').appendTo('#lista-municipios-escolhidos');
            ordenarSelect();
        });
        $('.remove').click(function() {
            $('#lista-municipios-escolhidos option:selected').appendTo('#lista-municipios');
        });
        $('.add-all').click(function() {
            $('#lista-municipios option').appendTo('#lista-municipios-escolhidos');
        });
        $('.remove-all').click(function() {
            $('#lista-municipios-escolhidos option').appendTo('#lista-municipios');
        });
    });

    function ordenarSelect() {
        $("#lista-municipios-escolhidos").html($("option", $("#lista-municipios-escolhidos")).sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
        }));
    }
</script>