<?= $this->Form->input('operadora_id', ['opitions' => $operadoras, 'label' => '', 'empty' => 'Selecione OPERADORA']); ?>
<script type="text/javascript">
    $("#operadora-id").change(function() {
        $.ajax({
            type: "POST",
            url: "<?= $this->request->webroot ?>regioes-municipios/filtro-operadora/" + $("#operadora-id").val(),
            beforeSend: function() {
                $("#loader").click();
            },
            success: function(data) {
                $("#fechar").click();
                $("#resposta").empty();
                $("#resposta").append(data);
            }
        });
    });
</script>