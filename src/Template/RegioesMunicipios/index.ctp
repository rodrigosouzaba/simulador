<div class="col-xs-3 col-xs-offset-3 fonteReduzida">
    <?= $this->Form->input('estados', ['opitions' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
</div>
<div id="operadoras" class="col-xs-3 fonteReduzida">
    <?= $this->Form->input('operadora_id', ['opitions' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
</div>

<div id="resposta" class="col-xs-12">
</div>

<?= $this->element('processando'); ?>
<script type="text/javascript">
    $("#estados").change(function() {
        $.ajax({
            type: "POST",
            url: "<?= $this->request->webroot ?>regioes-municipios/filtro-estado/" + $("#estados").val(),
            beforeSend: function() {
                $("#loader").click();
            },
            success: function(data) {
                $("#fechar").click();
                $("#operadoras").empty();
                $("#operadoras").append(data);
            }
        });
    });
</script>