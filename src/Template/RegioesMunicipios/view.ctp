<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Regioes Municipio'), ['action' => 'edit', $regioesMunicipio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Regioes Municipio'), ['action' => 'delete', $regioesMunicipio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $regioesMunicipio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Regioes Municipios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Regioes Municipio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="regioesMunicipios view large-9 medium-8 columns content">
    <h3><?= h($regioesMunicipio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Municipio') ?></th>
            <td><?= $regioesMunicipio->has('municipio') ? $this->Html->link($regioesMunicipio->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $regioesMunicipio->municipio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tabela') ?></th>
            <td><?= $regioesMunicipio->has('tabela') ? $this->Html->link($regioesMunicipio->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $regioesMunicipio->tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($regioesMunicipio->id) ?></td>
        </tr>
    </table>
</div>
