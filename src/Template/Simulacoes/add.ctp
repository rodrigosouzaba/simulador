<?= $this->Form->create($simulacao, ['id' => 'simulacao', 'url' => '/simulacoes/cotar', 'class' => 'form form-validate']) ?>
<?= $this->request->action == 'edit' ?  $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Editar Cálculos']) : '' ?>
<div class="clearfix">&nbsp;</div>

<?= $this->Form->hidden('id'); ?>

<div class="col-xs-12">
    <div class="tituloField centralizada">
        Dados do Cálculo
    </div>

    <div class="clearfix">&nbsp;</div>

    <div class="faixaEtaria col-md-3 col-xs-12">
        <?= $this->Form->select('estado_id', $estados, ['label' => false, "required" => "required", 'default' => $sessao["estado_id"], 'class' => 'fonteReduzida estadoPME', 'empty' => 'ESTADO DE COMERCIALIZAÇÃO']); ?>
    </div>

    <div class="faixaEtaria col-md-3 form-inline centralizada col-xs-12">
        <?= $this->Form->input('tipo_cnpj_id', ['label' => false, "required" => "required", 'options' => $cnpjs, 'empty' => 'TIPO DE CNPJ', 'class' => 'fonteReduzida']); ?>
    </div>

    <div class="faixaEtaria col-md-6 col-xs-12" style="margin-top: -10px !important">
        <?= $this->Form->input('familia', ['type' => 'checkbox', 'label' => 'Este Cálculo é referente a um Grupo Familiar (Cônjuge e Filhos)?']); ?>
        <div class='text-danger' style="padding-left: 20px;margin-top: -14px !important"><b>*</b> Informação necessária
            para evitar preços divergentes</div>
    </div>
    <div class="clearfix">&nbsp;</div>

    <div class="faixaEtaria col-md-3 col-xs-12">
        <?= $this->Form->input('nome', ['label' => false, 'placeholder' => 'Nome da Empresa (opcional)']); ?>
    </div>

    <div class="faixaEtaria col-md-3 col-xs-12">
        <?= $this->Form->input('contato', ['label' => false, 'placeholder' => 'Pessoa de Contato (opcional)']); ?>
    </div>

    <div class="faixaEtaria col-md-3 col-xs-12">
        <?= $this->Form->input('email', ['label' => false, "type" => "text", 'placeholder' => 'E-mail (opcional)']); ?>
    </div>

    <div class="faixaEtaria col-md-3 col-xs-12">
        <?= $this->Form->input('telefone', ['label' => false, 'placeholder' => 'Telefone (opcional)']); ?>
    </div>

    <div class="clearfix">&nbsp;</div>

    <?= $this->element('calculadora_idade'); ?>

    <div class="clearfix">&nbsp;</div>

    <!-- Quantidade de Vidas -->

    <div class="faixaEtaria col-md-1 col-xs-12 col-md-offset-1">
        <?= $this->Form->input('faixa1', ['placeholder' => '0 a 18 anos', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>

    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('faixa2', ['placeholder' => '19 a 23 anos', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>

    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('faixa3', ['placeholder' => '24 a 28 anos', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>

    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('faixa4', ['placeholder' => '29 a 33 anos', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>

    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('faixa5', ['placeholder' => '34 a 38 anos', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>

    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('faixa6', ['placeholder' => '39 a 43 anos', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>

    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('faixa7', ['placeholder' => '44 a 48 anos', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>

    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('faixa8', ['placeholder' => '49 a 53 anos', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>

    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('faixa9', ['placeholder' => '54 a 58 anos', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>

    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('faixa10', ['placeholder' => '59 ou +', 'required' => false, 'class' => 'centralizada', 'label' => false]); ?>
    </div>
</div>

<div class="clearfix">&nbsp;</div>

<?php echo $this->element('forms/buttons') ?>
<?= $this->Form->end() ?>


<script type="text/javascript">
    $('#simular').hide();
    $('#myTabs a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    })
    $('input:radio[name=ramo_id]:nth(0)').attr('checked', true);
    $('input:radio[name=modalidade_id]:nth(0)').attr('checked', true);

    $('#myModal').on('shown.bs.modal', function() {
        $('#myInput').focus()
    });

    $("#grupototal").click(function() {

        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>simulacoes/grupototal/",
            //            data: $("#operadora-id").serialize(),
            success: function(data) {
                $("#total").empty();
                $("#total").append(data);

            }
        });
        $('#simular').show();
    });

    $("#grupofamiliar").click(function() {

        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>simulacoes/grupofamiliar/",
            //            data: $("#operadora-id").serialize(),
            success: function(data) {
                $("#familias").empty();
                $("#familias").append(data);

            }
        });
        $('#simular').show();
    });

    $("#operadora-id").change(function() {
        $.ajax({
            type: "Post",
            url: "<?php echo $this->request->webroot ?>simulacoes/preencheSelect/o/" + (this).value,
            data: $("#operadora-id").serialize(),
            success: function(data) {
                $("#selectProdutos").empty();
                $("#selectProdutos").append(data);

            }
        });
    });

    $("#salvar").click(function() {
        $.ajax({
            type: "post",
            data: $("#simulacao").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/emitirSimulacao/" + $("#simulacao")
                .serialize(),
            success: function(data) {
                $("#tabelas").empty();
                $("#tabelas").append(data);
            }
        });
    });

    // $("#enviar").click(function() {
    //     if ($("#tipo-cnpj-id").val() != '' && $("#estado-id").val() != '') {
    //         $('#loader').trigger('click');
    //     }
    // });
</script>