<style type="text/css">
    .jumbotron {
        background-color: #eee;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 10px;
        line-height: 1.5;
        width: 100%;
        /*width: 97%;*/
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }


    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
        font-size: 60%;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .totalTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
        font-weight: bold;
    }

    .fonteReduzida {
        font-size: 10px;
        text-align: justify !important;
        text-align: justify;
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        line-height: 1.3;
        margin-bottom: 0;
        cursor: default;
        display: block;
        font-style: normal;
        font-variant-caps: normal;
        vertical-align: baseline;
        white-space: normal;

    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .nobreak {
        page-break-inside: avoid;
        width: 100% !important;
    }

    .info {
        color: #a9a9a9;
        font-weight: lighter;
        font-style: italic;
        text-align: center;
        width: 100%;
    }

    .verde {
        color: #128c7e;
    }

    .pagina {
        padding: 20px;
        margin-top: 170px;
    }

    @media(max-width: 767px) {
        .pagina {
            margin-top: 160px;
        }
    }

    @media(min-width: 768px) and (max-width: 895px) {
        .pagina {
            margin-top: 250px;
        }
    }

    @media(min-width: 896px) and (max-width: 1218px) {
        .pagina {
            margin-top: 200px;
        }
    }
</style>
<?php
$texto = "Visualize sua cotação aqui: https://corretorparceiro.com.br/app/simulacoes/cotacao/";
$linkWhats = 'https://wa.me?text=' . $texto . $filtro->id;
?>
<?= $this->Form->create('dadosPDF', ['id' => 'dadosPDF', 'url' => ['controller' => 'simulacoes', 'action' => 'pdf']]); ?>
<?= $this->Form->input("simulacao_id", ["type" => "hidden", "value" => $simulacao['id']]); ?>
<?= $this->Form->input("observacao", ["type" => "hidden", "value" => $simulacao['observaco']]); ?>
<?= $this->Form->input("rede", ["type" => "hidden", "value" => $simulacao['rede']]); ?>
<?= $this->Form->input("reembolso", ["type" => "hidden", "value" => $simulacao['reembolso']]); ?>
<?= $this->Form->input("carencia", ["type" => "hidden", "value" => $simulacao['carencia']]); ?>
<?= $this->Form->input("documento", ["type" => "hidden", "value" => $simulacao['documento']]); ?>
<?php
if (isset($filtroPDF)) :
?>
    <?= $this->Form->input("abrangencia_id", ["type" => "hidden", "value" => $filtroPDF['abrangencia_id']]); ?>
    <?= $this->Form->input("tipo_id", ["type" => "hidden", "value" => $filtroPDF['tipo_id']]); ?>
    <?= $this->Form->input("tipo_produto_id", ["type" => "hidden", "value" => $filtroPDF['tipo_produto_id']]); ?>
    <?= $this->Form->input("coparticipacao", ["type" => "hidden", "value" => $filtroPDF['coparticipacao']]); ?>
    <?= $this->Form->input("filtroreembolso", ["type" => "hidden", "value" => $filtroPDF['filtroreembolso']]); ?>
<?php
endif;
foreach ($findTabelas as $key => $dadoPDF) {
    if ($key != "simulacao_id" && $key != "abrangencia_id" && $key != "tipo_id" && $key != "tipo_produto_id" && $key != "coparticipacao" && $key != "filtroreembolso" && substr($key, 0, 5) != "check" && $key != "observacao" && $key != "rede" && $key != "reembolso" && $key != "carencia" && $key != "documento") {
        if ($dadoPDF != 0) {
            echo $this->Form->input($key, ["type" => "hidden", "value" => $key]);
        }
    }
}
?>
<!-- HIDDEN FIELD COM O TIPO DE AÇÃO REFERENTE AO PDF (DOWNLOAD ou GERAÇÃO NO BROWSER) VIA jQuery -->
<?= $this->Form->input("tipopdf", ["type" => "hidden", "value" => ""]); ?>
<?= $this->Form->end(); ?>

<?php if (isset($sessao)) : ?>
    <?= $this->element('new_acoes'); ?>
<?php else : ?>
    <div class="col-xs-12 centralizada well well-sm" id="acoesgerador">
        <!-- DOWNLOAD DO PDF -->
        <?= $this->Form->button($this->Html->tag('span', '', ['class' => 'fa fa-download', 'aria-hidden' => 'true']) . " Download", ['class' => ' btn btn-lg btn-default', "id" => "pdfdownload", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, "data-toggle" => "tooltip", "data-placement" => "bottom", "title" => "Download do PDF"]); ?>
    </div>
<?php endif; ?>
<div class="container">
    <div id="header">
        <?php echo $this->element('cabecalho_cotacao'); ?>
    </div>
    <?php foreach ($tabelasOrdenadas as $chave => $tabelas) {
    ?>
        <br>

        <!--<div class="jumbotron" style="padding: 20px !important;">-->
        <table>
            <tr>
                <td style="font-size: 115%; text-align: justify; line-height: 16px; width: 100%;">
                    À <b><?= $simulacao['nome'] ?></b>, A/C: <b><?= $simulacao['contato'] ?></b>.<br>
                    Agradecemos pela oportunidade. Segue abaixo as simulações dos planos para análise.<br>
                    Informamos que os valores, regras de comercialização e condições contratuais são determinadas <br>
                    pelas seguradoras/operadoras e podem ser alterados pelas mesmas a qualquer momento.
                </td>
            </tr>
        </table>


        <!--</div>-->


        <?php
        $faixas = null;
        $i = 1;
        for ($i = 1; $i <= 12; $i++) {
            if ($simulacao['faixa' . $i] > 0) {
                $faixas = $faixas + 1;
            }
        }
        $largura = 100 / ($faixas + 1);

        $tamanhoFonte = 'font-size:95%'; ?>

        <br /> <br />
        <?php
        foreach ($tabelas as $tabelasOperadora) {
            foreach ($tabelasOperadora as $tabelaIndividual) {
                switch ($tabelaIndividual['tipo_cnpj']) {
                    case "0":
                        $cnpj = "Para CNPJ Padrão";
                        break;
                    case "1":
                        $cnpj = "CEI";
                        break;
                    case "2":
                        $cnpj = "MEI";
                        break;
                    case "3":
                        $cnpj = "Para CNPJ padrão, CEI ou MEI";
                        break;
                } ?>
                <?php $dados = $tabelaIndividual['minimo_vidas'] . " à " . $tabelaIndividual['maximo_vidas'] . " vidas - " . $cnpj; ?>

        <?php
                break;
            }
            break;
        } ?>
        <table style="width: 100%" cellspacing="0">
            <tr>
                <td style="width: 15%; text-align: left; font-weight:bold;">
                    <img src='<?= $this->request->webroot . $operadoras[$chave]['imagen']['caminho'] . $operadoras[$chave]['imagen']['nome']; ?>' height="40" /><br />
                </td>
                <td style="text-align: left; font-weight:bold;">
                    <?= $operadoras[$chave]['nome'] . '<br/><small style="font-weight: 100">' . $operadoras[$chave]['detalhe'] . "</small>" ?>
                </td>

                <td style="text-align: right;">
                    <span style="margin-left: 88px;"><?= $dados ?></span>
                </td>
            </tr>
        </table>



        <br />
        <?php $corfundo = '#eee'; ?>
        <table style="width: 100%; border:  solid 0.1mm #ddd;<?= $tamanhoFonte ?>" cellspacing="0">
            <tr style="width: 100% !important; ">
                <?php
                $i = 1;
                for ($i = 1; $i <= 12; $i++) {
                    if ($simulacao['faixa' . $i] > 0) {
                ?>
                        <td style="border: solid 0.1mm #dddddd;text-align:center; width: <?= $largura ?>%;<?= $tamanhoFonte ?>">
                            <?php
                            switch ($i) {
                                case 1:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                    break;
                                case 2:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                    break;
                                case 3:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                    break;
                                case 4:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                    break;
                                case 5:
                                    echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                    break;
                                case 6:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                    break;
                                case 7:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                    break;
                                case 8:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                    break;
                                case 9:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                    break;
                                case 10:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 à 64";
                                    break;
                                case 11:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 65 à 80";
                                    break;
                                case 12:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> + de 81";
                                    break;
                            } ?>

                        </td>

                <?php
                    }
                } ?>
                <td class="negrito" style=" border:solid 0.1mm #dddddd;width: <?= $largura ?>%;text-align:center;<?= $tamanhoFonte ?>">
                    <?php
                    $x = 1;
                    $vidas = 0;
                    for ($x = 1; $x <= 12; $x++) {
                        $vidas = $vidas + $simulacao['faixa' . $x];
                    }
                    echo "Total:<br>" . $vidas . " Vida(s)"; ?>
                </td>
            </tr>
            <?php
            foreach ($tabelas as $produto) {
                foreach ($produto as $produto) {
            ?>
                    <tr>
                        <td></td>
                    </tr>
                    <tr style="background-color: #ccc; text-align: center;">
                        <td style="padding: 5px 0 5px 3px; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a" colspan="<?= $faixas + 1 ?>">
                            <?php
                            if ($produto['coparticipacao'] === 's') {
                                $co = "C/COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'];
                            } else {
                                $co = "S/COPARTICIPAÇÃO ";
                            } ?>
                            <?php
                            if ($produto['tipo_contratacao'] === '0') {
                                $tipo_contratacao = "OPCIONAL";
                            } elseif ($produto['tipo_contratacao'] === '1') {
                                $tipo_contratacao = "COMPULSÓRIO";
                            } else {
                                $tipo_contratacao = "";
                            } ?>


                            <?php
                            $tamanho = count($produto["tabelas_cnpjs"]);
                            $i = 1;
                            $cnpjFinal = "";
                            foreach ($produto["tabelas_cnpjs"] as  $cnpj) {
                                if ($i < $tamanho) {
                                    $cnpjFinal = $cnpjFinal . " " . $cnpj["cnpj"]["nome"] . ",";
                                } else {
                                    $cnpjFinal = $cnpjFinal . " " . $cnpj["cnpj"]["nome"];
                                }
                                $i++;
                            }

                            ?>

                            <?= "<b>" . $produto['produto']['nome'] . " - " . $produto['descricao'] . " - " . $tipo_contratacao . " " . $co . " - " . $produto["cobertura"]["nome"] . "</b> <br> " . $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas - " . $produto['tipo']['nome'] . " - " . $cnpjFinal ?>
                        </td>
                    </tr>
                    <tr style="background-color: #fff;border-left: solid 0.1mm #5a5a5a;  ">
                        <?php
                        $i = 1;


                        for ($i = 1; $i <= 12; $i++) {
                            if ($simulacao['faixa' . $i] > 0) {
                                if ($i == 1) {
                                    $bordaEsquerda = "border-left: solid 0.1mm #5a5a5a;";
                                } else {
                                    $bordaEsquerda = "border-left:solid 0.1mm #dddddd;";
                                } ?>
                                <td style="width: <?= $largura ?>%;<?= $bordaEsquerda ?>; text-align:center; border-bottom: solid 0.1mm #5a5a5a; padding: 10px 0; vertical-align: middle">
                                    <?php if ($produto['faixa' . $i] == 0) : ?>
                                        ***
                                    <?php else : ?>
                                        <?= $this->Number->currency($simulacao['faixa' . $i] * $produto['faixa' . $i]) ?>
                                        <br />

                                        <small style="font-size:75%"> <?= number_format($produto['faixa' . $i], 2, ",", ".") ?> p/ vida</small>
                                    <?php endif ?>

                                </td>
                        <?php
                            }
                        } ?>
                        <td style="font-weight: bold;width: <?= $largura ?>%; text-align: center;border-left:solid 0.1mm #dddddd;border-bottom: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a;<?= $tamanhoFonte ?>">
                            <?php
                            $i = 1;
                            $totalPorProduto = 0;
                            for ($i = 1; $i <= 12; $i++) {
                                $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * $produto['faixa' . $i]);
                            }
                            $operadorascomiof = array(1, 28, 29, 30, 31, 16);
                            if (in_array($produto['operadora']['id'], $operadorascomiof)) {
                            ?>
                                <?= $this->Number->currency($totalPorProduto) ?>
                                <?=
                                "<br>
                                + IOF: " . $this->Number->currency($totalPorProduto * 0.0238, "R$ ") . "<br>"
                                    . "<b>" . $this->Number->currency($totalPorProduto * 1.0238, "R$ ") . "</b>"
                                ?>
                            <?php
                            } else {
                            ?>

                                <b><?= $this->Number->currency($totalPorProduto) ?></b>
                            <?php
                            } ?>
                        </td>

                    </tr>
            <?php
                }
            } ?>
        </table>

        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">

            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                OBSERVAÇÕES IMPORTANTES
            </div>



            <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
                <?php
                foreach ($tabelas as $produto) {
                    foreach ($produto as $produto) {
                        echo ($produto['operadora']['observacoes'] != null) ? nl2br($produto['operadora']['observacoes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                        break;
                    }
                    break;
                } ?>
            </div>

        </div>

        <?php
        if ($simulacao['rede'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <?php
                foreach ($tabelas as $produto) {
                    foreach ($produto as $produto) {
                        //                                            debug($produto['operadora']);die();
                        if (isset($produto['operadora']['url_rede']) && $produto['operadora']['url_rede'] <> null) {
                            $linkRedeCompleta = "- Para rede completa " . $this->Html->link('clique aqui', $produto['operadora']['url_rede'], ['target' => "_blank"]) . ".";
                        } else {
                            $linkRedeCompleta = "";
                        }
                        break;
                    }
                    break;
                }

                foreach ($tabelas as $produto) {
                    foreach ($produto as $produto) {
                        if (isset($produto['operadora']['url_rede']) && $produto['operadora']['url_rede'] <> null) {
                            $linkRedeCompleta = "- Para rede completa " . $this->Html->link('clique aqui', $produto['operadora']['url_rede'], ['target' => "_blank"]) . ".";
                        } else {
                            $linkRedeCompleta = "";
                        }
                        break;
                    }
                    break;
                } ?>
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    REDE REFERENCIADA (Resumo) <span><?= $linkRedeCompleta ?></span>
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            echo ($produto['operadora']['redes'] != null) ? nl2br($produto['operadora']['redes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";

                            break;
                        }
                        break;
                    } ?>
                </div>

            </div>
            <div class="clearfix">&nbsp;</div>

        <?php
        } ?>

        <?php
        if ($simulacao['reembolso'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">

                <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                </div>


                <div style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;" class="fonteReduzida">

                    <?php
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            echo ($produto['operadora']['reembolsos'] != null) ? nl2br($produto['operadora']['reembolsos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                            break;
                        }
                        break;
                    } ?>
                </div>


            </div>


        <?php
        } ?>
        <div class="clearfix">&nbsp;</div>

        <div class="nobreak">
            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">

                <!-- /*             <div style="background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;"> */ -->
                ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
            </div>

            <div style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;" class="fonteReduzida">
                <?php
                foreach ($tabelas as $produto) {
                    foreach ($produto as $produto) {
                        echo ($produto['operadora']['regioes'][0] != null) ? "<b>COMERCIALIZAÇÃO: </b>" . $produto['operadora']['regioes'][0]['descricao'] : "<b>COMERCIALIZAÇÃO: </b><span class='info'>Consulte Operadora</span>";
                        break;
                    }
                    break;
                } ?>
                <br>
                <br>
                <b>ATENDIMENTO </b>
                <div class="fonteReduzida">
                    <br>
                    <?php
                    $redes = null;
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            $redes[$produto['produto']['descricao']] = $produto['regio']['descricao'] . " / " . $produto['abrangencia']['descricao'];
                        }
                    } ?>

                    <?php
                    foreach ($redes as $chave => $valor) {
                        $arr = explode("/", $valor, 2);
                        echo "<span><b>" . nl2br($chave) . ":</b> " . $arr[1] . "</span><br/>";
                    } ?>
                </div>
            </div>

        </div>
        <?php
        if ($simulacao['carencia'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">

                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;">
                    CARÊNCIAS (Resumo)
                </div>


                <div class="fonteReduzida" style="padding: 5px; border: 0.1mm solid #ccc;">
                    <!--<div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">-->
                    <?php
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            echo ($produto['operadora']['carencias'] != null) ? nl2br($produto['operadora']['carencias'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                            break;
                        }
                        break;
                    } ?>
                </div>

            </div>


        <?php
        } ?>
        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                DEPENDENTES
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php
                $redes = null;
                foreach ($tabelas as $produto) {
                    foreach ($produto as $produto) {
                        echo ($produto['operadora']['opcionais'] != null) ? nl2br($produto['operadora']['opcionais'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";

                        break;
                    }
                    break;
                } ?>
            </div>

            <div class="clearfix">&nbsp;</div>

        </div>
        <?php
        if ($simulacao['informacao'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">

                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                    DOCUMENTOS NECESSÁRIOS
                </div>


                <div class="fonteReduzida" style="padding: 5px;width: 100%; border: 0.1mm solid #ccc;max-width: 100% !important">
                    <?php
                    foreach ($tabelas as $produto) {
                        foreach ($produto as $produto) {
                            echo ($produto['operadora']['informacoes'] != null) ? nl2br($produto['operadora']['informacoes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>";
                            break;
                        }
                        break;
                    } ?>
                </div>

            </div>


        <?php
        } ?>
        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                FORMAS DE PAGAMENTO
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?php
                foreach ($tabelas as $produto) {
                    foreach ($produto as $produto) {
                        echo ($produto['operadora']['formas_pagamentos'] != null) ? $produto['operadora']['formas_pagamentos'][0]['descricao'] : "<div class='info'>Consulte Operadora</div>";
                        break;
                    }
                    break;
                } ?>
            </div>
        </div>

    <?php
    }
    //die();
    ?>

    <div class="rodape-home col-xs-12" style="text-align: center;">
        <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
            <span style="text-decoration: none !important; font-size: 16px;"> corretorparceiro.com.br</span>
        </a>
        <br>
        <span style="color: #004057; font-size: 16px;">Assessoria de Apoio a Corretores</span>
    </div>
</div>
<?= $this->element('modal-login'); ?>
<?= $this->element('cotacao-email'); ?>

<script>
    var action = '<?= $this->request->params["action"] ?>';
    if (action === 'filtro') {
        var serialize = 'operadorasFiltro';
    } else {
        var serialize = 'operadoras';
    }
    $('#newacoes').css('flex-direction', 'column-reverse');
    window.onbeforeprint = function() {
        $('#newacoes').hide();
    }

    window.onafterprint = function(event) {
        $('#newacoes').show();
    }

    $("#whatsapp").remove();
    $("#pdfbrowser").remove();
    $("#compartilhar").remove();
    $("#exibir-nome").remove();
    $("#pdfdownload").click(function(event) {
        event.preventDefault();
        window.print();
    });
    $("#pdfbrowser").click(function(event) {
        event.preventDefault();
        $("#tipopdf").val("I");
        $("#dadosPDF").attr("target", "_blank");
        $("#dadosPDF").submit();
    });
    $("#whatsapp").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        let link = "<?= $linkWhats ?>";
        if (sessao != "") {
            window.open(link);
        } else {
            $("#modal-login").modal("show");
        }

    });
    $("#btn-email").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        if (sessao != "") {
            $("#modal-compartilhamento").modal("show");
        } else {
            $("#modal-login").modal("show");
        }
    });
</script>