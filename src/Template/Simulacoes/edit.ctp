
<?php $this->assign('title', $title); ?>
<?= $this->Form->create($simulacao, ['id' => 'simulacao']) ?>
<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Editar Cálculos']); ?>
<h4 style="padding: 10px 0" class="centralizada">Editar Cálculo Saúde PME</h4>
<?php // debug($simulacao);?>
<div id="pagina">
    <div class="col-md-12">
        <div class="tituloField">
            Paramêtros do Cálculo  <small class='text-info'><b>*</b>Informações necessárias para evitar preços divergentes</small>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('estado_id', ['label' => false, 'options' => $estados, 'class' => 'fonteReduzida', 'empty' => 'SELECIONE O ESTADO']); ?>            
        </div>
        <div class="col-md-3 form-inline centralizada">
            <?= $this->Form->input('tipo_cnpj_id', ['label' => false, 'options' => $cnpjs, 'empty' => 'SELECIONE TIPO DE CNPJ']); ?>            
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('familia', ['type' => 'checkbox', 'label' => 'Este Cálculo é referente a um Grupo Familiar (Cônjuge e Filhos)?']); ?>            
        </div>

    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12 ">
        <div class="tituloField">
            Dados do Cliente
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('nome', ['label' => 'Nome da Empresa / Pessoa (opcional)']); ?>            
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('contato', ['label' => 'Nome do Contato (A/C) (opcional)']); ?>            
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('email', ['label' => 'E-mail (opcional)', "type" => "text"]); ?>            
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('telefone', ['label' => 'Telefone (opcional)']); ?>            
        </div>

    </div>

    <!-- Quantidade de Vidas --> 
    <div class="col-md-12 qtdVidas">
        <div class="clearfix">&nbsp;</div>
        <div class="tituloField">
            Vidas Por Faixa Etária    
        </div>
        <div class="col-md-12">
            <div class="faixaEtaria col-md-1 col-md-offset-1">

                <?= $this->Form->input('faixa1', ['label' => '0 a 18 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa2', ['label' => '19 a 23 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa3', ['label' => '24 a 28 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa4', ['label' => '29 a 33 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa5', ['label' => '34 a 38 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa6', ['label' => '39 a 43 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>

            <div class="faixaEtaria col-md-1">
                <?= $this->Form->input('faixa7', ['label' => '44 a 48 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa8', ['label' => '49 a 53 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">
                <?= $this->Form->input('faixa9', ['label' => '54 a 58 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa10', ['label' => '59 ou + anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
           
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">

        <div class="col-md-12 centralizada">       
			<?=
            $this->Form->button('Calcular', ['class' => 'btn btn-md btn-primary',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Gerar Cálculo', 'id' => 'editar'])
            ?>
        </div>               
    </div>
    <div class="clearfix">&nbsp;</div>
    <!-- Anexos a serem Inseridos --> 

    <div class="col-md-12" id="dadosSimulacao">

    </div>

</div>


<?php echo $this->element('forms/buttons') ?>
<?= $this->Form->end() ?>


<script type="text/javascript">
    $('input:radio[name=ramo_id]:nth(0)').attr('checked', true);
    $('input:radio[name=modalidade_id]:nth(0)').attr('checked', true);

    // $('#myModal').on('shown.bs.modal', function () {
    //     $('#myInput').focus()
    // });

    $("#estado-id").val("<?= $simulacao['estado_id'] ?>");

    // $("#editar").click(function () {
    //     $('#loader').trigger('click');

    // });

</script>
