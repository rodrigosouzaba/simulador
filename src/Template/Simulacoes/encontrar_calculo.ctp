<?php
if (isset($simulacoes) && !empty($simulacoes)) {
    ?><table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= 'Nº Cálculo' ?></th>
                <th><?= 'Data' ?></th>
                <th><?= 'Nome' ?></th>
                <th><?= 'Contato' ?></th>
                <th><?= 'E-mail' ?></th>
                <th class="centralizada"><?= 'Total de Vidas' ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($simulacoes as $simulaco): ?>
                <tr>
                    <td><?= $this->Number->format($simulaco->id) ?></td>
                    <td><?= h($simulaco->data) ?></td>
                    <td><?= $simulaco->nome ?></td>
                    <td><?= $simulaco->contato ?></td>
                    <td><?= $simulaco->email ?></td>
                    <td class="centralizada"><?php
                        $totalVidas = 0;
                        $i = 1;
                        for ($i = 1; $i <= 10; $i++) {
//                    debug($simulaco['faixa'.$i]);
                            if (isset($simulaco['faixa' . $i])) {

                                $totalVidas = $totalVidas + $simulaco['faixa' . $i];
                            }
                        }
                        echo $totalVidas;
                        ?>
                    </td>
                    <td class="actions">
                        <?= $this->Html->link('', ['action' => 'view', $simulaco->id], ['title' => __('Visualizar Simulação'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?php // echo $this->Form->postLink('', ['action' => 'delete', $simulaco->id], ['confirm' => __('Tem certeza que deseja excluir o Simulação {0}?', $simulaco->id), 'title' => __('Deletar'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>

                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php } else {
    ?>
    <p class="bg-danger centralizada">Nada encontrado...</p>
<?php }
?>

<script type="text/javascript">
//    $("#busca").hide();
    $("#tipo-busca").change(function () {
        $("#busca").show();

//       

    });
    $("#encontrar").click(function () {
//        alert();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>simulacoes/encontrarCalculo/",
            data: $("#encontrarCalculo").serialize(),
            beforeSend: function () {
                $('#loading').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                $("#listaTabelas").empty();
                $("#listaTabelas").append(data);
                $("#loading").empty();
            }
        });
    });

</script>