<?= $this->Form->create('operadoras', ['id' => 'operadoras', 'url' => ['action' => 'pdf']]); ?>
<div id="resultado">
    <div class="alert alert-info centralizada" role="alert" style="margin-top:20px;">
        Foram filtrados <strong> <?= $totalTabelas ?></strong> cálculos de <b><?= $totalsemfiltro ?></b> opções.

        <?= $this->Html->link($this->Html->tag('span', 'Editar Cálculo Original', ['class' => '', 'aria-hidden' => 'true']), ['action' => 'view', $simulacao['id']], ['class' => 'btn btn-sm btn-primary', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Cálculo Original', 'style' => 'margin-left: 5px;']); ?>
        <?= $this->Html->link($this->Html->tag('span', 'Reenviar PDF', ['class' => '', 'aria-hidden' => 'true']), ['action' => 'reenviarpdf', $filtro['id']], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Reenviar PDF', 'style' => 'margin-left: 5px;']); ?>


        <!--<a href="#collapseFiltro" class="btn btn-primary" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="collapseFiltro" style="margin-left: 20px;">Deseja filtrar os resultados? </a>-->
        <div class="collapse" id="collapseFiltro" style="text-align: left !important;">
            <div class="clearfix">&nbsp;</div>
            <div class="well">

                <?= $this->Form->hidden('simulacao_id', ['value' => $simulacao['id']]) ?>
                <div class="col-xs-3">

                    <?= $this->Form->input('abrangencia_id', ['label' => 'Flitre por Área de Atendimento', 'empty' => 'SELECIONE']); ?>
                </div>
                <div class="col-xs-3">
                    <?= $this->Form->input('tipo_id', ['label' => 'Flitre por Tipo de Acomodação', 'empty' => 'SELECIONE']); ?>
                </div>


                <div class="col-xs-3">
                    <?= $this->Form->input('tipo_produto_id', ['options' => $tipos_produtos, 'label' => 'Flitre por Tipo de Produto', 'empty' => 'SELECIONE']); ?>
                </div>
                <div class="col-xs-3">
                    <div class="col-xs-12" style=' padding-bottom: 12px;
                         padding-left: 2px;'>
                        Co-Participação?<?php $simnao = array('s' => 'Sim', 'n' => 'Não') ?>
                    </div>

                    <?= $this->Form->radio('coparticipacao', $simnao, array('label' => ['Co-participação?', 'style' => 'float: left; margin-right: 15px'])); ?>

                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="centralizada">
                    <a href="#" class="btn btn-xs btn-primary" role="button" id="filtrar"><i class="fa fa-search" aria-hidden="true"></i> Filtrar</a>
                    <?= $this->Html->link($this->Html->tag('span', 'Limpar', ['class' => '', 'aria-hidden' => 'true']), ['action' => 'view', $simulacao['id']], ['class' => 'btn btn-xs btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Exibir todas as Tabelas do Cálculo novamente na Tela', 'style' => 'margin-left: 5px;']); ?>


                </div>

            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>


    <?php if ($totalTabelas > 0) { ?>
        <!--<div id="operadoras">-->
        <?php
        //    debug($simulacao);
        //    die();
        $idOperadora = null;
        foreach ($tabelasOrdenadas as $chave => $tabelas) {
            $numeroTabelas = 0;
            foreach ($tabelas as $p => $t) {
                //                                    debug($p);
                //                                    debug($t);
                //                                    die();
                $numeroTabelas = $numeroTabelas + count($t);
            }
        ?>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                <div class="panel panel-default col-xs-12" style="margin: 5px 0;">
                    <div class="panel-heading" role="tab" id="heading<?= $chave ?>" style="min-height: 120px; background-color: #fff !important;">
                        <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $chave ?>" aria-expanded="true" aria-controls="collapse<?= $chave ?>">
                            <div class="col-md-3 vermais">

                                <i class="fa fa-chevron-circle-up" aria-hidden="true" style="font-size: 110% !important;"></i> Clique para visualizar <?= $numeroTabelas ?> tabelas(s)
                        </a>


                    </div>
                    </a>
                    <a class="controle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $chave ?>" aria-expanded="true" aria-controls="collapse<?= $chave ?>">

                        <h4 class="panel-title centralizada col-md-6">
                            <?php
                            foreach ($operadoras as $operadora) {
                                if ($operadora['id'] == $chave) {
                                    echo $this->Html->image("../" . $operadora->imagen->caminho . "/" . $operadora->imagen->nome, ['class' => 'logoSimulacao']);
                                    echo "<p class='detalheOperadora'>" . $operadora['detalhe'] . "</p>";
                                }
                            }
                            ?>
                        </h4>
                    </a>
                    <div class="col-md-3">&nbsp;</div>
                </div>
                <div id="collapse<?= $chave ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $chave ?>">
                    <div class="panel-body">


                        <!--                        <ul class="list-group semMargem">-->

                        <table class="table table-condensed">
                            <tr>
                                <!--                                <td class="beneficiarios" style="width: 4%;">
                                    <small>Exibir Todas<br><input type="checkbox" id="checkAll<?= $chave ?>" class="todas" value="<?= $chave ?>" ></small>
                                </td>-->
                                <!--<td class="beneficiarios" colspan="1">&nbsp;</td>-->
                                <?php
                                $i = 1;

                                for ($i = 1; $i <= 12; $i++) {

                                    if ($simulacao['faixa' . $i] > 0) {
                                ?>
                                        <td class='beneficiarios negrito centralizadaVertical' style="border: solid 1px #ddd;">
                                            <?php
                                            switch ($i) {
                                                case 1:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                                    break;
                                                case 2:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                                    break;
                                                case 3:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                                    break;
                                                case 4:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                                    break;
                                                case 5:
                                                    echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                                    break;
                                                case 6:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                                    break;
                                                case 7:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                                    break;
                                                case 8:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                                    break;
                                                case 9:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                                    break;
                                                case 10:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 à 64";
                                                    break;
                                                case 11:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 65 à 80";
                                                    break;
                                                case 12:
                                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> + de 81";
                                                    break;
                                            }
                                            ?>

                                        </td>

                                <?php
                                    }
                                }
                                ?>
                                <td class="beneficiarios negrito">
                                    <?php
                                    $x = 1;
                                    $vidas = 0;
                                    for ($x = 1; $x <= 12; $x++) {
                                        $vidas = $vidas + $simulacao['faixa' . $x];
                                    }
                                    echo "Total:<br>" . $vidas . " Vida(s)";
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $faixas = null;
                            $i = 1;
                            for ($i = 1; $i <= 12; $i++) {
                                if ($simulacao['faixa' . $i] > 0) {
                                    $faixas = $faixas + 1;
                                }
                            }

                            foreach ($tabelas as $produto) {
                                //                                    debug($produto);die();
                                foreach ($produto as $produto) {
                            ?>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr style="background-color: #eee; border: solid 1px #ddd">
                                        <!--<td class="centralizada"><?= $this->Form->input($produto['id'], ['type' => 'checkbox', 'id' => 'imprimir' . $produto['id'], 'value' => $produto['id'], 'class' => 'noMarginBottom ' . $chave, 'title' => 'Selecionar Para Impresão', 'label' => '']); ?></td>-->
                                        <td class="beneficiariosTransparente centralizadaVertical centralizada" colspan="<?= $faixas + 1 ?>">
                                            <b><?= $produto['descricao'] . "</b> | " . $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas - " . $produto['tipo']['nome'] ?>
                                        </td>
                                    </tr>
                                    <tr style="border: solid 1px #ddd">
                                        <!--<td>&nbsp;</td>-->
                                        <?php
                                        $i = 1;


                                        for ($i = 1; $i <= 12; $i++) {

                                            if ($simulacao['faixa' . $i] > 0) {

                                                //                                                    foreach ($tabelas as $produto) {
                                                //                                                        debug($simulacao->faixa5);die();
                                        ?>
                                                <td class='beneficiariosTransparente centralizadaVertical'>
                                                    <?= $this->Number->currency($simulacao['faixa' . $i] * $produto['faixa' . $i]) ?>
                                                    <br /><small> <?= "R$ " . str_replace('.', ",", $produto['faixa' . $i]) . " por vida" ?></small>
                                                </td>
                                        <?php
                                                // }
                                            }
                                        }
                                        ?>
                                        <td class="beneficiariosTransparente negrito centralizada centralizadaVertical">
                                            <?php
                                            $i = 1;
                                            $totalPorProduto = 0;
                                            for ($i = 1; $i <= 12; $i++) {
                                                $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * $produto['faixa' . $i]);
                                            }
                                            ?>
                                            <?= $this->Number->currency($totalPorProduto) ?></td>

                                    </tr>
                            <?php
                                }
                            }
                            ?>
                            </tr>


                        </table>

                        <!--</ul>-->

                        <?php if ($filtro['observacao'] == 1) { ?>

                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                OBSERVAÇÕES IMPORTANTES
                            </div>



                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php
                                $redes = null;
                                $teste = null;
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        //                                    debug($produto);die();
                                        $redes[$produto['produto']['descricao']] = $produto['produto']['observaco']['descricao'];
                                    }
                                }
                                foreach ($redes as $chave => $valor) {
                                    if ($valor != '') {
                                        echo nl2br($valor);
                                        $teste = 1;
                                        break;
                                    }
                                }
                                echo ($teste != 1) ? "<div class='info'>Consulte Operadora</div>" : '';
                                ?>
                            </div>




                            <div class="clearfix">&nbsp;</div>
                        <?php } ?>

                        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                            ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
                        </div>



                        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                            <?php
                            $redes = null;
                            foreach ($tabelas as $produto) {
                                foreach ($produto as $produto) {
                                    //                                    debug($produto);die();
                                    $redes[$produto['produto']['descricao']] = $produto['regio']['descricao'] . " / " . $produto['abrangencia']['descricao'];
                                }
                            }
                            ?>
                            <table class="table table-condensed ">
                                <tr>
                                    <td style="border: 0 !important"></td>
                                    <td style="border: 0 !important;text-align: center;">Comercialização </td>
                                    <td style='border: 0 !important;text-align: center;'>Atendimento</td>
                                </tr>
                                <?php
                                foreach ($redes as $chave => $valor) {
                                    echo "<tr><td style='border: 0 !important;vertical-align: middle;'><b class='fonteReduzida'>" . $chave . "</b> </td>";
                                    $arr = explode("/", $valor, 2);
                                    //                                    $first = $arr[0];
                                    echo ($arr[0] != ' ') ? "<td style='border: 0 !important;vertical-align: middle;text-align: center;text-transform: uppercase'><span class='fonteReduzida'>" . nl2br($arr[0]) . "</span></td>" : "<td style='border: 0 !important;vertical-align: middle;text-align: center;'><span class='fonteReduzida'>CONSULTE OPERADORA</span></td>";
                                    echo ($arr[1] != ' ') ? "<td style='border: 0 !important;vertical-align: middle;text-align: center;text-transform: uppercase'><span class='fonteReduzida'>" . nl2br($arr[1]) . "</span></td></tr>" : "<td style='border: 0 !important;vertical-align: middle;text-align: center;'><span class='fonteReduzida'>CONSULTE OPERADORA</span></td></tr>";
                                }
                                ?>
                            </table>

                        </div>
                        <!--                        <div class="clearfix">&nbsp;</div>

                                                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                                    TIPOS DE PRODUTOS
                                                </div>
                                                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                        <?php
                        $redes = null;
                        foreach ($tabelas as $produto) {
                            foreach ($produto as $produto) {
                                //                                    debug($produto);die();
                                $redes[$produto['produto']['descricao']] = $produto['tipos_produto']['nome'];
                            }
                        }

                        foreach ($redes as $chave => $valor) {
                            echo "<b>" . $chave . ":</b> ";
                            echo ($valor != '') ? nl2br($valor) : "<span class='info'>Consulte Operadora</span>";
                            echo "<div class='clearfix'>&nbsp;</div>";
                        }
                        ?>

                                                </div>-->


                        <div class="clearfix">&nbsp;</div>

                        <?php if ($filtro['rede'] == 1) { ?>

                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                REDE REFERENCIADA (Resumo)
                            </div>



                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php
                                $redes = null;
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        //                                    debug($produto);die();
                                        $redes[$produto['produto']['descricao']] = $produto['produto']['rede']['descricao'];
                                    }
                                }
                                foreach ($redes as $chave => $valor) {
                                    echo "<b>" . $chave . ":</b> ";
                                    echo ($valor != '') ? nl2br($valor) : "<span class='info'>Consulte Operadora</span>";
                                    echo "<div class='clearfix'>&nbsp;</div>";
                                }
                                ?>
                            </div>

                            <div class="clearfix">&nbsp;</div>

                        <?php
                        }

                        if ($filtro['reembolso'] == 1) {
                        ?>
                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                COBERTURAS, REEMBOLSOS E DIFERENCIAIS (Resumo)
                            </div>



                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php
                                $redes = null;
                                $teste = null;
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        //                                    debug($produto);die();
                                        $redes[$produto['produto']['descricao']] = $produto['produto']['reembolso']['descricao'];
                                    }
                                }
                                foreach ($redes as $chave => $valor) {
                                    if ($valor != '') {
                                        echo nl2br($valor);
                                        $teste = 1;
                                        break;
                                    }
                                }
                                echo ($teste != 1) ? "<div class='info'>Consulte Operadora</div>" : '';
                                ?>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        <?php
                        }

                        if ($filtro['carencia'] == 1) {
                        ?>
                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                CARÊNCIAS (Resumo)
                            </div>



                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php
                                $redes = null;
                                $teste = null;
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        //                                    debug($produto);die();
                                        $redes[$produto['produto']['descricao']] = $produto['produto']['carencia']['descricao'];
                                    }
                                }
                                foreach ($redes as $chave => $valor) {
                                    if ($valor != '') {
                                        echo nl2br($valor);
                                        $teste = 1;
                                        break;
                                    }
                                }
                                echo ($teste != 1) ? "<div class='info'>Consulte Operadora</div>" : '';
                                ?>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                        <?php
                        }

                        if ($filtro['informacao'] == 1) {
                        ?>
                            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                                DOCUMENTOS NECESSÁRIOS
                            </div>



                            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                                <?php
                                //                                                        debug($produto['produto']);
                                //                                                        die();
                                $redes = null;
                                $teste = null;
                                foreach ($tabelas as $produto) {
                                    foreach ($produto as $produto) {
                                        //                                    debug($produto);die();
                                        $redes[$produto['produto']['descricao']] = $produto['produto']['informaco']['descricao'];
                                    }
                                }
                                foreach ($redes as $chave => $valor) {
                                    if ($valor != '') {
                                        echo nl2br($valor);
                                        $teste = 1;
                                        break;
                                    }
                                }
                                echo ($teste != 1) ? "<div class='info'>Consulte Operadora</div>" : '';
                                ?>
                            </div>
                        <?php } ?>


                    </div>
                </div>
            </div>
</div>




<?php
        }
    }

    //debug($filtro);
?>
<div class="col-md-12 centralizada" style="background-color: #ddd; margin-top: 20px;clear: both;">
    <!--    <div class="clearfix">&nbsp;</div>

    <div class="col-md-12 centralizada" style="background-color: #ddd; font-weight: bold">
        EXIBIDO NO PDF
    </div>
    <div class="col-md-1">&nbsp;</div>
    <div class="col-md-12 centralizada">
        <strong>
            <?php
            echo ($filtro['observacao'] == 1) ? "Observações Importantes<br/>" : "";
            echo ($filtro['rede'] == 1) ? "Rede Credenciada (Resumo)<br/>" : "";
            echo ($filtro['reembolso'] == 1) ? "Observações Importantes<br/>" : "";
            echo ($filtro['carencia'] == 1) ? "Carência (Resumo)<br/>" : "";
            echo ($filtro['informacao'] == 1) ? "Documentos Necessários" : "";
            ?>
        </strong>

        <div class="clearfix">&nbsp;</div>

            <div class="col-md-12 centralizada">

        <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-file-pdf-o', 'aria-hidden' => 'true']) . ' Gerar PDF'), ['class' => 'btn btn-md btn-primary', 'id' => 'pdf']) ?>



        <?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', $btnCancelar, ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Retornar', 'id' => 'retornar']); ?>

            </div>
        <div class="clearfix">&nbsp;</div>
        <div id="salvarSimulacao"></div>
    </div>-->


    <?php
    echo $this->Form->end();
    ?>

</div>
<?= $this->element('processando'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('.todas').click(function() {
            $('.' + (this).value).prop('checked', this.checked);
        });
    });

    $("#pdf").click(function() {

        $.ajax({
            type: "post",
            //            data: $("#operadoras").serialize(),
            //            url: "<?php echo $this->request->webroot ?>simulacoes/pdf/",
            beforeSend: function() {
                $('#loader').trigger('click');
            },
            success: function(data) {
                $('#fechar').trigger('click');
            }
        });
    });
    $("#filtrar").click(function() {
        $.ajax({
            type: "post",
            data: $("#operadoras").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/filtro/",
            beforeSend: function() {
                $('#loader').trigger('click');
            },
            success: function(data) {
                $('#fechar').trigger('click');
                $('#resultado').empty();
                $('#resultado').append(data);
                $("body").removeClass("modal-open");
                $('.modal-backdrop').remove()
            }
        });
    });
</script>
