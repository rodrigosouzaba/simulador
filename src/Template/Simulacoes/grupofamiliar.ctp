
<div class="clearfix">&nbsp;</div>
<!--    <div class="tituloField centralizada">
        Vidas por Titular ou Grupo Familiar  
    </div>-->
<div class="col-xs-12  well well-sm" id="familia-1" style="margin-bottom: 5px !important; ">
    <?= $this->Form->input('grupofamiliar', ['label' => '', 'value' => '1', 'type' => 'hidden']); ?>
    <div class="col-xs-12" style="padding-left:0!important;padding-right: 0!important; ">
        <div class="faixaEtaria col-md-6 col-md-offset-3 col-xs-12" style="padding-left:0!important;padding-right: 0!important; ">
            <?= $this->Form->input('Grupos.1.nome_titular', ['label' => '', 'placeholder' => 'GRUPO 1', 'value' => 'GRUPO 1', 'required' => false, 'class' => 'centralizada']); ?>
        </div>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa1', ['label' => '0 a 18 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa2', ['label' => '19 a 23 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa3', ['label' => '24 a 28 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa4', ['label' => '29 a 33 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa5', ['label' => '34 a 38 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa6', ['label' => '39 a 43 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa7', ['label' => '44 a 48 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa8', ['label' => '49 a 53 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa9', ['label' => '54 a 58 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa10', ['label' => '59 a 64 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa11', ['label' => '65 a 80 anos', 'required' => false, 'class' => 'centralizada']); ?> 
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.1.faixa12', ['label' => '+ de 81 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
</div>
<!--<div class="col-xs-12 qtdVidas">-->
<!--    <div class="tituloField centralizada">
        Vidas por Titular ou Grupo Familiar  
    </div>-->
<div class="col-xs-12  well well-sm" id="familia-2" style="margin-bottom: 5px !important; ">
    <?= $this->Form->input('grupofamiliar', ['label' => '', 'value' => '2', 'type' => 'hidden']); ?>
    <div class="col-xs-12" style="padding-left:0!important;padding-right: 0!important; ">
        <div class="faixaEtaria col-md-6 col-md-offset-3  col-xs-12" style="padding-left:0!important;padding-right: 0!important; ">
            <?= $this->Form->input('Grupos.2.nome_titular', ['label' => '', 'placeholder' => 'GRUPO 2', 'value' => 'GRUPO 2', 'required' => false, 'class' => 'centralizada']); ?>
        </div>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa1', ['label' => '0 a 18 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa2', ['label' => '19 a 23 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa3', ['label' => '24 a 28 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa4', ['label' => '29 a 33 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa5', ['label' => '34 a 38 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa6', ['label' => '39 a 43 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa7', ['label' => '44 a 48 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa8', ['label' => '49 a 53 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa9', ['label' => '54 a 58 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa10', ['label' => '59 a 64 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa11', ['label' => '65 a 80 anos', 'required' => false, 'class' => 'centralizada']); ?> 
    </div>
    <div class="faixaEtaria col-md-1 col-xs-12">
        <?= $this->Form->input('Grupos.2.faixa12', ['label' => '+ de 81 anos', 'required' => false, 'class' => 'centralizada']); ?>
    </div>
</div>

<div id="multiplasfamilias">

</div>
<div class="centralizada">
    <?=
    $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'glyphicon glyphicon-plus-sign', 'aria-hidden' => 'true']) . " Adicionar Grupo Familiar", '#novafamilia', ['class' => 'btn  btn-default addFamilia', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Adicionar família']);
    ?>
</div>
<div class="clearfix">&nbsp;</div>



<script type="text/javascript">
    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    var a = 1;
    var cloneCount = 2;
    $(".addFamilia").click(function () {
        if (
                $('#grupos-' + cloneCount + '-faixa1').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa2').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa3').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa4').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa5').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa6').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa7').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa8').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa9').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa10').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa11').val() != '' ||
                $('#grupos-' + cloneCount + '-faixa12').val() != ''

                ) {
//            if (cloneCount != 1) {
//                $('<div class="spacer spacer-md">&nbsp;</div>').appendTo('#multiplasfamilias');
//            }
            cloneCount++;
            var indice = cloneCount - 1;
            console.log(cloneCount);
            console.log(indice);
            $('#grupos-' + indice + '-nome-titular').clone().attr('id', 'grupos-' + cloneCount + '-nome-titular').attr('name', 'Grupos[' + cloneCount + '][nome_titular]').appendTo('#multiplasfamilias').wrap('<div class="col-xs-12 well well-sm" id="familia' + cloneCount + '" style="margin-bottom: 5px !important; ">');
            $('#grupos-' + cloneCount + '-nome-titular').val('GRUPO ' + cloneCount).wrap('<div class="faixaEtaria col-md-6 col-md-offset-3 col-xs-12" >');
            $('#grupos-' + indice + '-faixa1').clone().attr('id', 'grupos-' + cloneCount + '-faixa1').attr('name', 'Grupos[' + cloneCount + '][faixa1]').attr('placeholder', '0 a 18').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa2').clone().attr('id', 'grupos-' + cloneCount + '-faixa2').attr('name', 'Grupos[' + cloneCount + '][faixa2]').attr('placeholder', '19 a 23').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa3').clone().attr('id', 'grupos-' + cloneCount + '-faixa3').attr('name', 'Grupos[' + cloneCount + '][faixa3]').attr('placeholder', '24 a 28').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa4').clone().attr('id', 'grupos-' + cloneCount + '-faixa4').attr('name', 'Grupos[' + cloneCount + '][faixa4]').attr('placeholder', '29 a 33').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa5').clone().attr('id', 'grupos-' + cloneCount + '-faixa5').attr('name', 'Grupos[' + cloneCount + '][faixa5]').attr('placeholder', '34 a 38').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa6').clone().attr('id', 'grupos-' + cloneCount + '-faixa6').attr('name', 'Grupos[' + cloneCount + '][faixa6]').attr('placeholder', '39 a 43').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa7').clone().attr('id', 'grupos-' + cloneCount + '-faixa7').attr('name', 'Grupos[' + cloneCount + '][faixa7]').attr('placeholder', '44 a 48').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa8').clone().attr('id', 'grupos-' + cloneCount + '-faixa8').attr('name', 'Grupos[' + cloneCount + '][faixa8]').attr('placeholder', '49 a 53').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa9').clone().attr('id', 'grupos-' + cloneCount + '-faixa9').attr('name', 'Grupos[' + cloneCount + '][faixa9]').attr('placeholder', '54 a 58').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa10').clone().attr('id', 'grupos-' + cloneCount + '-faixa10').attr('name', 'Grupos[' + cloneCount + '][faixa10]').attr('placeholder', '59 a 64').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa11').clone().attr('id', 'grupos-' + cloneCount + '-faixa11').attr('name', 'Grupos[' + cloneCount + '][faixa11]').attr('placeholder', '65 a 80').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');
            $('#grupos-' + indice + '-faixa12').clone().attr('id', 'grupos-' + cloneCount + '-faixa12').attr('name', 'Grupos[' + cloneCount + '][faixa12]').attr('placeholder', '+ de 81').val("").appendTo('#familia' + cloneCount).wrap('<div class="col-md-1 col-xs-12 faixaEtaria">');

            $("#novafamilia").focus();
        } else {
            alert('Insira ao menos um integrante do grupo familiar anterior');
        }
    });

</script>
