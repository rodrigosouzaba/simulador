
<!-- Quantidade de Vidas --> 
<div class="col-xs-12 qtdVidas ">
    <div class="clearfix">&nbsp;</div>
<!--    <div class="tituloField centralizada">
        Total de vidas da empresa   
    </div>-->
    <div class="col-xs-12  well well-sm">
        <div class="faixaEtaria col-md-1 col-xs-12 col-md-offset-1">

            <?= $this->Form->input('faixa1', ['label' => '0 a 18 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
        <div class="faixaEtaria col-md-1 col-xs-12">

            <?= $this->Form->input('faixa2', ['label' => '19 a 23 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
        <div class="faixaEtaria col-md-1 col-xs-12">

            <?= $this->Form->input('faixa3', ['label' => '24 a 28 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
        <div class="faixaEtaria col-md-1 col-xs-12">

            <?= $this->Form->input('faixa4', ['label' => '29 a 33 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
        <div class="faixaEtaria col-md-1 col-xs-12">

            <?= $this->Form->input('faixa5', ['label' => '34 a 38 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
        <div class="faixaEtaria col-md-1 col-xs-12">

            <?= $this->Form->input('faixa6', ['label' => '39 a 43 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>

        <div class="faixaEtaria col-md-1 col-xs-12">
            <?= $this->Form->input('faixa7', ['label' => '44 a 48 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
        <div class="faixaEtaria col-md-1 col-xs-12">

            <?= $this->Form->input('faixa8', ['label' => '49 a 53 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
        <div class="faixaEtaria col-md-1 col-xs-12">
            <?= $this->Form->input('faixa9', ['label' => '54 a 58 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
        <div class="faixaEtaria col-md-1 col-xs-12">

            <?= $this->Form->input('faixa10', ['label' => '59 ou + anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
<!--
        <div class="faixaEtaria col-md-1 col-xs-12">

            <?= $this->Form->input('faixa11', ['label' => '65 a 80 anos', 'required' => false, 'class' => 'centralizada']); ?> 

        </div>
        <div class="faixaEtaria col-md-1 col-xs-12">

            <?= $this->Form->input('faixa12', ['label' => '+ de 81 anos', 'required' => false, 'class' => 'centralizada']); ?>

        </div>
-->
    </div>
</div>


<script type="text/javascript">
    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
   

/*
    var a = 1;
    var cloneCount = 1;
    $(".addFamilia").click(function () {
        if (cloneCount != 1) {
            $('<div class="spacer spacer-md">&nbsp;</div>').appendTo('#multiplasfamilias');
        }
        cloneCount++;
        $('#nome-titular-1').clone().attr('id', 'nome-titular-' + cloneCount).attr('name', 'nome_titular[' + cloneCount + ']').attr('placeholder', 'Nome titular família ' + cloneCount).attr('value', 'Nome titular família ' + cloneCount).appendTo('#multiplasfamilias').wrap('<div class="col-md-12">');

        $('#faixa1-1').clone().attr('id', 'faixa1-' + cloneCount).attr('name', 'faixa1[' + cloneCount + ']').attr('placeholder', '0 a 18').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa2-1').clone().attr('id', 'faixa2-' + cloneCount).attr('name', 'faixa2[' + cloneCount + ']').attr('placeholder', '19 a 23').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa3-1').clone().attr('id', 'faixa3-' + cloneCount).attr('name', 'faixa3[' + cloneCount + ']').attr('placeholder', '24 a 28').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa4-1').clone().attr('id', 'faixa4-' + cloneCount).attr('name', 'faixa4[' + cloneCount + ']').attr('placeholder', '29 a 33').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa5-1').clone().attr('id', 'faixa5-' + cloneCount).attr('name', 'faixa5[' + cloneCount + ']').attr('placeholder', '34 a 38').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa6-1').clone().attr('id', 'faixa6-' + cloneCount).attr('name', 'faixa6[' + cloneCount + ']').attr('placeholder', '39 a 43').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa7-1').clone().attr('id', 'faixa7-' + cloneCount).attr('name', 'faixa7[' + cloneCount + ']').attr('placeholder', '44 a 48').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa8-1').clone().attr('id', 'faixa8-' + cloneCount).attr('name', 'faixa8[' + cloneCount + ']').attr('placeholder', '49 a 53').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa9-1').clone().attr('id', 'faixa9-' + cloneCount).attr('name', 'faixa9[' + cloneCount + ']').attr('placeholder', '54 a 58').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa10-1').clone().attr('id', 'faixa10-' + cloneCount).attr('name', 'faixa10[' + cloneCount + ']').attr('placeholder', '59 a 64').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa11-1').clone().attr('id', 'faixa11-' + cloneCount).attr('name', 'faixa11[' + cloneCount + ']').attr('placeholder', '65 a 80').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('#faixa12-1').clone().attr('id', 'faixa12-' + cloneCount).attr('name', 'faixa12[' + cloneCount + ']').attr('placeholder', '+ de 81').val("").appendTo('#multiplasfamilias').wrap('<div class="col-md-1">');
        $('<div class="col-md-12 " style="border-bottom: 1px solid #c1c1c1">').appendTo('#multiplasfamilias');

        $("#novafamilia").focus();
    });
*/

</script>
