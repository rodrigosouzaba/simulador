<?php $this->assign('title', $title); ?>
<?= $this->Form->create('encontrarCalculo', ['id' => 'encontrarCalculo']) ?>

<!--
<div class="col-md-2">
<?=
$this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-eye', 'aria-hidden' => 'true']) . ' Ver todas', ['action' => 'index'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'verTodas']);
?>
<?=
$this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Cálculo', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addAbrangencia']);
?>
</div>-->
<h4 style="padding: 10px 0" class="centralizada">Cálculos Saúde PME</h4>

<div class="col-md-2">
    <?php $busca = array('c' => 'Nº do Cálculo', 'n' => 'Nome', 'e' => 'E-mail'); ?>
    <?= $this->Form->input('tipo_busca', ['options' => $busca, 'label' => '', 'empty' => 'Pesquisar cálculos']); ?>
</div>
<div class="col-md-5" id="busca">
    <div class="col-md-8" style="padding-right: 0;">

        <?= $this->Form->input('info_busca', ['label' => '', 'style' => 'margin-bottom: 0 !important', 'placeholder' => 'Digite o que deseja buscar aqui..']); ?>
    </div>
    <div class="col-md-4" style="padding-left: 5; padding-top: 5px;">
        <?= $this->Html->link('', '#', ['title' => __('Encontrar'), 'class' => 'btn btn-primary fa fa-search', 'style' => 'margin-top: 5px !important;', 'id' => 'encontrar', 'style' => 'float: left;z-index:99;']) ?>
        <div id="loading" style="float:left; margin-left: 10px; z-index:9;"></div>
    </div>

</div>
<?= $this->Form->end() ?>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-12" id="resposta">
    <table cellpadding="0" cellspacing="0" >
        <thead>
            <tr>
                <th class="col-md-1 centralizada"><?= 'Nº Cálculo' ?></th>
                <th class="col-md-2 centralizada"><?= 'Data' ?></th>
                <th class="col-md-2 "><?= 'Nome' ?></th>
                <th class="centralizada col-md-1"><?= 'Vidas' ?></th>
                <th class="col-md-1 centralizada"><span data-toggle="popover" data-trigger="hover" title="Status dos Cálculos" data-content="Agora você pode acompanhar de perto cada cálculo e alterar comentários sobre cada Negócio"> <?= 'Selecione Status' ?> </span></th>

                <th class="actions col-md-2 "><?= 'Ações' ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($simulacoes as $simulaco): ?>

                <tr>
                    <td class="centralizada"><?= $simulaco->id ?></td>
                    <td class="centralizada"><?= h($simulaco->data) ?></td>
                    <td>
                        <span data-toggle="popover" data-html="true" data-placement="top" data-trigger="click" title="Dados do Cliente" 
                              data-content="<b>Contato: </b><?= $simulaco['contato'] ?> <br> <b>Telefone: </b><?= $simulaco['telefone'] ?> <br> <b>Email: </b><?= $simulaco['email'] ?>" >
                            <i class=""></i> <?=
                            $this->Html->link('', '#historico' . $simulaco['id'], [
                                'class' => 'fa fa-info-circle listaracompanhamentos',
                                'style' => 'margin: 0 5px;']);
                            ?>
                        </span>
                        <?= $simulaco->nome ?> 
                    </td>
                    <td class="centralizada"><?php
                        $totalVidas = 0;
                        $i = 1;
                        for ($i = 1; $i <= 12; $i++) {
//                    debug($simulaco['faixa'.$i]);
                            if (isset($simulaco['faixa' . $i])) {

                                $totalVidas = $totalVidas + $simulaco['faixa' . $i];
                            }
                        }
                        echo $totalVidas;
                        ?>
                    </td>
                    <td class="centralizada"> 
                        <?= $this->Form->input('status_id', ['label' => '', 'value' => $simulaco->status_id, 'empty' => 'SEM STATUS', 'options' => $status, 'simulacao_id' => $simulaco->id, 'class' => 'change_status']) ?>
                    </td>
                    <td class="actions">
                        <span data-toggle='tooltip' data-placement='top' title='Acompanhamento '>

                            <?=
                            $this->Html->link('', '#historico' . $simulaco->id, [
                                'class' => 'btn btn-sm btn-default fa fa-list listarpdf',
                                'role' => 'button',
                                'value' => $simulaco->id,
                                'data-toggle' => 'modal',
//                                data-toggle="modal" data-target="#modalFiltro"
                                'data-target' => '#modalFiltro',
                                'aria-expanded' => "false"
                            ])
                            ?>
                        </span>
                        <?=
                        $this->Html->link('', ['action' => 'edit', $simulaco->id], [
                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Cálculo',
                            'class' => 'btn btn-sm btn-default glyphicon glyphicon-edit'])
                        ?>
                        <?php
                        if ($simulaco->nome_titular != '') {
                           echo  $this->Html->link('', ['action' => 'view_grupo', $simulaco->id], [
                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Visualizar Cálculo',
                            'title' => __('Visualizar Cálculo'),
                            'class' => 'btn btn-sm btn-default glyphicon glyphicon-eye-open']);
                        } else {
                           echo  $this->Html->link('', ['action' => 'view', $simulaco->id], [
                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Visualizar Cálculo',
                            'title' => __('Visualizar Cálculo'),
                            'class' => 'btn btn-sm btn-default glyphicon glyphicon-eye-open']);
                        }
                        ?>
                       
                        <span data-toggle = 'tooltip' data-placement='top' title ='Comentários'>

                            <?=
                            $this->Html->link('', '#historico' . $simulaco->id, [
                                'title' => __('Comentários'),
                                'value' => $simulaco->id,
                                'class' => 'btn btn-sm btn-default fa fa-comment listarcomentarios',
                                'data-toggle' => 'modal',
//                                data-toggle="modal" data-target="#modalFiltro"
                                'data-target' => '#modalComentarios',
                                'aria-expanded' => "false"])
                            ?>
                        </span>

                        <?php // echo $this->Form->postLink('', ['action' => 'delete', $simulaco->id], ['confirm' => __('Tem certeza que deseja excluir o Cálculo Nº {0}?', $simulaco->id), 'title' => __('Deletar'), 'class' => 'btn btn-default glyphicon glyphicon-trash'])      ?>

                    </td>

                </tr>
                <tr class="collapse " id="historico<?= $simulaco->id ?>">
                    <td colspan="7" >

                        <?php
//                debug($filtros->toArray());
                        $possuefiltro = null;
                        foreach ($filtros as $filtro) {
//                            echo $filtro['simulacao_id'];

                            if ($filtro['simulacao_id'] == $simulaco->id) {
                                if ($possuefiltro == null) {
                                    ?>
                                    <div class="col-xs-12 well well-sm" style="margin: 5px; padding: 5px !important;">
                                        <?php
                                    }
                                    ?>
                                    <div class="col-xs-10" style="margin-top: 7px; font-size: 85%;">
                                        PDF gerado e enviado em: <?= h($filtro['created']); ?>
                                    </div>
                                    <div class="col-xs-2">
                                        <?php
                                        echo $this->Html->link('', ['controller' => 'simulacoes', 'action' => 'filtropdf', $filtro['id']], [
                                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Visualizar dados do PDF',
                                            'class' => 'btn btn-sm btn-default fa fa-file-pdf-o',
                                            'style' => 'margin: 3px 0;']);

                                        echo $this->Html->link('', ['controller' => 'simulacoes', 'action' => 'reenviarpdf', $filtro['id']], [
                                            'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Reenviar PDF',
                                            'class' => 'reenvio btn btn-sm btn-default fa fa-envelope',
                                            'style' => 'margin: 3px 0;'])
                                        ?>
                                        <!--
                                        <?= $this->Html->link($this->Html->tag('span', 'Reenviar PDF', ['class' => '', 'aria-hidden' => 'true']), ['action' => 'reenviarpdf', $filtro['id']], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Reenviar PDF', 'style' => 'margin-left: 5px;']); ?>
                                        -->
                                    </div>
                                    <?php
                                    $possuefiltro = true;
                                }
                            }
                            if ($possuefiltro != true) {
                                ?>
                                <div class="well well-sm centralizada" style="font-size: 80%; color: #666">
                                    Nenhum PDF gerado ainda para o cálculo selecionado
                                </div><?php
                            } else {
                                echo "</div>";
                            }
                            ?>
                    </td>
                </tr>

            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>




<!-- Button trigger modal -->
<button type="button" id="loader" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#modalReenvio">
</button>
<!-- Modal -->
<div class="modal fade" id="modalReenvio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span style="margin-right: 20px !important;">Processando </span><img src='../../img/spinner.gif' style='max-height: 60px;margin:-10px;' /> </h4> 
            </div>

        </div>
    </div>
</div>

<!-- Modal FILTRO-->
<div class="modal fade" id="modalFiltro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="conteudoModal">

        </div>
    </div>
</div>

<!-- Modal COMENTÁRIOS-->
<div class="modal fade" id="modalComentarios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="conteudoModalComentarios">

        </div>
    </div>
</div>


<script type="text/javascript">
    $('[data-toggle="popover"]').popover();
    $("#busca").hide();
    $("#tipo-busca").change(function () {
        $("#busca").show();
//       

    });
//    $('.collapse').collapse()
    $("#encontrar").click(function () {
//        alert();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>simulacoes/encontrarCalculo/",
            data: $("#encontrarCalculo").serialize(),
            beforeSend: function () {
                $('#loading').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                $("#resposta").empty();
                $("#resposta").append(data);
                $("#loading").empty();
            }
        });
    });
    $(".listarpdf").click(function () {
//        alert();
//         var id = document.getElementsByTagName("H1")[0].getAttribute("class"); 
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>simulacoes/listarpdfs/" + $(this).attr("value"),
//            beforeSend: function () {
//                $('#loading').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
//            },
            success: function (data) {
                $("#conteudoModal").empty();
                $("#conteudoModal").append(data);
//                $("#loading").empty();
            }
        });
    });
    $(".listarcomentarios").click(function () {
        $.ajax({
            type: "POST",
            url: "<?= $this->request->webroot ?>simulacoes-comentarios/index/" + $(this).attr("value"),
            success: function (data) {
                $("#conteudoModalComentarios").empty();
                $("#conteudoModalComentarios").append(data);
            }
        });
    });
    $(".change_status").change(function () {
//    console.log();
        $.ajax({
            type: "POST",
            url: "<?= $this->request->webroot ?>simulacoes/alterar-status/" + $(this).attr("simulacao_id") + "/" + (this).value,
            success: function (data) {
                location.reload();
            }
        });
    });

    $(".reenvio").click(function () {
        $.ajax({
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#close').trigger('click');
            }
        });



    });
</script>