 <?= $this->Form->input('tabelas._ids', ['options' => $options]);?>
<div class="direita">
    <?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-arrow-circle-right']) . ' Avançar', '#regioes', ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'id' => 'regioesDisponiveis']); ?>
</div> 

<script>
    $("#regioesDisponiveis").click(function () {
        $.ajax({
            type: "Post",
            url: "<?php echo $this->request->webroot ?>simulacoes/regioesDisponiveis/"+$("#tabelas-ids").serialize(),
            data: $("#tabelas-ids").serialize(),
            success: function (data) {
                $("#selectEstados").empty();
                $("#selectEstados").append(data);
            }
        });
    });
</script>