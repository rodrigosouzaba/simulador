<style type="text/css">
    body {
        page-break-before: always;
    }

    .jumbotron {
        background-color: #eee;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 10px;
        line-height: 1.5;
        width: 100%;
        /*width: 97%;*/
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }


    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
        font-size: 60%;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .totalTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
        font-weight: bold;
    }

    .fonteReduzida {
        font-size: 10px;
        text-align: justify !important;
        text-align: justify;
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        line-height: 1.3;
        margin-bottom: 0;
        cursor: default;
        display: block;
        font-style: normal;
        font-variant-caps: normal;
        vertical-align: baseline;
        white-space: normal;

    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .nobreak {
        page-break-inside: avoid;
        width: 100% !important;
    }

    .info {
        color: #a9a9a9;
        font-weight: lighter;
        font-style: italic;
        text-align: center;
        width: 100%;
    }
</style>
<?php foreach ($tabelasOrdenadas as $operadora_id => $operadora) { ?>

    <page backtop="32mm" backbottom="7mm" backleft="5mm" backright="5mm" style="font-size: 11px">
        <page_header style="margin-right: 20px;margin-left: 20px; margin-top: 20px; width: 100%; margin-bottom: 300px !important;">

            <?php echo $this->element('cabecalho_pdf'); ?>

        </page_header>
        <page_footer style="font-size: 15px;">
            <div class="rodape-home col-xs-12" style="text-align: center;">
                <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
                    <span style="text-decoration: none !important"> corretorparceiro.com.br</span>
                </a>
                <br>
                <span style="color: #004057">Assessoria de Apoio a Corretores</span>
            </div>
        </page_footer>
        <br>

        <!--<div class="jumbotron" style="padding: 20px !important;">-->
        <table>
            <tr>
                <td style="font-size: 115%; text-align: justify; line-height: 16px; width: 100%;">
                    À <b><?= $simulacao['nome'] ?></b>, A/C: <b><?= $simulacao['contato'] ?></b>.<br>
                    Agradecemos pela oportunidade. Segue abaixo as simulações dos planos para análise.<br>
                    Informamos que os valores, regras de comercialização e condições contratuais são determinadas <br>
                    pelas seguradoras/operadoras e podem ser alterados pelas mesmas a qualquer momento.
                </td>
            </tr>
        </table>


        <!--</div>-->

        <?php
        $faixas = null;
        $i = 1;
        for ($i = 1; $i <= 12; $i++) {
            if ($simulacao['faixa' . $i] > 0) {
                $faixas = $faixas + 1;
            }
        }
        $largura = 100 / ($faixas + 1);

        $tamanhoFonte = 'font-size:95%'; ?>

        <br /> <br />
        <table style="width: 100%" cellspacing="0">
            <tr>
                <td style="width: 15%; text-align: left; font-weight:bold;">
                </td>
                <td style="text-align: left; font-weight:bold;">
                    <?= $operadora['dados']['nome'] . '<br/><small style="font-weight: 100">' . $operadora['dados']['detalhe'] . "</small>" ?>
                </td>

                <td style="text-align: right;">
                    <?php
                    switch ($operadora['tabelas'][0]['tipo_cnpj']) {
                        case "0":
                            $cnpj = "Para CNPJ Padrão";
                            break;
                        case "1":
                            $cnpj = "CEI";
                            break;
                        case "2":
                            $cnpj = "MEI";
                            break;
                        case "3":
                            $cnpj = "Para CNPJ padrão, CEI ou MEI";
                            break;
                    } ?>
                    <span style="margin-left: 88px;"><?= $operadora['tabelas'][0]['minimo_vidas'] . " à " . $operadora['tabelas'][0]['maximo_vidas'] . " vidas - " . $cnpj; ?></span>
                </td>
            </tr>
        </table>

        <br />
        <?php $corfundo = '#eee'; ?>
        <table style="width: 100%; border:  solid 0.1mm #ddd;<?= $tamanhoFonte ?>" cellspacing="0">
            <tr style="width: 100% !important; ">
                <?php
                for ($i = 1; $i <= 12; $i++) {
                    if ($simulacao['faixa' . $i] > 0) {
                ?>
                        <td style="border: solid 0.1mm #dddddd;text-align:center; width: <?= $largura ?>%;<?= $tamanhoFonte ?>">
                            <?php
                            switch ($i) {
                                case 1:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 0 à 18";
                                    break;
                                case 2:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 19 à 23";
                                    break;
                                case 3:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 24 à 28";
                                    break;
                                case 4:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 29 à 33";
                                    break;
                                case 5:
                                    echo $simulacao['faixa' . $i] . " vida(s) <br/>34 à 38";
                                    break;
                                case 6:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 39 à 43";
                                    break;
                                case 7:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 44 à 48";
                                    break;
                                case 8:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 49 à 53";
                                    break;
                                case 9:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 54 à 58";
                                    break;
                                case 10:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 59 à 64";
                                    break;
                                case 11:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> 65 à 80";
                                    break;
                                case 12:
                                    echo $simulacao['faixa' . $i] . " vida(s)<br/> + de 81";
                                    break;
                            } ?>

                        </td>

                <?php
                    }
                } ?>
                <td class="negrito" style=" border:solid 0.1mm #dddddd;width: <?= $largura ?>%;text-align:center;<?= $tamanhoFonte ?>">
                    <?php
                    $x = 1;
                    $vidas = 0;
                    for ($x = 1; $x <= 12; $x++) {
                        $vidas = $vidas + $simulacao['faixa' . $x];
                    }
                    echo "Total:<br>" . $vidas . " Vida(s)"; ?>
                </td>
            </tr>

            <!-- IMPRIME PREÇOS POR TABELA -->
            <?php foreach ($operadora['tabelas'] as $produto) : ?>
                <tr>
                    <td></td>
                </tr>
                <tr style="background-color: #ccc; text-align: center;">
                    <td style="padding: 5px 0 5px 3px; border-top: solid 0.1mm #5a5a5a; border-left: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a" colspan="<?= $faixas + 1 ?>">
                        <?php
                        if ($produto['coparticipacao'] === 's') {
                            $co = "C/COPARTICIPAÇÃO " . $produto['detalhe_coparticipacao'];
                        } else {
                            $co = "S/COPARTICIPAÇÃO ";
                        } ?>
                        <?php
                        if ($produto['tipo_contratacao'] === '0') {
                            $tipo_contratacao = "OPCIONAL";
                        } elseif ($produto['tipo_contratacao'] === '1') {
                            $tipo_contratacao = "COMPULSÓRIO";
                        } else {
                            $tipo_contratacao = "";
                        } ?>


                        <?php
                        $tamanho = count($produto["tabelas_cnpjs"]);
                        $i = 1;
                        $cnpjFinal = "";
                        foreach ($produto["tabelas_cnpjs"] as  $cnpj) {
                            if ($i < $tamanho) {
                                $cnpjFinal = $cnpjFinal . " " . $cnpj["cnpj"]["nome"] . ",";
                            } else {
                                $cnpjFinal = $cnpjFinal . " " . $cnpj["cnpj"]["nome"];
                            }
                            $i++;
                        }

                        ?>

                        <?= "<b>" . $produto['nome'] . " - " . $produto['descricao'] . " - " . $tipo_contratacao . " " . $co . " - " . $produto["cobertura"]["nome"] . "</b> <br> " . $produto['minimo_vidas'] . " à " . $produto['maximo_vidas'] . " vidas - " . $produto['tipo']['nome'] . " - " . $cnpjFinal ?>
                    </td>
                </tr>
                <tr style="background-color: #fff;border-left: solid 0.1mm #5a5a5a;  ">
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                        if ($simulacao['faixa' . $i] > 0) {
                            if ($i == 1) {
                                $bordaEsquerda = "border-left: solid 0.1mm #5a5a5a;";
                            } else {
                                $bordaEsquerda = "border-left:solid 0.1mm #dddddd;";
                            } ?>
                            <td style="width: <?= $largura ?>%;<?= $bordaEsquerda ?>; text-align:center; border-bottom: solid 0.1mm #5a5a5a; padding: 10px 0; vertical-align: middle">
                                <?php if ($produto['faixa' . $i] == 0) : ?>
                                    ***
                                <?php else : ?>
                                    <?= $this->Number->currency($simulacao['faixa' . $i] * $produto['faixa' . $i]) ?>
                                    <br />

                                    <small style="font-size:75%"> <?= number_format($produto['faixa' . $i], 2, ",", ".") ?> p/ vida</small>
                                <?php endif ?>

                            </td>
                    <?php
                        }
                    } ?>
                    <td style="font-weight: bold;width: <?= $largura ?>%; text-align: center;border-left:solid 0.1mm #dddddd;border-bottom: solid 0.1mm #5a5a5a; border-right: solid 0.1mm #5a5a5a;<?= $tamanhoFonte ?>">
                        <?php
                        $i = 1;
                        $totalPorProduto = 0;
                        for ($i = 1; $i <= 12; $i++) {
                            $totalPorProduto = $totalPorProduto + ($simulacao['faixa' . $i] * $produto['faixa' . $i]);
                        }
                        $operadorascomiof = array(1, 28, 29, 30, 31, 16);
                        if (in_array($produto['operadora']['id'], $operadorascomiof)) {
                        ?>
                            <?= $this->Number->currency($totalPorProduto) ?>
                            <?=
                            "<br>
                                + IOF: " . $this->Number->currency($totalPorProduto * 0.0238, "R$ ") . "<br>"
                                . "<b>" . $this->Number->currency($totalPorProduto * 1.0238, "R$ ") . "</b>"
                            ?>
                        <?php
                        } else {
                        ?>

                            <b><?= $this->Number->currency($totalPorProduto) ?></b>
                        <?php
                        } ?>
                    </td>

                </tr>
            <?php endforeach; ?>
        </table>

        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                OBSERVAÇÕES IMPORTANTES
            </div>
            <div class="fonteReduzida" style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;">
                <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
                <?= ($operadora['dados']['observacoes'] != null) ? nl2br($operadora['dados']['observacoes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>

            </div>
        </div>

        <?php
        if ($simulacao['rede'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    REDE REFERENCIADA (Resumo) <span><?= (isset($operadora['dados']['url_rede']) && !is_null($operadora['dados']['url_rede'])) ? "- Para rede completa " . $this->Html->link('clique aqui', $operadora['dados']['url_rede'], ['target' => "_blank"]) . "." : "" ?></span>
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?= !is_null($operadora['dados']['redes']) ? nl2br($operadora['dados']['redes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                </div>

            </div>
            <div class="clearfix">&nbsp;</div>

        <?php
        } ?>

        <?php
        if ($simulacao['reembolso'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">

                <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                </div>


                <div style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;" class="fonteReduzida">
                    <?= !is_null($operadora['dados']['reembolsos']) ? nl2br($operadora['dados']['reembolsos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                </div>


            </div>


        <?php
        } ?>
        <div class="clearfix">&nbsp;</div>

        <div class="nobreak">
            <div style="width: 100%;border: 0.1mm solid #ccc; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
            </div>

            <div style="padding: 5px; width: 100%; border: 0.1mm solid #ccc;" class="fonteReduzida">
                <?php
                $areas = array();
                foreach ($operadora['tabelas'] as $tabela) {
                    foreach ($tabela['areas_comercializaco']['municipios'] as $municipio) {
                        $areas[$tabela['id']]['nome'] = $tabela['nome'] . ' - ' . $tabela['descricao'];
                        $areas[$tabela['id']]['atendimento'] = $tabela['abrangencia']['nome'];
                        $areas[$tabela['id']]['estados'][$municipio['estado']['id']]['nome'] =  $municipio['estado']['nome'];
                        $areas[$tabela['id']]['estados'][$municipio['estado']['id']]['municipios'][] =  $municipio['nome'];
                    }
                }
                ?>
                <div class="fonteReduzida" style="margin-left: 10px;">
                    <?php foreach ($areas as $area) : ?>
                        <strong><?= $area['nome'] ?></strong>
                        <br>
                        <div style="margin-left: 10px; border-bottom: 1px solid #cccccc;">
                            <b>COMERCIALIZAÇÃO </b>
                            </br>
                            <?php foreach ($area['estados'] as $estado) : ?>
                                <strong><?= $estado['nome'] ?>:</strong>
                                <?php foreach ($estado['municipios'] as $municipio) : ?>
                                    <?= $municipio ?>,
                                <?php endforeach; ?>
                                </br>
                            <?php endforeach; ?>
                            <br>
                            <b>ATENDIMENTO:</b> <?= $area['atendimento'] ?>
                            </br>
                        </div>
                        </br>
                    <?php endforeach; ?>
                </div>
            </div>

        </div>
        <?php
        if ($simulacao['carencia'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">

                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;">
                    CARÊNCIAS (Resumo)
                </div>
                <div class="fonteReduzida" style="padding: 5px; border: 0.1mm solid #ccc;">
                    <?= !is_null($operadora['dados']['carencias']) ? nl2br($operadora['dados']['carencias'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                </div>
            </div>
        <?php
        } ?>
        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                DEPENDENTES
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?= !is_null($operadora['dados']['opcionais']) ? nl2br($operadora['dados']['opcionais'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
            </div>
            <div class="clearfix">&nbsp;</div>

        </div>
        <?php
        if ($simulacao['informacao'] == 1) {
        ?>
            <div class="clearfix">&nbsp;</div>
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%;max-width: 100% !important">
                    DOCUMENTOS NECESSÁRIOS
                </div>
                <div class="fonteReduzida" style="padding: 5px;width: 100%; border: 0.1mm solid #ccc;max-width: 100% !important">
                    <?= !is_null($operadora['dados']['informacoes']) ? nl2br($operadora['dados']['informacoes'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
                </div>
            </div>


        <?php
        } ?>
        <div class="clearfix">&nbsp;</div>
        <div class="nobreak">
            <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                FORMAS DE PAGAMENTO
            </div>
            <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                <?= !is_null($operadora['dados']['formas_pagamentos']) ? nl2br($operadora['dados']['formas_pagamentos'][0]['descricao']) : "<div class='info'>Consulte Operadora</div>" ?>
            </div>
        </div>

    </page>
<?php
}
//die();
?>