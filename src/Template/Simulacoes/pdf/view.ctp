<h2><?php echo __('Books Catalogue'); ?></h2>
<table cellpadding="0" cellspacing="0">
    <thead>
    <tr>
        <th><?php echo 'Id'; ?></th>
        <th><?php echo 'Name'; ?></th>
        <th><?php echo 'Author'; ?></th>    
    </tr>
    </thead>
    <tbody>
    <?php foreach ($simulacao as $book): ?>
    <tr>
        <td><?php echo h($book['Book']['id']); ?></td>
        <td><?php echo h($book['Book']['name']); ?></td>
        <td><?php echo h($book['Book']['author']); ?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>  