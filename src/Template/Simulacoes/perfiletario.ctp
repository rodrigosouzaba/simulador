<div class="col-md-12 centralizada" style="padding: 20px;">
    <h3>Gerencie seus perfis empresariaisde maneira prática e inteligente</h3>
    <div class="col-md-6 col-md-offset-3 centralizada" style="padding: 20px;">
        <ul class="nav nav-pills centralizada nav-justified">
            <li><a data-toggle="pill" href="#individual">Digitar cada data de nascimento</a></li>
            <li><a data-toggle="pill" href="#arquivo">Copiar de algum arquivo</a><small>Excel, Word, PDF</small></li>
        </ul> 
    </div>
    <div class="tab-content">
        <div id="individual" class="tab-pane fade">
            <div class="col-md-12 centralizada well well-sm">
                <h3 style="margin-top: 0 !important;"> Qual o total de vidas da Empresa? </h3>
                &nbsp;
                <div class="col-xs-12 centralizada">
                    <?= $this->Form->create('detalhes', ['id' => 'detalhes']) ?>

                    <div class="col-xs-12 col-md-2 col-md-offset-2 centralizada">
                        <?= $this->Form->input('titulares', ['class' => 'centralizada']) ?> 
                    </div>
                    <div class="col-xs-12 col-md-2 centralizada">
                        <?= $this->Form->input('dependentes', ['class' => 'centralizada']) ?>        
                    </div>
                    <div class="col-xs-12 col-md-2 centralizada">
                        <?= $this->Form->input('agregados', ['class' => 'centralizada']) ?>        
                    </div>
                    <div class="col-xs-12 col-md-2 centralizada">
                        <?= $this->Form->input('afastados', ['class' => 'centralizada']) ?>        
                    </div>
                </div>
                <div class="col-md-12 centralizada">
                    <?=
                    $this->Html->link($this->Html->tag('span', '', [
                                'class' => '',
                                'aria-hidden' => 'true']) . ' Detalhar', '#', [
                        'class' => 'btn btn-md btn-primary',
                        'role' => 'button',
                        'escape' => false,
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'id' => 'detalhar'])
                    ?>
                </div>  
                <?= $this->Form->end() ?>
            </div>
            <div class="col-xs-12 fonteReduzida centralizada">

                <div class="col-xs-12 centralizada" id="formvidas" >
                </div>

            </div>


        </div>
        <div id="arquivo" class="tab-pane fade">
            <div class="col-md-12 centralizada well well-sm">
                <h3>Digite as datas de nascimento em suas respectivas categorias</h3>
                <?= $this->Form->create('colar-arquivo', ['id' => 'colar-arquivo']) ?>

                <table class="table table-condensed">
                    <tr>
                        <td></td>
                        <td class="centralizada">Masculino</td>
                        <td class="centralizada">Feminino</td>
                    </tr>
                    <tr>
                        <td class="centralizada centralizadaVertical"><h4>Titulares</h4></td>
                        <td>                            
                            <?= $this->Form->input('Titulares.m', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea']) ?>
                        </td>
                        <td>
                            <?= $this->Form->input('Titulares.f', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="centralizada centralizadaVertical"><h4>Dependentes</h4></td>
                        <td>                            
                            <?= $this->Form->input('Dependentes.m', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea']) ?>
                        </td>
                        <td>
                            <?= $this->Form->input('Dependentes.f', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="centralizada centralizadaVertical"><h4>Agregados</h4></td>
                        <td>                            
                            <?= $this->Form->input('Agregados.m', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea']) ?>
                        </td>
                        <td>
                            <?= $this->Form->input('Agregados.f', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="centralizada centralizadaVertical"><h4>Afastados</h4></td>
                        <td>                            
                            <?= $this->Form->input('Afastados.m', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea']) ?>
                        </td>
                        <td>
                            <?= $this->Form->input('Afastados.f', ['label' => '', 'class' => 'centralizada fonteReduzida', 'type' => 'textarea']) ?>
                        </td>
                    </tr>
                </table>

                <div class="col-md-12">
                    <div class="clearfix">&nbsp;</div>
                    <div class="col-md-12 centralizada">
                        <?=
                        $this->Html->link($this->Html->tag('span', '', [
                                    'class' => '',
                                    'aria-hidden' => 'true']) . ' Calcular Faixas Etárias', '#', [
                            'class' => 'btn btn-md btn-primary',
                            'role' => 'button',
                            'escape' => false,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'id' => 'enviar-colados'])
                        ?>
                    </div>  
                    <div id="resposta-colar" style="text-align: left;"></div>
                </div>
                <?= $this->Form->end(); ?>




            </div>
        </div>
    </div>

    <script type="text/javascript">
        //    $("#detalhes").hide();
        $("#detalhar").click(function () {
            //        $("#formvidas").show();
            //        var vidas = parseInt($(this).val());
            $.ajax({
                type: "put",
                data: $("#detalhes").serialize(),
                url: "<?php echo $this->request->webroot ?>simulacoes/perfiletario/",
                success: function (data) {
                    $("#formvidas").empty();
                    $("#formvidas").append(data);

                }
            });
        });
        $("#enviar-colados").click(function () {
            $.ajax({
                type: "post",
                data: $("#colar-arquivo").serialize(),
                url: "<?php echo $this->request->webroot ?>simulacoes/colar-arquivo/",
                success: function (data) {
                    $("#resposta-colar").empty();
                    $("#resposta-colar").append(data);

                }
            });
        });

    </script>