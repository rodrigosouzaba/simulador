<?= $this->Form->input('estado_id', ['options' => $estados, 'label' => 'Estados', 'empty' => 'SELECIONE']); ?>


<script>
    $("#estado-id").change(function () {
        $.ajax({
            type: "Post",
            url: "<?php echo $this->request->webroot ?>simulacoes/preencheSelect/r/" + (this).value,
            data: $("#estado-id").serialize(),
            success: function (data) {
                $("#selectRegioes").empty();
                $("#selectRegioes").append(data);
            }
        });

       

    });
</script>
