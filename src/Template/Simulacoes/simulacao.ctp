<style>
    .alert-secondary {
        background-color: #f5f5f5;
    }
</style>
<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
$error = $session->read('erro');
$session->delete('erro');

?>
<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Cálculos']); ?>
<div class="card-body">
    <div class="row">
        <div class="col-sm-12">
            <div id="resposta">
                <?= $this->Form->create('operadoras', ['id' => 'operadoras', 'url' => ['action' => 'salvaFiltros']]); ?>
                <?= $this->element('filtro_simulacao'); ?>
                <div id="resultado" class="total col-xs-12">
                    <?= $this->element('body_calculos'); ?>
                </div>
            </div>
            <?= $this->element('avisos'); ?>
            <div id="aviso" class="col-xs-12 centralizada" style="display: none;">
                <div class="text-danger">Selecione os cálculos que deseja exibir na cotação</div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Form->end(); ?>
<?php echo $this->element('forms/buttonsFechar') ?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#fieldShareEmail").hide();

        $(function() {
            $('[data-tooltip="true"]').tooltip();
        });
        $('[data-toggle="tooltip"]').tooltip();
        $('.todas').click(function() {
            $('.' + (this).value).prop('checked', this.checked);
        });

        var atendimento = '<?= $filtro["abrangencia_id"] ?>';
        var acomodacao = '<?= $filtro["tipo_id"] ?>';
        var tipo_produto = '<?= $filtro["tipo_produto_id"] ?>';
        var coparticipacao = '<?= $filtro["coparticipacao"] ?>';
        var tipo_contratacao = '<?= $filtro["tipo_contratacao"] ?>';
        var filtroreembolso = '<?= $filtro["filtroreembolso"] ?>';

        if (atendimento || acomodacao || tipo_produto || coparticipacao || tipo_contratacao || filtroreembolso) {
            $("#abrangencia-id").val(atendimento);
            $("#tipo-id").val(acomodacao);
            $("#tipo-produto-id").val(tipo_produto);
            $("#coparticipacao").val(coparticipacao);
            $("#tipo-contratacao").val(tipo_contratacao);
            $("#filtroreembolso").val(filtroreembolso);
            $("#filtroreembolso").change();


        } else {
            if ('<?= $error ?>') {
                $("#aviso").appendTo("#conteudoAviso");
                $("#modalAviso").modal("show");
                $("#aviso").show();
            }
        }
    });

    $('.c').click(function() {
        $('.' + $(this).attr("value")).prop('checked', this.checked);
    });
</script>

