<?php if ($is_filtro) : ?>
    <?= $this->element('resposta_calculos') ?>
    <script>
        $(".todas").click(function() {
            $('.' + (this).value).prop('checked', this.checked);
        });
    </script>
<?php else : ?>
    <?= $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Cálculos']); ?>
    <?= $this->Form->create('operadoras', ['id' => 'operadoras', 'url' => ['action' => 'salvaFiltros']]); ?>
    <?= $this->element('filtro_simulacao'); ?>

    <div id="resultado">
        <?= $this->element('resposta_calculos') ?>
        <script>
            $(".todas").click(function() {
                $('.' + (this).value).prop('checked', this.checked);
            });
        </script>
    </div>

    <?php echo $this->Form->end(); ?>
    <?php echo $this->element('forms/buttonsFechar') ?>
<?php endif; ?>