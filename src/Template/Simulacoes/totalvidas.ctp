<h3>Digite as datas de nascimento em suas respectivas categorias</h3>

<?= $this->Form->create('formVidas', ['id' => 'formVidas']) ?>
<div class="col-xs-12 col-md-3" >
    <h4>Titulares</h4>
    <?php
    $i = 1;
    for ($i = 1; $i <= $detalhamento['titulares']; $i++) {
        ?>
        <div class="col-xs-12 col-md-6">
            Masculino
            &nbsp;
            <?= $this->Form->input('Titulares.m.data' . $i, ['label' => '', 'class' => 'data-nascimento centralizada']) ?>
        </div>
        <div class="col-xs-12 col-md-6">
            Feminino
            &nbsp;
            <?= $this->Form->input('Titulares.f.data' . $i, ['label' => '', 'class' => 'data-nascimento centralizada']) ?>
        </div>
    <?php } ?>
</div>
<div class="col-xs-12 col-md-3" >
    <h4>Dependentes</h4>
    <?php
    $i = 1;
    for ($i = 1; $i <= $detalhamento['dependentes']; $i++) {
        ?>
        <div class="col-xs-12 col-md-6">
            Masculino
            &nbsp;
            <?= $this->Form->input('Dependentes.m.data' . $i, ['label' => '', 'class' => 'data-nascimento centralizada']) ?>
        </div>
        <div class="col-xs-12 col-md-6">
            Feminino
            &nbsp;
            <?= $this->Form->input('Dependentes.f.data' . $i, ['label' => '', 'class' => 'data-nascimento centralizada']) ?>
        </div>
        <?php
    }
    if ($detalhamento['dependentes'] == '' || $detalhamento['dependentes'] == 0) {
        ?>
        <br>
        <small style="padding-top: 20px"><i>Nada informado</i></small>
        <?php
    }
    ?>
</div>
<div class="col-xs-12 col-md-3" >
    <h4>Agregados</h4>
    <?php
    $i = 1;
    for ($i = 1; $i <= $detalhamento['agregados']; $i++) {
        ?>
        <div class="col-xs-12 col-md-6">
            Masculino
            &nbsp;
            <?= $this->Form->input('Agregados.m.data' . $i, ['label' => '', 'class' => 'data-nascimento centralizada']) ?>
        </div>
        <div class="col-xs-12 col-md-6">
            Feminino
            &nbsp;
            <?= $this->Form->input('Agregados.f.data' . $i, ['label' => '', 'class' => 'data-nascimento centralizada']) ?>
        </div>
        <?php
    }
    if ($detalhamento['agregados'] == '' || $detalhamento['dependentes'] == 0) {
        ?>
        <br>
        <small style="padding-top: 20px"><i>Nada informado</i></small>
        <?php
    }
    ?>
</div>
<div class="col-xs-12 col-md-3" >
    <h4>Afastados</h4>
    <?php
    $i = 1;
    for ($i = 1; $i <= $detalhamento['afastados']; $i++) {
        ?>
        <div class="col-xs-12 col-md-6">
            Masculino
            &nbsp;
            <?= $this->Form->input('Afastados.m.data' . $i, ['label' => '', 'class' => 'data-nascimento centralizada']) ?>
        </div>
        <div class="col-xs-12 col-md-6">
            Feminino
            &nbsp;
            <?= $this->Form->input('Afastados.f.data' . $i, ['label' => '', 'class' => 'data-nascimento centralizada']) ?>
        </div>
    <?php }
     if ($detalhamento['afastados'] == '' || $detalhamento['dependentes'] == 0) {
        ?>
        <br>
        <small style="padding-top: 20px"><i>Nada informado</i></small>
        <?php
    }
    ?>
</div>

<div class="col-md-12">
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12 centralizada">
        <?=
        $this->Html->link($this->Html->tag('span', '', [
                    'class' => '',
                    'aria-hidden' => 'true']) . ' Calcular', '#', [
            'class' => 'btn btn-md btn-primary',
            'role' => 'button',
            'escape' => false,
            'data-toggle' => 'tooltip',
            'data-placement' => 'top',
            'id' => 'calcular'])
        ?>
    </div>               
</div>
<?= $this->Form->end() ?>

<script type="text/javascript">
//    $("#formvidas").hide();
    $('.data-nascimento').mask("99/99/9999");
    $("#vidas").keyup(function () {
//        $("#formvidas").show();
        var vidas = parseInt($(this).val());
        $.ajax({
            type: "get",
//            data: dependentes.serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/totalvidas/" + vidas,
            success: function (data) {
                $("#formvidas").empty();
                $("#formvidas").append(data);

            }
        });
    });
    $("#calcular").click(function () {
//        $("#formvidas").show();
        $.ajax({
            type: "post",
            data: $("#formVidas").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/totalvidas/",
            success: function (data) {
                $("#formvidas").empty();
                $("#formvidas").append(data);

            }
        });
    });

</script>