<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Simulaco'), ['action' => 'edit', $simulacao->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Simulaco'), ['action' => 'delete', $simulacao->id], ['confirm' => __('Are you sure you want to delete # {0}?', $simulacao->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Simulaco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas Regioes'), ['controller' => 'TabelasRegioes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabelas Regio'), ['controller' => 'TabelasRegioes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="simulacoes view large-9 medium-8 columns content">
    <h3><?= h($simulacao->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Tabelas Regio') ?></th>
            <td><?= $simulacao->has('tabelas_regio') ? $this->Html->link($simulacao->tabelas_regio->id, ['controller' => 'TabelasRegioes', 'action' => 'view', $simulacao->tabelas_regio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($simulacao->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa1') ?></th>
            <td><?= $this->Number->format($simulacao->faixa1) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa2') ?></th>
            <td><?= $this->Number->format($simulacao->faixa2) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa3') ?></th>
            <td><?= $this->Number->format($simulacao->faixa3) ?></td>
        </tr>
        <tr>
            <th><?= __('Data') ?></th>
            <td><?= h($simulacao->data) ?></td>
        </tr>
    </table>
</div>
