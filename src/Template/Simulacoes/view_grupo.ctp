<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
//debug($maisbarato);
//debug($intermediario);
//debug($maiscaro);
//die();
if (isset($todas_simulacoes)) {
    ?>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">
        <h4 class="centralizada">Cálculo por Grupo Familiar</h4>
        <!--<div class="well well-lg">-->
        <h4 class="centralizada"> <b>Total da empresa</b></h4>
        <div class="col-md-4 barato centralizada">
            <h4>
                Menor preço
            </h4>
            <?php
            $totalMenor = 0;
            foreach ($todas_simulacoes as $simulacao) {
                $i = 0;
                $total = 0;
                for ($i = 1; $i <= 12; $i++) {
                    $totalMenor = $totalMenor + ($simulacao['faixa' . $i] * $maisbarato[$simulacao['id']]['faixa' . $i]);
                }
            }
            ?>
            <b style="font-size: 110%"><?= $this->Number->currency($totalMenor, "R$ ") ?></b>
        </div>
        <div class="col-md-4 intermediario centralizada">
            <h4>
                Preço médio    
            </h4>
            <?php
            $totalIntermediario = 0;
            foreach ($todas_simulacoes as $simulacao) {
                $i = 0;
                $total = 0;
                for ($i = 1; $i <= 12; $i++) {
                    $totalIntermediario = $totalIntermediario + ($simulacao['faixa' . $i] * $intermediario[$simulacao['id']]['faixa' . $i]);
                }
            }
            ?>
            <b style="font-size: 110%"><?= $this->Number->currency($totalIntermediario, "R$ ") ?></b>
        </div>
        <div class="col-md-4 caro centralizada">
            <h4>
                Maior preço    
            </h4>
            <?php
            $totalMaior = 0;
            foreach ($todas_simulacoes as $simulacao) {
                $i = 0;
                $total = 0;
                for ($i = 1; $i <= 12; $i++) {
                    $totalMaior = $totalMaior + ($simulacao['faixa' . $i] * $maiscaro[$simulacao['id']]['faixa' . $i]);
                }
            }
            ?>
            <b style="font-size: 110%"><?= $this->Number->currency($totalMaior, "R$ ") ?></b>
        </div>
        <!--</div>-->
    </div>
    <div class="clearfix" style="border-bottom: 1px solid #ddd;">&nbsp;</div>
    <div class="clearfix" >&nbsp;</div>


    <div id="resultado" class="total col-xs-12">
        <table class="table table-condensed table-hover">

            <?php
            $idOperadora = null;
            foreach ($todas_simulacoes as $simulacao) {
                ?>
                <tr class="centralizada">
                    <td colspan="3" class="centralizada">
                        <h3 class="panel-title">
                            <b><?= $simulacao['nome_titular'] ?></b>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td class="centralizada">
                        <h4>
                            Menor preço    
                        </h4>
                        <?php
                        $i = 0;
                        $total = 0;
                        for ($i = 1; $i <= 12; $i++) {
                            $total = $total + ($simulacao['faixa' . $i] * $maisbarato[$simulacao['id']]['faixa' . $i]);
                        }
                        ?>
                        <?= $maisbarato[$simulacao['id']]['operadora']['nome']; ?><br/>
                        <b style="font-size: 120%"><?= $this->Number->currency($total, "R$ ") ?></b><br/>
                        <?= $maisbarato[$simulacao['id']]['descricao']; ?>
                    </td>
                    <td class="centralizada">
                        <h4>
                            Preço médio
                        </h4>
                        <?php
                        $i = 0;
                        $total = 0;
                        for ($i = 1; $i <= 12; $i++) {
                            $total = $total + ($simulacao['faixa' . $i] * $intermediario[$simulacao['id']]['faixa' . $i]);
                        }
                        ?>
                        <?= $intermediario[$simulacao['id']]['operadora']['nome']; ?><br/>
                        <b style="font-size: 120%"><?= $this->Number->currency($total, "R$ ") ?></b><br/>
                        <?= $intermediario[$simulacao['id']]['descricao']; ?>

                    </td>
                    <td class="centralizada">
                        <h4>
                            Maior preço
                        </h4>

                        <?php
                        $i = 0;
                        $total = 0;
                        for ($i = 1; $i <= 12; $i++) {
                            $total = $total + ($simulacao['faixa' . $i] * $maiscaro[$simulacao['id']]['faixa' . $i]);
                        }
                        ?>
                        <?= $maiscaro[$simulacao['id']]['operadora']['nome']; ?><br/>
                        <b style="font-size: 120%"><?= $this->Number->currency($total, "R$ ") ?></b><br/>
                        <?php // debug($maiscaro[$simulacao['id']]); ?>
                        <?= $maiscaro[$simulacao['id']]['descricao']; ?>

                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <?= $this->Html->link('Detalhar', ['action' => 'view', $simulacao['id']], ['class' => 'btn btn-md btn-primary']); ?>
                    </td>
                </tr>


                <?php
            }
            echo $this->Form->end();
            ?>
        </table>

    </div>





<?php } else {
    ?>
    <div class="clearfix">&nbsp;</div>
    <div class="alert alert-danger centralizada" role="alert"> Nenhuma tabela encontrada dentro dos Filtros selecionados</div>

<?php } ?>
</div>
<?= $this->element('processando'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('.todas').click(function () {
            $('.' + (this).value).prop('checked', this.checked);
        });
    });
    $('.c').click(function () {
        $('.' + $(this).attr("value")).prop('checked', this.checked);

        alert();
    });

    $("#pdf").click(function () {

        $.ajax({
            type: "post",
//            data: $("#operadoras").serialize(),
//            url: "<?php echo $this->request->webroot ?>simulacoes/pdf/",
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#fechar').trigger('click');
            }
        });
    });

    $("#abrangencia-id").change(function () {
        $.ajax({
            type: "post",
            data: $("#operadoras").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/filtro/",
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#fechar').trigger('click');
                $('#resultado').empty();
                $('#resultado').append(data);
                $("body").removeClass("modal-open");
                $('.modal-backdrop').remove()
            }
        });
    });
    $("#tipo-id").change(function () {
        $.ajax({
            type: "post",
            data: $("#operadoras").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/filtro/",
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#fechar').trigger('click');
                $('#resultado').empty();
                $('#resultado').append(data);
                $("body").removeClass("modal-open");
                $('.modal-backdrop').remove()
            }
        });
    });
    $("#tipo-produto-id").change(function () {
        $.ajax({
            type: "post",
            data: $("#operadoras").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/filtro/",
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#fechar').trigger('click');
                $('#resultado').empty();
                $('#resultado').append(data);
                $("body").removeClass("modal-open");
                $('.modal-backdrop').remove()
            }
        });
    });
    $("#coparticipacao").change(function () {

        $.ajax({
            type: "post",
            data: $("#operadoras").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/filtro/",
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#fechar').trigger('click');
                $('#resultado').empty();
                $('#resultado').append(data);
                $("body").removeClass("modal-open");
                $('.modal-backdrop').remove()
            }
        });
    });
    $("#tipo-contratacao").change(function () {
        $.ajax({
            type: "post",
            data: $("#operadoras").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/filtro/",
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#fechar').trigger('click');
                $('#resultado').empty();
                $('#resultado').append(data);
                $("body").removeClass("modal-open");
                $('.modal-backdrop').remove()
            }
        });
    });
    $("#filtroreembolso").change(function () {
        $.ajax({
            type: "post",
            data: $("#operadoras").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/filtro/",
            beforeSend: function () {
                $('#loader').trigger('click');
            },
            success: function (data) {
                $('#fechar').trigger('click');
                $('#resultado').empty();
                $('#resultado').append(data);
                $("body").removeClass("modal-open");
                $('.modal-backdrop').remove()
            }
        });
    });
</script>