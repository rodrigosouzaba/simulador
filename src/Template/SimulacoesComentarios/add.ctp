<?= $this->Form->create($simulacoesComentario) ?>
<?php
echo $this->Form->input('simulacao_id', ['id' => 'simulacao_id', 'value' => $simulacao_id, 'type' => 'hidden']);
echo $this->Form->input('mensagem');
?>
<div  class="centralizada">
    <?= $this->Form->input('alerta', ['type' => 'checkbox', 'value' => 'S', 'label' => 'Criar um alerta para ações futuras?', 'onchange' => "valueChanged()"]);
    ?>
</div>
<div id="dataalerta" class="centralizada">
    <?= $this->Form->input('data_alerta', ['empty' => true, 'label' => 'Selecione a data']); ?>
    <div class="clearfix">&nbsp;</div>
</div>
<div class="centralizada">
    <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary btn-sm']) ?>
</div>
<?= $this->Form->end() ?>

<script type="text/javascript">
    $("#dataalerta").hide();
    $("#alerta").click(function () {
        $("#dataalerta").show();
    });

    function valueChanged()
    {
        if ($('#alerta').is(":checked"))
            $("#dataalerta").show();
        else
            $("#dataalerta").hide();
    }
</script>