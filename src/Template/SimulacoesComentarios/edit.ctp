<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $simulacoesComentario->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $simulacoesComentario->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Simulacoes Comentarios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="simulacoesComentarios form large-9 medium-8 columns content">
    <?= $this->Form->create($simulacoesComentario) ?>
    <fieldset>
        <legend><?= __('Edit Simulacoes Comentario') ?></legend>
        <?php
            echo $this->Form->input('mensagem');
            echo $this->Form->input('alerta');
            echo $this->Form->input('data_alerta', ['empty' => true]);
            echo $this->Form->input('simulacao_id', ['options' => $simulacoes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
