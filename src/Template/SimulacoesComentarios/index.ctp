<div class="modal-header centralizada">
    <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"> Cálculo Nº <b><?= $simulacao_id ?></b> </h4> 
</div>
<div class="modal-body" >    
    <div class="centralizada">
        <?=
        $this->Html->link('Adicionar Comentário', '#', [
            'class' => 'novocomentario btn btn-sm btn-primary',
            'style' => 'margin: 3px 0;',
            'id' => 'novocomentario'])
        ?>
    </div>
    <div id="respostaComentario"></div>
    <?php if (!empty($simulacoesComentarios)) { ?>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?= 'Data' ?></th>
                    <!--<th><?= 'data_alerta' ?></th>-->
                    <th><?= 'Mensagem' ?></th>
                    <th><?= 'Alerta' ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($simulacoesComentarios as $simulacoesComentario): ?>
                    <tr>
                        <!--<td><?= $this->Number->format($simulacoesComentario->id) ?></td>-->
                        <td><?= h($simulacoesComentario->created) ?></td>
                        <!--<td><?= h($simulacoesComentario->alerta) ?></td>-->
                        <!--<td><?= h($simulacoesComentario->data_alerta) ?></td>-->
                        <td><?= $simulacoesComentario->mensagem ?></td>
                        <td><?= $simulacoesComentario->data_alerta ?></td>
                        <td class="actions">
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $simulacoesComentario->id], ['confirm' => __('Tem certeza que deseja deletar?', $simulacoesComentario->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php } ?>

</div>
<script type="text/javascript">
    $('#processo').hide('click');
    $(".reenvio").click(function () {
        $.ajax({
            beforeSend: function () {
                $('#processo').show('click');
            },
            success: function (data) {
                $('#close').trigger('click');
            }
        });
    });

    $("#novocomentario").click(function () {
        $.ajax({
            type: "GET",
            url: "<?= $this->request->webroot ?>simulacoes-comentarios/add/<?=$simulacao_id?>",
            success: function (data) {
                $("#respostaComentario").empty();
                $("#respostaComentario").append(data);
            }
        });
    });
</script>