<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Simulacoes Comentario'), ['action' => 'edit', $simulacoesComentario->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Simulacoes Comentario'), ['action' => 'delete', $simulacoesComentario->id], ['confirm' => __('Are you sure you want to delete # {0}?', $simulacoesComentario->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Simulacoes Comentarios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Simulacoes Comentario'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="simulacoesComentarios view large-9 medium-8 columns content">
    <h3><?= h($simulacoesComentario->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Alerta') ?></th>
            <td><?= h($simulacoesComentario->alerta) ?></td>
        </tr>
        <tr>
            <th><?= __('Simulaco') ?></th>
            <td><?= $simulacoesComentario->has('simulaco') ? $this->Html->link($simulacoesComentario->simulaco->id, ['controller' => 'Simulacoes', 'action' => 'view', $simulacoesComentario->simulaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($simulacoesComentario->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($simulacoesComentario->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Data Alerta') ?></th>
            <td><?= h($simulacoesComentario->data_alerta) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Mensagem') ?></h4>
        <?= $this->Text->autoParagraph(h($simulacoesComentario->mensagem)); ?>
    </div>
</div>
