<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Simulacoes Tabela'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="simulacoesTabelas index large-9 medium-8 columns content">
    <h3><?= __('Simulacoes Tabelas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('simulacao_id') ?></th>
                <th><?= $this->Paginator->sort('tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($simulacoesTabelas as $simulacoesTabela): ?>
            <tr>
                <td><?= $this->Number->format($simulacoesTabela->id) ?></td>
                <td><?= $simulacoesTabela->has('simulaco') ? $this->Html->link($simulacoesTabela->simulaco->id, ['controller' => 'Simulacoes', 'action' => 'view', $simulacoesTabela->simulaco->id]) : '' ?></td>
                <td><?= $simulacoesTabela->has('tabela') ? $this->Html->link($simulacoesTabela->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $simulacoesTabela->tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $simulacoesTabela->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $simulacoesTabela->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $simulacoesTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $simulacoesTabela->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
