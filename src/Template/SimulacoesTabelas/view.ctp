<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Simulacoes Tabela'), ['action' => 'edit', $simulacoesTabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Simulacoes Tabela'), ['action' => 'delete', $simulacoesTabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $simulacoesTabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Simulacoes Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Simulacoes Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="simulacoesTabelas view large-9 medium-8 columns content">
    <h3><?= h($simulacoesTabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Simulaco') ?></th>
            <td><?= $simulacoesTabela->has('simulaco') ? $this->Html->link($simulacoesTabela->simulaco->id, ['controller' => 'Simulacoes', 'action' => 'view', $simulacoesTabela->simulaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tabela') ?></th>
            <td><?= $simulacoesTabela->has('tabela') ? $this->Html->link($simulacoesTabela->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $simulacoesTabela->tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($simulacoesTabela->id) ?></td>
        </tr>
    </table>
</div>
