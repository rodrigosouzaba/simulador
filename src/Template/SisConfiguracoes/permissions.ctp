<section id="AppSisConfiguracoes" <?= ($modal == 1)? 'style="padding:0;"' : 'style="padding:0% 5% 5% 5%"' ?>>
	<div class="section-body card" <?= ($modal == 1)? 'style="margin:0;"' : '' ?>>
	    <div class="card-head card-head-sm style-primary">
	        <header>
                <i class="fa fa-fw fa-cog" style="margin-bottom:0;"></i> Configurações 
                <i class="fa fa-angle-right" style="margin-bottom:0;"></i> <b>Permissões de Acesso</b>
            </header>
	    </div>
		<div class="card-body tab-content">
			<div id="tab1" class="tab-pane active">	
				<div id="gridFuncionalidades">     
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-6">
	                		<div class="alert alert-callout" role="alert" style="margin-left: 0px;">
								<h4>
									Usuarios 
									<button id="listUsers" type="button" class="btn btn-xs btn-default pull-right">GERIR</button>
								</h4>
								<hr style="margin:0 0 7px 0;"/>
								<table style="width:100%;margin-bottom:0" cellpadding="0" cellspacing="0">
									<tr>
										<th>Total</th>
										<th>Ativos</th>
										<th>Inativos</th>
									</tr>
									<tr>
										<td><?= $totalUsers; ?></td>
										<td><?= $usersAtivos; ?></td>
										<td><?= $usersInativos; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="col-lg-6">
	                		<div class="alert alert-callout" role="alert" style="margin-left: 0px;">
								<h4>
									Perfis 
									<button id="listGrupos" type="button" class="btn btn-xs btn-default pull-right">GERIR</button>
								</h4>
								<hr style="margin:0 0 7px 0;"/>
								<table style="width:100%;margin-bottom:0" cellpadding="0" cellspacing="0">
									<tr>
										<th>Total</th>
										<th>Ativos</th>
										<th>Inativos</th>
									</tr>
									<tr>
										<td><?= count($dadosGrupos); ?></td>
										<td><?= $gruposAtivos; ?></td>
										<td><?= $gruposInativos; ?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>

					<hr style="margin-left: 0px;"/>

					<div class="row">
						<div class="col-lg-6">
	                		<div class="alert alert-callout alert-warning" role="alert" style="margin-left: 0px;">
								<h4>
									Funcionalidades 
									<button id="addFuncionalidades" type="button" class="btn btn-xs btn-warning pull-right">Cadastrar</button>
								</h4>
								<hr style="margin:0 0 7px 0;"/>
								<table style="width:100%;margin-bottom:0" cellpadding="0" cellspacing="0">
									<tr>
										<th>Total</th>
										<th>Ativos</th>
										<th>Inativos</th>
										<th>Sem Permissão</th>
									</tr>
									<tr>
										<td><?= count($dadosFuncionalidades); ?></td>
										<td><?= $funcionalidadesAtivos; ?></td>
										<td><?= $funcionalidadesInativos; ?></td>
										<td><?= $funcionalidadesSemPermissao; ?></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="col-lg-6">
	                		<div class="alert alert-callout alert-info" role="alert" style="margin-left: 0px;">
								<h4>
									Permissões
									<button id="addPermissoes" type="button" class="btn btn-xs btn-info pull-right">Atualizar Automaticamente</button>
								</h4>
								<hr style="margin:0 0 7px 0;"/>
								<table style="width:100%;margin-bottom:0" cellpadding="0" cellspacing="0">
									<tr>
										<th>Total</th>
										<th>Adicionar</th>
										<th>Remover</th>
										<th>Órfãs</th>
									</tr>
									<tr>
										<td><?= $dadosPermissions['total']; ?></td>
										<td><?= count($dadosPermissions['adicionar']); ?></td>
										<td><?= count($dadosPermissions['remover']); ?></td>
										<td><?= count($dadosPermissions['semVinculoComFuncionalidade']); ?></td>
									</tr>
								</table>
							</div>
						</div>               
					</div>
					<div class="row" style="margin-top:10px;">
						<div class="col-lg-6">
	                		<div class="alert alert-callout alert-danger" role="alert" style="margin-left: 0px;">
								<h4>
									Módulos 
									<button id="listModulos" type="button" class="btn btn-xs btn-danger pull-right">GERIR</button>
								</h4>
								<hr style="margin:0 0 7px 0;"/>
								<table style="width:100%;margin-bottom:0" cellpadding="0" cellspacing="0">
									<tr>
										<th>Total</th>
										<th>Ativos</th>
										<th>Inativos</th>
										<th>Sem Funcionalidade</th>
									</tr>
									<tr>
										<td><?= count($dadosModulos); ?></td>
										<td><?= $modulosAtivos; ?></td>
										<td><?= $modulosInativos; ?></td>
										<td><?= $moduloSemFuncionalidade ?></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="col-lg-6">
	                		<div class="alert alert-callout alert-success" role="alert" style="margin-left: 0px;">
								<h4>
									Sessão 
									<button id="lock" type="button" class="btn btn-xs btn-success pull-right">REVALIDAR SESSÃO</button>
								</h4>
								<hr style="margin:0 0 7px 0;"/>
								<table style="width:100%;margin-bottom:0" cellpadding="0" cellspacing="0">
									<tr>
										<th>Último login</th>
									</tr>
									<tr>
										<td><?= $this->Time->format($dadosUser['ultimologin'] , 'dd/MM/YY  HH:mm') ?> </td>
									</tr>
								</table>
							</div>
						</div>
					</div>

					<hr style="margin-left: 0px;"/>

	                <?php foreach ($dadosModulos as $modulo) { ?>
	                <h4 style="padding-left: 5px;cursor:pointer;"><label class="label label-<?= ($modulo->active == 1) ? 'primary' : 'danger'; ?>" style="text-transform:uppercase;">Módulo: <?= $modulo->name; ?></label> <i id="<?= $modulo->id ?>" class="fa fa-edit editModulos"></i></h4> 
	                <table class="table table-condensed table-striped table-hover" cellspacing="0" width="100%" style="">
	                    <thead>
	                        <tr>
	                            <th>Funcionalidade</th>
	                            <th class="text-center" style="width:150px;">Grupos Associadas</th>
	                            <th class="text-center" style="width:180px;">Permissões Associadas</th>
	                            <th style="width:100px;">Ativo</th>
	                            <th style="width:50px;">Ações</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <?php foreach ($dadosFuncionalidades as $v) { ?>
								<?php if ($modulo->id == $v->sis_modulo_id) { ?>
									<tr>
										<td><?= $v->name; ?></td>
										<td class="text-center"><?= $v->countGroupsAssociateds; ?></td>
										<td class="text-center"><?= $v->countPermissionsAssociated; ?></td>
										<td><label class="label label-<?= $v->ativo_label; ?>"><?= $v->ativo; ?></label></td>
										<td>
											<div class="btn-group">
												<button type="button" class="btn btn-default btn-icon-toggle dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i></button>
												<ul class="dropdown-menu dropdown-menu-right" role="menu">
													<li><?= $this->Html->link('<i class="fa fa-edit"></i>&nbsp Editar', 'javascript: void(0)', array("escape" => false, 'id' => $v->id, 'class' => 'editFuncionalidades')) ?></li>
													<li><?= $this->Html->link('<i class="fa fa-trash"></i>&nbsp Excluir', 'javascript: void(0)', array("escape" => false, 'id' => $v->id, 'class' => 'delFuncionalidades')) ?></li>
													<li style="background: #F1F1F1; font-size: 9px; text-align: center;">Atualizado em: <?= @$v['modified'] ?></li>
												</ul>
											</div>
										</td>
									</tr>
	                        	<?php } ?>
							<?php } ?>
	                    </tbody>
	                </table>
                    <?php } ?>
	            </div>				
			</div>
		</div>
	</div>
</section>