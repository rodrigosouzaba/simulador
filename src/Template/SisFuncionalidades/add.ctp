
<?php echo $this->Form->create('SisFuncionalidade', ['class' => 'form form-validate', 'role' => 'form']); ?>
<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Cadastrar Funcionalidade']); ?>
<div class="card-body">
    <div class="row">
		<div class="col-md-12">
			<div class="form-group"> 
	    		<?php echo $this->Form->input('sis_modulo_id', array('label' => 'Módulo', 'required', 'empty' => 'Selecione', 'class'=>'chosen', 'options' => $optionsModulos)); ?>
            </div>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">                
                <?php echo $this->Form->input('name', ['label' => 'Nome', 'class' => 'form-control', 'required' ]); ?>
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-12">
            <div class="form-group">
			    <label class="control-label">Permissões</label>
			    <?php echo $this->Form->input('permission', ['label' => false, 'multiple' => 'multiple', 'type' => 'select', 'options' => $optionsPermissions, 'class' => 'chosen-select', 'required'=>'required', 'class'=>'form-control', 'div'=>['class'=>'col-sm-12 controls'],'after'=>'Obs.: Permissões com (*) são órfãs.']); ?>
			</div>
		</div>
	</div>
    <div class="row">
    	<div class="col-md-12">
    		
		</div>
	</div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Grupos</label>
                <?= $this->Form->input('sis_grupo', ['options' => $optionsGrupos, 'label' => false, 'multiple' => 'multiple', 'selected' => [], 'class' => 'chosen-select', 'required'=>'required']); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('forms/buttons') ?>
<?php echo $this->Form->end(); ?>
