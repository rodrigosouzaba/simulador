<?php echo $this->Form->create('Group', ['class' => 'form form-validate', 'role' => 'form']); ?>
<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Cadastrar Grupos']); ?>
<div class="card-body">
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
            <div class="form-group">
                <?php echo $this->Form->input('name', ['label' => 'Nome', 'required', 'class' => 'form-control']); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
            <div class="form-group">
                <?php echo $this->Form->input('funcionalidade', [
                    'label' => 'Funcionalidades',
                    'multiple' => 'true',
                    'required',
                    'type' => 'select',
                    'options' => $optionsFuncionalidades,
                    'class'=> 'chosen-select'
                ]);?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('forms/buttons') ?>
<?php echo $this->Form->end(); ?>
