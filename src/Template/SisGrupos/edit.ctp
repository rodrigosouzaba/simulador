<?= $this->Form->create('Group', ['class' => 'form form-validate', 'role' => 'form']); ?>
<?= $this->element('forms/title', ['title' => '<i class="fa fa-edit"></i> Editar Grupo']); ?>
<?= $this->Form->hidden('id'); ?>
<div class="card-body">
    <div class="row">
        <div class="col-md-9">
            <div class="form-group">
                <?= $this->Form->input('name', ['label' => 'Nome', 'class' => 'form-control', 'required']); ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
        	    <?= $this->Form->input('active', ['type' => 'select', 'label' => 'Ativo', 'required', 'options' => [1 => 'Sim', 0 => 'Não']]); ?>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:15px;">
    	<div class="col-md-12">
            <div class="form-group">
			    <?= $this->Form->input('sis_funcionalidade', array('label' => false, 'multiple' => 'true', 'type' => 'select', 'options' => $optionsFuncionalidades, 'default'=>$selectedFuncionalidades, 'class' => 'chosen-select', 'required'=>'required', 'class'=>'form-control', 'div'=>['class'=>'col-sm-12 controls'])); ?>
			    <label class="control-label">Funcionalidade</label>
			</div>
		</div>
	</div>
    <div class="row" style="margin-top:15px;">
    	<div class="col-md-12">
            <div class="form-group">
			    <?= $this->Form->input('user_id', array('label' => false, 'multiple' => 'true', 'type' => 'select', 'options' => $optionsUsers, 'default' => $selectedUsers, 'class' => 'chosen-select', 'required'=>'required', 'class'=>'form-control', 'div'=>['class'=>'col-sm-12 controls'])); ?>
			    <label class="control-label">Usuários</label>
			</div>
		</div>
	</div>
</div>
<?= $this->element('forms/buttons') ?>
<?= $this->Form->end(); ?>