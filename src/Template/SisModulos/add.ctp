<?php echo $this->Form->create('SisModulos', ['class' => 'form form-validate', 'role' => 'form']); ?>
<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-plus-square"></i> Cadastrar Módulo']); ?>
<div class="card-body">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">                
                <?php echo $this->Form->input('name', ['label' => 'Nome', 'class' => 'form-control']); ?>
            </div>
        </div>
        <div class="col-md-4">
        	<?php echo $this->Form->input('active', ['type' => 'radio', 'legend' => 'Ativo', 'class' => 'radio-inline radio-styled tipo', 'options' => [1 => 'Sim', 0 => 'Não']]); ?>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-12">
            <div class="form-group">
			    <?php echo $this->Form->input('funcionalidade', ['label' => false, 'multiple' => 'true', 'type' => 'select', 'options' => $optionsFuncionalidades, 'class' => 'chosen-select', 'class'=>'form-control', 'div'=>['class'=>'col-sm-12 controls']]); ?>
			    <label class="control-label">Funcionalidade</label>
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('forms/buttons') ?>
<?php echo $this->Form->end(); ?>