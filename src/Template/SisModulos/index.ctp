<section id="AppSisModulos" <?php echo ($modal == 1)? 'style="padding:0;"' : '' ?>>
    <div class="section-body" <?php echo ($modal == 1)? 'style="margin:0;"' : '' ?>>
        <div class="card-head card-head-sm style-primary">
            <header>
                <i class="fa fa-fw fa-cog" style="vertical-align: inherit;  margin-left: -0.1em;  margin-top: -0.2em;"></i> 
                Módulos
            </header>
            <div class="tools">
                <button id="voltar" type="button" class="btn btn-default ink-reaction btn-flat" data-dismiss="modal">
                    <a href="javascript: void()">
                        <i class="fa fa-fw fa-arrow-left"></i> 
                        Voltar
                    </a>
                </button>
                <button id="cadastrarSisModulo" type="button" class="btn ink-reaction btn-default" style="float: right">
                    <i class="fa fa-plus-square-o"></i>
                    Incluir
                </button>
            </div>
        </div>
        <div class="card card-collapsed" <?php echo ($modal == 0)? 'style="margin-bottom:200px;"' : 'style="margin-bottom:0;"' ?>>
            <div id="gridSisModulos" style="padding: 24px;">                
                <h4>Total de registros: <?php echo count($dados); ?></h4> 
                <table id=""
                       class="table table-condensed table-hover" 
                       cellspacing="0" 
                       width="100%"
                       style="margin-bottom:0;">
                    <thead>
                        <tr>
                            <th style="width:40%">Nome</th>
                            <th class="text-center">Funcionalidades Associadas</th>
                            <th>Ativo</th>
                            <th style="width:50px;">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php  if(!empty($dados)){  ?>
                
                        <?php foreach ($dados as $k => $v) { ?>
                            <tr>
                                <td><?php echo $v->name; ?></td>
                                <td class="text-center"><?= $v->funcionalidadesAssociadas; ?></td>
                                <td><label class="label label-<?php echo $v->ativo_label; ?>"><?php echo $v->ativo; ?></label></td>
                                <td>
                                    <div class="dropdown btn-SisModulos">
                                        <button type="button" class="btn btn-primary btn-icon-toggle dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i></button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <li><?php echo $this->Html->link('<i class="fa fa-pencil-square-o"></i>&nbsp Editar', 'javascript: void(0)', array("escape" => false, 'id' => $v->id, 'class' => 'btnEditarSisModulos')) ?></li>
                                            <li><?php echo $this->Html->link('<i class="fa fa-trash"></i>&nbsp Excluir', 'javascript: void(0)', array("escape" => false, 'id' => $v->id, 'class' => 'btnDeletarSisModulos')) ?></li>
                                            <li style="background: #F1F1F1; font-size: 9px; text-align: center;">Atualizado em: <?php echo @$v->modified ?></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php }else{ ?>   
                            <td colspan="9">
                                    <div class="alert alert-danger">
                                        <i class="fa fa-exclamation-circle"></i> Não há registros cadastrados
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
               
            </div>
        </div>
    </div>
</section>