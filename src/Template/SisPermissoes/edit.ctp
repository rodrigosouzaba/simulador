<div rel="body" style="overflow-y:scroll;">
<?php echo $this->Form->create('Permission', array('inputDefaults' => array('div' => 'form-group','label' => array('class' => ''),'wrapInput' => '','class' => 'form-control'),'class' => 'well', 'style' => 'margin-top:10px;')); ?>
<?php echo $this->Form->hidden('id'); ?>
<fieldset>
    <?php echo $this->Form->input('name', array('label'=>'Permissão (controller.action)','placeholder' => 'Permissão')); ?>
    <?php echo $this->Form->input('description', array('label'=>'Observação da permissão','placeholder' => 'Descrição')); ?>
    <div class="row" style="margin-top: 20px;">
	    <?php echo $this->Form->input('funcionalidade', array('label'=>'Funcionalidades', 'multiple' => 'true', 'type' => 'select', 'options'=>$optionsFuncionalidades, 'selected' => $selectedFuncionalidades, 'class'=>'chosen-select','div'=>array('class'=>'col-xs-12')));?>
	</div>
</fieldset>
<?php echo $this->Form->end(); ?>
</div>
<?php echo $this->element('formsButtons') ?>