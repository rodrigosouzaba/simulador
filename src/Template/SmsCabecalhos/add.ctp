<div class="fonteReduzida">
	<?= $this->Form->create($smsCabecalho, ["id" => "form-add-cabecalho"]) ?>
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<?= $this->Form->input('nome', ["label" => false, "placeholder" => "Novo Cabeçalho"]); ?>
		</div>

	</div>
	<div class="clearfix">&nbsp;</div>
	<div class="row">
		<div class="col-xs-12 centralizada">
			<?= $this->Html->link("Salvar", "#", ["class" => "btn btn-primary btn-md", "id" => "salvarSmsCabecalho"]) ?>
			<?= $this->Html->link("Cancelar", "#", ["class" => "btn btn-default btn-md", "data-dismiss" => "modal", "id" => "fechar"]) ?>
		</div>
	</div>
	<?= $this->Form->end() ?>
</div>

<script type="text/javascript">
	$("#salvarSmsCabecalho").click(function() {
		$(".modal-title").text("Novo Cabeçalho");
		$.ajax({
			type: "POST",
			url: "<?php echo $this->request->webroot ?>sms-cabecalhos/add",
			data: $("#form-add-cabecalho").serialize(),
			success: function(data) {
				$(".close").each(function() {
					$(this).click();
				});
				$("#cabecalhos").click();
			}

		});
	});
</script>