<div class="row">
    <div class="col-xs-12">

        <?= $this->Form->create($smsCabecalho) ?>
        <fieldset>
            <?= $this->Form->input('nome', ["label" => "Cabecalho"]); ?>
        </fieldset>
        <div class="centralizada">
            <?= $this->Form->button("Salvar", ["class" => "btn btn-primary"]) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>