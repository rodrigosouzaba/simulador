<div class="col-md-12 centralizada">
    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-plus']) . ' Novo Cabeçalho', "#", ['class' => 'btn btn-default', "id" => "addSmsCabecalho", "data-target" => "#modal-add-sms", "data-toggle" => "modal", 'escape' => false]) ?>
</div>
<table cellpadding="0" cellspacing="0" class="table table-condensed">
    <thead>
        <tr>
            <!--                 <th><?= $this->Paginator->sort('id') ?></th> -->
            <th><?= $this->Paginator->sort('Cabecalho') ?></th>
            <th class="actions">Ações</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($smsCabecalhos as $smsCabecalho) : ?>
            <tr>
                <!--                 <td><?= $this->Number->format($smsCabecalho->id) ?></td> -->
                <td><?= h($smsCabecalho->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']), "#", ['class' => 'btn btn-sm btn-primary editar', 'escape' => false, 'value' => $smsCabecalho->id, 'title' => 'Editar', "data-target" => "#modal-add-sms", "data-toggle" => "modal"]) ?>
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-trash']), "#", ['class' => 'btn btn-sm btn-danger excluir', 'escape' => false, 'value' => $smsCabecalho->id, 'title' => 'Excluir']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
</div>

<script type="text/javascript">
    $("#addSmsCabecalho").click(function() {
        $(".modal-title").text("Novo Cabeçalho");
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-cabecalhos/add",
            beforeSend: function() {
                $("#modalResposta").empty();
            },
            success: function(data) {
                $("#modalResposta").append(data);
            }
        });
    });

    $(".excluir").click(function() {
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot . $this->request->controller ?>/delete/" + $(this).attr("value"),
            success: function(data) {
                alert("Excluído com sucesso.");
                $("#cabecalhos").click();
            }
        });
    });
    $(".editar").click(function() {
        $(".modal-title").text("Editar Cabeçalho");
        id = $(this).attr("value");
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-cabecalhos/edit/" + id,
            beforeSend: function() {
                $("#modalResposta").empty();
            },
            success: function(data) {
                $("#modalResposta").append(data);
            }
        });
    });
</script>
