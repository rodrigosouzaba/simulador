<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sms Cabecalho'), ['action' => 'edit', $smsCabecalho->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sms Cabecalho'), ['action' => 'delete', $smsCabecalho->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsCabecalho->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sms Cabecalhos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Cabecalho'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="smsCabecalhos view large-9 medium-8 columns content">
    <h3><?= h($smsCabecalho->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($smsCabecalho->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($smsCabecalho->id) ?></td>
        </tr>
    </table>
</div>
