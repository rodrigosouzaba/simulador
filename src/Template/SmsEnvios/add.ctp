<?= $this->Form->create($smsEnvio, ["id" => "nova-campanha"]) ?>
<div class="row fonteReduzida">
	<div class="col-xs-12 col-md-6">
		<?= $this->Form->hidden("user_id", ["value" => $sessao["id"]]); ?>
		<?= $this->Form->input('sms_cabecalho_id', ['options' => $smsCabecalhos, 'label' => false, 'empty' => 'SELECIONE O CABEÇALHO DA MENSAGEM']); ?>
	</div>
	<div class="col-xs-12 col-md-6">
		<?= $this->Form->input('sms_lista_id', ['options' => $grupos, 'label' => false, 'empty' => 'SELECIONE A LISTA DE DESTINATÁRIOS DA MENSAGEM']);
		?>
	</div>
</div>
<div class="clearfix">&nbsp;</div>
<div class="row">
	<div class="col-xs-12 col-md-6">
		<?= $this->Form->input("descricao", ["label" => false, "placeholder" => "Descrição da Campanha"]); ?>
	</div>
	<div class="col-xs-12 col-md-6">
		<?= $this->Form->input("data_agendamento", ["label" => false]); ?>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<?= $this->Form->input('mensagem'); ?>
		<small class="caracteres"></small>
	</div>
</div>
<div class="clearfix">&nbsp;</div>
<div class="row">
	<div class="col-xs-12 centralizada">
		<?= $this->Html->link("Salvar", "#", ["class" => "btn btn-primary btn-md", "id" => "salvar-campanha"]) ?>
		<?= $this->Html->link("Cancelar", "#", ["class" => "btn btn-default btn-md", "data-dismiss" => "modal", "id" => "fechar-nova-campanha"]) ?>
	</div>
</div>
<?= $this->Form->end() ?>
<script type="text/javascript">
	$("#sms-cabecalho-id").change(function() {
		$('#mensagem').append($("#sms-cabecalho-id option:selected").text() + " ");
	});

	$("#salvar-campanha").click(function() {
		$.ajax({
			type: "POST",
			url: "<?php echo $this->request->webroot . $this->request->controller ?>/add",
			data: $("#nova-campanha").serialize(),
			success: function(data) {
				alert("Salvo com sucesso");
				$("#fechar-nova-campanha").click();
				$("#campanhas").click();
			}
		});
	});

	$(document).on("input", "#mensagem", function() {
		var limite = 160;
		var informativo = "caracteres restantes.";
		var caracteresDigitados = $(this).val().length;
		var caracteresRestantes = limite - caracteresDigitados;

		if (caracteresRestantes <= 0) {
			var comentario = $("textarea[name=mensagem]").val();
			$("textarea[name=mensagem]").val(comentario.substr(0, limite));
			$(".caracteres").text("0 " + informativo);
		} else {
			$(".caracteres").text(caracteresRestantes + " " + informativo);
		}
	});
</script>