<div class="col-xs-12">
    <?= $this->Form->create($smsEnvio) ?>
    <fieldset>

        <div class="col-md-6">
            <?= $this->Form->input('descricao', ['class' => 'col-md-6']); ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('sms_lista_id', ['options' => $grupos]); ?>
        </div>
        <div class="col-md-12">
            <?= $this->Form->input('data_agendamento', ['empty' => true]); ?>
        </div>
        <div class="col-md-12">
            <?= $this->Form->input('mensagem'); ?>
        </div>

    </fieldset>
    <div class="clearfix">&nbsp</div>
    <?= $this->Form->button('Salvar', ['class' => 'btn btn-info col-md-2 col-md-offset-5']) ?>
    <?= $this->Form->end() ?>
</div>