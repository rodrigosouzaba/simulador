<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Envio[]|\Cake\Collection\CollectionInterface $envios
 */

$hoje = Cake\I18n\Time::now();
$top_admin = [1, 7, 1606];
?>
<div class="col-md-12 centralizada">
    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-plus']) . ' Nova Campanha', "#", ['class' => 'btn btn-default', "id" => "addSmsEnvio", "data-target" => "#modal-add-sms", "data-toggle" => "modal", 'escape' => false]) ?>
</div>
<h4 class="left">Pendentes</h4>


<table cellpadding="0" cellspacing="0" class="table table-condensed">
    <thead>
        <tr>
            <th style="width: 10%;"><?= $this->Paginator->sort('created', ['label' => 'Criada em']) ?></th>
            <th style="width: 15%"><?= $this->Paginator->sort('descricao', ['label' => 'Descrição']) ?></th>
            <th style="width: 15%"><?= $this->Paginator->sort('lista_id') ?></th>
            <th style="width: 10%;"><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
            <th style="width: 10%;"><?= $this->Paginator->sort('data_agendamento') ?></th>
            <?php if ($sessao['role'] === 'admin') { ?>
                <th class="actions" style="width: 20%;">Ações</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($envios as $envio) :
        ?>
            <tr>
                <td><?= h($envio->created) ?></td>
                <td><?= $envio->descricao ?></td>
                <td><?= $envio->sis_grupo->name ?></td>
                <td><?= $envio->sms_situaco->nome ?></td>
                <td><?= $envio->data_agendamento ?></td>
                <?php
                if ($sessao['role'] === 'admin') {
                ?>
                    <td class="actions">

                        <?php
                        if (in_array($sessao['id'], $top_admin)) {

                            if ($envio->autorizado <> 1) {
                                echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-check', 'aria-hidden' => 'true']), "#", ['title' => 'Autorizar Envio Pendente', 'class' => 'btn btn-sm btn-danger autorizar', 'role' => 'button', 'escape' => false, 'type' => 'button', "campanha" => $envio->id]);
                            } else {
                                echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-check', 'aria-hidden' => 'true']), "#", ['title' => 'Envio Autorizado', 'class' => 'btn btn-sm btn-success', 'role' => 'button', 'escape' => false, 'type' => 'button', "disabled" => "disabled"]);
                            }
                        }
                        ?>


                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-eye', 'aria-hidden' => 'true']), '#', ['title' => 'Detalhes', 'class' => 'btn btn-sm btn-primary view', 'role' => 'button', 'escape' => false, 'type' => 'button', 'campanha_id' => $envio->id]) ?>

                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-edit', 'aria-hidden' => 'true']), "#", ['title' => 'Editar', 'class' => 'btn btn-sm btn-primary editar', 'role' => 'button', 'escape' => false, 'type' => 'button', 'campanha_id' => $envio->id]); ?>

                        <?php if ($envio->situacao_id != 1) { ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']), "#", ['title' => 'Excluir', 'class' => 'btn btn-sm btn-danger excluir', 'role' => 'button', 'escape' => false, 'type' => 'button', 'value' =>  $envio->id]); ?>
                        <?php }
                        ?>

                    </td>
                <?php } ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="divider"> &nbsp;</div>
<h4>Autorizadas</h4>
<table cellpadding="0" cellspacing="0" class="table table-condensed">
    <thead>
        <tr>
            <th style="width: 15%"><?= $this->Paginator->sort('descricao', ['label' => 'Descrição']) ?></th>
            <th style="width: 15%"><?= $this->Paginator->sort('lista_id') ?></th>
            <th style="width: 15%"><?= $this->Paginator->sort('tipo_envio') ?></th>
            <th style="width: 15%"><?= $this->Paginator->sort('Status') ?></th>
            <th style="width: 10%;"><?= $this->Paginator->sort('created', ['label' => 'Criada em']) ?></th>
            <th style="width: 10%;"><?= $this->Paginator->sort('data_agendamento') ?></th>
            <th style="width: 10%;"><?= $this->Paginator->sort('data_envio', ['label' => 'Data de Autorização']) ?></th>
            <?php if ($sessao['role'] === 'admin') { ?>
                <th class="actions" style="width: 20%;">Ações</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($envios_finalizados as $envio) : ?>
            <tr>
                <td style=""><?= $envio->descricao ?></td>
                <td><?= $envio->sis_grupo->name ?></td>
                <td style=""><?= $envio->tipo_envio ?></td>
                <?php if ($envio->sms_situacao_id == 1 && $hoje > $envio->data_agendamento) : ?>
                    <td class="text-success"><strong>Enviado</strong></td>
                <?php elseif ($envio->sms_situacao_id == 2 || $hoje < $envio->data_agendamento) : ?>
                    <td class="text-warning"><strong>Em espera</strong></td>
                <?php else : ?>
                    <td class="text-danger"><strong>Cancelado</strong></td>
                <?php endif; ?>
                <td style=""><?= h($envio->created) ?></td>
                <td style=""><?= $envio->data_agendamento ?></td>
                <td style=""><?= $envio->data_envio ?></td>
                <?php if (in_array($sessao['id'], $top_admin) && $envio->sms_situacao_id == 2 || $hoje < $envio->data_agendamento) { ?>
                    <td style=""><?= $this->Form->button('Cancelar', ['class' => 'btn btn-danger btn-sm cancelar', 'value' => $envio->id]) ?></td>
                <?php } ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<!--MODAL DE ESCOLHA DO TIPO DE ENVIO-->
<style>
    #modal-tipo-envio .modal-dialog .modal-content .modal-header {
        padding: 2px 5px;
        border: 0px;
    }

    #modal-tipo-envio .modal-dialog .modal-content .modal-footer {
        border: 0px;
    }

    #body-campanhas {
        padding: 0px;
        padding-top: 15px;
    }

    #aguarde {
        display: none;
    }
</style>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-tipo-envio">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="centralizada">
                    <p>Tipo de Envio</p>
                    <?= $this->Form->hidden('id', ['id' => 'id-campanha']) ?>
                    <?= $this->Form->hidden('id', ['id' => 'tipo-envio-campanha']) ?>
                    <?= $this->Form->button('Zenvia', ['id' => 'send-zenvia', 'class' => 'btn btn-info col-xs-2 col-xs-offset-3']) ?>
                    <?= $this->Form->button('Modem', ['id' => 'send-modem', 'class' => 'btn btn-primary col-xs-2 col-xs-offset-2']) ?>
                    <div class="col-xs-12 centralizada" id="aguarde">
                        <small class="text-danger">Aguarde...</small><?= $this->Html->image("ciranda.gif", ['style' => ['height: 20px']]) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal-campanhas">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" id="title-campanhas"></div>
            </div>
            <div class="modal-body col-xs-12" id="body-campanhas"></div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $("#addSmsEnvio").click(function() {
        $(".modal-title").text("Nova Campanha");
        $("#modalResposta").empty();
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-envios/add",
            success: function(data) {
                $("#modalResposta").append(data);
            }
        });
    });
    $("#listas").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-listas/index",
            success: function(data) {
                $("#resposta").empty();
                $("#resposta").append(data);
            }
        });
    });
    $("#situacoes").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-situacoes/index",
            success: function(data) {
                $("#resposta").empty();
                $("#resposta").append(data);
            }
        });
    });
    $("#cabecalhos").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-cabecalhos/index",
            success: function(data) {
                $("#resposta").empty();
                $("#resposta").append(data);
            }
        });
    });

    $(".excluir").click(function() {
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot . $this->request->controller ?>/delete/" + $(this).attr("value"),
            success: function(data) {
                $("#campanhas").click();
                alert("Excluído com sucesso.");
            }
        });
    });

    $(".autorizar").click(function() {
        let id = $(this).attr("campanha")
        $("#modal-tipo-envio").modal("show");
        $("#id-campanha").val(id);

    });

    $("#send-zenvia").click(function() {
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot . $this->request->controller ?>/autorizar/",
            data: {
                id: $("#id-campanha").val(),
                tipo_envio: "zenvia"
            },
            beforeSend: function() {
                $("#aguarde").show();
            },
            success: function(data) {
                $.ajax({
                    type: "post",
                    url: "<?php echo $this->request->webroot . $this->request->controller ?>/zenvia/" + $("#id-campanha").val(),
                    success: function(data) {
                        $("#modal-tipo-envio").modal("hide");
                        $("#campanhas").click();
                        /* $("#modalAviso").modal("show");
                         * $("#conteudoAviso").html("<span class='text-danger'>Envio Autorizado com sucesso!</span>");
                         */
                    }
                })
            }
        });
    });

    $("#send-modem").click(function() {
        $.ajax({
            type: "post",
            data: {
                id: $("#id-campanha").val(),
                tipo_envio: "modem"
            },
            url: "<?php echo $this->request->webroot . $this->request->controller ?>/autorizar/" + $(this).attr("id"),
            beforeSend: function() {
                $("#aguarde").show();
            },
            success: function(data) {
                $("#modal-tipo-envio").modal("hide");
                $("#campanhas").click();
                $("#modalAviso").modal("show");
                $("#conteudoAviso").html("<span class='text-danger'>Envio Autorizado com sucesso!</span>");
            }
        });
    });

    $(".cancelar").click(function() {
        $(this).html("Cancelando <img src='<?= $this->request->webroot ?>img/ciranda.gif' style='height: 16px;'>");
        $(this).attr('disabled', true);
        let id = $(this).val();
        $.ajax({
            type: "post",
            data: {
                id: id
            },
            url: "<?= $this->request->webroot . $this->request->controller ?>/cancelaEnvio/",
            success: function() {
                $("#campanhas").click();
            }
        });
    });

    $(".editar").click(function() {
        let id = $(this).attr("campanha_id");
        $("#title-campanhas").empty();
        $("#body-campanhas").empty();
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-envios/edit/" + id,
            success: function(data) {
                $("#title-campanhas").append("Editar Campanha");
                $("#body-campanhas").append(data);
                $("#modal-campanhas").modal("show");
            }
        });
    });

    $(".view").click(function() {
        let id = $(this).attr("campanha_id");
        $("#title-campanhas").empty();
        $("#body-campanhas").empty();
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-envios/view/" + id,
            success: function(data) {
                $("#title-campanhas").append("Detalhes da Campanha");
                $("#body-campanhas").append(data);
                $("#modal-campanhas").modal("show");
            }
        });
    });
</script>