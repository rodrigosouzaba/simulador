<style>
    .caixa {
        background-color: #f6f3f1;
    }
</style>
<div class="col-xs-12">
    <table class="vertical-table col-xs-12">
        <tr>
            <th><?= __('Lista') ?></th>
            <td><?= $smsEnvio->has('sms_lista') ? $this->Html->link($smsEnvio['sms_lista']['descricao'], ['controller' => 'SmsListas', 'action' => 'view', $smsEnvio->sms_lista->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Usuário') ?></th>
            <td><?= $smsEnvio->has('user') ? $this->Html->link($smsEnvio->user->nome, ['controller' => 'Users', 'action' => 'view', $smsEnvio->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situação') ?></th>
            <td><?= $smsEnvio->has('sms_situaco') ? $this->Html->link($smsEnvio->sms_situaco->nome, ['controller' => 'SmsSituacoes', 'action' => 'view', $smsEnvio->sms_situaco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Descrição') ?></th>
            <td><?= h($smsEnvio->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Autorizado') ?></th>
            <td><?= $autorizado = $this->Number->format($smsEnvio->autorizado) ? "Sim" : "Não" ?></td>
        </tr>
        <tr>
            <th><?= __('Criada em') ?></th>
            <td><?= h($smsEnvio->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Data Agendamento') ?></th>
            <td><?= h($smsEnvio->data_agendamento) ?></td>
        </tr>
    </table>
    <div class="col-xs-12 caixa">
        <h4><?= __('Mensagem') ?></h4>
        <?= $this->Text->autoParagraph(h($smsEnvio->mensagem)); ?>
    </div>
</div>