<style>
	#nome {
		display: none;
	}
</style>
<div class="fonteReduzida">
	<div class="centralizada">
		<h4>Selecione o arquivo e clique no botão enviar.</h4>
		<h6 style="line-height: 20px;">Arquivos TXT com estrutura de dados da seguinte forma: "CELULAR"; "APELIDO"; "DATA DE NASCIMENTO" <br />
			Exemplo: <b>"71999999999"; "MARIA"; "1985-11-01"</b></h6>
	</div>
	<?= $this->Form->create($smsLista, ['type' => 'file']) ?>
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<?= $this->Form->button("Selecione a lista", ['id' => 'btn-lista', 'class' => 'btn btn-default col-xs-12']) ?>
			<?= $this->Form->input('nome', ["type" => "file", "label" => false, "placeholder" => "Nome da Lista"]); ?>
		</div>
		<div class="col-xs-12 col-md-6">
			<?= $this->Form->input('descricao', ["label" => false, "placeholder" => "Descrição da Lista"]); ?>
		</div>
	</div>
	<div class="clearfix">&nbsp;</div>
	<div class="row">
		<div class="col-xs-12 centralizada">
			<?= $this->Form->button("Salvar", ["class" => "btn btn-primary btn-md"]) ?>
			<?= $this->Html->link("Cancelar", "#", ["class" => "btn btn-default btn-md", "data-dismiss" => "modal"]) ?>
		</div>
	</div>
	<?= $this->Form->end() ?>
</div>
<script>
	$("#btn-lista").click(function(evt) {
		evt.preventDefault();
		$("#nome").click();
	});
	$('input[type="file"]').change(function() {
		$("#btn-lista").empty();
		$("#btn-lista").append($(this).val().substring(12));
	});
</script>