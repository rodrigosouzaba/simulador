<div class="centralizada">
    <h4>Selecione o arquivo e clique no botão enviar.</h4>
    <h6 style="line-height: 20px;">Arquivos TXT com estrutura de dados da seguinte forma: "CELULAR"; "APELIDO"; "DATA DE NASCIMENTO" <br />
        Exemplo: <b>"71999999999"; "MARIA"; "1985-11-01"</b></h6>
    <?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>
    <?php echo $this->Form->input('lista_id', ['type' => 'hidden', 'value' => $lista_id]); ?>
    <div class="upload-frm">
        <div class="col-xs-6">
            <?php echo $this->Form->input('file', ['type' => 'file', 'label' => '', 'class' => 'centralizada']); ?>
        </div>
        <div class="col-xs-6">
            <?php echo $this->Form->input('descricao', ['placeholder' => 'Descrição', 'label' => '', 'class' => 'centralizada']); ?>
        </div>
    </div>
    <div class="upload-frm">
        <?php echo $this->Form->button(__('Editar'), ['type' => 'submit', 'class' => 'btn btn-md btn-primary', "id" => 'enviar']); ?>
    </div>
    <?php echo $this->Form->end(); ?>

</div>