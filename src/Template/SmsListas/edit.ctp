<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $smsLista->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $smsLista->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sms Listas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Envios'), ['controller' => 'Envios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Envio'), ['controller' => 'Envios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sms Envios'), ['controller' => 'SmsEnvios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sms Envio'), ['controller' => 'SmsEnvios', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="smsListas form large-9 medium-8 columns content">
    <?= $this->Form->create($smsLista) ?>
    <fieldset>
        <legend><?= __('Edit Sms Lista') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('descricao');
            echo $this->Form->input('caminho');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
