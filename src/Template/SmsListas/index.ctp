<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Lista[]|\Cake\Collection\CollectionInterface $listas
 */
?>
<div class="col-md-12 centralizada">
    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-plus']) . ' Nova Lista', "#", ['class' => 'btn btn-default', "id" => "addSmsLista", "data-target" => "#modal-add-sms", "data-toggle" => "modal", 'escape' => false]) ?>
</div>
<table cellpadding="0" cellspacing="0" class="table table-condensed">
    <thead>
        <tr>
            <!--<th scope="col"><?= $this->Paginator->sort('id') ?></th>-->
            <th scope="col"><?= $this->Paginator->sort('descricao', ['labbel' => 'Descrição']) ?></th>
            <th scope="col"><?= $this->Paginator->sort('Arquivo') ?></th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($smsListas as $lista) : ?>
            <tr>
                <!--<td><?= $this->Number->format($lista->id) ?></td>-->
                <td><?= h($lista->descricao) ?></td>
                <td><?= h($lista->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']), "#", ['escape' => false, 'class' => 'arquivo btn btn-primary btn-sm', 'value' => $lista->id, "data-target" => "#modal-add-sms", "data-toggle" => "modal",  'title' => 'Editar Lista']) ?>
                    <!--                     <?= $this->Html->link(__('Edit'), "#", ['class' => 'edit', 'value' => $lista->id, "data-target" => "#modal-add-sms", "data-toggle" => "modal"]) ?> -->
                    <!--                     <?= $this->Html->link(__('Sync Mailchimp'), ['action' => 'syncmailchimp', $lista->id]) ?> -->
                    <!--   <?= $this->Html->link(__('View'), ['action' => 'view', $lista->id]) ?>
                                                                                       <?= $this->Html->link(__('Edit'), ['action' => 'edit', $lista->id]) ?>
                                                                                    -->
                    <?= $this->Form->postLink($this->Html->tag('i', '', ['class' => 'fa fa-trash']), ['action' => 'delete', $lista->id], ['class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete # {0}?', $lista->id), 'escape' => false, 'title' => 'Exluir Lista']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
</div>

<script type="text/javascript">
    $("#addSmsLista").click(function() {
        $(".modal-title").text("Nova Lista");
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-listas/add",
            beforeSend() {
                $("#modalResposta").empty();
            },
            success: function(data) {
                $("#modalResposta").append(data);
            }
        });
    });

    $(".edit").click(function() {
        $(".modal-title").text("Editar Lista");
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-listas/edit/" + $(this).attr("value"),
            beforeSend() {
                $("#modalResposta").empty();
            },
            success: function(data) {
                $("#modalResposta").append(data);
            }
        });
    });

    $(".arquivo").click(function() {
        $(".modal-title").text("Editar Lista");
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-listas/add-arquivo/" + $(this).attr("value"),
            beforeSend() {
                $("#modalResposta").empty();
            },
            success: function(data) {
                $("#modalResposta").append(data);
            }
        });
    });
</script>
