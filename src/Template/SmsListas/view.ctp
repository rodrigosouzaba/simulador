<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sms Lista'), ['action' => 'edit', $smsLista->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sms Lista'), ['action' => 'delete', $smsLista->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsLista->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sms Listas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Lista'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Envios'), ['controller' => 'Envios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Envio'), ['controller' => 'Envios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sms Envios'), ['controller' => 'SmsEnvios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Envio'), ['controller' => 'SmsEnvios', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="smsListas view large-9 medium-8 columns content">
    <h3><?= h($smsLista->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($smsLista->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($smsLista->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Caminho') ?></th>
            <td><?= h($smsLista->caminho) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($smsLista->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($smsLista->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Envios') ?></h4>
        <?php if (!empty($smsLista->envios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Mensagem') ?></th>
                <th><?= __('Sms Lista Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Sms Situacao Id') ?></th>
                <th><?= __('Data Envio') ?></th>
                <th><?= __('Data Agendamento') ?></th>
                <th><?= __('Autorizado') ?></th>
                <th><?= __('Descricao') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($smsLista->envios as $envios): ?>
            <tr>
                <td><?= h($envios->id) ?></td>
                <td><?= h($envios->created) ?></td>
                <td><?= h($envios->mensagem) ?></td>
                <td><?= h($envios->sms_lista_id) ?></td>
                <td><?= h($envios->user_id) ?></td>
                <td><?= h($envios->sms_situacao_id) ?></td>
                <td><?= h($envios->data_envio) ?></td>
                <td><?= h($envios->data_agendamento) ?></td>
                <td><?= h($envios->autorizado) ?></td>
                <td><?= h($envios->descricao) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Envios', 'action' => 'view', $envios->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Envios', 'action' => 'edit', $envios->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Envios', 'action' => 'delete', $envios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $envios->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sms Envios') ?></h4>
        <?php if (!empty($smsLista->sms_envios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Mensagem') ?></th>
                <th><?= __('Sms Lista Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Sms Situacao Id') ?></th>
                <th><?= __('Data Envio') ?></th>
                <th><?= __('Data Agendamento') ?></th>
                <th><?= __('Autorizado') ?></th>
                <th><?= __('Descricao') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($smsLista->sms_envios as $smsEnvios): ?>
            <tr>
                <td><?= h($smsEnvios->id) ?></td>
                <td><?= h($smsEnvios->created) ?></td>
                <td><?= h($smsEnvios->mensagem) ?></td>
                <td><?= h($smsEnvios->sms_lista_id) ?></td>
                <td><?= h($smsEnvios->user_id) ?></td>
                <td><?= h($smsEnvios->sms_situacao_id) ?></td>
                <td><?= h($smsEnvios->data_envio) ?></td>
                <td><?= h($smsEnvios->data_agendamento) ?></td>
                <td><?= h($smsEnvios->autorizado) ?></td>
                <td><?= h($smsEnvios->descricao) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SmsEnvios', 'action' => 'view', $smsEnvios->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SmsEnvios', 'action' => 'edit', $smsEnvios->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SmsEnvios', 'action' => 'delete', $smsEnvios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsEnvios->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
