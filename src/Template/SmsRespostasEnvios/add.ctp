<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sms Respostas Envios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sms Envios'), ['controller' => 'SmsEnvios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sms Envio'), ['controller' => 'SmsEnvios', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="smsRespostasEnvios form large-9 medium-8 columns content">
    <?= $this->Form->create($smsRespostasEnvio) ?>
    <fieldset>
        <legend><?= __('Add Sms Respostas Envio') ?></legend>
        <?php
            echo $this->Form->input('sms_envio_id', ['options' => $smsEnvios, 'empty' => true]);
            echo $this->Form->input('celular');
            echo $this->Form->input('data', ['empty' => true]);
            echo $this->Form->input('resultado');
            echo $this->Form->input('porta_chipeira');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
