<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sms Respostas Envio'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sms Envios'), ['controller' => 'SmsEnvios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sms Envio'), ['controller' => 'SmsEnvios', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="smsRespostasEnvios index large-9 medium-8 columns content">
    <h3><?= __('Sms Respostas Envios') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('sms_envio_id') ?></th>
                <th><?= $this->Paginator->sort('celular') ?></th>
                <th><?= $this->Paginator->sort('data') ?></th>
                <th><?= $this->Paginator->sort('resultado') ?></th>
                <th><?= $this->Paginator->sort('porta_chipeira') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($smsRespostasEnvios as $smsRespostasEnvio): ?>
            <tr>
                <td><?= $this->Number->format($smsRespostasEnvio->id) ?></td>
                <td><?= $smsRespostasEnvio->has('sms_envio') ? $this->Html->link($smsRespostasEnvio->sms_envio->id, ['controller' => 'SmsEnvios', 'action' => 'view', $smsRespostasEnvio->sms_envio->id]) : '' ?></td>
                <td><?= h($smsRespostasEnvio->celular) ?></td>
                <td><?= h($smsRespostasEnvio->data) ?></td>
                <td><?= h($smsRespostasEnvio->resultado) ?></td>
                <td><?= h($smsRespostasEnvio->porta_chipeira) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $smsRespostasEnvio->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $smsRespostasEnvio->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $smsRespostasEnvio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsRespostasEnvio->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
