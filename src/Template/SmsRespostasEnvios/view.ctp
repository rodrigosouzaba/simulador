<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sms Respostas Envio'), ['action' => 'edit', $smsRespostasEnvio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sms Respostas Envio'), ['action' => 'delete', $smsRespostasEnvio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsRespostasEnvio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sms Respostas Envios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Respostas Envio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sms Envios'), ['controller' => 'SmsEnvios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sms Envio'), ['controller' => 'SmsEnvios', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="smsRespostasEnvios view large-9 medium-8 columns content">
    <h3><?= h($smsRespostasEnvio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Sms Envio') ?></th>
            <td><?= $smsRespostasEnvio->has('sms_envio') ? $this->Html->link($smsRespostasEnvio->sms_envio->id, ['controller' => 'SmsEnvios', 'action' => 'view', $smsRespostasEnvio->sms_envio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Celular') ?></th>
            <td><?= h($smsRespostasEnvio->celular) ?></td>
        </tr>
        <tr>
            <th><?= __('Resultado') ?></th>
            <td><?= h($smsRespostasEnvio->resultado) ?></td>
        </tr>
        <tr>
            <th><?= __('Porta Chipeira') ?></th>
            <td><?= h($smsRespostasEnvio->porta_chipeira) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($smsRespostasEnvio->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Data') ?></th>
            <td><?= h($smsRespostasEnvio->data) ?></td>
        </tr>
    </table>
</div>
