<div class="fonteReduzida">
    <?= $this->Form->create($smsSituaco,["id" => "form-add-situacao"]) ?>
    <div class="row">
	    <div class="col-xs-12 col-md-12">
		    <?= $this->Form->input('nome', ["label" => false, "placeholder" => "Nova Situação"]); ?>
	    </div>

    </div>
	<div class="clearfix">&nbsp;</div>
    <div class="row">
	    <div class="col-xs-12 centralizada">
		    <?= $this->Html->link("Salvar", "#",["class" => "btn btn-primary btn-md", "id" => "add-situacao"]) ?>
		    <?= $this->Html->link("Cancelar", "#", ["class" => "btn btn-default btn-md", "data-dismiss" => "modal", "id" => "fechar"]) ?>
	    </div>
    </div>
        <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
	$("#add-situacao").click(function () {
	    $.ajax({
	        type: "post",
	        url: "<?php echo $this->request->webroot ?>sms-situacoes/add",
            data: $("#form-add-situacao").serialize(),
	        success: function (data) {
	            $("#fechar").click();
	            alert("Salvo com sucesso.");
	            $("#situacoes").click();
	        }
	    });
	});
</script>