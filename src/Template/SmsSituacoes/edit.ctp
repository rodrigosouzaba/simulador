<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $smsSituaco->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $smsSituaco->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sms Situacoes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="smsSituacoes form large-9 medium-8 columns content">
    <?= $this->Form->create($smsSituaco) ?>
    <fieldset>
        <legend><?= __('Edit Sms Situaco') ?></legend>
        <?php
            echo $this->Form->input('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
