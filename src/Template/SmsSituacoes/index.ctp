<?= $this->Html->link('Nova Situação', "#", ['class' => 'btn btn-danger right', "id" => "addSmsSituacao","data-target" => "#modal-add-sms", "data-toggle" => "modal"]) ?>
 <table cellpadding="0" cellspacing="0" class="table table-condensed">
        <thead>
            <tr>
<!--                 <th><?= $this->Paginator->sort('id') ?></th> -->
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($smsSituacoes as $smsSituaco): ?>
            <tr>
<!--                 <td><?= $this->Number->format($smsSituaco->id) ?></td> -->
                <td><?= h($smsSituaco->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $smsSituaco->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $smsSituaco->id]) ?>
                    <?= $this->Html->link("Excluir", "#",['value' => $smsSituaco->id, "class" => "excluir"]) ?>
<!--                     <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $smsSituaco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsSituaco->id)]) ?> -->
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
    </div>
<script type="text/javascript">
	$("#addSmsSituacao").click(function () {
		$(".modal-title").text("Nova Situação");
	    $.ajax({
	        type: "get",
	        url: "<?php echo $this->request->webroot ?>sms-situacoes/add",
	        success: function (data) {
	            $("#modalResposta").empty();
	            $("#modalResposta").append(data);
	        }
	    });
	});

	$(".excluir").click(function () {
	    $.ajax({
	        type: "post",
	        url: "<?php echo $this->request->webroot ?>sms-situacoes/delete/"+$(this).attr("value"),
	        success: function (data) {
	            alert("Excluído com sucesso.");
	            $("#situacoes").click();
	        }
	    });

	});
</script>