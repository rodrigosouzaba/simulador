<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $smsenviado->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $smsenviado->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Smsenviados'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="smsenviados form large-9 medium-8 columns content">
    <?= $this->Form->create($smsenviado) ?>
    <fieldset>
        <legend><?= __('Edit Smsenviado') ?></legend>
        <?php
            echo $this->Form->input('destinatario');
            echo $this->Form->input('nome');
            echo $this->Form->input('cod_resultado');
            echo $this->Form->input('resultado');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
