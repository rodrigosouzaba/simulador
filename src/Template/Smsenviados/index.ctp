
<div class="col-md-12 col-sm-12 col-xs-12">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Usuário</th>
                <th><?= $this->Paginator->sort('created', ['label' => 'Data Envio']) ?></th>
                <th style="width: 30% !important"><?= $this->Paginator->sort('mensagem') ?></th>
                <th><?= $this->Paginator->sort('resultado') ?></th>
<!--                 <th class="actions"><?= __('Actions') ?></th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($smsenviados as $smsenviado): ?>
                <tr>                 
                    <td>
	                    <?= h($smsenviado->nome." ".$smsenviado->sobrenome) ?>
	                    <br>
	                    <small>
	                    	<?= $smsenviado->username ?><br>
	                    	<?= $smsenviado->destinatario_enviado; ?><br>
	                    	<?= $smsenviado->estado ?><br>
	                    	<?= $smsenviado->email ?><br>
	                    </small>
	                </td>
                    <td><?= h($smsenviado->created) ?></td>
                    <td><?= $smsenviado->mensagem ?></td>
                    <td>
                        <?php
                        if ($smsenviado->cod_resultado === '00') {
                            $class = 'label label-success';
                        } else {

                            $class = 'label label-danger';
                        }
                        ?>
                        <span class="<?= $class ?>">
                        <?php switch($smsenviado->cod_resultado){
	                        case "00":
	                        	$resultado = "SMS Enviado";
	                        	break;
	                        case "10":
	                        if($smsenviado->resultado == "Field 'from' invalid"){
		                        $resultado = "Caracteres excedidos";
	                        }elseif ($smsenviado->resultado == "Incorrect or incomplete 'to' mobile number"){
		                        $resultado = "Destinatário inválido";
	                        }else{
		                        $resultado = $smsenviado->resultado;
	                        }
		                        break;
		                    default:
		                    $resultado = $smsenviado->resultado;
		                    break;
                        }?>
                            <?= $smsenviado->cod_resultado . " - " . $resultado ?>

                        </span>
                    </td>
<!--
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $smsenviado->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $smsenviado->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $smsenviado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsenviado->id)]) ?>
                    </td>
-->
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<script type="text/javascript">
	$(".celular").mask("(99) 9 9999-9999");
	</script>