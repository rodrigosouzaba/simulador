<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Smsenviado'), ['action' => 'edit', $smsenviado->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Smsenviado'), ['action' => 'delete', $smsenviado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $smsenviado->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Smsenviados'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Smsenviado'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="smsenviados view large-9 medium-8 columns content">
    <h3><?= h($smsenviado->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Destinatario') ?></th>
            <td><?= h($smsenviado->destinatario) ?></td>
        </tr>
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($smsenviado->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Cod Resultado') ?></th>
            <td><?= h($smsenviado->cod_resultado) ?></td>
        </tr>
        <tr>
            <th><?= __('Resultado') ?></th>
            <td><?= h($smsenviado->resultado) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($smsenviado->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($smsenviado->created) ?></td>
        </tr>
    </table>
</div>
