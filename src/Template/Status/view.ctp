<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Status'), ['action' => 'edit', $status->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Status'), ['action' => 'delete', $status->id], ['confirm' => __('Are you sure you want to delete # {0}?', $status->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Simulacoes'), ['controller' => 'Simulacoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Simulaco'), ['controller' => 'Simulacoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="status view large-9 medium-8 columns content">
    <h3><?= h($status->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($status->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($status->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Simulacoes') ?></h4>
        <?php if (!empty($status->simulacoes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Contato') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Faixa1') ?></th>
                <th><?= __('Faixa2') ?></th>
                <th><?= __('Faixa3') ?></th>
                <th><?= __('Faixa4') ?></th>
                <th><?= __('Faixa5') ?></th>
                <th><?= __('Faixa6') ?></th>
                <th><?= __('Faixa7') ?></th>
                <th><?= __('Faixa8') ?></th>
                <th><?= __('Faixa9') ?></th>
                <th><?= __('Faixa10') ?></th>
                <th><?= __('Faixa11') ?></th>
                <th><?= __('Faixa12') ?></th>
                <th><?= __('Data') ?></th>
                <th><?= __('Telefone') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Calculoremoto') ?></th>
                <th><?= __('Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($status->simulacoes as $simulacoes): ?>
            <tr>
                <td><?= h($simulacoes->id) ?></td>
                <td><?= h($simulacoes->nome) ?></td>
                <td><?= h($simulacoes->contato) ?></td>
                <td><?= h($simulacoes->email) ?></td>
                <td><?= h($simulacoes->faixa1) ?></td>
                <td><?= h($simulacoes->faixa2) ?></td>
                <td><?= h($simulacoes->faixa3) ?></td>
                <td><?= h($simulacoes->faixa4) ?></td>
                <td><?= h($simulacoes->faixa5) ?></td>
                <td><?= h($simulacoes->faixa6) ?></td>
                <td><?= h($simulacoes->faixa7) ?></td>
                <td><?= h($simulacoes->faixa8) ?></td>
                <td><?= h($simulacoes->faixa9) ?></td>
                <td><?= h($simulacoes->faixa10) ?></td>
                <td><?= h($simulacoes->faixa11) ?></td>
                <td><?= h($simulacoes->faixa12) ?></td>
                <td><?= h($simulacoes->data) ?></td>
                <td><?= h($simulacoes->telefone) ?></td>
                <td><?= h($simulacoes->user_id) ?></td>
                <td><?= h($simulacoes->calculoremoto) ?></td>
                <td><?= h($simulacoes->status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Simulacoes', 'action' => 'view', $simulacoes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Simulacoes', 'action' => 'edit', $simulacoes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Simulacoes', 'action' => 'delete', $simulacoes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $simulacoes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
