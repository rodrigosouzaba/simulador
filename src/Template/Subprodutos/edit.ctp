<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $subproduto->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $subproduto->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Subprodutos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subprodutos form large-9 medium-8 columns content">
    <?= $this->Form->create($subproduto) ?>
    <fieldset>
        <legend><?= __('Edit Subproduto') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('tipo_id', ['options' => $tipos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
