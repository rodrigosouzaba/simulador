<?=
$this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Sub-Produto', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'addSubProduto']);
?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <!--<th><?= $this->Paginator->sort('Código') ?></th>-->
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('Tipo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subprodutos as $subproduto): ?>
            <tr>
                <!--<td><?= $this->Number->format($subproduto->id) ?></td>-->
                <td><?= h($subproduto->nome) ?></td>
                <td><?= h($subproduto->tipo->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $subproduto->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $subproduto->id], ['confirm' => __('Tem certeza que deseja excluir o Sub-Produto Código # {0}?', $subproduto->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>