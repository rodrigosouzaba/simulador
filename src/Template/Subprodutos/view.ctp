<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Subproduto'), ['action' => 'edit', $subproduto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Subproduto'), ['action' => 'delete', $subproduto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subproduto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Subprodutos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subproduto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subprodutos view large-9 medium-8 columns content">
    <h3><?= h($subproduto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($subproduto->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo') ?></th>
            <td><?= $subproduto->has('tipo') ? $this->Html->link($subproduto->tipo->id, ['controller' => 'Tipos', 'action' => 'view', $subproduto->tipo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($subproduto->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($subproduto->descricao)); ?>
    </div>
</div>
