<style>
    .checkbox {
        padding-right: 5px;
    }
</style>

<?= $this->Form->create($tabela) ?>
<div class="col-xs-12">
    <div class="col-md-6 fonteReduzida">
        <?= $this->Form->input('nome', ['required' => 'required']); ?>
    </div>

    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('cod_ans', ['label' => 'Código ANS']); ?>
    </div>

    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('vigencia', ['label' => 'Vigência', 'required' => 'required']); ?>
    </div>

    <div class="col-md-12 fonteReduzida">
        <?= $this->Form->input('descricao', ['label' => 'Observação', 'required' => 'required']); ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="spacer-md">&nbsp;</div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('minimo_vidas', ['label' => 'Mínimo de Vidas', "style" => "margin-bottom: 0 !important", 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('maximo_vidas', ['label' => 'Máximo de Vidas', "style" => "margin-bottom: 0 !important", 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('titulares', ["style" => "margin-bottom: 0 !important", 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida form-inline">
        <?php
        $ids = array();
        foreach ($tabela["tabelas_cnpjs"] as $cnpj_selecionados) {
            $ids[] = $cnpj_selecionados["cnpj_id"];
        }
        ?>
        <?= $this->Form->input('cnpjs', ['options' => $cnpjs, 'multiple' => 'checkbox', "class" => "form-group", "label" => "Tipos de CNPJ", "style" => "max-height: 70px; min-height: 0 !important", "required" => false]); ?>
    </div>
    <div class="spacer-md">&nbsp;</div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-3 fonteReduzida" id="subproduto">
        <?= $this->Form->input('estado_id', ['options' => $estados, 'label' => ['text' => 'Estado', 'class' => 'text-danger'], 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida" id="operadoras">
        <?= $this->Form->input('operadora_id', ['options' => $operadoras, 'label' => ['text' => 'Operadoras', 'class' => 'text-danger'], 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida lero" id="produtos">
        <?= $this->Form->input('produto_id', ['options' => $produtos, 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida" id="comercializacoes">
        <?= $this->Form->input('regiao_id', ['options' => $regioes, "label" => ['text' => 'Comercializações', 'class' => 'text-danger'], 'empty' => 'SELECIONE', "required" => "required"]); ?>
    </div>
    <div class="spacer-xs">&nbsp;</div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('tipo_id', ['options' => $tipos, 'label' => 'Tipo de Acomodação', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('cobertura_id', ['options' => $coberturas, 'empty' => 'SELECIONE', 'label' => 'Tipo de cobertura', 'required' => true]); ?>
    </div>

    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('abrangencia_id', ['options' => $abrangencias, 'label' => ['text' => 'Área de Utilização', 'class' => 'text-danger'], 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">

        <?php $contratacao = array('2' => 'AMBOS', '0' => 'OPCIONAL', '1' => 'COMPULSÓRIO'); ?>
        <?= $this->Form->input('tipo_contratacao', ['options' => $contratacao, 'label' => 'Tipo de Contratação', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>

    <div class="spacer-xs">&nbsp;</div>
    <div class="col-md-3 fonteReduzida">
        <?php $filtroreembolso = array('N' => 'SEM REEMBOLSO', 'S' => 'COM REEMBOLSO'); ?>
        <?= $this->Form->input('reembolso', ['options' => $filtroreembolso, 'label' => 'Possui Reembolso', 'empty' => 'SELECIONE', 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('coparticipacao', ['options' => $coparticipacao, 'empty' => 'SELECIONE', 'label' => 'Possui Coparticipação?', 'selected' => $tabela['coparticipacao']]); ?>
    </div>
    <div class="col-md-3 fonteReduzida" id="detalheCoparticipacao" style="display: none;">
        <?= $this->Form->input('detalhe_coparticipacao', ['label' => 'Detalhes Coparticipação. Exemplo ("10%", "FN10", etc..) ', 'style' => 'margin-bottom: 0 !important']); ?>
    </div>
    <div id="odontoValor">
        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('odonto_operadora', ['options' => $odonto_operadoras, 'empty' => 'Selecione Operadora']); ?>
        </div>
        <div class="col-md-3 fonteReduzida">
            <label for="odonto_valor" id="label-odonto">Preço Odonto por Vida <b style="color:#C3232D">*</b></label>
            <?= $this->Form->input('odonto_valor', ['label' => false, 'style' => 'margin-bottom: 0 !important', 'min' => 0, 'step' => '0.01']); ?>
        </div>
    </div>
</div>

<div class="spacer-xs">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-12 centralizada fonteReduzida ">


        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa1', ['label' => '0 à 18 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa1]); ?>
            <label for="check1">
                <?= $this->Form->checkbox('check1', ["id" => "check1", "class" => "consulta", "campo" => "faixa1", "tabindex" => "1"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa2', ['label' => '19 à 23 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa2]); ?>
            <label for="check2">
                <?= $this->Form->checkbox('check2', ["id" => "check2", "class" => "consulta", "campo" => "faixa2", "tabindex" => "2"]); ?>
                So consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa3', ['label' => '24 à 28 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa3]); ?>
            <label for="check3">
                <?= $this->Form->checkbox('check3', ["id" => "check3", "class" => "consulta", "campo" => "faixa3", "tabindex" => "3"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa4', ['label' => '29 à 33 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa4]); ?>
            <label for="check4">
                <?= $this->Form->checkbox('check4', ["id" => "check4", "class" => "consulta", "campo" => "faixa4", "tabindex" => "4"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa5', ['label' => '34 à 38 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa5]); ?>
            <label for="check5">
                <?= $this->Form->checkbox('check5', ["id" => "check5", "class" => "consulta", "campo" => "faixa5", "tabindex" => "5"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa6', ['label' => '39 à 43 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa6]); ?>
            <label for="check6">
                <?= $this->Form->checkbox('check6', ["id" => "check6", "class" => "consulta", "campo" => "faixa6", "tabindex" => "6"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa7', ['label' => '44 à 48 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa7]); ?>
            <label for="check7">
                <?= $this->Form->checkbox('check7', ["id" => "check7", "class" => "consulta", "campo" => "faixa7", "tabindex" => "7"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa8', ['label' => '49 à 53 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa8]); ?>
            <label for="check8">
                <?= $this->Form->checkbox('check8', ["id" => "check8", "class" => "consulta", "campo" => "faixa8", "tabindex" => "8"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa9', ['label' => '54 à 58 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa9]); ?>
            <label for="check9">
                <?= $this->Form->checkbox('check9', ["id" => "check9", "class" => "consulta", "campo" => "faixa9", "tabindex" => "9"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa10', ['label' => '59 à 64 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa10]); ?>
            <label for="check10">
                <?= $this->Form->checkbox('check10', ["id" => "check10", "class" => "consulta", "campo" => "faixa10", "tabindex" => "10"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa11', ['label' => '65 à 80 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa11]); ?>
            <label for="check11">

                <?= $this->Form->checkbox('check11', ["id" => "check11", "class" => "consulta", "campo" => "faixa11", "tabindex" => "11"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa12', ['label' => '+ de 81 anos', 'type' => 'number', 'min' => 0, 'step' => 0.01, 'class' => 'centralizada dinheiro', 'required' => 'required', 'value' => $tabela->faixa12]); ?>
            <label for="check12">
                <?= $this->Form->checkbox('check12', ["id" => "check12", "class" => "consulta", "campo" => "faixa12", "tabindex" => "12"]); ?>
                Sob consulta
            </label>
        </div>


    </div>
</div>

<div class="clearfix">&nbsp;</div>

<?= $this->element('botoesAdd') ?>
<?= $this->Form->end() ?>

<?= $this->element("processando"); ?>

<script type="text/javascript">
    $(document).ready(function() {

        for (var i = 1; i <= 12; i++) {
            if ($("#check" + i).is(":checked")) {
                $("#faixa" + i).css("background-color", 'lightgray');
            } else {
                $("#faixa" + i).css("background-color", 'white');
            }
        }

        if ($("#odonto-valor").val() == '' | $("#odonto-valor").val() == null) {
            $("#odontoValor").hide();
        }
        var arr = <?= json_encode($ids); ?>;
        $.each(arr, function(index, val) {
            $("#cnpjs-" + val).prop('checked', true);
        });

        $("#estado-id").change(function() {
            $("#loader").click();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>tabelas/find_operadoras/" + $(
                    "#estado-id").val(),
                success: function(data) {
                    $("#operadoras").empty();
                    $("#operadoras").append(data);
                    $("#fechar").click();
                }
            });

        });

        $(".consulta").click(function() {
            var campo = $(this).attr("campo");
            if ($(this).is(":checked")) {
                $("#" + campo).css("background-color", 'lightgray');
            } else {
                $("#" + campo).css("background-color", 'white');
            }
        })

        $("#operadora-id").change(function() {
            $("#acoes").show();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>tabelas/find_produtos/" + (this).value,
                data: $("#operadora-id").serialize(),
                success: function(data) {
                    $("#produtos").empty();
                    $("#produtos").append(data);
                }
            });
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>tabelas/find_regioes/" + (this).value +
                    "/" + $("#estado-id").val(),
                data: $("#operadora-id").serialize(),
                success: function(data) {
                    $("#regiao").empty();
                    $("#regiao").append(data);
                }
            });
            $.ajax({
                type: 'GET',
                url: "<?php echo $this->request->webroot ?>tabelas/find_comercializacoes/" + $("#operadora-id").val(),
                success: function(data) {
                    $("#comercializacoes").empty();
                    $("#comercializacoes").append(data);
                    $("#fechar").click();
                }
            });
        });
    });
    $("#coparticipacao").change(function() {
        //           alert((this).value);
        if ((this).value === 's') {
            $("#detalheCoparticipacao").show();
        } else {
            $("#detalheCoparticipacao").hide();

        }

    });
    $("#tipo-produto-id").change(function() {
        if ($(this).val() == 4 || $(this).val() == 5 || $(this).val() == 6) {
            $("#odontoValor").show();
            $("#odonto-valor").prop('required', true);
        } else {
            $("#odonto-valor").removeAttr("required");
            $("#odontoValor").hide();
        }
    });
</script>