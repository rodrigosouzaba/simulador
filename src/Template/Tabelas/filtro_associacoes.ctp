<div class="col-md-3">
    <?= $this->Form->control('rede_id', ['empty' => 'SELECIONE REDE', 'options' => $redes]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('observacao_id', ['empty' => 'SELECIONE OBSERVAÇÃO', 'options' => $observacoes]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('reembolso_id', ['empty' => 'SELECIONE REEMBOLSO', 'options' => $reembolsos]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('carencia_id', ['empty' => 'SELECIONE CARENCIA', 'options' => $carencias]) ?>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-md-3">
    <?= $this->Form->control('opicional_id', ['empty' => 'SELECIONE DEPENDENTE', 'options' => $opcionais]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('informacao_id', ['empty' => 'SELECIONE DOCUMENTO', 'options' => $informacoes]) ?>
</div>
<div class="col-md-3">
    <?= $this->Form->control('formas_pagamento_id', ['empty' => 'SELECIONE FORMAS PAGAMENTO', 'options' => $formas_pagamentos]) ?>
</div>