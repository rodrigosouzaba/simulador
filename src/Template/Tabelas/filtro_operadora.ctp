<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<?= $this->Form->create('selecaoTabelas', ['id' => 'selecaoTabelas']); ?>

<?php if ($tabelas) { ?>
    <div class="col-md-12">


        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td class="tituloTabela centralizada" style="width: 2%"><?= 'Sel.' ?></td>

                    <td class="tituloTabela centralizada" style="width: 17%"><?= 'Produto' ?></td>
                    <td class="tituloTabela centralizada">Tipo Cobertura</td>
                    <td class="tituloTabela centralizada">Contratação</td>
                    <td class="tituloTabela centralizada" style="width: 7%">CNPJ</td>
                    <td class="tituloTabela centralizada" style="width: 5%"><?= 'Vidas' ?></td>
                    <td class="tituloTabela centralizada"><?= 'Coparticipação' ?></td>
                    <td class="tituloTabela centralizada">Acomodação</td>
                    <td class="tituloTabela centralizada" style="width: 6%"><?= 'Vigência' ?></td>
                    <td class="tituloTabela text-center centralizada" style="width: 6%"><?= 'Prioridade' ?></td>

                    <td class="actions tituloTabela centralizada"><?= __('Actions') ?></td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tabelas as $tabela) :
                ?>
                    <tr>
                        <td style="width: 2%" class=" centralizada">
                            <?= $this->Form->input('tabela' . $tabela->id, ['type' => 'checkbox', 'value' => $tabela->id, 'label' => '', 'class' => 'checkHabilita']) ?>
                        </td>
                        <td style="font-size: 90% !important" class=" centralizada">
                            <?= "<b>" . $tabela->produto->nome . "</b><br/><small>" . $tabela->nome . "</small>" ?></td>
                        <td style="font-size: 90% !important" class=" centralizada">
                            <?= (isset($tabela->tipos_produto->nome) ? $tabela->tipos_produto->nome : "") ?>
                            <?php //debug($tabela);
                            ?>
                        </td>
                        <td style="font-size: 90% !important" class=" centralizada">
                            <?php

                            switch ($tabela->tipo_contratacao) {
                                case 0:
                                    echo "OPCIONAL";
                                    break;
                                case 1:
                                    echo "COMPULSÓRIO";
                                    break;
                                case 2:
                                    echo "AMBOS";
                                    break;
                            }
                            ?>
                        </td>
                        <td style="font-size: 90% !important" class=" centralizada">
                            <?php
                            //                         	debug(count($tabela["tabelas_cnpjs"]));die();
                            $tamanho = count($tabela["tabelas_cnpjs"]);
                            $i = 1;
                            foreach ($tabela["tabelas_cnpjs"] as  $cnpj) {
                                if ($i < $tamanho) {
                                    echo $cnpj["cnpj"]["nome"] . ",";
                                } else {
                                    echo $cnpj["cnpj"]["nome"];
                                }
                                $i++;
                            }

                            ?>
                        </td>
                        <td style="font-size: 90% !important" class=" centralizada">
                            <?= $tabela->minimo_vidas . " a " . $tabela->maximo_vidas ?></td>
                        <td style="font-size: 90% !important" class=" centralizada">
                            <?= ($tabela->coparticipacao == 'n') ? "S/ Coparticipação" : "C/ Coparticipação " . $tabela->detalhe_coparticipacao ?>
                        </td>
                        <td style="font-size: 90% !important" class=" centralizada"><?= h($tabela->tipo->nome) ?></td>
                        <td style="font-size: 90% !important" class=" centralizada"><?= h($tabela->vigencia) ?></td>
                        <td class="text-center centralizada" style="font-size: 90% !important"><?= h($tabela->prioridade) ?>
                        </td>

                        <td class="actions" style="font-size: 90% !important">

                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), ['action' => 'edit', $tabela->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar / Preços']) ?>

                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-copy', 'aria-hidden' => 'true']), ['action' => 'duplicar', $tabela->id], ['class' => 'btn btn-sm btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Duplicar tabela']) ?>

                            <?php
                            if ($sessao['role'] == 'admin') :
                                if ($tabela->validade == null || $tabela->validade == 0) {

                                    echo $this->Html->link(
                                        $this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']),
                                        '#',
                                        [
                                            'class' => 'btn btn-sm btn-danger margemMinima validar',
                                            'escape' => false,
                                            'value' => $tabela->id,
                                            'data-toggle' => 'tooltip',
                                            'data-placement' => 'top',
                                            'title' => 'SUSPENSA'
                                        ]
                                    );
                                } else {

                                    echo $this->Html->link(
                                        $this->Html->tag('span', '', ['class' => 'fa fa-check', 'aria-hidden' => 'true']),
                                        '#',
                                        array(
                                            'class' => 'btn btn-sm btn-success margemMinima invalidar',
                                            'escape' => false,
                                            'value' => $tabela->id,
                                            'data-toggle' => 'tooltip',
                                            'data-placement' => 'top',
                                            'title' => 'ATIVA'
                                        )
                                    );
                                }

                                echo $this->Html->link($this->Html->tag('span', '', [
                                    'class' => 'fa fa-list',
                                    'aria-hidden' => 'true'
                                ]), '#', [
                                    'class' => 'btn btn-sm btn-default margemMinima alterarPrioridade',
                                    'escape' => false,
                                    'value' => $tabela->id,
                                    'data-placement' => 'top',
                                    'title' => 'Definir Prioridade',
                                    'data-toggle' => "modal",
                                    'data-target' => "#modalPrioridade"
                                ]);

                                if ($tabela->atualizacao == null || $tabela->atualizacao == 0) :

                                    echo $this->Html->link('', '#', [
                                        'class' => 'btn btn-sm btn-default fa fa-refresh colocarAtlz',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'value' => $tabela->id,
                                        'title' => 'Colocar em Atualização de Preços'
                                    ]);
                                else :
                                    echo $this->Html->link('', '#', [
                                        'class' => 'btn btn-sm btn-danger fa fa-refresh removerAtlz',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'value' => $tabela->id,
                                        'title' => 'Remover Atualização de Preços'
                                    ]);
                                endif;

                                echo $this->Form->postLink(
                                    $this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']),
                                    array(
                                        'action' => 'delete', $tabela->id
                                    ),
                                    array(
                                        'class' => 'btn btn-sm btn-danger margemMinima',
                                        'escape' => false,
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'title' => 'Remover',
                                        'confirm' => 'Confirma exclusão da Tabela?'
                                    )
                                );
                            endif;
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div class="col-md-12">
        <?=
            $this->Html->link($this->Html->tag('span', '', [
                'class' => 'fa fa-trash',
                'aria-hidden' => 'true'
            ]) . ' Deletar Tabelas', '#', [
                'class' => 'btn btn-sm btn-danger',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Deletar tabelas',
                'id' => 'deleteLote',
                'confirm' => 'Tem certeza que deseja apagar essas tabelas?'
            ])
        ?>

        <?=
            $this->Html->link($this->Html->tag('span', '', [
                'class' => 'fa fa-percent',
                'aria-hidden' => 'true'
            ]) . ' Reajustar Tabelas', '#', [
                'class' => 'btn btn-sm btn-default',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Reajustar Tabelas',
                'id' => 'reajustarLote',
                'data-toggle' => "modal",
                'data-target' => "#modalTabelas"
            ])
        ?>
        <?=
            $this->Html->link($this->Html->tag('span', '', [
                'class' => 'fa fa-calendar',
                'aria-hidden' => 'true'
            ]) . ' Alterar Vigência', '#', [
                'class' => 'btn btn-sm btn-default',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Alterar Vigências',
                'id' => 'reajustarVigencia',
                'data-toggle' => "modal",
                'data-target' => "#modalTabelas"
            ])
        ?>
        <?= $this->Form->hidden('tabelas'); ?>

        <?=
            $this->Form->button('<i class="fa fa-file-pdf-o"></i> Gerar Tabela', array(
                'type' => 'submit',
                'class' => 'btn btn-sm btn-default',
                'escape' => false
            ));
        ?>


        <!-- <?=
                    $this->Html->link($this->Html->tag('span', '', [
                        'target' => '_blank',
                        'class' => 'fa fa-file-pdf-o',
                        'aria-hidden' => 'true'
                    ]) . ' Gerar Tabela', '#', [
                        'class' => 'btn btn-sm btn-default',
                        'role' => 'button',
                        'escape' => false,
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'Gerar Tabelas',
                        'id' => 'gerarTabela'
                    ])

                //                        $form->end('Save Post');
                ?>-->
        <div id="pdf"></div>
    </div>
<?php } else {
?>
    <div class="clearfix">&nbsp;</div>
    <div class="clearfix">&nbsp;</div>
    <p class="bg-danger centralizada">Nenhuma tabela encontrada</p>
<?php } ?>




<!-- Modal -->
<div class="modal fade" id="modalTabelas" tabindex="-1" role="dialog" aria-labelledby="modalTabelasLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalTabelasLabel">Reajustar Tabelas</h4>
                <small> Formato 99.99 - Usar "." como separador de decimais. Não Há necessidade de digitar "%".</small>
            </div>
            <div class="modal-body" id="percentual" style="min-height: 150px !important;">
                <div class="clearfix"> &nbsp;</div>
                &nbsp;
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalPrioridade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Alterar Ordem de Prioridade da Tabela</h4>
                <small> As tabelas serão exibidas nas simulações de acordo com sua <b>Ordem de Prioridade</b>
                    definida</small>
            </div>
            <div class="modal-body" id="prioridade" style="min-height: 150px !important;">
                <div class="clearfix"> &nbsp;</div>
                &nbsp;
            </div>

        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<?= $this->element('processando'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#deleteLote").hide();
        $("#reajustarLote").hide();

        $('#modalTabelas').on('shown.bs.modal', function() {
            $('#myInput').focus()
        });
        $('#modalPrioridade').on('shown.bs.modal', function() {
            $('#myInput').focus()
        });
        $(".validar").click(function() {
            $.ajax({
                type: "post",
                url: "<?php echo $this->request->webroot ?>tabelas/validar/" + $(this).attr(
                    'value'),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });
        $(".invalidar").click(function() {
            $.ajax({
                type: "post",
                url: "<?php echo $this->request->webroot ?>tabelas/invalidar/" + $(this).attr(
                    'value'),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });
        $(".colocarAtlz").click(function() {
            $.ajax({
                type: "post",
                url: "<?php echo $this->request->webroot ?>tabelas/colocarAtlz/" + $(this).attr(
                    'value'),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });
        $(".removerAtlz").click(function() {
            $.ajax({
                type: "post",
                url: "<?php echo $this->request->webroot ?>tabelas/removerAtlz/" + $(this).attr(
                    'value'),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });
        $("#deleteLote").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: "<?php echo $this->request->webroot ?>tabelas/deleteLote/",
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });
        $("#reajustarLote").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: "<?php echo $this->request->webroot ?>tabelas/reajusteLote/" + $(
                    "#selecaoTabelas").serialize(),
                success: function(data) {
                    $("#percentual").empty();
                    $("#percentual").append(data);

                }
            });
        });
        $("#reajustarVigencia").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: "<?php echo $this->request->webroot ?>tabelas/reajusteVigencia/" + $(
                    "#selecaoTabelas").serialize(),
                success: function(data) {
                    $("#percentual").empty();
                    $("#percentual").append(data);

                }
            });
        });
        $(".alterarPrioridade").click(function() {
            $.ajax({
                //                type: "post",
                data: $("#prioridadeForm").serialize(),
                url: "<?php echo $this->request->webroot ?>tabelas/prioridade/" + $(this).attr(
                    "value"),
                success: function(data) {
                    $("#prioridade").empty();
                    $("#prioridade").append(data);

                }
            });
        });

        //        $("#pdf").click(function () {
        //
        //            $.ajax({
        //                type: "post",
        //                data: $("#operadoras").serialize(),
        //                url: "<?php echo $this->request->webroot ?>simulacoes/pdf/" + $("#operadoras").serialize(),
        //                beforeSend: function () {
        //                    $('#loader').trigger('click');
        //                },
        //                success: function (data) {
        //                    $('#fechar').trigger('click');
        //                }
        //            });
        //        });



    });
    $("#gerarTabela").click(function() {
        $.ajax({
            type: "post",
            data: $('#selecaoTabelas').serialize(),
            url: "<?php echo $this->request->webroot ?>tabelas/gerarTabela/" + $('#selecaoTabelas')
                .serialize(),
            beforeSend: function() {
                $('#loader').trigger('click');
            },
            success: function(data) {
                $('#fechar').trigger('click');
            }
        });
    });

    $(".checkHabilita").click(function() {
        $("#deleteLote").show();
        $("#reajustarLote").show();
    });
</script>