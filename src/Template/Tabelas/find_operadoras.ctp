<?= $this->Form->input('operadora_id', ['options' => $operadoras, "label" => "Operadoras", "empty" => "SELECIONE A OPERADORA", "required" => 'required']); ?>
<script type="text/javascript">
    $("#operadora-id").change(function() {
        $("#loader").click();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>tabelas/find_produtos/" + $("#operadora-id").val(),
            data: $("#estado-id").serialize(),
            success: function(data) {
                $("#produtos").empty();
                $("#produtos").append(data);
                $("#fechar").click();
            }
        });

        $.ajax({
            type: 'GET',
            url: "<?php echo $this->request->webroot ?>tabelas/find_comercializacoes/" + $("#operadora-id").val(),
            success: function(data) {
                $("#comercializacoes").empty();
                $("#comercializacoes").append(data);
                $("#fechar").click();
            }
        });

    });
</script>
