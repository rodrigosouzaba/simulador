<?= $this->Form->create("gerador", ["id" => "gerador"]) ?>
<div class="clearfix">
	&nbsp;
</div>
<div class="col-xs-12 centralizada">
	<h4 style="margin: 0 !important; ">SELECIONE AS CARACTERÍSTICAS DA TABELA</h4>
</div>
<div class="col-xs-12">

	<div class="clearfix">
		&nbsp;
	</div>
	<div class="col-xs-12 col-md-2 col-md-offset-1">
		<?php $ramos = array("SPF" => "SAÚDE PESSOA FÍSICA", "SPJ" => "SAÚDE PESSOA JURÍDICA","SPE" => "SAÚDE PROJETOS ESPECIAIS", "OPF" => "ODONTO PESSOA FÍSICA", "OPJ" => "ODONTO PESSOA JURÍDICA", "OPE" => "ODONTO PROJETOS ESPECIAIS")?>
		<?= $this->Form->input('ramo_id', ['options' => $ramos,"label" => false, "empty" => "SELECIONE O RAMO"]); ?>

	</div>
	<div class="col-xs-12 col-md-2">
		<?= $this->Form->input('estado_id', ['options' => $estados,"label" => false, "empty" => "SELECIONE O ESTADO"]); ?>
	
	</div>
	<div class="col-xs-12 col-md-2" id="op">
		<?= $this->Form->input('operadora_id', ['options' => "","label" => false, "empty" => "SELECIONE A OPERADORA", "disabled" => "disabled"]); ?>
	</div>
	<div class="col-xs-12 col-md-2" id="produtos">
		<?= $this->Form->input('produto_id', ['options' => "","label" => false, "empty" => "SELECIONE O NÚMERO VIDAS", "disabled" => "disabled"]); ?>
	</div>
	<div class="col-xs-12 col-md-2">
		<?php 
			$coparticipacao = array("s" => "COM COPARTICIPAÇÃO","n" => "SEM COPARTICIPAÇÃO", "99" => "TODOS");
		?>
		<?= $this->Form->input('coparticipacao', ['options' => $coparticipacao,"label" => false, "empty" => "SELECIONE COPARTICIPAÇÃO", "disabled" => "disabled"]); ?>
	</div>
	<div class="col-xs-12 col-md-8 col-md-offset-2" id="respostaProdutos">
		
	</div>
	<?= $this->Form->end() ?>
	<div class="col-xs-12" style="height: 10px !important"> 
		<?= "&nbsp;" ?>
	</div>
		
	 <div class="col-xs-12 col-md-12 centralizada">
		     <?= $this->Html->link('Gerar Tabela', "#", ['class' => 'btn btn-md btn-default', 'role' => 'button',"disabled" => "disabled",'escape' => false, "id" => "gerarTabela"]); ?>
		     <?= $this->Html->link('Download PDF', "#", ['class' => 'btn btn-md btn-primary', 'role' => 'button',"disabled" => "disabled",'escape' => false, "id" => "gerarPDF"]); ?>
	 </div>

<div class="col-xs-12" id="resposta">
</div>
	
	
</div>

<script type="text/javascript">
$("#estado-id").change(function () {
	if($(this).val() != ''){
	   $.ajax({
	       type: "POST",
	       url: "<?php echo $this->request->webroot ?>tabelas/find-operadoras/"+$("#estado-id").val(),
	       data: $("#estado-id").serialize(),
	       success: function (data) {
	           $("#op").empty();
	           $("#op").append(data);
	           $("#operadora-id").prop("disabled", false);
	       }
	   });
	}else{
		$("#operadora-id").prop();
	}
});
$("#produto-id").change(function () {
	$("#coparticipacao").prop("disabled", false);
});
$("#coparticipacao").change(function () {
	$("#gerarTabela").removeAttr("disabled");
});

$("#gerarTabela").click(function () {
	$.ajax({
       type: "POST",
       url: "<?php echo $this->request->webroot ?>tabelas/tabela/",
       data: $("#gerador").serialize(),
       success: function (data) {
           $("#resposta").empty();
           $("#resposta").append(data);
       }
   });
});


$("#coparticipacao").change(function () {
   $.ajax({
       type: "POST",
       url: "<?php echo $this->request->webroot ?>tabelas/gerador-produtos/",
       data: $("#gerador").serialize(),
       success: function (data) {
           $("#respostaProdutos").empty();
           $("#respostaProdutos").append(data);
       }
   });
});


</script>