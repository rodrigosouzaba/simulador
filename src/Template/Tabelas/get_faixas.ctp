<?php if (!$is_vitalmed) : ?>
    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa1', ['label' => '0 à 18 anos', 'type' => 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa1]); ?>
        <label for="check1">
            <?= $this->Form->checkbox('check1', ["id" => "check1", "class" => "consulta", "campo" => "faixa1", "tabindex" => "1"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa2', ['label' => '19 à 23 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa2]); ?>
        <label for="check2">
            <?= $this->Form->checkbox('check2', ["id" => "check2", "class" => "consulta", "campo" => "faixa2", "tabindex" => "2"]); ?>
            So consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa3', ['label' => '24 à 28 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa3]); ?>
        <label for="check3">
            <?= $this->Form->checkbox('check3', ["id" => "check3", "class" => "consulta", "campo" => "faixa3", "tabindex" => "3"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa4', ['label' => '29 à 33 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa4]); ?>
        <label for="check4">
            <?= $this->Form->checkbox('check4', ["id" => "check4", "class" => "consulta", "campo" => "faixa4", "tabindex" => "4"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa5', ['label' => '34 à 38 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa5]); ?>
        <label for="check5">
            <?= $this->Form->checkbox('check5', ["id" => "check5", "class" => "consulta", "campo" => "faixa5", "tabindex" => "5"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa6', ['label' => '39 à 43 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa6]); ?>
        <label for="check6">
            <?= $this->Form->checkbox('check6', ["id" => "check6", "class" => "consulta", "campo" => "faixa6", "tabindex" => "6"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa7', ['label' => '44 à 48 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa7]); ?>
        <label for="check7">
            <?= $this->Form->checkbox('check7', ["id" => "check7", "class" => "consulta", "campo" => "faixa7", "tabindex" => "7"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa8', ['label' => '49 à 53 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa8]); ?>
        <label for="check8">
            <?= $this->Form->checkbox('check8', ["id" => "check8", "class" => "consulta", "campo" => "faixa8", "tabindex" => "8"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa9', ['label' => '54 à 58 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa9]); ?>
        <label for="check9">
            <?= $this->Form->checkbox('check9', ["id" => "check9", "class" => "consulta", "campo" => "faixa9", "tabindex" => "9"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa10', ['label' => '59 à 64 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa10]); ?>
        <label for="check10">
            <?= $this->Form->checkbox('check10', ["id" => "check10", "class" => "consulta", "campo" => "faixa10", "tabindex" => "10"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa11', ['label' => '65 à 80 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa11]); ?>
        <label for="check11">

            <?= $this->Form->checkbox('check11', ["id" => "check11", "class" => "consulta", "campo" => "faixa11", "tabindex" => "11"]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-1">
        <?= $this->Form->input('faixa12', ['label' => '+ de 81 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa12]); ?>
        <label for="check12">
            <?= $this->Form->checkbox('check12', ["id" => "check12", "class" => "consulta", "campo" => "faixa12", "tabindex" => "12"]); ?>
            Sob consulta
        </label>
    </div>
<?php else : ?>

    <div class="faixaEtaria col-md-2">
        <?= $this->Form->input('faixa_extra_1', ['label' => '0 à 14 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
        <label for="check_extra_1">
            <?= $this->Form->checkbox('check_extra_1', ["id" => "check_extra_1", "class" => "consulta", "campo" => "faixa_extra_1", "tabindex" => "1", "value" => (isset($tabela) ? $tabela->faixa_extra_1 : '')]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-2">
        <?= $this->Form->input('faixa_extra_2', ['label' => '15 à 29 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
        <label for="check_extra_2">
            <?= $this->Form->checkbox('check_extra_2', ["id" => "check_extra_2", "class" => "consulta", "campo" => "faixa_extra_2", "tabindex" => "2", "value" => (isset($tabela) ? $tabela->faixa_extra_2 : '')]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-2">
        <?= $this->Form->input('faixa_extra_3', ['label' => '30 à 39 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
        <label for="check_extra_3">
            <?= $this->Form->checkbox('check_extra_3', ["id" => "check_extra_3", "class" => "consulta", "campo" => "faixa_extra_3", "tabindex" => "3", "value" => (isset($tabela) ? $tabela->faixa_extra_3 : '')]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-2">
        <?= $this->Form->input('faixa_extra_4', ['label' => '40 à 49 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
        <label for="check_extra_4">
            <?= $this->Form->checkbox('check_extra_4', ["id" => "check_extra_4", "class" => "consulta", "campo" => "faixa_extra_4", "tabindex" => "4", "value" => (isset($tabela) ? $tabela->faixa_extra_4 : '')]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-2">
        <?= $this->Form->input('faixa_extra_5', ['label' => '50 à 58 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
        <label for="check_extra_5">
            <?= $this->Form->checkbox('check_extra_5', ["id" => "check_extra_5", "class" => "consulta", "campo" => "faixa_extra_5", "tabindex" => "5", "value" => (isset($tabela) ? $tabela->faixa_extra_5 : '')]); ?>
            Sob consulta
        </label>
    </div>

    <div class="faixaEtaria col-md-2">
        <?= $this->Form->input('faixa_extra_6', ['label' => '+ de 58', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
        <label for="check_extra_6">
            <?= $this->Form->checkbox('check_extra_6', ["id" => "check_extra_6", "class" => "consulta", "campo" => "faixa_extra_6", "tabindex" => "6", "value" => (isset($tabela) ? $tabela->faixa_extra_6 : '')]); ?>
            Sob consulta
        </label>
    </div>
<?php endif; ?>