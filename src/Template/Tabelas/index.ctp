<section id="AppTabelas">
    <?= $this->Form->hidden('old', ['id' => 'tablePmeOld', 'value' => 1]); ?>
    <div class="row">
        <?php echo $this->Form->create('search', ['id' => 'pesquisarTabela', 'class' => 'form', 'role' => 'form']); ?>
        <div class="col-md-3 fonteReduzida">
            <?=
            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Tabela', ['action' => 'add'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'tabelas.index']);
            ?>
        </div>
        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('estados', ['id' => 'estados-old', 'options' => $estados, 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
        </div>
        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('operadora_id', ['id' => 'operadora-id-old', 'options' => '', 'label' => '', 'empty' => 'Selecione OPERADORA', 'disabled' => true]); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    <div class="row">
        <div id="gridCalculo" clas="col-md-12" style="padding: 15px;">
            <form id="selecaoTabelas">
                <?php if (isset($tabelas) && !empty($tabelas)) { ?>
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <td class="tituloTabela centralizada" style="width: 2%"><?= 'Sel.' ?></td>
                                <td class="tituloTabela centralizada"><?= 'Operadora' ?></td>
                                <td class="tituloTabela centralizada" style="width: 17%"><?= 'Produto' ?></td>
                                <td class="tituloTabela centralizada">Tipo Cobertura</td>
                                <td class="tituloTabela centralizada">Contratação</td>
                                <td class="tituloTabela centralizada" style="width: 7%">CNPJ</td>
                                <td class="tituloTabela centralizada" style="width: 5%"><?= 'Vidas' ?></td>
                                <td class="tituloTabela centralizada"><?= 'Coparticipação' ?></td>
                                <td class="tituloTabela centralizada">Acomodação</td>
                                <td class="tituloTabela centralizada" style="width: 6%"><?= 'Vigência' ?></td>
                                <td class="tituloTabela text-center centralizada" style="width: 6%"><?= 'Prioridade' ?></td>
                                <th class="actions" style="width: max-content;"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($tabelas as $tabela) : ?>
                                <tr>
                                    <td style="width: 2%" class=" centralizada"> <?= $this->Form->input('tabela' . $tabela->id, ['type' => 'checkbox', 'value' => $tabela->id, 'label' => '', 'class' => 'checkHabilita']) ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= $this->Html->image("../" . $tabela->operadora->imagen->caminho . "/" . $tabela->operadora->imagen->nome, ['class' => 'logoMiniatura']) ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= "<b>" . $tabela->produto->nome . "</b><br/><small>" . $tabela->nome . "</small>" ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= (isset($tabela->tipos_produto->nome) ? $tabela->tipos_produto->nome : "") ?> </td>
                                    <td style="font-size: 90% !important" class=" centralizada">
                                        <?php

                                        switch ($tabela->tipo_contratacao) {
                                            case 0:
                                                echo "OPCIONAL";
                                                break;
                                            case 1:
                                                echo "COMPULSÓRIO";
                                                break;
                                            case 2:
                                                echo "AMBOS";
                                                break;
                                        }
                                        ?>
                                    </td>
                                    <td style="font-size: 90% !important" class=" centralizada">
                                        <?php
                                        $tamanho = count($tabela["tabelas_cnpjs"]);
                                        $i = 1;
                                        foreach ($tabela["tabelas_cnpjs"] as  $cnpj) {
                                            if ($i < $tamanho) {
                                                echo $cnpj["cnpj"]["nome"] . ",";
                                            } else {
                                                echo $cnpj["cnpj"]["nome"];
                                            }
                                            $i++;
                                        }

                                        ?>
                                    </td>
                                    <td style="font-size: 90% !important" class=" centralizada"> <?= $tabela->minimo_vidas . " a " . $tabela->maximo_vidas ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"> <?= ($tabela->coparticipacao == 'n') ? "S/ Coparticipação" : "C/ Coparticipação " . $tabela->detalhe_coparticipacao ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= h($tabela->tipo->nome) ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= h($tabela->vigencia) ?></td>
                                    <td class="text-center centralizada" style="font-size: 90% !important"><?= h($tabela->prioridade) ?></td>

                                    <td class="actions" style="font-size: 90% !important">

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm dropdown-toggle <?= $tabela->validade == null || $tabela->validade == 0 ? "btn-danger" : ($tabela->atualizacao != null || $tabela->atualizacao != 0 ? "btn-warning" : "btn-success")  ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-cogs"></i>
                                            </button>
                                            <ul class="dropdown-menu" style="left: auto; right: 0;">
                                                <?php if ($sessao['role'] == 'admin') : ?>
                                                    <li><a href="#"><?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']) . " Editar produto", ['action' => 'edit', $tabela->id], ['role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar / Preços', 'sid' => 'tabelas.edit']) ?></a></li>
                                                    <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-copy', 'aria-hidden' => 'true']) . " Duplicar Produto", ['action' => 'duplicar', $tabela->id], ['role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Duplicar tabela', 'sid' => 'tabelas.duplicar']) ?></li>
                                                    <li>
                                                        <?php if ($tabela->validade == null || $tabela->validade == 0) {

                                                            echo $this->Html->link(
                                                                $this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . " Produto Suspenso",
                                                                '#',
                                                                [
                                                                    'class' => ' btn-danger validar',
                                                                    'escape' => false,
                                                                    'sid' => 'tabelas.validar',
                                                                    'value' => $tabela->id,
                                                                    'data-toggle' => 'tooltip',
                                                                    'data-placement' => 'top',
                                                                    'title' => 'SUSPENSA'
                                                                ]
                                                            );
                                                        } else {

                                                            echo $this->Html->link(
                                                                $this->Html->tag('span', '', ['class' => 'fa fa-check', 'aria-hidden' => 'true']) . " Produto Ativo",
                                                                '#',
                                                                array(
                                                                    'class' => ' btn-success margemMinima invalidar',
                                                                    'escape' => false,
                                                                    'sid' => 'tabelas.invalidar',
                                                                    'value' => $tabela->id,
                                                                    'data-toggle' => 'tooltip',
                                                                    'data-placement' => 'top',
                                                                    'title' => 'ATIVA'
                                                                )
                                                            );
                                                        }
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link($this->Html->tag('span', '', [
                                                            'class' => 'fa fa-list',
                                                            'aria-hidden' => 'true'
                                                        ]) . " Definir Prioridade", '#', [
                                                            'class' => 'btn-default margemMinima alterarPrioridade',
                                                            'escape' => false,
                                                            'value' => $tabela->id,
                                                            'sid' => 'tabelas.prioridade',
                                                            'data-placement' => 'top',
                                                            'title' => 'Definir Prioridade',
                                                            // 'data-toggle' => "modal",
                                                            // 'data-target' => "#modalPrioridade"
                                                        ]);
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        if ($tabela->atualizacao == null || $tabela->atualizacao == 0) :

                                                            echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-refresh']) . " Atualizado", '#', [
                                                                'class' => ' btn-default colocarAtlz',
                                                                'data-toggle' => 'tooltip',
                                                                'data-placement' => 'top',
                                                                'sid' => 'tabelas.colocarAtlz',
                                                                'escape' => false,
                                                                'value' => $tabela->id,
                                                                'title' => 'Colocar em Atualização de Preços'
                                                            ]);
                                                        else :
                                                            echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-refresh']) . " Em Atualização", '#', [
                                                                'class' => ' btn-danger removerAtlz',
                                                                'data-toggle' => 'tooltip',
                                                                'data-placement' => 'top',
                                                                'sid' => 'tabelas.removerAtlz',
                                                                'escape' => false,
                                                                'value' => $tabela->id,
                                                                'title' => 'Remover Atualização de Preços'
                                                            ]);
                                                        endif;
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link(
                                                            $this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']) . " Deletar Produto",
                                                            array(
                                                                'action' => 'delete', $tabela->id
                                                            ),
                                                            array(
                                                                'class' => ' btn-default',
                                                                'escape' => false,
                                                                'data-toggle' => 'tooltip',
                                                                'sid' => 'tabelas.delete',
                                                                'data-placement' => 'top',
                                                                'title' => 'Remover',
                                                                'confirm' => 'Confirma exclusão da Tabela?'
                                                            )
                                                        ); ?>
                                                    </li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="col-md-12">
                        <?=
                        $this->Html->link($this->Html->tag('span', '', [
                            'class' => 'fa fa-trash',
                            'aria-hidden' => 'true'
                        ]) . ' Deletar Produtos', '#', [
                            'class' => 'btn btn-sm btn-danger',
                            'role' => 'button',
                            'escape' => false,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Deletar Produtos',
                            'id' => 'deleteLote',
                            'sid' => 'produtos.deleteLote'
                        ])
                        ?>
                    </div>
                    <div class="col-md-12">
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                <?php
                } else {
                ?>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="col-md-12 text-danger centralizada"> NENHUM PRODUTO ENCONTRADO</div>
                <?php
                }
                ?>
            </form>
        </div>
    </div>
</section>
<div class="col-md-12">
    <?=
    $this->Html->link($this->Html->tag('span', '', [
        'class' => 'fa fa-trash',
        'aria-hidden' => 'true'
    ]) . ' Deletar Tabelas', '#', [
        'class' => 'btn btn-sm btn-danger',
        'role' => 'button',
        'escape' => false,
        'data-toggle' => 'tooltip',
        'data-placement' => 'top',
        'title' => 'Deletar tabelas',
        'id' => 'deleteLote',
        'sid' => 'produtos.deleteLote',
        'confirm' => 'Tem certeza que deseja apagar essas tabelas?'
    ])
    ?>

    <?=
    $this->Html->link($this->Html->tag('span', '', [
        'class' => 'fa fa-percent',
        'aria-hidden' => 'true'
    ]) . ' Reajustar Tabelas', '#', [
        'class' => 'btn btn-sm btn-default',
        'role' => 'button',
        'escape' => false,
        'data-toggle' => 'tooltip',
        'data-placement' => 'top',
        'title' => 'Reajustar Tabelas',
        'id' => 'reajustarLote',
        'sid' => 'tabelas.reajusteLote',
        'data-toggle' => "modal",
        'data-target' => "#modalTabelas"
    ])
    ?>
    <?=
    $this->Html->link($this->Html->tag('span', '', [
        'class' => 'fa fa-calendar',
        'aria-hidden' => 'true'
    ]) . ' Alterar Vigência', '#', [
        'class' => 'btn btn-sm btn-default',
        'role' => 'button',
        'escape' => false,
        'data-toggle' => 'tooltip',
        'data-placement' => 'top',
        'title' => 'Alterar Vigências',
        'id' => 'reajustarVigencia',
        'sid' => 'tabelas.reajusteVigencia',
        'data-toggle' => "modal",
        'data-target' => "#modalTabelas"
    ])
    ?>
    <?= $this->Form->hidden('tabelas'); ?>

    <?=
    $this->Form->button('<i class="fa fa-file-pdf-o"></i> Gerar Tabela', array(
        'type' => 'submit',
        'class' => 'btn btn-sm btn-default',
        'escape' => false
    ));
    ?>


    <!-- <?=
            $this->Html->link($this->Html->tag('span', '', [
                'target' => '_blank',
                'class' => 'fa fa-file-pdf-o',
                'aria-hidden' => 'true'
            ]) . ' Gerar Tabela', '#', [
                'class' => 'btn btn-sm btn-default',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Gerar Tabelas',
                'id' => 'gerarTabela'
            ])

            //                        $form->end('Save Post');
            ?>-->
    <div id="pdf"></div>

    <!-- Modal -->
    <div class="modal fade" id="modalTabelas" tabindex="-1" role="dialog" aria-labelledby="modalTabelasLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalTabelasLabel">Reajustar Tabelas</h4>
                    <small> Formato 99.99 - Usar "." como separador de decimais. Não Há necessidade de digitar "%".</small>
                </div>
                <div class="modal-body" id="percentual" style="min-height: 150px !important;">
                    <div class="clearfix"> &nbsp;</div>
                    &nbsp;
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        $("#deleteLote").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "produtos/deleteLote",
                success: function(data) {
                    window.location = baseUrl + 'produtos';
                }
            });
        });

        $("#deleteLote").hide();
        $("#reajustarLote").hide();

        $('#modalTabelas').on('shown.bs.modal', function() {
            $('#myInput').focus()
        });

        $(".validar").click(function() {
            $.ajax({
                type: "post",
                url: baseUrl + "tabelas/validar/" + $(this).attr(
                    'value'),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });

        $(".invalidar").click(function() {
            $.ajax({
                type: "post",
                url: baseUrl + "tabelas/invalidar/" + $(this).attr(
                    'value'),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });

        $(".colocarAtlz").click(function() {
            $.ajax({
                type: "post",
                url: baseUrl + "tabelas/colocarAtlz/" + $(this).attr(
                    'value'),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });

        $(".removerAtlz").click(function() {
            $.ajax({
                type: "post",
                url: baseUrl + "tabelas/removerAtlz/" + $(this).attr(
                    'value'),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });

        $("#deleteLote").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "tabelas/deleteLote/",
                success: function(data) {
                    $('#operadora-id').trigger('change');

                }
            });
        });

        $("#reajustarLote").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "tabelas/reajusteLote/" + $(
                    "#selecaoTabelas").serialize(),
                success: function(data) {
                    $("#percentual").empty();
                    $("#percentual").append(data);

                }
            });
        });

        $("#reajustarVigencia").click(function() {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "tabelas/reajusteVigencia/" + $(
                    "#selecaoTabelas").serialize(),
                success: function(data) {
                    $("#percentual").empty();
                    $("#percentual").append(data);

                }
            });
        });

        $("#gerarTabela").click(function() {
            $.ajax({
                type: "post",
                data: $('#selecaoTabelas').serialize(),
                url: baseUrl + "tabelas/gerarTabela/" + $('#selecaoTabelas')
                    .serialize(),
                beforeSend: function() {
                    $('#loader').trigger('click');
                },
                success: function(data) {
                    $('#fechar').trigger('click');
                }
            });
        });

        $(".checkHabilita").click(function() {
            $("#deleteLote").show();
            $("#reajustarLote").show();
        });
    </script>