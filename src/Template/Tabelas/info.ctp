<?= $this->Form->create('adicionais', ['id' => 'info']) ?>
<div class="modal-content" id="modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Informações Adicionais da Tabela</h4>
    </div>
    <div class="modal-body">

        <div class="col-md-12">

            <div class="tituloField">
                Carências
            </div>
            <div class="col-md-12 alturaFixa100">
                <?= $this->Form->radio('carencia_id', $carencias); ?>            
            </div>
        </div>
        <div class="col-md-12">

            <div class="tituloField">
                Reembolsos
            </div>
            <div class="col-md-12 alturaFixa100">
                <?= $this->Form->radio('reembolso_id', $reembolsos); ?>            
            </div>
        </div>
        <div class="col-md-12">

            <div class="tituloField">
                Rede Credenciada
            </div>
            <div class="col-md-12 alturaFixa100">
                <?= $this->Form->radio('rede_id', $redes); ?>            
            </div>
        </div>
        <div class="col-md-12">

            <div class="tituloField">
                Opcionais
            </div>
            <div class="col-md-12 alturaFixa100">
                <?= $this->Form->radio('opcional_id', $opcionais); ?>            
            </div>
        </div>
        <div class="col-md-12">

            <div class="tituloField">
                Informações Extras
            </div>
            <div class="col-md-12 alturaFixa100">
                <?= $this->Form->radio('informacao_id', $informacoes); ?>            
            </div>
        </div>
        <div class="col-md-12">

            <div class="tituloField">
                Observações
            </div>
            <div class="col-md-12 alturaFixa100">
                <?= $this->Form->radio('observacao_id', $observacoes); ?>            
            </div>

        </div>



        <?= $this->Form->end() ?>
        <div class="modal-footer" style="border-top:none !important;">

            <div class="col-md-12 centralizada fonteReduzida">
                <?=
                $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Adicionar', '#', ['class' => 'btn btn-sm btn-primary', 'role' => 'button', 'escape' => false, 'id' => 'adicionarDetalhes']);
                ?>
                <?=
                $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', '#', ['class' => 'btn btn-md btn-default', 'data-dismiss' => "modal", 'role' => 'button', 'escape' => false]);
                ?>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    $("#adicionarDetalhes").click(function () {
        $.ajax({
            type: "Post",
//                data: $("#info").serialize(),
            url: "<?php echo $this->request->webroot ?>tabelas/adicionardetalhes/" + $("#info").serialize(),
            success: function (data) {
                $("#detalhes").empty();
                $("#detalhes").append(data);

            }
        });
    });

    $("#salvar").click(function () {
        $.ajax({
            type: "post",
            data: $("#simulacao").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/emitirSimulacao/" + $("#simulacao").serialize(),
            success: function (data) {
                $("#tabelas").empty();
                $("#tabelas").append(data);

            }
        });
    });
    $("#simular").click(function () {
        $.ajax({
            type: "post",
            data: $("#simulacao").serialize(),
            url: "<?php echo $this->request->webroot ?>simulacoes/encontrarTabelas/" + $("#simulacao").serialize(),
            success: function (data) {
                $("#dadosSimulacao").empty();
                $("#dadosSimulacao").append(data);

            }
        });
    });

    $(".botaoSalvar").hide();
</script>
