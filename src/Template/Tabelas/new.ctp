<section id="AppTabelas">
    <div class="row">
        <?php echo $this->Form->create('search', ['id' => 'pesquisarTabela', 'class' => 'form', 'role' => 'form']); ?>
        <div class="col-md-4 fonteReduzida">
            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Nova Tabela', ['action' => 'add', 1], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'sid' => 'tabelas.index']); ?>
        </div>
        <div class="col-md-2 fonteReduzida">
            <div class="form-group ">
                <?= $this->Form->input('estados', ['options' => $estados, 'value' => $estados ? $estado : '', 'class' => 'filtros', 'label' => '', 'empty' => 'Selecione ESTADO']); ?>
            </div>
        </div>
        <div class="col-md-2 fonteReduzida">
            <div class="form-group ">
                <?= $this->Form->input('operadoras', ['type' => 'select', 'options' => $operadoras ? $operadoras : '', 'value' => $operadora ? $operadora : '', 'class' => 'filtros', 'id' => 'operadora-id', 'label' => false, 'empty' => 'Selecione OPERADORA', 'disabled' => $operadoras ? false : true]); ?>
            </div>
        </div>
        <div class="col-md-2 fonteReduzida">
            <div class="form-group ">
                <?= $this->Form->input('comercializacoes', ['type' => 'select', 'options' => isset($comercializacoes) && $comercializacoes ? $comercializacoes : '', 'value' => isset($comercializacao) && $comercializacao ? $comercializacao : '', 'class' => 'filtros', 'id' => 'comercializacoes', 'label' => false, 'empty' => 'Selecione Comercialização', 'disabled' => isset($comercializacoes) && $comercializacoes ? false : true]); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>

    <div class="row">
        <div id="gridCalculo" clas="col-md-12" style="padding: 15px;">
            <form id="selecaoTabelas">
                <?php if (isset($tabelas) && !empty($tabelas)) { ?>
                    <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <td class="tituloTabela centralizada" style="width: 2%"><?= 'Sel.' ?></td>
                                <td class="tituloTabela centralizada"><?= 'Operadora' ?></td>
                                <td class="tituloTabela centralizada" style="width: 17%"><?= 'Produto' ?></td>
                                <td class="tituloTabela centralizada">Tipo Cobertura</td>
                                <td class="tituloTabela centralizada">Contratação</td>
                                <td class="tituloTabela centralizada" style="width: 7%">CNPJ</td>
                                <td class="tituloTabela centralizada" style="width: 5%"><?= 'Vidas' ?></td>
                                <td class="tituloTabela centralizada"><?= 'Coparticipação' ?></td>
                                <td class="tituloTabela centralizada">Acomodação</td>
                                <td class="tituloTabela centralizada" style="width: 6%"><?= 'Vigência' ?></td>
                                <td class="tituloTabela text-center centralizada" style="width: 6%"><?= 'Prioridade' ?></td>
                                <td style="width: 5%" class="tituloTabela centralizada">Status</td>
                                <th class="actions" style="width: max-content;"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($tabelas as $tabela) : ?>
                                <tr>
                                    <td style="width: 2%" class=" centralizada"> <?= $this->Form->input('tabela' . $tabela->id, ['type' => 'checkbox', 'value' => $tabela->id, 'label' => '', 'class' => 'checkHabilita']) ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= !is_null($tabela->areas_comercializaco->operadora->imagen) ? $this->Html->image("../" . $tabela->areas_comercializaco->operadora->imagen->caminho . "/" . $tabela->areas_comercializaco->operadora->imagen->nome, ['class' => 'logoMiniatura']) : $tabela->areas_comercializaco->operadora->nome ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= $tabela->nome ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= $tabela->cobertura->nome ?> </td>
                                    <td style="font-size: 90% !important" class=" centralizada">
                                        <?php

                                        switch ($tabela->tipo_contratacao) {
                                            case 0:
                                                echo "OPCIONAL";
                                                break;
                                            case 1:
                                                echo "COMPULSÓRIO";
                                                break;
                                            case 2:
                                                echo "AMBOS";
                                                break;
                                        }
                                        ?>
                                    </td>
                                    <td style="font-size: 90% !important" class=" centralizada">
                                        <?php
                                        if (!empty($tabela->cnpjs)) {
                                            $tam = count($tabela->cnpjs) - 1;
                                            foreach ($tabela->cnpjs as $key => $cnpj) {
                                                echo $cnpj->nome . (!($key == $tam) ? ", " : "");
                                            }
                                        } ?>
                                    </td>
                                    <td style="font-size: 90% !important" class=" centralizada"> <?= $tabela->minimo_vidas . " a " . $tabela->maximo_vidas ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"> <?= ($tabela->coparticipacao == 'n') ? "S/ Coparticipação" : "C/ Coparticipação " . $tabela->detalhe_coparticipacao ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= h($tabela->tipo->nome) ?></td>
                                    <td style="font-size: 90% !important" class=" centralizada"><?= h($tabela->vigencia) ?></td>
                                    <td class="text-center centralizada" style="font-size: 90% !important"><?= h($tabela->prioridade) ?></td>
                                    <td class="centralizada"> <label style="font-weight: bold;" class="label label-<?= $tabela->new == 1 ? 'success' : 'danger' ?> "><?= $tabela->new == 1 ? 'NOVA' : 'VELHA' ?></label></td>
                                    <td class="actions" style="font-size: 90% !important; text-align: center;">

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm dropdown-toggle <?= $tabela->validade == null || $tabela->validade == 0 ? "btn-danger" : ($tabela->atualizacao != null || $tabela->atualizacao != 0 ? "btn-warning" : "btn-success")  ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-cogs"></i>
                                            </button>
                                            <ul class="dropdown-menu" style="left: auto; right: 0;">
                                                <?php if ($sessao['role'] == 'admin') : ?>
                                                    <li><a href="#"><?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']) . " Editar produto", ['action' => 'edit', $tabela->id, '1'], ['role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar / Preços', 'sid' => 'tabelas.edit']) ?></a></li>
                                                    <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-copy', 'aria-hidden' => 'true']) . " Duplicar Produto", ['action' => 'duplicar', $tabela->id], ['role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Duplicar tabela', 'sid' => 'tabelas.duplicar']) ?></li>
                                                    <li>
                                                        <?php if ($tabela->validade == null || $tabela->validade == 0) {

                                                            echo $this->Html->link(
                                                                $this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . " Produto Suspenso",
                                                                '#',
                                                                [
                                                                    'class' => ' btn-danger validar',
                                                                    'escape' => false,
                                                                    'sid' => 'tabelas.validar',
                                                                    'value' => $tabela->id,
                                                                    'data-toggle' => 'tooltip',
                                                                    'data-placement' => 'top',
                                                                    'title' => 'SUSPENSA'
                                                                ]
                                                            );
                                                        } else {

                                                            echo $this->Html->link(
                                                                $this->Html->tag('span', '', ['class' => 'fa fa-check', 'aria-hidden' => 'true']) . " Produto Ativo",
                                                                '#',
                                                                array(
                                                                    'class' => ' btn-success margemMinima invalidar',
                                                                    'escape' => false,
                                                                    'sid' => 'tabelas.invalidar',
                                                                    'value' => $tabela->id,
                                                                    'data-toggle' => 'tooltip',
                                                                    'data-placement' => 'top',
                                                                    'title' => 'ATIVA'
                                                                )
                                                            );
                                                        }
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link($this->Html->tag('span', '', [
                                                            'class' => 'fa fa-list',
                                                            'aria-hidden' => 'true'
                                                        ]) . " Definir Prioridade", 'javascript:void(0);', [
                                                            'class' => ' btn-default margemMinima alterarPrioridade',
                                                            'escape' => false,
                                                            'value' => $tabela->id,
                                                            'sid' => 'tabelas.prioridade',
                                                            'data-placement' => 'top',
                                                            'title' => 'Definir Prioridade',
                                                            // 'data-toggle' => "modal",
                                                            // 'data-target' => "#modalPrioridade"
                                                        ]);
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        if ($tabela->atualizacao == null || $tabela->atualizacao == 0) :

                                                            echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-refresh']) . " Atualizado", '#', [
                                                                'class' => ' btn-default colocarAtlz',
                                                                'data-toggle' => 'tooltip',
                                                                'data-placement' => 'top',
                                                                'sid' => 'tabelas.colocarAtlz',
                                                                'escape' => false,
                                                                'value' => $tabela->id,
                                                                'title' => 'Colocar em Atualização de Preços'
                                                            ]);
                                                        else :
                                                            echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-refresh']) . " Em Atualização", '#', [
                                                                'class' => ' btn-danger removerAtlz',
                                                                'data-toggle' => 'tooltip',
                                                                'data-placement' => 'top',
                                                                'sid' => 'tabelas.removerAtlz',
                                                                'escape' => false,
                                                                'value' => $tabela->id,
                                                                'title' => 'Remover Atualização de Preços'
                                                            ]);
                                                        endif;
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link(
                                                            $this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']) . " Deletar Produto",
                                                            array(
                                                                'action' => 'delete', $tabela->id
                                                            ),
                                                            array(
                                                                'class' => ' btn-default',
                                                                'escape' => false,
                                                                'data-toggle' => 'tooltip',
                                                                'sid' => 'tabelas.delete',
                                                                'data-placement' => 'top',
                                                                'title' => 'Remover',
                                                                // 'confirm' => 'Confirma exclusão da Tabela?'
                                                            )
                                                        ); ?>
                                                    </li>
                                                    <li style="background: #F1F1F1; font-size: 9px; text-align: center;">Criado em: <?php echo @$tabela->created ?></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="col-md-12">
                        <?=
                        $this->Html->link($this->Html->tag('span', '', [
                            'class' => 'fa fa-trash',
                            'aria-hidden' => 'true'
                        ]) . ' Deletar Produtos', 'javascript:void(0);', [
                            'class' => 'btn btn-sm btn-danger',
                            'role' => 'button',
                            'escape' => false,
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => 'Deletar Produtos',
                            'id' => 'deleteLoteProdutos',
                            'sid' => 'produtos.deleteLote'
                        ])
                        ?>
                    </div>
                    <div class="col-md-12">
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                <?php
                } else {
                ?>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="col-md-12 text-danger centralizada"> NENHUM PRODUTO ENCONTRADO</div>
                <?php
                }
                ?>


                <?= $this->Form->end() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?=
            $this->Html->link($this->Html->tag('span', '', [
                'class' => 'fa fa-trash',
                'aria-hidden' => 'true'
            ]) . ' Deletar Tabelas', 'javascript: void(0);', [
                'class' => 'btn btn-sm btn-danger',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Deletar tabelas',
                'id' => 'deleteLote',
                'sid' => 'tabelas.deleteLote',
                // 'confirm' => 'Tem certeza que deseja apagar essas tabelas?'
            ])
            ?>

            <?=
            $this->Html->link($this->Html->tag('span', '', [
                'class' => 'fa fa-percent',
                'aria-hidden' => 'true'
            ]) . ' Reajustar Tabelas', '#', [
                'class' => 'btn btn-sm btn-default',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Reajustar Tabelas',
                'id' => 'reajustarLote',
                'sid' => 'tabelas.reajusteLote',
                'data-toggle' => "modal",
                'data-target' => "#modalTabelas"
            ])
            ?>

            <?=
            $this->Html->link($this->Html->tag('span', '', [
                'class' => 'fa fa-calendar',
                'aria-hidden' => 'true'
            ]) . ' Alterar Vigência', '#', [
                'class' => 'btn btn-sm btn-default',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Alterar Vigências',
                'id' => 'reajustarVigencia',
                'sid' => 'tabelas.reajusteVigencia',
                'data-toggle' => "modal",
                'data-target' => "#modalTabelas"
            ])
            ?>
            <?= $this->Form->hidden('tabelas'); ?>

            <?=
            $this->Form->button('<i class="fa fa-file-pdf-o"></i> Gerar Tabela', array(
                'type' => 'submit',
                'class' => 'btn btn-sm btn-default',
                'escape' => false
            ));
            ?>

            <div id="pdf"></div>
        </div>
    </div>
    <!-- Modal -->


</section>