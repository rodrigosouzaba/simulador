<style>
    .checkbox {
        padding-right: 5px;
    }
</style>

<?php $this->assign('title', $title); ?>

<?= $this->Form->create($tabela) ?>
<!-- INICIO -->
<div class="col-xs-12">
    <div class="col-md-6 fonteReduzida">
        <?= $this->Form->input('nome', ["style" => "margin-bottom: 0 !important;", 'required' => 'required']); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('cod_ans', ['label' => 'Código ANS', "style" => "margin-bottom: 0 !important;"]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('vigencia', ['label' => 'Válida até', "style" => "margin-bottom: 0 !important;", 'required' => 'required']); ?>
    </div>
    <div class="col-xs-12">&nbsp;</div>
    <div class="col-xs-12 fonteReduzida">
        <?= $this->Form->input('area_comercializacao_id', ['options' => $areas_comercializacoes]) ?>
    </div>
</div>
<div class="col-xs-12">&nbsp;</div>

<div class="col-xs-12">

    <div class="col-md-12 fonteReduzida">
        <?= $this->Form->input('descricao', ["label" => "Observação", 'required' => 'required']); ?>
    </div>
</div>
<div class="col-xs-12">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('minimo_vidas', ['label' => 'Mínimo de Vidas', "required" => "required"]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('maximo_vidas', ['label' => 'Máximo de Vidas', "required" => "required"]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('titulares', ["required" => "required"]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <div class="form-inline">
            <?php
            $ids = array();
            if (!empty($tabela["tabelas_cnpjs"])) {
                foreach ($tabela["tabelas_cnpjs"] as $cnpj_selecionados) {
                    $ids[] = $cnpj_selecionados["cnpj_id"];
                }
            }
            ?>
            <?= $this->Form->input('cnpjs', ['options' => $cnpjs, 'multiple' => 'checkbox', "class" => "form-group", "label" => "Tipos de CNPJ", "style" => "max-height: 70px; min-height: 0 !important", "required" => false]); ?>

        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('tipo_id', ['options' => $tipos, 'label' => 'Tipo de Acomodação', 'empty' => 'SELECIONE', "required" => "required"]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('cobertura_id', ['options' => $coberturas, 'empty' => 'SELECIONE', 'label' => 'Tipo de Cobertura', 'required' => true]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('abrangencia_id', ['options' => $abrangencias, 'label' => 'Área de Utilização', 'empty' => 'SELECIONE', "required" => "required"]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?php $contratacao = array('0' => 'OPCIONAL', '1' => 'COMPULSÓRIO', '2' => 'AMBOS'); ?>
        <?= $this->Form->input('tipo_contratacao', ['options' => $contratacao, 'label' => 'Tipo de Contratação', 'empty' => 'SELECIONE', "required" => "required"]); ?>
    </div>
</div>

<div class="col-xs-12">&nbsp;</div>
<div class="col-xs-12">
    <div class="col-md-3 fonteReduzida">
        <?php $filtroreembolso = array('N' => 'SEM REEMBOLSO', 'S' => 'COM REEMBOLSO'); ?>
        <?= $this->Form->input('reembolso', ['options' => $filtroreembolso, 'label' => 'Possui Reembolso', 'empty' => 'SELECIONE', "required" => "required"]); ?>
    </div>
    <div class="col-md-3 fonteReduzida">
        <?= $this->Form->input('coparticipacao', ['options' => $coparticipacao, 'empty' => 'SELECIONE', 'label' => 'Possui Coparticipação?', "required" => "required"]); ?>
    </div>
    <div class="col-md-3 fonteReduzida" id="detalheCoparticipacao">
        <?= $this->Form->input('detalhe_coparticipacao', ['label' => 'Detalhes Coparticipação. Exemplo ("10%", "FN10", etc..) ', 'style' => 'margin-bottom: 0 !important']); ?>
    </div>
    <div id="odontoValor">
        <div class="col-md-3 fonteReduzida">
            <?= $this->Form->input('odonto_operadora', ['options' => $odonto_operadoras, 'empty' => 'Selecione Operadora']); ?>
        </div>
        <div class="col-md-3 fonteReduzida">
            <label for="odonto_valor" id="label-odonto">Preço Odonto por Vida <b style="color:#C3232D">*</b></label>
            <?= $this->Form->input('odonto_valor', ['label' => false, 'style' => 'margin-bottom: 0 !important', 'step' => '0.01']); ?>
        </div>
    </div>
</div>
<!-- PREÇOS -->

<div class="spacer-xs">&nbsp;</div>
<div id="faixas" class="col-md-12 centralizada fonteReduzida">
    <?php if (!$is_vitalmed) : ?>
        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa1', ['label' => '0 à 18 anos', 'type' => 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa1]); ?>
            <label for="check1">
                <?= $this->Form->checkbox('check1', ["id" => "check1", "class" => "consulta", "campo" => "faixa1", "tabindex" => "1"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa2', ['label' => '19 à 23 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa2]); ?>
            <label for="check2">
                <?= $this->Form->checkbox('check2', ["id" => "check2", "class" => "consulta", "campo" => "faixa2", "tabindex" => "2"]); ?>
                So consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa3', ['label' => '24 à 28 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa3]); ?>
            <label for="check3">
                <?= $this->Form->checkbox('check3', ["id" => "check3", "class" => "consulta", "campo" => "faixa3", "tabindex" => "3"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa4', ['label' => '29 à 33 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa4]); ?>
            <label for="check4">
                <?= $this->Form->checkbox('check4', ["id" => "check4", "class" => "consulta", "campo" => "faixa4", "tabindex" => "4"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa5', ['label' => '34 à 38 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa5]); ?>
            <label for="check5">
                <?= $this->Form->checkbox('check5', ["id" => "check5", "class" => "consulta", "campo" => "faixa5", "tabindex" => "5"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa6', ['label' => '39 à 43 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa6]); ?>
            <label for="check6">
                <?= $this->Form->checkbox('check6', ["id" => "check6", "class" => "consulta", "campo" => "faixa6", "tabindex" => "6"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa7', ['label' => '44 à 48 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa7]); ?>
            <label for="check7">
                <?= $this->Form->checkbox('check7', ["id" => "check7", "class" => "consulta", "campo" => "faixa7", "tabindex" => "7"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa8', ['label' => '49 à 53 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa8]); ?>
            <label for="check8">
                <?= $this->Form->checkbox('check8', ["id" => "check8", "class" => "consulta", "campo" => "faixa8", "tabindex" => "8"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa9', ['label' => '54 à 58 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa9]); ?>
            <label for="check9">
                <?= $this->Form->checkbox('check9', ["id" => "check9", "class" => "consulta", "campo" => "faixa9", "tabindex" => "9"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa10', ['label' => '59 à 64 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa10]); ?>
            <label for="check10">
                <?= $this->Form->checkbox('check10', ["id" => "check10", "class" => "consulta", "campo" => "faixa10", "tabindex" => "10"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa11', ['label' => '65 à 80 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa11]); ?>
            <label for="check11">

                <?= $this->Form->checkbox('check11', ["id" => "check11", "class" => "consulta", "campo" => "faixa11", "tabindex" => "11"]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-1">
            <?= $this->Form->input('faixa12', ['label' => '+ de 81 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required', 'value' => $tabela->faixa12]); ?>
            <label for="check12">
                <?= $this->Form->checkbox('check12', ["id" => "check12", "class" => "consulta", "campo" => "faixa12", "tabindex" => "12"]); ?>
                Sob consulta
            </label>
        </div>
    <?php else : ?>

        <div class="faixaEtaria col-md-2">
            <?= $this->Form->input('faixa_extra_1', ['label' => '0 à 14 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
            <label for="check_extra_1">
                <?= $this->Form->checkbox('check_extra_1', ["id" => "check_extra_1", "class" => "consulta", "campo" => "faixa_extra_1", "tabindex" => "1", "value" => (isset($tabela) ? $tabela->faixa_extra_1 : '')]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-2">
            <?= $this->Form->input('faixa_extra_2', ['label' => '15 à 29 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
            <label for="check_extra_2">
                <?= $this->Form->checkbox('check_extra_2', ["id" => "check_extra_2", "class" => "consulta", "campo" => "faixa_extra_2", "tabindex" => "2", "value" => (isset($tabela) ? $tabela->faixa_extra_2 : '')]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-2">
            <?= $this->Form->input('faixa_extra_3', ['label' => '30 à 39 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
            <label for="check_extra_3">
                <?= $this->Form->checkbox('check_extra_3', ["id" => "check_extra_3", "class" => "consulta", "campo" => "faixa_extra_3", "tabindex" => "3", "value" => (isset($tabela) ? $tabela->faixa_extra_3 : '')]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-2">
            <?= $this->Form->input('faixa_extra_4', ['label' => '40 à 49 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
            <label for="check_extra_4">
                <?= $this->Form->checkbox('check_extra_4', ["id" => "check_extra_4", "class" => "consulta", "campo" => "faixa_extra_4", "tabindex" => "4", "value" => (isset($tabela) ? $tabela->faixa_extra_4 : '')]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-2">
            <?= $this->Form->input('faixa_extra_5', ['label' => '40 à 58 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
            <label for="check_extra_5">
                <?= $this->Form->checkbox('check_extra_5', ["id" => "check_extra_5", "class" => "consulta", "campo" => "faixa_extra_5", "tabindex" => "5", "value" => (isset($tabela) ? $tabela->faixa_extra_5 : '')]); ?>
                Sob consulta
            </label>
        </div>

        <div class="faixaEtaria col-md-2">
            <?= $this->Form->input('faixa_extra_6', ['label' => '+ de 59 anos', 'number', "step" => "0.01", 'class' => 'centralizada', 'required' => 'required']); ?>
            <label for="check_extra_6">
                <?= $this->Form->checkbox('check_extra_6', ["id" => "check_extra_6", "class" => "consulta", "campo" => "faixa_extra_6", "tabindex" => "6", "value" => (isset($tabela) ? $tabela->faixa_extra_6 : '')]); ?>
                Sob consulta
            </label>
        </div>
    <?php endif; ?>
</div>
<!-- PREÇOS -->

<!-- NOVOS RELACIONAMENTOS -->
<div class="col-xs-12 clearfix">&nbsp;</div>
<h4 class="centralizada">Novas Associações</h4>
<div class="col-md-12 fonteReduzida" id="field-associacoes">
    <div class="col-md-3">
        <?= $this->Form->control('rede_id', ['empty' => 'SELECIONE REDE', 'options' => isset($redes) && !empty($redes) ? $redes : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('observacao_id', ['empty' => 'SELECIONE OBSERVACAO', 'options' => isset($observacoes) && !empty($observacoes) ? $observacoes : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('reembolso_id', ['empty' => 'SELECIONE REEMBOLSO', 'options' => isset($reembolsos) && !empty($reembolsos) ? $reembolsos : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('carencia_id', ['empty' => 'SELECIONE CARENCIA', 'options' => isset($carencias) && !empty($carencias) ? $carencias : '']) ?>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-3">
        <?= $this->Form->control('opcional_id', ['empty' => 'SELECIONE DEPENDENTE', 'options' => isset($opcionais) && !empty($opcionais) ? $opcionais : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('informacao_id', ['empty' => 'SELECIONE DOCUMENTO', 'options' => isset($informacoes) && !empty($informacoes) ? $informacoes : '']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('formas_pagamento_id', ['empty' => 'SELECIONE FORMAS PAGAMENTO', 'options' => isset($formas_pagamentos) && !empty($formas_pagamentos) ? $formas_pagamentos : '']) ?>
    </div>
</div>
<!-- NOVOS RELACIONAMENTOS -->
<div class="clearfix">&nbsp;</div>

<?= $this->element('botoesAdd') ?>

<?= $this->Form->end() ?>


<?= $this->element("processando"); ?>

<script type="text/javascript">
    $("#area-comercializacao-id").change(function() {
        var comercializacao = $(this).val();
        $.ajax(`${baseUrl}tabelas/filtroAssociacoes/${comercializacao}`)
            .then(function(data) {
                $("#field-associacoes").empty();
                $("#field-associacoes").append(data);
            })
        $.ajax(`${baseUrl}tabelas/getFaixas/${comercializacao}`)
            .then(function(data) {
                $("#faixas").empty();
                $("#faixas").append(data);
            })
    })

    $("#cobertura-id").change(function() {
        if ($(this).val() == 5 || $(this).val() == 6) {
            $("#odontoValor").show();
            $("#odonto-valor").prop('required', true);
            $("#odonto-operadora").prop('required', true);
        } else {
            $("#odontoValor").hide();
            $("#odonto-valor").removeAttr("required");
            $("#odonto-operadora").removeAttr("required");
        }
    });
    $(document).ready(function() {
        $("#acoes").hide();
        $("#detalheCoparticipacao").hide();
        $("#odontoValor").hide();
        $("#coparticipacao").change(function() {
            //           alert((this).value);
            if ((this).value === 's') {
                $("#detalheCoparticipacao").show();
            } else {
                $("#detalheCoparticipacao").hide();

            }

        });

        var arr = <?= json_encode($ids); ?>;
        $.each(arr, function(index, val) {
            $("#cnpjs-" + val).prop('checked', true);
        });


        $("#estado-id").change(function() {
            $("#acoes").show();
            $("#loader").click();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>tabelas/find_operadoras/" + $(
                    "#estado-id").val(),
                success: function(data) {
                    $("#operadoras").empty();
                    $("#operadoras").append(data);
                    $("#fechar").click();
                }
            });
        });
    });

    $("#tipo-produto-id").change(function() {
        if ($(this).val() == 4 || $(this).val() == 5 || $(this).val() == 6) {
            $("#odontoValor").show();
            $("#odonto-valor").prop('required', true);
            $("#odonto-operadoras").prop('required', true);
        } else {
            $("#odonto-operadoras").removeAttr("required");
            $("#odonto-valor").removeAttr("required");
            $("#odontoValor").hide();
        }
    });
</script>