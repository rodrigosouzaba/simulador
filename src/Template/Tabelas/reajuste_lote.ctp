<?= $this->Form->create('percentualReajuste', ['id' => 'percentualReajuste']); ?>

<div class="col-md-12 fonteReduzida">    
    <?php
    foreach ($tabelas as $tabela) {
        echo $this->Form->input('tabelas_id'.$tabela, ['type' => 'hidden', 'value' => $tabela]);
    }
    ?>
    <?= $this->Form->input('percentual', ['label' => '', 'placeholder' => 'Percentual de Reajuste']); ?>
</div>

<div class="col-md-12 fonteReduzida centralizada">
 <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => "btn btn-primary btn-md ", "id" => "sendReajuste"]) ?>

 <?=
 $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['controller' => 'Tabelas', 'action' => 'index'], ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false]);
 ?>   
</div>


<script type="text/javascript">
    $("#operadora-id").change(function () {

        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>tabelas/busca_tabela/" + $("#operadora-id").val(),
            data: $("#operadora-id").serialize(),
            beforeSend: function () {
                $('#loadingMatriz').html("<img src='<?= $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                $("#tabelas").empty();
                $("#tabelas").append(data);
                $("#loadingMatriz").empty();
            }
        });

    });

</script>


