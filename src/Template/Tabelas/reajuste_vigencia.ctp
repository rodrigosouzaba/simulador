<?= $this->Form->create('percentualReajuste', ['id' => 'percentualReajuste', 'class' => 'form form-validate', 'role' => 'form']); ?>
<?= $this->element('forms/title', ['title' => '<i class="fa fa-edit"></i> Reajustar Tabelas']); ?>
<div class="card-body">
    <div id="res" style="display: none;"></div>
    <div class="row">
        <div class="col-md-12">
            <small> As tabelas serão exibidas nas simulações de acordo com sua <b>Ordem de Prioridade</b>definida</small>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 fonteReduzida">    
            <?php
            foreach ($tabelas as $tabela) {
                echo $this->Form->input('tabelas_id'.$tabela, ['type' => 'hidden', 'value' => $tabela]);
            }
            ?>
            <?= $this->Form->input('vigencia', ['label' => 'Vigência', 'type' => 'date']); ?>
        </div>
    </div>
</div>
<?= $this->element('forms/buttons') ?>
<?= $this->Form->end(); ?>


