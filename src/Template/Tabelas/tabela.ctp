<style type="text/css">
    .jumbotron{
        background-color: #C0C0C0;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 0 30px 0;
        padding-left: 20px;
        line-height: 1.5;
        /*width: 97%;*/
    }
    .fonteReduzida{
        color: #4d4d4d;cursor: pointer;display: block;font-size: 7px !important;line-height: 1.5;margin-bottom: 0;
    }
    .operadora{
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }
    .TituloOperadora{
        background-color: #337ab7;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        line-height: 1.5;
        width: 100%;
    }
    .detalhesOperadora{
        padding: 10px 0px;
        font-weight: bold;
    }
    .larguraPadrao{
        width: 700px !important;
    }
    .tabela{
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }
    .operadoras{

    }

    .tituloField{
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
    }
    .topoTabela{
        padding:5px 0; background-color: #ddd; text-align:center;font-weight:bold;font-size:10px;
    }
    .corpoTabela{
        padding:5px 0;  text-align:center;font-size:10px;
    }

    .fonteReduzida{
        font-size: 9px;
    }
    .negrito{
        font-weight: bold;
    }
    .obs{
        text-align: justify;
    }
    .esquerda{
        float: left;
        margin: 15px;

        font-family: sans-serif;
    }
    .direita{
        float: right;
    }
    .total{
        width: 100%;
    }
    .p15{
        width: 15%;
    }
    .p85{
        width: 85%;
    }
    .totalTabela{
        width: 100% !important; border-collapse:collapse; font-size: 7.8px; text-align: center;background-color: #C0C0C0;
    }
    .t100{
        width: 100%;border-collapse:collapse; font-size: 7.8px; text-align: center;background-color: #C0C0C0;
    }
</style>
<div>&nbsp;</div>
<?php echo $this->element('cabecalho_tabela'); ?>
<?php foreach($tabelasOrdenadas as $tabelasOrdenadas){?>
<table style="width: 100%; border-collapse:collapse;" class="table table-condensed table-striped table-bordered">
	<tr style="width: 100%;">
		<td style="max-width: 50px !important">&nbsp;</td>
		<?php 
			foreach($tabelasOrdenadas as $produto => $acomodacoes){ 
			?>
	    <td class="titulo-tabela centralizada" style="background-color: #000000; color: #ffffff;  margin: 3px; text-align: center;" colspan="<?= array_sum(array_map("count", $acomodacoes))?>">
	        <?= $produto ?>
	    </td>	
	    <?php }?>    
	</tr>
    <tr>
        <td style="max-width: 50px !important">&nbsp;</td>
		<?php 
			foreach($tabelasOrdenadas as $produto => $acomodacoes){ 
				foreach($acomodacoes as $acomodacao => $co){
// 					debug($co);
		?>
		<td class="centralizada titulo-tabela" colspan="<?= count($co,COUNT_RECURSIVE )?>">
    		<?= $acomodacao ?>
    	</td>
    <?php  }
	    } ?>
    </tr>
    <tr>
	    <td style="max-width: 50px !important">&nbsp;</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada titulo-tabela">
					    <?php $c = explode("-",$coparticipacao); ?>
<!-- 					    <?= $coparticipacao?> -->
					    <?= $c[1]?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td style="max-width: 50px !important" class="centralizada">até 18 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa1"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada"  style="max-width: 50px !important">19 a 23 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa2"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada"  style="max-width: 50px !important">24 a 28 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa3"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada"  style="max-width: 50px !important">29 a 33 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa4"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada" style="max-width: 50px !important">34 a 38 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa5"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada" style="max-width: 50px !important">39 a 43 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa6"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada" style="max-width: 50px !important">44 a 48 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa7"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada" style="max-width: 50px !important">49 a 53 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa8"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada" style="max-width: 50px !important">54 a 58 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa9"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada" style="max-width: 50px !important">59 a 64 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa10"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada" style="max-width: 50px !important">65 a 80 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa11"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>
    <tr>
	    <td class="centralizada" style="max-width: 50px !important">+ de 81 anos</td>
	    <?php foreach($tabelasOrdenadas as $a){
		    foreach($a as $co){
			    foreach($co as $coparticipacao => $tabela){ ?>
				    <td class="centralizada">
					    <?= $this->Number->format($tabela["faixa12"], ['before' => 'R$ ', 'places' => 2])?>
				    </td>
			    <?php }
		    }
	    } ?>
    </tr>

</table>
<?php }?>


<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
    FORMAS DE PAGAMENTOS
</div>
<div class="corpo-gerador-tabelas"> 
    <?php
        foreach ($operadora["formas_pagamentos"] as $pgto) {
            echo ($pgto['descricao'] != null) ? nl2br($pgto['descricao']) : "<div class='info'>Consulte Operadora</div>";
}
    ?> 
</div>
<div>&nbsp;</div>
<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
    OBSERVAÇÕES IMPORTANTES
</div>
<div class="corpo-gerador-tabelas"> 
    <?php
// 	    debug($operadora);
        foreach ($operadora["observacoes"] as $obs) {
            echo ($obs['descricao'] != null) ? nl2br($obs['descricao']) : "<div class='info'>Consulte Operadora</div>";
}
    ?> 
</div>
<div>&nbsp;</div>
<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
    DEPENDENTES
</div>
<div class="corpo-gerador-tabelas"> 
    <?php
// 	    debug($operadora);
        foreach ($operadora["opcionais"] as $dependentes) {
            echo ($dependentes['descricao'] != null) ? nl2br($dependentes['descricao']) : "<div class='info'>Consulte Operadora</div>";
}
    ?> 
</div>
<div>&nbsp;</div>
<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
    REDE CREDENCIADA<small> (Resumo)</small>
</div>
<div class="corpo-gerador-tabelas"> 
    <?php
// 	    debug($operadora);
        foreach ($operadora["redes"] as $rede) {
            echo ($rede['descricao'] != null) ? nl2br($rede['descricao']) : "<div class='info'>Consulte Operadora</div>";
}
    ?> 
</div>
<div>&nbsp;</div>
<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
</div>
<div class="corpo-gerador-tabelas"> 
    <?php
// 	    debug($operadora);
        foreach ($operadora["reembolsos"] as $diferenciais) {
            echo ($diferenciais['descricao'] != null) ? nl2br($diferenciais['descricao']) : "<div class='info'>Consulte Operadora</div>";
}
    ?> 
</div>
<div>&nbsp;</div>
<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
    CARÊNCIAS <small>(Resumo)</small>
</div>
<div class="corpo-gerador-tabelas"> 
    <?php
        foreach ($operadora["carencias"] as $carencias) {
            echo ($carencias['descricao'] != null) ? nl2br($carencias['descricao']) : "<div class='info'>Consulte Operadora</div>";
}
    ?> 
</div>
<div>&nbsp;</div>
<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
    ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
</div>
<div class="corpo-gerador-tabelas"> 
    <div> 
	    <b>ÁREA DE COMERCIALIZAÇÃO:</b>
    </div>
    <div >
    <?php
        foreach ($operadora["regioes"] as $comercializacao) {
            echo ($comercializacao['descricao'] != null) ? nl2br($comercializacao['descricao']) : "<div class='info'>Consulte Operadora</div>";
}
    ?> 
    </div>
    <div>&nbsp;</div>
    <div> 
	    <b>ÁREA DE ATENDIMENTO</b>
    </div>
    <?php
	    $dados = array();
	    foreach($tabelas as $tabela){
			$dados[$tabela["produto"]["nome"]] = $tabela["abrangencia"]["descricao"];
    	}
			foreach($dados as $produto => $atendimento){
				echo "<div><b>".$produto.":</b>"." ".$atendimento."</div>";
			}	
		?>	    
    
    
</div>
<div>&nbsp;</div>
<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
    DOCUMENTOS NECESSÁRIOS
</div>
<div class="corpo-gerador-tabelas"> 
    <?php
        foreach ($operadora["informacoes"] as $documentos) {
            echo ($documentos['descricao'] != null) ? nl2br($documentos['descricao']) : "<div class='info'>Consulte Operadora</div>";
}
    ?> 
</div>
<!-- </page> -->
<script type="text/javascript">
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

    </script>
