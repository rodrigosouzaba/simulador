<?= $this->Form->create('buscarGrupo', ['id' => 'buscarGrupo']); ?>
<div class="col-md-4 col-md-offset-2">
    <h4 style="float: left;">Estado </h4> <div id="loadingMatriz" style="float: right;"></div>
    <?= $this->Form->input('estado_id', ['options' => $estados, 'label' => '', 'empty' => 'Selecione o ESTADO', 'disabled' => '', 'id' => 'estado-id']); ?>
</div>
<div class="col-md-4 fonteReduzida">    
    <h4 style="float: left;">Operadora </h4> <div id="loadingMatriz" style="float: right;"></div>
    <div id="operadoras">
        <?= $this->Form->input('operadora_fake', ['options' => '', 'label' => '', 'empty' => 'Selecione a OPERADORA', 'disabled' => 'true']); ?>
    </div>
</div>
<div class="col-md-12 fonteReduzida" id="grupo" style="display: none;">
    <div class="col-xs-12 centralizada">
        <h4>Defina o Grupo de Tabelas</h4>    
    </div>
    <div class="col-xs-2 col-xs-offset-1">
        <?php $contratacao = array('0' => 'OPCIONAL', '1' => 'COMPULSÓRIO', '2' => 'AMBOS') ?>
        <?= $this->Form->input('tipo_contratacao_id', ['options' => $contratacao, 'empty' => 'SELECIONE']); ?>
    </div>    
    <div class="col-xs-2">
        <?= $this->Form->input('tipo_id', ['label' => 'Acomodação', 'empty' => 'SELECIONE']); ?>
    </div>    
    <div class="col-xs-2">
        <?= $this->Form->input('minimo_vidas', ['label' => 'Mínimo de Vidas', 'empty' => 'SELECIONE']); ?>

    </div>    
    <div class="col-xs-2">
        <?= $this->Form->input('maximo_vidas', ['label' => 'Máximo de Vidas', 'empty' => 'SELECIONE']); ?>
    </div>    
    <div class="col-xs-2">
        <?php $co = array('s' => 'SIM', 'n' => 'NÃO') ?>
        <?= $this->Form->input('coparticipacao', ['options' => $co, 'empty' => 'SELECIONE']); ?>
    </div>    
    <div class="col-xs-12 centralizada">
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-search', 'aria-hidden' => 'true']) . ' Buscar Tabelas', '#', ['class' => 'btn btn-md btn-primary ', 'role' => 'button', 'escape' => false, 'id' => 'buscar']);
        ?>
    </div>    
</div>

<?= $this->Form->end() ?>
<div id="tabelas" class="fonteReduzida"></div>

<script type="text/javascript">
    $(document).ready(function(){
    });

    $("#estado-id").change(function(){
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>tabelas/filtro-estado",
            data: {estado: $("#estado-id").val()},
            success: function (data) {
                $("#operadoras").empty();
                $("#operadoras").append(data);
            }
        });
    });

    $("#buscar").click(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>tabelas/busca_tabela",
            data: $("#buscarGrupo").serialize(),
            success: function(data){
                $("#tabelas").empty();
                $("#tabelas").append(data);
            }
        });
    });
</script>


