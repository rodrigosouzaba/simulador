<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tabela'), ['action' => 'edit', $tabela->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tabela'), ['action' => 'delete', $tabela->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabela->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Produtos'), ['controller' => 'Produtos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Produto'), ['controller' => 'Produtos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tabelas view large-9 medium-8 columns content">
    <h3><?= h($tabela->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($tabela->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Produto') ?></th>
            <td><?= $tabela->has('produto') ? $this->Html->link($tabela->produto->id, ['controller' => 'Produtos', 'action' => 'view', $tabela->produto->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Acomodacao') ?></th>
            <td><?= h($tabela->acomodacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tabela->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa1') ?></th>
            <td><?= $this->Number->format($tabela->faixa1) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa2') ?></th>
            <td><?= $this->Number->format($tabela->faixa2) ?></td>
        </tr>
        <tr>
            <th><?= __('Faixa3') ?></th>
            <td><?= $this->Number->format($tabela->faixa3) ?></td>
        </tr>
        <tr>
            <th><?= __('Vigencia') ?></th>
            <td><?= h($tabela->vigencia) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($tabela->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Regioes') ?></h4>
        <?php if (!empty($tabela->regioes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tabela->regioes as $regioes): ?>
            <tr>
                <td><?= h($regioes->id) ?></td>
                <td><?= h($regioes->nome) ?></td>
                <td><?= h($regioes->estado_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Regioes', 'action' => 'view', $regioes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Regioes', 'action' => 'edit', $regioes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Regioes', 'action' => 'delete', $regioes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $regioes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
