<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Tabelas Cnpjs'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cnpjs'), ['controller' => 'Cnpjs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cnpj'), ['controller' => 'Cnpjs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tabelasCnpjs form large-9 medium-8 columns content">
    <?= $this->Form->create($tabelasCnpj) ?>
    <fieldset>
        <legend><?= __('Add Tabelas Cnpj') ?></legend>
        <?php
            echo $this->Form->input('tabela_id', ['options' => $tabelas]);
            echo $this->Form->input('cnpj_id', ['options' => $cnpjs]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
