<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tabelas Cnpj'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cnpjs'), ['controller' => 'Cnpjs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cnpj'), ['controller' => 'Cnpjs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tabelasCnpjs index large-9 medium-8 columns content">
    <h3><?= __('Tabelas Cnpjs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('tabela_id') ?></th>
                <th><?= $this->Paginator->sort('cnpj_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tabelasCnpjs as $tabelasCnpj): ?>
            <tr>
                <td><?= $this->Number->format($tabelasCnpj->id) ?></td>
                <td><?= $tabelasCnpj->has('tabela') ? $this->Html->link($tabelasCnpj->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $tabelasCnpj->tabela->id]) : '' ?></td>
                <td><?= $tabelasCnpj->has('cnpj') ? $this->Html->link($tabelasCnpj->cnpj->id, ['controller' => 'Cnpjs', 'action' => 'view', $tabelasCnpj->cnpj->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tabelasCnpj->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tabelasCnpj->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tabelasCnpj->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasCnpj->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
