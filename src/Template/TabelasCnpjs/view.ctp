<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tabelas Cnpj'), ['action' => 'edit', $tabelasCnpj->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tabelas Cnpj'), ['action' => 'delete', $tabelasCnpj->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasCnpj->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas Cnpjs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabelas Cnpj'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cnpjs'), ['controller' => 'Cnpjs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cnpj'), ['controller' => 'Cnpjs', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tabelasCnpjs view large-9 medium-8 columns content">
    <h3><?= h($tabelasCnpj->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Tabela') ?></th>
            <td><?= $tabelasCnpj->has('tabela') ? $this->Html->link($tabelasCnpj->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $tabelasCnpj->tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Cnpj') ?></th>
            <td><?= $tabelasCnpj->has('cnpj') ? $this->Html->link($tabelasCnpj->cnpj->id, ['controller' => 'Cnpjs', 'action' => 'view', $tabelasCnpj->cnpj->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tabelasCnpj->id) ?></td>
        </tr>
    </table>
</div>
