<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Tabelas Filtradas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Filtros'), ['controller' => 'Filtros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Filtro'), ['controller' => 'Filtros', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tabelasFiltradas form large-9 medium-8 columns content">
    <?= $this->Form->create($tabelasFiltrada) ?>
    <fieldset>
        <legend><?= __('Add Tabelas Filtrada') ?></legend>
        <?php
            echo $this->Form->input('filtro_id', ['options' => $filtros]);
            echo $this->Form->input('tabela_id', ['options' => $tabelas]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
