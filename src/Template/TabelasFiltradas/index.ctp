<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tabelas Filtrada'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Filtros'), ['controller' => 'Filtros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Filtro'), ['controller' => 'Filtros', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tabelasFiltradas index large-9 medium-8 columns content">
    <h3><?= __('Tabelas Filtradas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('filtro_id') ?></th>
                <th><?= $this->Paginator->sort('tabela_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tabelasFiltradas as $tabelasFiltrada): ?>
            <tr>
                <td><?= $this->Number->format($tabelasFiltrada->id) ?></td>
                <td><?= $tabelasFiltrada->has('filtro') ? $this->Html->link($tabelasFiltrada->filtro->id, ['controller' => 'Filtros', 'action' => 'view', $tabelasFiltrada->filtro->id]) : '' ?></td>
                <td><?= $tabelasFiltrada->has('tabela') ? $this->Html->link($tabelasFiltrada->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $tabelasFiltrada->tabela->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tabelasFiltrada->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tabelasFiltrada->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tabelasFiltrada->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasFiltrada->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
