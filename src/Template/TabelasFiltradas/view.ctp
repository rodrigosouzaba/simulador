<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tabelas Filtrada'), ['action' => 'edit', $tabelasFiltrada->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tabelas Filtrada'), ['action' => 'delete', $tabelasFiltrada->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasFiltrada->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas Filtradas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabelas Filtrada'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Filtros'), ['controller' => 'Filtros', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Filtro'), ['controller' => 'Filtros', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tabelasFiltradas view large-9 medium-8 columns content">
    <h3><?= h($tabelasFiltrada->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Filtro') ?></th>
            <td><?= $tabelasFiltrada->has('filtro') ? $this->Html->link($tabelasFiltrada->filtro->id, ['controller' => 'Filtros', 'action' => 'view', $tabelasFiltrada->filtro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tabela') ?></th>
            <td><?= $tabelasFiltrada->has('tabela') ? $this->Html->link($tabelasFiltrada->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $tabelasFiltrada->tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tabelasFiltrada->id) ?></td>
        </tr>
    </table>
</div>
