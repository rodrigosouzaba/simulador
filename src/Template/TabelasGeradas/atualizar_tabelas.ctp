<div class="clearfix">&nbsp</div>
<ul class="" id="">
        <?php foreach ($tabelas_geradas as $key => $tabela_gerada) : ?>
                <?php
                $tabela_gerada = str_replace("_", " ", $tabela_gerada);
                ?>
                <div class="col-xs-12 option">
                        <div class="col-xs-11 link" value="<?= $key ?>"><?= $tabela_gerada ?></div>
                        <a href="#" class="col-xs-1 deletar"><i class="fa fa-trash"></i></a>
                </div>
        <?php endforeach; ?>
</ul>
<script>
        $(".link").click(function() {
                let id = $(this).attr("value");
                if (id != '') {
                        $.ajax({
                                type: "POST",
                                url: baseUrl + "tabelasGeradas/tabelaSalva",
                                data: {
                                        "tabela_gerada_id": id
                                },
                                beforeSend: function() {
                                        $("#loader").click()
                                },
                                success: function(data) {
                                        $("#respostaProdutos").empty();
                                        $("#resposta").empty();
                                        $("#respostaProdutos").append(data);
                                        $("#fechar").click()
                                        $.ajax({
                                                type: "GET",
                                                url: baseUrl + "tabelasGeradas/produtoSalvo/" + id,
                                                crossOrigin: true,
                                                success: function(data) {
                                                        var result = Object.keys(data).map(function(key) {
                                                                return [data[key]];
                                                        });
                                                        result.forEach(function(salvo) {
                                                                $(".produto").each(function() {
                                                                        if ($(this).val() == salvo) {
                                                                                $("#produtos-" + salvo).click();
                                                                                $(".modal-backdrop").hide();
                                                                        }
                                                                });
                                                        });
                                                }
                                        });
                                        $("#modalTabelasSalvas").modal('hide');
                                }
                        });
                        $.ajax({
                                type: "GET",
                                url: baseUrl + "tabelasGeradas/optionsSalvas/" + id,
                                success: function(data) {

                                        //PREENCHIMENTO DE RAMOS
                                        $("#ramo-id").empty();
                                        $("#ramo-id").append("<option value=''>SELECIONE RAMO</option>");
                                        $.each(data['ramos'], function(key, value) {
                                                if (key == data['selecteds']['ramo']) {
                                                        $("#ramo-id").append("<option value='" + key + "' selected>" + value + "</option>");
                                                } else {
                                                        $("#ramo-id").append("<option value='" + key + "'>" + value + "</option>");
                                                }
                                        });

                                        //PREENCHIMENTO DE OPERADORAS
                                        $("#operadora-id").empty();
                                        $("#operadora-id").append("<option value=''>SELECIONE OPERADORA</option>");
                                        $.each(data['operadoras'], function(key, value) {
                                                if (key == data['selecteds']['operadora']) {
                                                        $("#operadora-id").append("<option value=" + key + " selected>" + value + "</option>");
                                                } else {
                                                        $("#operadora-id").append("<option value=" + key + ">" + value + "</option>");
                                                }
                                        });
                                        $("#operadora-id").attr("disabled", false);

                                        //PREENCHIMENTO DE COPARTICIPACAO
                                        $("#coparticipacao").empty();
                                        $("#coparticipacao").append("<option value=''>COPARTICIPACAO</option>");
                                        $.each(data['coparticipacao'], function(key, value) {
                                                if (key == data['selecteds']['coparticipacao']) {
                                                        $("#coparticipacao").append("<option value=" + key + " selected>" + value + "</option>");
                                                } else {
                                                        $("#coparticipacao").append("<option value=" + key + ">" + value + "</option>");
                                                }
                                        });
                                        $("#coparticipacao").attr("disabled", false);

                                        //MOSTRA PROFISSÕES ESCONDE VIDAS
                                        if (data['selecteds']['ramo'] == "SPF" || data['selecteds']['ramo'] == "OPF") {
                                                $("#ocupacoes").show();
                                                $("#vidaspme").hide();
                                                $("#profissao-id").val(data['selecteds']['profissao_id']);
                                        } else {
                                                $("#ocupacoes").hide();
                                                $("#vidaspme").show();
                                        }
                                        //CASO HAJA VIDA MOSTRA VIDA
                                        if (data['vidas'] != 'sem') {
                                                //CRIAÇÃO DO ELEMENTO
                                                $("#vidaspme").show();
                                                $("#vidaspme").empty();
                                                $('<select>', {
                                                        id: 'vidas',
                                                        name: "vidas",
                                                        class: 'fonteReduzida'
                                                }).appendTo('#vidaspme');

                                                //ADIÇÃO DE EVENTO
                                                $("#vidas").change(function() {
                                                        $("#loader").click();
                                                        $.ajax({
                                                                type: "POST",
                                                                url: baseUrl + "tabelasGeradas/geradorProdutos/",
                                                                data: $("#gerador").serialize(),
                                                                success: function(data) {
                                                                        $("#resposta").empty();
                                                                        $("#respostaProdutos").empty();
                                                                        $("#respostaProdutos").append(data);
                                                                        $("#fechar").click();

                                                                }
                                                        });
                                                });
                                                //PREENCHIMENTO DE COPARTICIPACAO
                                                $("#vidas").empty();
                                                $("#vidas").append("<option value=''>NÚMERO DE VIDAS</option>");
                                                $.each(data['vidas'], function(key, value) {
                                                        if (data['selecteds']['vidas'] == key) {
                                                                $("#vidas").append("<option value=" + key + " selected>" + value + "</option>");
                                                        } else {
                                                                $("#vidas").append("<option value=" + key + ">" + value + "</option>");
                                                        }
                                                });
                                                $("#vidas").attr("disabled", false);
                                        }
                                }
                        });
                }
        });
        $(".deletar").click(function() {
                let id = $(this).siblings(".link").attr("value");
                $.ajax({
                        type: "POST",
                        url: baseUrl + "tabelasGeradas/delete/" + id,
                        beforeSend: function() {
                                $("#loader").click()
                        },
                        success: function(data) {
                                $.ajax({
                                        type: "POST",
                                        url: baseUrl + "tabelasGeradas/atualizarTabelas/",
                                        success: function(data) {
                                                $("#corpo-modal").empty();
                                                $("#corpo-modal").append(data);
                                                $("#modalProcessando").modal("hide");
                                        }
                                });
                        }
                });
        });
</script>