<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<div class="clearfix">&nbsp;</div>

<?= $this->Form->create('editarPercentual', ['id' => 'editarPercentual']); ?>


<?php if ($tabelas) { ?> 
    <div class="col-md-4">
        <?= $this->Form->input('percentual', ['placeholder' => 'Percentual de Reajuste', 'label' => 'Percentual de Reajuste']); ?>
        <div class="well well-sm">

            <?= $this->Form->input('faixa2', ['label' => 'Variação Faixa 2', 'placeholder' => 'Variação relativa a Faixa 1']); ?>
            <?= $this->Form->input('faixa3', ['label' => 'Variação Faixa 3', 'placeholder' => 'Variação relativa a Faixa 2']); ?>
            <?= $this->Form->input('faixa4', ['label' => 'Variação Faixa 4', 'placeholder' => 'Variação relativa a Faixa 3']); ?>
            <?= $this->Form->input('faixa5', ['label' => 'Variação Faixa 5', 'placeholder' => 'Variação relativa a Faixa 4']); ?>
            <?= $this->Form->input('faixa6', ['label' => 'Variação Faixa 6', 'placeholder' => 'Variação relativa a Faixa 5']); ?>
            <?= $this->Form->input('faixa7', ['label' => 'Variação Faixa 7', 'placeholder' => 'Variação relativa a Faixa 6']); ?>
            <?= $this->Form->input('faixa8', ['label' => 'Variação Faixa 8', 'placeholder' => 'Variação relativa a Faixa 7']); ?>
            <?= $this->Form->input('faixa9', ['label' => 'Variação Faixa 9', 'placeholder' => 'Variação relativa a Faixa 8']); ?>
            <?= $this->Form->input('faixa10', ['label' => 'Variação Faixa 10', 'placeholder' => 'Variação relativa a Faixa 9']); ?>
            <?= $this->Form->input('faixa11', ['label' => 'Variação Faixa 11', 'placeholder' => 'Variação relativa a Faixa 10']); ?>
            <?= $this->Form->input('faixa12', ['label' => 'Variação Faixa 12', 'placeholder' => 'Variação relativa a Faixa 11']); ?>

        </div>
        <span id="helpBlock" class="help-block">Não há necessidade de digitar (%)</span>
    </div>
    <div class="col-md-4">
        <h6>
            <b>Tabelas Secundárias</b>    
        </h6>
        <?php
        foreach ($tabelas as $id => $nometabela) {
            echo $this->Form->checkbox('Tabelas[' . $nometabela . ']', ['label' => $nometabela, 'value' => $id, 'hiddenField' => 'false']) . " " . $nometabela . "<br/>";
        }
        ?>
    </div>
    <div class="col-md-4">
    </div>
    <div class="col-md-12 centralizada">
        <div class="clearfix">&nbsp;</div>
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar', '#', ['class' => 'btn btn-md btn-primary ', 'role' => 'button', 'escape' => false, 'id' => 'editar', 'type' => 'submit']);
        ?>
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['action' => 'index'], ['class' => 'btn btn-md btn-default ', 'role' => 'button', 'escape' => false]);
        ?>
    </div>
    <?php
} else {
    ?>
    <div class="clearfix">&nbsp;</div>
    <p class="bg-danger centralizada">Nenhuma tabela encontrada</p>
<?php } ?>


<?= $this->Form->end() ?>
<div id="aaaa"></div>

<script type="text/javascript">
    $("#tabelassecundaria-id").change(function () {
        if ($("#tabelassecundaria-id").val() == $("#tabelamatriz-id").val()) {
            alert('Tabelas devem ser diferentes.');
            $("#percentual").prop('disabled', true);
        } else {
            $("#percentual").removeAttr('disabled');
        }

    });

    $("#editar").click(function () {
        $.ajax({
            type: "POST",
            url: baseUrl + "tabelas/editarPercentual",
            data: $("#editarPercentual").serialize(),
            success: function (data) {
                alert('Atualizado com sucesso!');
            }
        });

    });

</script>