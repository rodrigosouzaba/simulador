<?= $this->Form->input('coparticipacao', ['options' => $coparticipacao, "label" => false, "empty" => "TIPO DE COPARTICIPAÇÃO", "class" => "fonteReduzida"]); ?>

<script type="text/javascript">
    $("#coparticipacao").change(function() {
        $("#resposta").empty();
        $("#respostaProdutos").empty();
        $("#respostaAreasComercializacoes").empty();
        if ($("#ramo-id").val() == 'SPJ' || $("#ramo-id").val() == 'OPJ' || $("#ramo-id").val() == 'OPF') {
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelasGeradas/geradorVidas/",
                data: $("#gerador").serialize(),
                success: function(data) {
                    $("#vidaspme").empty();
                    $("#vidaspme").append(data);
                    $("#fechar").click();
                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelasGeradas/filterAreasComercializacoes/",
                data: $("#gerador").serialize(),
                success: function(data) {
                    $("#respostaAreasComercializacoes").append(data);
                    $("#respostaAreasComercializacoes").removeClass('hide');
                }
            });
        }
    });
</script>