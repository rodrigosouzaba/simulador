<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tabelasGerada->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasGerada->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tabelas Geradas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tabelasGeradas form large-9 medium-8 columns content">
    <?= $this->Form->create($tabelasGerada) ?>
    <fieldset>
        <legend><?= __('Edit Tabelas Gerada') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('estado_id', ['options' => $estados]);
            echo $this->Form->input('ramo');
            echo $this->Form->input('operadora');
            echo $this->Form->input('vidas');
            echo $this->Form->input('coparticipacao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
