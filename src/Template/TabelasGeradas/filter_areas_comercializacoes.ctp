<legend>Areas de Comercialização</legend>
<?php foreach ($areas_comercializacoes as $id => $area) :
    echo $this->Form->input('area_comercializacaos[' . $id . ']', ['value' => $id, 'label' => $area, "type" => "checkbox", 'class' => 'area-comercializacao fonteReduzida']);
endforeach; ?>
<script>
    var ramo = $("#ramo-id").val();
    $(".area-comercializacao").click(function() {
        $("#respostaProdutos").addClass('hide');
        $.ajax({
            type: "POST",
            url: baseUrl + "tabelasGeradas/geradorProdutos/",
            data: $("#gerador").serialize(),
            success: function(data) {
                $("#resposta").empty();
                $("#respostaProdutos").empty();
                $("#respostaProdutos").append(data);
                $("#respostaProdutos").removeClass('hide');
            }
        });
    });
</script>