<?= $this->Form->control('contratacao_id', ['options' => $contratacao, 'empty' => 'CONTRATACAO', 'label' => false]) ?>

<script>
    $("#contratacao-id").change(function() {
        $("#coparticipacao").attr('disabled', true);
        $("#vidas").attr('disabled', true);
        $("#respostaProdutos").addClass('hide');
        $.ajax({
            type: "POST",
            url: baseUrl + "tabelasGeradas/coparticipacao/",
            data: $("#gerador").serialize(),
            success: function(data) {
                $("#resposta").empty();
                $("#respostaProdutos").empty();
                $("#coparticipacao-combo").empty();
                $("#coparticipacao-combo").append(data);
            }
        });
    });
</script>