<?= $this->Form->input('operadora_id', ['options' => $operadoras, "label" => false, "empty" => "OPERADORA", "class" => "fonteReduzida selectpicker", "data-dropup-auto" => "false"]); ?>
<script type="text/javascript">
	$("#operadora-id").change(function() {
		$("#vidas").val("");
		$("#vidas").prop("disabled", true);
		$("#coparticipacao").prop("disabled", true);
		$("#contratacao-id").prop("disabled", true);
		$("#2etapa").hide();
		$("#respostaProdutos").addClass('hide');
		$("#resposta").empty();
		if ($("#ramo-id").val() == 'SPJ') {
			$.ajax({
				type: "POST",
				url: baseUrl + "tabelasGeradas/filterContratacao/",
				data: $("#gerador").serialize(),
				success: function(data) {
					$("#contratacao").empty();
					$("#contratacao").append(data);
				}
			});
		} else if ($("#ramo-id").val() == 'SPF') {
			$.ajax({
				type: "POST",
				url: baseUrl + "tabelasGeradas/coparticipacao/",
				data: $("#gerador").serialize(),
				success: function(data) {
					$("#coparticipacao-combo").empty();
					$("#coparticipacao-combo").append(data);
				}
			});
		} else {
			$("#coparticipacao-combo").addClass('hide');
			$.ajax({
				type: "POST",
				url: baseUrl + "tabelasGeradas/geradorVidas/",
				data: $("#gerador").serialize(),
				success: function(data) {
					$("#vidaspme").empty();
					$("#vidaspme").append(data);
				}
			});
		}
	});
</script>