<?= $this->Form->input('ramo_id', ['options' => $ramos, "label" => false, "empty" => "RAMO", "class" => "fonteReduzida"]); ?>
<script type="text/javascript">
	$("#ramo-id").change(function() {
		if ($(this).val() != '') {
			$("#coparticipacao").val("");
			$("#coparticipacao").prop("disabled", true);
			$("#area-comercializacao-id").prop("disabled", true);
			$("#contratacao-id").prop("disabled", true);
			$("#produto-id").val("");
			$("#produto-id").prop("disabled", true);
			$("#vidas").val("");
			$("#vidas").prop("disabled", true);
			$("#2etapa").hide();
			$("#resposta").empty();
			$("#loader").click();
			if ($("#ramo-id").val() == 'SPJ') {
				$("#areas_comercializacoes").removeClass("hide");
				$("#contratacao").removeClass("hide");
			} else if ($("#ramo-id").val() == 'SPF') {
				$("#areas_comercializacoes").removeClass("hide");
				$("#contratacao").addClass("hide");
			} else {
				$("#areas_comercializacoes").addClass("hide");
				$("#contratacao").addClass("hide");
			}

			if ($("#ramo-id").val() == 'SPF') {
				$("#vidaspme").hide();
				$("#ocupacoes").show();
				$("#profissao-id").prop("disabled", false);
				$("#operadora-id").prop("disabled", true);
				$("#fechar").click();

				$("#profissao-id").change(function() {
					$("#coparticipacao").prop("disabled", true);
					$("#areas_comercializacoes").prop("disabled", true);
					$("#respostaProdutos").addClass("hide");
					console.log($("#coparticipacao").prop("disabled", true));
					$("#loader").click();
					$.ajax({
						type: "POST",
						url: baseUrl + "tabelasGeradas/findPfOperadoras/" + $("#estado-id").val() + "/" + $("#profissao-id").val(),
						success: function(data) {
							$("#op").empty();
							$("#op").append(data);
							$("#operadora-id").prop("disabled", false);
							$("#fechar").click();
						}
					});
				});


			} else {
				$("#ocupacoes").hide();
				$("#vidaspme").show();
				$.ajax({
					type: "POST",
					url: baseUrl + "tabelasGeradas/findOperadoras/" + $("#ramo-id").val() + "/" + $("#estado-id").val(),
					success: function(data) {
						$("#op").empty();
						$("#op").append(data);
						$("#operadora-id").prop("disabled", false);
					}
				});
			}
			$("#fechar").click();
		}
	});
</script>