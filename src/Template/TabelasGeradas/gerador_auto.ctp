<style>
	#fundo{
		background-color: #fff;
		opacity: 0.5;
		width: 100%;
		height:100%;
		z-index: 9998;
		position: absolute;
		display: none;
		top: 0;
		left: 0;
	}	
</style>
<?= $this->element("processandofull");?>
<?= $this->Form->create("gerador", ["id" => "gerador", 'url' => ['action' => 'pdf']]) ?>
<div id="fundo">
</div>
	<div class="clearfix">
		&nbsp;
	</div>
	<div class="col-xs-12 centralizada">
		<h4 style="text-transform: uppercase; color: #004057;"><b>Gerador de Tabelas de Preços Saúde e Odonto</b></h4>
		
		<h5 style="margin: 15px 0 !important;" class="centralizada col-xs-12"><b>SELECIONE AS CARACTERÍSTICAS DA TABELA</b></h5>
	</div>
	<div class="col-xs-12">
		<div class="col-xs-12 col-md-2 col-md-offset-1">
			<?= $this->Form->input('estado_id', ['options' => $estados,"label" => false, "empty" => "ESCOLHA O ESTADO", "class" => "fonteReduzida", "data-dropup-auto" => "false"]); ?>
		</div>
		<div class="col-xs-12 col-md-2" id="ramosdisponiveis">
		<?php $ramos = array("SPF" => "SAÚDE - PESSOA FÍSICA", "SPJ" => "SAÚDE - PME", "OPF" => "ODONTO - PESSOA FÍSICA", "OPJ" => "ODONTO - PME")?>
			<?= $this->Form->input('ramo_id', ['options' => $ramos,"label" => false, "empty" => "ESCOLHA O RAMO", "class" => "fonteReduzida"]); ?>
	
		</div>
	    <div class="col-xs-12 col-md-2" id="ocupacoes">
	    	<?=$this->Form->input("profissao_id", ["options" => $profissoes, "empty" => "PROFISSÃO", "label" => false, "class" => "fonteReduzida"]); ?>
	    </div>
		<div class="col-xs-12 col-md-2" id="op">
			<?= $this->Form->input('operadora_id', ['options' => "","label" => false, "empty" => "OPERADORA", "disabled" => "disabled", "class" => "fonteReduzida","class" => "fonteReduzida selectpicker", "data-dropup-auto" => "false"]); ?>
		</div>
		<div class="col-xs-12 col-md-2" id="coparticipacao-combo">
			<?= $this->Form->input('coparticipacao', ['options' => '',"label" => false, "empty" => "TIPO DE COPARTICIPAÇÃO", "disabled" => "disabled", "class" => "fonteReduzida"]); ?>
		</div>
		<div class="col-xs-12 col-md-2" id="vidaspme">
			<?= $this->Form->input('produto_id', ['options' => "","label" => false, "empty" => "NÚMERO VIDAS", "disabled" => "disabled", "class" => "fonteReduzida"]); ?>
		</div>
	
	
		<div class="col-xs-12 col-md-10 col-md-offset-1" id="respostaProdutos">
			
		</div>
		<div class="col-xs-12" style="height: 10px !important"> 
			<?= "&nbsp;" ?>
		</div>

	
	<div class="col-xs-12" id="resposta">
	</div>
	<div class="col-xs-12" id="ss">
	</div>
		
			<?= $this->Form->end() ?>
	
	</div>

<?= $this->element("processando");?>
<script type="text/javascript">
	
$(document).ready(function() {
    $("#ocupacoes").hide();	
	$("#fundo").hide();
	$("#resposta").on("contextmenu",function(e){
	   return false;
	}); 
	$('#resposta').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
}); 
	
	if ($(window).width() < 960) {
// 		alert("AQUI");
		$("#tabela-gerada-id").width("80%");
		$("#tabela-gerada-id").css("margin-left" ,"10%");
	}
	
	$("#fieldShareEmail").hide();
	$("#shareEmail").click(function () {
		$("#fieldShareEmail").show();
	});
	$("#ramo-id").attr('disabled', true);
	$("#ramo-id option[value='OPE']").attr('disabled', true);

$("#estado-id").change(function () {
	$("#ramo-id").removeAttr("disabled");
	$("#operadora-id").val("");
	$("#operadora-id").prop("disabled", true);
	$("#coparticipacao").val("");
	$("#coparticipacao").prop("disabled", true);
	$("#produto-id").val("");
	$("#produto-id").prop("disabled", true);
	$("#vidas").val("");
	$("#vidas").prop("disabled", true);
	$("#2etapa").hide();
	$("#resposta").empty();
	$("#loader").click();

	$.ajax({
       type: "POST",
       url: baseUrl + "tabelasGeradas/findRamos/",
	   data: $("#estado-id").serialize(),
       success: function (data) {
           $("#ramosdisponiveis").empty();
           $("#ramosdisponiveis").append(data);
		   $("#fechar").click();
       }
   });
});

$("#produto-id").change(function () {
	$("#coparticipacao").val("");
	$("#coparticipacao").prop("disabled", false);
});
$("#coparticipacao").change(function () {
	$("#gerarTabela").removeAttr("disabled");
});

$("#gerarTabela").click(function () {
	$.ajax({
       type: "POST",
       url: baseUrl + "tabelasGeradas/tabela/",
       data: $("#gerador").serialize(),
       success: function (data) {
           $("#resposta").empty();
           $("#resposta").append(data);
		   $("#gerarPDF").removeAttr("disabled");

       }
   });
});


$("#tabela-gerada-id").change(function () {
	if($(this).val() != ''){
	$("#loaderfull").click();
// 		$("#fundo").show();
	$(".modal-backdrop.in").css("opacity", "1");

// 	$("#loaderfull").css("display", "block");
	$.ajax({
       type: "POST",
       url: baseUrl + "tabelasGeradas/view/",
       data: $("#tabela-gerada-id").serialize(),
       success: function (data) {
           $("#resposta").empty();
           $("#resposta").append(data);
           setTimeout(
            function() {
	    		$("#fecharfull").click();	
				$(".modal-backdrop").hide();
		    },
            6000);
       }
   });
   }
});
	   
	   
	   
$("#coparticipacao").change(function () {
	$("#resposta").empty();

	$("#loader").click();
   if($("#ramo-id").val() == 'SPJ' || $("#ramo-id").val() == 'OPJ'){
	   $.ajax({
	       type: "POST",
	       url: baseUrl + "tabelasGeradas/geradorVidas/"+$("#ramo-id").val()+"/"+$("#operadora-id").val(),
	       data: $("#estado-id").serialize(),
	       success: function (data) {
	           $("#vidaspme").empty();
	           $("#vidaspme").append(data);
	           $("#fechar").click();	           
	       }
	   });
	   
   }else{
	   $.ajax({
	       type: "POST",
	       url: baseUrl + "tabelasGeradas/geradorProdutos/",
	       data: $("#gerador").serialize(),
	       success: function (data) {
	           $("#respostaProdutos").empty();
	           $("#respostaProdutos").append(data);
	           $("#fechar").click();	           

	       }
	   });
   }
});

</script>