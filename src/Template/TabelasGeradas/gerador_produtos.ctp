<style>
    .icones-btn {
        padding: 0;
        border: 0;
        padding: 0;
    }

    .icones-btn:hover {
        padding: 0;
        border: 0;
        padding: 0;
    }

    .icones-btn img {
        width: 35px;
    }
</style>
<?php
if (isset($tabelaSalva)) {
    echo $this->Form->input('estado_id', ['value' => $tabelaSalva['estado_id'], 'type' => 'hidden']);
    echo $this->Form->input('vidas', ['value' => $tabelaSalva['vidas'], 'type' => 'hidden']);
    echo $this->Form->input('ramo_id', ['value' => $tabelaSalva['ramo'], 'type' => 'hidden']);
    echo $this->Form->input('coparticipacao', ['value' => $tabelaSalva['coparticipacao'], 'type' => 'hidden']);
    echo $this->Form->input('operadora_id', ['value' => $tabelaSalva['operadora'], 'type' => 'hidden']);
    echo $this->Form->input('profissao_id', ['value' => $tabelaSalva['profissao_id'], 'type' => 'hidden']);
}
?>
<div id="2etapa">
    <legend>Produtos</legend>
    <div class="col-xs-12" style="margin: 5px 0; padding-bottom: 10px;">
        <?php if (!empty($produtos)) : ?>
            <div class="col-xs-12 col-md-2" id="marcarTodos">
                <?= $this->Form->input("Marcar Todos", ['value' => 0, "type" => "checkbox", "class" => "fonteReduzida", "id" => "todos", "unchecked" => "unchecked"]) ?>
            </div>
            <?php foreach ($produtos as $id => $produto) { ?>
                <div class="col-md-2 col-xs-12">
                    <?= $this->Form->input("Produtos[" . $id . "]", ['value' => $id, "type" => "checkbox",  "label" => $produto, "class" => "fonteReduzida produto"]) ?>
                </div>
            <?php } ?>
        <?php else : ?>
            <div class="alert alert-danger">
                <span class="text-danger">Nenhum produto encontrado!</span>
            </div>
        <?php endif; ?>
    </div>
</div>

<?= $this->element('modal-login') ?>

<!-- Modal -->
<div class="modal fade" id="modalShare" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body clearfix">
                <div class="col-xs-12">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <?php if (isset($sessao) && $sessao != null) {
                ?>

                    <div class="col-xs-12 col-md-6 centralizada">
                        <?= $this->Form->button($this->Html->tag('span', '', ['class' => 'fa fa-envelope fa-2x', 'aria-hidden' => 'true']) . "<br>Enviar para o meu E-mail", ['class' => ' btn btn-lg btn-block btn-default', 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false, 'id' => 'pdfemail']); ?>
                        <small><?= $sessao["email"] ?></small>

                    </div>
                    <div class="col-xs-12 col-md-6 centralizada">
                        <?=
                        $this->Html->link($this->Html->tag('span', '', [
                            'class' => 'fas fa-share fa-2x',
                            'aria-hidden' => 'true'
                        ]) . '<br>Enviar para <u>outro</u> E-mail', "#", [
                            'class' => 'btn btn-block btn-lg btn-default',
                            'role' => 'button',
                            'escape' => false,
                            'style' => 'border: none !important',
                            "id" => "outroemail"
                        ]); ?>

                    </div>
                    <div class="col-xs-12">&nbsp;</div>
                    <div class="col-xs-12 centralizada" id="fieldShareEmail">
                        <?= $this->Form->input('email-cliente', ["label" => false, "placeholder" => "Digite o e-mail", "class" => "centralizada"]); ?>
                        <?= $this->Form->button("Enviar", ['class' => ' btn btn-md btn-primary', "id" => "pdfoutroemail", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false,]); ?>
                    <?php
                } else {
                    ?>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12 centralizada" id="fieldShareEmail2">
                            <?= $this->Form->input('email-cliente', ["label" => false, "placeholder" => "Digite o e-mail", "class" => "centralizada"]); ?>
                            <?= $this->Form->button("Enviar", ['class' => ' btn btn-md btn-primary', "id" => "pdfoutroemail2", 'style' => 'display: inline !important;border: none !important', 'role' => 'button', 'escape' => false,]); ?>
                        <?php
                    } ?>


                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>

<!-- MODAL DE DIÁLOGO -->
<div class="modal fade" id="modalDialogo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body centralizada" style="padding-top: 0px;" id="corpo-dialogo">
                <h4>Pronto!</h4>
                <h4>Acesse suas tabelas no campo Tabelas Favoritas.</h4>
            </div>
        </div>
    </div>
</div>
<!-- FIM MODAL DE DIÁLOGO -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#acoesgerador").css("width", $(window).width() + "px !important");
        $("[data-tt=tooltip]").tooltip();
    });

    $("#nome-tabela").hide();

    //PREENCHENDO OS INPUTS PARA INFORMAR O USUÁRIO
    var tabelaSalva = $("#respostaProdutos").children();
    var tabelaSalva = Object.keys(tabelaSalva).map(function(key) {
        return [Number(key), tabelaSalva[key]];
    });

    $(function() {
        $('[data-tooltip="true"]').tooltip();
    });


    $("#exibir-nome").click(function() {
        $("#loader").click();
        $.ajax({
            type: "POST",
            url: baseUrl + "links/add/",
            data: $("#gerador").serialize(),
            success: function(data) {
                $("#fechar").click();
            }
        });
    });


    $(".produto").click(function() {
        var a = $('input:checkbox').filter(':checked');

        if (a.length >= 1) {
            $("#acoesgerador").show();
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelasGeradas/tabela/",
                data: $("#gerador").serialize(),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $("#resposta").empty();
                    $("#resposta").append(data);
                    $("#fechar").click();
                }
            });
        } else {
            $("#resposta").empty();
            $("#acoesgerador").hide();
        }

    });

    $("#todos").click(function() {
        $("#loader").trigger("click");
        if ($(this).prop("checked") == true) {
            $('.produto').each(function(i, that) {
                $(that).prop('checked', true);
            });
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelasGeradas/tabela/",
                data: $("#gerador").serialize(),
                beforeSend: function() {
                    $("#loader").click();
                },
                success: function(data) {
                    $("#resposta").empty();
                    $("#resposta").append(data);
                    $("#acoesgerador").show();
                    $("#fechar").click();
                }
            });
        } else {
            $("#resposta").empty();
            $("#acoesgerador").hide();
            $('.produto').each(function(i, that) {
                $(that).prop('checked', false);
            });

        }
        $("#fechar").click();
    });
</script>