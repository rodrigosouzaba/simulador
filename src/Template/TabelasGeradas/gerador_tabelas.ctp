<style>
	#fundo {
		background-color: #fff;
		opacity: 0.5;
		width: 100%;
		height: 100%;
		z-index: 9998;
		position: absolute;
		display: none;
		top: 0;
		left: 0;
	}
</style>

<section id="AppOdontos">
	<div class="col-xs-12 text-center sub" style="margin-bottom: 6px;">

		<div class="modulo">Configurar Tabelas</div>
		<div class="btn-group centralizada" role="group">
			<?= $this->Html->link(__('Configurar Tabelas'), '/tabelasGeradas/geradorTabelas', ["class" => "btn btn-sm btn-default", "type" => "button", 'sid' => 'tabelasGeradas.geradorTabelas']) ?>
			<?= $this->Html->link(__('Links de Tabelas'), '/links', ["class" => "btn btn-sm btn-default", "type" => "button", 'sid' => 'links.index']) ?>
		</div>
	</div>
	<div class="clearfix">&nbsp</div>
</section>
<?= $this->element('gerador') ?>