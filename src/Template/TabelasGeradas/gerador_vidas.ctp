<?= $this->Form->input('vidas', ['options' => $options, "label" => false, "empty" => "NÚMERO VIDAS", "class" => "fonteReduzida", 'value' => '']); ?>
<script type="text/javascript">
    $("#vidas").change(function() {
        $("#resposta").empty();
        $.ajax({
            type: "POST",
            url: baseUrl + "tabelasGeradas/filterAreasComercializacoes/",
            data: $("#gerador").serialize(),
            success: function(data) {
                $("#respostaAreasComercializacoes").removeClass('hide');
                $("#respostaAreasComercializacoes").empty();
                $("#respostaAreasComercializacoes").append(data);
            }
        });
    });
</script>