<style type="text/css">
    .jumbotron {
        background-color: #C0C0C0;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 0 30px 0;
        padding-left: 20px;
        line-height: 1.5;
        /*width: 97%;*/
    }

    .fonteReduzida {
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        font-size: 7px !important;
        line-height: 1.5;
        margin-bottom: 0;
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }

    .TituloOperadora {
        background-color: #337ab7;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        line-height: 1.5;
        width: 100%;
    }

    .detalhesOperadora {
        padding: 10px 0px;
        font-weight: bold;
    }

    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .fonteReduzida {
        font-size: 9px;
    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .esquerda {
        float: left;
        margin: 15px;

        font-family: sans-serif;
    }

    .direita {
        float: right;
    }

    .total {
        width: 100%;
    }

    .p15 {
        width: 15%;
    }

    .p85 {
        width: 85%;
    }

    .totalTabela {
        width: 100% !important;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .t100 {
        width: 100%;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }
</style>
<page backtop="5mm" backbottom="5mm" backleft="0mm" backright="0mm" style="font-size: 11px">

    <page_header style="font-size: 7px;">

        <div class="col-xs-12" style="background-color: #004057; color: #ffffff;  padding: 5px 0; text-align: center; font-weight: bold">
            <?= $operadora["nome"] . " " . $operadora["detalhe"] . " " ?><script>
                $("#vidas").val()
            </script>
        </div>

        <table style="width: 100%; margin-bottom: 30px !important;">
            <tr style="margin-bottom: 30px !important;">

                <td style="width: 34%">

                    <?php if ($sessao['imagem_id'] === null) { ?>
                        <img src='https://corretorparceiro.com.br/app/img/insiralogo.jpg' />
                    <?php } else {
                    ?>
                        <img src="https://corretorparceiro.com.br/app/<?= $sessao['imagem']['caminho'] . $sessao['imagem']['nome'] ?>" style="margin-left: 20px; max-height: 70px; padding: 5px 0;" />
                    <?php } ?>
                </td>
                <td style="width: 39%">

                    <table style="width: 100%;">
                        <tr style="width: 100%;">
                            <td style="width: 100%;font-size:80%; margin-left: 2px;">
                                <b> <?= $sessao['nome'] . " " . $sessao['sobrenome']; ?></b>
                            </td>
                        </tr>
                        <tr style="font-size:80%">
                            <td>
                                <img src="https://corretorparceiro.com.br/app/img/celular.png" height="15" style="height: 15px !important;" /><b>Celular: </b> <?= $sessao['celular']; ?>
                            </td>
                        </tr>
                        <tr style="font-size:80%">
                            <td>
                                <img src="https://corretorparceiro.com.br/app/img/whatsapp.png" height="15" style="height: 15px !important;margin-left: 5px !important;" /><b>Whatsapp: </b> <?= $sessao['whatsapp']; ?>
                            </td>
                        </tr>
                        <tr style="font-size:80%">
                            <td>
                                <img src="https://corretorparceiro.com.br/app/img/email.png" height="15" style="height: 15px !important;" /> <b>Email: </b><?= $sessao['email']; ?>
                            </td>

                        </tr>
                        <tr style="font-size:80%">
                            <td>
                                <img src="https://corretorparceiro.com.br/app/img/site.png" height="15" style="height: 15px !important;" /> <b>Site: </b><?= $sessao['site']; ?>
                            </td>
                        </tr>
                    </table>

                </td>
                <td style="text-align: right; width: 25%;">
                    <?php
                    // 	            debug($tabelas);
                    // 	            echo "<p style='font-size:80%; margin: 1px !important'>Estado: " . $tabelas[0]['estado']['nome'] . "</p>";
                    ?>
                    <p style="font-size:80%; margin: 1px !important">Gerado em <?= date('d/m/Y') ?></p>
                    <?php
                    switch (date('m')):
                        case '01':
                            $vigencia = 'Janeiro';
                            break;
                        case '02':
                            $vigencia = 'Fevereiro';
                            break;
                        case '03':
                            $vigencia = 'Março';
                            break;
                        case '04':
                            $vigencia = 'Abril';
                            break;
                        case '05':
                            $vigencia = 'Maio';
                            break;
                        case '06':
                            $vigencia = 'Junho';
                            break;
                        case '07':
                            $vigencia = 'Julho';
                            break;
                        case '08':
                            $vigencia = 'Agosto';
                            break;
                        case '09':
                            $vigencia = 'Setembro';
                            break;
                        case '10':
                            $vigencia = 'Outubro';
                            break;
                        case '11':
                            $vigencia = 'Novembro';
                            break;
                        case '12':
                            $vigencia = 'Dezembro';
                            break;

                    endswitch;
                    ?>
                    <p style="font-size:80%; margin: 1px !important">Vigência: <?= $vigencia . "/" . date('Y') ?></p>
                    <p style="font-size:80%; margin: 1px !important">Sujeito a alterações</p>
                </td>
            </tr>
        </table>
    </page_header>

    <page_footer style="font-size: 14px;">
        <div class="rodape-home col-xs-12" style="text-align: center;">
            Ferramentas de apoio a corretores.<br> Desenvolvido por <a href="https://corretorparceiro.com.br" style='color: #337ab7 !important; text-decoration: none !important'><?= '<img src="https://corretorparceiro.com.br/app/img/ciranda.png" style="height: 15px; margin-left: 5px;" />' ?><span style="text-decoration: none !important"> corretorparceiro.com.br</span></a>
        </div>
    </page_footer>
    <br>

    <?php
    foreach ($tabelas as $t) {
        foreach ($t as $dados) {
            foreach ($dados as $info) {
                //    debug($info);die();
                $validade = $info['vigencia'];
                $regiao = $info['regio']['estado']['nome'] . " / " . $info['regio']['nome'];
                $titulo = $info['operadora']['nome'] . " - PLANO " . $info['modalidade']['nome'] . " " . $info['minimo_vidas'] . " À " . $info['maximo_vidas'] . " VIDAS";
                $imagem = $info['operadora']['imagen']['caminho'] . $info['operadora']['imagen']['nome'];
                break;
            }
        }
    }
    ?>


    <table style="width: 100%; border-collapse:collapse;">
        <tr style="width: 100%;">
            <td style="font-family: sans-serif; font-size: 9px; padding-top: 3px ;padding-bottom: 3px; background-color: #000000; color: #ffffff; width: 100%; margin: 3px; text-align: center;">
                <?= $titulo ?>
            </td>
        </tr>
    </table>
    <br />
    <table style="width: 100%;border-collapse:collapse; font-size: 7.8px; text-align: center;background-color: #C0C0C0;">
        <tr style="background-color: #C0C0C0; width: 100%">
            <td style="width: 10%;text-align: center; border: solid 0.5px #000; ">
                Faixa Etária
            </td>
            <?php
            foreach ($tabelas as $chave => $tabela) {

                $fator = count($tabela);
                foreach ($tabela as $v) {

                    foreach ($v as $value) {

                        $colspan = count($v) * $fator;

                        break;
                    }
                    break;
                }
                $largura = 2220 / count($tabela);
            ?>
                <td style=" text-align: center; border: solid 0.5px #000; " colspan="<?= $colspan ?>"><?= $chave ?></td>
            <?php
            }
            ?>
        </tr>
        <tr>
            <td style="background-color: #C0C0C0; margin: 10px;border: solid 0.5px #000;">
                Acomodação
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                foreach ($tabela as $chave => $valor) {
                    //                    debug(count($tabelas));die();
            ?>
                    <td style="background-color: #C0C0C0;border: solid 0.5px #000" colspan="<?= count($valor) ?>"><?= $chave ?></td>
            <?php
                }
                //                break;
            }
            ?>
        </tr>
        <tr>
            <td style="background-color: #C0C0C0; margin: 10px;border: solid 0.5px #000">
                Código ANS:
            </td>
            <?php
            foreach ($tabelas as $tabela) {

                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?>
                        <td style="background-color: #C0C0C0;border: solid 0.5px #000" colspan="1"><?= $info ?></td>
            <?php
                    }
                }
            }
            ?>
        </tr>
        <tr>
            <td style="background-color: #C0C0C0; margin: 10px;border: solid 0.5px #000;">
                Produto
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
                        $largura = 90 / $total;
            ?>
                        <td style="width: <?= $largura ?>% ;background-color: #C0C0C0;border: solid 0.5px #000; vertical-align: middle; font-size: 7px;" colspan="1"><?= (strpos($dados['nome'], 'SEM COPARTICIPAÇÃO')) ? 'S/ COPARTICIPAÇÃO' : substr($dados['nome'], strpos($dados['nome'], "COPARTICIPAÇÃO")); ?></td>
            <?php
                    }
                }
            }
            ?>
        </tr>


        <tr style="background-color: #fff">
            <td style="background-color: #fff;">
                <?= '00 à 18 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?><td><?= $this->Number->format($dados['faixa1'], ['before' => 'R$ ', 'places' => 2]) ?></td><?php
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                                        ?>
        </tr>
        <tr style="background-color: #C0C0C0">
            <td style="background-color: #C0C0C0;">
                <?= '19 à 23 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?><td><?= $this->Number->format($dados['faixa2'], ['before' => 'R$ ', 'places' => 2]) ?></td><?php
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                                        ?>
        </tr>
        <tr style="background-color: #fff">
            <td style="background-color: #fff;">
                <?= '24 à 28 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?><td><?= $this->Number->format($dados['faixa3'], ['before' => 'R$ ', 'places' => 2]) ?></td><?php
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                                        ?>
        </tr>
        <tr style=" background-color: #C0C0C0">
            <td style="background-color: #C0C0C0;">
                <?= '29 à 33 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?><td><?= $this->Number->format($dados['faixa4'], ['before' => 'R$ ', 'places' => 2]) ?></td><?php
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                                        ?>
        </tr>
        <tr style="background-color: #fff">
            <td style="background-color: #fff;">
                <?= '34 à 38 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?><td><?= $this->Number->format($dados['faixa5'], ['before' => 'R$ ', 'places' => 2]) ?></td><?php
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                                        ?>
        </tr>
        <tr style="background-color: #C0C0C0">
            <td style="background-color: #C0C0C0;">
                <?= '39 à 43 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?><td><?= $this->Number->format($dados['faixa6'], ['before' => 'R$ ', 'places' => 2]) ?></td><?php
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                                        ?>
        </tr>

        <tr style="background-color: #fff">
            <td style="background-color: #fff;">
                <?= '44 à 48 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?><td><?= $this->Number->format($dados['faixa7'], ['before' => 'R$ ', 'places' => 2]) ?></td><?php
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                                        ?>
        </tr>
        <tr style="background-color: #C0C0C0">
            <td style="background-color: #C0C0C0;">
                <?= '49 à 53 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?>
                        <td><?= $this->Number->format($dados['faixa8'], ['before' => 'R$ ', 'places' => 2]) ?></td>
            <?php
                    }
                }
            }
            ?>
        </tr>
        <tr style="background-color: #fff">
            <td style="background-color: #fff;">
                <?= '54 à 58 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?>
                        <td><?= $this->Number->format($dados['faixa9'], ['before' => 'R$ ', 'places' => 2]) ?></td>
            <?php
                    }
                }
            }
            ?>
        </tr>
        <tr style=" background-color: #C0C0C0">
            <td style="background-color: #C0C0C0;">
                <?= '+ de 59 anos' ?>
            </td>
            <?php
            foreach ($tabelas as $tabela) {
                $colspan = count($tabela);
                foreach ($tabela as $chave => $valor) {
                    foreach ($valor as $info => $dados) {
            ?>
                        <td><?= $this->Number->format($dados['faixa10'], ['before' => 'R$ ', 'places' => 2]) ?></td>
            <?php
                    }
                }
            }
            ?>
        </tr>


    </table>
    <br />
    <br />
    <table style="width: 100%; border: 1px solid #000;border-collapse:collapse;font-size: 80%">
        <tr style="width: 100%; font-weight: bold; background-color: #C0C0C0; text-align: center; border: 1px solid #000;border-collapse:collapse;">
            <td style="width: 100%; border: 1px solid #000;border-collapse:collapse; padding: 3px;">Observações Gerais</td>
        </tr>
        <tr>
            <td>
                <?php
                foreach ($tabelas as $tabela) {
                    $colspan = count($tabela);
                    foreach ($tabela as $chave => $valor) {
                        foreach ($valor as $info => $dados) {
                            echo nl2br($dados['produto']['observaco']['descricao']);
                            break;
                        }
                        break;
                    }
                    break;
                }

                //                die();
                ?>
            </td>
        </tr>
    </table>
    <br />
    <table style="width: 100%; border: 1px solid #000;border-collapse:collapse;font-size: 80%">
        <tr style="width: 100%; font-weight: bold; background-color: #C0C0C0; text-align: center; border: 1px solid #000;border-collapse:collapse;">
            <td style="width: 100%; border: 1px solid #000;border-collapse:collapse; padding: 3px;">Documentos Necessários</td>
        </tr>
        <tr>
            <td>
                <?php
                foreach ($tabelas as $tabela) {
                    $colspan = count($tabela);
                    foreach ($tabela as $chave => $valor) {
                        foreach ($valor as $info => $dados) {
                            echo nl2br($dados['produto']['informaco']['descricao']);
                            break;
                        }
                        break;
                    }
                    break;
                }

                //                die();
                ?>
            </td>
        </tr>
    </table>
    <br />
    <table style="width: 100%; border: 1px solid #000;border-collapse:collapse;font-size: 80%">
        <tr style="width: 100%; font-weight: bold; background-color: #C0C0C0; text-align: center; border: 1px solid #000;border-collapse:collapse;">
            <td style="width: 100%; border: 1px solid #000;border-collapse:collapse; padding: 3px;">Regras para inclusão de Beneficiários e Dependentes</td>
        </tr>
        <tr>
            <td>
                <?php
                foreach ($tabelas as $tabela) {
                    $colspan = count($tabela);
                    foreach ($tabela as $chave => $valor) {
                        foreach ($valor as $info => $dados) {

                            echo nl2br($dados['produto']['opcionai']['descricao']);
                            break;
                        }
                        break;
                    }
                    break;
                }
                ?>
            </td>
        </tr>
    </table>
    <br />
    <table style="width: 100%; border: 1px solid #000;border-collapse:collapse; font-size: 80%">
        <tr style="width: 100%; font-weight: bold; background-color: #C0C0C0; text-align: center; border: 1px solid #000;border-collapse:collapse;">
            <td style="width: 100%; border: 1px solid #000;border-collapse:collapse; padding: 3px;" colspan="2">Rede Credenciada</td>
        </tr>



        <?php
        foreach ($tabelas as $tabela) {
            $colspan = count($tabela);
            foreach ($tabela as $chave => $valor) {
                foreach ($valor as $info => $dados) {
                    $rede[$dados['produto']['descricao']] = $dados['produto']['rede']['descricao'];
                }
            }
        }
        $cor = '#fff';
        foreach ($rede as $chave => $valor) {
            if (!empty($valor)) {
                echo "<tr style='background-color: " . $cor . "'><td style='width:10%;'>" . $chave . "</td>";
                echo "<td style='width:90%;'>" . nl2br($valor) . "</td></tr>";
                switch ($cor):
                    case ('#fff');
                        $cor = '#C0C0C0';
                        break;
                    case ('#C0C0C0');
                        $cor = '#fff';
                        break;
                endswitch;
            }
        }
        ?>

    </table>
    <br />
    <table style="width: 100%; border: 1px solid #000;border-collapse:collapse; font-size: 80%">
        <tr style="width: 100%; font-weight: bold; background-color: #C0C0C0; text-align: center; border: 1px solid #000;border-collapse:collapse;">
            <td style="width: 100%; border: 1px solid #000;border-collapse:collapse; padding: 3px;">Área de Comercialização / Atendimento</td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <?php
                foreach ($tabelas as $tabela) {
                    $colspan = count($tabela);
                    foreach ($tabela as $chave => $valor) {
                        foreach ($valor as $info => $dados) {

                            echo nl2br($dados['regio']['descricao']);
                            break;
                        }
                        break;
                    }
                    break;
                }
                ?>
            </td>
        </tr>
    </table>


</page>
<?php die(); ?>
