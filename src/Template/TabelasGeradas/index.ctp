<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tabelas Gerada'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tabelasGeradas index large-9 medium-8 columns content">
    <h3><?= __('Tabelas Geradas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('estado_id') ?></th>
                <th><?= $this->Paginator->sort('ramo') ?></th>
                <th><?= $this->Paginator->sort('operadora') ?></th>
                <th><?= $this->Paginator->sort('vidas') ?></th>
                <th><?= $this->Paginator->sort('coparticipacao') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tabelasGeradas as $tabelasGerada): ?>
            <tr>
                <td><?= $this->Number->format($tabelasGerada->id) ?></td>
                <td><?= $tabelasGerada->has('user') ? $this->Html->link($tabelasGerada->user->id, ['controller' => 'Users', 'action' => 'view', $tabelasGerada->user->id]) : '' ?></td>
                <td><?= $tabelasGerada->has('estado') ? $this->Html->link($tabelasGerada->estado->id, ['controller' => 'Estados', 'action' => 'view', $tabelasGerada->estado->id]) : '' ?></td>
                <td><?= h($tabelasGerada->ramo) ?></td>
                <td><?= h($tabelasGerada->operadora) ?></td>
                <td><?= h($tabelasGerada->vidas) ?></td>
                <td><?= h($tabelasGerada->coparticipacao) ?></td>
                <td><?= h($tabelasGerada->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), "/tabelasGeradas/view/$tabelasGerada->id") ?>
                    <?= $this->Html->link(__('Edit'), "/tabelasGeradas/edit/$tabelasGerada->id") ?>
                    <?= $this->Form->postLink(__('Delete'), "/tabelasGeradas/delete/$tabelasGerada->id", ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasGerada->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
