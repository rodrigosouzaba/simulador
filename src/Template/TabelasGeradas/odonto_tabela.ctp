<?php if (!empty($tabelasOrdenadas)) : ?>
    <?= $this->element('new_acoes') ?>
    <?php echo $this->element('cabecalho_tabelas_personalizadas'); ?>

    <!-- PRIMEIRA TABELA -->
    <?php foreach ($tabelasOrdenadas as $operadora_id => $operadora) { ?>
        <?php foreach ($operadora['tabelas'] as $grupo) { ?>
            <table style="width: 100%; border-collapse:collapse;" class="table table-condensed table-striped table-bordered">
                <colgroup>
                    <col span="1" style="width: 110px;">
                </colgroup>

                <!-- NOME -->
                <tr style="width: 100%; background-color: #004057">
                    <td style="max-width: 50px !important">&nbsp;</td>
                    <?php foreach ($grupo as $tabela) { ?>
                        <td class="titulo-tabela centralizada" style="background-color: #004057; color: #ffffff;  margin: 3px; text-align: center;">
                            <?= $tabela->nome ?>
                        </td>
                    <?php } ?>
                </tr>

                <!-- PREÇOS -->
                <tr>
                    <td style="max-width: 50px !important" class="centralizada">Preço p/ vida</td>
                    <?php foreach ($grupo as $tabela) { ?>
                        <td class="centralizada">
                            <?= $this->Number->format($tabela["preco_vida"], ['before' => 'R$ ', 'places' => 2]) ?>
                        </td>
                    <?php } ?>
                </tr>
            </table>
        <?php } ?>
    <?php } ?>

    <!-- OBS IMPORTANTES -->
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            OBSERVAÇÕES IMPORTANTES
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
            <?php if (!empty($operadora['observacoes'])) {
                foreach ($operadora['observacoes'] as $observacao) {
                    echo '<strong>Produtos: ' . substr($observacao['tabelas'], 0, -5) . '</strong></br></br>' . $observacao['data'];
                }
            } else {
                echo "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <!-- /OBS IMPORTANTES-->

    <!-- REDE CREDENCIADA -->
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            REDE REFERENCIADA (Resumo)
            <span>
                <?php
                if (isset($operadora['operadora']['url_rede']) && !is_null($operadora['operadora']['url_rede'])) {
                    echo "- Para rede completa " . $this->Html->link('clique aqui', $operadora['operadora']['url_rede'], ['target' => "_blank"]) . ".";
                } else {
                    echo "";
                }
                ?>
            </span>
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php if (!empty($operadora['redes'])) {
                foreach ($operadora['redes'] as $redes) {
                    echo '<strong>Produtos: ' . substr($redes['tabelas'], 0, -5) . '</strong></br></br>' . $redes['data'];
                }
            } else {
                echo "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <!-- /REDE CREDENCIADA -->

    <!-- DIFERENCIAIS -->
    <div class="nobreak">
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php if (!empty($operadora['reembolsos'])) {
                foreach ($operadora['reembolsos'] as $reembolsos) {
                    echo '<strong>Produtos: ' . substr($reembolsos['tabelas'], 0, -5) . '</strong></br></br>' . $reembolsos['data'];
                }
            } else {
                echo "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <!-- /DIFERENCIAIS -->

    <!-- COMERCIALIZACAO -->
    <div class="nobreak">
        <div class="clearfix">&nbsp;</div>
        <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; width: 100%;max-width: 100% !important">
            ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
        </div>
        <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 100%;max-width: 100% !important">
            <strong>Areas de Atendimento</strong>
            <br>
            <?php foreach ($operadora['atendimentos'] as $tipo => $tabelas) {
                echo '<strong>' . $tipo  . '</strong>: ' . $tabelas . '<br>';
            } ?>
            <br>
            <strong>Areas de Comercialização</strong>
            <br>
            <?php if (!empty($operadora['areas_comercializacoes'])) {
                foreach ($operadora['areas_comercializacoes'] as $areas_comercializacoes) {
                    echo '<strong>Produtos: ' . substr($areas_comercializacoes['tabelas'], 0, -5) . '</strong></br></br>' . substr($areas_comercializacoes['data'], 0, -2) . '.';
                }
            } else {
                echo "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <!-- /COMERCIALIZACAO -->

    <!-- CARENCIAS -->
    <div class="nobreak">
        <div class="clearfix">&nbsp;</div>
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            CARÊNCIAS (Resumo)
        </div>

        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php if (!empty($operadora['carencias'])) {
                foreach ($operadora['carencias'] as $carencias) {
                    echo '<strong>Produtos: ' . substr($carencias['tabelas'], 0, -5) . '</strong></br></br>' . $carencias['data'];
                }
            } else {
                echo "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <!-- /CARENCIAS -->

    <!-- DEPENDENTES -->
    <div class="nobreak">
        <div class="clearfix">&nbsp;</div>
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            DEPENDENTES
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php if (!empty($operadora['dependentes'])) {
                foreach ($operadora['dependentes'] as $dependentes) {
                    echo '<strong>Produtos: ' . substr($dependentes['tabelas'], 0, -5) . '</strong></br></br>' . $dependentes['data'];
                }
            } else {
                echo "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <!-- /DEPENDENTES -->

    <!-- DOCUMENTOS NECESSÁRIOS -->
    <div class="nobreak">
        <div class="clearfix">&nbsp;</div>
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            DOCUMENTOS NECESSÁRIOS
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php if (!empty($operadora['documentos'])) {
                foreach ($operadora['documentos'] as $documentos) {
                    echo '<strong>Produtos: ' . substr($documentos['tabelas'], 0, -5) . '</strong></br></br>' . $documentos['data'];
                }
            } else {
                echo "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <!-- /DOCUMENTOS NECESSÁRIOS -->

    <!-- FORMAS DE PAGAMENTOS -->
    <div class="nobreak">
        <div class="clearfix">&nbsp;</div>
        <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            FORMAS DE PAGAMENTOS
        </div>
        <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
            <?php if (!empty($operadora['pagamentos'])) {
                foreach ($operadora['pagamentos'] as $pagamentos) {
                    echo '<strong>Produtos: ' . substr($pagamentos['tabelas'], 0, -5) . '</strong></br></br>' . $pagamentos['data'];
                }
            } else {
                echo "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
        <div class="clearfix">&nbsp;</div>
    </div>
    <!-- /FORMAS DE PAGAMENTOS -->

    </div>

<?php else : ?>
    <div class="alert alert-danger"><span class="text-danger texxt-center">Nenhuma tabela encontrada</span></div>
<?php endif; ?>