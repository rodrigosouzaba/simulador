<!-- <div>&nbsp;</div> -->
<?php foreach ($tabelasOrdenadas as $tabelasOrdenadas) { ?>

	<page backtop="32mm" backbottom="7mm" backleft="5mm" backright="5mm" style="font-size: 11px">
		<page_header style="margin-right: 20px;margin-left: 20px; margin-top: 20px; width: 100%; margin-bottom: 300px !important;">

			<?= $this->element('cabecalho_tabela'); ?>

		</page_header>
		<page_footer style="font-size: 14px;">
			<div class="rodape-home col-xs-12" style="text-align: center;">
				Ferramentas de apoio a corretores.<br> Desenvolvido por <a href="https://corretorparceiro.com.br" style='color: #337ab7 !important; text-decoration: none !important'><?= '<img src="https://corretorparceiro.com.br/app/img/ciranda.png" style="height: 15px; margin-left: 5px;" />' ?><span style="text-decoration: none !important"> corretorparceiro.com.br</span></a>
			</div>
		</page_footer>


		<br>
		<table style="width: 100%; border-collapse:collapse;">
			<tr style="width: 100%;">
				<td style="max-width: 50px !important">&nbsp;</td>
				<?php
				foreach ($tabelasOrdenadas as $produto => $acomodacoes) {
				?>
					<td class="titulo-tabela centralizada" style="background-color: #000000; color: #ffffff;  margin: 3px; text-align: center;" colspan="<?= array_sum(array_map("count", $acomodacoes)) ?>">
						<?= $produto ?>
					</td>
				<?php } ?>
			</tr>
			<tr>
				<td style="max-width: 50px !important">&nbsp;</td>
				<?php
				foreach ($tabelasOrdenadas as $produto => $acomodacoes) {
					foreach ($acomodacoes as $acomodacao => $co) {
						// 					debug($co);
				?>
						<td class="centralizada titulo-tabela" colspan="<?= count($co, COUNT_RECURSIVE) ?>">
							<?= $acomodacao ?>
						</td>
				<?php  }
				} ?>
			</tr>
			<tr>
				<td style="max-width: 50px !important">&nbsp;</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada titulo-tabela">
								<?php $c = explode("-", $coparticipacao); ?>
								<?= $c[1] ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td style="max-width: 50px !important" class="centralizada">até 18 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa1"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">19 a 23 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa2"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">24 a 28 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa3"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">29 a 33 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa4"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">34 a 38 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa5"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">39 a 43 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa6"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">44 a 48 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa7"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">49 a 53 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa8"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">54 a 58 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa9"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">59 a 64 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa10"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">65 a 80 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa11"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>
			<tr>
				<td class="centralizada" style="max-width: 50px !important">+ de 81 anos</td>
				<?php foreach ($tabelasOrdenadas as $a) {
					foreach ($a as $co) {
						foreach ($co as $coparticipacao => $tabela) { ?>
							<td class="centralizada">
								<?= $this->Number->format($tabela["faixa12"], ['before' => 'R$ ', 'places' => 2]) ?>
							</td>
				<?php }
					}
				} ?>
			</tr>

		</table>
	<?php }


switch ($dados["ramo_id"]) {
	case "SPF":
		$forma_pagamentos = "pf_formas_pagamentos";
		$obsD = "pf_observacoes";
		$dependentesD = "pf_dependentes";
		$redes = "pf_redes";
		$reembolsos = "pf_reembolsos";
		$carenciasD = "pf_carencias";
		$comercializacoes = "pf_comercializacoes";
		$atendimentos = "pf_atendimento";
		$documentosD = "pf_documentos";
		$produtosD = "pf_produto";
		break;
	case "SPJ":
		$forma_pagamentos = "formas_pagamentos";
		$obsD = "observacoes";
		$dependentesD = "opcionais";
		$redes = "redes";
		$reembolsos = "reembolsos";
		$carenciasD = "carencias";
		$comercializacoes = "regioes";
		$atendimentos = "abrangencia";
		$documentosD = "informacoes";
		$produtosD = "produto";
		break;
}

	?>


	<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
		FORMAS DE PAGAMENTOS
	</div>
	<div class="corpo-gerador-tabelas">
		<?php
		foreach ($operadora[$forma_pagamentos] as $pgto) {
			echo ($pgto['descricao'] != null) ? nl2br($pgto['descricao']) : "<div class='info'>Consulte Operadora</div>";
		}
		?>
	</div>
	<div>&nbsp;</div>
	<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
		OBSERVAÇÕES IMPORTANTES
	</div>
	<div class="corpo-gerador-tabelas">
		<?php
		// 	    debug($operadora);
		foreach ($operadora[$obsD] as $obs) {
			echo ($obs['descricao'] != null) ? nl2br($obs['descricao']) : "<div class='info'>Consulte Operadora</div>";
		}
		?>
	</div>
	<div>&nbsp;</div>
	<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
		DEPENDENTES
	</div>
	<div class="corpo-gerador-tabelas">
		<?php
		// 	    debug($operadora);
		foreach ($operadora[$dependentesD] as $dependentes) {
			echo ($dependentes['descricao'] != null) ? nl2br($dependentes['descricao']) : "<div class='info'>Consulte Operadora</div>";
		}
		?>
	</div>
	<div>&nbsp;</div>
	<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
		REDE CREDENCIADA<small> (Resumo)</small>
	</div>
	<div class="corpo-gerador-tabelas">
		<?php
		// 	    debug($operadora);
		foreach ($operadora[$redes] as $rede) {
			echo ($rede['descricao'] != null) ? nl2br($rede['descricao']) : "<div class='info'>Consulte Operadora</div>";
		}
		?>
	</div>
	<div>&nbsp;</div>
	<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
		OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
	</div>
	<div class="corpo-gerador-tabelas">
		<?php
		// 	    debug($operadora);
		foreach ($operadora[$reembolsos] as $diferenciais) {
			echo ($diferenciais['descricao'] != null) ? nl2br($diferenciais['descricao']) : "<div class='info'>Consulte Operadora</div>";
		}
		?>
	</div>
	<div>&nbsp;</div>
	<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
		CARÊNCIAS <small>(Resumo)</small>
	</div>
	<div class="corpo-gerador-tabelas">
		<?php
		foreach ($operadora[$carenciasD] as $carencias) {
			echo ($carencias['descricao'] != null) ? nl2br($carencias['descricao']) : "<div class='info'>Consulte Operadora</div>";
		}
		?>
	</div>
	<div>&nbsp;</div>
	<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
		ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
	</div>
	<div class="corpo-gerador-tabelas">
		<div>
			<b>ÁREA DE COMERCIALIZAÇÃO:</b>
		</div>
		<div>
			<?php
			foreach ($operadora[$comercializacoes] as $comercializacao) {
				echo ($comercializacao['descricao'] != null) ? nl2br($comercializacao['descricao']) : "<div class='info'>Consulte Operadora</div>";
			}
			?>
		</div>
		<div>&nbsp;</div>
		<div>
			<b>ÁREA DE ATENDIMENTO</b>
		</div>
		<?php
		$dadosT = array();
		/*
	    debug($produtosD);
		    debug($atendimentos);
*/
		foreach ($tabelas as $tabela) {
			$dadosT[$tabela[$produtosD]["nome"]] = $tabela[$atendimentos]["descricao"];
		}
		foreach ($dadosT as $produto => $atendimento) {
			echo "<div><b>" . $produto . ":</b>" . " " . $atendimento . "</div>";
		}
		?>


	</div>
	<div>&nbsp;</div>
	<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
		DOCUMENTOS NECESSÁRIOS
	</div>
	<div class="corpo-gerador-tabelas">
		<?php
		foreach ($operadora[$documentosD] as $documentos) {
			echo ($documentos['descricao'] != null) ? nl2br($documentos['descricao']) : "<div class='info'>Consulte Operadora</div>";
		}
		?>
	</div>
	</page>
