<style type="text/css">
    .jumbotron {
        background-color: #C0C0C0;
        color: inherit;
        /*margin-bottom: 30px;*/
        padding: 30px 0 30px 0;
        padding-left: 20px;
        line-height: 1.5;
        /*width: 97%;*/
    }

    .fonteReduzida {
        color: #4d4d4d;
        cursor: pointer;
        display: block;
        font-size: 7px !important;
        line-height: 1.5;
        margin-bottom: 0;
    }

    .operadora {
        /*width: 97% !important;*/
        background-color: #337ab7;
        border-color: solid 1mm #337ab7;
        color: #fff;
        padding: 5px;
    }

    .TituloOperadora {
        background-color: #337ab7;
        padding: 5px;
        color: #fff;
        font-weight: bold;
        line-height: 1.5;
        width: 100%;
    }

    .detalhesOperadora {
        padding: 10px 0px;
        font-weight: bold;
    }

    .larguraPadrao {
        width: 700px !important;
    }

    .tabela {
        border: 1px solid #ddd;
        table-layout: fixed;
        width: 500pt;
    }

    .operadoras {}

    .centralizada {
        text-align: center;
    }

    .tituloField {
        border-bottom: 1px solid #ddd;
        color: #000;
        cursor: default;
        display: block;
        font-size: 10px;
        font-style: normal;
        font-variant-caps: normal;
        font-weight: bold;
        height: 18px;
        line-height: 18px;
        margin: 5px 0px;
        outline: 0 none rgb(85, 85, 85);
        padding: 0;
        vertical-align: baseline;
        white-space: normal;
    }

    .topoTabela {
        padding: 5px 0;
        background-color: #ddd;
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }

    .corpoTabela {
        padding: 5px 0;
        text-align: center;
        font-size: 10px;
    }

    .fonteReduzida {
        font-size: 10px;
    }

    .negrito {
        font-weight: bold;
    }

    .obs {
        text-align: justify;
    }

    .esquerda {
        float: left;
        margin: 15px;

        font-family: sans-serif;
    }

    .direita {
        float: right;
    }

    .total {
        width: 100%;
    }

    .p15 {
        width: 15%;
    }

    .p85 {
        width: 85%;
    }

    .totalTabela {
        width: 100% !important;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .t100 {
        width: 100%;
        border-collapse: collapse;
        font-size: 7.8px;
        text-align: center;
        background-color: #C0C0C0;
    }

    .4col {
        width: 25% !important;
    }

    .corpo-gerador-tabelas {
        border: 1px solid #ddd !important;
        padding: 5px;
        width: 100%;
    }

    .titulo-tabela {
        width: 100%;
        background-color: #ccc;
        text-align: center;
        font-weight: bold;
        padding: 5px;
    }

    .precos {
        border: solid 0.5px #ddd;
        text-align: center;
        padding: 3px 0 3px 0 !important;
    }

    .nobreak {
        page-break-inside: avoid;
    }

    .rodape-home {
        text-align: center;
        color: #666;
        margin-bottom: 10px;
    }

    .page_header {
        width: 100% !important;
        max-width: 100% !important;
        min-width: 100% !important;
    }
</style>

<page backtop="100px" backbottom="100px" backleft="20px" backright="20px" style="font-size: 11px">
    <page_header style="margin-right: 20px;margin-left: 20px; margin-top: 20px; margin-bottom: 300px !important;">

        <?php echo $this->element("cabecalho_tabela"); ?>

    </page_header>
    <page_footer style="font-size: 15px;">
        <div class="rodape-home col-xs-12" style="text-align: center;">
            <a href="https://corretorparceiro.com.br" style='text-decoration: none !important; color: #d9534f;'>
                <span style="text-decoration: none !important"> corretorparceiro.com.br</span>
            </a>
            <br>
            <span style="color: #004057">Assessoria de Apoio a Corretores</span>
        </div>
    </page_footer>
    <div style="width: 100%;">&nbsp;</div>

    <div style="background-color: #004057; color: #ffffff;  padding: 5px 0; text-align: center; font-weight: bold;" width="740">
        <?php

        $dadosOperadora = $operadora["nome"] . " " . $operadora["detalhe"] . " ";
        switch ($dados["ramo_id"]) {
            case "SPJ":
                echo $dadosOperadora . $vidas[0] . " A " . $vidas[1] . " VIDAS";
                break;
            case "SPF":
                echo $dadosOperadora;
                break;
            case "OPF":
                echo $dadosOperadora;
                break;
            case "OPJ":
                echo $dadosOperadora . $vidas[0] . " A " . $vidas[1] . " VIDAS";
                break;
        }
        ?>
    </div>
    <?php
    foreach ($tabelasOrdenadas as $tabelasOrdenadas) {
    ?>

        <div class="centralizada" style="width: 100% !important">


            <table style="border-collapse:collapse; margin-bottom: 20px !important" width="740">
                <!-- NOME PRODUTO -->
                <tr>
                    <td class="precos" style="width: 5% !important;">&nbsp;</td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                        foreach ($tipo_produto as $tipo_produto => $acomodacoes) {
                    ?>
                            <td style="text-align: center;border: solid 0.5px #ddd; font-weight: bold; width: 5px !important" colspan="<?= array_sum(array_map("count", $acomodacoes)); ?>">
                                <?= $produto ?>
                            </td>
                    <?php
                        }
                    } ?>
                </tr>
                <!-- ./NOME PRODUTO -->

                <!-- TIPO PRODUTO -->
                <tr>
                    <td style="border: solid 0.5px #ddd;">&nbsp;</td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $tipos_produto) {
                        foreach ($tipos_produto as $tipo_produto => $acomodacoes) {
                            $text = '';
                            $t = explode("(", $tipo_produto);
                            if (isset($t[1])) {
                                $text =  rtrim($t[0]) . "<br>(" . rtrim($t[1]);
                            } else {
                                $text = rtrim($t[0]);
                            } ?>
                            <td style="font-size: 80%; border: solid 0.5px #ddd; text-align: center; width: 5px !important;" colspan="<?= array_sum(array_map("count", $acomodacoes)) ?>">
                                <span style="background-color: #FFF"><?= $text ?></span>
                            </td>
                    <?php
                        }
                    } ?>
                </tr>
                <!-- /TIPO PRODUTO -->

                <!-- ACOMODAÇÃO -->
                <tr>
                    <td style="border: solid 0.5px #ddd;">&nbsp;</td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $tipos_produto) {
                        foreach ($tipos_produto as $tipo_produto => $acomodacoes) {
                            foreach ($acomodacoes as $acomodacao => $co) {
                    ?>
                                <td style="border: solid 0.5px #ddd;text-align: center; width: <?= 95 / ($totalTabelas / array_sum(array_map("count", $acomodacoes))) ?>% !important; vertical-align: middle; font-size: 80%;" colspan="<?= count($co, COUNT_RECURSIVE) ?>">
                                    <!--
    		                                                                                                                                                                                                                                                                                                                                                                                                                                                <?php if (strlen($acomodacao) > 10) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                echo substr($acomodacao, 0, 12) . "<br>" . substr($acomodacao, 12);
                                                                                                                                                                                                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                echo $acomodacao;
                                                                                                                                                                                                                                                                                                                                                                                                                                                            } ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                -->
                                    <?= ($acomodacao == 'AMBULATORIAL' ? "SEM ACOMODAÇÃO" : $acomodacao) ?>
                                </td>
                    <?php
                            }
                        }
                    } ?>
                </tr>
                <!-- /ACOMODAÇÃO -->

                <!-- NOME TABELA E DETALHES -->
                <tr style="background-color: #d7d7d7;">
                    <td width="80">&nbsp;</td>


                    <?php
                    $x = 0;
                    foreach ($tabelasOrdenadas as $produto => $tipos_produto) {
                        foreach ($tipos_produto as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>

                                    <td style="border: solid 0.5px #ddd;text-align: center; font-size: 75%; white-space: pre !important">
                                        <?php $c = explode("-", $coparticipacao); ?>
                                        <span style="font-weight: bold;"><?= $tabela["nome"] ?></span><br>
                                        <span><?= substr($c[1], 0, 3) != "SEM" ? "COM " . $c[1] : $c[1] ?></span>
                                        <small style="font-weight: 100 !important">
                                            <?php
                                            $x++;
                                            if ($this->request->data("ramo_id") == "SPJ") {
                                                echo "<br>";
                                                $i = 1;
                                                foreach ($tabela["tabelas_cnpjs"] as $cnpj) {
                                                    if ($i <= count($tabela["tabelas_cnpjs"])) {
                                                        echo $cnpj["cnpj"]["nome"] . ",";
                                                    } else {
                                                        echo $cnpj["cnpj"]["nome"] . "<br>";
                                                    }
                                                    $i++;
                                                }
                                            }
                                            if ($tabela["cod_ans"]) {
                                                echo "<br>Cód. ANS: " . $tabela["cod_ans"];
                                            } ?>
                                        </small>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <!-- /NOME TABELA E DETALHES -->

                <?php
                if ($dados["ramo_id"] <> "OPF" && $dados["ramo_id"] <> "OPJ") {
                    if ($totalTabelas >= 5) {
                        $medida = 650;
                    } else {
                        $medida = 655;
                    } ?>


                    <tr>
                        <td class="precos" width="80">até 18 anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa1"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>
                    <!-- FAIXA 2: 19 a 23 anos -->
                    <tr>
                        <td class="precos" width="80">19 a 23 anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa2"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>


                    <!-- FAIXA 3: 24 a 28 anos -->
                    <tr>
                        <td class="precos" width="80">24 a 28 anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa3"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>

                    <!-- FAIXA 4: 29 a 33 anos -->
                    <tr>
                        <td class="precos" width="80">29 a 33 anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa4"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>

                    <!-- FAIXA 5: 34 a 38 anos -->
                    <tr>
                        <td class="precos" width="80">34 a 38 anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa5"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>

                    <!-- FAIXA 6: 39 a 43 anos -->
                    <tr>
                        <td class="precos" width="80">39 a 43 anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa6"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>

                    <!-- FAIXA 7: 44 a 48 anos -->
                    <tr>
                        <td class="precos" width="80">44 a 48 anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa7"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>

                    <!-- FAIXA 8: 49 a 53 anos -->
                    <tr>
                        <td class="precos" width="80">49 a 53 anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa8"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>

                    <!-- FAIXA 9: 54 a 58 anos -->
                    <tr>
                        <td class="precos" width="80">54 a 58 anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa9"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>

                    <!-- FAIXA 10: 54 a 58 anos -->
                    <tr>
                        <td class="precos" width="80">59 ou + anos</td>
                        <?php
                        foreach ($tabelasOrdenadas as $produto => $tipo_produto) {
                            foreach ($tipo_produto as $acomodacoes) {
                                foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                    foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                        ?>
                                        <td class="precos" width="<?= $medida / $x ?>">
                                            <?= $this->Number->format($tabela["faixa10"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        </td>
                        <?php
                                    }
                                }
                            }
                        } ?>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td style="max-width: 50px !important" class="centralizada">Preço p/ vida</td>
                        <?php foreach ($tabelasOrdenadas as $a) {
                            foreach ($a as $co) {
                                foreach ($co as $coparticipacao => $tabela) {
                        ?>
                                    <td class="centralizada">
                                        <?= $this->Number->format($tabela["preco_vida"], ['before' => 'R$ ', 'places' => 2]) ?>
                                    </td>
                        <?php
                                }
                            }
                        } ?>
                    </tr>
                <?php
                } ?>

            </table>
            <div style="width: 100%;">&nbsp;</div>

        </div>
    <?php
    }

    switch ($dados["ramo_id"]) {
        case "SPF":
            $forma_pagamentos = "pf_formas_pagamentos";
            $obsD = "pf_observacoes";
            $dependentesD = "pf_dependentes";
            $redes = "pf_redes";
            $reembolsos = "pf_reembolsos";
            $carenciasD = "pf_carencias";
            $comercializacoes = "pf_comercializacoes";
            $atendimentos = "pf_atendimento";
            $documentosD = "pf_documentos";
            $produtosD = "pf_produto";
            break;
        case "SPJ":
            $forma_pagamentos = "formas_pagamentos";
            $obsD = "observacoes";
            $dependentesD = "opcionais";
            $redes = "redes";
            $reembolsos = "reembolsos";
            $carenciasD = "carencias";
            $comercializacoes = "regioes";
            $atendimentos = "abrangencia";
            $documentosD = "informacoes";
            $produtosD = "produto";
            break;
        case "OPJ":
            $forma_pagamentos = "odonto_formas_pagamentos";
            $obsD = "odonto_observacaos";
            $dependentesD = "odonto_dependentes";
            $redes = "odonto_redes";
            $reembolsos = "odonto_reembolsos";
            $carenciasD = "odonto_carencias";
            $comercializacoes = "odonto_comercializacoes";
            $atendimentos = "odonto_atendimentos";
            $documentosD = "odonto_documentos";
            $produtosD = "odonto_produto";
            break;
        case "OPF":
            $forma_pagamentos = "odonto_formas_pagamentos";
            $obsD = "odonto_observacaos";
            $dependentesD = "odonto_dependentes";
            $redes = "odonto_redes";
            $reembolsos = "odonto_reembolsos";
            $carenciasD = "odonto_carencias";
            $comercializacoes = "odonto_comercializacoes";
            $atendimentos = "odonto_atendimento";
            $documentosD = "odonto_documentos";
            $produtosD = "odonto_produto";
            break;
    }

    ?>

    <?php
    if (isset($entidades) && $entidades <> null) {
    ?>
        <div>&nbsp;</div>
        <div class="titulo-tabela">
            ENTIDADES E PROFISSÕES
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($entidades as $pfentidadespfoperadora) {
                echo "<b>" . $pfentidadespfoperadora["pf_entidade"]["nome"] . ":</b>";
                $totalProfissoes = count($pfentidadespfoperadora["pf_entidade"]["pf_entidades_profissoes"]);
                $indice = 1;
                foreach ($pfentidadespfoperadora["pf_entidade"]["pf_entidades_profissoes"] as $profissao) {
                    if ($indice < $totalProfissoes) {
                        echo $profissao["pf_profisso"]["nome"] . ", ";
                    } else {
                        echo $profissao["pf_profisso"]["nome"] . " ";
                    }
                    $indice++;
                }
            } ?>
        </div>
        <div>&nbsp;</div>
    <?php
    } ?>
    <div class="nobreak">
        <div>&nbsp;</div>
        <div class="titulo-tabela">
            OBSERVAÇÕES IMPORTANTES
        </div>
        <div class="corpo-gerador-tabelas">
            <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$obsD] as $obs) {
                echo ($obs['descricao'] != null) ? nl2br($obs['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>


    </div>
    <div class="nobreak">
        <div>&nbsp;</div>
        <div class="titulo-tabela">
            REDE REFERENCIADA<small> (Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$redes] as $rede) {
                echo ($rede['descricao'] != null) ? nl2br($rede['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div class="nobreak">

        <div>&nbsp;</div>
        <div class="titulo-tabela">
            OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$reembolsos] as $diferenciais) {
                echo ($diferenciais['descricao'] != null) ? nl2br($diferenciais['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>

    </div>


    <div class="nobreak">
        <div>&nbsp;</div>
        <div class="titulo-tabela">
            ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
        </div>
        <div class="corpo-gerador-tabelas">
            <p style="padding: 0 !important; margin: 0 !important">
                <b>ÁREA DE COMERCIALIZAÇÃO: </b><?php
                                                foreach ($operadora[$comercializacoes] as $comercializacao) {
                                                    echo ($comercializacao['descricao'] != null) ? nl2br($comercializacao['descricao']) : "<div class='info'>Consulte Operadora</div>";
                                                }
                                                ?>

            </p>
            <p>
                <b>ÁREA DE ATENDIMENTO</b>

                <?php
                $dadosT = array();
                foreach ($tabelas as $tabela) {
                    $dadosT[$tabela[$produtosD]["nome"]] = $tabela[$atendimentos]["descricao"];
                }
                foreach ($dadosT as $produto => $atendimento) {
                    echo "<br><b>" . $produto . ":</b>" . " " . $atendimento;
                }
                ?>
            </p>
        </div>
    </div>

    <div class="nobreak">


        <div>&nbsp;</div>
        <div class="titulo-tabela">
            CARÊNCIAS <small>(Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$carenciasD] as $carencias) {
                echo ($carencias['descricao'] != null) ? nl2br($carencias['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>

    </div>
    <div class="nobreak">
        <div>&nbsp;</div>
        <div class="titulo-tabela">
            DEPENDENTES
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$dependentesD] as $dependentes) {
                echo ($dependentes['descricao'] != null) ? nl2br($dependentes['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>

    <div class="nobreak">

        <div>&nbsp;</div>
        <div class="titulo-tabela">
            DOCUMENTOS NECESSÁRIOS
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$documentosD] as $documentos) {
                echo ($documentos['descricao'] != null) ? nl2br($documentos['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>
    <div class="nobreak">
        <div>&nbsp;</div>
        <div class="titulo-tabela">
            FORMAS DE PAGAMENTO
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$forma_pagamentos] as $pgto) {
                echo ($pgto['descricao'] != null) ? nl2br($pgto['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    </div>


</page>
<?php
?>