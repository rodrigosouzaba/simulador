<div class="container">
    <?php
    echo $this->element('share-email');
    echo $this->element('modal-login');
    ?>
    <?= $this->Form->create('download-pdf', ['id' => 'download-pdf', 'url' => ['action' => 'pdf',]]); ?>

    <?php if (isset($tabelasOrdenadas) && $tabelasOrdenadas != '') { ?>
        <?= $this->Form->create('download-pdf', ['id' => 'download-pdf', 'url' => ['action' => 'pdf',]]); ?>
        <?= $this->element('shared_cabecalho'); ?>
        <div class="col-xs-12" style="background-color: #004057; color: #ffffff;  padding: 5px 0; text-align: center; font-weight: bold; ">
            <?php
            $dadosOperadora = $operadora["nome"] . " " . $operadora["detalhe"] . " ";
            switch ($dados["ramo_id"]) {
                case "SPJ":
                    echo $dadosOperadora . $vidas[0] . " A " . $vidas[1] . " VIDAS";
                    break;
                case "SPF":
                    echo $dadosOperadora;
                    break;
                case "OPF":
                    echo $dadosOperadora;
                    break;
                case "OPJ":
                    echo $dadosOperadora . $vidas[0] . " A " . $vidas[1] . " VIDAS";
                    break;
            }
            ?>
        </div>
        <style>
            .table td {
                border: solid 0.5px #cccccc !important;
            }
        </style>
        <?php foreach ($tabelasOrdenadas  as $operadora_id => $operadora) : ?>
            <?php foreach ($operadora['tabelas'] as $grupos) :  ?>
                <table class="table table-condensed table-striped">
                    <colgroup>
                        <col span="1" style="width: 110px;">
                    </colgroup>
                    <tr style="width: 100%;">
                        <td style="max-width: 50px !important;">&nbsp;</td>
                        <?php foreach ($grupos as $key => $produto) { ?>
                            <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                                <?= $produto->nome ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <tr style="width: 100%;">
                        <td style="max-width: 50px !important;">&nbsp;</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                                <?= $produto['cobertura']->nome ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <tr style="width: 100%;">
                        <td style="max-width: 50px !important;">&nbsp;</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                                <strong><?= ($produto['acomodacao']->nome == 'AMBULATORIAL' ? "SEM ACOMODAÇÃO" : $produto['acomodacao']->nome) ?></strong>
                            </td>
                        <?php } ?>
                    </tr>
                    <tr style="width: 100%;">
                        <td style="max-width: 50px !important;">&nbsp;</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;">
                                <?= ($produto->coparticipacao == 's' ? "COM COPARTICIPAÇÃO" : "SEM COPARTICIPAÇÃO") ?>
                                </br>
                                <small>Cód. ANS: <?= $produto->cod_ans ?></small>
                            </td>
                        <?php } ?>
                    </tr>

                    <!-- PREÇOS -->
                    <!-- até 18 anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">até 18 anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check1 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa1, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- 19 a 23 anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">19 a 23 anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check2 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa2, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- 24 a 28 anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">24 a 28 anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check3 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa3, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- 29 a 33 anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">29 a 33 anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check4 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa4, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- 34 a 38 anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">34 a 38 anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check5 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa5, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- 39 a 43 anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">39 a 43 anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check6 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa6, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- 44 a 48 anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">44 a 48 anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check7 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa7, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- 49 a 53 anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">49 a 53 anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check8 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa8, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- 54 a 58 anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">54 a 58 anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check9 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa9, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- 59 ou + anos -->
                    <tr style="width: 100%;">
                        <td class="text-center idades">59 ou + anos</td>
                        <?php foreach ($grupos as $produto) { ?>
                            <td class="centralizada" style="margin: 3px; text-align: center;">
                                <?php if ($produto->check10 == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php elseif ($produto->atualizacao == 1) : ?>
                                    <strong>Comercialização suspensa</strong>
                                <?php else : ?>
                                    <?= $this->Number->format($produto->faixa10, ['before' => 'R$ ', 'places' => 2]) ?>
                                <?php endif; ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <!-- FIM - PREÇOS -->
                </table>
            <?php endforeach; ?>
            <!-- OBS IMPORTANTES -->
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    OBSERVAÇÕES IMPORTANTES
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
                    <?php if (!empty($operadora['observacoes'])) {
                        foreach ($operadora['observacoes'] as $observacao) {
                            echo '<strong>Produtos: ' . substr($observacao['tabelas'], 0, -5) . '</strong></br></br>' . $observacao['data'];
                        }
                    } else {
                        echo "<div class='info'>Consulte Operadora</div>";
                    }
                    ?>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
            <!-- /OBS IMPORTANTES-->

            <!-- REDE CREDENCIADA -->
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    REDE REFERENCIADA (Resumo)
                    <span>
                        <?php
                        if (isset($operadora['operadora']['url_rede']) && !is_null($operadora['operadora']['url_rede'])) {
                            echo "- Para rede completa " . $this->Html->link('clique aqui', $operadora['operadora']['url_rede'], ['target' => "_blank"]) . ".";
                        } else {
                            echo "";
                        }
                        ?>
                    </span>
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php if (!empty($operadora['redes'])) {
                        foreach ($operadora['redes'] as $redes) {
                            echo '<strong>Produtos: ' . substr($redes['tabelas'], 0, -5) . '</strong></br></br>' . $redes['data'];
                        }
                    } else {
                        echo "<div class='info'>Consulte Operadora</div>";
                    }
                    ?>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
            <!-- /REDE CREDENCIADA -->

            <!-- DIFERENCIAIS -->
            <div class="nobreak">
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php if (!empty($operadora['reembolsos'])) {
                        foreach ($operadora['reembolsos'] as $reembolsos) {
                            echo '<strong>Produtos: ' . substr($reembolsos['tabelas'], 0, -5) . '</strong></br></br>' . $reembolsos['data'];
                        }
                    } else {
                        echo "<div class='info'>Consulte Operadora</div>";
                    }
                    ?>
                </div>
            </div>
            <!-- /DIFERENCIAIS -->

            <!-- COMERCIALIZACAO -->
            <div class="nobreak">
                <div class="clearfix">&nbsp;</div>
                <div style=" background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%; width: 100%;max-width: 100% !important">
                    ÁREAS DE COMERCIALIZAÇÃO E ATENDIMENTO
                </div>
                <div class="fonteReduzida" style="padding: 5px;border: 0.1mm solid #ccc;width: 100%;max-width: 100% !important">
                    <?php if (!empty($operadora['areas_comercializacoes'])) {
                        foreach ($operadora['areas_comercializacoes'] as $areas_comercializacoes) {
                            echo '<strong>Produtos: ' . substr($areas_comercializacoes['tabelas'], 0, -5) . '</strong></br></br>' . $areas_comercializacoes['data'];
                        }
                    } else {
                        echo "<div class='info'>Consulte Operadora</div>";
                    }
                    ?>
                </div>
            </div>
            <!-- /COMERCIALIZACAO -->

            <!-- CARENCIAS -->
            <div class="nobreak">
                <div class="clearfix">&nbsp;</div>
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    CARÊNCIAS (Resumo)
                </div>

                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php if (!empty($operadora['carencias'])) {
                        foreach ($operadora['carencias'] as $carencias) {
                            echo '<strong>Produtos: ' . substr($carencias['tabelas'], 0, -5) . '</strong></br></br>' . $carencias['data'];
                        }
                    } else {
                        echo "<div class='info'>Consulte Operadora</div>";
                    }
                    ?>
                </div>
            </div>
            <!-- /CARENCIAS -->

            <!-- DEPENDENTES -->
            <div class="nobreak">
                <div class="clearfix">&nbsp;</div>
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    DEPENDENTES
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php if (!empty($operadora['dependentes'])) {
                        foreach ($operadora['dependentes'] as $dependentes) {
                            echo '<strong>Produtos: ' . substr($dependentes['tabelas'], 0, -5) . '</strong></br></br>' . $dependentes['data'];
                        }
                    } else {
                        echo "<div class='info'>Consulte Operadora</div>";
                    }
                    ?>
                </div>
            </div>
            <!-- /DEPENDENTES -->

            <!-- DOCUMENTOS NECESSÁRIOS -->
            <div class="nobreak">
                <div class="clearfix">&nbsp;</div>
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    DOCUMENTOS NECESSÁRIOS
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php if (!empty($operadora['documentos'])) {
                        foreach ($operadora['documentos'] as $documentos) {
                            echo '<strong>Produtos: ' . substr($documentos['tabelas'], 0, -5) . '</strong></br></br>' . $documentos['data'];
                        }
                    } else {
                        echo "<div class='info'>Consulte Operadora</div>";
                    }
                    ?>
                </div>
            </div>
            <!-- /DOCUMENTOS NECESSÁRIOS -->

            <!-- ENTIDADES DE CLASSE -->
            <?php if ($dados["ramo_id"] == 'SPF') : ?>
                <div class="nobreak">
                    <div class="clearfix">&nbsp;</div>
                    <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                        ENTIDADES E PROFISSÕES
                    </div>
                    <div class="fonteReduzida" style="font-size: 8px;padding: 5px;width: 100%; border: 0.1mm solid #ccc; text-align: justify !important">
                        <?php foreach ($operadora['entidades'] as $key => $entidade) { ?>
                            <strong><?= $entidade['nome'] ?></strong>
                            </br>
                            <span><?= $entidade['data'] ?></span>
                            </br>
                        <?php } ?>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            <?php endif; ?>
            <!-- /ENTIDADES DE CLASSE -->

            <!-- FORMAS DE PAGAMENTOS -->
            <div class="nobreak">
                <div class="clearfix">&nbsp;</div>
                <div style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                    FORMAS DE PAGAMENTOS
                </div>
                <div style="padding: 5px; width: 100%; border: 0.5px solid #ccc;" class="fonteReduzida">
                    <?php if (!empty($operadora['pagamentos'])) {
                        foreach ($operadora['pagamentos'] as $pagamentos) {
                            echo '<strong>Produtos: ' . substr($pagamentos['tabelas'], 0, -5) . '</strong></br></br>' . $pagamentos['data'];
                        }
                    } else {
                        echo "<div class='info'>Consulte Operadora</div>";
                    }
                    ?>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
            <!-- /FORMAS DE PAGAMENTOS -->

</div>
<?php endforeach; ?>

<?php } else {
        echo "<div class='col-xs-12'>Nenhuma Tabela se aplica as Características selecionadas</div>";
    } ?>
<?= $this->Form->end() ?>
<script type="text/javascript">
    $('#myTabs a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $("#aaa").click(function() {
        $.ajax({
            type: "POST",
            url: baseUrl + "tabelasGeradas/pdf",
            data: $("#gerador").serialize(),
            success: function(data) {
                $("#ss").empty();
                $("#ss").append(data);
            }
        });
    });

    $("#shared").submit(function() {
        $("#loader").click();
        return true;
    });
    $("#shared").bind('ajax:complete', function() {
        $("#fechar").click();
    });
    $(".url").click(function() {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(this).text()).select();
        document.execCommand("copy");
        $temp.remove();
    });
    $("#pdfdownload").click(function() {
        $("#tipo").val("D");
        setInterval(function() {
            $('#fechar').click();
        }, 50000);
    });



    $("#pdfbrowser").click(function() {
        $("#tipo").val("I");
        setInterval(function() {
            $('#fechar').click();
        }, 50000);
    });

    $("#whatsapp").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        let shared = "<?= $sharedWhats ?>";
        if (sessao != "") {
            window.open(shared);
        } else {
            $("#modal-login").modal("show");
        }

    });
    $("#btn-email").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        if (sessao != "") {
            $("#modal-compartilhamento").modal("show");
        } else {
            $("#modal-login").modal("show");
        }
    });
    // '&text=Visualize%20sua%20tabela%20de%20pre%C3%A7os%20aqui%20http%3A%2F%2Fcorretorparceiro.com.br%2Fferramentas%2Fshareds%2Fview%2F11',
</script>
<div class="spacer-md">&nbsp;</div>