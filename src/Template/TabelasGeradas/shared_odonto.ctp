<div class="container">
    <?php
    echo $this->element("processando");
    echo $this->element('share-email');
    echo $this->element('modal-login');
    ?>
    <?= $this->element('shared_cabecalho'); ?>
    <?php foreach ($tabelasOrdenadas as $tabelasOrdenadas) { ?>
        <table style="width: 100%; border-collapse:collapse;" class="table table-condensed table-striped table-bordered">
            <tr style="width: 100%; background-color: #004057">
                <td style="max-width: 50px !important">&nbsp;</td>
                <?php
                foreach ($tabelasOrdenadas as $produto => $acomodacoes) {
                ?>
                    <td class="titulo-tabela centralizada" style="background-color: #004057; color: #ffffff;  margin: 3px; text-align: center;" colspan="<?= array_sum(array_map("count", $acomodacoes)) ?>">
                        <?= $produto ?>
                    </td>
                <?php } ?>
            </tr>
            <tr>
                <td style="max-width: 50px !important;">&nbsp;</td>
                <?php
                foreach ($tabelasOrdenadas as $produto => $acomodacoes) {
                    foreach ($acomodacoes as $acomodacao => $co) {
                ?>
                        <td class="centralizada titulo-tabela" colspan="<?= count($co, COUNT_RECURSIVE) ?>">
                            <?= $acomodacao ?>
                        </td>
                <?php  }
                } ?>
            </tr>
            <tr>
                <td style="max-width: 50px !important" class="centralizada">Preço p/ vida</td>
                <?php foreach ($tabelasOrdenadas as $a) {
                    foreach ($a as $co) {
                        foreach ($co as $coparticipacao => $tabela) { ?>
                            <td class="centralizada">
                                <?= $this->Number->format($tabela["preco_vida"], ['before' => 'R$ ', 'places' => 2]) ?>
                            </td>
                <?php }
                    }
                } ?>
            </tr>


        </table>
    <?php } ?>


    <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
        FORMAS DE PAGAMENTOS
    </div>
    <div class="corpo-gerador-tabelas">
        <?php
        foreach ($operadora["odonto_formas_pagamentos"] as $pgto) {
            echo ($pgto['descricao'] != null) ? nl2br($pgto['descricao']) : "<div class='info'>Consulte Operadora</div>";
        }
        ?>
    </div>
    <div>&nbsp;</div>
    <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
        OBSERVAÇÕES IMPORTANTES
    </div>
    <div class="corpo-gerador-tabelas">
        <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
        <?php
        // 	    debug($operadora);
        foreach ($operadora["odonto_observacaos"] as $obs) {
            echo ($obs['descricao'] != null) ? nl2br($obs['descricao']) : "<div class='info'>Consulte Operadora</div>";
        }
        ?>
    </div>
    <!--
<div>&nbsp;</div>
<div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
    DEPENDENTES
</div>
<div class="corpo-gerador-tabelas">
    <?php
    // 	    debug($operadora);
    foreach ($operadora["opcionais"] as $dependentes) {
        echo ($dependentes['descricao'] != null) ? nl2br($dependentes['descricao']) : "<div class='info'>Consulte Operadora</div>";
    }
    ?>
</div>
-->
    <div>&nbsp;</div>
    <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
        REDE REFERENCIADA<small> (Resumo)</small>
    </div>
    <div class="corpo-gerador-tabelas">
        <?php
        // 	    debug($operadora);
        foreach ($operadora["odonto_redes"] as $rede) {
            echo ($rede['descricao'] != null) ? nl2br($rede['descricao']) : "<div class='info'>Consulte Operadora</div>";
        }
        ?>
    </div>
    <div>&nbsp;</div>
    <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
        OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
    </div>
    <div class="corpo-gerador-tabelas">
        <?php
        // 	    debug($operadora);
        foreach ($operadora["odonto_reembolsos"] as $diferenciais) {
            echo ($diferenciais['descricao'] != null) ? nl2br($diferenciais['descricao']) : "<div class='info'>Consulte Operadora</div>";
        }
        ?>
    </div>
    <div>&nbsp;</div>
    <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
        CARÊNCIAS <small>(Resumo)</small>
    </div>
    <div class="corpo-gerador-tabelas">
        <?php
        foreach ($operadora["odonto_carencias"] as $carencias) {
            echo ($carencias['descricao'] != null) ? nl2br($carencias['descricao']) : "<div class='info'>Consulte Operadora</div>";
        }
        ?>
    </div>
    <div>&nbsp;</div>
    <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
        ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
    </div>
    <div class="corpo-gerador-tabelas">
        <div>
            <b>ÁREA DE COMERCIALIZAÇÃO:</b>
        </div>
        <div>
            <?php
            foreach ($operadora["odonto_comercializacoes"] as $comercializacao) {
                echo ($comercializacao['descricao'] != null) ? nl2br($comercializacao['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
        <div>&nbsp;</div>
        <div>
            <b>ÁREA DE ATENDIMENTO</b>
        </div>
        <?php
        $dados = array();
        foreach ($tabelas as $tabela) {
            // 		    debug($tabela);die();
            $dados[$tabela["odonto_produto"]["nome"]] = $tabela["odonto_atendimento"]["descricao"];
        }
        foreach ($dados as $produto => $atendimento) {
            echo "<div><b>" . $produto . ":</b>" . " " . $atendimento . "</div>";
        }
        ?>


    </div>
    <div>&nbsp;</div>
    <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
        DOCUMENTOS NECESSÁRIOS
    </div>
    <div class="corpo-gerador-tabelas">
        <?php
        foreach ($operadora["odonto_documentos"] as $documentos) {
            echo ($documentos['descricao'] != null) ? nl2br($documentos['descricao']) : "<div class='info'>Consulte Operadora</div>";
        }
        ?>
    </div>
</div>
<!-- </page> -->
<script type="text/javascript">
    $('#myTabs a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $('#myTabs a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $("#aaa").click(function() {
        $.ajax({
            type: "POST",
            url: baseUrl + "tabelasGeradas/pdf",
            data: $("#gerador").serialize(),
            success: function(data) {
                $("#ss").empty();
                $("#ss").append(data);
            }
        });
    });
    $("#shared").submit(function() {
        $("#loader").click();
        return true;
    });
    $("#shared").bind('ajax:complete', function() {
        $("#fechar").click();
    });
    $(".url").click(function() {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(this).text()).select();
        document.execCommand("copy");
        $temp.remove();
    });
    $("#pdfdownload").click(function() {
        $("#tipo").val("D");
        setInterval(function() {
            $('#fechar').click();
        }, 50000);
    });

    $("#pdfbrowser").click(function() {
        $("#tipo").val("I");
        setInterval(function() {
            $('#fechar').click();
        }, 50000);
    });

    $("#whatsapp").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        let shared = "<?= $sharedWhats ?>";
        if (sessao != "") {
            window.open(shared);
        } else {
            $("#modal-login").modal("show");
        }

    });
    $("#btn-email").click(function(e) {
        e.preventDefault();
        let sessao = '<?= $sessao["role"] ?>';
        if (sessao != "") {
            $("#modal-compartilhamento").modal("show");
        } else {
            $("#modal-login").modal("show");
        }
    });
</script>
<div class="spacer-md">&nbsp;</div>