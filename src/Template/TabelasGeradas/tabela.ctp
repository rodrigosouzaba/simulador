<?= $this->element('new_acoes', ['idTabela']) ?>
<?php if ($operadora['status'] == 'OCULTA' && $operadora['status'] == 'EM ATUALIZAÇÃO') : ?>
    <div class="centralizada col-xs-12 text-danger">
        <h1>EM ATUALIZAÇÃO</h1>
    </div>
<?php elseif ($operadora['status'] == 'INATIVA') : ?>
    <div class="centralizada col-xs-12 text-danger">
        <h1>COMERCIALIZAÇÃO SUSPENSA</h1>
    </div>
<?php else : ?>
    <?php if (isset($tabelasOrdenadas) && $tabelasOrdenadas != '') { ?>
        <?= $this->Form->create('download-pdf', ['id' => 'download-pdf', 'url' => ['action' => 'pdf',]]); ?>
        <?= $this->element("cabecalho_tabelas_personalizadas"); ?>
        <div class="col-xs-12" style="background-color: #004057; color: #ffffff;  padding: 5px 0; text-align: center; font-weight: bold; ">
            <?php
            $dadosOperadora = $operadora["nome"] . " " . $operadora["detalhe"] . " ";
            switch ($dados["ramo_id"]) {
                case "SPJ":
                    echo $dadosOperadora . $vidas[0] . " A " . $vidas[1] . " VIDAS";
                    break;
                case "SPF":
                    echo $dadosOperadora;
                    break;
                case "OPF":
                    echo $dadosOperadora;
                    break;
                case "OPJ":
                    echo $dadosOperadora . $vidas[0] . " A " . $vidas[1] . " VIDAS";
                    break;
            }
            ?>
        </div>

        <?php foreach ($tabelasOrdenadas as $tabelasOrdenadas) { ?>


            <table style="width: 100%; border-collapse:collapse;" class="table table-condensed table-striped table-bordered">
                <tr style="width: 100%;">
                    <td style="max-width: 50px !important;">&nbsp;</td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $cobertura => $acomodacao) {
                    ?>
                            <td class="titulo-tabela centralizada" style="margin: 3px; text-align: center;" colspan="<?= array_sum(array_map("count", $acomodacao)) ?>">
                                <?= $produto ?>
                            </td>
                    <?php
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="max-width: 50px !important">&nbsp;</td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $tipos_produto) {

                        foreach ($tipos_produto as $cobertura => $acomodacoes) {

                    ?>
                            <td class="centralizada titulo-tabela" colspan="<?= array_sum(array_map("count", $acomodacoes)) ?>">
                                <?= $cobertura ?>
                            </td>
                    <?php
                        }
                    }
                    ?>
                </tr>

                <tr>
                    <td style="max-width: 50px !important">&nbsp;</td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as $acomodacao => $co) {
                    ?>
                                <td class="centralizada titulo-tabela" colspan="<?= count($co, COUNT_RECURSIVE) ?>">
                                    <strong> <?= ($acomodacao == 'AMBULATORIAL' ? "SEM ACOMODAÇÃO" : $acomodacao) ?></strong>
                                </td>
                    <?php
                            }
                        }
                    }
                    ?>
                <tr>
                    <td style="max-width: 50px !important">&nbsp;</td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {

                    ?>
                                    <td class="centralizada">
                                        <?php

                                        $c = explode("-", $coparticipacao); ?>
                                        <b><?= $tabela['nome']; ?></b><br>
                                        <small style="font-weight: 100 !important">
                                            <?= substr($c[1], 0, 3) != "SEM" ? "COM " . $c[1] : $c[1] ?><br>
                                            <?php
                                            if ($this->request->data("ramo_id") === "SPJ") {
                                                $i = 1;
                                                foreach ($tabela["tabelas_cnpjs"] as $cnpj) {
                                                    if ($i < count($tabela["tabelas_cnpjs"])) {
                                                        echo $cnpj["cnpj"]["nome"] . ",";
                                                    } else {
                                                        echo $cnpj["cnpj"]["nome"] . "<br>";
                                                    }
                                                    $i++;
                                                }
                                            }
                                            if ($tabela["cod_ans"]) {
                                                echo "Cód. ANS: " . $tabela["cod_ans"];
                                            }
                                            ?>
                                        </small>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td style="max-width: 50px !important" class="centralizada">até 18 anos</td>
                    <?php
                    foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) {
                    ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check1'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa1"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php
                                }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td class="centralizada" style="max-width: 50px !important">19 a 23 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) { ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check2'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa2"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td class="centralizada" style="max-width: 50px !important">24 a 28 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) { ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check3'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa3"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td class="centralizada" style="max-width: 50px !important">29 a 33 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) { ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check4'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa4"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td class="centralizada" style="max-width: 50px !important">34 a 38 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) { ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check5'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa5"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td class="centralizada" style="max-width: 50px !important">39 a 43 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) { ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check6'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa6"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td class="centralizada" style="max-width: 50px !important">44 a 48 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) { ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check7'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa7"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td class="centralizada" style="max-width: 50px !important">49 a 53 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) { ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check8'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa8"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td class="centralizada" style="max-width: 50px !important">54 a 58 anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) { ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check9'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa9"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php }
                            }
                        }
                    } ?>
                </tr>
                <tr>
                    <td class="centralizada" style="max-width: 50px !important">59 ou + anos</td>
                    <?php foreach ($tabelasOrdenadas as $produto => $cobertura) {
                        foreach ($cobertura as $acomodacoes) {
                            foreach ($acomodacoes as  $acomodacao => $coparticipacoes) {
                                foreach ($coparticipacoes as  $coparticipacao => $tabela) { ?>
                                    <td class="centralizada">
                                        <?php if ($tabela['check10'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php elseif ($tabela['atualizacao'] == 1) : ?>
                                            <strong>Comercialização suspensa</strong>
                                        <?php else : ?>
                                            <?= $this->Number->format($tabela["faixa10"], ['before' => 'R$ ', 'places' => 2]) ?>
                                        <?php endif; ?>
                                    </td>
                    <?php }
                            }
                        }
                    } ?>
                </tr>


            </table>
        <?php }
        switch ($dados["ramo_id"]) {
            case "SPF":
                $forma_pagamentos = "pf_formas_pagamentos";
                $obsD = "pf_observacoes";
                $dependentesD = "pf_dependentes";
                $redes = "pf_redes";
                $reembolsos = "pf_reembolsos";
                $carenciasD = "pf_carencias";
                $comercializacoes = "pf_comercializacoes";
                $atendimentos = "pf_atendimento";
                $documentosD = "pf_documentos";
                $produtosD = "pf_produto";
                break;
            case "SPJ":
                $forma_pagamentos = "formas_pagamentos";
                $obsD = "observacoes";
                $dependentesD = "opcionais";
                $redes = "redes";
                $reembolsos = "reembolsos";
                $carenciasD = "carencias";
                $comercializacoes = "regioes";
                $atendimentos = "abrangencia";
                $documentosD = "informacoes";
                $produtosD = "produto";
                break;
        }

        ?>

        <?php
        // 	 debug($entidades);
        if (isset($entidades) && $entidades <> null) { ?>
            <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
                ENTIDADES E PROFISSÕES
            </div>
            <div class="corpo-gerador-tabelas">
                <?php
                foreach ($entidades as $pfentidadespfoperadora) {
                    echo "<b>" . $pfentidadespfoperadora["pf_entidade"]["nome"] . ": </b>";
                    $totalProfissoes = count($pfentidadespfoperadora["pf_entidade"]["pf_entidades_profissoes"]);
                    $indice = 1;
                    foreach ($pfentidadespfoperadora["pf_entidade"]["pf_entidades_profissoes"] as $profissao) {
                        if ($indice < $totalProfissoes) {
                            echo $profissao["pf_profisso"]["nome"] . ", ";
                        } else {
                            echo $profissao["pf_profisso"]["nome"] . " ";
                        }
                        $indice++;
                    }
                }
                ?>
            </div>
            <div>&nbsp;</div>
        <?php } ?>

        <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            OBSERVAÇÕES IMPORTANTES
        </div>
        <div class="corpo-gerador-tabelas">
            <strong>Esta simulação poderá variar de acordo com o perfil do cliente, a critério da operadora. Preços, condições e regras de aceitação, estão sujeitas a confirmação da operadora no processo de implantação do contrato.</strong><br><br>
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$obsD] as $obs) {
                echo ($obs['descricao'] != null) ? nl2br($obs['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
        <div>&nbsp;</div>
        <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            REDE REFERENCIADA<small> (Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$redes] as $rede) {
                echo ($rede['descricao'] != null) ? nl2br($rede['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
        <div>&nbsp;</div>
        <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            OPCIONAIS, DIFERENCIAIS E REEMBOLSOS <small>(Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$reembolsos] as $diferenciais) {
                echo ($diferenciais['descricao'] != null) ? nl2br($diferenciais['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
        <div>&nbsp;</div>
        <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            ÁREA DE COMERCIALIZAÇÃO E ATENDIMENTO
        </div>
        <div class="corpo-gerador-tabelas">
            <div>
                <b>ÁREA DE COMERCIALIZAÇÃO:</b>
            </div>
            <div>
                <?php
                foreach ($operadora[$comercializacoes] as $comercializacao) {
                    echo ($comercializacao['descricao'] != null) ? nl2br($comercializacao['descricao']) : "<div class='info'>Consulte Operadora</div>";
                }
                ?>
            </div>
            <div>&nbsp;</div>
            <div>
                <b>ÁREA DE ATENDIMENTO</b>
            </div>
            <?php
            $dadosT = array();

            foreach ($tabelas as $tabela) {
                $dadosT[$tabela[$produtosD]["nome"]] = $tabela[$atendimentos]["descricao"];
            }
            foreach ($dadosT as $produto => $atendimento) {
                echo "<div><b>" . $produto . ":</b>" . " " . $atendimento . "</div>";
            }
            ?>
        </div>
        <div>&nbsp;</div>
        <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            CARÊNCIAS <small>(Resumo)</small>
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$carenciasD] as $carencias) {
                echo ($carencias['descricao'] != null) ? nl2br($carencias['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
        <div>&nbsp;</div>
        <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            DEPENDENTES
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            // 	    debug($operadora);
            foreach ($operadora[$dependentesD] as $dependentes) {
                echo ($dependentes['descricao'] != null) ? nl2br($dependentes['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
        <div>&nbsp;</div>

        <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            DOCUMENTOS NECESSÁRIOS
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$documentosD] as $documentos) {
                echo ($documentos['descricao'] != null) ? nl2br($documentos['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
        <div>&nbsp;</div>
        <div class="col-xs-12" style="width: 100%; background-color: #ccc; text-align: center;font-weight: bold; padding: 5px;font-size: 90%">
            FORMAS DE PAGAMENTOS
        </div>
        <div class="corpo-gerador-tabelas">
            <?php
            foreach ($operadora[$forma_pagamentos] as $pgto) {
                echo ($pgto['descricao'] != null) ? nl2br($pgto['descricao']) : "<div class='info'>Consulte Operadora</div>";
            }
            ?>
        </div>
    <?php } else {
        echo "<div class='col-xs-12'>Nenhuma Tabela se aplica as Características selecionadas</div>";
    } ?>
    <script type="text/javascript">
        $('#myTabs a').click(function(e) {
            e.preventDefault()
            $(this).tab('show')
        });
        $("#aaa").click(function() {
            $.ajax({
                type: "POST",
                url: baseUrl + "tabelasGeradas/pdf",
                data: $("#gerador").serialize(),
                success: function(data) {
                    $("#ss").empty();
                    $("#ss").append(data);
                }
            });
        });
    </script>

<?php
endif;
?>
<div class="spacer-md">&nbsp;</div>