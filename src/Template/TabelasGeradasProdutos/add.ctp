<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Tabelas Geradas Produtos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas Geradas'), ['controller' => 'TabelasGeradas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabelas Gerada'), ['controller' => 'TabelasGeradas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Produtos'), ['controller' => 'Produtos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Produto'), ['controller' => 'Produtos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tabelasGeradasProdutos form large-9 medium-8 columns content">
    <?= $this->Form->create($tabelasGeradasProduto) ?>
    <fieldset>
        <legend><?= __('Add Tabelas Geradas Produto') ?></legend>
        <?php
            echo $this->Form->input('tabela_gerada_id', ['options' => $tabelasGeradas]);
            echo $this->Form->input('produto_id', ['options' => $produtos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
