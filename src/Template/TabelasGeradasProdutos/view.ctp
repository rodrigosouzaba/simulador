<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tabelas Geradas Produto'), ['action' => 'edit', $tabelasGeradasProduto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tabelas Geradas Produto'), ['action' => 'delete', $tabelasGeradasProduto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasGeradasProduto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas Geradas Produtos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabelas Geradas Produto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas Geradas'), ['controller' => 'TabelasGeradas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabelas Gerada'), ['controller' => 'TabelasGeradas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Produtos'), ['controller' => 'Produtos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Produto'), ['controller' => 'Produtos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tabelasGeradasProdutos view large-9 medium-8 columns content">
    <h3><?= h($tabelasGeradasProduto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Tabelas Gerada') ?></th>
            <td><?= $tabelasGeradasProduto->has('tabelas_gerada') ? $this->Html->link($tabelasGeradasProduto->tabelas_gerada->id, ['controller' => 'TabelasGeradas', 'action' => 'view', $tabelasGeradasProduto->tabelas_gerada->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Produto') ?></th>
            <td><?= $tabelasGeradasProduto->has('produto') ? $this->Html->link($tabelasGeradasProduto->produto->id, ['controller' => 'Produtos', 'action' => 'view', $tabelasGeradasProduto->produto->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tabelasGeradasProduto->id) ?></td>
        </tr>
    </table>
</div>
