<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tabelasRegio->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasRegio->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tabelas Regioes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tabelasRegioes form large-9 medium-8 columns content">
    <?= $this->Form->create($tabelasRegio) ?>
    <fieldset>
        <legend><?= __('Edit Tabelas Regio') ?></legend>
        <?php
            echo $this->Form->input('tabela_id', ['options' => $tabelas]);
            echo $this->Form->input('regiao_id', ['options' => $regioes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
