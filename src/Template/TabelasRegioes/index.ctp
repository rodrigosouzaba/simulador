<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tabelas Regio'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tabelasRegioes index large-9 medium-8 columns content">
    <h3><?= __('Tabelas Regioes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('tabela_id') ?></th>
                <th><?= $this->Paginator->sort('regiao_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tabelasRegioes as $tabelasRegio): ?>
            <tr>
                <td><?= $this->Number->format($tabelasRegio->id) ?></td>
                <td><?= $tabelasRegio->has('tabela') ? $this->Html->link($tabelasRegio->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $tabelasRegio->tabela->id]) : '' ?></td>
                <td><?= $tabelasRegio->has('regio') ? $this->Html->link($tabelasRegio->regio->id, ['controller' => 'Regioes', 'action' => 'view', $tabelasRegio->regio->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tabelasRegio->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tabelasRegio->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tabelasRegio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasRegio->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
