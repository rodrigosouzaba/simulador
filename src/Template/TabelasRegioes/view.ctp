<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tabelas Regio'), ['action' => 'edit', $tabelasRegio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tabelas Regio'), ['action' => 'delete', $tabelasRegio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tabelasRegio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas Regioes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabelas Regio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tabela'), ['controller' => 'Tabelas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Regioes'), ['controller' => 'Regioes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Regio'), ['controller' => 'Regioes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tabelasRegioes view large-9 medium-8 columns content">
    <h3><?= h($tabelasRegio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Tabela') ?></th>
            <td><?= $tabelasRegio->has('tabela') ? $this->Html->link($tabelasRegio->tabela->id, ['controller' => 'Tabelas', 'action' => 'view', $tabelasRegio->tabela->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Regio') ?></th>
            <td><?= $tabelasRegio->has('regio') ? $this->Html->link($tabelasRegio->regio->id, ['controller' => 'Regioes', 'action' => 'view', $tabelasRegio->regio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tabelasRegio->id) ?></td>
        </tr>
    </table>
</div>
