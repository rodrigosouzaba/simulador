<?=

$this->Form->create($tipo, ['class' => "form-inline"]) ?>

<div class="col-md-12 fonteReduzida">
    <?php
    echo $this->Form->input('nome');
    echo $this->Form->input('descricao');
   ?>
</div>
<div class="col-md-12 ">
    <div class="spacer-lg"></div>
    <div class="col-md-12 tituloField">
        Este Tipo possue algum Sub-Produto associado a ele?
    </div>
    <div class="col-md-12 fonteReduzida">
        <?= $this->Form->radio('flag', ['1' => ' Sim', '0'=> ' Não'], ['label' => 'Teste']);?>
    </div>

</div>

<div class="col-md-12 fonteReduzida centralizada">
    <div class="spacer-lg"></div>
    <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-save', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => 'btn btn-md btn-primary']) ?>
    <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['action' => 'index'], ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false]);?>
</div>
<?= $this->Form->end() ?>