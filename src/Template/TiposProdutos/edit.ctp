<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tiposProduto->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tiposProduto->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="tiposProdutos form large-9 medium-8 columns content">
    <?= $this->Form->create($tiposProduto) ?>
    <fieldset>
        <legend><?= __('Edit Tipos Produto') ?></legend>
        <?php
            echo $this->Form->input('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
