<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tiposProdutos index large-9 medium-8 columns content">
    <h3><?= __('Tipos Produtos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tiposProdutos as $tiposProduto): ?>
            <tr>
                <td><?= $this->Number->format($tiposProduto->id) ?></td>
                <td><?= h($tiposProduto->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tiposProduto->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tiposProduto->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tiposProduto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tiposProduto->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
