<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tipos Produto'), ['action' => 'edit', $tiposProduto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tipos Produto'), ['action' => 'delete', $tiposProduto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tiposProduto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tipos Produtos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipos Produto'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tiposProdutos view large-9 medium-8 columns content">
    <h3><?= h($tiposProduto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($tiposProduto->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tiposProduto->id) ?></td>
        </tr>
    </table>
</div>
