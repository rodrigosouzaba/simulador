<style>
    ::-webkit-input-placeholder {
        color: #000;
    }

    :-moz-placeholder {
        /* Firefox 18- */
        color: #000;
        opacity: 100%;
    }

    ::-moz-placeholder {
        /* Firefox 19+ */
        color: #000;
    }

    :-ms-input-placeholder {
        color: #000;
    }
</style>

<h3 class="text-center">Cadastre seu Usuário</h3>

<?= $this->Form->create($user, ['id' => 'add', 'class' => 'form-inline']) ?>
<div class="col-md-12"><?= $this->Form->hidden('tipo', ['value' => 'PF']); ?></div>

<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('username', ["label" => false, 'placeholder' => 'Login (CPF)', 'class' => "validateCpf add centralizada"]); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('data_nascimento', ["label" => false, 'placeholder' => 'Data de Nascimento', 'type' => 'text', 'class' => 'menorIdade centralizada']); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('nome', ["label" => false, "class" => "centralizada", "placeholder" => "Nome"]); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('sobrenome', ['required' => 'required', "class" => "centralizada", "label" => false, "placeholder" => "Sobrenome"]); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('estado_id', ["label" => false, 'empty' => 'ESTADO (UF)', 'required' => 'required', 'class' => 'centralizada', 'style' => 'color: #666 !important;']); ?>
</div>
<div class="col-xs-6" id="div-municipios">
    <?= $this->Form->input('municipio', ["label" => false, "empty" => "Municipio", "class" => "centralizada", "disabled" => true, "options" => "", "style" => "margin-bottom: 1rem;", 'required' => 'required']); ?>
</div>

<div class="col-xs-6">
    <?= $this->Form->input('email', ['onkeyup' => '(this).value=(this).value.toLowerCase();', "label" => false, "placeholder" => "E-mail", "class" => "centralizada"]); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('celular', ["label" => false, "placeholder" => "DDD + Celular", 'class' => 'celular centralizada']); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('password', ["label" => false, 'placeholder' => "Senha", 'title' => 'Mínimo de 6 dígitos', "class" => "centralizada"]); ?>
</div>
<div class="col-xs-12 col-md-6">
    <?= $this->Form->input('confirmacao', ["class" => "centralizada", "label" => false, 'placeholder' => 'Confirme sua senha', 'title' => 'Mínimo de 6 dígitos', 'required' => 'required', 'type' => 'password']); ?>
</div>
<div class="col-xs-12  centralizada" style="margin-top: -12px !important; margin-bottom: 10px;">
    <small class="text-danger">Senha e Confirmação com no mínimo <b>6 dígitos</b></small>
</div>

<?= $this->Form->button('Cadastrar', ['class' => "btn btn-primary btn-md col-xs-12 col-md-2 col-md-offset-5", 'id' => 'salvar']) ?>

<?= $this->Form->hidden('role', ['value' => 'padrao']); ?>
<?= $this->Form->end() ?>



<script type="text/javascript">
    $("#myModalLabelCadastro").text('Cadastro de Novo Usuário');
    $(".celular").mask("(99) 9 9999-9999");
    $(".validateCpf").mask("999.999.999-99");
    $('.tipo-add').val("PF");

    $(".menorIdade").blur(function() {
        var dataNascimento = $(this).val();
        var res = dataNascimento.substring(6);
        //        alert(res);
        if (res !== '____') {
            if (res <= <?= date('Y') - 18 ?>) {} else {
                alert('É necessário possuir mais de 18 anos para utilizar o sistema');
                $(this).val('');
                //                $("#data-nascimento").focus();
            }
        }
    });

    $('#data-nascimento').mask("99/99/9999");
    $(".validateCpf").blur(function() {
        //        alert('Aqui');
        CPF = $(this).val();
        //        alert(CPF);
        if (CPF == '___.___.___-__') {
            return false;
        }
        if (!CPF) {
            return false;
        }
        erro = new String;
        cpfv = CPF;
        if (cpfv.length == 14 || cpfv.length == 11) {
            cpfv = cpfv.replace('.', '');
            cpfv = cpfv.replace('.', '');
            cpfv = cpfv.replace('-', '');

            var nonNumbers = /\D/;

            if (nonNumbers.test(cpfv)) {
                erro = "A verificacao de CPF suporta apenas números!";
            } else {
                if (cpfv == "00000000000" ||
                    cpfv == "11111111111" ||
                    cpfv == "22222222222" ||
                    cpfv == "33333333333" ||
                    cpfv == "44444444444" ||
                    cpfv == "55555555555" ||
                    cpfv == "66666666666" ||
                    cpfv == "77777777777" ||
                    cpfv == "88888888888" ||
                    cpfv == "99999999999") {

                    erro = "Número de CPF inválido!"
                }
                var a = [];
                var b = new Number;
                var c = 11;

                for (i = 0; i < 11; i++) {
                    a[i] = cpfv.charAt(i);
                    if (i < 9)
                        b += (a[i] * --c);
                }
                if ((x = b % 11) < 2) {
                    a[9] = 0
                } else {
                    a[9] = 11 - x
                }
                b = 0;
                c = 11;
                for (y = 0; y < 10; y++)
                    b += (a[y] * c--);

                if ((x = b % 11) < 2) {
                    a[10] = 0;
                } else {
                    a[10] = 11 - x;
                }
                if ((cpfv.charAt(9) != a[9]) || (cpfv.charAt(10) != a[10])) {
                    erro = "Número de CPF inválido.";
                }
            }
        } else {
            if (cpfv.length == 0) {
                return false;
            } else {
                erro = "Número de CPF inválido.";
            }
        }
        if (erro.length > 0) {
            $(this).val('');
            alert(erro);
            setTimeout(function() {
                $(this).focus();
            }, 100);
            return false;
        }
        return $(this);
    });

    $(".pj").click(function() {
        $(".add").mask("99.999.999/9999-99");
        $("label[for*='username-add']").html("Login (CNPJ)*");
        $('.add').off("change");
        $('.tipo-add').val("PJ");
        //        alert('Zerou func e Entrou PJ');
        $('.add').change(function() {
            CNPJ = $(this).val();
            if (!CNPJ) {
                return false;
            }
            erro = new String;
            if (CNPJ == "00.000.000/0000-00") {
                erro += "CNPJ inválido\n\n";
            }
            CNPJ = CNPJ.replace(".", "");
            CNPJ = CNPJ.replace(".", "");
            CNPJ = CNPJ.replace("-", "");
            CNPJ = CNPJ.replace("/", "");

            var a = [];
            var b = new Number;
            var c = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
            for (i = 0; i < 12; i++) {
                a[i] = CNPJ.charAt(i);
                b += a[i] * c[i + 1];
            }
            if ((x = b % 11) < 2) {
                a[12] = 0
            } else {
                a[12] = 11 - x
            }
            b = 0;
            for (y = 0; y < 13; y++) {
                b += (a[y] * c[y]);
            }
            if ((x = b % 11) < 2) {
                a[13] = 0;
            } else {
                a[13] = 11 - x;
            }
            if ((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])) {
                erro += "Dígito verificador com problema!";
            }
            if (erro.length > 0) {
                $(this).val('');
                alert(erro);
                setTimeout(function() {
                    $(this).focus()
                }, 50);
            }
            return $(this);
        });
        //        $('#username').validacnpj();
    });
    $('#usernameAdd').focus();
    $("#estado-id").change(function() {
        let estado = $(this).val();

        $.ajax({
            type: "GET",
            url: "<?= $this->request->webroot . $this->request->controller ?>/FiltroMunicipio/" + estado,
            success: function(municipios) {
                $("#div-municipios").empty();
                $("#div-municipios").append(municipios);
            }
        });
    });
</script>
