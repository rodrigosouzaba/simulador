<div class="clearfix">&nbsp;</div>
<?= $this->Flash->render() ?>
<div class="col-md-4 col-md-offset-4 text-center">
    <?php if (file_exists(WWW_ROOT . "uploads/imagens/usuarios/logos/" . $sessao['imagem']['nome']) && $sessao['imagem_id'] != null) : ?>
        <?= $this->Html->image('../uploads/imagens/usuarios/logos/' . $sessao['imagem']['nome'], ['class' => 'custom-logo', 'alt' => 'Logo do usuário', 'id' => 'photoUser']) ?>
    <?php else : ?>
        <?= $this->Html->image('../uploads/imagens/usuarios/logos/default.png', ['class' => 'custom-logo', 'confirm' => 'Após a exclusão todos os seus dados serão excluidos e você não poderá realizar mais cotações. Tem certeza que deseja continuar?', 'alt' => 'Logo do usuário', 'id' => 'photoUser']) ?>
    <?php endif; ?>
</div>
<div class="col-md-6 col-md-offset-3 centralizada">
    <h4>Selecione o arquivo desejado e clique no botão enviar.</h4>
    <h6 style="line-height: 20px;">Arquivos personalizados de logotipos devem estar nos formatos <b>GIF, PNG, JPG ou JPEG. </b> <br />
        Tamanho máximo do arquivo <b>1MB.</b></h6>
    <?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>
    <?php echo $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id]); ?>
    <div class="upload-frm">
        <?php echo $this->Form->input('file', ['type' => 'file', 'label' => '', 'class' => 'centralizada']); ?>
    </div>
    <div class="upload-frm">
        <?php echo $this->Form->button(__('Enviar'), ['type' => 'submit', 'class' => 'btn btn-md btn-primary']); ?>
        <?= $this->Html->link('Excluir cadastro', '/users/delete/' . $user_id, ['class' => 'btn btn-md btn-danger', 'role' => 'button', 'escape' => false]) ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>