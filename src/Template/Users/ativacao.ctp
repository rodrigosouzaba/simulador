<style>
    ::-webkit-input-placeholder {
        color: #000;
    }

    :-moz-placeholder {
        /* Firefox 18- */
        color: #000;
        opacity: 100%;
    }

    ::-moz-placeholder {
        /* Firefox 19+ */
        color: #000;
    }

    :-ms-input-placeholder {
        color: #000;
    }
</style>
<?php $this->assign('title', $title); ?>
<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
// debug($user);
?>
<div class="col-md-12 users form centralizada">
    <div class="col-md-12">
        <?php

        if ($user['validacao_email'] <> "ATIVO") {
            $class = 'fa fa-remove text text-danger';
            $situacao = 'Ativação de e-mail pendente';
        } else {
            $class = 'fa fa-check text text-success';
            $situacao = 'Ativado';
        }

        if ($user['validacao'] <> "ATIVO") {
            $class_cel = 'fa fa-remove text text-danger';
            $situacao_cel = 'Ativação de celular pendente';
        } else {
            $class_cel = 'fa fa-check text text-success';
            $situacao_cel = 'ativado';
        }
        ?>
        <h3 class="">Bem-vindo(a), <?= $user['nome'] . " " . $user['sobrenome'] ?></h3>
    </div>
</div>
<div class="col-xs-12 users form centralizada ">
    <?= $this->Flash->render('auth') ?>

</div>
<div class=" col-xs-12 col-md-6 col-md-offset-3 centralizada">
    <div class=" col-xs-12 text-danger">
        É necessário ativar seu usuário<br>
        Digite abaixo os códigos enviados para o seu celular e e-mail</b>
    </div>
    <div class="col-xs-12 clearfix">&nbsp;</div>
    <?= $this->Form->create('user', ['id' => 'formativacaocel']) ?>
    <?= $this->Form->input('user-id', ['type' => 'hidden', 'value' => $userId]); ?>


    <!-- ATIVAÇÃO CELULAR -->
    <div class="col-xs-12 col-md-6">

        <?php if ($user['validacao'] <> "ATIVO") { ?>
            <div class="col-xs-12 ">
                <b><i class="<?= $class_cel ?>"></i></b> <?= $situacao_cel ?><br>
                <?= $user['celular'] ?>
            </div>
            <div style="margin: 5px 0;">
                <small>
                    Seu celular está incorreto? <?= $this->Html->link('Clique aqui', "#", ['role' => 'button', "data-toggle" => "modal", "data-target" => "#modal-celular", 'escape' => false, 'data-placement' => 'top', 'title' => 'Alterar Celular']) ?>
                </small>
            </div>
            <?= $this->Form->input('codigo-cel', ['placeholder' => 'Digite aqui código SMS', 'label' => '', 'class' => 'centralizada', "style" => 'margin-bottom: 0px']) ?>
            <div style="margin: 5px 0;">
                <small>
                    Não recebeu o código SMS? <?= $this->Html->link('Clique aqui', '#', ["id" => "reenvio", 'user_id' => $userId, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Reenviar Código SMS']) ?>
                </small>
            </div>
            <?= $this->Html->link('Ativar Celular', "#", ['class' => 'btn btn-sm btn-primary', "style" => "margin-bottom: 20px;", 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Ativar Celular', 'id' => 'ativar-cel']); ?>

        <?php } else { ?>
            <div class="text-success">
                <b>
                    <i class="fa fa-check text text-success fa-3x"></i>
                </b>
                <h3 style="margin-top: 10px;">Celular Ativado</h3>
                <h5><?= $user['celular'] ?></h5>

            </div>
        <?php     }
        ?>
    </div>

    <!-- ATIVAÇÃO E-MAIL -->
    <div class="col-xs-12 col-md-6">
        <?php if ($user['validacao_email'] <> "ATIVO") { ?>

            <span class="col-xs-12 ">
                <b> <i class="<?= $class ?>"></i></b> <?= $situacao ?><br />
                <?= $user['email'] ?>
            </span>
            <div style="margin: 5px 0;">
                <small>
                    Seu e-mail está incorreto? <?= $this->Html->link('Clique aqui', "#", ["id" => "reenvio", 'role' => 'button', 'escape' => false, "data-toggle" => "modal", "data-target" => "#modal-email", 'title' => 'Alterar E-mail']) ?>
                </small>
            </div>

            <?= $this->Form->input('codigo-email', ['placeholder' => 'Digite aqui código E-mail', 'label' => '', 'class' => 'centralizada', "style" => 'margin-bottom: 0px']) ?>
            <div style="margin: 5px 0;">
                <small>
                    Não recebeu o código e-mail? <?= $this->Html->link('Clique aqui', '#', ["id" => "reenvio-email", 'user_id' => $userId,  'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Reenviar Código E-mail']) ?>
                </small>
            </div>

            <?= $this->Html->link('Ativar E-mail', "#", ['class' => 'btn btn-sm btn-primary', "style" => "margin-bottom: 20px;", 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Ativar Cadastro', 'id' => 'ativar-email']); ?>
    </div>

<?php } else { ?>

    <div class="text-success">
        <b>
            <i class="fa fa-check text text-success fa-3x"></i>
        </b>
        <h3 style="margin-top: 10px;">E-mail Ativado</h3>
        <h5><?= $user['email'] ?></h5>

    </div>
<?php } ?>

</div>

<div class="col-xs-12 col-md-6 col-md-offset-3 centralizada">

    <?= $this->Html->link('Cancelar', ['action' => 'logout'], ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Voltar a Página Principal']) ?>

</div>
<div id="resposta-ativacao"></div>
<?= $this->Form->end() ?>


<!-- Modal CELULAR-->
<div class="modal fade" id="modal-celular" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button id="fecharcel" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Corrigir Celular</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->create('mudar-celular', ['id' => 'mudar-celular']) ?>
                <?= $this->Form->input('id', ['type' => 'hidden', 'value' => $userId]); ?>
                <?= $this->Form->input('novo-cel', ['placeholder' => 'Digite aqui o celular correto', 'label' => '', 'class' => 'centralizada celular', "style" => 'margin-bottom: 0px']) ?>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" id="salvar-celular">Salvar</button>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal EMAIL -->
<div class="modal fade" id="modal-email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Corrigir E-mail</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->create('mudar-email', ['id' => 'mudar-email', 'url' => ['action' => 'mudarEmail']]) ?>
                <?= $this->Form->input('id', ['type' => 'hidden', 'value' => $userId]); ?>
                <?= $this->Form->input('novo-email', ['placeholder' => 'Digite aqui o e-mail correto', "type" => "email", 'label' => '', 'class' => 'centralizada', "style" => 'margin-bottom: 0px']) ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" id="btn-muda-email">Salvar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->element("processando"); ?>

<script type="text/javascript">
    $(document).ready(function() {
        var email = '<?= $user["validacao"] ?>';
        var celular = '<?= $user["validacao_email"] ?>';
        // 		alert(email+" " + celular);
        if (email == 'ATIVO' && celular == 'ATIVO') {
            window.location = '<?= $this->request->webroot ?>users/central/';
        }
    });
    $('#modal-celular').on('shown.bs.modal', function() {
        $('#novo-cel').focus()
    });


    $(".celular").mask("(99) 9 9999-9999");

    $("#reenvio").click(function() {
        $("#reenvio").attr('disabled', true);
        let user_id = $(this).attr('user_id');
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>users/reenviarCodigo/" + user_id
        }).then(function() {
            setTimeout(function() {
                $("#reenvio").attr('disabled', false);
            }, 60000)

        })
    });
    $("#reenvio-email").click(function() {
        $("#reenvio-email").attr('disabled', true);
        let user_id = $(this).attr('user_id');
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>users/reenviarCodigoEmail/" + user_id
        }).then(function() {
            setTimeout(function() {
                $("#reenvio-email").attr('disabled', false);
            }, 60000)

        })
    })

    $("#ativar-cel").click(function() {
        $("#loader").click();
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>users/ativacaocel/" + $("#user-id").val() + "/" + $("#codigo-cel").val(),
            success: function(data) {
                $("#fechar").click();
                $("#resposta-ativacao").empty();
                $("#resposta-ativacao").append(data);
            }

        });
    });
    $("#ativar-email").click(function() {
        $("#loader").click();
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>users/ativacaoemail/" + $("#user-id").val() + "/" + $("#codigo-email").val(),
            success: function(data) {
                $("#fechar").click();
                $("#resposta-ativacao").empty();
                $("#resposta-ativacao").append(data);
            }

        });
    });

    $("#salvar-celular").click(function() {
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>users/mudarcel/",
            data: $("#mudar-celular").serialize(),
            success: function(data) {
                alert("Celular alterado com Sucesso");
                $("#fecharcel").trigger("click");
                location.reload();
                /*
                		           $("#respostaProdutos").empty();
                		           $("#respostaProdutos").append(data);
                */
            }
        });
    });
    /*
        $("#ativacao").click(function () {
            $.ajax({
                type: "post",
                data: $("#ativacao").serialize(),
                url: "<?php echo $this->request->webroot ?>users/ativacao/" + "<?= $userId ?>",
            });
        });
    */
    $("#btn-muda-email").click(function() {
        $("#mudar-email").submit();
    });
</script>