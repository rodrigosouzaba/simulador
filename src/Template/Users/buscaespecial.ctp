<?php
/*
$primeiroDia[0]['data'] = $primeiroDia[0]['data']->format('Y-m-d');
$diff = abs(strtotime($primeiroDia[0]['data']) - strtotime(date('Y-m-d')));
$dias = floor($diff / (60 * 60 * 24)) / 30 * 20;
*/

if (!empty($users->toArray())) {
foreach ($users as $user):
        ?>
       <tr>
                    <td class="centralizada" style="vertical-align: middle"><small><?= $user->created ?></small></td>
                    <td class="centralizada"  style="vertical-align: middle">
                        <small><?= $this->Time->format($user->ultimologin, 'dd/MM/YY  HH:mm') ?></small>
                    </td>
                    <td  style="vertical-align: middle">
                        <?php if (isset($user->imagem_id) && !empty($user->imagem_id)) { ?>
                            <span style="font-size: 90%" data-toggle="popover" 
                                  data-trigger="hover"  
                                  data-placement="top" 
                                  data-html="true"
                                  data-content="<img src='/<?= $user->imagem_caminho . $user->imagem_nome ?>'/>" 
                                  ><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                              <?php }
                              ?>
                              <?=
                              ucwords(strtolower($user->nome)) . " " .
                              ucwords(strtolower($user->sobrenome)) .
                              "<br><small style='font-size:80%'>" .
                              $user->username . "<br>" .
                              $user->estado_nome . "<br>" .
                              $user->celular . "<br>" .
                              $user->email
                              . "</small>"
                              ?>
                    </td>
                    <?php //debug($user); ?>


                    <td  style="vertical-align: middle">

                        <small>
                            Saúde PJ: <?= $user->calculos_pme ?><br>
                            Saúde PF: <?= $user->calculos_pf ?><br>
                            Odonto PJ: <?= $user->calculos_odonto_pj ?><br>
                            Odonto PF: <?= $user->calculos_odonto_pf ?><br>
                            Tabelas: <?= $user->total_tabelas_geradas ?><br>
                        </small>
                    </td>
                    <td  style="vertical-align: middle">
                        <small>
                            Saúde PJ: <?= $user->calculosdia_pme ?><br>
                            Saúde PF: <?= $user->calculosdia_pf ?><br>
                            Odonto PJ: <?= $user->calculosdia_odonto_pj ?><br>
                            Odonto PF: <?= $user->calculosdia_odonto_pf ?><br>
                            Tabelas: <?= $user->tabelas_geradas_dia ?>
                        </small>
                    </td>
                    <td class="actions"  style="vertical-align: middle;line-height: 2px !important">
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-edit', 'aria-hidden' => 'true']), ['action' => 'edit', $user->id], ['class' => 'btn btn-xs btn-default cod-celular', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Usuário', "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                        <br>
                        <!-- ATIVACAO CELULAR -->
                        <?php
                        switch ([$user->validacao]) {
                            case ['ATIVO']:
                                $icone = "fa fa-check";
                                $cor = "success";
                                $title = 'CELULAR ATIVADO';
                                break;
                            case ['INATIVO']:
                                $icone = "fa fa-minus";
                                $cor = "default";
                                $title = 'ATIVAÇÃO PENDENTE<br/><small>Celular inativo</small>';
                                break;
                            case ['PENDENTE']:
                                $icone = "fa fa-times";
                                $cor = "warning";
                                $title = 'REATIVAÇÃO PENDENTE<br/><small>Celular alterado</small>';
                                break;
                        }
                        ?>
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => $icone, 'aria-hidden' => 'true']), ['action' => 'reenviarCodigo', $user->id], ['class' => 'cod-email btn btn-xs btn-' . $cor, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top','data-html'=>"true", 'title' => $title, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                        <br> 
                        <!-- /ATIVACAO CELULAR -->
                        
                        <!-- ATIVACAO E-MAIL -->
                        <?php
                        switch ([$user->validacao_email]) {
                            case ['ATIVO']:
                                $iconeEmail = "fa fa-check";
                                $corEmail = "success";
                                $titleEmail = 'E-MAIL ATIVADO';
                                break;
                            case ['INATIVO']:
                                $iconeEmail = "fa fa-minus";
                                $corEmail = "default";
                                $titleEmail = 'INATIVO<br/><small>E-mail inativo</small>';
                                break;
                            case ['PENDENTE']:
                                $iconeEmail = "fa fa-times";
                                $corEmail = "warning";
                                $titleEmail = 'REATIVAÇÃO PENDENTE<br/><small>E-mail alterado</small>';
                                break;
                        }
                        ?>
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => $iconeEmail, 'aria-hidden' => 'true']), ['action' => 'reenviarCodigoEmailAdmin', $user->id], ["value" => $user->id,'class' => 'btn btn-xs btn-' . $corEmail, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top','data-html'=>"true", 'title' => $titleEmail, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                        <!-- /ATIVACAO E-MAIL -->
                        
                        <br>        
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-phone', 'aria-hidden' => 'true']), ['action' => 'recuperarSenhaAdmin', $user->id], ['class' => 'btn btn-xs btn-info', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Senha Provisória SMS', "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                        <br>          
                        <?=
                        $this->Html->link(
                                $this->Html->tag('span', '', ['class' => 'fa fa-envelope ', 'aria-hidden' => 'true']), ['action' => 'recuperarSenhaEmail', $user->id], ['class' => 'btn btn-xs btn-info', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Senha Provisória EMAIL', "style" => "min-width: 24px !important; margin:1px 0 !important"]);
                        ?>
                        <br>          
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']), '#', ['class' => 'btn btn-xs btn-danger deletar', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Excluir Usuário', 'value' => $user->id, "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                        <br>
                        <?php
	                        switch($user->bloqueio){
		                        case "S":
		                        	$iconeBloqueio = "fa fa-lock";
		                        	$acaoBloqueio = "DESBLOQUEIO";
		                        	$title = "Desbloquear Usuário";
		                        break;
		                        case "N":
			                        $acaoBloqueio = "BLOQUEIO";
		                        	$iconeBloqueio = "fa fa-unlock";
		                        	$title = "Bloquear Usuário";		                        	
		                        break;
	                        }
// 	                        debug($user);die();
                        ?>          
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => $iconeBloqueio, 'aria-hidden' => 'true']), '#', ['class' => 'btn btn-xs btn-default bloqueio', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $title, 'value' => $user->id,"acao" => $acaoBloqueio, "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                    </td>

                </tr>


        <?php
    endforeach;
} else {
    ?>
    <tr>
        <td colspan="7" class="centralizada">Nenhum usuário encontrado</td>
    </tr>
<?php }
?>


<script type="text/javascript">
    $(document).ready(function () {
        $("body").tooltip({selector: '[data-toggle=tooltip]'});
        $('#togle').tooltip();
          $('[data-toggle="popover"]').popover();
          
//           $("#estado-id :selected").text()
          $("#estatisticas").empty();
          $("#estatisticas").append("<div class='col-xs-12 centralizada'>&nbsp;</div><div class='col-xs-12'><div class='col-xs-12 col-md-2 col-md-offset-5 centralizada' style='margin-bottom: 10px;;color: #fff;background-color: #000 !important;font-size: 100% !important;'>Usuários Ativos: <?= count($users->toArray()) ?></div></div><div class='col-xs-12 centralizada'><div class='stats col-xs-12 col-md-2 col-md-offset-1' style='background-color: #eee'>Saúde PME: <?= $totais["calculosSaudePJ"] ?><br>Últimos 30 dias: <?= $totais["calculosMesSaudePJ"]?></div><div class='stats col-xs-12 col-md-2 ' style='background-color: #ccc'>Saúde PF: <?= $totais["calculosSaudePF"] ?><br>Últimos 30 dias: <?= $totais["calculosMesSaudePF"]?></div><div class='stats col-xs-12 col-md-2' style='background-color: #eee'>Odonto PME: <?= $totais["calculosOdontoPJ"] ?><br>Últimos 30 dias: <?= $totais["calculosMesOdontoPJ"]?></div><div class='stats col-xs-12 col-md-2' style='background-color: #ccc'>Odonto PF: <?= $totais["calculosOdontoPF"] ?><br>Últimos 30 dias: <?= $totais["calculosMesOdontoPF"]?></div><div class='stats col-xs-12 col-md-2' style='background-color: #eee'>Tabelas Geradas: <?= $totais["tabelasGeradas"] ?><br>Últimos 30 dias: <?= $totais["tabelasGeradasMes"]?></div></div></div>");
          
    });
     $(".bloqueio").click(function () {
//        alert($(this).attr('value'));
//        var a = <?= $this->request->here; ?>;
//        alert(a);
		var acao = $(this).attr('acao');
		var id = $(this).attr('value');
            $.ajax({
                type: "post",
                url: "<?php echo $this->request->webroot ?>users/bloqueio/" + id + "/" + acao,
                success: function (data) {
                    alert(acao +' realizado com sucesso');
//                    $("#status_usuario").change();
                    location.reload();
//                    window.location.replace("<?php echo $this->request->webroot ?>users/delete/" + $(this).attr('value'));
                }
            });
       });

</script>