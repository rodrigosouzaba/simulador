<?= $this->Form->create('simulacao', ['id' => 'simulacao']) ?>
<!--<div class="col-xs-6 col-xs-offset-3 ">-->
    <div id="pagina" class="col-md-10 col-md-offset-1 col-xs-12">
    <div class="col-md-12 ">

        <div class="tituloField">
            Dados da Empresa
        </div>
        <fieldset>
            <div class="col-md-3">
                <?= $this->Form->input('nome', ['label' => 'Nome da Empresa / Pessoa (opcional)']); ?>            
            </div>
            <div class="col-md-3">
                <?= $this->Form->input('contato', ['label' => 'Nome do Contato (A/C) (opcional)']); ?>            
            </div>
            <div class="col-md-3">
                <?= $this->Form->input('email', ['label' => 'E-mail (opcional)']); ?>            
            </div>
            <div class="col-md-3">
                <?= $this->Form->input('telefone', ['label' => 'Telefone (opcional)']); ?>            
            </div>


        </fieldset>
    </div>

    <!-- Quantidade de Vidas --> 
    <!-- Quantidade de Vidas --> 
    <div class="col-md-12 qtdVidas">
        <div class="clearfix">&nbsp;</div>
        <div class="tituloField">
            Vidas  
        </div>
        <div class="col-md-12">
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa1', ['label' => '0 a 18 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa2', ['label' => '19 a 23 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa3', ['label' => '24 a 28 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa4', ['label' => '29 a 33 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa5', ['label' => '34 a 38 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa6', ['label' => '39 a 43 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>

            <div class="faixaEtaria col-md-1">
                <?= $this->Form->input('faixa7', ['label' => '44 a 48 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa8', ['label' => '49 a 53 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">
                <?= $this->Form->input('faixa9', ['label' => '54 a 58 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa10', ['label' => '59 a 64 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa11', ['label' => '65 a 80 anos', 'required' => false, 'class' => 'centralizada']); ?> 

            </div>
            <div class="faixaEtaria col-md-1">

                <?= $this->Form->input('faixa12', ['label' => '+ de 81 anos', 'required' => false, 'class' => 'centralizada']); ?>

            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">
        <div class="tituloField">
            Tipo CNPJ
        </div>
        <div class="col-md-3 form-inline">

            <?php 
//            debug($user);
            $tipos_cnpj = array('0' => 'CNPJ Padrão', '1' => 'CEI', '2' => 'MEI'); ?>



            <input name="tipo_cnpj_id" id='tipo_cnpj_id' value="" type="hidden">
            <div class="form-group">
                <label for="tipo-cnpj-id-0">
                    <input name="tipo_cnpj_id" value="0" checked="checked" id="tipo-cnpj-id-0" required="required" type="radio" class="form-control">CNPJ Padrão
                </label>
            </div>
            <div class="form-group">
                <label for="tipo-cnpj-id-1">
                    <input name="tipo_cnpj_id" value="1" id="tipo-cnpj-id-1" required="required" type="radio" class="form-control">CEI
                </label>
            </div>
            <div class="form-group">
                <label for="tipo-cnpj-id-2">
                    <input name="tipo_cnpj_id" value="2" id="tipo-cnpj-id-2" required="required" type="radio" class="form-control">MEI
                </label>      
            </div>
            <br/>
        </div>
        <div class="col-md-9">
            <?= $this->Form->input('familia', ['type' => 'checkbox', 'label' => 'Este Cálculo é referente a um Grupo Familiar (Cônjuge, Filhos, Pai ou Mãe)?']); ?>            

        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-12">
            <small class='text-info'><b>*</b>Informação necessária para evitar preços divergentes</small>
        </div>
    </div>

    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">
        <div class="tituloField">
            Paramêtros do Cálculo
        </div>

        <div class="col-md-4">
            <div class="tituloField">
                Ramo
            </div>

            <?= $this->Form->radio('ramo_id', $ramos, ['required' => 'required']); ?>
        </div>
        <div class="col-md-4">
            <div class="tituloField">
                Modalidade
            </div>
            <?php $disabled = array('1', '2'); ?>

            <input name="modalidade_id" value="" type="hidden">
            <label for="modalidade-id-3">
                <input name="modalidade_id" value="3" id="modalidade-id-3" required="required" type="radio">PME
            </label>  

        </div>
        <div class="col-md-4">
            <div class="tituloField">
                Estado de Comercialização
            </div>
            <?php $estados = ['5' => 'Bahia']; ?>
            <?= $this->Form->input('estado_id', ['options' => $estados, 'label' => '', 'class' => 'fonteReduzida']); ?>
        </div>

        <div class="clearfix">&nbsp;</div>
        <div class="col-md-12 centralizada">
            <?=
            $this->Html->link($this->Html->tag('span', '', [
                        'class' => 'fa fa-file-text-o',
                        'aria-hidden' => 'true']) . ' Enviar', '#', [
                'class' => 'btn btn-md btn-primary',
                'role' => 'button',
                'escape' => false,
                'data-toggle' => 'tooltip',
                'data-placement' => 'top',
                'title' => 'Enviar',
                'id' => 'simular'])
            ?>

        </div>               
    </div>
    <div class="clearfix">&nbsp;</div>
    <!-- Anexos a serem Inseridos --> 

    <div class="col-md-12" id="dadosSimulacao">

    </div>


</div>


<?= $this->Form->end() ?>

<!-- Button trigger modal -->
<button type="button" id="loader" class="btn btn-primary btn-lg hidden" data-toggle="modal" data-target="#myModal">
</button>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span style="margin-right: 20px !important;">Processando </span><img src="<?= $this->request->webroot?>img/spinner.gif" style='max-height: 60px;margin:-10px;' /> </h4> 
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    $('input:radio[name=ramo_id]:nth(0)').attr('checked', true);
    $('input:radio[name=modalidade_id]:nth(0)').attr('checked', true);
//    $("#telefone").mask("(99) 9 9999-9999");
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    });
    $(document).ready(function () {
    $("#simular").click(function () {
        $.ajax({
            type: "post",
            data: $("#simulacao").serialize(),
            url: "<?php echo $this->request->webroot ?>users/calculoremoto/<?= $userId?>",
            beforeSend: function () {
                    $('#loader').trigger('click');
                },
            success: function (data) {
            $('#close').trigger('click');
            $("#pagina").empty();
            $("#pagina").append(data);
            }
        
        });
    });

});

</script>
