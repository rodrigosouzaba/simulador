<section id="AppCalculos">
	<style>
		@media(max-width: 991px) {

			table thead {
				background: none;
				white-space: nowrap;
			}

			table {
				display: block;
				overflow-x: auto;
				white-space: nowrap;
			}
		}
	</style>
	<div class="col-xs-12 centralizada" style="margin-bottom: 10px;">
		<h4>GERENCIE SEUS NEGÓCIOS E AGENDE LEMBRETES<br>
			<small class="text-info" style="font-weight: bold"><?= count($calculos) ?> Oportunidades<br>
			</small>
		</h4>
	</div>

	<div class="col-xs-12">
		<div class="col-xs-12 col-md-3">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Localizar Nº do Cálculo" id="num-calculo">
				<span class="input-group-btn">
					<button type="button" class="btn btn-default" id="pesquisar-calculo" style="height: 37px"> <i class="fas fa-search"></i> </button>
				</span>
			</div>
		</div>
		<div class="col-xs-12 col-md-3">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Localizar Cliente" id="nome-cliente">
				<span class="input-group-btn">
					<button type="button" class="btn btn-default" id="pesquisar-cliente" style="height: 37px"> <i class="fas fa-search"></i> </button>
				</span>
			</div>
		</div>
		<div class="col-xs-12 col-md-3">
			<?= $this->Form->input("ramos", ["options" => $ramos,  "empty" => "FILTRAR POR RAMO", "label" => false]); ?>
		</div>
		<div class="col-xs-12 col-md-3">
			<?= $this->Form->input("status_id", ['id' => 'filter-status-id', "options" => $status, "empty" => "FILTRAR POR FASE DA NEGOCIAÇÃO", "label" => false]); ?>
		</div>
	</div>
	<div class="col-xs-12">&nbsp;</div>

	<div class="col-xs-12">


		<table>
			<thead>
				<tr>
					<th>Data</th>
					<th>Cálculo</th>
					<th>Ramo</th>
					<th>Cliente</th>
					<th>Fase da Negociação</th>
					<th style="width: 10%">Comentários</th>
				</tr>
			</thead>
			<tbody id="resultado">
				<?php
				foreach ($calculos as $calculo) {
					switch ($calculo["ramo"]) {
						case "SPF":
							$ramo = "Saúde Pessoa Física";
							$controller = "PfCalculos";
							$class = 'btnViewCalculo';
							$classEdit = 'editar-calculo';
							$coluna = "pf_calculo_id";
							break;
						case "SPJ":
							$ramo = "Saúde Pessoa Jurídica";
							$coluna = "simulacao_id";
							$class = 'btnViewCalculoPME';
							$classEdit = 'btnEditCalculoPME';
							$controller = "Simulacoes";
							break;
						case "OPF":
							$ramo = "Odonto Pessoa Física";
							$controller = "OdontoCalculos";
							$class = 'btnViewCalculoOdonto';
							$classEdit = 'btnEditCalculoOdonto';
							$coluna = "odonto_calculo_id";
							break;
						case "OPJ":
							$ramo = "Odonto Pessoa Jurídica";
							$controller = "OdontoCalculosOdonto";
							$class = 'btnViewCalculoOdonto';
							$classEdit = 'btnEditCalculoOdontoPME';
							$coluna = "odonto_calculo_id";
							break;
					}
				?>
					<tr id="<?= $calculo["ramo"] . $calculo["id"] ?>">
						<td><?= $calculo["data"]; ?></td>
						<td>
							<span data-toggle='tooltip' data-placement='top' title='Visualizar'>
								<?=
								// $this->Html->link($this->Html->tag('span', '', ['class' => "far fa-eye", 'aria-hidden' => 'true']) . " " . $calculo["id"], ["controller" => $controller, "action" => "view", $calculo["id"]], [
								$this->Html->link($this->Html->tag('span', '', ['class' => "far fa-eye", 'aria-hidden' => 'true']) . " " . $calculo["id"], 'javascript: void(0);', [
									'role' => 'button',
									"escape" => false,
									'value' => $calculo["id"],
									'class' => $class,
									'calculo_id' => $calculo["id"],
									'aria-expanded' => "false"
								])
								?>
							</span>
						</td>
						<td><?= $ramo ?></td>
						<td>
							<span data-toggle='tooltip' data-placement='top' title='Editar'>

								<?=
								$this->Html->link($this->Html->tag('span', '', ['class' => "far fa-edit", 'aria-hidden' => 'true']) . (!empty($calculo["nome"]) ? " " . $calculo["nome"] : ' Nome não preenchido'), "#", [
									'class' => $classEdit,
									'escape' => false,
									'calculo_id' => $calculo["id"],
									"controller" => $controller,
									'aria-expanded' => "false"
								])
								?>
							</span>
						</td>
						<td>
							<?= $this->Form->input("status_id", ["options" => $status, "ramo" => $calculo["ramo"], "value" => $calculo["status_id"], "calculo_id" => $calculo["id"],  "empty" => "Selecione a Fase da Negociação", "label" => false, "class" => "mudar-status"]); ?>

						</td>
						<td>
							<span data-toggle='tooltip' data-placement='top' title='Adicionar Lembrete'>
								<?php
								$total = null;
								$color = null;
								if (isset($alertas[$calculo["ramo"]][$calculo["id"]])) {
									$total = count($alertas[$calculo["ramo"]][$calculo["id"]]);
								}
								if (isset($calculo["alerta"]) && $calculo["alerta"] === 'S') {
									// 							$color = "background-color: #a94442 !important";	   
									$color = "background-color: red !important";
								}
								?>
								<a href="#<?= $calculo["ramo"] . $calculo["id"] ?>" class="btn btn-sm btn-default far fa-bell alerta-add " role="button" calculo_id="<?= $calculo["id"] ?>" ramo="<?= $calculo["ramo"] ?>" value="<?= $calculo["id"] ?>" aria-expanded="false"><span class='badge' style="font-family: 'Open Sans', sans-serif !important; margin-left: 3px !important; font-size: 90%; <?= $color ?>"><?= $total ?></span></a>

							</span>


						</td>
					</tr>

				<?php
				}
				?>
			</tbody>
		</table>
	</div>


	<!-- Modal -->
	<div class="modal fade" id="modalLembrete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title centralizada" id="myModalLabel">Observações e Lembretes</h4>
				</div>
				<div class="modal-body clearfix" id="modal-alerta">
				</div>
			</div>
		</div>
	</div>
</section>