<style>
    .panel-title {
        background-color: transparent;
    }

    .panel-default {
        border: 0px;
        margin-top: 15px;
    }

    .panel-default:first-of-type {
        margin-top: 0;
    }

    .panel-default .panel-heading {
        background-color: #FFF;
    }

    .panel-title {
        font-size: 18pt;
        /* margin-top: 20px; */
    }

    .acoesCentral:hover img {
        background-color: #FFF !important;
    }

    a:hover .shadow-inset-center {
        -webkit-animation: shadow-inset-center 0.4s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
        animation: shadow-inset-center 0.4s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
    }

    .panel-heading {
        border-bottom: 1px solid transparent;
        border-image: linear-gradient(to right, white, lightgray, gray, black, gray, lightgray, white);
        border-image-slice: 1;
    }

    .new {
        border-radius: 5px;
        -webkit-animation: shadow-inset-center 2.0s cubic-bezier(0.250, 0.460, 0.450, 0.940) infinite;
        animation: shadow-inset-center 2.0s cubic-bezier(0.250, 0.460, 0.450, 0.940) infinite;
    }

    .icones {
        font-size: 30pt;
        height: 72px;
        width: auto;
        transition: height 0.2s;
    }


    .icones:hover {
        height: 90px;
    }

    .selecionado {
        height: 90px;
    }

    .selecionado:hover {
        height: 90px;
    }

    @-webkit-keyframes shadow-inset-center {
        0% {
            box-shadow: inset 0 0 0 0 rgba(0, 0, 0, 0);
        }

        100% {
            box-shadow: inset 0 0 14px 0px rgba(0, 0, 0, 0.5);
        }
    }

    @keyframes shadow-inset-center {
        0% {
            box-shadow: inset 0 0 0 0 rgba(0, 0, 0, 0);
        }

        100% {
            box-shadow: inset 0 0 14px 0px rgba(0, 0, 0, 0.5);
        }
    }
</style>

<!-- <div class="col-xs-12 centralizada banner bannerLg" style="max-width: 800px">
    <div class="col-xs-12" style="padding: 0 !important">
        <div id="slider-wrapper2">
            <div id="textohome" style="height:200px;">
                <?php foreach ($banners as $banner) :
                    #  1 É o ID correspondente a este setor do site
                    if ($banner->banner_id == 1) : ?>
                        <div class="ls-slide" data-ls="transition2d:5;slidedelay:4000" style="width: 100%;">
                            <p class="ls-l" style="left:0;text-align: center;width: 100%;" data-ls="durationin:1000;">
                                <?= $this->Html->image("banners/imagens/" . $banner->nome, ["style" => " max-width: 100% !important"]); ?>
                            </p>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div> -->
<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');

if ($sessao['role'] === 'admin') {
    $disabilitado = '';
} else {
    $disabilitado = array("class" => "alerta");
}
$grupos = $sessao['grupos'];
?>

<div class="col-xs-12 col-md-8 col-md-offset-2 centralizada">
    <div class="col-xs-12">
        <div class="panel panel-default" style="box-shadow: none;">
            <!-- ### COTAÇÕES E SIMULAÇÕES ### -->
            <div class="panel-heading">
                <h2 class="panel-title">Faça Cotações e Simulações</h2>
            </div>
            <div class="panel-body">
                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Simule <b>Planos de Saúde</b> para seus clientes em segundos" class="text-danger">
                        <!-- # 1 - MULTICALCULOS PLANO DE SAÚDE -->
                        <?php if (in_array(1, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/saude.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px']) . '<br/><p class="botaoCentralInterna" > Multicálculo <br/>Planos de Saúde</p>', ['controller' => 'Users', 'action' => 'saude'], ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral', 'role' => 'button', 'escape' => false]); ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/saude.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px']) . '<br/><p class="botaoCentralInterna" > Multicálculo <br/>Planos de Saúde</p>', "#", ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral alerta', 'role' => 'button', 'escape' => false]); ?>
                        <?php endif; ?>
                    </span>
                </div>

                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Simule <b>Planos Odontológicos</b> para seus clientes em segundos" class="text-danger">
                        <!-- # 2 - MULTICALCULOS PLANO DE ODONTOLÓGICOS -->
                        <?php if (in_array(2, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link(
                                $this->Html->image(
                                    "central/iconeOdonto.png",
                                    ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']
                                ) . '<br/> <p class="botaoCentralInterna">Multicálculo <br/>Planos Odontológicos</p>',
                                ['controller' => 'Users', 'action' => 'odonto'],
                                ['class' => 'btn btn-lg  btn-default  acoesCentral btn-block botoesCentral', 'style' => 'border: unset !important', 'role' => 'button', 'escape' => false]
                            ); ?>
                        <?php else : ?>
                            <?= $this->Html->link(
                                $this->Html->image(
                                    "central/iconeOdonto.png",
                                    ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']
                                ) . '<br/> <p class="botaoCentralInterna">Multicálculo <br/>Planos Odontológicos</p>',
                                "#",
                                ['class' => 'btn btn-lg  btn-default  acoesCentral btn-block botoesCentral alerta', 'style' => 'border: unset !important', 'role' => 'button', 'escape' => false]
                            ); ?>
                        <?php endif; ?>
                    </span>
                </div>

                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%;" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Crie <b>Tabelas de Preços</b> como preferir e com seu próprio <b>logotipo</b>" class="text-danger">
                        <!-- # 3 - TABELAS DE PREÇOS PERSONALIZADAS -->
                        <?php if (in_array(3, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link(
                                $this->Html->image(
                                    "central/lista.png",
                                    ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']
                                ) . '<br/> <p class="botaoCentralInterna">Tabelas de Preços<br/>Personalizadas</p>',
                                '/tabelasGeradas/gerador',
                                ['class' => 'btn btn-lg  btn-default btn-block acoesCentral botoesCentral', 'role' => 'button', 'escape' => false]
                            ); ?>
                        <?php else : ?>
                            <?= $this->Html->link(
                                $this->Html->image(
                                    "central/lista.png",
                                    ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']
                                ) . '<br/> <p class="botaoCentralInterna">Tabelas de Preços<br/>Personalizadas</p>',
                                "#",
                                ['class' => 'btn btn-lg  btn-default btn-block acoesCentral botoesCentral alerta', 'role' => 'button', 'escape' => false]
                            ); ?>
                        <?php endif; ?>
                    </span>
                </div>

                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Encontre os Credenciados mais próximos" class="text-danger">
                        <!-- # 4 - COMPARADOR DE REDE -->
                        <?php if (in_array(4, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/redes.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/><p class="botaoCentralInterna">Comparador de Rede<br/>Planos de Saúde</p>', "#", ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral', 'role' => 'button', 'escape' => false, 'id' => 'btn-rede-credenciada', "data-toggle" => "modal", "data-target" => "#modal-rede-credenciada"]); ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/redes.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/><p class="botaoCentralInterna">Comparador de Rede<br/>Planos de Saúde</p>', "#", ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral alerta', 'role' => 'button', 'escape' => false]); ?>
                        <?php endif; ?>
                    </span>

                </div>

                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%;" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Solicite e Gerencie cotações de Saúde e Odonto para <b>Empresas de Médio e Grande Porte</b>" class="text-danger">
                        <!-- # 5 - FORMULÁRIO DE COTAÇÃO SAÚDE EMPRESARIAL -->
                        <?php if (in_array(5, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/iconePerfilEtario.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">Formulário de Cotação <br/>Saúde Empresarial</p>', "#modal-alerta", ['class' => 'btn btn-lg  btn-default btn-block acoesCentral botoesCentral alerta', 'style' => 'border: unset !important', 'role' => 'button', 'escape' => false]); ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/iconePerfilEtario.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">Formulário de Cotação <br/>Saúde Empresarial</p>', "#", ['class' => 'btn btn-lg  btn-default btn-block acoesCentral botoesCentral alerta', 'style' => 'border: unset !important', 'role' => 'button', 'escape' => false]); ?>
                        <?php endif; ?>
                    </span>

                </div>


                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Calcule <b>Seguro Auto</b> para seus clientes em segundos" class="text-danger">
                        <!-- # 6 - MULTICÁCULO SEGURO AUTO -->
                        <?php if (in_array(6, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/auto.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/><p class="botaoCentralInterna">Multicálculo<br/>Seguro Auto</p>', "#auto", ['link' => 'auto', 'class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral confirmacao', 'role' => 'button', 'escape' => false, 'target' => '_parent']); ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/auto.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/><p class="botaoCentralInterna">Multicálculo<br/>Seguro Auto</p>', "#modal-alerta", ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral alerta', 'role' => 'button', 'escape' => false]); ?>
                        <?php endif; ?>
                    </span>
                </div>

                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Calcule <b>Seguro Residencial</b> para seus clientes em segundos" class="text-danger">
                        <!-- # 7 - MULTICÁCULO SEGURO RESIDENCIAL -->
                        <?php if (in_array(7, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link(
                                $this->Html->image("central/residencial.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;'])
                                    . '<br/><p class="botaoCentralInterna">Multicálculo<br/>Seguro Residencial</p>',
                                "#residencial",
                                ['link' => 'residencial', 'class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral confirmacao', 'role' => 'button', 'escape' => false]
                            ); ?>
                        <?php else : ?>
                            <?= $this->Html->link(
                                $this->Html->image("central/residencial.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;'])
                                    . '<br/><p class="botaoCentralInterna">Multicálculo<br/>Seguro Residencial</p>',
                                "#modal-alerta",
                                ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral alerta', 'role' => 'button', 'escape' => false]
                            ); ?>
                        <?php endif; ?>
                    </span>
                </div>

                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <!-- # 12 - SEGURO PATRIMONIAL EMPRESARIAL MULTIRISCO -->
                    <?php if (in_array(12, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                        <?= $this->Html->link($this->Html->image("central/patrimonial.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/><p class="botaoCentralInterna" >Seguro Patrimonial<br/>Empresarial Multirisco</p>', "#modal-alerta", ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral  alerta', 'role' => 'button', 'escape' => false]) ?>
                    <?php else : ?>
                        <?= $this->Html->link($this->Html->image("central/patrimonial.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/><p class="botaoCentralInterna" >Seguro Patrimonial<br/>Empresarial Multirisco</p>', "#modal-alerta", ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral  alerta', 'role' => 'button', 'escape' => false]) ?>
                    <?php endif; ?>
                </div>

            </div><!-- panel-body -->
            <!-- ### /COTAÇÕES E SIMULAÇÕES ### -->

            <!-- ### FECHE SUAS VENDAS ONLINE ### -->
            <div class="panel-heading">
                <h2 class="panel-title">Feche suas Vendas Online
                </h2>
            </div>
            <div class="panel-body">
                <?php foreach ($vendasOnlines as $vendaOnline) : ?>
                    <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                        <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="<?= $vendaOnline->dica ?>" class="text-danger">
                            <!-- # 9 - PLANO DE SAÚDE INDIVIDUAL E EMPRESARIAL -->
                            <?php if (in_array(9, $alloweds_functions) || $sessao['role'] === 'admin') :
                                $link = $vendaOnline->exibir_vendas_online == 1 ? '/vendas/' . $vendaOnline->id . '/' . $sessao['estado_id'] : '#modal-alerta';
                                echo $this->Html->link($this->Html->image("vendas_online/" . $vendaOnline->imagem, ['class' => $vendaOnline->exibir_vendas_online == 1 ? 'shadow-inset-center' : 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px']) . '<br/><p class="botaoCentralInterna" >' . $vendaOnline->linha1 . '<br/>' . $vendaOnline->linha2 . '</p>', $link, ['class' =>  $vendaOnline->exibir_vendas_online == 1 ? 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral' : 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral alerta', 'role' => 'button', 'escape' => false]);
                            else :
                                echo $this->Html->link($this->Html->image("vendas_online/" . $vendaOnline->imagem, ['class' => $vendaOnline->exibir_vendas_online == 1 ? 'shadow-inset-center' : 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px']) . '<br/><p class="botaoCentralInterna" >' . $vendaOnline->linha1 . '<br/>' . $vendaOnline->linha2 . '</p>', "#modal-alerta", ['class' => 'btn btn-lg  btn-default acoesCentral btn-block botoesCentral alerta', 'role' => 'button', 'escape' => false]);
                            endif; ?>
                    </div>
                    </span>
                <?php endforeach; ?>
            </div><!-- panel-body -->
            <!-- ### /FECHE SUAS VENDAS ONLINE ### -->

            <?php if (false) : ?>
                <!-- ### DIVULGUE E GERENCIE SEUS NEGÓCIOS ### -->
                <div class="panel-heading">
                    <h2 class="panel-title">Divulgue e Gerencie seus Negócios</h2>
                </div>
                <div class="panel-body">
                    <div class="col-xs-6 col-md-3 " style="padding: 10px; min-height: 140px;">
                        <!-- # 15 - CRM MEUS NEGÓCIOS -->
                        <?php if (in_array(15, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/crm.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">CRM <br/>Minhas Cotações</p>', ["action" => "calculos"], ['class' => 'btn btn-lg  btn-default btn-block acoesCentral botoesCentral', 'role' => 'button', 'escape' => false]) ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/crm.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">CRM <br/>Minhas Cotações</p>', '#modal-alerta', ['class' => 'btn btn-lg  btn-default btn-block acoesCentral botoesCentral alerta', 'role' => 'button', 'escape' => false]) ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                        <!-- # 16 - GESTÃO DE EQUIPES DE VENDAS -->
                        <?php if (in_array(16, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/iconesGestao.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px']) . '<br/><p class="botaoCentralInterna" > Gestão de <br/>Equipes de  Vendas</p>', '#modal-alerta', ['class' => 'btn btn-lg btn-block btn-default acoesCentral botoesCentral alerta', 'role' => 'button botoesCentral  ', 'escape' => false]) ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/iconesGestao.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px']) . '<br/><p class="botaoCentralInterna" > Gestão de <br/>Equipes de  Vendas</p>', '#modal-alerta', ['class' => 'btn btn-lg btn-block btn-default acoesCentral botoesCentral alerta', 'role' => 'button botoesCentral  ', 'escape' => false]) ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-6 col-md-3 " style="padding: 10px; min-height: 140px;">
                        <!-- # 17 - COMUNICADOS E AVISOS IMPORTANTES -->
                        <?php if (in_array(17, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/informativos.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">Comunicados e<br/>Avisos Importantes</p>', "#modal-alerta", ['class' => 'alerta btn btn-lg  btn-default btn-block acoesCentral botoesCentral', 'role' => 'button', 'escape' => false]) ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/informativos.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">Comunicados e<br/>Avisos Importantes</p>', "#modal-alerta", ['class' => 'alerta btn btn-lg  btn-default btn-block acoesCentral botoesCentral', 'role' => 'button', 'escape' => false]) ?>
                        <?php endif; ?>
                    </div>
                    <!--                 	$this->Html->tag("crm.png", ['style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) -->

                    <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                        <!-- # 18 - SALDO DE PREMIAÇÕES DE VENDAS -->
                        <?php if (in_array(18, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/premiacoes.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/><p class="botaoCentralInterna" > Saldo de <br/>Premiações de Vendas</p>', '#modal-alerta', ['class' => 'alerta btn btn-lg btn-block btn-default acoesCentral botoesCentral alerta', 'role' => 'button botoesCentral  ', 'escape' => false]) ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/premiacoes.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/><p class="botaoCentralInterna" > Saldo de <br/>Premiações de Vendas</p>', '#modal-alerta', ['class' => 'alerta btn btn-lg btn-block btn-default acoesCentral botoesCentral alerta', 'role' => 'button botoesCentral  ', 'escape' => false]) ?>
                        <?php endif; ?>
                        <!-- 						$this->Html->image("iconesGestao.png", ['style' => 'color: #003e58 !important', 'style' => 'max-height: 72px']) -->
                    </div>

                    <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                        <!-- # 19 - TREINAMENTOS VIDEOAULAS -->
                        <?php if (in_array(19, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/help.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">Treinamentos<br/>Videoaulas</p>', '#modal-alerta', ['class' => 'btn btn-lg  btn-default  acoesCentral btn-block botoesCentral alerta', 'style' => 'border: unset !important', 'role' => 'button', 'escape' => false]); ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/help.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">Treinamentos<br/>Videoaulas</p>', '#modal-alerta', ['class' => 'btn btn-lg  btn-default  acoesCentral btn-block botoesCentral alerta', 'style' => 'border: unset !important', 'role' => 'button', 'escape' => false]); ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                        <!-- # 20 - MINHA LOJA VENDAS ONLINE -->
                        <?php if (in_array(20, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/carrinho.png", ['class' => 'shadow-inset-center', 'class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">Minha Loja<br/>Vendas Online</p>', '#modal-alerta', ['class' => 'btn btn-lg  btn-default  acoesCentral btn-block botoesCentral alerta', 'style' => 'border: unset !important', 'role' => 'button', 'escape' => false]); ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/carrinho.png", ['class' => 'shadow-inset-center', 'class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom:3px;']) . '<br/> <p class="botaoCentralInterna">Minha Loja<br/>Vendas Online</p>', '#modal-alerta', ['class' => 'btn btn-lg  btn-default  acoesCentral btn-block botoesCentral alerta', 'style' => 'border: unset !important', 'role' => 'button', 'escape' => false]); ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                        <!-- # 21 - MARKETING DIGITAL MATERIAL DE DIVULGAÇÃO -->
                        <?php if (in_array(21, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/central.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom: 2px;']) . '<br/> <p class="botaoCentralInterna">Marketing Digital <br/>Material de Divulgação</p>', ['controller' => 'midias-compartilhadas', 'action' => 'show'], ['class' => 'btn btn-lg btn-block btn-default acoesCentral botoesCentral', 'role' => 'button', 'escape' => false]); ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/central.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom: 2px;']) . '<br/> <p class="botaoCentralInterna">Marketing Digital <br/>Material de Divulgação</p>', '#modal-alerta', ['class' => 'btn btn-lg btn-block btn-default acoesCentral botoesCentral alerta', 'role' => 'button', 'escape' => false]); ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                        <!-- # 21 - AGENDA DE CONTATOS ÚTEIS -->
                        <?php if (in_array(21, $alloweds_functions) || $sessao['role'] === 'admin') : ?>
                            <?= $this->Html->link($this->Html->image("central/agenda.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom: 2px;']) . '<br/> <p class="botaoCentralInterna">Agenda de <br/>Contatos Úteis</p>', '#modal-alerta', ['class' => 'btn btn-lg btn-block btn-default acoesCentral botoesCentral alerta', 'role' => 'button', 'escape' => false]); ?>
                        <?php else : ?>
                            <?= $this->Html->link($this->Html->image("central/agenda.png", ['class' => 'shadow-inset-center', 'style' => 'color: #003e58 !important', 'style' => 'max-height: 72px; margin-bottom: 2px;']) . '<br/> <p class="botaoCentralInterna">Agenda de <br/>Contatos Úteis</p>', '#modal-alerta', ['class' => 'btn btn-lg btn-block btn-default acoesCentral botoesCentral alerta', 'role' => 'button', 'escape' => false]); ?>
                        <?php endif; ?>
                    </div>
                </div><!-- panel-body -->
            <?php endif; ?>
            <!-- ### /DIVULGUE E GERENCIE SEUS NEGÓCIOS ### -->

            <!-- ### FECHE SUAS VENDAS ONLINE ### -->
            <div class="panel-heading">
                <h2 class="panel-title">Avalie e deixe seu Comentário</h2>
            </div>
            <div class="panel-body" style="margin-bottom: 30px;">
                <?= $this->Form->create(null, ['id' => 'form-avaliacao']); ?>
                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%; padding-top: 20px;" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Ruim" class="text-danger">
                        <?= $this->Html->image('botoes/insatisfeito.png', ['class' => 'icones', 'nota' => '1']); ?>
                        </br>
                        <span style="color: #333333">Ruim</span>
                    </span>
                </div>
                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%; padding-top: 20px;" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Razoável" class="text-danger">
                        <?= $this->Html->image('botoes/razoavel.png', ['class' => 'icones', 'nota' => '2']); ?>
                        </br>
                        <span style="color: #333333">Razoável</span>
                    </span>
                </div>
                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <div class="icon-text text-center">
                        <?= $this->Html->image('botoes/bom.png', ['class' => 'icones', 'nota' => '3']); ?>
                        <br />
                        <span>Bom</span>
                    </div>
                </div>
                <div class="col-xs-6 col-md-3" style="padding: 10px; min-height: 140px;">
                    <span style="font-size: 90%; padding-top: 20px;" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="Amei" class="text-danger">
                        <?= $this->Html->image('botoes/muito-satisfeito.png', ['class' => 'icones', 'nota' => '5']); ?>
                        </br>
                        <span style="color: #333333">Amei</span>
                    </span>
                </div>
                <?= $this->Form->input('nota', ['class' => 'form-control', 'type' => 'hidden']) ?>
                <div id="comentar" class="col-xs-10 col-xs-offset-1" style="display: none;">
                    <div class="radios flex" style="justify-content: space-evenly; padding: 5px; height: 20%; align-items: center;">
                        <?= $this->Form->radio('tipo', ['CRITICA' => 'Crítica', 'ERRO' => 'Erro', 'ELOGIO' => 'Elogio', 'SUGESTAO' => 'Sugestão']) ?>
                    </div>
                    <?= $this->Form->textarea('comentario', ['id' => 'comentario', 'placeholder' => 'Deixe seu comentário', 'style' => 'height: 50%; width: 80%; margin: 0 auto; text-align: center; display: block;']) ?>
                </div>
                <?= $this->Form->end(); ?>
            </div><!-- panel-body -->
            <!-- ### /FECHE SUAS VENDAS ONLINE ### -->
        </div><!-- panel-default -->

    </div><!-- col-xs-12 -->
</div>


<div id="rs"></div>

<div id="modal-confirmacao" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center">Texto de confirmacao..</p>
            </div>
            <div class="modal-footer" style="text-align: center; border: 0;">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <a id="link-continuar" class="btn btn-primary">Continuar</a>
            </div>
        </div>
    </div>
</div>

<?= $this->element('pesquisar-rede'); ?>
<?= $this->element('modal-marketing'); ?>