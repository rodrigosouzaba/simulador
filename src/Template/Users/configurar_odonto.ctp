<section id="AppOdontos">
    <div class="col-xs-12 text-center sub" style="margin-bottom: 6px;">
        <div class="modulo">Configurar Odonto</div>
        <?= $controller = '' ?>
        <div class="btn-group" role="group">
            <?= $this->Html->link('Operadoras', '/odontoOperadoras/new', ["class" => $controller == 'OdontoOperadoras' && $action == 'new' ? "btn btn-sm btn-primary" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoOperadoras.new']) ?>
            <?= $this->Html->link('Areas Comercializações', '/odontoAreasComercializacoes/index', ["class" => $controller == 'OdontoAreasComercializacoes' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoAreasComercializacoes.index']) ?>
            <?= $this->Html->link('Carências', '/odontoCarencias/index', ["class" => $controller == 'OdontoCarencias' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoCarencias.index']) ?>
            <?= $this->Html->link('Observações', '/odontoObservacaos/index', ["class" => $controller == 'OdontoObservacaos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoObservacaos.index']) ?>
            <?= $this->Html->link('Redes', '/odontoRedes/index', ["class" => $controller == 'OdontoRedes' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoRedes.index']) ?>
            <?= $this->Html->link('Reembolsos', '/odontoReembolsos/index', ["class" => $controller == 'OdontoReembolsos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoReembolsos.index']) ?>
            <?= $this->Html->link('Dependentes', '/odontoDependentes/index', ["class" => $controller == 'OdontoDependentes' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoDependentes.index']) ?>
            <?= $this->Html->link('Documentos', '/odontoDocumentos/index', ["class" => $controller == 'OdontoDocumentos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoDocumentos.index']) ?>
            <?= $this->Html->link('Pagamentos', '/odontoFormasPagamentos/index', ["class" => $controller == 'OdontoFormasPagamentos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoFormasPagamentos.index']) ?>
            <?= $this->Html->link('Produtos e Preços', '/odontoTabelas/new', ["class" => $controller == 'OdontoTabelas' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-default linkAjax", "type" => "button", 'sid-' => 'odontoTabelas.new']) ?>
        </div>
        <div class="clearfix">&nbsp</div>
        <div class="btn-group" role="group">
            <?= $this->Html->link('Operadoras', '/odontoOperadoras/index', ["class" => $controller == 'OdontoOperadoras' && $action == 'index' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-danger linkAjax", "type" => "button", 'sid-' => 'odontoOperadoras.index']) ?>
            <?= $this->Html->link('Produtos e Preços', '/odontoTabelas/index', ["class" => $controller == 'OdontoTabelas' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-danger linkAjax", "type" => "button", 'sid-' => 'odontoTabelas.index']) ?>
            <?= $this->Html->link('Áreas Comercialização', '/odontoComercializacoes/index', ["class" => $controller == 'OdontoProdutos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-danger linkAjax", "type" => "button", 'sid-' => 'odontoComercializacoes.index']) ?>
            <?= $this->Html->link('Produtos', '/odontoProdutos/index', ["class" => $controller == 'OdontoProdutos' ? "btn btn-sm btn-primary linkAjax" : "btn btn-sm btn-danger linkAjax", "type" => "button", 'sid-' => 'odontoProdutos.index']) ?>
        </div>
    </div>
    <div class="clearfix">&nbsp</div>
    <div id="gridOdonto" style="padding: 24px;">

    </div>
</section>