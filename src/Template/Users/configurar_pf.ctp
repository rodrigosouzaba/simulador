<section id="AppPf">
    <div class="col-xs-12 text-center sub" style="margin-bottom: 6px;">
        <div class="modulo">Configurar Saúde PF</div>
        <?= $controller = '' ?>
        <div class="btn-group" role="group">
            <?= $this->Html->link('Operadoras', '/pfOperadoras/new', ["class" =>  $controller == 'PfOperadoras' && $action == 'new' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'id' => 'inicial', 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfOperadoras.new']) ?>
            <?= $this->Html->link('Áreas Comercialização', '/pfAreasComercializacoes/index', ["class" =>  $controller == 'PfAreasComercializacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfAreasComercializacoes.index']) ?>
            <?= $this->Html->link('Metrópoles', '/metropoles/index', ["class" =>  $controller == 'metropoles' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'metropoles.index']) ?>
            <?= $this->Html->link('Coberturas', '/pfCoberturas/index', ["class" =>  $controller == 'PfCoberturas' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfCoberturas.index']) ?>
            <?= $this->Html->link('Entidades', '/pfEntidades/index', ["class" =>  $controller == 'PfEntidades' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfEntidades.index']) ?>
            <?= $this->Html->link('Profissões', '/pfProfissoes/index', ["class" =>  $controller == 'PfProfissoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfProfissoes.index']) ?>
            <?= $this->Html->link('Observações', '/pfObservacoes/index', ["class" =>  $controller == 'PfObservacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfObservacoes.index']) ?>
            <?= $this->Html->link('Redes', '/pfRedes/index', ["class" =>  $controller == 'PfRedes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfRedes.index']) ?>
            <?= $this->Html->link('Reembolsos', '/pfReembolsos/index', ["class" =>  $controller == 'PfReembolsos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfReembolsos.index']) ?>
            <?= $this->Html->link('Carências', '/pfCarencias/index', ["class" =>  $controller == 'PfCarencias' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfCarencias.index']) ?>
            <?= $this->Html->link('Dependentes', '/pfDependentes/index', ["class" =>  $controller == 'PfDependentes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfDependentes.index']) ?>
            <?= $this->Html->link('Documentos', '/pfDocumentos/index', ["class" =>  $controller == 'PfDocumentos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfDocumentos.index']) ?>
            <?= $this->Html->link('Pagamentos', '/pfFormasPagamentos/index', ["class" =>  $controller == 'PfFormasPagamentos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfFormasPagamentos.index']) ?>
            <?= $this->Html->link('Produtos e Preços', '/pfTabelas/new', ["class" =>  $controller == 'PfTabelas'  && $action == 'new' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'requestjs' => 'AppPfTabelas', 'sid' => 'pfTabelas.index']) ?>
        </div>
        </br></br>
        <div class="btn-group" role="group">
            <?= $this->Html->link('Operadoras', '/pfOperadoras/index', ["class" =>  $controller == 'PfOperadoras' && $action == 'index' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button", 'id' => 'inicial', 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfOperadoras.index']) ?>
            <?= $this->Html->link('Produtos e Preços', '/pfTabelas/index', ["class" =>  $controller == 'PfTabelas'  && $action == 'index' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button", 'requestjs' => 'AppPfTabelas', 'sid' => 'pfTabelas.index']) ?>
            <?= $this->Html->link('Áreas Comercialização', '/pfComercializacoes/index', ["class" =>  $controller == 'PfComercializacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfComercializacoes.index']) ?>
            <?= $this->Html->link('Produtos', '/pfProdutos/index', ["class" =>  $controller == 'PfProdutos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-danger linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'pfProdutos.index']) ?>
        </div>
    </div>
    <div class="clearfix">&nbsp</div>
    <div class="row">
        <div id="gridPf" class="col-sm-12">

        </div>
        <div>
</section>