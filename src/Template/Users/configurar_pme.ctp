<section id="AppPme">
    <div class="col-xs-12 text-center sub" style="margin-bottom: 6px;">
        <div class="modulo">Configurar Saúde PME</div>
        <div class="btn-group centralizada" role="group">
            <?= $controller = ''; ?>
            <?= $this->Html->link('Operadoras', '/operadoras/new', ["class" => "btn btn-sm btn-default linkAjax", "type" => "button", "sid" => "operadoras.new"]) ?>
            <?= $this->Html->link('Áreas Comercialização', '/areasComercializacoes/index', ["class" =>  $controller == 'AreasComercializacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", "sid" => "areasComercializacoes.index"]) ?>
            <?= $this->Html->link('Metrópoles', '/metropoles/index', ["class" =>  $controller == 'metropoles' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", 'r-equestjs' => 'AppPfProdutos', 'sid' => 'metropoles.index']) ?>
            <?= $this->Html->link('Coberturas', '/coberturas/index', ["class" =>  $controller == 'Coberturas' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", "sid" => "coberturas.index"]) ?>
            <?= $this->Html->link('Produtos e Preços', '/tabelas/new',  ["class" =>  $controller == 'Tabelas' && $action == "new" ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax", "type" => "button", 'requestjs' => "AppTabelas"]) ?>
            <?= $this->Html->link('Observacões', '/observacoes/index', ["class" =>  $controller == 'Observacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", "sid" => "observacoes.index"]) ?>
            <?= $this->Html->link('Redes', '/redes/index', ["class" =>  $controller == 'Redes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", "sid" => "redes.index"]) ?>
            <?= $this->Html->link('Reembolsos', '/reembolsos/index', ["class" =>  $controller == 'Reembolsos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", "sid" => "reembolsos.index"]) ?>
            <?= $this->Html->link('Carências', '/carencias/index', ["class" =>  $controller == 'Carencias' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", "sid" => "carencias.index"]) ?>
            <?= $this->Html->link('Dependentes', '/opcionais/index', ["class" =>  $controller == 'Opcionais' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", "sid" => "opcionais.index"]) ?>
            <?= $this->Html->link('Documentos', '/informacoes/index', ["class" =>  $controller == 'Informacoes' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", "sid" => "informacoes.index"]) ?>
            <?= $this->Html->link('Pagamentos', '/formasPagamentos/index', ["class" =>  $controller == 'FormasPagamentos' ? 'btn btn-sm btn-primary linkAjax' : "btn btn-sm btn-default linkAjax ", "type" => "button", "sid" => "formasPagamentos.index"]) ?>
        </div>
    </div>
    <div class="clearfix">&nbsp</div>
    <div id="gridPme" style="padding: 15px;">

    </div>
</section>