 <?php

    use Cake\I18n\Time;
    ?>
 <?php
    if (!empty($users->toArray())) { ?>
     <div class="col-xs-12" id="estatisticas">
         <div class="clearfix">&nbsp;</div>
         <div class="col-xs-12">
             <div class="col-xs-12 col-md-2 col-md-offset-4 centralizada" style="margin-bottom: 10px;color: #fff;background-color: #003956 !important;font-size: 100% !important;">
                 Usuários: <?= $totais["total"] ?>
             </div>
             <div class="col-xs-12 col-md-2 centralizada" style="margin-bottom: 10px;color: #fff;background-color: #003956 !important;font-size: 100% !important;">
                 Novos: <?= $totais["novos"] ?>
             </div>
         </div>
         <div class="col-xs-12 centralizada">
             <div class="stats col-xs-12 col-md-2 col-md-offset-1" style="background-color: #eee">
                 Saúde PME: <?= $totais['calculosSaudePJ'] ?><br>Últimos 30 dias: <?= $totais['calculosMesSaudePJ'] ?>
             </div>
             <div class="stats col-xs-12 col-md-2 " style="background-color: #ccc">
                 Saúde PF: <?= $totais['calculosSaudePF'] ?><br>Últimos 30 dias: <?= $totais['calculosMesSaudePF'] ?>
             </div>
             <div class="stats col-xs-12 col-md-2" style="background-color: #eee">
                 Odonto PME: <?= $totais['calculosOdontoPJ'] ?><br>Últimos 30 dias: <?= $totais['calculosMesOdontoPJ'] ?>
             </div>
             <div class="stats col-xs-12 col-md-2" style="background-color: #ccc">
                 Odonto PF: <?= $totais['calculosOdontoPF'] ?><br>Últimos 30 dias: <?= $totais['calculosMesOdontoPF'] ?>
             </div>
             <div class="stats col-xs-12 col-md-2" style="background-color: #eee">
                 Tabelas Geradas: <?= $totais['tabelasGeradas'] ?><br>Últimos 30 dias: <?= $totais['tabelasGeradasMes'] ?>
             </div>
         </div>
     </div>

     <div class="col-xs-12">&nbsp;</div>
     <table cellpadding="0" cellspacing="0" style="width: 100%" class="table table-condensend table-hover ">
         <thead>
             <tr>
                 <th class="tituloTabela centralizada" style="width: 10% !important">
                     <?= $this->Paginator->sort('created', ['label' => 'Cadastro']) ?></th>
                 <th class="tituloTabela centralizada" style="width: 10% !important">
                     <?= $this->Paginator->sort('ultimologin', ['label' => 'Último Acesso']) ?></th>
                 <th class="tituloTabela" style="width: 35%"><?= $this->Paginator->sort('nome') ?></th>
                 <th class="tituloTabela" style="width: 19% !important">
                     <?= $this->Paginator->sort('total_calculos', ['label' => 'Cálculos']) ?></th>
                 <th class="tituloTabela" style="width: 19% !important">
                     <?= $this->Paginator->sort('ultimocalculo', ['label' => 'Últimas 36hrs']) ?></th>
                 <th class="actions tituloTabela" style="width: 7% !important">
                     <button class="btn btn-xs btn-danger centralizada" id="btnExcluir">
                         <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                         <!-- 	                	<i class="fas fa-trash-alt"></i> -->
                     </button>
                 </th>
             </tr>
         </thead>
         <?= $this->Form->create("MultiDelete", ['url' => ['#'], 'id' => 'formLote']); ?>

         <tbody id="resultadoBusca" style="display: table-row-group">
             <?php
                    echo $this->Form->create();
                    foreach ($users as $user) : ?>
                 <tr>
                     <!--                 <td class="centralizada" style="vertical-align: middle;"> <?= $this->Form->checkbox('id[]', ['hiddenField' => false, 'value' => $user->id]) ?></td> -->
                     <td class="centralizada" style="vertical-align: middle"><small><?= $user->created ?></small></td>
                     <td class="centralizada" style="vertical-align: middle">
                         <small><?= $this->Time->format($user->ultimologin, 'dd/MM/YY  HH:mm') ?></small>
                     </td>
                     <td style="vertical-align: middle" class="">
                         <?php if (isset($user->imagem_id) && !empty($user->imagem_id)) { ?>
                             <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="<img src='/<?= $user->imagem_caminho . $user->imagem_nome ?>'/>"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                         <?php }
                                    ?>
                         <?=
                                        ucwords(strtolower($user->nome)) . " " .
                                            ucwords(strtolower($user->sobrenome)) .
                                            "<br><small style='font-size:80%'>" .
                                            $user->username . "<br>" .
                                            $user->estado_nome . "<br>" .
                                            $user->celular . "<br>" .
                                            $user->email
                                            . "</small>"
                                    ?>
                     </td>

                     <td style="vertical-align: middle" class="">

                         <small>
                             Saúde PJ: <?= $user->calculos_pme ?><br>
                             Saúde PF: <?= $user->calculos_pf ?><br>
                             Odonto PJ: <?= $user->calculos_odonto_pj ?><br>
                             Odonto PF: <?= $user->calculos_odonto_pf ?><br>
                             Tabelas: <?= $user->total_tabelas_geradas ?><br>
                         </small>
                     </td>
                     <td style="vertical-align: middle" class="">
                         <small>
                             Saúde PJ: <?= $user->calculosdia_pme ?><br>
                             Saúde PF: <?= $user->calculosdia_pf ?><br>
                             Odonto PJ: <?= $user->calculosdia_odonto_pj ?><br>
                             Odonto PF: <?= $user->calculosdia_odonto_pf ?><br>
                             Tabelas: <?= $user->tabelas_geradas_dia ?>
                         </small>
                     </td>
                     <td class="actions" style="vertical-align: middle;line-height: 2px !important">
                         <div style="width: 24px !important; text-align: center !important">
                             <?= $this->Form->checkbox('id[]', ['hiddenField' => false, 'value' => $user->id, "class" => "selecionar-exclusao"]) ?>
                         </div>
                         <br>
                         <?= $this->Html->link(
                                        $this->Html->tag('span', '', ['class' => 'fa fa-user', 'aria-hidden' => 'true']),
                                        '#',
                                        [
                                            'class' => 'btn btn-xs btn-default edit-grupo',
                                            'role' => 'button',
                                            'escape' => false,
                                            'data-toggle' => 'modal',
                                            'data-target' => '#modalGrupos',
                                            'aria-expanded' => "false", 'title' => 'Editar Grupos', 'value' => $user->id, "style" => "min-width: 24px !important; margin:1px 0 !important"
                                        ]
                                    ); ?>
                         <br>
                         <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-edit', 'aria-hidden' => 'true']), ['action' => 'edit', $user->id], ['class' => 'btn btn-xs btn-default cod-celular', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Usuário', "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                         <br>
                         <!-- ATIVACAO CELULAR -->
                         <?php

                                    switch ([$user->validacao]) {
                                        case ['ATIVO']:
                                            $icone = "fa fa-check";
                                            $cor = "success";
                                            $title = 'CELULAR ATIVADO';
                                            break;
                                        case ['INATIVO']:
                                            $icone = "fa fa-minus";
                                            $cor = "default";
                                            $title = 'ATIVAÇÃO PENDENTE<br/><small>Celular inativo</small>';
                                            break;
                                        case ['PENDENTE']:
                                            $icone = "fa fa-times";
                                            $cor = "warning";
                                            $title = 'REATIVAÇÃO PENDENTE<br/><small>Celular alterado</small>';
                                            break;
                                    }
                                    ?>
                             <?= $this->Html->link($this->Html->tag('span', '', ['class' => $icone, 'aria-hidden' => 'true']), ['action' => 'reenviarCodigo', $user->id], ['class' => 'cod-email btn btn-xs btn-' . $cor, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-html' => "true", 'title' => $title, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                             <br>
                             <!-- /ATIVACAO CELULAR -->

                             <!-- ATIVACAO E-MAIL -->
                             <?php
                                        switch ([$user->validacao_email]) {
                                            case ['ATIVO']:
                                                $iconeEmail = "fa fa-check";
                                                $corEmail = "success";
                                                $titleEmail = 'E-MAIL ATIVADO';
                                                break;
                                            case ['INATIVO']:
                                                $iconeEmail = "fa fa-minus";
                                                $corEmail = "default";
                                                $titleEmail = 'INATIVO<br/><small>E-mail inativo</small>';
                                                break;
                                            case ['PENDENTE']:
                                                $iconeEmail = "fa fa-times";
                                                $corEmail = "warning";
                                                $titleEmail = 'REATIVAÇÃO PENDENTE<br/><small>E-mail alterado</small>';
                                                break;
                                        }
                                        ?>
                                 <?= $this->Html->link($this->Html->tag('span', '', ['class' => $iconeEmail, 'aria-hidden' => 'true']), ['action' => 'reenviarCodigoEmailAdmin', $user->id], ["value" => $user->id, 'class' => 'btn btn-xs btn-' . $corEmail, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-html' => "true", 'title' => $titleEmail, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                                 <!-- /ATIVACAO E-MAIL -->

                                 <br>
                                 <?=
                                                $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-key ', 'aria-hidden' => 'true']), ['action' => 'recuperarPadrao', $user->username], ['class' => 'btn btn-xs btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Senha Provisória', "style" => "min-width: 24px !important; margin:1px 0 !important"]);
                                            ?>


                                 <br>
                                 <?php
                                            switch ($user->bloqueio) {
                                                case "S":
                                                    $iconeBloqueio = "fa fa-lock";
                                                    $acaoBloqueio = "DESBLOQUEIO";
                                                    $title = "Desbloquear Usuário";
                                                    break;
                                                case "N":
                                                    $acaoBloqueio = "BLOQUEIO";
                                                    $iconeBloqueio = "fa fa-unlock";
                                                    $title = "Bloquear Usuário";
                                                    break;
                                            }
                                            // 	                        debug($user);die();
                                            ?>
                                     <?= $this->Html->link($this->Html->tag('span', '', ['class' => $iconeBloqueio, 'aria-hidden' => 'true']), '#', ['class' => 'btn btn-xs btn-default bloqueio', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $title, 'value' => $user->id, "acao" => $acaoBloqueio, "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                     </td>

                 </tr>
             <?php endforeach; ?>
             <?= $this->Form->end(); ?>
         </tbody>
     </table>
 <?php } else {

        echo "<div class='col-xs-12'>&nbsp;</div>";
        echo "<div class='col-xs-12 centralizada alert alert-danger'>Nenhum Usuário encontrado</div>";
    } ?>
 <div class="paginator">
     <ul class="pagination">
         <?= $this->Paginator->first(__('Primeira', true)); ?>
         <?= $this->Paginator->prev('< ' . __('')) ?>
         <?= $this->Paginator->numbers() ?>
         <?= $this->Paginator->next(__('') . ' >') ?>
         <?= $this->Paginator->last(__('Última', true), array('class' => 'disabled')); ?>


     </ul>
     <p><?= $this->Paginator->counter() ?></p>
 </div>
 <!-- Modal -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-body" id="renderView"></div>
         </div>
     </div>
 </div>
 <div class="modal fade" id="modalGrupos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
             <div class="modal-body" id="renderGrupos"></div>
         </div>
     </div>
 </div>
 <?= $this->element('avisos') ?>

 <script type="text/javascript">
     $("#modalGrupos").on('hide.bs.modal', function() {
         $.ajax({
             type: "post",
             url: "<?php echo $this->request->webroot ?>users-grupos/syncUser/",
             data: $("#addGrupo").serialize(),
             success: function(data) {
                 $("#resposta-grupos").empty();
                 $("#resposta-grupos").append(data);
             }
         });
     });

     $(document).ready(function() {
         $('[data-toggle="popover"]').popover();

         $("body").tooltip({
             selector: '[data-toggle=tooltip]'
         });
         $('#togle').tooltip();
     });

     $("#tipo-busca").change(function() {
         $("#busca").show();
         if ($("#tipo-busca").val() === 'cpf') {
             $("#info-busca").mask("999.999.999-99");
         } else {
             $("#info-busca").unmask();
         }
     });

     $(".cod-cel").click(function() {
         $("#loader").click();
     });
     $(".cod-email").click(function() {
         $("#loader").click();
     });


     $(".smsForm").click(function() {
         //        alert();
         $.ajax({
             type: "get",
             url: "<?php echo $this->request->webroot ?>users/sms/" + $(this).attr('value'),
             //            data: $("#encontrarCalculo").serialize(),
             success: function(data) {
                 $("#renderView").empty();
                 $("#renderView").append(data);
             }
         });
     });

     $(".emailUsuario").click(function() {
         //        alert();
         $.ajax({
             type: "get",
             url: "<?php echo $this->request->webroot ?>users/enviarEmail/" + $(this).attr('value'),
             //            data: $("#encontrarCalculo").serialize(),
             success: function(data) {
                 $("#renderView").empty();
                 $("#renderView").append(data);
             }
         });
     });

     $(".edit-grupo").click(function() {
         $.ajax({
             type: "get",
             url: "<?php echo $this->request->webroot ?>users/editGrupos/" + $(this).attr('value'),
             success: function(data) {
                 $("#renderGrupos").empty();
                 $("#renderGrupos").append(data);
             }
         });
     });


     $(".deletar").click(function() {
         //        alert($(this).attr('value'));
         //        var a = <?= $this->request->here; ?>;
         //        alert(a);
         if (confirm("Confirma exclusão do Usuário?")) {
             $.ajax({
                 type: "post",
                 url: "<?php echo $this->request->webroot ?>users/delete/" + $(this).attr('value'),
                 //            data: $("#encontrarCalculo").serialize(),
                 success: function(data) {
                     alert('Usuário excluído com sucesso');
                     //                    $("#status_usuario").change();
                     location.reload();
                     //                    window.location.replace("<?php echo $this->request->webroot ?>users/delete/" + $(this).attr('value'));
                 }
             });
         } else {
             return false;
         }
     });

     $(".bloqueio").click(function() {
         var acao = $(this).attr('acao');
         var id = $(this).attr('value');
         $.ajax({
             type: "post",
             url: "<?php echo $this->request->webroot ?>users/bloqueio/" + id + "/" + acao,
             success: function(data) {
                 alert(acao + ' realizado com sucesso');
                 location.reload();
             }
         });
     });

     $("#btnExcluir").click(function() {
         //SE CONSULTA TIVER ORIGEM NO CAMPO DE ENCONTRAR USUÁRIO
         var busca = $("#info-busca").val();
         var estado = $("#estado-id").val();
         var status = $("#status_usuario").val();

         var users = $('input:checkbox').filter(':checked');
         // 	console.log(users);
         $("#loader").click();


         $.ajax({
             type: "post",
             data: users,
             url: "<?= $this->request->webroot ?>users/delete-lote/",
             success: function(data) {
                 if (busca) {
                     $.ajax({
                         type: "POST",
                         url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
                         data: $("#info-busca").serialize(),
                         success: function(data) {
                             $("#respostaConsulta").empty();
                             $("#respostaConsulta").append(data);
                             $("#fechar").click();
                             $("#conteudoAviso").empty();
                             $("#conteudoAviso").append("<div class='centralizada'>Usuário(s) Exluido(s) com Sucesso.</div>");
                             $("#modalAviso").modal("show");
                         }
                     });
                 } else {
                     $.ajax({
                         type: "POST",
                         url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
                         data: $("#busca-especial").serialize(),
                         success: function(data) {
                             $("#respostaConsulta").empty();
                             $("#respostaConsulta").append(data);
                             $("#fechar").click();
                             $("#conteudoAviso").empty();
                             $("#conteudoAviso").append("<div class='centralizada'>Usuário(s) Exluido(s) com Sucesso.</div>");
                             $("#modalAviso").modal("show");
                         }
                     });
                 }
             },
             error: function() {
                 $("#conteudoAviso").empty();
                 $("#conteudoAviso").append("<div class='centralizada text-danger'>Erro no processo de Exclusão</div>");
                 $("#modalAviso").modal("show");
             }
         });
     });
 </script>
