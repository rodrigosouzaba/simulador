<style>
    .logo-options {}

    .logo-edit {
        height: 100px;
        width: 300px;
    }

    .logo-space {
        display: flex;
    }
</style>
<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<?php $this->assign('title', $title); ?>
<?= $this->Form->create($user) ?>
<div class="modulo text-center">Meus dados</div>
<div class="col-xs-10 col-xs-offset-1">
    <div class="col-md-12 text-center">
        <div class="btn-group" role="group">
            <?= $this->Html->link('Alterar Senha', ['action' => 'novasenha', $user->id], ['class' => 'btn btn-sm btn-default']) ?>
            <?= $this->Html->link('Alterar Logotipo', '#', ['class' => 'btn btn-sm btn-default', 'id' => 'btn-altera-logo']) ?>
            <?= $this->Html->link('Excluir cadastro', '/users/delete/' . $user->id, ['class' => 'btn btn-sm btn-danger', 'confirm' => 'Após a exclusão todos os seus dados serão excluidos e você não poderá realizar mais cotações. Tem certeza que deseja continuar?', 'role' => 'button', 'escape' => false]) ?>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-12">
        <div class="col-md-6">
            <?= $this->Form->input('nome', ['label' => false, 'placeholder' => 'Nome']); ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('sobrenome', ['label' => false, 'placeholder' => 'Nome', 'required' => 'required']); ?>
        </div>
    </div>
    <div class="col-md-12">

        <div class="col-md-6"><?= $this->Form->input('username', ['label' => false, 'placeholder' => 'Login (CPF)', 'disabled' => 'disabled']); ?></div>
        <div class="col-md-6"><?= $this->Form->input('data_nascimento', ['label' => false, 'placeholder' => 'Data de Nascimento', 'type' => 'text']); ?></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"><?= $this->Form->input('celular', ['label' => false, 'placeholder' => 'Celular']); ?></div>
        <div class="col-md-6"><?= $this->Form->input('whatsapp', ['label' => false, 'placeholder' => 'Whatsapp']); ?></div>
        <div class="col-md-4"><?= $this->Form->input('email', ['label' => false, 'placeholder' => 'E-mail']); ?></div>
        <div class="col-md-4"><?= $this->Form->input('site', ['label' => false, 'placeholder' => 'Site']); ?></div>
        <div class="col-md-4"><?= $this->Form->input('facebook', ['label' => false, 'placeholder' => 'Facebook']); ?></div>
        <div class="col-md-6"><?= $this->Form->input('estado_id', ['label' => false, 'empty' => 'Estado']); ?></div>
        <div class="col-md-6" id="div-municipios"><?= $this->Form->input('municipio_id', ['label' => false, 'empty' => 'Município', 'style' => 'margin-bottom: 1rem;', 'required' => true]); ?></div>
    </div>
</div>
<?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => "btn btn-primary btn-md col-md-2 col-md-offset-5", 'id' => 'salvar']) ?>
<div class="modal fade" id="modalLogo" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Inserir ou alterar logotipo</h4>
            </div>
            <div class="modal-body row">
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<?= $this->element("processando"); ?>
<div class="spacer spacer-lg">&nbsp</div>

<script type="text/javascript">
    $("#salvar").click(function() {
        $("#loader").click();
    });

    $("#btn-altera-logo").click(() => {
        $.ajax({
            url: '/app/users/add-imagem/<?= $user->id ?>',
            method: 'GET',
            success: (data) => {
                $("#modalLogo .modal-body").empty();
                $("#modalLogo .modal-body").append(data);
                $("#modalLogo").modal("show");
            }
        });
    });

    $("#celular").mask("(99) 9 9999-9999");
    $("#data-nascimento").mask("99/99/9999");
    $("#whatsapp").mask("(99) 9 9999-9999");

    $("[name='data_nascimento[year]']").find('option')
        .remove();
    for (i = <?= date('Y') ?>; i >= 1920; i--) {
        $("[name='data_nascimento[year]']").append($('<option>', {
            value: i,
            text: i
        }));
    }
    $("[name='data_nascimento[year]']").val(<?= $user->data_nascimento->year ?>)

    $("#estado-id").change(function() {
        let estado = $(this).val();
        $.ajax({
            type: "GET",
            url: "<?= $this->request->webroot . $this->request->controller ?>/FiltroMunicipio/" + estado,
            success: data => {
                $("#div-municipios").empty();
                $("#div-municipios").append("<label>Municipio</label>");
                $("#div-municipios").append(data);
            }
        });
    });
</script>