<?= $this->Form->create('Users', ['class' => 'form form-validate', 'role' => 'form']); ?>
<?= $this->element('forms/title', ['title' => '<i class="fa fa-edit"></i> Editar usuário']); ?>
<?= $this->Form->hidden('id'); ?>
<div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">                
                <?= $this->Form->input('nome', ['label' => 'Nome', 'class' => 'form-control', 'required']); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">                
                <?= $this->Form->input('sobrenome', ['label' => 'Sobrenome', 'class' => 'form-control', 'required']); ?>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:15px;">
    	<div class="col-md-12">
            <div class="form-group">
			    <?= $this->Form->input('sis_grupo', array('label' => false, 'multiple' => 'true', 'type' => 'select', 'options' => $optionsGrupos, 'default'=>$selectedGrupos, 'class' => 'chosen-select', 'required'=>'required', 'class'=>'form-control', 'div'=>['class'=>'col-sm-12 controls'])); ?>
			    <label class="control-label">Grupo</label>
			</div>
		</div>
	</div>
</div>
<?= $this->element('forms/buttons') ?>
<?= $this->Form->end(); ?>