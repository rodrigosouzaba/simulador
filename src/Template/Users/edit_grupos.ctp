<style>
    ul {
        list-style: none !important;
    }

    .input-links {
        display: flex;
        align-items: center;
    }

    .input-links input[type='checkbox'] {
        margin: 0px 5px;
    }

    .input-links input[type='checkbox']:checked~input[type='text'] {
        display: block;
    }

    .input-links input[type='text'] {
        display: none;
        width: 30%;
        margin: 0 15px;
        box-shadow: 0 0 0 0;
        border: 0px;
        border-bottom: 1px solid #003E55;
        border-radius: 2px;
        padding: 0rem 0.4rem;
        height: auto;
    }

    .input-links input[type='text']:focus {
        background-color: #FFF;
    }
</style>
<?php
function generatePageTree($datas, $parent = 1, $limit = 0)
{
    if ($limit > 10) return ''; // Make sure not to have an endless recursion
    $tree = '<ul>';
    for ($i = 1, $ni = count($datas); $i <= $ni; $i++) {
        if ($datas[$i]['grupo_pai_id'] == $parent) {
            $tree .= '<li>';
            $tree .= $datas[$i]['nome'];
            $tree .= generatePageTree($datas, $datas[$i]['id'], $limit++);
            $tree .= '</li>';
        }
    }
    $tree .= '</ul>';
    return $tree;
}
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Grupos do Usuário</h4>
</div>
<div class='clearer'>&nbsp;</div>
<?= $this->Form->create('User_Grupos', ['class' => 'form-inline', 'id' => 'addGrupo']) ?>
<div class="col-md-12"><?= $this->Form->hidden('user_id', ['value' => $id]); ?></div>
<?php
foreach ($grupos as $item) {
    ?>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
        <div class="panel panel-default" style='margin-bottom:10px !important;'>
            <div class="panel-heading" role="tab" id="<?php echo $item['id'] ?>">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#<?php echo $item['id'] ?>" href="#<?php echo $item['id'] ?>-content" aria-expanded="true" aria-controls="<?php echo $item['id'] ?>">
                        <?php echo $item['nome'] ?>
                    </a>
                </h4>
            </div>
            <div id="<?php echo $item['id'] ?>-content" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="<?php echo $item['id'] ?>">
                <div class="panel-body">
                    <?php
                        if (isset($item['children'])) {
                            foreach ($item['children'] as $children1) {
                                $check1 = '';
                                (array_key_exists($children1['id'], $userGrupos) ? $check1 = 'checked="checked"' : '');
                                echo sprintf("<ul><li class='input-links'><input type='checkbox' name='%s' value='%s' %s/> %s" . ($children1['grupo_pai_id'] == 19 ? "<input type='text' name='link[%s]' value='%s'/>" : ""), $children1['nome'], $children1['id'], $check1, $children1['nome'], $children1['id'], (key_exists($children1['id'], $links) ? $links[$children1['id']] : ''));
                                if (isset($children1['children'])) {
                                    foreach ($children1['children'] as $children2) {
                                        $check2 = '';
                                        (array_key_exists($children2['id'], $userGrupos) ? $check2 = 'checked="checked"' : '');
                                        echo sprintf("<ul><li><input type='checkbox' name='%s' value='%s' %s/> %s</li>", $children2['nome'], $children2['id'], $check2, $children2['nome']);
                                        if (isset($children2['children'])) {
                                            foreach ($children2['children'] as $children3) {
                                                $check3 = '';
                                                (array_key_exists($children3['id'], $userGrupos) ? $check3 = 'checked="checked"' : '');
                                                echo sprintf("<ul><li><input type='checkbox' name='%s' value='%s' %s/> %s</li>", $children3['nome'], $children3['id'], $check3, $children3['nome']);
                                                if (isset($children3['children'])) {
                                                    foreach ($children3['children'] as $children4) {
                                                        $check4 = '';
                                                        (array_key_exists($children4['id'], $userGrupos) ? $check4 = 'checked="checked"' : '');
                                                        echo sprintf("<ul><li><input type='checkbox' name='%s' value='%s' %s/> %s</li>", $children4['nome'], $children4['id'], $check4, $children4['nome']);
                                                        if (isset($children4['children'])) {
                                                            foreach ($children4['children'] as $children5) {
                                                                $check5 = '';
                                                                (array_key_exists($children5['id'], $userGrupos) ? $check5 = 'checked="checked"' : '');
                                                                echo sprintf("<ul><li><input type='checkbox' name='%s' value='%s' %s/> %s</li>", $children5['nome'], $children5['id'], $check5, $children5['nome']);
                                                            }
                                                        } else {
                                                            echo "</ul>";
                                                        }
                                                    }
                                                } else {
                                                    echo "</ul>";
                                                }
                                            }
                                        } else {
                                            echo "</ul>";
                                        }
                                    }
                                } else {
                                    echo "</ul>";
                                }
                            }
                        }
                        ?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
    </div>


    <div id='resposta-grupos'></div>
    <div class="col-xs-12 col-md-6 col-md-offset-3">
        <!-- <?= $this->Html->link('Salvar', '#', ['class' => "btn btn-primary btn-md btn-block", 'id' => 'salvarGrupos']) ?> -->

    </div>


    <div class="clearfix">&nbsp;</div>
    <?= $this->Form->end() ?>



    <script type="text/javascript">
        $("#salvarGrupos").click(function() {
            $.ajax({
                type: "post",
                url: "<?php echo $this->request->webroot ?>users-grupos/syncUser/",
                data: $("#addGrupo").serialize(),
                success: function(data) {
                    $("#resposta-grupos").empty();
                    $("#resposta-grupos").append(data);
                }
            });
        });
    </script>
