<style type="text/css">
    .field{
        width: 100%; 
        text-align: center;
        -webkit-appearance: none;
        -moz-appearance: none;
        border-radius: 0;
        background-color: #fff;
        border-style: solid;
        border-width: 1px;
        border-color: #ccc;
        box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
        color: rgba(0,0,0,0.75);
        display: block;
        font-family: sans-serif;
        font-size: 0.875rem;
        height: 2.3125rem;
        margin: 0 0 1rem 0;
        padding: 0.5rem;
        width: 100%;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-transition: border-color 0.15s linear,background 0.15s linear;
        -moz-transition: border-color 0.15s linear,background 0.15s linear;
        -ms-transition: border-color 0.15s linear,background 0.15s linear;
        -o-transition: border-color 0.15s linear,background 0.15s linear;
        transition: border-color 0.15s linear,background 0.15s linear;
    }
    .botao{
        background-color: #337ab7; color: #fff; border-color: 1px solid #2e6da4;
        display: inline-block;
        padding: 6px 12px;
        margin-top: 10px;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    .opcoes{
        -webkit-appearance: none !important;
        -moz-appearance: none !important;
        background-color: #FAFAFA;
        border-radius: 0;
        background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeD0iMTJweCIgeT0iMHB4IiB3aWR0aD0iMjRweCIgaGVpZ2h0PSIzcHgiIHZpZXdCb3g9IjAgMCA2IDMiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDYgMyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PHBvbHlnb24gcG9pbnRzPSI1Ljk5MiwwIDIuOTkyLDMgLTAuMDA4LDAgIi8+PC9zdmc+);
        background-position: 100% center;
        background-repeat: no-repeat;
        border-style: solid;
        border-width: 1px;
        border-color: #ccc;
        color: rgba(0,0,0,0.75);
        font-family: inherit;
        font-size: 0.875rem;
        line-height: normal;
        padding: 0.5rem;
        border-radius: 0;
        height: 2.3125rem;
        width: 100%;
        padding-right: 5px; 
    }
</style>
<?= $this->Form->create('user') ?>
<div style="float: left; width: 48%">
    <?=
    $this->Form->input('operadora_id', [ 'label' => '', 'options' => $operadoras_fechadas, 'empty' => 'SELECIONE OPERADORA', 'class' => 'opcoes']);
    ?>
</div>
<div style="float: left; width: 48%; margin-left: 2%">
    <?=
    $this->Form->input('email', ['label' => '', 'placeholder' => 'Digite aqui seu email', 'class' => 'field'
    ]);
    ?>

</div>
<div style="text-align: center">
    <?=
    $this->Form->submit('Solicitar Arquivos', [
        'class' => 'botao',
        'id' => 'arquivos']);
    ?>    
</div>
<?= $this->Form->end() ?>
