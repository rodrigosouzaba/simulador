<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Enviar Email</h4>
</div>
<div class="modal-body" >

    <b>Usuário: </b> <?= ucwords(strtolower($user['nome'])) . " " . ucwords(strtolower($user['sobrenome'])) ?> 
    <br/>
    <b>E-mail: </b> <?= $user['email'] ?> 

    <br/>
    <br/>
    <?php
    echo $this->Form->create('enviarEmailForm', ['id' => 'enviarEmailForm']);
    echo $this->Form->hidden('id', ['value' => $user['id']]);
    echo $this->Form->input('mensagem', ['type' => 'textarea']);
    ?>
    <br/>
</div>
<div class="modal-footer centralizada">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
    <!--<button type="button" class="btn btn-primary">Enviar</button>-->

    <?= $this->Html->link(('Enviar'), 'javascript:void(0)', ['class' => "btn btn-primary btn-md ", 'id' => 'btnEnviarEmail', 'role' => 'button', 'escape' => false]) ?>
    <div id="loading2" style="float: left">&nbsp;</div>
    <?= $this->Form->end(); ?>
</div>
<div id="r"></div>
<script type="text/javascript">

    $("#btnEnviarEmail").click(function () {
//                window.location.replace('http://110.2.1.16/sendsms?username=admin&password=admin&phonenumber=' + cel + '&message=' + msg + '&[port=1&]');
        $.ajax({
            type: "post",
            data: $("#enviarEmailForm").serialize(),
            url: "<?= $this->request->webroot ?>users/enviar_email",
            beforeSend: function () {
                $('#loading2').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                alert('Email enviado com sucesso!');
                $("#loading2").empty();
//                $("#r").append(data);
                $(".close").trigger("click");
            }
        });


    });


</script>

