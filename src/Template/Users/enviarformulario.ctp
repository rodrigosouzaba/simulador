<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Envie o formulário de Cálculo para seu cliente</h4>
</div>
<div class="modal-body">
    <div class="well" style="text-align: justify">

        Seu cliente receberá um formulário idêntico ao que você preenche para realizar um cálculo. Sua LOGOMARCA será exibida no topo da página e este cálculo estará disponível no seu histórico de cálculos.
    </div>


    <?php
    echo $this->Form->create('formclientedestinatario', ['id' => 'formclientedestinatario']);
    echo $this->Form->input('nome');
    echo $this->Form->input('email');
    echo $this->Form->input('celular', ['class' => 'celular']);
    ?>
    <div class="centralizada">
        Envie este link para seu cliente
        <div class="well well-sm" id="copiar" style="margin-bottom: 10px;">
            https://corretorparceiro.com.br/app/users/calculoremoto/<?= $sessao['id'] ?>
        </div>
        <small style="margin-bottom: 10px;">Esse link poderá ser enviado pelo Whatsapp, Email, SMS ou ser compartilhado em redes sociais</small>

        <?= $this->Html->link(('Clique aqui para copiar'), '#', ['style' => 'margin-top: 10px;', 'class' => "btn btn-default btn-sm copiar ", 'data-clipboard-target' => '#copiar', 'role' => 'button', 'escape' => false, 'id' => 'btCopiar']) ?>

        <div style="margin-top: 10px;">
            &nbsp;
            <span class="label label-default" id="labelCopia">Copiado!</span>
        </div>

    </div>
    <br />
</div>
<div class="modal-footer centralizada">
    <div id="respostaFormulario"></div>
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
    <!--<button type="button" class="btn btn-primary">Enviar</button>-->

    <?= $this->Html->link(('Enviar'), 'javascript:void(0)', ['class' => "btn btn-primary btn-md ", 'id' => 'enviarformcliente', 'role' => 'button', 'escape' => false]) ?>
    <div id="loadingFormCliente" style="float: left">&nbsp;</div>
    <?= $this->Form->end(); ?>
</div>
<div id="respostaIndicacao"></div>
<script type="text/javascript">
    $(".celular").mask("(99) 9 9999-9999");
    $("#labelCopia").hide();

    $("#btCopiar").click(function() {
        $("#labelCopia").show();
        setTimeout(function() {
            $("#labelCopia").hide()
        }, 1000);
    });
    $("#enviarformcliente").click(function() {
        $.ajax({
            type: "POST",
            data: $("#formclientedestinatario").serialize(),
            url: "<?= $this->request->webroot ?>users/enviarformulario/",
            beforeSend: function() {
                $('#loadingFormCliente').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />  Processando    ");
            },
            success: function(data) {
                //                alert('Indicação enviada!');
                $("#loadingFormCliente").empty();
                //                $("#respostaFormulario").empty();
                //                $("#respostaFormulario").append(data);
                $(".close").trigger("click");
            }
        });


    });
</script>
