<?= $this->Form->create('erro', ['id' => 'erro']) ?>
<?= $this->Form->hidden('id'); ?>
<div class="col-md-12"><h4>Sugestões ou Erros</h4><?= $this->Form->textarea('mensagem', ['id' => 'mensagem', 'label' => 'Mensagem']); ?>
    <div id="resposta" class="text-danger fonteReduzida"><div class="clearfix">&nbsp;</div></div>

</div>



<div class="col-md-12 centralizada fonteReduzida" style="margin-top: 5px;">


    <?= $this->Html->link(($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Enviar'), 'javascript:void(0)', ['class' => "btn btn-primary btn-md ", 'id' => 'enviar', 'role' => 'button', 'escape' => false]) ?>

    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['controller' => 'users', 'action' => 'central'], ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false]);
    ?>
</div>
<div class="col-md-12 centralizada fonteReduzida" style="z-index: 9;">
    <span  id="loading">&nbsp;</span> 
</div>
<?= $this->Form->end() ?>
<?= $this->element("processando");?>
<script type="text/javascript">


    $("#enviar").click(function () {
        if ($("#mensagem").val() !== "") {
            $.ajax({
                type: "post",
                data: $("#mensagem").serialize(),
                url: "<?php echo $this->request->webroot ?>users/erro/",
                beforeSend: function () {
                    $('#loader').click();
                },
                success: function (data) {
                    $('#fechar').click();
                    alert('Mensagem enviada com sucesso');
                    window.location = "<?= $this->request->webroot ?>users/central";
                }
            });
        } else {
            $("#resposta").empty();
            $("#resposta").append("<span class='text-danger'>Mensagem Vazia</span>");
        }
    });
</script>