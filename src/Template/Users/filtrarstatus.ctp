<?php
if (!empty($users->toArray())) {
    foreach ($users as $user):
        ?>
       <tr>
                    <td class="centralizada" style="vertical-align: middle"><small><?= $user->created ?></small></td>
                    <td class="centralizada"  style="vertical-align: middle">
                        <small><?= $this->Time->format($user->ultimologin, 'dd/MM/YY  HH:mm') ?></small>
                    </td>
                    <td  style="vertical-align: middle">
                        <?php if (isset($user->imagem_id) && !empty($user->imagem_id)) { ?>
                            <span style="font-size: 90%" data-toggle="popover" 
                                  data-trigger="hover"  
                                  data-placement="top" 
                                  data-html="true"
                                  data-content="<img src='/<?= $user->imagem_caminho . $user->imagem_nome ?>'/>" 
                                  ><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                              <?php }
                              ?>
                              <?=
                              ucwords(strtolower($user->nome)) . " " .
                              ucwords(strtolower($user->sobrenome)) .
                              "<br><small style='font-size:80%'>" .
                              $user->username . "<br>" .
                              $user->estado_nome . "<br>" .
                              $user->celular . "<br>" .
                              $user->email
                              . "</small>"
                              ?>
                    </td>
                    <?php //debug($user); ?>


                    <td  style="vertical-align: middle">

                        <small>
                            Saúde PJ: <?= $user->calculos_pme ?><br>
                            Saúde PF: <?= $user->calculos_pf ?><br>
                            Odonto PJ: <?= $user->calculos_odonto_pj ?><br>
                            Odonto PF: <?= $user->calculos_odonto_pf ?><br>
                            Tabelas: <?= $user->total_tabelas_geradas ?><br>
                        </small>
                    </td>
                    <td  style="vertical-align: middle">
                        <small>
                            Saúde PJ: <?= $user->calculosdia_pme ?><br>
                            Saúde PF: <?= $user->calculosdia_pf ?><br>
                            Odonto PJ: <?= $user->calculosdia_odonto_pj ?><br>
                            Odonto PF: <?= $user->calculosdia_odonto_pf ?><br>
                            Tabelas: <?= $user->tabelas_geradas_dia ?>
                        </small>
                    </td>
                    <td class="actions"  style="vertical-align: middle;line-height: 2px !important">
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-edit', 'aria-hidden' => 'true']), ['action' => 'edit', $user->id], ['class' => 'btn btn-xs btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Usuário', "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                        <br>
                        <?php
                        switch ([$user->validacao, $user->validacao_email]) {
                            case ['ATIVO', 'ATIVO']:
                                $icone = "fa fa-check";
                                $cor = "success";
                                $title = 'ATIVADO';
                                break;
                            case ['ATIVO', 'INATIVO']:
                                $icone = "fa fa-minus";
                                $cor = "default";
                                $title = 'ATIVAÇÃO PENDENTE<br/><small>E-mail inativo</small>';
                                break;
                            case ['INATIVO', 'INATIVO']:
                                $icone = "fa fa-minus";
                                $cor = "default";
                                $title = 'ATIVAÇÃO PENDENTE<br><small>Celular e E-mail inativos</small>';
                                break;
                            case ['INATIVO', 'ATIVO']:
                                $icone = "fa fa-minus";
                                $cor = "default";
                                $title = 'ATIVAÇÃO PENDENTE<br/><small>Celular inativo</small>';
                                break;
                            case ['PENDENTE', 'ATIVO']:
                                $icone = "fa fa-times";
                                $cor = "warning";
                                $title = 'REATIVAÇÃO PENDENTE<br/><small>Celular alterado</small>';
                                break;
                            case ['ATIVO', 'PENDENTE']:
                                $icone = "fa fa-times";
                                $cor = "warning";
                                $title = 'REATIVAÇÃO PENDENTE<br/><small>E-mail alterado</small>';
                                break;
                        }
                        ?>
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => $icone, 'aria-hidden' => 'true']), ['action' => 'reenviarCodigo', $user->id], ['class' => 'btn btn-xs btn-' . $cor, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top','data-html'=>"true", 'title' => $title, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                        <br>        
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-phone', 'aria-hidden' => 'true']), ['action' => 'recuperarSenhaAdmin', $user->id], ['class' => 'btn btn-xs btn-info', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Senha Provisória SMS', "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                        <br>          
                        <?=
                        $this->Html->link(
                                $this->Html->tag('span', '', ['class' => 'fa fa-envelope ', 'aria-hidden' => 'true']), ['action' => 'recuperarSenhaEmail', $user->id], ['class' => 'btn btn-xs btn-info', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Senha Provisória EMAIL', "style" => "min-width: 24px !important; margin:1px 0 !important"]);
                        ?>
                        <br>          
                        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']), '#', ['class' => 'btn btn-xs btn-danger deletar', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Excluir Usuário', 'value' => $user->id, "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                    </td>

                </tr>

        <?php
    endforeach;
} else {
    ?>
    <tr>
        <td colspan="7" class="centralizada">Nenhum usuário encontrado</td>
    </tr>
<?php }
?>


<script type="text/javascript">
    $(document).ready(function () {
        $("body").tooltip({selector: '[data-toggle=tooltip]'});
        $('#togle').tooltip();
          $('[data-toggle="popover"]').popover();
    });
    

//        $('#togle').tooltip();
    
    
    $("#busca").hide();
    $("#tipo-busca").change(function () {
        $("#busca").show();
//        alert($("#tipo-busca").val());
        if ($("#tipo-busca").val() === 'cpf') {
            $("#info-busca").mask("999.999.999-99");
        } else {
            $("#info-busca").unmask();
        }
    });
    $("#encontrar").click(function () {
//        alert();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>users/encontrarUsuario/",
            data: $("#encontrarCalculo").serialize(),
            beforeSend: function () {
                $('#loading').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                $("#resultadoBusca").empty();
                $("#resultadoBusca").append(data);
                $("#loading").empty();
            }
        });
    });
    $(".smsForm").click(function () {
//        alert();
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/sms/" + $(this).attr('value'),
//            data: $("#encontrarCalculo").serialize(),
            success: function (data) {
                $("#renderView").empty();
                $("#renderView").append(data);
            }
        });
    });

    $(".emailUsuario").click(function () {
//        alert();
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/enviarEmail/" + $(this).attr('value'),
//            data: $("#encontrarCalculo").serialize(),
            success: function (data) {
                $("#renderView").empty();
                $("#renderView").append(data);
            }
        });
    });
    $(".deletar").click(function () {
//        alert($(this).attr('value'));
        if (confirm("Confirma exclusão do Usuário?")) {
            $.ajax({
                type: "post",
                url: "<?php echo $this->request->webroot ?>users/delete/" + $(this).attr('value'),
//            data: $("#encontrarCalculo").serialize(),
                success: function (data) {
                    alert('Usuário excluído com sucesso');
                    $("#status_usuario").change();

//                    window.location.replace("<?php echo $this->request->webroot ?>users/delete/" + $(this).attr('value'));
                }
            });
        } else {
            return false;
        }
    });


</script>