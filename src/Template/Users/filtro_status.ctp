<?php 
		foreach($calculos as $calculo){
			switch($calculo["ramo"]){
				case "SPF":
					$ramo = "Saúde Pessoa Física";
					$controller = "PfCalculos";
					$coluna = "pf_calculo_id";
				break;
				case "SPJ":
					$ramo = "Saúde Pessoa Jurídica";
					$coluna = "simulacao_id";
					$controller = "Simulacoes";
				break;
				case "OPF":
					$ramo = "Odonto Pessoa Física";
					$controller = "OdontoCalculos";
					$coluna = "odonto_calculo_id";
				break;
				case "OPJ":
					$ramo = "Odonto Pessoa Jurídica";
					$controller = "OdontoCalculos";
					$coluna = "odonto_calculo_id";
				break;
			}
	?>
			<tr id="<?=$calculo["ramo"].$calculo["id"]?>">
				<td><?= $calculo["data"]; ?></td>
				<td>
					<span data-toggle='tooltip' data-placement='top' title='Visualizar'>
						<?=
			                $this->Html->link($calculo["id"], ["controller" => $controller, "action" => "view" ,$calculo["id"]], [
			                    'role' => 'button',
			                    'value' => $calculo["id"],
			                    'aria-expanded' => "false"
			                ])
		                ?>
				   </span>	
				</td>
				<td><?= $ramo ?></td>
				<td>
					<span data-toggle='tooltip' data-placement='top' title='Editar'>
					<?=
		                $this->Html->link((!empty($calculo["nome"]) ? $calculo["nome"] : 'Nome não preenchido'), "#" ,[
		                    'class' => 'editar-calculo',
		                    'calculo_id' => $calculo["id"],
		                    "controller" => $controller,
		                    'aria-expanded' => "false"
		                ])
	                ?>
					</span>
				</td>
				<td>
					<?= $this->Form->input("status_id", ["options" => $status, "ramo" => $calculo["ramo"],"value" => $calculo["status_id"],"calculo_id" => $calculo["id"],  "empty" => "Selecione a Fase da Negociação", "label" => false, "class" => "mudar-status"]); ?>
					
				</td>
				<td>
				   <span data-toggle='tooltip' data-placement='top' title='Adicionar Lembrete'>
				   <?php 
					   $total = null;
					   $color = null;
					   if(isset($alertas[$calculo["ramo"]][$calculo["id"]])){
							$total = count($alertas[$calculo["ramo"]][$calculo["id"]]);	
					   }
					   if(isset($calculo["alerta"]) && $calculo["alerta"] === 'S'){
// 							$color = "background-color: #a94442 !important";	   
							$color = "background-color: red !important";	   
					   }
					   ?>
					   <a href="#<?=$calculo["ramo"].$calculo["id"]?>" class="btn btn-sm btn-default far fa-bell alerta-add " role="button" calculo_id = "<?= $calculo["id"]?>" ramo="<?= $calculo["ramo"] ?>" value="<?= $calculo["id"] ?>" aria-expanded="false"><span class='badge' style="font-family: 'Open Sans', sans-serif !important; margin-left: 3px !important; font-size: 90%; <?= $color?>"><?=$total?></span></a>

				   </span>
				  
				</td>
			</tr>
	        
	<?php		
		}
	?>
	
<script type="text/javascript">
	$(".paginator").remove();
	$(".mudar-status").change(function () {
		var ramo = null;
		switch($(this).attr("ramo")) {
	    case "SPF":
	        ramo = "pf-calculos";
	        break;
	    case "SPJ":
	        ramo = "simulacoes";
	        break;
	    case "OPF":
	        ramo = "odonto-calculos";
	        break;
	    case "OPJ":
	        ramo = "odonto-calculos";
	        break;
		}
		
		var status = (this).value;
		 
	    $.ajax({
	        type: "POST",
	        url: "<?= $this->request->webroot ?>"+ramo+"/alterar-status/" + $(this).attr("calculo_id") + "/" + (this).value,
	        success: function (data) {
		        if(status == 1 || status == 2 || status == 3){
			        $('#modalLembrete').modal('show');
		        }
	        }
	    });
	    $.ajax({
	        type: "GET",
	        url: "<?= $this->request->webroot ?>alertas/add/" + $(this).attr("ramo") + "/"+ $(this).attr("calculo_id"),
	        success: function (data) {
	            $('#modal-alerta').empty();
		        $('#modal-alerta').append(data);
	        }
	    });
    });
    
    $(".alerta-add").click(function () {
	    $.ajax({
	        type: "GET",
	        url: "<?= $this->request->webroot."alertas/add/" ?>" + $(this).attr("ramo") + "/" + $(this).attr("value")+"/true",
	        success: function (data) {
		        $("#modal-alerta").empty();
		        $("#modal-alerta").append(data);
		        $('#modalLembrete').modal('show');
	        }
	    });

    });
    
    $(".editar-calculo").click(function () {
	    $.ajax({
	        type: "GET",
	        url: "<?= $this->request->webroot ?>" + $(this).attr("controller") + "/edit/" + $(this).attr("calculo_id"),
	        success: function (data) {
		        $("#modal-alerta").empty();
		        $("#modal-alerta").append(data);
		        $("#myModalLabel").text("Editar Cálculo");
		        if($(window).width() >= 800){
			        $(".modal-dialog").css('width',"90%");
		        }
		        $('#modalLembrete').modal('show');
	        }
	    });
    });
</script>