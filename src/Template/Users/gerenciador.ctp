<!-- <div class="col-xs-12 centralizada" style="margin-bottom: 20px"> <h3>Gerenciador do Sistema </h3></div> -->

<div class="col-xs-12 col-md-offset-2">

    <div class="col-xs-12 col-md-2">
        <?=
        $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'fa fa-4x fa-envelope-o', 'aria-hidden' => 'true', 'style' => 'max-height: 72px; color: #003c56']) . '<br/><p class="botaoCentral">  E-mail<br> Marketing</p>',
            'https://login.mailchimp.com/',
            ['class' => 'btn btn-lg btn-block btn-default acoesCentral', 'style' => 'color: #003c56', 'role' => 'button', 'escape' => false, 'target' => '_blank']
        );
        ?>
    </div>

    <div class="col-xs-12 col-md-2">
        <?= $this->Html->link($this->Html->image("smscorreto.png", ['style' => 'max-height: 72px;padding-top:5px']) . '<br/><p class="botaoCentral"> SMS <br>Marketing</p>', ['controller' => 'Users', 'action' => 'smsmarketing'], ['class' => 'btn btn-lg btn-block btn-default acoesCentral centralizada', 'style' => 'color: #003c56', 'role' => 'button', 'escape' => false]); ?>
    </div>

    <div class="col-xs-12 col-md-2">
        <?=
        $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'fa fa-4x fa-whatsapp', 'aria-hidden' => 'true', 'style' => 'max-height: 72px; color: #003c56']) . ' <br><p class="botaoCentral"> Material</br>de Divulgação </p>',
            ['controller' => 'midias-compartilhadas', 'action' => 'index'],
            ['class' => 'btn btn-lg btn-block btn-default acoesCentral', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left']
        );
        ?>
    </div>
    <div class="col-xs-12 col-md-2">
        <?=
        $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'fa fa-4x fa-user-plus', 'aria-hidden' => 'true', 'style' => 'max-height: 72px; color: #003c56']) . ' <br><p class="botaoCentral"> Gerenciar<br>Prospects</p>',
            ['controller' => 'prospects', 'action' => 'index'],
            ['class' => 'btn btn-lg btn-block btn-default acoesCentral disabled', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left']
        );
        ?>
    </div>

</div>
<div class="row">
    <div class="col-xs-12">&nbsp;</div>
</div>
<div class="col-xs-12 col-md-offset-2">

    <div class="col-xs-12 col-md-2">
        <?=
        $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'fa fa-4x fa-feed', 'aria-hidden' => 'true', 'style' => 'max-height: 72px; color: #003c56']) . ' <br><p class="botaoCentral">Gerenciar<br> Comunicados</p>',
            ['#'],
            ['class' => 'btn btn-lg btn-block btn-default acoesCentral disabled', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Enviar nova senha ']
        );
        ?>
    </div>

    <div class="col-xs-12 col-md-2">
        <?=
        $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'fa fa-4x fa-image', 'aria-hidden' => 'true', 'style' => 'max-height: 72px; color: #003c56']) . ' <br><p class="botaoCentral">Gerenciar<br>Banners</p>',
            ['controller' => 'Banners', 'action' => 'index'],
            ['class' => 'btn btn-lg btn-block btn-default acoesCentral', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Configurar Banners ']
        );
        ?>
    </div>
    <div class="col-xs-12 col-md-2">
        <?=
        $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'fa fa-4x fa-youtube', 'aria-hidden' => 'true', 'style' => 'max-height: 72px; color: #003c56']) . ' <br><p class="botaoCentral"> Canal<br> Youtube </p>',
            ['#'],
            ['class' => 'btn btn-lg btn-block btn-default acoesCentral disabled', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Enviar nova senha ']
        );
        ?>
    </div>
    <div class="col-xs-12 col-md-2">
        <?=
        $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'fa-4x far fa-comments', 'aria-hidden' => 'true', 'style' => 'max-height: 72px; color: #003c56']) . ' <br><p class="botaoCentral"> FAQ <br>Chatbot</p>',
            ["#"],
            ['class' => 'btn btn-lg btn-block btn-default acoesCentral disabled', 'role' => 'button', 'escape' => false]
        );
        ?>
    </div>

</div>
<div class="row">
    <div class="col-xs-12">&nbsp;</div>
</div>
<div class="col-xs-12 col-md-offset-2">

    <!--
    <div class="col-xs-12 col-md-2">
        <?=
        $this->Html->link(
            $this->Html->tag('span', '', ['class' => 'fas fa-4x fa-file-signature', 'aria-hidden' => 'true', 'style' => 'max-height: 72px; color: #003c56']) . ' <br><p class="botaoCentral"> Parceiros<br> Consórcios</p>',
            ['controller' => "parceiros", "action" => "index"],
            ['class' => 'btn btn-lg btn-block btn-default acoesCentral disabled', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'left']
        );
        ?>             
    </div>
-->
</div>