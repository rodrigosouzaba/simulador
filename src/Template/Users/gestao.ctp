<div class="btn-group centralizada" role="group">
	<div class="btn-group" role="group">
        <button type="button" class="btn btn-md btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Saúde PME
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><?= $this->Html->link(__('Tabelas'), ['controller' => 'Tabelas', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Ramos'), ['controller' => 'Ramos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Modalidades'), ['controller' => 'Modalidades', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Operadoras'), ['controller' => 'Operadoras', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Produtos'), ['controller' => 'Produtos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Tipo de Acomodação'), ['controller' => 'Tipos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Área de Comercialização'), ['controller' => 'Regioes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Área de Atendimento'), ['controller' => 'Abrangencias', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Carências'), ['controller' => 'Carencias', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Dependentes'), ['controller' => 'Opcionais', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Documentos Necessários'), ['controller' => 'Informacoes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Observações'), ['controller' => 'Observacoes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Rede Credenciada'), ['controller' => 'Redes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Formas de Pagamentos'), ['controller' => 'FormasPagamentos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Reembolsos'), ['controller' => 'Reembolsos', 'action' => 'index']) ?></li>
<!--
            <li><?= $this->Html->link(__('Campanhas'), ['controller' => 'Campanhas', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Operadoras Fechadas'), ['controller' => 'OperadorasFechadas', 'action' => 'index']) ?></li>
-->
        </ul>
    </div>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-md btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Saúde PF
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><?= $this->Html->link(__('Tabelas'), ['controller' => 'PfTabelas', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Entidades'), ['controller' => 'PfEntidades', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Operadoras'), ['controller' => 'PfOperadoras', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Produtos'), ['controller' => 'PfProdutos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Tipo de Acomodação'), ['controller' => 'PfAcomodacoes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Área de Comercialização'), ['controller' => 'PfComercializacoes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Área de Atendimento'), ['controller' => 'PfAtendimentos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Carências'), ['controller' => 'PfCarencias', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Dependentes'), ['controller' => 'PfDependentes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Documentos Necessários'), ['controller' => 'PfDocumentos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Observações'), ['controller' => 'PfObservacoes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Rede Credenciada'), ['controller' => 'PfRedes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Formas de Pagamentos'), ['controller' => 'PfFormasPagamentos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Reembolsos'), ['controller' => 'PfReembolsos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Profissões'), ['controller' => 'PfProfissoes', 'action' => 'index']) ?></li>
             <li><?= $this->Html->link(__('Operadoras Fechadas'), ['controller' => 'PfOperadorasFechadas', 'action' => 'index']) ?></li>
        </ul>
    </div>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-md btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Odonto
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu  dropdown-menu-right">
            <li><?= $this->Html->link(__('Tabelas'), ['controller' => 'OdontoTabelas', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Operadoras'), ['controller' => 'OdontoOperadoras', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Produtos'), ['controller' => 'OdontoProdutos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Área de Comercialização'), ['controller' => 'OdontoComercializacoes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Área de Atendimento'), ['controller' => 'OdontoAtendimentos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Carências'), ['controller' => 'OdontoCarencias', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Dependentes'), ['controller' => 'OdontoDependentes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Documentos Necessários'), ['controller' => 'OdontoDocumentos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Observações'), ['controller' => 'OdontoObservacaos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Rede Credenciada'), ['controller' => 'OdontoRedes', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Formas de Pagamentos'), ['controller' => 'OdontoFormasPagamentos', 'action' => 'index']) ?></li>                                            
            <li><?= $this->Html->link(__('Reembolsos'), ['controller' => 'OdontoReembolsos', 'action' => 'index']) ?></li>
             <li><?= $this->Html->link(__('Operadoras Fechadas'), ['controller' => 'OdontoOperadorasFechadas', 'action' => 'index']) ?></li>
        </ul>
    </div>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-md btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Usuários
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu dropdown-menu-right">
            <li><?= $this->Html->link(__('Listar'), ['controller' => 'Users', 'action' => 'index']) ?></li>
            <!--<li role="separator" class="divider"></li>-->
            <li><?= $this->Html->link(__('Logotipos'), ['controller' => 'Users', 'action' => 'logotipos']) ?></li>
            <li><?= $this->Html->link(__('Indicações'), ['controller' => 'Indicados', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Produtos Sugeridos'), ['controller' => 'ProdutosSugeridos', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('SMS Enviados'), ['controller' => 'Smsenviados', 'action' => 'index']) ?></li>
        </ul>
    </div>
    
<?=
$this->Html->link('Marketing', ['controller' => 'Users', 'action' => 'marketing'], ['class' => 'btn btn-md  btn-default ', 'role' => 'button', 'escape' => false]);
?>
</div>