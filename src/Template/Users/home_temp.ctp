
        <?= $this->Flash->render('auth') ?>

<div class="col-md-12  users form" style="float: right; text-align: right">
    


    <?= $this->Form->create($user, ['id' => 'login', 'class' => 'form-inline']) ?>
<!--        <input name="tipo" value="" type="hidden">
    <div class="form-group">
        <label for="tipo-pf"><input name="tipo" value="pf" id="tipo-pf" type="radio" class="form-control">Pessoa Física</label>
    </div>
    <div class="form-group">
        <label for="tipo-pj"><input name="tipo" value="pj" id="tipo-pj" type="radio" class="form-control">Pessoa Jurídica</label>
    </div>-->
    <div class="form-group"><?= $this->Form->input('username', ['label' => '', 'placeholder' => 'CPF', 'style' => 'height: 30px; padding: 2px; font-size: 100%']) ?></div>
    <div class="form-group"><?= $this->Form->input('password', ['label' => '', 'placeholder' => 'Senha', 'style' => 'height: 30px; padding: 2px; font-size: 100%']) ?></div>
    <div class="form-group" style="margin-bottom: 12px !important">
        <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-user', 'aria-hidden' => 'true']) . ' Login'), ['class' => 'btn btn-sm btn-default ', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Login', 'id' => 'logar']); ?>    
        <span data-toggle='tooltip' data-placement = 'top' title ='Esqueci minha senha'> 
            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-unlock-alt', 'aria-hidden' => 'true']) . ' Esqueci a senha', '#', ['class' => 'btn btn-sm btn-default ', 'data-toggle' => "modal", 'data-target' => "#myModal", 'role' => 'button', 'escape' => false, 'id' => 'senha', 'data-placement' => 'top', 'title' => 'Esqueci minha Senha']); ?>
        </span>
        <span data-toggle='tooltip' data-placement = 'top' title ='Cadastrar Usuário'> 
            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Cadastro Novo', ['controller' => 'users', 'action' => 'add'], ['class' => 'btn btn-sm btn-primary', 'role' => 'button', 'escape' => false,  'title' => 'Cadastro Novo', 'id' => 'add']) ?>
        </span>

    </div>





</div>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Digite o CPF cadastrado no Sistema</h4>
            </div>
            <div class="modal-body" style="min-height: 150px;">

            </div>

        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cadastro de Novo Usuário</h4>
            </div>
            <div class="modal-body" style="min-height: 500px;">

            </div>

        </div>
    </div>
</div>

<?= $this->Form->end() ?>
<script type="text/javascript">
    $("#username").mask("999.999.999-99");

    var count = 0;
//    $('#username').focus(removermascara);
//    $('#username').blur(updateCount);

    function updateCount() {
        if ($(this).val().length == 14) {
            $("#username").mask("99.999.999/9999-99");
        }
        if ($(this).val().length == 11) {
            $("#username").mask("999.999.999-99");
        }

    }

    function removermascara() {
        $("#username").unmask();
    }

    $("#logar").click(function () {
        $.ajax({
            type: "post",
            data: $("#login").serialize(),
            url: "<?php echo $this->request->webroot ?>users/",
        });
    });

    $("#senha").click(function () {
        $.ajax({
            type: "get",
            data: $("#a").serialize(),
            url: "<?php echo $this->request->webroot ?>users/recuperarSenha/",
            success: function (data) {
                $(".modal-body").empty();
                $(".modal-body").append(data);

            }
        });
    });
//    $("#add").click(function () {
//        $.ajax({
//            type: "get",
////            data: $("#a").serialize(),
//            url: "<?php echo $this->request->webroot ?>users/add/",
//            success: function (data) {
//                $(".modal-body").empty();
//                $(".modal-body").append(data);
//
//            }
//        });
//    });



    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    });
    $('#modalAdd').on('shown.bs.modal', function () {
        $('#myInput').focus()
    });

</script>

<!--Start of Tawk.to Script-->
<!--<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/598ca78b1b1bed47ceb03fec/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>-->
<!--End of Tawk.to Script-->
