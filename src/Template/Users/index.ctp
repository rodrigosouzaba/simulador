    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="col-md-4">
            <div class="input-group" style="margin-top: 5px;">
                <input type="text" name="info_busca" class="form-control" placeholder="Pesquisar por Nome, CPF, Email ou Celular" id="info-busca">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default" id="encontrar" style="height: 37px"> <i class="fas fa-search"></i> </button>
                </span>
            </div>
        </div>

        <?= $this->Form->create("busca-especial", ["id" => "busca-especial"]); ?>
        <div class="col-md-4">
            <?= $this->Form->input('estado-id', ['options' => $estados, 'label' => '', 'empty' => 'Filtrar por Estado', 'id' => 'estado-id']); ?>
        </div>
        <div class="col-md-4">
            <?php $status_usuarios = array("NOVOS" => "NOVOS", 'ATIVO' => 'ATIVOS', "ATIVADO" => "ATIVADOS", "SEM-USO" => "ATIVADOS SEM USO", 'PARCIAL-PENDENTE' => 'REATIVAÇÃO PARCIAL PENDENTE', 'TOTAL-PENDENTE' => 'REATIVAÇÃO TOTAL PENDENTE', 'INATIVO' => 'ATIVAÇÃO PENDENTE'); ?>
            <?= $this->Form->input('status', ['options' => $status_usuarios, 'label' => '', 'empty' => 'Filtrar por status', 'id' => 'status_usuario']); ?>
        </div>
        <?= $this->Form->end(); ?>

    </div>
    <div id="respostaConsulta" class="col-xs-12"></div>
    <?= $this->element('avisos') ?>
    <script type="text/javascript">
        $(document).ready(function() {
            listagem();
        });

        function listagem() {
            $(".modalProcessando").modal("hide");
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>users/consulta-usuarios/",
                beforeSend: function() {
                    $("#modalProcessando").modal("show");
                },
                success: function(data) {
                    $("#respostaConsulta").empty();
                    $("#respostaConsulta").append(data);
                    $("#modalProcessando").modal("hide");
                },
                error: function() {
                    $("#modalProcessando").modal("hide");
                    $("#conteudoAviso").empty();
                    $("#conteudoAviso").append('<h3 class="text-danger centralizada">Erro ao lista Usuários</h3>');
                    $("#modalAviso").modal("hide");
                    $("#modalAviso").modal("show");
                    listagem()
                }
            });
        }

        $("#status_usuario").change(function() {
            if ($(this).val()) {
                $("#loader").click();
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
                    data: $("#busca-especial").serialize(),
                    success: function(data) {
                        $("#respostaConsulta").empty();
                        $("#respostaConsulta").append(data);
                        $("#fechar").click();
                        // 	                $(".paginator").hide();

                    }
                });
            }
        });
        $("#estado-id").change(function() {
            $("#loader").click();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
                data: $("#busca-especial").serialize(),
                success: function(data) {
                    $("#respostaConsulta").empty();
                    $("#respostaConsulta").append(data);
                    $("#fechar").click();
                    // 	            $(".paginator").hide();
                }
            });
        });

        $("#encontrar").click(function() {
            $("#loader").click();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
                data: $("#info-busca").serialize(),
                success: function(data) {
                    $("#respostaConsulta").empty();
                    $("#respostaConsulta").append(data);
                    $("#fechar").click();
                    // 		        $(".paginator").hide();
                }
            });
        });
    </script>
