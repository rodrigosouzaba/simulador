<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Indique um amigo</h4>
</div>
<div class="modal-body" >
    <!--
        <b>Usuário: </b> <?= ucwords(strtolower($user['nome'])) . " " . ucwords(strtolower($user['sobrenome'])) ?> 
        <br/>
        <b>E-mail: </b> <?= $user['celular'] ?> -->


    <?php
    echo $this->Form->create('indicacao', ['id' => 'indicacaoamigo']);
    echo $this->Form->hidden('id', ['value' => $user['id']]);
    echo $this->Form->hidden('cel', ['value' => $user['celular'], 'id' => 'cel']);
    echo $this->Form->input('nome', ['type' => 'text']);
    echo $this->Form->input('email');
    echo $this->Form->input('celular', ['class' => 'celular']);
    ?>
    <br/>
</div>
<div class="modal-footer centralizada">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
    <!--<button type="button" class="btn btn-primary">Enviar</button>-->

    <?= $this->Html->link(('Enviar'), 'javascript:void(0)', ['class' => "btn btn-primary btn-md ", 'id' => 'enviarindicacao', 'role' => 'button', 'escape' => false]) ?>
    <div id="loadingIndicacao" style="float: left">&nbsp;</div>
    <?= $this->Form->end(); ?>
</div>
<div id="respostaIndicacao"></div>
<script type="text/javascript">
    $(".celular").mask("(99) 9 9999-9999");
    
    $("#enviarindicacao").click(function () {
        $.ajax({
            type: "POST",
            data: $("#indicacaoamigo").serialize(),
            url: "<?= $this->request->webroot ?>users/indicar/",
            beforeSend: function () {
                $('#loadingIndicacao').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                alert('Indicação enviada!');
                $("#loadingIndicacao").empty();
                $("#respostaIndicacaoa").append(data);
                $(".close").trigger("click");
            }
        });


    });


</script>

