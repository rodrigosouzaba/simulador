<?php echo $this->element('forms/title', ['title' => '<i class="fa fa-user"></i> Usuarios']); ?>
<div class="card-body">

    <div class="card-head card-head-sm" style="border-bottom:1px solid #f2f3f3;">
        <div class="tools" style="padding-right: 2%;">
            <div class="btn-group" style="margin-right: 0px;">
                <button type="button" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" class="btn ink-reaction btn-collapse btn-default">
                    <i class="fa fa-angle-down"></i>
                </button>
            </div>
        </div>
        <header>
            <i class="fa fa-filter" style="vertical-align: inherit;margin-top: -0.3em;margin-left: 2px;margin-right: 4px;"></i> 
            Filtro
        </header>
    </div>

    <div class="collapse" id="collapseExample" class="card-body style-default-light" style="padding: 5px 24px; background-color: #8080802b">
        <?php echo $this->Form->create('search', ['id' => 'pesquisarUsers', 'class' => 'form', 'role' => 'form']); ?>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group ">
                    <?php echo $this->Form->input('Users.nome', ['label' => 'Nome', 'class' => 'form-control']); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= $this->Form->input('Users.username', ['label' => 'CPF', 'class' => 'form-control cpf']); ?>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group ">
                    <?php echo $this->Form->input('Users.bloqueio', ['label' => 'Ativo', 'options' => ['N' => 'Sim', 'S' => 'Não'], 'empty' => 'Todos', 'class' => 'form-control']); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 10px;">
                <button type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processando..." class="btn btn-primary pull-right" style="margin-right: -4px;">
                    <i class="fa fa-search"></i>&nbsp;
                    Pesquisar
                </button>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>

    <div id="gridUsers" style="padding: 24px;" class="row">
        <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
            Total de registros: <?php echo count($dados); ?>
            <table class="table table-condensed table-striped">
                <tr>
                    <th style="width: 115px;">CPF</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Celular</th>
                    <th class="text-center" style="width:5%;">Ativo</th>
                    <th class="text-center" style="width:10%; ">Ações</th>
                </tr>
                <?php foreach ($dados as $v) { ?>
                    <tr>
                        <td><?= $v->username ?></td>
                        <td><?= $v->fullname ?></td>
                        <td><?= $v->email ?></td>
                        <td><?= $v->celular ?></td>
                        <td class="text-center" ><label class="label label-<?php echo $v->ativo_label; ?>"><?php echo $v->ativo; ?></label></td>
                        <td class="text-center" >
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-default btn-icon-toggle dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i></button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><?php echo $this->Html->link('<i class="fa fa-edit"></i>&nbsp Editar', 'javascript: void(0)', array("escape" => false, 'id' => $v->id, 'class' => 'btnEditarUsers')) ?></li>
                                    <li><?php echo $this->Html->link('<i class="fa fa-trash"></i>&nbsp Excluir', 'javascript: void(0)', array("escape" => false, 'id' => $v->id, 'class' => 'btnDeletarUsers')) ?></li>
                                    <li style="background: #F1F1F1; font-size: 9px; text-align: center;">Atualizado em: <?php echo @$v->modified ?></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php }?>
            </table>
    <?php # echo $this->element('paginator') ?>
        </div>
    </div>
</div>
<?php echo $this->element('forms/buttonsFechar') ?>