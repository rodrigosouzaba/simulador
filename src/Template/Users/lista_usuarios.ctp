<div class="col-md-12 col-sm-12 col-xs-12">

    <?= $this->Form->create("busca-especial", ["id" => "busca-especial"]); ?>
    <div class="col-md-4">
        <div class="input-group" style="margin-top: 5px;">
            <input type="text" name="info_busca" class="form-control" placeholder="Pesquisar por Nome, CPF, Email ou Celular" id="info-busca">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" id="encontrar" style="height: 37px"> <i class="fas fa-search"></i> </button>
            </span>
        </div>
    </div>

    <div class="col-md-4">
        <?= $this->Form->input('estado-id', ['options' => $estados, 'label' => '', 'empty' => 'Filtrar por Estado', 'id' => 'estado-id']); ?>
    </div>
    <div class="col-md-4">
        <?php $status_usuarios = array("NOVOS" => "NOVOS", 'ATIVO' => 'ATIVOS', "ATIVADO" => "ATIVADOS", "SEM-USO" => "ATIVADOS SEM USO", 'PARCIAL-PENDENTE' => 'REATIVAÇÃO PARCIAL PENDENTE', 'TOTAL-PENDENTE' => 'REATIVAÇÃO TOTAL PENDENTE', 'INATIVO' => 'ATIVAÇÃO PENDENTE'); ?>
        <?= $this->Form->input('status', ['options' => $status_usuarios, 'label' => '', 'empty' => 'Filtrar por status', 'id' => 'status_usuario']); ?>
    </div>
    <?= $this->Form->end(); ?>

</div>
<div id="respostaConsulta" class="col-xs-12">
    <?php
    if (!empty($users->toArray())) { ?>
        <div class="col-xs-12">&nbsp;</div>
        <table cellpadding="0" cellspacing="0" style="width: 100%" class="table table-condensend table-hover ">
            <thead>
                <tr>
                    <th class="tituloTabela centralizada" style="width: 10% !important">
                        <?= $this->Paginator->sort('created', ['label' => 'Cadastro']) ?></th>
                    <th class="tituloTabela centralizada" style="width: 10% !important">
                        <?= $this->Paginator->sort('ultimologin', ['label' => 'Último Acesso']) ?></th>
                    <th class="tituloTabela" style="width: 35%"><?= $this->Paginator->sort('nome') ?></th>
                    <th class="tituloTabela" style="width: 20% !important">
                        Grupos</th>
                    <th class="actions tituloTabela centralizada" style="width: 35% !important">
                        <button class="btn btn-xs btn-danger centralizada" id="btnExcluir">
                            <span class="fa fa-trash" aria-hidden="true"></span>
                        </button>
                    </th>
                </tr>
            </thead>
            <?= $this->Form->create("MultiDelete", ['url' => ['#'], 'id' => 'formLote']); ?>

            <tbody id="resultadoBusca" style="display: table-row-group">
                <?php
                echo $this->Form->create();
                foreach ($users as $user) : ?>
                    <tr>
                        <td class="centralizada" style="vertical-align: middle"><small><?= $user->created ?></small></td>
                        <td class="centralizada" style="vertical-align: middle">
                            <small><?= $this->Time->format($user->ultimologin, 'dd/MM/YY  HH:mm') ?></small>
                        </td>
                        <td style="vertical-align: middle" class="">
                            <?php if (isset($user->imagem_id) && !empty($user->imagem_id)) { ?>
                                <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="<img src='/<?= $user->imagem_caminho . $user->imagem_nome ?>'/>"><i class="fa fa-picture-o" aria-hidden="true"></i></span>
                            <?php }
                            ?>
                            <?=
                            ucwords(strtolower($user->nome)) . " " .
                                ucwords(strtolower($user->sobrenome)) .
                                "<br><small style='font-size:80%'>" .
                                $user->username . "<br>" .
                                $user->estado . "<br>" .
                                $user->celular . "<br>" .
                                $user->email
                                . "</small>"
                            ?>
                        </td>


                        <td style="vertical-align: middle" class="">
                            <?= $user->grupos ?>
                        </td>
                        <td class="actions centralizada" style="text-align: center; padding-top: 50px;">
                            <?= $this->Html->link(
                                $this->Html->tag('span', '', ['class' => 'fa fa-user', 'aria-hidden' => 'true']),
                                '#',
                                [
                                    'class' => 'btn btn-xs btn-default edit-grupo',
                                    'role' => 'button',
                                    'escape' => false,
                                    'data-toggle' => 'tooltip', 'value' => $user->id, "style" => "min-width: 24px !important; margin:1px 0 !important",
                                    "data-placement" => "top", 'title' => 'Editar Grupos'
                                ]
                            ); ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-edit', 'aria-hidden' => 'true']), ['action' => 'edit', $user->id], ['class' => 'btn btn-xs btn-default cod-celular', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Usuário', "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                            <!-- ATIVACAO CELULAR -->
                            <?php

                            switch ([$user->validacao]) {
                                case ['ATIVO']:
                                    $icone = "fa fa-check";
                                    $cor = "success";
                                    $title = 'CELULAR ATIVADO';
                                    break;
                                case ['INATIVO']:
                                    $icone = "fa fa-minus";
                                    $cor = "default";
                                    $title = 'ATIVAÇÃO PENDENTE<br/><small>Celular inativo</small>';
                                    break;
                                case ['PENDENTE']:
                                    $icone = "fa fa-times";
                                    $cor = "warning";
                                    $title = 'REATIVAÇÃO PENDENTE<br/><small>Celular alterado</small>';
                                    break;
                            }
                            ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => $icone, 'aria-hidden' => 'true']), ['action' => 'reenviarCodigo', $user->id], ['class' => 'cod-email btn btn-xs btn-' . $cor, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-html' => "true", 'title' => $title, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                            <!-- /ATIVACAO CELULAR -->

                            <!-- ATIVACAO E-MAIL -->
                            <?php
                            switch ([$user->validacao_email]) {
                                case ['ATIVO']:
                                    $iconeEmail = "fa fa-check";
                                    $corEmail = "success";
                                    $titleEmail = 'E-MAIL ATIVADO';
                                    break;
                                case ['INATIVO']:
                                    $iconeEmail = "fa fa-minus";
                                    $corEmail = "default";
                                    $titleEmail = 'INATIVO<br/><small>E-mail inativo</small>';
                                    break;
                                case ['PENDENTE']:
                                    $iconeEmail = "fa fa-times";
                                    $corEmail = "warning";
                                    $titleEmail = 'REATIVAÇÃO PENDENTE<br/><small>E-mail alterado</small>';
                                    break;
                            }
                            ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => $iconeEmail, 'aria-hidden' => 'true']), ['action' => 'reenviarCodigoEmailAdmin', $user->id], ["value" => $user->id, 'class' => 'btn btn-xs btn-' . $corEmail, 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-html' => "true", 'title' => $titleEmail, "style" => "min-width: 24px !important; margin:1px 0 !important"]) ?>
                            <!-- /ATIVACAO E-MAIL -->
                            <?=
                            $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-key ', 'aria-hidden' => 'true']), ['action' => 'recuperarPadrao', $user->username], ['class' => 'btn btn-xs btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Senha Provisória', "style" => "min-width: 24px !important; margin:1px 0 !important"]);
                            ?>
                            <?php
                            switch ($user->bloqueio) {
                                case "S":
                                    $iconeBloqueio = "fa fa-lock";
                                    $acaoBloqueio = "DESBLOQUEIO";
                                    $title = "Desbloquear Usuário";
                                    break;
                                case "N":
                                    $acaoBloqueio = "BLOQUEIO";
                                    $iconeBloqueio = "fa fa-unlock";
                                    $title = "Bloquear Usuário";
                                    break;
                            }
                            ?>
                            <?= $this->Html->link($this->Html->tag('span', '', ['class' => $iconeBloqueio, 'aria-hidden' => 'true']), '#', ['class' => 'btn btn-xs btn-default bloqueio', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => $title, 'value' => $user->id, "acao" => $acaoBloqueio, "style" => "min-width: 24px !important; margin:1px 0 !important"]); ?>
                            <div class="btn btn-xs" style="padding: 1px 0px;">
                                <?= $this->Form->checkbox('id[]', ['hiddenField' => false, 'value' => $user->id, "class" => "selecionar-exclusao"]) ?>
                            </div>
                        </td>

                    </tr>
                <?php endforeach; ?>
                <?= $this->Form->end(); ?>
            </tbody>
        </table>
    <?php } else {

        echo "<div class='col-xs-12'>&nbsp;</div>";
        echo "<div class='col-xs-12 centralizada alert alert-danger'>Nenhum Usuário encontrado</div>";
    } ?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first(__('Primeira', true)); ?>
            <?= $this->Paginator->prev('< ' . __('')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('') . ' >') ?>
            <?= $this->Paginator->last(__('Última', true), array('class' => 'disabled')); ?>


        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body" id="renderView"></div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalGrupos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body" id="renderGrupos"></div>
            </div>
        </div>
    </div>
</div>
<?= $this->element('avisos') ?>
<script type="text/javascript">
    $(".edit-grupo").click(function() {
        $("#modalGrupos").modal("show");
    });

    $("#modalGrupos").on('hide.bs.modal', function() {
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>users-grupos/syncUser/",
            data: $("#addGrupo").serialize(),
            success: function(data) {
                $("#resposta-grupos").empty();
                $("#resposta-grupos").append(data);
            }
        });
    });

    $("#status_usuario").change(function() {
        $("#busca-especial").submit()
    });
    $("#estado-id").change(function() {
        $("#busca-especial").submit()
    });

    $("#encontrar").click(function() {
        $("#busca-especial").submit()
    });

    $(document).ready(function() {
        $('[data-toggle="popover"]').popover();

        $("body").tooltip({
            selector: '[data-toggle=tooltip]'
        });
        $('#togle').tooltip();
    });

    $("#tipo-busca").change(function() {
        $("#busca").show();
        if ($("#tipo-busca").val() === 'cpf') {
            $("#info-busca").mask("999.999.999-99");
        } else {
            $("#info-busca").unmask();
        }
    });

    $(".cod-cel").click(function() {
        $("#loader").click();
    });
    $(".cod-email").click(function() {
        $("#loader").click();
    });


    $(".smsForm").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/sms/" + $(this).attr('value'),
            success: function(data) {
                $("#renderView").empty();
                $("#renderView").append(data);
            }
        });
    });

    $(".emailUsuario").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/enviarEmail/" + $(this).attr('value'),
            success: function(data) {
                $("#renderView").empty();
                $("#renderView").append(data);
            }
        });
    });

    $(".edit-grupo").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/editGrupos/" + $(this).attr('value'),
            success: function(data) {
                $("#renderGrupos").empty();
                $("#renderGrupos").append(data);
            }
        });
    });


    $(".deletar").click(function() {
        if (confirm("Confirma exclusão do Usuário?")) {
            $.ajax({
                type: "post",
                url: "<?php echo $this->request->webroot ?>users/delete/" + $(this).attr('value'),
                success: function(data) {
                    alert('Usuário excluído com sucesso');
                    location.reload();
                }
            });
        } else {
            return false;
        }
    });

    $(".bloqueio").click(function() {
        var acao = $(this).attr('acao');
        var id = $(this).attr('value');
        $.ajax({
            type: "post",
            url: "<?php echo $this->request->webroot ?>users/bloqueio/" + id + "/" + acao,
            success: function(data) {
                alert(acao + ' realizado com sucesso');
                location.reload();
            }
        });
    });

    $("#btnExcluir").click(function() {
        //SE CONSULTA TIVER ORIGEM NO CAMPO DE ENCONTRAR USUÁRIO
        var busca = $("#info-busca").val();
        var estado = $("#estado-id").val();
        var status = $("#status_usuario").val();

        var users = $('input:checkbox').filter(':checked');
        $("#loader").click();


        $.ajax({
            type: "post",
            data: users,
            url: "<?= $this->request->webroot ?>users/delete-lote/",
            success: function(data) {
                if (busca) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
                        data: $("#info-busca").serialize(),
                        success: function(data) {
                            $("#respostaConsulta").empty();
                            $("#respostaConsulta").append(data);
                            $("#fechar").click();
                            $("#conteudoAviso").empty();
                            $("#conteudoAviso").append("<div class='centralizada'>Usuário(s) Exluido(s) com Sucesso.</div>");
                            $("#modalAviso").modal("show");
                        }
                    });
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->request->webroot ?>users/consulta_usuarios/",
                        data: $("#busca-especial").serialize(),
                        success: function(data) {
                            $("#respostaConsulta").empty();
                            $("#respostaConsulta").append(data);
                            $("#fechar").click();
                            $("#conteudoAviso").empty();
                            $("#conteudoAviso").append("<div class='centralizada'>Usuário(s) Exluido(s) com Sucesso.</div>");
                            $("#modalAviso").modal("show");
                        }
                    });
                }
            },
            error: function() {
                $("#conteudoAviso").empty();
                $("#conteudoAviso").append("<div class='centralizada text-danger'>Erro no processo de Exclusão</div>");
                $("#modalAviso").modal("show");
            }
        });
    });
</script>