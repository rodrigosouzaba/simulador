<section class="section-account" style="background-color:#e5e6e66e">
<div class="img-backdrop" style="text-align: center;">
		<img src="http://corretorparceiro.com.br/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png" style="width: 30%; padding-top: 3%;">
	</div>
	<div class="spacer" style="height: 10px;"></div>
	<div class="card style-transparent">
		<div style="padding-top:20px">
				<div class="row" style="margin: 0;">
					<div class="col-xs-12 arealogin" >

						<?= $this->Form->create('User', ['id' => 'loginModal']) ?>
							<div class="input text" >
								<input type="text" name="username" class="form-control text-center input" placeholder="Usuário (CPF)" id="username" kl_vkbd_parsed="true" >
							</div>
							<div class="input password" >
								<input type="password" name="password" class="form-control text-center input" placeholder="Senha" id="password" kl_vkbd_parsed="true" >
							</div>
							<!-- <button id="entrar" class="col-xs-12 btn btn-primary btn-login" style="margin-bottom: 5px" type="button" >
								Entrar <span id="loading-login" ></span>
							</button> -->
							<div class="form-group col-xs-12" >
								<?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-user', 'aria-hidden' => 'true']) . ' Login'), ['class' => 'btn btn-md btn-primary btn-block ', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Login', 'id' => 'logarModal', 'style' => 'margin-bottom : 10px !important;']); ?>    
							</div>
							<div class="form-group col-xs-12" >
								<?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', 'https://corretorparceiro.com.br', ['class' => 'btn btn-md btn-default btn-block', 'data-dismiss' => "modal", 'role' => 'button', 'escape' => false]); ?>
							</div>
							<?= $this->Form->end() ?>
						</form>

						<div id="recuperar" style="display: none;" >
							<div class="col-xs-12 text-center" style="margin-bottom: 15px;">
								<div class="input text"><input type="text" name="cpf" class="text-center col-xs-12 col-md-3 col-md-offset-3 placeholder" placeholder="Digite o CPF Cadastrado" style="margin: 0px;" id="cpf" kl_vkbd_parsed="true" ></div>
								<a href="#" id="post-senha" style="color: #ea1919; font-size: 13px; text-decoration: none;">Enviar Nova Senha</a>
							</div>
						</div>
					</div>
                </div>
		</div><!--end .card-body -->
	</div><!--end .card -->
</section>