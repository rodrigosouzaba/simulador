<style>
    .background {
        height: 100vh;
        width: 100vw;
        background-image: linear-gradient(0deg, #003E55 50%, white 50%);
        align-items: center;
        justify-content: center;
        flex-direction: column;
    }

    .area-login {
        background-color: white;
        margin: 0 auto;
        width: 500px;
        height: 500px;
        box-shadow: 0 0 40px -10px #ccc;
        border-radius: 3px;
    }

    .logo {
        width: 100%;
        text-align: center;
    }

    .logo img {
        width: 60%;
        max-width: 300px;
    }

    .login-title {
        font-size: 22px;
        text-align: center;
        margin: 50px 0;
    }

    .btn-login {
        background-color: #003E55;
        border-color: #002b3c;
        font-size: 14px;
        color: #FFF;
        width: 100%;
        margin: 20px 0;
    }

    .input-login {
        text-align: center;
    }

    .input-login::placeholder {
        color: #001922;
    }

    .esqueceu-senha {
        width: 100%;
        display: block;
    }

    @media (max-width: 500px) {
        .area-login {
            width: 100vw;
        }
    }
</style>
<?php
$frases_login = [
    'Acesse sua Conta',
    'Entre no seu perfil',
    'Acesse o Corretor Parceiro'
];
?>
<div class="background">
    <div class="logo">
        <?= $this->Html->image('https://corretorparceiro.com.br/wp-content/uploads/2020/09/cropped-logo-web-corpar-3-1.png') ?>
    </div>
    <div class="row area-login">
        <h1 class="login-title"><?= $frases_login[rand(0, count($frases_login) - 1)]; ?></h1>
        <div class="col-md-8 col-md-offset-2">
            <?= $this->Form->create($user) ?>
            <?= $this->Form->input('username', ['class' => 'input-login', 'label' => false, 'placeholder' => 'Usuário (CPF)']) ?>
            <?= $this->Form->input('password', ['class' => 'input-login', 'label' => false, 'placeholder' => 'Senha']) ?>
            <?= $this->Form->submit('Entrar', ['class' => 'btn btn-default btn-login', 'id' => 'entrar']) ?>
            <?= $this->Form->end() ?>
            <a href="#" class="text-center esqueceu-senha">Esqueceu sua senha? <span class="text-danger">Clique Aqui</span></a>
            <div class="spacer-xs">&nbsp;</div>
            <div class="text-center" style="width: 100%;"> Não possui cadastro?</div>
            <?= $this->Html->link('Cadastre-se Grátis', '#', ['class' => 'btn btn-danger', 'style' => 'width: 100%;']) ?>
        </div>
    </div>

</div>