<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<?= $this->Flash->render('auth') ?>

<div class="col-xs-12 col-md-4 col-md-offset-4  users form centralizada">
    <h2>Login</h2>
    <?= $this->Form->create($user, ['id' => 'loginModal']) ?>

    <?= $this->Form->input('test', ['type' => 'hidden', 'value' => $test]) ?>
    <div class="form-group col-xs-12" style="margin-bottom: 0 !important"><?= $this->Form->input('username', ['label' => '', 'placeholder' => 'CPF', 'class' => 'username centralizada', 'style' => 'margin-bottom: 0px !important; ']) ?></div>
    <div class="form-group col-xs-12" style="margin-bottom: 0 !important"><?= $this->Form->input('password', ['type' => 'password', 'label' => '', 'placeholder' => 'Senha', 'class' => 'centralizada', 'style' => 'margin-bottom: 5px !important; ']) ?></div>
    <div class="form-group col-xs-12" >
            <small>
        Esqueceu sua senha?
                
        <?=
            $this->Html->link(
                    'Clique aqui', '#', [
//                        'class' => 'btn btn-md btn-default btn-block', 
                'role' => 'button',
                'escape' => false,
                'id' => 'senha-modal',
                'data-placement' => 'top',
                'title' => 'Esqueci minha Senha',
                'style' => 'margin-bottom : 10px !important;',
                'data-toggle' => "modal",
                'data-target' => "#modalRecuperarSenha"]);
            ?>
            </small>
       
    </div>
    <div class="form-group col-xs-6" >
        <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-user', 'aria-hidden' => 'true']) . ' Login'), ['class' => 'btn btn-md btn-primary btn-block ', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Login', 'id' => 'logarModal', 'style' => 'margin-bottom : 10px !important;']); ?>    
    </div>

    <div class="form-group col-xs-6" >

        <?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['action' => 'login'], ['class' => 'btn btn-md btn-default btn-block', 'data-dismiss' => "modal", 'role' => 'button', 'escape' => false]); ?>
    </div>
    <?= $this->Form->end() ?>
</div>
<div class="clearfix">&nbsp;</div>


<!-- Modal -->
<div class="modal fade" id="modalRecuperarSenha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >Digite o CPF cadastrado no Sistema</h4>
            </div>
            <div class="modal-body" style="min-height: 150px;" id="conteudoModalSenha">

            </div>

        </div>
    </div>
</div>
<!-- Modal 
<div class="modal fade" id="modalSenha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header centralizada">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Recuperar senha</h4>
            </div>
            <div class="modal-body" style="min-height: 500px;">

            </div>

        </div>
    </div>
</div>-->
<?= $this->Form->end() ?>
<script type="text/javascript">
    $(".username").mask("999.999.999-99");

    var count = 0;
    //    $('#username').focus(removermascara);
    //    $('#username').blur(updateCount);

    function updateCount() {
        if ($(this).val().length == 14) {
            $("username").mask("99.999.999/9999-99");
        }
        if ($(this).val().length == 11) {
            $("#username").mask("999.999.999-99");
        }

    }

    function removermascara() {
        $("#username").unmask();
    }

//    $("#logarModal").click(function () {
//        alert('AQUI');
//        $.ajax({
//            type: "POST",
//            data: $("#loginModal").serialize(),
//            url: "<?= $this->request->webroot ?>users/login_modal/"
//        });
//    });


    $("#senha-modal").click(function () {
//        $("#myModalLabelCadastro").text('Recuperação de Senha');
        $.ajax({
            type: "get",
//            data: $("#a").serialize(),
            url: "<?php echo $this->request->webroot ?>users/recuperarSenha/",
            success: function (data) {
                $("#conteudoModalSenha").empty();
                $("#conteudoModalSenha").append(data);

            }
        });
    });

</script>

<!--Start of Tawk.to Script-->
<!--<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/598ca78b1b1bed47ceb03fec/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>-->
<!--End of Tawk.to Script-->
