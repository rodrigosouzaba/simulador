<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('username', ['label' => 'Login']) ?></th>
            <th><?= $this->Paginator->sort('nome') ?></th>
            <th><?= $this->Paginator->sort('imagem_id', ['label' => 'Imagem']) ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody id="resultadoBusca">
        <?php foreach ($users as $user) : ?>
            <tr>
                <?php // debug($user->ultimocalculo);die();
                ?>
                <td><?= h($user->username) ?></td>
                <td><?= ucwords(strtolower($user->nome)) . " " . ucwords(strtolower($user->sobrenome)) ?></td>
                <td><?php if (isset($user->imagen->caminho)) { ?>
                        <span style="font-size: 90%" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="<img src='/<?= $user->imagen->caminho . $user->imagen->nome ?>'/>" class="text-danger">
                            <img src="/app/<?= $user->imagen->caminho . $user->imagen->nome ?>" class="logoMiniatura" />
                        </span>
                    <?php } ?></td>

                <td class="actions">
                    <?php
                    echo $this->Html->link($this->Html->tag('span', '', ['class' => 'glyphicon glyphicon-edit', 'aria-hidden' => 'true']), ['action' => 'edit', $user->id], ['class' => 'btn btn-xs btn-default', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar Usuário', 'style' => 'margin-left: 5px;']);
                    echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-envelope', 'aria-hidden' => 'true']), '#', ['class' => 'btn btn-xs btn-default emailForm', 'role' => 'button', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#enviarEmail', 'title' => 'Enviar Email', 'style' => 'margin-left: 5px;', 'value' => $user->id]);
                    echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-commenting', 'aria-hidden' => 'true']), '#', ['class' => 'btn btn-xs btn-default smsForm', 'role' => 'button', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#myModal', 'title' => 'Enviar SMS', 'style' => 'margin-left: 5px;', 'value' => $user->id]);
                    echo $this->Html->link($this->Html->tag('span', '', ['class' => 'glyphicon glyphicon-trash', 'aria-hidden' => 'true']), ['action' => 'remover_logo', $user->id], ['class' => 'btn btn-xs btn-danger', 'role' => 'button', 'escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Excluir logotipo do usuário', 'style' => 'margin-left: 5px;']);
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first(__('Primeira', true)); ?>
        <?= $this->Paginator->prev('< ' . __('')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('') . ' >') ?>
        <?= $this->Paginator->last(__('Última', true), array('class' => 'disabled')); ?>


    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>

<!-- Modal -->
<div class="modal fade" id="enviarEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modalEmail">

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="renderView">

        </div>
    </div>
</div>

<script type="text/javascript">
    $("#busca").hide();
    $('[data-toggle="popover"]').popover();

    $("#tipo-busca").change(function() {
        $("#busca").show();
        //        alert($("#tipo-busca").val());
        if ($("#tipo-busca").val() === 'cpf') {
            $("#info-busca").mask("999.999.999-99");
        } else {
            $("#info-busca").unmask();
        }
    });

    $(".smsForm").click(function() {
        //        alert();
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/sms/" + $(this).attr('value'),
            //            data: $("#encontrarCalculo").serialize(),
            success: function(data) {
                $("#renderView").empty();
                $("#renderView").append(data);
            }
        });
    });

    $("#encontrar").click(function() {
        //        alert();
        $.ajax({
            type: "POST",
            url: "<?php echo $this->request->webroot ?>users/encontrarUsuario/",
            data: $("#encontrarCalculo").serialize(),
            beforeSend: function() {
                $('#loading').html("<img src='<?php $this->request->webroot ?>img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function(data) {
                $("#resultadoBusca").empty();
                $("#resultadoBusca").append(data);
                $("#loading").empty();
            }
        });
    });
    $(".emailForm").click(function() {
        //        alert();
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>users/enviarEmail/" + $(this).attr('value'),
            //            data: $("#encontrarCalculo").serialize(),
            success: function(data) {
                $("#modalEmail").empty();
                $("#modalEmail").append(data);
            }
        });
    });
</script>
