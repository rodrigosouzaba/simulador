<style>
.five {
    width: 7% !important;
    vertical-align: middle !important;
    text-align: center !important;
}

.checkboxmailchimp {
    margin-top: 15% !important;
    margin-left: -3px !important;
}
</style>
<div class='col-xs-12'>
    <?php
    //Grupos MAILCHIMP
    /*
        Ferramentas = c81fff0722;
        Z ANTIGO - Natuseg - Inativos = 1dd8fbd2a5;
        Z ANTIGO - Natuseg - Ativos = 8c02a6aaed;
        Z ANTIGO - Natuseg - Bloqueados = 24c3657c29;
        Z ANTIGO - Corpar = b8be87ae2d;
        Z ANTIGO - Corpar - Bloqueados = fdda6f1c5f;
        Z ANTIGO - Corpar - Prospects = 835c25f778;
        Z ANTIGO - Corpar - News = 3961bd91ca;
        Z ANTIGO - Prospects - Qualicorp = ba5e8e707a;
        TODOS BA = 1f9b10bbad;
        TODOS ES = c238c953c3;
        TODOS AM = 58678d9cdd;
        TODOS MG = c9a9e2bbc2;
        TODOS RN = dc9e0f8472;
        TODOS SUSEP = 3bcf0ef8f8;
        CORRETORES ASSESSORIA = 4653094540;
        NATUSEG ATIVOS = 0b34c537ce;
        NATUSEG INATIVOS = 2dbceb5a19;
    */

    $idBA = '1f9b10bbad';
    $idES = 'c238c953c3';
    $idAM = '58678d9cdd';
    $idMG = 'c9a9e2bbc2';
    $idRN = 'dc9e0f8472';
    $idSusep = '3bcf0ef8f8';
    $idAssessoria = '4653094540';
    $idInativos = '2dbceb5a19';
    $idAtivos = '0b34c537ce';
    $idFA = 'c81fff0722';
            
    if($users){
    ?>
    <table class="table table-condensed table-hover">
        <tr>
            <th style='vertical-align: middle'>Usuário</th>
            <th class='five'>FA</th>
            <th class='five'>BA</th>
            <th class='five'>ES</th>
            <th class='five'>AM</th>
            <th class='five'>MG</th>
            <th class='five'>RN</th>
            <th class='five'>SUSEP</th>
            <th class='five'>Natuseg Ativos</th>
            <th class='five'>Natuseg Inativos</th>
        </tr>
        <?= $this->Form->create('update-mailchimp', ['id' => 'update-mailchimp', '#']) ?>

        <?php
        foreach ($users->members as $user) { 
            // debug($user);die;
            $ba = false;
            $es = false;
            $am = false;
            $mg = false;
            $rn = false;
            $fa = false;
            $susep = false;
            $assessoria = false;
            $ativo = false;
            $inativo = false;
            
            if($user->interests->$idBA == true){
                $ba = 'checked';
            }
            if($user->interests->$idES == true){
                $es = 'checked';
            }
            if($user->interests->$idAM == true){
                $am = 'checked';
            }
            if($user->interests->$idMG == true){
                $mg = 'checked';
            }
            if($user->interests->$idRN == true){
                $rn = 'checked';
            }
            if($user->interests->$idFA == true){
                $fa = 'checked';
            }
            if($user->interests->$idSusep == true){
                $susep = 'checked';
            }
            if($user->interests->$idAssessoria == true){
                $assessoria = 'checked';
            }
            if($user->interests->$idAtivos == true){
                $ativo = 'checked';
            }
            if($user->interests->$idInativos == true){
                $inativo = 'checked';
            }
        ?>
        <tr class='line' mailchimp-id='<?= md5($user->email_address)?>'>
            <td>
                <?= ($user->merge_fields->NOME ? $user->merge_fields->NOME : '<i>Sem Nome</i>') ?><br>
                <?= $user->email_address ?><br>
                <small style='text-transform: capitalize'>
                    <?= ($user->status == 'subscribed' ? 'Assinante' : ($user->status == 'unsubscribed'  ? 'Descadastrou-se'  : $user->status)) ?>
                </small>
            </td>
            <!-- <td>
                <a href="#" class="btn btn-default singlesync">Sincronizar</a>
            </td> -->
            <td class='centralizada'>
                <?= $this->Form->input(md5($user->email_address).'.changed', ['type' => 'hidden', 'class' => 'checkboxmailchimp', 'value'  => 'false']); ?>
                <?= $this->Form->input(md5($user->email_address).'.interests.c81fff0722', ['type' => 'checkbox', 'label' => false, $fa, 'class' => 'checkboxmailchimp']); ?>
            </td>
            <td class='centralizada'>
                <?= $this->Form->input(md5($user->email_address).'.interests.1f9b10bbad', ['type' => 'checkbox', 'label' => false, $ba, 'class' => 'checkboxmailchimp']); ?>
            </td>
            <td class='centralizada'>
                <?= $this->Form->input(md5($user->email_address).'.interests.c238c953c3', ['type' => 'checkbox', 'label' => false, $es, 'class' => 'checkboxmailchimp']); ?>
            </td>
            <td class='centralizada'>
                <?= $this->Form->input(md5($user->email_address).'.interests.58678d9cdd', ['type' => 'checkbox', 'label' => false, $am, 'class' => 'checkboxmailchimp']); ?>
            </td>
            <td class='centralizada'>
                <?= $this->Form->input(md5($user->email_address).'.interests.c9a9e2bbc2', ['type' => 'checkbox', 'label' => false, $mg, 'class' => 'checkboxmailchimp']); ?>
            </td>
            <td class='centralizada'>
                <?= $this->Form->input(md5($user->email_address).'.interests.dc9e0f8472', ['type' => 'checkbox', 'label' => false, $rn, 'class' => 'checkboxmailchimp']); ?>
            </td>
            <td class='centralizada'>
                <?= $this->Form->input(md5($user->email_address).'.interests.3bcf0ef8f8', ['type' => 'checkbox', 'label' => false, $susep, 'class' => 'checkboxmailchimp']); ?>
            </td>
            <td class='centralizada'>
                <?= $this->Form->input(md5($user->email_address).'.interests.0b34c537ce', ['type' => 'checkbox', 'label' => false, $ativo, 'class' => 'checkboxmailchimp']); ?>
            </td>
            <td class='centralizada'>
                <?= $this->Form->input(md5($user->email_address).'.interests.2dbceb5a19', ['type' => 'checkbox', 'label' => false, $inativo, 'class' => 'checkboxmailchimp']); ?>
            </td>
        </tr>
        <?php }
        $this->Form->end();
        ?>
    </table>

    <div class='col-xs-12 centralizada'>
        <a href="#" class="btn btn-danger" id="syncbutton"><i class="fa fa-refresh" aria-hidden="true"></i>
            Sincronizar</a>
    </div>
    <div class='col-xs-12'>&nbsp;</div>

    <div class="paginator">
        <ul class="pagination">
            <?php

    $i = 0;
    $label = 1;
    $pages = ceil($users->total_items/50);

        for ($i=0; $i < $pages ; $i++) { 
            $active = '';
            
            if(isset($_GET['l']) && $_GET['l'] == $label){
                $active = 'active';
            }
            
            echo '<li class="'.$active.'">'. $this->Html->link($label, ["action" => "mailchimp_data", $i, "l" => $label]).'</li>';
            $label++;
        }
    ?>
        </ul>
    </div>
    <?php
    }else{

    }

    ?>
</div>
<?= $this->element('processando'); ?>

<script type="text/javascript">
$('.line').click(function() {
    var id = $(this).attr('mailchimp-id');
    $('#' + id + '-changed').val('true');
});
$("#syncbutton").click(function() {
    $('#loader').trigger('click');
    $.ajax({
        type: "POST",
        url: "<?php echo $this->request->webroot ?>users/syncmailchimp/",
        data: $("#update-mailchimp").serialize(),
        success: function(data) {
            $('#fechar').trigger('click');
        }
    });
});
$(".singlesync").click(function() {
    $('#loader').trigger('click');
    $.ajax({
        type: "POST",
        url: "<?php echo $this->request->webroot ?>users/syncmailchimpunique/",
        data: $("#update-mailchimp").serialize(),
        success: function(data) {
            $('#fechar').trigger('click');
        }
    });
});
</script>