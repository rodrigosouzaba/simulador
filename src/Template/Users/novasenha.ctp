<?php $this->assign('title', $title); ?>
<?= $this->Form->create($user) ?>
<div class="col-xs-12 col-md-6 col-md-offset-3 centralizada">
    <h3 style="margin-bottom: 20px;"><?= $user->nome ?>, defina uma nova senha</h3>
    <?= $this->Form->hidden('id'); ?>
    <?= $this->Form->hidden('role', ['readonly' => 'readonly', 'label' => 'Tipo', 'options' => ['admin' => 'ADMIN', 'padrao' => 'USUÁRIO']], ['value' => $user['role']]); ?>
    <?= $this->Form->input('password', ['label' => 'Crie uma senha', 'value' => '', 'required' => 'required', 'class' => 'centralizada']); ?>
    <?= $this->Form->input('confirmacao', ['label' => 'Confirme sua senha', 'value' => '', 'required' => 'required', 'class' => 'centralizada', 'type' => 'password']); ?>
    <div id="aviso" class="alertaform"></div>
    <div class="col-md-12 centralizada fonteReduzida">
        <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => "btn btn-primary btn-md disabled", 'id' => 'salvar']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
    $('#confirmacao').blur(function () {
        var pass = $('input[name=password]').val();
        var repass = $('input[name=confirmacao]').val();
        if ((pass.length < 6) || (repass.length < 6)) {
//           alert('Senha e Confirmação devem ter ao menos 6 dígitos');
            $('#aviso').empty();
            $('#aviso').append('Senha e Confirmação devem ter ao menos 6 dígitos');
            $('#password').val('');
            $('#confirmacao').val('');
        } else if (pass != repass) {
            $('#aviso').empty();
            $('#aviso').append('Senha e Confirmação não conferem');
            $('#salvar').addClass('disabled');
        } else {
            $('#aviso').empty();
            $('#salvar').removeClass('disabled');
        }
    });
    $('#salvar').click(function () {
        var pass = $('input[name=password]').val();
        var repass = $('input[name=confirmacao]').val();
        if (pass != repass) {
            window.location("<?= $this->request->webroot."users/novasenha/".$user->id?>");
        } 
    });
</script>

<style>
    .has-success {
        background:green;
    }
    .has-error {
        background:red;
    }
</style>