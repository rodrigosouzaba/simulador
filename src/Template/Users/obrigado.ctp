<div class="col-xs-12 col-md-10 col-md-offset-1">
    <div class="jumbotron   centralizada" style="padding: 40px !important">
        <h3 style="margin: 0 !important">Obrigado por enviar seus dados.</h3>
        <br/>
        Em breve <b><?= $user['nome'] . " " . $user['sobrenome'] ?></b> entrará em contato com você!
    </div>
</div>