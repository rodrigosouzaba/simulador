<section id="AppOdontoCalculos">
    <div id="pagina">
        <style>
            .nav-tabs>li.active>a,
            .nav-tabs>li.active>a>span,
            .nav-tabs>li.active>a:focus,
            .nav-tabs>li.active>a:focus>span,
            .nav-tabs>li.active>a:hover {
                color: #fff !important;
                background-color: #003C55 !important
            }

            .nav-tabs>li>a {
                border-radius: 4px !important;
                border: 1px solid #ddd !important
            }

            .nav-tabs>li>a:hover,
            .nav-tabs>li>a:hover>span {
                background-color: #c2c2c2 !important
            }

            .nav-tabs>li.active>a,
            .nav-tabs>li.active>a:hover>span,
            .nav-tabs>li.active>a:focus,
            .nav-tabs>li.active>a:hover {
                border-bottom-color: #ddd !important;
                background-color: #003C55 !important
            }
        </style>



        <ul class="nav nav-tabs" id="myTabs" role="tablist" style="border: none !important; margin-left: 0 !important; ">
            <li role="presentation" class="col-md-4 col-xs-12">
                <a href="#pf" class="btn btn-default" id="odonto-pf-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">
                    <b style="text-transform: uppercase">Odonto - Pessoa Física</b>
                    <br>
                    <span class="fonteMicro">
                        Individual, Familiar e Adesão
                    </span>

                </a>
            </li>
            <li role="presentation" class="col-md-4 col-xs-12">
                <a href="#pme" class="btn btn-default" role="tab" id="odonto-pme-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true" style="border-radius: 4px !important;">
                    <b style="text-transform: uppercase">Odonto - Pessoa Jurídica</b><br>
                    <span class="fonteMicro">
                        Micro, Pequenas, Médias Empresas
                    </span>
                </a>
            </li>
            <li class="col-md-4 col-xs-12">
                <a href="#" class="btn btn-default" role="tab" id="odonto-gerador-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true" style="border-radius: 4px !important;">
                    <b style="text-transform: uppercase">Gerar Tabelas de preços</b><br>
                    <span class="fonteMicro">
                        Planos Odontológicos
                    </span>
                </a>


            </li>
            <!--
            <li role="presentation" class="col-md-3 col-xs-12">
                <a href="#pme" class="btn btn-default disabled" role="tab" id="pme-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true" style="border-radius: 4px !important;">
                    <b style="text-transform: uppercase">Saúde Pessoa Jurídica </b><small class="text text-danger">(BREVE)</small><br>
                    <span class="fonteMicro">
                        Projetos Especiais e Convenções Coletivas
                    </span>
                </a>
            </li>
        -->


        </ul>

        <div class="col-xs-12">&nbsp;</div>
        <div id="respostaAjax" class="col-xs-12">
        </div>

        <?= $this->element("processando") ?>
        <script type="text/javascript">
            $('#myTabs a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            });
            $(document).ready(function() {
                
            });

        </script>
    </div>
</section>