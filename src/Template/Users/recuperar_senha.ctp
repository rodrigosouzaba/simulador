<?= $this->Form->create('user') ?>
<div class="col-md-3"></div>
<div class="col-md-6"><?= $this->Form->input('cpf', ['label' => '', 'class' => 'centralizada']); ?></div>
 

<div class="col-md-12 centralizada fonteReduzida">
    <div class="col-xs-6 centralizada fonteReduzida">
        <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-user', 'aria-hidden' => 'true', 'id' => 'texto']) . ' Recuperar senha'), ['class' => 'btn btn-sm btn-primary btn-block ', 'id' => 'recuperarSenha', 'role' => 'button', 'escape' => false]); ?>    
    </div>
    <div class="col-xs-6 centralizada fonteReduzida">
        <button type="button" id="fecharModal" class="btn btn-sm btn-default btn-block" data-dismiss="modal"> <span class="fa fa-remove" aria-hidden="true"></span> Cancelar</button>
    </div>
</div>
<div class="clearfix">&nbsp;</div>

<div class="clearfix">&nbsp;</div>

<div class="col-xs-12 fonteReduzida centralizada" id="loading">&nbsp;</div>
<?= $this->Form->end() ?>
<?= $this->element("processando"); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#cpf').focus();
        $("#cpf").mask("999.999.999-99");
        $("#celular").mask("(99) 9 9999-9999");
    });

    $("#recuperarSenha").click(function () {
	    $("#loader").click();
    });
</script>