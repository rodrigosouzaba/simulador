<div id="pagina">
    <style>
        .nav-tabs>li.active>a,
        .nav-tabs>li.active>a>span,
        .nav-tabs>li.active>a:focus,
        .nav-tabs>li.active>a:focus>span,
        .nav-tabs>li.active>a:hover {
            color: #fff !important;
            background-color: #003C55 !important
        }

        .nav-tabs>li>a {
            border-radius: 4px !important;
            border: 1px solid #ddd !important
        }

        .nav-tabs>li>a:hover,
        .nav-tabs>li>a:hover>span {
            background-color: #c2c2c2 !important
        }

        .nav-tabs>li.active>a,
        .nav-tabs>li.active>a:hover>span,
        .nav-tabs>li.active>a:focus,
        .nav-tabs>li.active>a:hover {
            border-bottom-color: #ddd !important;
            background-color: #003C55 !important
        }
    </style>
    <!--
    <p class="centralizada" style="font-size: 16px;text-transform: uppercase" id="titulo">
        Selecione o tipo de cálculo
    </p>
-->
    <!-- 	<div class="col-xs-12 col-md-offset-2 col-md-8" style="z-index: 999;"> -->
    <div class="col-md-12" style="padding-left: 0 !important; padding-right: 0 !important;">


        <!-- 			<a href="#" id="pf-tab" class="btn btn-default btn-lg btn-block"><b>SAÚDE PESSOA FÍSICA</b><br>Individual, Familiar e Adesão</a> -->



        <ul class="nav nav-tabs" id="myTabs" role="tablist" style="border: none !important; margin-left: 0 !important; ">
            <li role="presentation" class="col-md-4 col-xs-12" id="spf">
                <a href="#pf" class="btn btn-default" id="pf-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">
                    <b id="titulo" style="text-transform: uppercase">Saúde - Pessoa Física</b>
                    <br>
                    <span class="fonteMicro">
                        Individual, Familiar e Adesão
                    </span>
                </a>
            </li>
            <li role="presentation" class="col-md-4 col-xs-12">
                <a href="#pme" class="btn btn-default" role="tab" id="pme-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true" style="border-radius: 4px !important;">
                    <b style="text-transform: uppercase">Saúde - Pessoa Jurídica</b><br>
                    <span class="fonteMicro">
                        Micro, Pequenas e Médias Empresas
                    </span>
                </a>
            </li>
            <li class="col-md-4 col-xs-12">
                <a href="#" class="btn btn-default" role="tab" id="gerador-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true" style="border-radius: 4px !important;">
                    <b style="text-transform: uppercase">Gerar Tabelas de preços</b><br>
                    <span class="fonteMicro">
                        Planos de Saúde
                    </span>
                </a>


            </li>
            <!--
	        <li role="presentation" class="col-md-3 col-xs-12">
	            <a href="#pme" class="btn btn-default disabled" role="tab" id="pme-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true" style="border-radius: 4px !important;">
		            <b style="text-transform: uppercase">Saúde Pessoa Jurídica </b><small class="text text-danger">(BREVE)</small><br>
		            <span class="fonteMicro">
		            	Projetos Especiais e Convenções Coletivas
		            </span>
	            </a>
	        </li>
	    -->


        </ul>
        <!-- 		</div> -->
    </div>
    <div class="col-xs-12">&nbsp;</div>
    <div id="respostaAjax" class="col-xs-12">
    </div>




</div>
