

<?php $this->assign('title', $title); ?>
<?= $this->Form->create($user) ?>
<fieldset>
    <?= $this->Form->hidden('id'); ?>
    <div class="col-md-12"><?= $this->Form->input('nome', ['readonly' => 'readonly', 'value' =>$user['nome']]); ?></div>
    <div class="col-md-6"><?= $this->Form->input('username', ['readonly' => 'readonly','label' => 'Login (CPF)']); ?></div>
    <div class="col-md-6"><?= $this->Form->input('new_password', ['label' => 'Nova Senha', 'value' => '']); ?></div>
    <div class="col-md-6"><?= $this->Form->hidden('role', ['readonly' => 'readonly','label' => 'Tipo', 'options' => ['admin' => 'ADMIN', 'padrao' => 'USUÁRIO']], ['value' => $user['role']]); ?></div>
</fieldset>
<div class="col-md-12 centralizada fonteReduzida">
    <?= $this->Form->button(__($this->Html->tag('span', '', ['class' => 'fa fa-floppy-o', 'aria-hidden' => 'true']) . ' Salvar'), ['class' => "btn btn-primary btn-md "]) ?>

    <?=
    $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-remove', 'aria-hidden' => 'true']) . ' Cancelar', ['action' => 'view',$user->id], ['class' => 'btn btn-md btn-default', 'role' => 'button', 'escape' => false]);
    ?>
</div>
<?= $this->Form->end() ?>