<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Enviar SMS</h4>
</div>
<div class="modal-body" >

    <b>Usuário: </b> <?= ucwords(strtolower($user['nome'])) . " " . ucwords(strtolower($user['sobrenome'])) ?> 
    <br/>
    <b>Celular: </b> <?= $user['celular'] ?> 

    <br/>
    <br/>
    <?php
    echo $this->Form->create('sms', ['id' => 'sms']);
    echo $this->Form->hidden('id', ['value' => $user['id']]);
    echo $this->Form->hidden('cel', ['value' => $user['celular'], 'id' => 'cel']);
    echo $this->Form->input('mensagem');
    ?>
    <br/>
</div>
<div class="modal-footer centralizada">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
    <!--<button type="button" class="btn btn-primary">Enviar</button>-->

    <?= $this->Html->link(('Enviar'), 'javascript:void(0)', ['class' => "btn btn-primary btn-md ", 'id' => 'enviar', 'role' => 'button', 'escape' => false]) ?>
    <div id="loading2" style="float: left">&nbsp;</div>
    <?= $this->Form->end(); ?>
</div>
<div id="a"></div>
<script type="text/javascript">

    $("#enviar").click(function () {
//                window.location.replace('http://110.2.1.16/sendsms?username=admin&password=admin&phonenumber=' + cel + '&message=' + msg + '&[port=1&]');
        $.ajax({
            type: "post",
            data: $("#sms").serialize(),
            url: "<?= $this->request->webroot ?>users/sms/",
            beforeSend: function () {
                $('#loading2').html("<img src='/img/spinner.gif' style='max-height: 60px;margin:-10px;' />");
            },
            success: function (data) {
                alert('SMS Enviado com Sucesso!');
                $("#loading2").empty();
//                $("#a").append(data);
                $(".close").trigger("click");
            }
        });


    });


</script>