<div class="col-xs-12 centralizada" style="margin-bottom: 20px">
    <h3>SMS Marketing</h3>
</div>

<ul class="nav nav-tabs" id="myTabs" role="tablist" style="border: none !important; margin-left: 0 !important; ">
    <li role="presentation" class="col-md-offset-3 col-md-3 col-xs-12">
        <a href="#" class="btn btn-default" id="campanhas" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">
            <b style="text-transform: uppercase">CAMPANHAS</b>
        </a>
    </li>
    <!-- <li class="col-md-4 col-xs-12">
        <a href="#" class="btn btn-default" role="tab" id="listas" data-toggle="tab" aria-controls="profile" aria-expanded="true" style="border-radius: 4px !important;">
            <b style="text-transform: uppercase">LISTAS</b>
        </a>
    </li> -->
    <!-- <li class="col-md-3 col-xs-12">
        <a href="#" class="btn btn-default" role="tab" id="situacoes" data-toggle="tab" aria-controls="profile" aria-expanded="true" style="border-radius: 4px !important;">
            <b style="text-transform: uppercase">SITUAÇÕES</b>
        </a>
    </li> -->
    <li class="col-md-3 col-xs-12">
        <a href="#" class="btn btn-default" role="tab" id="cabecalhos" data-toggle="tab" aria-controls="profile" aria-expanded="true" style="border-radius: 4px !important;">
            <b style="text-transform: uppercase">Cabeçalhos</b>
        </a>
    </li>

</ul>
<div class="clearfix">&nbsp;</div>

<div id="resposta" class="col-xs-12"></div>

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="modal-add-sms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Novo(a)</h4>
            </div>
            <div class="modal-body" id="modalResposta">
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    $("#campanhas").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-envios/index",
            beforeSend: function() {
                $("#modalProcessando").modal("show")
            },
            success: function(data) {
                $("#modalProcessando").modal("hide");
                $("#resposta").empty();
                $("#resposta").append(data);
            }
        });
    });
    $("#listas").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-listas/index",
            beforeSend: function() {
                $("#modalProcessando").modal("show")
            },
            success: function(data) {
                $("#modalProcessando").modal("hide");
                $("#resposta").empty();
                $("#resposta").append(data);
            }
        });
    });
    $("#situacoes").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-situacoes/index",
            beforeSend: function() {
                $("#modalProcessando").modal("show")
            },
            success: function(data) {
                $("#modalProcessando").modal("hide");
                $("#resposta").empty();
                $("#resposta").append(data);
            }
        });
    });
    $("#cabecalhos").click(function() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->request->webroot ?>sms-cabecalhos/index",
            beforeSend: function() {
                $("#modalProcessando").modal("show")
            },
            success: function(data) {
                $("#modalProcessando").modal("hide");
                $("#resposta").empty();
                $("#resposta").append(data);
            }
        });
    });
</script>