<div class="clearfix">&nbsp;</div>
<div class="jumbotron col-md-8 col-md-offset-2"  style="padding: 20px;">
    <h3 class="centralizada" style="margin: 0 0 20px !important;">Minha Conta</h3>
    <table class="table table-condensed" style="margin-bottom: 0 !important;">
        <tr>
            <td><i class="fa fa-user"></i> <?= __('Usuário') ?></td>
            <td><?= h($user['username']) ?></td>
        </tr>

        <tr>
            <td><i class="fa fa-user"></i> <?= __('Nome') ?></td>
            <td><?= h($user['nome']) . " " . h($user['sobrenome']) ?></td>
        </tr>

        <tr>
            <td><i class="fa fa-map-marker"></i> Estado de Atuação</td>
            <td><?= h($user['estado_nome']) ?></td>
        </tr>
        <tr>
            <td><i class="fa fa-envelope"></i> <?= __('E-mail') ?></td>
            <td><?= h($user['email']) ?></td>
        </tr>
        <tr>
            <td><i class="fa fa-phone"></i> <?= __('Celular') ?></td>
            <td><?= h($user['celular']) ?></td>
        </tr>
        <tr>
            <td><i class="fa fa-birthday-cake"></i> <?= __('Data de Nascimento') ?></td>
            <td><?= h($user['data_nascimento']) ?></td>
        </tr>
        <tr>
            <td><i class="fa fa-refresh"></i> Data da última alteração</td>
            <td><?= h($user['modified']) ?></td>
        </tr>
        <tr>
            <td><i class="fa fa-desktop"></i> Site</td>
            <!--$variavel = (!isset($variavel)) ? 'valor padrão' : $variavel;-->
            <td><?= (isset($user['site'])) ? h($user['site']) : '<small>Não informado</small>'; ?></td>
        </tr>
        <tr>
            <td><i class="fa fa-facebook-official"></i> Facebook (Perfil ou FanPage)</td>
            <td><?= (isset($user['facebook'])) ? h($user['facebook']) : '<small>Não informado</small>'; ?></td>
        </tr>
        <tr>
            <td><i class="fa fa-whatsapp"></i> Whatsapp</td>
            <td><?= (isset($user['whatsapp'])) ? h($user['whatsapp']) : '<small>Não informado</small>'; ?></td>
        </tr>
        <tr>
            <td style="vertical-align: middle !important">
                <i class="fa fa-picture-o"></i> <?= __('Logotipo Personalizado') ?>
                <br/><?php
//            echo $user['imagem_id'];
//                if (isset($imagem['id'])) {
//                    echo $this->Html->link('Alterar seu Logotipo', ['controller' => 'Users', 'action' => 'addImagem', $user['id']], ['escape' => false]);
////                   echo $this->Html->image($imagem['caminho'].$imagem['nome']);
//                }
//                else {
//                    echo $this->Html->link('Insira seu logotipo', ['controller' => 'Users', 'action' => 'addImagem', $user['id']], ['class' => 'btn btn-sm  btn-default ', 'role' => 'button', 'escape' => false]);
//                }
                ?>
            </td>
            <td style="vertical-align: middle !important">
                <?php
//            echo $user['imagem_id'];
                if (isset($imagem['id'])) {
                    echo "<img style='max-height: 50px !important;' src='/" . $imagem['caminho'] . $imagem['nome'] . "'/><br/><br/>";
//                    echo $this->Html->link('Alterar seu Logotipo', ['controller' => 'Users', 'action' => 'addImagem', $user['id']], ['escape' => false]);
//                   echo $this->Html->image($imagem['caminho'].$imagem['nome']);
                } else {
                    echo $this->Html->link('Insira seu Logotipo', ['controller' => 'Users', 'action' => 'addImagem', $user['id']], ['escape' => false]);

//                    echo $this->Html->link('Insira seu logotipo', ['controller' => 'Users', 'action' => 'addImagem', $user['id']], ['class' => 'btn btn-sm  btn-default ', 'role' => 'button', 'escape' => false]);
                }
                ?>
            </td>
        </tr>

    </table>
    <div class="col-md-12 centralizada">
        <?=
        $this->Html->link('Alterar Logotipo', ['action' => 'addImagem', $user['id']], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'edit']);
        ?>
        <?=
        $this->Html->link('Alterar Dados', ['action' => 'edit', $user['id']], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'edit']);
        ?>
        <?=
        $this->Html->link('Alterar Senha', ['action' => 'senha', $user['id']], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false, 'id' => 'senha']);
        ?>
        <!--<?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-picture-o', 'aria-hidden' => 'true']) . ' Central de Marketing', ['controller' => 'Campanhas', 'action' => 'index'], ['class' => 'btn btn-sm btn-primary botaoSalvar', 'role' => 'button', 'escape' => false]);
        ?>-->
    </div>
</div>

