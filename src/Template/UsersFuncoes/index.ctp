<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Users Funco'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Funcoes'), ['controller' => 'Funcoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Funco'), ['controller' => 'Funcoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersFuncoes index large-9 medium-8 columns content">
    <h3><?= __('Users Funcoes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('funcao_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersFuncoes as $usersFunco): ?>
            <tr>
                <td><?= $this->Number->format($usersFunco->id) ?></td>
                <td><?= $usersFunco->has('user') ? $this->Html->link($usersFunco->user->id, ['controller' => 'Users', 'action' => 'view', $usersFunco->user->id]) : '' ?></td>
                <td><?= $usersFunco->has('funco') ? $this->Html->link($usersFunco->funco->id, ['controller' => 'Funcoes', 'action' => 'view', $usersFunco->funco->id]) : '' ?></td>
                <td><?= h($usersFunco->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usersFunco->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usersFunco->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usersFunco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersFunco->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
