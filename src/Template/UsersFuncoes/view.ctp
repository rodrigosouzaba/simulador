<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Users Funco'), ['action' => 'edit', $usersFunco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Funco'), ['action' => 'delete', $usersFunco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersFunco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Funcoes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Funco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Funcoes'), ['controller' => 'Funcoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Funco'), ['controller' => 'Funcoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersFuncoes view large-9 medium-8 columns content">
    <h3><?= h($usersFunco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $usersFunco->has('user') ? $this->Html->link($usersFunco->user->id, ['controller' => 'Users', 'action' => 'view', $usersFunco->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Funco') ?></th>
            <td><?= $usersFunco->has('funco') ? $this->Html->link($usersFunco->funco->id, ['controller' => 'Funcoes', 'action' => 'view', $usersFunco->funco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersFunco->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($usersFunco->created) ?></td>
        </tr>
    </table>
</div>
