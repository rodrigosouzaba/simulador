<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Users Grupo'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersGrupos index large-9 medium-8 columns content">
    <h3><?= __('Users Grupos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('grupo_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersGrupos as $usersGrupo): ?>
            <tr>
                <td><?= $this->Number->format($usersGrupo->id) ?></td>
                <td><?= $usersGrupo->has('user') ? $this->Html->link($usersGrupo->user->id, ['controller' => 'Users', 'action' => 'view', $usersGrupo->user->id]) : '' ?></td>
                <td><?= $usersGrupo->has('grupo') ? $this->Html->link($usersGrupo->grupo->id, ['controller' => 'Grupos', 'action' => 'view', $usersGrupo->grupo->id]) : '' ?></td>
                <td><?= h($usersGrupo->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usersGrupo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usersGrupo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usersGrupo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersGrupo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
