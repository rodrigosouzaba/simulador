<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Users Grupo'), ['action' => 'edit', $usersGrupo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Grupo'), ['action' => 'delete', $usersGrupo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersGrupo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Grupos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Grupo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersGrupos view large-9 medium-8 columns content">
    <h3><?= h($usersGrupo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $usersGrupo->has('user') ? $this->Html->link($usersGrupo->user->id, ['controller' => 'Users', 'action' => 'view', $usersGrupo->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Grupo') ?></th>
            <td><?= $usersGrupo->has('grupo') ? $this->Html->link($usersGrupo->grupo->id, ['controller' => 'Grupos', 'action' => 'view', $usersGrupo->grupo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersGrupo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($usersGrupo->created) ?></td>
        </tr>
    </table>
</div>
