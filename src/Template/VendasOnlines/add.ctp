<div class="col-md-12">
    <div class="modulo">Adicionar Categoria</div>
    <div class="space-md">&nbsp</div>
    <?= $this->Form->create($vendasOnline, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <div class="col-md-2 flex" style="padding: 5px 20px; align-items: baseline;">
            <?= $this->Form->checkbox('exibir', ['value' => 1, 'id' => 'check-ocultar']) ?>
            <label for="check-ocultar">Exibir na Tela Principal</label>
        </div>
        <div class="col-md-2 flex" style="padding: 5px 20px; align-items: baseline;">
            <?= $this->Form->checkbox('exibir_menu', ['value' => 1, 'id' => 'exibir_menu']) ?>
            <label for="exibir_menu">Exibir no Menu</label>
        </div>
        <div class="col-md-2 flex" style="padding: 5px 20px; align-items: baseline;">
            <?= $this->Form->checkbox('exibir_vendas_online', ['value' => 1, 'id' => 'exibir_vendas_online']) ?>
            <label for="exibir_vendas_online">Exibir em Vendas Online</label>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('link', ['label' => false, 'value' => 'https://corretorparceiro.com.br/app/users/lojas/' . $vendasOnline->id, 'readonly']) ?>
        </div>
        <div class="col-md-2">
            <?= $this->Form->input('nome', ['label' => false, 'placeholder' => 'Botão Linha 1']) ?>
        </div>
        <div class="col-md-2">
            <?= $this->Form->input('subcategoria', ['label' => false, 'placeholder' => 'Botão Linha 2']) ?>
        </div>
        <div class="col-md-2">
            <?= $this->Form->input('funcionalidade_id', ['label' => false, 'empty' => 'Permissão', 'options' => $funcionalidades]) ?>
        </div>
        <div class="col-md-2">
            <?= $this->Form->input('ordem', ['label' => false, 'placeholder' => 'Prioridade']) ?>
        </div>
        <div class="col-md-4">
            <?= $this->element('file_input', ['value' => $vendasOnline->icones, 'placeholder' => 'Icone', 'name' => 'icone', 'id' => 'icone', 'accept' => 'image/*']) ?>
        </div>
        <div class="clearfix">&nbsp</div>
        <!-- LINHA 2  -->
        <div class="col-md-3">
            <?= $this->Form->input('linha1', ['label' => false, 'placeholder' => 'Nome Tela Principal Linha1', 'class' => 'linha2', 'disabled' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('linha2', ['label' => false, 'placeholder' => 'Nome Tela Principal Linha2', 'class' => 'linha2', 'disabled' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $this->element('file_input', ['value' => $vendasOnline->imagem, 'placeholder' => 'Imagem', 'name' => 'imagem', 'id' => 'iimagem', 'accept' => 'image/*', 'disabled' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('dica', ['label' => false, 'placeholder' => 'Dica', 'class' => 'linha2', 'disabled' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('menu_linha1', ['label' => false, 'placeholder' => 'Menu Linha 1', 'class' => 'linha3', 'disabled' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input('menu_linha2', ['label' => false, 'placeholder' => 'Menu Linha 2', 'class' => 'linha3', 'disabled' => true]) ?>
        </div>
        <div class="clearfix">&nbsp</div>
    </fieldset>
    <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary col-md-2 col-md-offset-5']) ?>
    <?= $this->Form->end() ?>
</div>
<script>
    $("#check-ocultar").click(function() {
        if ($(this).is(':Checked')) {
            $(".linha2").removeAttr('disabled');
        } else {
            $(".linha2").attr('disabled', true);
        }
    });
    $("#exibir_menu").click(function() {
        if ($(this).is(':Checked')) {
            $(".linha3").removeAttr('disabled');
        } else {
            $(".linha3").attr('disabled', true);
        }
    });
    $(document).ready(function() {
        if ($("#check-ocultar").is(':Checked')) {
            $(".linha2").removeAttr('disabled');
        } else {
            $(".linha2").attr('disabled', true);
        }

        if ($("#exibir_menu").is(':Checked')) {
            $(".linha3").removeAttr('disabled');
        } else {
            $(".linha3").attr('disabled', true);
        }
    })
</script>