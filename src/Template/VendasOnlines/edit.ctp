<div class="vendasOnlines form large-9 medium-8 columns content">
    <?= $this->Form->create($vendasOnline) ?>
    <fieldset>
        <legend>Editar Venda Online</legend>
        <?php
        echo $this->Form->input('nome');
        echo $this->Form->input('icone');
        echo $this->Form->input('ordem');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
