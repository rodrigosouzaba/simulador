<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<style>
    .img-thumbnail {
        max-height: 120px;
        max-width: 120px;
    }

    .buttons {
        width: 100%;
        display: flex;
        overflow: auto;
        justify-content: center;
        padding-bottom: 10px;
    }

    .group-operadoras {
        display: flex;
        flex-wrap: wrap;
    }

    .before-card {
        width: 15%;
        margin: 0.71%;
        text-decoration: none;
        text-align: center;
    }

    .card {
        border: 1px solid #ccc;
        border-radius: 5px;
        overflow: hidden;
    }

    .card-header img {
        width: 100%;
    }

    .card-body {
        padding: 5px;
        font-size: 14px;
        color: #595959
    }

    .btn-vendas-active,
    .btn-vendas-active:active,
    .btn-vendas-active:hover,
    .btn-vendas-active:focus {
        background-color: #003656;
        color: #FFF;
    }

    .btnModalVideos {
        color: #d9534f;
        border: 0;
    }

    .btnModalVideos:hover {
        color: #d9534f;
    }

    .btnManuais {
        color: #d9534f;
        border: 0;
    }

    .btnManuais:hover {
        color: #d9534f;
    }

    .btnManuais:focus {
        color: #d9534f;
        background-color: #FFF;
    }

    .btnMaterial {
        color: #d9534f;
        border: 0;
    }

    .btnMaterial:hover {
        color: #d9534f;
    }

    .btnMaterial:focus {
        color: #d9534f;
        background-color: #FFF;
    }

    @media(max-width: 991px) {
        .before-card {
            width: 23%;
            margin: 1%;
        }

        .buttons {
            justify-content: start;

        }

    }

    @media(max-width: 600px) {
        .before-card {
            width: 46%;
            margin: 2%;
        }

    }
</style>
<div id="lojas" class="centralizada btn-group buttons" role="tablist">
    <?php foreach ($vendasOnline as $index => $venda) : ?>
        <?= $this->Html->link($venda['nome'] . '</br>' . $venda['subcategoria'], '#tab-' . $venda['id'], ["class" =>  isset($this->request->params['pass'][0]) ? $this->request->params['pass'][0] == $venda['id'] ? 'btn btn-md btn-default btn-vendas btn-vendas-active' : "btn btn-md btn-default btn-vendas" : "btn btn-md btn-default btn-vendas", 'escape' => false,  "type" => "button", "role" => "tab", "data-toggle" => "tab"]) ?>
    <?php endforeach; ?>
</div>
<div class="spacer-md">&nbsp;</div>
<div class="tab-content" style="margin-top: 20px;">
    <?php foreach ($vendasOnline as $venda) : ?>
        <div role="tabpanel" class="tab-pane <?= isset($this->request->params['pass'][0]) ? $this->request->params['pass'][0] == $venda['id'] ? 'active' : false : false ?>" id="tab-<?= $venda['id'] ?>">
            <div class="col-xs-12 col-md-10 col-md-offset-1 group-operadoras">
                <?php foreach ($venda['vendas_onlines_operadoras'] as $operadora) : ?>
                    <div class="before-card">
                        <form id="form-<?= $operadora['id'] ?>" method="POST" action="<?= $this->request->webroot ?>vendaonline" target="_blank">
                            <?= $this->Form->input('destino', ['type' => 'hidden', 'value' => $operadora->link]) ?>
                        </form>
                        <div id="<?= $operadora['id'] ?>" class="card" redirect="<?= $operadora->redirect ?>" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="<?= $operadora->tooltip ?>">
                            <div class="card-header">
                                <?= $this->Html->image('vendas_online/' . $operadora->imagem) ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <span><?= $operadora->linha1 ?></span>
                            <br />
                            <span><?= $operadora->linha2 ?></span>
                        </div>
                        <?= $this->Form->textarea(null, ['id' => "aviso-" . $operadora['id'], "value" => $operadora->aviso, 'class' => 'hidden']) ?>
                        <?php if (!empty($operadora->link_video)) : ?>
                            <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'fas fa-play']) . ' Assistir  Vídeo', ['class' => 'btn btn-sm btn-default btnModalVideos', 'urlVideo' => $operadora->link_video]) ?>
                        <?php endif; ?>
                        <?php if (!empty($operadora->manual)) : ?>
                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'far fa-bookmark']) . ' Ler Manual', $this->request->webroot . 'uploads/venda_online/manuais/' . $operadora->manual, ['class' => 'btn btn-sm btn-default btnManuais', 'target' => '_blank', 'escape' => false]) ?>
                        <?php endif; ?>
                        <?php if (!empty($operadora->material_vendas)) : ?>
                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'far fa-folder']) . ' Material de Vendas',  $operadora->material_vendas, ['class' => 'btn btn-sm btn-default btnMaterial', 'target' => '_blank', 'escape' => false]) ?>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?= $this->element('modal_avisos') ?>

<!-- Modal VIDEOS -->
<div class="modal fade" id="ModalVideos" tabindex="-1" role="dialog" aria-labelledby="ModalVideosLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center" style="border: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" style="display: flex; justify-content: center;">
                <iframe id="iframe-videos" frameborder='0' width="560" height="315" src="" allow="autoplay; encrypted-media" allowfullscreen>
                </iframe>
            </div>
        </div>
    </div>
</div>
<script>
    $(".card").click(function() {
        let id = $(this).attr('id');
        let texto = $("#aviso-" + id).val();
        let redirect = $(this).attr("redirect");
        let destino = $("#form-" + id).children("#destino").val();

        if (texto !== '') {
            $("#conteudoAviso").empty();
            $("#conteudoAviso").append(texto);
            $("#modalAviso").modal('show');
            $("#continuar").attr("form", id);
            $("#solicitar").show();
            if (redirect == 1) {
                $("#continuar").attr("redirect", 1);
            } else {
                $("#continuar").removeAttr("redirect");
            }
        } else {
            $.ajax({
                url: baseUrl + 'vendas-onlines-operadoras/get-link-personalizado/' + id + '/<?= $sessao['username'] ?>',
                complete: function(data) {
                    data = data.responseJSON;
                    if (data.length > 0) {
                        if (redirect == 1) {
                            window.open(data[0].link, '_blank');
                        } else {
                            $("#form-" + id).attr('action', data[0].link);
                            $("#form-" + id).submit();
                        }
                    } else {
                        if (redirect == 1) {
                            window.open(destino, '_blank');
                        } else {
                            $("#form-" + id).submit();
                        }
                    }
                }
            })
        }
    });

    $("#continuar").click(function() {
        let id = $(this).attr("form");
        $("#modalAviso").modal('hide');
        let redirect = $(this).attr("redirect");
        let destino = $("#form-" + id).children("#destino").val();
        $.ajax({
            url: baseUrl + 'vendas-onlines-operadoras/get-link-personalizado/' + id + '/<?= $sessao['username'] ?>',
            complete: function(data) {
                data = data.responseJSON;
                if (data.length > 0) {
                    if (redirect == 1) {
                        window.open(data[0].link, '_blank');
                    } else {
                        $("#form-" + id).attr('action', data[0].link);
                        $("#form-" + id).submit();
                    }
                } else {
                    if (redirect == 1) {
                        window.open(destino, '_blank');
                    } else {
                        $("#form-" + id).submit();
                    }
                }
            }
        })
    });

    $(".btn-vendas").click(function() {
        var link = $(this);

        $(".btn-vendas").removeClass("btn-vendas-active");
        link.addClass("btn-vendas-active");
        $("#venda-online").val(link.attr("venda_online"));
    });

    $(".btnModalVideos").click(function() {
        let url = $(this).attr('urlVideo')
        $("#iframe-videos").attr("src", "");
        $("#iframe-videos").attr("src", url);
        $("#ModalVideos").modal("show");
    });

    $('#lojas a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });

    $('[data-toggle="popover"]').popover();

    $("#solicitar").click(function() {
        $("#conteudoAviso").empty()
        $("#conteudoAviso").append("<p class='centralizada'>Obrigado, em breve nossa equipe entrará em contato com você.<p/>")
        $("#solicitar").hide();
        $.post(baseUrl + 'avaliacaos/add', {
            user_id: "<?= $sessao['id'] ?>",
            nota: 99,
            tipo: 'ATENDIMENTO VENDAS ONLINE'
        })
    });

    $(".card").click(function() {
        let id = $(this).attr('id');
        let texto = $("#aviso-" + id).val();
        let redirect = $(this).attr("redirect");
        let destino = $("#form-" + id).children("#destino").val();

        if (texto !== '') {
            showModalAviso(id, texto);
            if (redirect == 1) {
                $("#continuar").attr("redirect", 1);
            } else {
                $("#continuar").removeAttr("redirect");
            }
        } else {
            getLinkPersonalizado(id, redirect, destino);
        }
    });

    $("#continuar").click(function() {
        let id = $(this).attr("form");
        $("#modalAviso").modal('hide');
        let redirect = $(this).attr("redirect");
        let destino = $("#form-" + id).children("#destino").val();

        getLinkPersonalizado(id, redirect, destino);

    });

    $(".btn-vendas").click(function() {
        var link = $(this);

        $(".btn-vendas").removeClass("btn-vendas-active");
        link.addClass("btn-vendas-active");
        $("#venda-online").val(link.attr("venda_online"));
    });

    $(".btnModalVideos").click(function() {
        let url = $(this).attr('urlVideo')
        $("#iframe-videos").attr("src", "");
        $("#iframe-videos").attr("src", url);
        $("#ModalVideos").modal("show");
    });

    $('#lojas a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });

    $('[data-toggle="popover"]').popover();

    function showModalAviso(id, texto) {
        $("#conteudoAviso").empty();
        $("#conteudoAviso").append(texto);
        $("#modalAviso").modal('show');
        $("#continuar").attr("form", id);
        $("#solicitar").show();
    }

    function getLinkPersonalizado(id, redirect, destino) {
        $.ajax({
            url: baseUrl + 'vendas-onlines-operadoras/get-link-personalizado/' + id + '/<?= $sessao['username'] ?>',
            complete: function(data) {
                data = data.responseJSON;
                if (data.length > 0) {
                    if (redirect == 1) {
                        window.open(data[0].link, '_blank');
                    } else {
                        $("#form-" + id).children("#destino").val(data[0].link);
                        $("#form-" + id).submit();
                    }
                } else {
                    if (redirect == 1) {
                        window.open(destino, '_blank');
                    } else {
                        $("#form-" + id).submit();
                    }
                }
            }
        })
    }
</script>