<?php
$controller = $this->request->controller;
$action = $this->request->action;
// debug($vendasOnlines->toArray());
?>
<style>
    .heading-color {
        background-color: #003656 !important;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 10px 10px 5px 10px;
    }

    .heading-color .panel-title {
        color: #FFF !important;
    }

    .bloco-add {
        border-right: 1px solid #f5f5f5
    }

    .img-thumbnail {
        max-height: 120px;
        max-width: 120px;
    }

    .buttons {
        margin-top: 20px;
    }

    .group-operadoras {
        display: flex;
        flex-wrap: wrap;
    }

    .before-card {
        width: 10%;
        margin: 0.71%;
        text-decoration: none;
        text-align: center;
    }

    .card {
        border: 1px solid #ccc;
        border-radius: 5px;
        overflow: hidden;
    }

    .card:hover .actions {
        visibility: visible;
    }

    .card-header {
        position: relative;
    }

    .card-header img {
        width: 100%;
    }

    .card-body {
        padding: 5px;
        font-size: 14px;
        color: #595959
    }

    .btn-vendas-active,
    .btn-vendas-active:active,
    .btn-vendas-active:hover,
    .btn-vendas-active:focus {
        background-color: #003656;
        color: #FFF;
    }

    .btnModalVideos {
        color: #d9534f;
        border: 0;
    }

    .btnModalVideos:hover {
        color: #d9534f;
    }

    .btnManuais {
        color: #d9534f;
        border: 0;
    }

    .btnManuais:hover {
        color: #d9534f;
    }

    .btnManuais:focus {
        color: #d9534f;
        background-color: #FFF;
    }

    .btnMaterial {
        color: #d9534f;
        border: 0;
    }

    .btnMaterial:hover {
        color: #d9534f;
    }

    .btnMaterial:focus {
        color: #d9534f;
        background-color: #FFF;
    }

    @media(max-width: 991px) {
        .before-card {
            width: 23%;
            margin: 1%;
        }

    }

    @media(max-width: 600px) {
        .before-card {
            width: 46%;
            margin: 2%;
        }

    }

    .oculta {
        opacity: 0.2;
    }

    #add-buttton {
        display: none;
    }

    .actions {
        visibility: hidden;
        position: absolute;
        left: 3px;
        right: 3px;
        bottom: 3px;
        display: flex;
        justify-content: space-between;
        flex-direction: column;
    }

    .estados {
        position: absolute;
        top: 50;
        left: 0;
        border: 1px solid #ccc;
    }
</style>
<div class="col-xs-12">
    <div class="modulo">Configurar Vendas Online</div>
    <div class="col-xs-12 centralizada" style="margin-bottom: 10px !important;">
        <div class="btn-group">
            <?php foreach ($vendasOnlines as $vendasOnline) : ?>
                <a href="<?= "#tab-" . $vendasOnline->id ?>" aria-controls="<?= $vendasOnline->id ?>" role="tab" data-toggle="tab" class="btn btn-sm btn-default btn-vendas">
                    <?= $vendasOnline->nome ?></br>
                    <?= $vendasOnline->subcategoria ?>
                </a>

            <?php endforeach; ?>
            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-edit']), "#", ['id' => 'link-editar', 'class' => 'btn btn-sm btn-default', 'escape' => false, 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => 'Editar Categoria']) ?>
            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-plus']), '/vendasOnlines/add', ['class' => 'btn btn-sm btn-default', 'escape' => false, 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => 'Adicionar Categoria']) ?>
        </div>
    </div>
    <div class="col-md-3 fontReduzida">
        <?=
        $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-plus', 'aria-hidden' => 'true']) . ' Novo Botão', ['controller' => 'VendasOnlinesOperadoras', 'action' => 'add'], ['id' => 'add-buttton', 'class' => 'btn btn-sm btn-primary', 'role' => 'button', 'escape' => false]);
        ?>
    </div>
    <div class="tab-content">
        <?php foreach ($vendasOnlines as $vendasOnline) : ?>
            <div role="tabpanel" class="tab-pane" id="tab-<?= $vendasOnline->id ?>">
                <div class="col-xs-12 cards group-operadoras">
                    <?php foreach ($vendasOnline->vendas_onlines_operadoras as $operadora) : ?>
                        <div class="before-card text-center">
                            <form id="form-<?= $operadora->id ?>" method="POST" action="<?= $this->request->webroot ?>vendaonline" target="_blank">
                                <?= $this->Form->input('destino', ['type' => 'hidden', 'value' => $operadora->link]) ?>
                            </form>
                            <div class="card">
                                <div class="card-header">
                                    <?= $this->Html->image('vendas_online/' . $operadora->imagem) ?>
                                    <div class="actions">
                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'far fa-copy']) . ' Duplicar', "/vendasOnlinesOperadoras/duplicar/".$operadora->id, ['class' => 'btn btn-sm btn-default', 'escape' => false, 'sid' => 'vendasOnlines.duplicar']) ?>
                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-edit']) . ' Editar', "/vendasOnlinesOperadoras/edit/".$operadora->id, ['class' => 'btn btn-sm btn-default', 'escape' => false, 'sid' => 'vendasOnlines.edit']) ?>
                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-eye']) . ' Vizualizar', '#', ['id' => $operadora->id, 'class' => 'btn btn-sm btn-default teste', 'redirect' => $operadora->redirect, 'escape' => false, 'sid' => 'vendasOnlines.view']) ?>
                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-trash']) . ' Excluir', "/vendasOnlinesOperadoras/delete/". $operadora->id, ['class' => 'btn btn-sm btn-danger', 'escape' => false, 'sid' => 'vendasOnlines.delete']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <span><?= $operadora->linha1 ?></span>
                                <br />
                                <span><?= $operadora->linha2 ?></span>
                            </div>
                            <?= $this->Form->textarea(null, ['id' => "aviso-$operadora->id", "value" => $operadora->aviso, 'class' => 'hidden']) ?>
                            <?php if (!empty($operadora->link_video)) : ?>
                                <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'fas fa-play']) . ' Assistir  Vídeo', ['class' => 'btn btn-sm btn-default btnModalVideos', 'urlVideo' => $operadora->link_video]) ?>
                            <?php endif; ?>
                            <?php if (!empty($operadora->manual)) : ?>
                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'far fa-bookmark']) . ' Ler Manual', $this->request->webroot . 'uploads/venda_online/manuais/' . $operadora->manual, ['class' => 'btn btn-sm btn-default btnManuais', 'target' => '_blank', 'escape' => false]) ?>
                            <?php endif; ?>
                            <?php if (!empty($operadora->material_vendas)) : ?>
                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'far fa-folder']) . ' Material de vendas',  $operadora->material_vendas, ['class' => 'btn btn-sm btn-default btnMaterial', 'target' => '_blank', 'escape' => false]) ?>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?= $this->element('modal_avisos') ?>

<!-- Modal VIDEOS -->
<div class="modal fade" id="ModalVideos" tabindex="-1" role="dialog" aria-labelledby="ModalVideosLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center" style="border: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" style="display: flex; justify-content: center;">
                <iframe id="iframe-videos" frameborder='0' width="560" height="315" src="" allow="autoplay; encrypted-media" allowfullscreen>
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
?>
<script>
    var vendaOnline = null;
    $(".btn-vendas").click(function() {
        var link = $(this);
        $(".btn-vendas").removeClass("btn-vendas-active");
        link.addClass("btn-vendas-active");

        let id = link.attr('aria-controls');
        $("#link-editar").attr("href", baseUrl + "vendasOnlines/edit/" + id);

        $("#add-buttton").attr('href', baseUrl + "vendasOnlinesOperadoras/add/" + id);

        if (id)
            $("#add-buttton").show();
    });

    $(".teste").click(function() {
        let id = $(this).attr('id');
        let texto = $("#aviso-" + id).val();
        let redirect = $(this).attr("redirect");
        let destino = $("#form-" + id).children("#destino").val();
        if (texto !== '') {
            $("#conteudoAviso").empty();
            $("#conteudoAviso").append(texto);
            $("#modalAviso").modal('show');
            $("#continuar").attr("form", id);
            $("#solicitar").show();
            if (redirect == 1) {
                $("#continuar").attr("redirect", 1);
            } else {
                $("#continuar").removeAttr("redirect");
            }
        } else {
            if (redirect == 1) {
                window.open(destino, '_blank');
            } else {
                $("#form-" + id).submit();
            }
        }
    });

    $("#continuar").click(function() {
        let id = $(this).attr("form");
        $("#modalAviso").modal('hide');
        let redirect = $(this).attr("redirect");
        let destino = $("#form-" + id).children("#destino").val();
        if (redirect == 1) {
            window.open(destino, '_blank');
        } else {
            $("#form-" + id).submit();
        }
    });

    $("#solicitar").click(function() {
        $("#conteudoAviso").empty()
        $("#conteudoAviso").append("<p class='centralizada'>Obrigado, em breve nossa equipe entrará em contato com você.<p/>")
        $("#solicitar").hide();
        $.post(baseUrl + 'avaliacaos/add', {
            user_id: "<?= $sessao['id'] ?>",
            nota: 99,
            tipo: 'Solicitação de Atendimento'
        })
    });

    $(".btnModalVideos").click(function() {
        let url = $(this).attr('urlVideo')
        $("#iframe-videos").attr("src", "");
        $("#iframe-videos").attr("src", url);
        $("#ModalVideos").modal("show");
    });
</script>