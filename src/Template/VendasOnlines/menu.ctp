<?php foreach ($vendasOnlines as $vendaOnline) : ?>
    <li>
        <a href="<?= $vendaOnline->exibir_vendas_online == 1 ? $this->request->webroot . "users/lojas/" . $vendaOnline->id : "#" ?>" class="<?= $vendaOnline->exibir_vendas_online == 1 ? false : "alerta" ?>">
            <i class="<?= $vendaOnline->icone ?>"></i> <?= $vendaOnline->menu_linha1 ?> <?= !empty($vendaOnline->menu_linha2) ? "<br><small style='margin-left: 20px'>$vendaOnline->menu_linha2</small>" : false ?>
        </a>
    </li>
<?php endforeach; ?>
<script>
    $(".alerta").click(function() {
        $("#modal-alerta").modal("show");
    })
</script>
