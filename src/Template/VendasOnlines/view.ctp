<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Vendas Online'), ['action' => 'edit', $vendasOnline->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Vendas Online'), ['action' => 'delete', $vendasOnline->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vendasOnline->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Vendas Onlines'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vendas Online'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="vendasOnlines view large-9 medium-8 columns content">
    <h3><?= h($vendasOnline->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($vendasOnline->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($vendasOnline->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Ordem') ?></th>
            <td><?= $this->Number->format($vendasOnline->ordem) ?></td>
        </tr>
    </table>
</div>
