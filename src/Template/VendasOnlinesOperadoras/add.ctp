<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Vendas Onlines Operadoras'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="vendasOnlinesOperadoras form large-9 medium-8 columns content">
    <?= $this->Form->create($vendasOnlinesOperadora) ?>
    <fieldset>
        <legend><?= __('Add Vendas Onlines Operadora') ?></legend>
        <?php
        echo $this->Form->input('nome');
        echo $this->Form->input('imagem');
        echo $this->Form->input('linha1');
        echo $this->Form->input('linha2');
        echo $this->Form->input('tooltip');
        echo $this->Form->input('prioridade');
        echo $this->Form->input('link_video');
        echo $this->Form->input('id_venda_online');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
