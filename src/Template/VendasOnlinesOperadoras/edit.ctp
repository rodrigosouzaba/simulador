<?php
$action = $this->request->action;
?>
<style>
    .heading-color {
        background-color: #003656 !important;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 10px 10px 5px 10px;
    }

    .heading-color .panel-title {
        color: #FFF !important;
    }

    .bloco-add {
        border-right: 1px solid #f5f5f5
    }

    .cards {
        display: flex;
        flex-wrap: wrap;
    }

    .card {
        width: 40%;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 5px;
        margin: 1%;
        cursor: pointer;
    }

    .card-image {
        /* border: 1px solid #ccc; */
        border-radius: 5px;
        text-align: center;
    }

    .card-text {
        text-align: center;
        font-size: 8pt;
        padding-top: 10px;
    }

    .padding-5 {
        padding-left: 5px;
        padding-right: 5px;
    }

    .user-link .cpf-listado,
    .user-link .personalizado-listado {
        background-color: transparent !important;
        border: 0;
        box-shadow: none;
        margin: 0;
    }

    #referencia {
        display: none;
    }

    .input-row {
        display: flex;
        align-items: center;
        flex-direction: row;
        width: 100%;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .input-row input {
        margin: 0;
    }
</style>
<div class="modulo">
    <?= h($vop->vendas_online->nome) . ' - ' . h($vop->vendas_online->subcategoria) ?>
</div>
<div class="col-md-12">
    <?= $this->Form->create($vop, ['id' => 'form-vop', 'enctype' => 'multipart/form-data']) ?>
    <?= $this->Form->input('id_venda_online', ['label' => false, 'empty' => 'Permissão', 'value' => $vop->vendas_online->id, 'type' => 'hidden']) ?>
    <div class="bloco-add col-md-6">
        <div class="col-md-6">
            <div style="display: flex; margin-top: 10px; align-items: baseline;">
                <?= $this->Form->checkbox('redirect', ['label' => false, 'id' => 'redirect', 'value' => 1]) ?>
                <label for="redirect">É redirecionamento?</label>
            </div>
        </div>
        <div class="col-md-6" style="display: flex; margin-top: 10px; align-items: baseline;">
            <?= $this->Form->checkbox('status', ['label' => false, 'id' => 'status']) ?>
            <label for="status">Ocultar Botão</label>
        </div>
        <div class="col-xs-12">
            <?= $this->Form->input('link', ['label' => false, 'value' =>  'https://corretorparceiro.com.br/app/venda-online/' . $vop->id, 'readonly']) ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('link', ['label' => false, 'placeholder' => 'Link Geral']) ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('tooltip', ['label' => false, 'placeholder' => 'Dica']) ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('linha1', ['label' => false, 'placeholder' => 'Linha1']) ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('linha2', ['label' => false, 'placeholder' => 'Linha2']) ?>
        </div>
        <!-- <div class="clearfix">&nbsp</div> -->
        <div class="col-md-6">
            <?= $this->Form->input('prioridade', ['type' => 'text', 'label' => false, 'placeholder' => 'Prioridade']) ?>
        </div>
        <div class="col-md-6">
            <?= $this->Form->input('link_video', ['label' => false, 'placeholder' => 'Link Vídeo']) ?>
        </div>
        <!-- <div class="clearfix">&nbsp</div> -->
        <div class="col-md-12">
            <?= $this->Form->input('material_vendas', ['label' => false, 'type' => 'text', 'placeholder' => 'Link Material Vendas']) ?>
        </div>
        <div class="input-row" style="padding:0; margin-bottom: 15px;">
            <div class="col-md-6">
                <?= $this->element('file_input', [
                    'id' => 'imagem',
                    'name' => 'imagem',
                    'placeholder' => 'Imagem do Botão (PNG)',
                    'value' => !empty($vop->imagem) ? $vop->imagem : '',
                    'accept' => '.png'
                ]) ?>
            </div>
            <div class="col-md-5">
                <?= $this->element('file_input', [
                    'id' => 'manual',
                    'name' => 'manual',
                    // 'label' => 'Inserir Manual (PDF)',
                    'placeholder' => 'Inserir Manual (PDF)',
                    'value' => !empty($vop->manual) ? $vop->manual : '',
                    'accept' => 'application/pdf'
                ]) ?>
            </div>
            <div class="col-md-1 padding-5">
                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-trash']), '#', ['id' => 'del-manual', 'class' => 'btn btn-sm btn-danger', 'escape' => false, !empty($vop->manual) ? '' : 'disabled']) ?>
            </div>
        </div>
        <div class="col-md-12">
            <?= $this->Form->input('aviso', ['label' => false, 'placeholder' => 'Aviso', 'class' => 'editor-especial']) ?>
        </div>
        <div class="clearfix">&nbsp</div>
        <?= $this->Form->button('Salvar', ['id' => 'salvar', 'class' => 'btn btn-primary col-md-4 col-md-offset-4']) ?>
    </div>
    <div class="col-md-6">
        <div class="clearfix">&nbsp</div>
        <div class="col-md-4 padding-5">
            <?= $this->Form->input('estado_id', ['label' => false, 'empty' => 'UF', 'options' => $estados]) ?>
        </div>
        <div class="col-md-4 padding-5">
            <?= $this->Form->input('ramo', ['label' => false, 'empty' => 'Ramo', 'options' => $ramos]) ?>
        </div>
        <div class="col-md-4 padding-5" id="operadora-field">
            <?= $this->Form->input('operadora_id', ['label' => false, 'empty' => 'Operadora', !empty($operadoras) ? '' : 'disabled']) ?>
        </div>
        <div class="clearfix">&nbsp</div>

        <div class="col-md-12">
            <select name="estados[_ids][]" multiple='multiple'>
                <?php foreach ($estados as $key => $estado) : ?>
                    <option value="<?= $key ?>" <?= isset($estados_selecionados) ? (in_array($estado, $estados_selecionados->toArray()) ? "selected" : "") : "" ?>><?= $estado ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="clearfix">&nbsp</div>
        <div class="input-row">
            <div class="col-md-5 padding-5">
                <?= $this->Form->input('find_cpf', ['label' => false, 'placeholder' => 'CPF', $action === 'add' ? 'disabled' : '']) ?>
            </div>
            <div class="col-md-6 padding-5">
                <?= $this->Form->input('add_link_personalizado', ['label' => false, 'placeholder' => 'Link Personalizado', $action === 'add' ? 'disabled' : '']) ?>
            </div>
            <div class="col-md-1 padding-5">
                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-plus']), '#', ['id' => 'add-link', 'class' => 'btn btn-sm btn-default', 'escape' => false, 'disabled']) ?>
            </div>
        </div>
        <div class="col-md-12">
            <div id="user" class="small hidden">
                <a href="#" user-id="" id="user-link" class="link-user"></a>
            </div>
            <span id="error-cpf" class="text-danger small hidden">
                Esse CPF já possui link personalizado
            </span>
        </div>
        <div style="height: 300px; width: 100%; overflow-y: scroll;">
            <table class="list-users-links">
                <thead>
                    <tr>
                        <th style="width: 30px;"></th>
                        <th>Usuário</th>
                        <th>Link Personalizado</th>
                    </tr>
                </thead>
                <tbody class="user-link">
                    <?php if (!empty($vop->links_personalizados)) : ?>
                        <?php foreach ($vop->links_personalizados as $links) : ?>
                            <tr id='linha-link-<?= $links['id'] ?>'>
                                <input type="hidden" name="ids[]" value="<?= $links['id'] ?>" />
                                <td>
                                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fas fa-trash']), '#', ['class' => 'btn btn-xs btn-danger del-link', 'escape' => false, 'title' => 'Remova este link personalizado', 'link-id' => $links['id']]) ?>
                                </td>
                                <td>
                                    <a href="#" class="link-user" user-username="<?= $links['cpf'] ?>"><?= $links['cpf'] ?></a>
                                    <input class="hidden" class="cpf-listado" name="cpfs[]" value="<?= $links['cpf'] ?>" user-username="<?= $links['cpf'] ?>" readonly />
                                </td>
                                <td>
                                    <input class="personalizado-listado" name="links_personalizados[]" value="<?= $links['link'] ?>" readonly />
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?= $this->Form->end() ?>
    <div class="col-md-12">
        <div id="referencia">
            <div id="refer-cpf">
                <a href="#" class="link-user"></a>
                <input class="cpf-listado" name="cpfs[]" class="hidden" readonly />
            </div>
            <div id="refer-link">
                <input class="personalizado-listado" name="links_personalizados[]" readonly />
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-user" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content row">
            <div class="modal-header" style="border: 0">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title centralizada" id="modal-title">Dados do Usuário</h4>
            </div>
            <div class="modal-body col-xs-12" id="data-user">

            </div>
            <div class="modal-footer" style="border: 0">
            </div>
        </div>

    </div>
</div>

<script>
    $(".link-user").click(function() {
        var id = $(this).attr(' user-id');
        var username = $(this).attr('user-username');
        link = ""
        if (id === "" || id === "undefined" || id === undefined) {
            $.get(baseUrl + 'avaliacaos/getUser/' + username, function(data) {
                $("#data-user").empty();
                $("#data-user").append(data);
                $("#modal-user").modal("show");
            });
        } else {
            $.get(baseUrl + 'avaliacaos/getUser/' + id, function(data) {
                $("#data-user").empty();
                $("#data-user").append(data);
                $("#modal-user").modal("show");
            });
        }
    });
    $("#ramo").change(() => {
        $.get(baseUrl + 'vendasOnlinesOperadoras/getOperadoras/' + $("#estado-id").val() + '/' +
            $("#ramo").val(), (data) => {
                $("#operadora-field").empty()
                $("#operadora-field").append(data)
            })
    });
    $("#add-link").click(e => {
        e.preventDefault();
        incluirLink()
    });

    $("#find-cpf").blur(() => {
        let username = $("#find-cpf").val();
        $.get(baseUrl + 'vendasOnlinesOperadoras/existUser/' + username, (data) => {
            if (data[0]) {
                $("#user").removeClass('hidden');
                $("#add-link").removeAttr('disabled');
                $("#user-link").attr('user-id', data[1]);
                $("#user-link").text(data[2]);
            } else {
                $("#user").addClass('hidden');
            }
        })
    })

    function incluirLink() {
        var novo_cpf = $("#find-cpf").val()
        var novo_link = $("#add-link-personalizado").val()
        var venda_online_operadora = <?= $vop->id ?>

        if (validaCpf(novo_cpf)) {
            let refer_cpf = getRefer(novo_cpf, "#refer-cpf");
            let refer_link = getRefer(novo_link, "#refer-link");
            saveLink(novo_cpf, novo_link, venda_online_operadora, (id) => {
                addLine(id, refer_cpf, refer_link);
            });
            refresh();
        }
    }

    function validaCpf(novoCpf) {
        var values = [];
        let cpfs = $("input[name='cpfs[]']").each(function() {
            values.push($(this).val());
        });
        if (values.indexOf(novoCpf) !== -1) {
            $("#error-cpf").removeClass('hidden');
            return false;
        } else {
            $("#error-cpf").addClass('hidden');
            return true;
        }
    }

    function getRefer(valOrigem, destino) {
        let referencia = $(destino);
        if (destino === "#refer-cpf") {
            referencia.children("a").text(valOrigem);
            referencia.children("a").attr("user-username", valOrigem);
            referencia.children("input").attr("hidden", true);
        } else {
            referencia.children("input").attr("value", valOrigem);
        }
        return referencia.html();
    }

    function addLine(id, refer_cpf, refer_link) {
        $(".user-link").append(" <tr id='linha-link-" + id + "'><td><a class='btn btn-xs btn-danger del-link' link-id='" + id + "'><i class='fas fa-trash'></i></a></td><td> " + refer_cpf + "</td><td>" + refer_link + "</td></tr> ");
    }

    function refresh() {
        $("#find-cpf").val('');
        $("#add-link-personalizado").val('');
        $("#find-cpf").focus();
    }

    function saveLink(cpf, link, vop, callback) {
        var id;
        $.post(baseUrl + 'vendasOnlinesOperadoras/addLink', {
                'cpf': cpf,
                'link': link,
                'vendas_onlines_operadora_id': vop,
            },
            (data) => {
                console.log(data)
                callback(data);
            })
    }

    $("#add-link-personalizado").keypress(function(e) {
        if (e.charCode == 13) {
            e.preventDefault();
            incluirLink();
        }
    })
    $("#find-cpf").mask("999.999.999-99");

    $("#del-manual").click(function() {
        $.get(baseUrl + 'vendasOnlinesOperadoras/del-manual/<?= $vop->id ?>', () => {
            $("#manual").val('');
        })
    });

    $(".del-link").click(function() {
        $.post(baseUrl + 'linksPersonalizados/delete/' + $(this).attr('link-id'), (data) => {
            $("#linha-link-" + $(this).attr('link-id')).remove()
        })
    });

    $(".remove-line").click(function() {
        $(this).parent('tr').remove();
    });

    $(document).ready(function() {

        $('#estados-ids').children().prop("selected", true);
        $('#salvar').click(function() {
            $('#estados-ids').children().prop("selected", true);
        });


        $('.add').click(function() {
            $('#lista-estados option:selected').appendTo('#estados-ids');
        });
        $('.remove').click(function() {
            $('#estados-ids option:selected').appendTo('#lista-estados');
        });
        $('.add-all').click(function() {
            $('#lista-estados option').appendTo('#estados-ids');
        });
        $('.remove-all').click(function() {
            $('#estados-ids option').appendTo('#lista-estados');
        });
    });
</script>