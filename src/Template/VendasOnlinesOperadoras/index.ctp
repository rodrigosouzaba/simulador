<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Vendas Onlines Operadora'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="vendasOnlinesOperadoras index large-9 medium-8 columns content">
    <h3><?= __('Vendas Onlines Operadoras') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('imagem') ?></th>
                <th><?= $this->Paginator->sort('texto') ?></th>
                <th><?= $this->Paginator->sort('tooltip') ?></th>
                <th><?= $this->Paginator->sort('prioridade') ?></th>
                <th><?= $this->Paginator->sort('link_video') ?></th>
                <th><?= $this->Paginator->sort('id_venda_online') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($vendasOnlinesOperadoras as $vendasOnlinesOperadora): ?>
            <tr>
                <td><?= $this->Number->format($vendasOnlinesOperadora->id) ?></td>
                <td><?= h($vendasOnlinesOperadora->nome) ?></td>
                <td><?= h($vendasOnlinesOperadora->imagem) ?></td>
                <td><?= h($vendasOnlinesOperadora->texto) ?></td>
                <td><?= h($vendasOnlinesOperadora->tooltip) ?></td>
                <td><?= $this->Number->format($vendasOnlinesOperadora->prioridade) ?></td>
                <td><?= h($vendasOnlinesOperadora->link_video) ?></td>
                <td><?= $this->Number->format($vendasOnlinesOperadora->id_venda_online) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $vendasOnlinesOperadora->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $vendasOnlinesOperadora->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $vendasOnlinesOperadora->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vendasOnlinesOperadora->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
