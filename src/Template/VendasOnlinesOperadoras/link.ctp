<?php
$session = $this->request->session();
$sessao = $session->read('Auth.User');
if (!empty($sessao)) {
    echo $this->element('menu');
}
?>
<style>
    body {
        margin: 0;
        padding: 0;
    }

    iframe {
        display: none;
        width: 100vw;
        height: calc(100vh - 50px);
        border: 0;
    }

    #solicitar {
        margin-right: 20px;
    }

    @media(max-width: 766px) {
        #solicitar {
            margin-right: 0px;
        }
    }
</style>
<!-- Modal -->
<div class="modal fade" id="modalAviso" tabindex="-1" role="dialog" aria-labelledby="modalAvisoLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 1; position: absolute; top: 15px; right: 15px;"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="conteudoAviso">
                <?= isset($texto) ? $texto : 'Sem texto' ?>
            </div>
            <div class="modal-footer" style="border: 0;">
                <div class="col-md-8 col-md-offset-2 centralizada">
                    <?= $this->Form->button('Solicitar Contato', ['id' => 'solicitar', 'class' => 'btn btn-danger']) ?>
                    <?= $this->Form->button('Continuar', ['class' => 'btn btn-primary', 'style' => 'width: 133px;', 'data-dismiss' => "modal", 'aria-label' => "Close"]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->element('modal-login') ?>
<?php if (!empty($sessao)) : ?>
    <iframe id="iframe" src="" class="iframe" style="margin-top: 50px;"></iframe>
<?php else : ?>
    <iframe id="iframe" src="" class="iframe"></iframe>
<?php endif; ?>
<script>
    $(document).ready(() => {
        $("#modalAviso").modal('show');
        $("#conteudoAviso").empty()
        $("#conteudoAviso").append('<?= $vop->aviso ?>')
    })
    $("#solicitar").click(function() {
        if ('<?= $sessao['id'] ?>' !== '') {
            $("#conteudoAviso").empty()
            $("#conteudoAviso").append("<p class='centralizada'>Obrigado, em breve nossa equipe entrará em contato com você.<p/>")
            $("#solicitar").hide();
            $.post(baseUrl + 'avaliacaos/add', {
                user_id: "<?= $sessao['id'] ?>",
                nota: 99,
                tipo: 'ATENDIMENTO VENDA ONLINE'
            })
        }
        $("#modalAviso").modal('hide')
        $("#modal-login").modal('show')
    })

    $('#modalAviso').on('hidden.bs.modal', function(e) {
        if ('<?= $vop->redirect ?>' == '1') {
            window.location.href = '<?= $vop->link ?>';
        } else {
            $("#iframe").attr("src", '<?= $vop->link ?>');
            $("#modalAviso").modal("hide");
        }
        $("#iframe").show();
    })
</script>
