<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Vendas Onlines Operadora'), ['action' => 'edit', $vendasOnlinesOperadora->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Vendas Onlines Operadora'), ['action' => 'delete', $vendasOnlinesOperadora->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vendasOnlinesOperadora->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Vendas Onlines Operadoras'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vendas Onlines Operadora'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="vendasOnlinesOperadoras view large-9 medium-8 columns content">
    <h3><?= h($vendasOnlinesOperadora->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($vendasOnlinesOperadora->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Imagem') ?></th>
            <td><?= h($vendasOnlinesOperadora->imagem) ?></td>
        </tr>
        <tr>
            <th><?= __('Texto') ?></th>
            <td><?= h($vendasOnlinesOperadora->texto) ?></td>
        </tr>
        <tr>
            <th><?= __('Tooltip') ?></th>
            <td><?= h($vendasOnlinesOperadora->tooltip) ?></td>
        </tr>
        <tr>
            <th><?= __('Link Video') ?></th>
            <td><?= h($vendasOnlinesOperadora->link_video) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($vendasOnlinesOperadora->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prioridade') ?></th>
            <td><?= $this->Number->format($vendasOnlinesOperadora->prioridade) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Venda Online') ?></th>
            <td><?= $this->Number->format($vendasOnlinesOperadora->id_venda_online) ?></td>
        </tr>
    </table>
</div>
