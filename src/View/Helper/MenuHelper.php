<?php

namespace App\View\Helper;

use App\Model\Entity\MidiasCompartilhada;
use App\Model\Entity\VendasOnline;
use Cake\View\Helper;

class MenuHelper extends Helper
{

    public function makeMenu($vendasOnline, $estadosSaude = [], $estadoOdonto = [], $midiasCompartilhadas = [])
    {

        $dados['linha'][0][0]['image'] = WEB_ROOT . '/app/img/menu/casa.png';
        $dados['linha'][0][0]['texto'] = 'Inicio';
        $dados['linha'][0][0]['link'] = WEB_ROOT;


        $dados['linha'][0][1]['image'] = WEB_ROOT . '/app/img/menu/estetoscopio.png';
        $dados['linha'][0][1]['texto'] = 'Planos de Saúde';
        $dados['linha'][0][1]['link'] = "#";
        $dados['linha'][0][1]['toggle'] = 'dropdown';
        $dados['linha'][0][1]['submenu'] = $estadosSaude;

        $dados['linha'][0][2]['image'] = WEB_ROOT . '/app/img/menu/dente.png';
        $dados['linha'][0][2]['texto'] = 'Planos Odontológicos';
        $dados['linha'][0][2]['link'] = "#";
        $dados['linha'][0][2]['toggle'] = 'dropdown';
        $dados['linha'][0][2]['submenu'] = $estadoOdonto;

        $dados['linha'][0][3]['texto'] = "Seguros";
        $dados['linha'][0][3]['image'] = WEB_ROOT . "app/img/menu/guardachuva.png";
        $dados['linha'][0][3]['link'] = WWW_ROOT . "/index.php/pagina-em-atualizacao";

        $dados['linha'][0][4]['texto'] = "Consórcios";
        $dados['linha'][0][4]['image'] = WEB_ROOT . "app/img/menu/chave.png";
        $dados['linha'][0][4]['link'] = WWW_ROOT . "/index.php/pagina-em-atualizacao";

        $dados['linha'][0][5]['texto'] = "Empréstimos";
        $dados['linha'][0][5]['image'] = WEB_ROOT . "app/img/menu/cifrao.png";
        $dados['linha'][0][5]['link'] = WWW_ROOT . "/index.php/pagina-em-atualizacao";

        $dados['linha'][0][6]['texto'] = "Benefícios";
        $dados['linha'][0][6]['image'] = WEB_ROOT . "app/img/menu/talheres.png";
        $dados['linha'][0][6]['link'] = WWW_ROOT . "/index.php/pagina-em-atualizacao";


        // SEGUNDA LINHA DO MENU
        if (!empty($dadosUser)) {
            $dados['linha'][1][0]['image'] = WEB_ROOT . "/app/img/menu/gerenciar.png";
            $dados['linha'][1][0]['texto'] = 'Ferramentas de Vendas';
            $dados['linha'][1][0]['link'] =  "/app/users/central";

            $dados['linha'][1][1]['image'] = WEB_ROOT . '/app/img/menu/funil.png';
            $dados['linha'][1][1]['texto'] = 'Leads';
            $dados['linha'][1][1]['link'] = "#";
            $dados['linha'][1][1]['id'] = "link-leads";
        } else {
            $dados['linha'][1][0]['image'] = WEB_ROOT . "/app/img/menu/gerenciar.png";
            $dados['linha'][1][0]['texto'] = 'Ferramentas de Vendas';
            $dados['linha'][1][0]['link'] = '#modal-login';
            $dados['linha'][1][0]['toggle'] = "modal";

            $dados['linha'][1][1]['image'] = WEB_ROOT . '/app/img/menu/funil.png';
            $dados['linha'][1][1]['texto'] = 'Leads';
            $dados['linha'][1][1]['link'] = "#modal-login";
            $dados['linha'][1][1]['toggle'] = 'modal';
            $dados['linha'][1][1]['target'] = '#modal-login';
        }

        $dados['linha'][1][1]['image'] = WEB_ROOT . '/app/img/menu/funil.png';
        $dados['linha'][1][1]['texto'] = 'Leads';
        $dados['linha'][1][1]['link'] = WEB_ROOT . 'index.php/pagina-em-atualizacao/';
        $dados['linha'][1][1]['toggle'] = 'modal';
        $dados['linha'][1][1]['target'] = '#modal-alerta';

        $dados['linha'][1][2]['image'] = WEB_ROOT . '/app/img/menu/card.png';
        $dados['linha'][1][2]['texto'] = 'Cotações';
        $dados['linha'][1][2]['link'] = '#';
        $dados['linha'][1][2]['toggle'] = 'dropdown';
        #
        $dados['linha'][1][2]['submenu'][0]['image'] = WEB_ROOT . '/app/img/menu/handshake.png';
        $dados['linha'][1][2]['submenu'][0]['link'] = '/app/users/calculos';
        $dados['linha'][1][2]['submenu'][0]['texto'] = 'Minhas Cotações';
        $dados['linha'][1][2]['submenu'][0]['requestjs'] = "AppCalculos";
        #
        $dados['linha'][1][2]['submenu'][1]['image'] = WEB_ROOT . '/app/img/menu/card.png';
        $dados['linha'][1][2]['submenu'][1]['link'] = '/app/tabelasGeradas';
        $dados['linha'][1][2]['submenu'][1]['texto'] = 'Tabelas Personalizadas';
        $dados['linha'][1][2]['submenu'][1]['small'] = '<br /><small style="margin-left: 20px">Saúde e Odonto</small>';
        #
        $dados['linha'][1][2]['submenu'][2]['image'] = WEB_ROOT . '/app/img/menu/estetoscopio.png';
        $dados['linha'][1][2]['submenu'][2]['link'] = '/app/users/saude';
        $dados['linha'][1][2]['submenu'][2]['texto'] = 'Multicálculo Saúde';
        $dados['linha'][1][2]['submenu'][2]['requestjs'] = "AppCalculos";
        #
        $dados['linha'][1][2]['submenu'][3]['image'] = WEB_ROOT . '/app/img/menu/dente.png';
        $dados['linha'][1][2]['submenu'][3]['link'] = '/app/users/odonto';
        $dados['linha'][1][2]['submenu'][3]['texto'] = 'Multicálculo Odonto';
        $dados['linha'][1][2]['submenu'][3]['requestjs'] = "AppOdontoCalculos";
        #
        $dados['linha'][1][2]['submenu'][4]['image'] = 'fas fa-users';
        $dados['linha'][1][2]['submenu'][4]['link'] = '#';
        $dados['linha'][1][2]['submenu'][4]['texto'] = 'Solicitação de Cotação';
        $dados['linha'][1][2]['submenu'][4]['small'] = '<br /><small style="margin-left: 20px">Saúde Grandes Empresas</small>';
        #
        $dados['linha'][1][2]['submenu'][4]['image'] = WEB_ROOT . '/app/img/menu/car.png';
        $dados['linha'][1][2]['submenu'][4]['link'] = 'https://villa.segfy.com:443/Publico/Segurados/Orcamentos/SolicitarCotacao?e=ej7LcHNjuCs%3D';
        $dados['linha'][1][2]['submenu'][4]['texto'] = 'Multicálculo Automóvel';
        $dados['linha'][1][2]['submenu'][4]['small'] = '';

        #
        $dados['linha'][1][2]['submenu'][5]['image'] = WEB_ROOT . '/app/img/menu/casa.png';
        $dados['linha'][1][2]['submenu'][5]['link'] = 'https://villa.segfy.com:443/Publico/Segurados/Orcamentos/SolicitarCotacaoResidencial?e=ej7LcHNjuCs%3D';
        $dados['linha'][1][2]['submenu'][5]['texto'] = 'Multicálculo Residencial';
        $dados['linha'][1][2]['submenu'][5]['small'] = '';

        $dados['linha'][1][3] = $vendasOnline;

        $dados['linha'][1][4] = $midiasCompartilhadas;

        $dados['linha'][1][5]['image'] = WEB_ROOT . '/app/img/menu/megafone.png';
        $dados['linha'][1][5]['texto'] = 'Comunicados';
        if (!empty($dadosUser)) {
            $dados['linha'][1][5]['link'] = "/comunicados";
            $dados['linha'][1][5]['class'] = "link-externo";
        } else {
            $dados['linha'][1][5]['link'] = "#modal-login";
            $dados['linha'][1][5]['toggle'] = 'modal';
        }

        $dados['linha'][1][6]['image'] = WEB_ROOT . '/app/img/menu/headset.png';
        $dados['linha'][1][6]['texto'] = 'Fale Conosco';
        $dados['linha'][1][6]['link'] = '#';
        $dados['linha'][1][6]['toggle'] = 'modal';
        if (!empty($dadosUser)) {
            $dados['linha'][1][6]['target'] = '#modalFaleConosco';
        } else {
            $dados['linha'][1][6]['target'] = '#faleConoscoVisitante';
        }
        /*
         $dados['linha'][1][7]['image'] = WEB_ROOT . '/app/img/menu/estrela.png';
         $dados['linha'][1][7]['texto'] = 'Avalie';
         $dados['linha'][1][7]['link'] = '#';
         $dados['linha'][1][7]['toggle'] = 'modal';
         $dados['linha'][1][7]['target'] = '#modalAvaliacao';
         */
        return $dados;
    }
}
