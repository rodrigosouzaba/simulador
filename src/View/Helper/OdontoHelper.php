<?php

namespace App\View\Helper;

use \Cake\ORM\TableRegistry;
use Cake\View\Helper;

class OdontoHelper extends Helper
{

    public $helpers = ['Form'];

    public function selectEstados(bool $nova, array $options)
    {
        $nome = $options['name'];
        $controller = $this->request->getParam('controller');
        if ($nova) {
            $estados =  TableRegistry::get($controller)->Estados
                ->find('list', ['valueField' => 'nome'])
                ->matching('OdontoAreasComercializacoes.OdontoOperadoras', function ($q) {
                    return $q->where([
                        'OR' => [
                            ['status <>' => 'INATIVA'],
                            ['status IS' => null]
                        ],
                        'nova' => 1
                    ]);
                });
        } else {
            $estados = TableRegistry::get($controller)->Estados
                ->find('list', ['valueField' => 'nome'])
                ->matching('OdontoOperadoras', function ($q) {
                    return $q->where([
                        'OR' => [
                            ['status <>' => 'INATIVA'],
                            ['status IS' => null]
                        ],
                        'NOT' => 'nova <=> 1'
                    ]);
                });
        }
        $options['options'] = $estados;
        return $this->Form->control($nome, $options);
    }
}
