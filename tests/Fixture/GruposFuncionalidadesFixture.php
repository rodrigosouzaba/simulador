<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GruposFuncionalidadesFixture
 *
 */
class GruposFuncionalidadesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'grupo_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'funcionalidade_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'FK_grupos_funcionalidades_grupos' => ['type' => 'index', 'columns' => ['grupo_id'], 'length' => []],
            'FK_grupos_funcionalidades_funcionalidades' => ['type' => 'index', 'columns' => ['funcionalidade_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_grupos_funcionalidades_funcionalidades' => ['type' => 'foreign', 'columns' => ['funcionalidade_id'], 'references' => ['funcionalidades', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'FK_grupos_funcionalidades_grupos' => ['type' => 'foreign', 'columns' => ['grupo_id'], 'references' => ['grupos', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'grupo_id' => 1,
            'funcionalidade_id' => 1
        ],
    ];
}
