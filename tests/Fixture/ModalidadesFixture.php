<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ModalidadesFixture
 *
 */
class ModalidadesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'nome' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'descricao' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'carencia_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'informacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'observacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'opcional_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'rede_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'reembolso_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'carencia_id' => ['type' => 'index', 'columns' => ['carencia_id'], 'length' => []],
            'informacao_id' => ['type' => 'index', 'columns' => ['informacao_id'], 'length' => []],
            'observacao_id' => ['type' => 'index', 'columns' => ['observacao_id'], 'length' => []],
            'opcional_id' => ['type' => 'index', 'columns' => ['opcional_id'], 'length' => []],
            'rede_id' => ['type' => 'index', 'columns' => ['rede_id'], 'length' => []],
            'reembolso_id' => ['type' => 'index', 'columns' => ['reembolso_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'modalidades_ibfk_6' => ['type' => 'foreign', 'columns' => ['reembolso_id'], 'references' => ['reembolsos', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'modalidades_ibfk_1' => ['type' => 'foreign', 'columns' => ['carencia_id'], 'references' => ['carencias', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'modalidades_ibfk_2' => ['type' => 'foreign', 'columns' => ['informacao_id'], 'references' => ['informacoes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'modalidades_ibfk_3' => ['type' => 'foreign', 'columns' => ['observacao_id'], 'references' => ['observacoes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'modalidades_ibfk_4' => ['type' => 'foreign', 'columns' => ['opcional_id'], 'references' => ['opcionais', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'modalidades_ibfk_5' => ['type' => 'foreign', 'columns' => ['rede_id'], 'references' => ['redes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'nome' => 'Lorem ipsum dolor sit amet',
            'descricao' => 'Lorem ipsum dolor sit amet',
            'carencia_id' => 1,
            'informacao_id' => 1,
            'observacao_id' => 1,
            'opcional_id' => 1,
            'rede_id' => 1,
            'reembolso_id' => 1
        ],
    ];
}
