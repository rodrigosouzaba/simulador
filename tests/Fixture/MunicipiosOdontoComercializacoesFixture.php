<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MunicipiosOdontoComercializacoesFixture
 *
 */
class MunicipiosOdontoComercializacoesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'municipio_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'odonto_comercializacao_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'FK_municipios_odonto_comercializacoes_municipios' => ['type' => 'index', 'columns' => ['municipio_id'], 'length' => []],
            'FK_municipios_odonto_comercializacoes_odonto_comercializacoes' => ['type' => 'index', 'columns' => ['odonto_comercializacao_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_municipios_odonto_comercializacoes_municipios' => ['type' => 'foreign', 'columns' => ['municipio_id'], 'references' => ['municipios', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_municipios_odonto_comercializacoes_odonto_comercializacoes' => ['type' => 'foreign', 'columns' => ['odonto_comercializacao_id'], 'references' => ['odonto_comercializacoes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'municipio_id' => 1,
            'odonto_comercializacao_id' => 1
        ],
    ];
}
