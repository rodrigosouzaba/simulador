<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MunicipiosPfComercializacoesFixture
 *
 */
class MunicipiosPfComercializacoesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'pf_comercializacao_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'municipio_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'FK_pf_comercializacoes_municipios_pf_comercializacoes' => ['type' => 'index', 'columns' => ['pf_comercializacao_id'], 'length' => []],
            'FK_pf_comercializacoes_municipios_municipios' => ['type' => 'index', 'columns' => ['municipio_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_pf_comercializacoes_municipios_municipios' => ['type' => 'foreign', 'columns' => ['municipio_id'], 'references' => ['municipios', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'FK_pf_comercializacoes_municipios_pf_comercializacoes' => ['type' => 'foreign', 'columns' => ['pf_comercializacao_id'], 'references' => ['pf_comercializacoes', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'pf_comercializacao_id' => 1,
            'municipio_id' => 1
        ],
    ];
}
