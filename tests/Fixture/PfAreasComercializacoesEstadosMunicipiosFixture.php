<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PfAreasComercializacoesEstadosMunicipiosFixture
 */
class PfAreasComercializacoesEstadosMunicipiosFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'estado_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pf_comercializacao_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'municipio_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_pf_comercializacoes_estados_municipios_pf_comercializacoes' => ['type' => 'index', 'columns' => ['pf_comercializacao_id'], 'length' => []],
            'fk_pf_comercializacoes_municipios' => ['type' => 'index', 'columns' => ['municipio_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'pf_areas_comercializacoes_estados_municipios_UN' => ['type' => 'unique', 'columns' => ['estado_id', 'pf_comercializacao_id', 'municipio_id'], 'length' => []],
            'fk_pf_comercializacoes_estados' => ['type' => 'foreign', 'columns' => ['estado_id'], 'references' => ['estados', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_pf_comercializacoes_municipios' => ['type' => 'foreign', 'columns' => ['municipio_id'], 'references' => ['municipios', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'estado_id' => 1,
                'pf_comercializacao_id' => 1,
                'municipio_id' => 1,
            ],
        ];
        parent::init();
    }
}
