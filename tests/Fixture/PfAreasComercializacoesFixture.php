<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PfAreasComercializacoesFixture
 */
class PfAreasComercializacoesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'descricao' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nome' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'pf_operadora_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pf_atendimento_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_pf_areas_comercializacoes_operadoras' => ['type' => 'index', 'columns' => ['pf_operadora_id'], 'length' => []],
            'fk_pf_areas_comercializacoes_abrangencias' => ['type' => 'index', 'columns' => ['pf_atendimento_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_pf_areas_comercializacoes_operadoras' => ['type' => 'foreign', 'columns' => ['pf_operadora_id'], 'references' => ['pf_operadoras', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_pf_areas_comercializacoes_pf_atendimentos' => ['type' => 'foreign', 'columns' => ['pf_atendimento_id'], 'references' => ['pf_atendimentos', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'descricao' => 'Lorem ipsum dolor sit amet',
                'nome' => 'Lorem ipsum dolor sit amet',
                'pf_operadora_id' => 1,
                'pf_atendimento_id' => 1,
            ],
        ];
        parent::init();
    }
}
