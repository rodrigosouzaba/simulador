<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PfComercializacoesMunicipiosFixture
 *
 */
class PfComercializacoesMunicipiosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'pf_tabela_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'municipio_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'municipios_pf_comercializacoes_municipios' => ['type' => 'index', 'columns' => ['municipio_id'], 'length' => []],
            'pf_tabelas_pf_comercializacoes_municipios' => ['type' => 'index', 'columns' => ['pf_tabela_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'municipios_pf_comercializacoes_municipios' => ['type' => 'foreign', 'columns' => ['municipio_id'], 'references' => ['municipios', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'pf_tabelas_pf_comercializacoes_municipios' => ['type' => 'foreign', 'columns' => ['pf_tabela_id'], 'references' => ['pf_tabelas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'pf_tabela_id' => 1,
            'municipio_id' => 1
        ],
    ];
}
