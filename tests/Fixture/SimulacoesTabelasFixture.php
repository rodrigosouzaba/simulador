<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SimulacoesTabelasFixture
 *
 */
class SimulacoesTabelasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'simulacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tabela_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'simulacao_id' => ['type' => 'index', 'columns' => ['simulacao_id'], 'length' => []],
            'tabela_id' => ['type' => 'index', 'columns' => ['tabela_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'simulacoes_tabelas_ibfk_2' => ['type' => 'foreign', 'columns' => ['tabela_id'], 'references' => ['tabelas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'simulacoes_tabelas_ibfk_1' => ['type' => 'foreign', 'columns' => ['simulacao_id'], 'references' => ['simulacoes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'simulacao_id' => 1,
            'tabela_id' => 1
        ],
    ];
}
