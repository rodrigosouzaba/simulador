<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TabelasRegioesFixture
 *
 */
class TabelasRegioesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'tabela_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'regiao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'tabela_id' => ['type' => 'index', 'columns' => ['tabela_id'], 'length' => []],
            'regiao_id' => ['type' => 'index', 'columns' => ['regiao_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'tabelas_regioes_ibfk_1' => ['type' => 'foreign', 'columns' => ['tabela_id'], 'references' => ['tabelas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'tabelas_regioes_ibfk_2' => ['type' => 'foreign', 'columns' => ['regiao_id'], 'references' => ['regioes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'tabela_id' => 1,
            'regiao_id' => 1
        ],
    ];
}
