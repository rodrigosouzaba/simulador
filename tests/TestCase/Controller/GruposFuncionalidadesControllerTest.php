<?php
namespace App\Test\TestCase\Controller;

use App\Controller\GruposFuncionalidadesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\GruposFuncionalidadesController Test Case
 */
class GruposFuncionalidadesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.grupos_funcionalidades',
        'app.grupos',
        'app.users',
        'app.imagens',
        'app.operadoras',
        'app.agendamentos',
        'app.tabelas',
        'app.tabelas_regioes',
        'app.regioes',
        'app.estados',
        'app.municipios',
        'app.pf_comercializacoes',
        'app.pf_operadoras',
        'app.pf_carencias',
        'app.pf_formas_pagamentos',
        'app.pf_dependentes',
        'app.pf_documentos',
        'app.pf_observacoes',
        'app.pf_produtos',
        'app.tipos_produtos',
        'app.produtos',
        'app.pf_tabelas',
        'app.pf_atendimentos',
        'app.pf_acomodacoes',
        'app.pf_entidades_tabelas',
        'app.pf_entidades',
        'app.pf_entidades_profissoes',
        'app.pf_profissoes',
        'app.pf_entidades_pf_operadoras',
        'app.pf_calculos_tabelas',
        'app.pf_calculos',
        'app.status',
        'app.simulacoes',
        'app.pdffiltros',
        'app.abrangencias',
        'app.tipos',
        'app.subprodutos',
        'app.filtros',
        'app.tabelas_filtradas',
        'app.simulacoes_tabelas',
        'app.simulacoes_comentarios',
        'app.alertas',
        'app.odonto_calculos',
        'app.odonto_tabelas',
        'app.odonto_comercializacoes',
        'app.odonto_operadoras',
        'app.odonto_redes',
        'app.odonto_produtos',
        'app.odonto_carencias',
        'app.odonto_reembolsos',
        'app.odonto_dependentes',
        'app.odonto_documentos',
        'app.odonto_observacaos',
        'app.odonto_formas_pagamentos',
        'app.municipios_odonto_comercializacoes',
        'app.odonto_atendimentos',
        'app.odonto_calculos_tabelas',
        'app.odonto_filtros_tabelas',
        'app.odonto_filtros',
        'app.links_tabelas',
        'app.links',
        'app.tabelas_geradas',
        'app.tabelas_geradas_produtos',
        'app.pf_filtros',
        'app.pf_filtros_tabelas',
        'app.pf_calculos_dependentes',
        'app.pf_pdffiltros',
        'app.pf_redes',
        'app.pf_reembolsos',
        'app.municipios_pf_comercializacoes',
        'app.municipios_regioes',
        'app.agendamentos_tabelas',
        'app.tabelas_cnpjs',
        'app.cnpjs',
        'app.formas_pagamentos',
        'app.observacoes',
        'app.opcionais',
        'app.redes',
        'app.reembolsos',
        'app.carencias',
        'app.informacoes',
        'app.users_grupos',
        'app.perfis_empresariais',
        'app.detalhes_perfis_empresariais',
        'app.indicados',
        'app.sms_envios',
        'app.sms_cabecalhos',
        'app.sms_listas',
        'app.envios',
        'app.sms_situacoes',
        'app.sms_respostas_envios',
        'app.funcionalidades'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
