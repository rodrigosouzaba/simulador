<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ModalidadesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ModalidadesController Test Case
 */
class ModalidadesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.modalidades',
        'app.carencias',
        'app.tabelas',
        'app.produtos',
        'app.operadoras',
        'app.reembolsos',
        'app.redes',
        'app.opcionais',
        'app.informacoes',
        'app.regioes',
        'app.estados',
        'app.tabelas_regioes',
        'app.observacoes'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
