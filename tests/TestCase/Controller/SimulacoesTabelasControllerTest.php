<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SimulacoesTabelasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SimulacoesTabelasController Test Case
 */
class SimulacoesTabelasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.simulacoes_tabelas',
        'app.simulacoes',
        'app.tabelas_regioes',
        'app.tabelas',
        'app.produtos',
        'app.operadoras',
        'app.ramos',
        'app.modalidades',
        'app.carencias',
        'app.informacoes',
        'app.observacoes',
        'app.opcionais',
        'app.redes',
        'app.reembolsos',
        'app.abrangencias',
        'app.tipos',
        'app.subprodutos',
        'app.regioes',
        'app.estados'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
