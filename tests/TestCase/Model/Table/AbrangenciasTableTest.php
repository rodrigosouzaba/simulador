<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AbrangenciasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AbrangenciasTable Test Case
 */
class AbrangenciasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AbrangenciasTable
     */
    public $Abrangencias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.abrangencias',
        'app.tabelas',
        'app.produtos',
        'app.operadoras',
        'app.reembolsos',
        'app.carencias',
        'app.redes',
        'app.opcionais',
        'app.informacoes',
        'app.regioes',
        'app.estados',
        'app.tabelas_regioes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Abrangencias') ? [] : ['className' => 'App\Model\Table\AbrangenciasTable'];
        $this->Abrangencias = TableRegistry::get('Abrangencias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Abrangencias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
