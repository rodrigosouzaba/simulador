<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AreasComercializacoesEstadosMunicipiosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AreasComercializacoesEstadosMunicipiosTable Test Case
 */
class AreasComercializacoesEstadosMunicipiosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AreasComercializacoesEstadosMunicipiosTable
     */
    public $AreasComercializacoesEstadosMunicipios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AreasComercializacoesEstadosMunicipios',
        'app.Estados',
        'app.AreaComercializacaos',
        'app.Municipios',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AreasComercializacoesEstadosMunicipios') ? [] : ['className' => AreasComercializacoesEstadosMunicipiosTable::class];
        $this->AreasComercializacoesEstadosMunicipios = TableRegistry::getTableLocator()->get('AreasComercializacoesEstadosMunicipios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AreasComercializacoesEstadosMunicipios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
