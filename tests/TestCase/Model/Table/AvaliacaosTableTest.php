<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AvaliacaosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AvaliacaosTable Test Case
 */
class AvaliacaosTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\AvaliacaosTable     */
    public $Avaliacaos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.avaliacaos',
        'app.users',
        'app.imagens',
        'app.operadoras',
        'app.estados',
        'app.regioes',
        'app.tabelas',
        'app.tabelas_regioes',
        'app.tabelas_cnpjs',
        'app.cnpjs',
        'app.simulacoes_tabelas',
        'app.simulacoes',
        'app.status',
        'app.pdffiltros',
        'app.abrangencias',
        'app.tipos',
        'app.subprodutos',
        'app.tipos_produtos',
        'app.produtos',
        'app.tabelas_filtradas',
        'app.filtros',
        'app.formas_pagamentos',
        'app.observacoes',
        'app.opcionais',
        'app.redes',
        'app.reembolsos',
        'app.carencias',
        'app.informacoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Avaliacaos') ? [] : ['className' => 'App\Model\Table\AvaliacaosTable'];        $this->Avaliacaos = TableRegistry::get('Avaliacaos', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Avaliacaos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
