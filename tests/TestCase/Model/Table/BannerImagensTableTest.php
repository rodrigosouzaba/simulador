<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BannerImagensTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BannerImagensTable Test Case
 */
class BannerImagensTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\BannerImagensTable     */
    public $BannerImagens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.banner_imagens'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BannerImagens') ? [] : ['className' => 'App\Model\Table\BannerImagensTable'];        $this->BannerImagens = TableRegistry::get('BannerImagens', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BannerImagens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
