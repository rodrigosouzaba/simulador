<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CarenciasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CarenciasTable Test Case
 */
class CarenciasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CarenciasTable
     */
    public $Carencias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.carencias',
        'app.tabelas',
        'app.produtos',
        'app.operadoras',
        'app.regioes',
        'app.estados',
        'app.tabelas_regioes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Carencias') ? [] : ['className' => 'App\Model\Table\CarenciasTable'];
        $this->Carencias = TableRegistry::get('Carencias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Carencias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
