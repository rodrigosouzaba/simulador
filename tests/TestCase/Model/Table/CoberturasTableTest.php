<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CoberturasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CoberturasTable Test Case
 */
class CoberturasTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\CoberturasTable     */
    public $Coberturas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.coberturas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Coberturas') ? [] : ['className' => 'App\Model\Table\CoberturasTable'];        $this->Coberturas = TableRegistry::get('Coberturas', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Coberturas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
