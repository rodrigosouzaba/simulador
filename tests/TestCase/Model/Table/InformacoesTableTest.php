<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InformacoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InformacoesTable Test Case
 */
class InformacoesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InformacoesTable
     */
    public $Informacoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.informacoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Informacoes') ? [] : ['className' => 'App\Model\Table\InformacoesTable'];
        $this->Informacoes = TableRegistry::get('Informacoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Informacoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
