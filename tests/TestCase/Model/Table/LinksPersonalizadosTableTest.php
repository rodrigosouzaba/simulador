<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LinksPersonalizadosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LinksPersonalizadosTable Test Case
 */
class LinksPersonalizadosTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\LinksPersonalizadosTable     */
    public $LinksPersonalizados;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.links_personalizados',
        'app.vendas_onlines_operadoras',
        'app.vendas_onlines',
        'app.funcionalidades',
        'app.grupos',
        'app.users',
        'app.imagens',
        'app.operadoras',
        'app.agendamentos',
        'app.tabelas',
        'app.tabelas_regioes',
        'app.regioes',
        'app.estados',
        'app.municipios',
        'app.pf_comercializacoes',
        'app.pf_operadoras',
        'app.pf_carencias',
        'app.pf_formas_pagamentos',
        'app.pf_dependentes',
        'app.pf_documentos',
        'app.pf_observacoes',
        'app.pf_produtos',
        'app.tipos_produtos',
        'app.produtos',
        'app.pf_tabelas',
        'app.pf_atendimentos',
        'app.pf_acomodacoes',
        'app.pf_entidades_tabelas',
        'app.pf_entidades',
        'app.pf_entidades_profissoes',
        'app.pf_profissoes',
        'app.pf_calculos_tabelas',
        'app.pf_calculos',
        'app.status',
        'app.simulacoes',
        'app.pdffiltros',
        'app.abrangencias',
        'app.tipos',
        'app.subprodutos',
        'app.filtros',
        'app.tabelas_filtradas',
        'app.simulacoes_tabelas',
        'app.simulacoes_comentarios',
        'app.alertas',
        'app.odonto_calculos',
        'app.odonto_tabelas',
        'app.odonto_comercializacoes',
        'app.odonto_operadoras',
        'app.odonto_redes',
        'app.odonto_produtos',
        'app.odonto_carencias',
        'app.odonto_reembolsos',
        'app.odonto_dependentes',
        'app.odonto_documentos',
        'app.odonto_observacaos',
        'app.odonto_formas_pagamentos',
        'app.municipios_odonto_comercializacoes',
        'app.odonto_atendimentos',
        'app.odonto_calculos_tabelas',
        'app.odonto_filtros',
        'app.odonto_filtros_tabelas',
        'app.pf_filtros',
        'app.pf_filtros_tabelas',
        'app.pf_calculos_dependentes',
        'app.pf_pdffiltros',
        'app.pf_redes',
        'app.pf_reembolsos',
        'app.pf_entidades_pf_operadoras',
        'app.municipios_pf_comercializacoes',
        'app.municipios_produtos',
        'app.municipios_regioes',
        'app.agendamentos_tabelas',
        'app.tabelas_cnpjs',
        'app.cnpjs',
        'app.formas_pagamentos',
        'app.observacoes',
        'app.opcionais',
        'app.redes',
        'app.reembolsos',
        'app.carencias',
        'app.informacoes',
        'app.tabelas_geradas',
        'app.tabelas_geradas_produtos',
        'app.links_tabelas',
        'app.links',
        'app.users_grupos',
        'app.perfis_empresariais',
        'app.detalhes_perfis_empresariais',
        'app.indicados',
        'app.sms_envios',
        'app.sms_cabecalhos',
        'app.sms_listas',
        'app.envios',
        'app.sms_situacoes',
        'app.sms_respostas_envios',
        'app.grupos_funcionalidades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LinksPersonalizados') ? [] : ['className' => 'App\Model\Table\LinksPersonalizadosTable'];        $this->LinksPersonalizados = TableRegistry::get('LinksPersonalizados', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LinksPersonalizados);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
