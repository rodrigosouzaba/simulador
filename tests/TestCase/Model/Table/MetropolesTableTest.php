<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MetropolesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MetropolesTable Test Case
 */
class MetropolesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MetropolesTable
     */
    public $Metropoles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Metropoles',
        'app.Municipios',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Metropoles') ? [] : ['className' => MetropolesTable::class];
        $this->Metropoles = TableRegistry::getTableLocator()->get('Metropoles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Metropoles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
