<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MidiasCompartilhadasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MidiasCompartilhadasTable Test Case
 */
class MidiasCompartilhadasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MidiasCompartilhadasTable
     */
    public $MidiasCompartilhadas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MidiasCompartilhadas',
        'app.Estados',
        'app.Operadoras',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MidiasCompartilhadas') ? [] : ['className' => MidiasCompartilhadasTable::class];
        $this->MidiasCompartilhadas = TableRegistry::getTableLocator()->get('MidiasCompartilhadas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MidiasCompartilhadas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
