<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MunicipiosRegioesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MunicipiosRegioesTable Test Case
 */
class MunicipiosRegioesTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\MunicipiosRegioesTable     */
    public $MunicipiosRegioes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.municipios_regioes',
        'app.municipios',
        'app.estados',
        'app.regioes',
        'app.operadoras',
        'app.imagens',
        'app.tabelas',
        'app.tabelas_regioes',
        'app.tabelas_cnpjs',
        'app.cnpjs',
        'app.simulacoes_tabelas',
        'app.simulacoes',
        'app.users',
        'app.tabelas_geradas',
        'app.tabelas_geradas_produtos',
        'app.links_tabelas',
        'app.links',
        'app.pf_tabelas',
        'app.pf_produtos',
        'app.pf_operadoras',
        'app.pf_carencias',
        'app.pf_formas_pagamentos',
        'app.pf_comercializacoes',
        'app.municipios_pf_comercializacoes',
        'app.pf_dependentes',
        'app.pf_documentos',
        'app.pf_observacoes',
        'app.pf_redes',
        'app.pf_reembolsos',
        'app.pf_entidades_pf_operadoras',
        'app.pf_entidades',
        'app.pf_entidades_profissoes',
        'app.pf_profissoes',
        'app.pf_entidades_tabelas',
        'app.tipos_produtos',
        'app.produtos',
        'app.pf_atendimentos',
        'app.pf_acomodacoes',
        'app.pf_calculos_tabelas',
        'app.pf_calculos',
        'app.status',
        'app.pf_filtros',
        'app.pf_filtros_tabelas',
        'app.pf_calculos_dependentes',
        'app.odonto_tabelas',
        'app.odonto_comercializacoes',
        'app.odonto_operadoras',
        'app.odonto_redes',
        'app.odonto_produtos',
        'app.odonto_carencias',
        'app.odonto_reembolsos',
        'app.odonto_dependentes',
        'app.odonto_documentos',
        'app.odonto_observacaos',
        'app.odonto_formas_pagamentos',
        'app.odonto_atendimentos',
        'app.odonto_calculos',
        'app.pdffiltros',
        'app.abrangencias',
        'app.tipos',
        'app.subprodutos',
        'app.tabelas_filtradas',
        'app.filtros',
        'app.formas_pagamentos',
        'app.observacoes',
        'app.opcionais',
        'app.redes',
        'app.reembolsos',
        'app.carencias',
        'app.informacoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MunicipiosRegioes') ? [] : ['className' => 'App\Model\Table\MunicipiosRegioesTable'];        $this->MunicipiosRegioes = TableRegistry::get('MunicipiosRegioes', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MunicipiosRegioes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
