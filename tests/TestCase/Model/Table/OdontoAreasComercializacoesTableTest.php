<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OdontoAreasComercializacoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OdontoAreasComercializacoesTable Test Case
 */
class OdontoAreasComercializacoesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OdontoAreasComercializacoesTable
     */
    public $OdontoAreasComercializacoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OdontoAreasComercializacoes',
        'app.OdontoOperadoras',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OdontoAreasComercializacoes') ? [] : ['className' => OdontoAreasComercializacoesTable::class];
        $this->OdontoAreasComercializacoes = TableRegistry::getTableLocator()->get('OdontoAreasComercializacoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OdontoAreasComercializacoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
