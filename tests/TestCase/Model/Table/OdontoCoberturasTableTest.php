<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OdontoCoberturasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OdontoCoberturasTable Test Case
 */
class OdontoCoberturasTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\OdontoCoberturasTable     */
    public $OdontoCoberturas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.odonto_coberturas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OdontoCoberturas') ? [] : ['className' => 'App\Model\Table\OdontoCoberturasTable'];        $this->OdontoCoberturas = TableRegistry::get('OdontoCoberturas', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OdontoCoberturas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
