<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OperadorasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OperadorasTable Test Case
 */
class OperadorasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OperadorasTable
     */
    public $Operadoras;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.operadoras',
        'app.produtos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Operadoras') ? [] : ['className' => 'App\Model\Table\OperadorasTable'];
        $this->Operadoras = TableRegistry::get('Operadoras', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Operadoras);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
