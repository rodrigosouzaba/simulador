<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PfAreasComercializacoesEstadosMunicipiosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PfAreasComercializacoesEstadosMunicipiosTable Test Case
 */
class PfAreasComercializacoesEstadosMunicipiosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PfAreasComercializacoesEstadosMunicipiosTable
     */
    public $PfAreasComercializacoesEstadosMunicipios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PfAreasComercializacoesEstadosMunicipios',
        'app.Estados',
        'app.PfComercializacaos',
        'app.Municipios',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PfAreasComercializacoesEstadosMunicipios') ? [] : ['className' => PfAreasComercializacoesEstadosMunicipiosTable::class];
        $this->PfAreasComercializacoesEstadosMunicipios = TableRegistry::getTableLocator()->get('PfAreasComercializacoesEstadosMunicipios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PfAreasComercializacoesEstadosMunicipios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
