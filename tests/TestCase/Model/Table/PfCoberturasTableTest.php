<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PfCoberturasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PfCoberturasTable Test Case
 */
class PfCoberturasTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\PfCoberturasTable     */
    public $PfCoberturas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pf_coberturas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PfCoberturas') ? [] : ['className' => 'App\Model\Table\PfCoberturasTable'];        $this->PfCoberturas = TableRegistry::get('PfCoberturas', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PfCoberturas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
