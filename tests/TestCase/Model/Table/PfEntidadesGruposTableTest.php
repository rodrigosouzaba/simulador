<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PfEntidadesGruposTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PfEntidadesGruposTable Test Case
 */
class PfEntidadesGruposTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PfEntidadesGruposTable
     */
    public $PfEntidadesGrupos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PfEntidadesGrupos',
        'app.PfOperadoras',
        'app.PfEntidades',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PfEntidadesGrupos') ? [] : ['className' => PfEntidadesGruposTable::class];
        $this->PfEntidadesGrupos = TableRegistry::getTableLocator()->get('PfEntidadesGrupos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PfEntidadesGrupos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
