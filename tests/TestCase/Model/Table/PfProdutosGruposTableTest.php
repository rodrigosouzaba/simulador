<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PfProdutosGruposTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PfProdutosGruposTable Test Case
 */
class PfProdutosGruposTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PfProdutosGruposTable
     */
    public $PfProdutosGrupos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PfProdutosGrupos',
        'app.PfOperadoras',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PfProdutosGrupos') ? [] : ['className' => PfProdutosGruposTable::class];
        $this->PfProdutosGrupos = TableRegistry::getTableLocator()->get('PfProdutosGrupos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PfProdutosGrupos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
