<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PfProfissoesAliasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PfProfissoesAliasTable Test Case
 */
class PfProfissoesAliasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PfProfissoesAliasTable
     */
    public $PfProfissoesAlias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PfProfissoesAlias',
        'app.PfProfissoes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PfProfissoesAlias') ? [] : ['className' => PfProfissoesAliasTable::class];
        $this->PfProfissoesAlias = TableRegistry::getTableLocator()->get('PfProfissoesAlias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PfProfissoesAlias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
