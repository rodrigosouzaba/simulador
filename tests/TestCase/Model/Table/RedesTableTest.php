<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RedesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RedesTable Test Case
 */
class RedesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RedesTable
     */
    public $Redes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.redes',
        'app.tabelas',
        'app.produtos',
        'app.operadoras',
        'app.regioes',
        'app.estados',
        'app.tabelas_regioes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Redes') ? [] : ['className' => 'App\Model\Table\RedesTable'];
        $this->Redes = TableRegistry::get('Redes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Redes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
