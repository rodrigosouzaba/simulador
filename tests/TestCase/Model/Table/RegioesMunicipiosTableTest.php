<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RegioesMunicipiosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RegioesMunicipiosTable Test Case
 */
class RegioesMunicipiosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RegioesMunicipiosTable
     */
    public $RegioesMunicipios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.regioes_municipios',
        'app.municipios',
        'app.estados',
        'app.regioes',
        'app.operadoras',
        'app.agendamentos',
        'app.users',
        'app.imagens',
        'app.tabelas_geradas',
        'app.tabelas_geradas_produtos',
        'app.tabelas',
        'app.tabelas_regioes',
        'app.agendamentos_tabelas',
        'app.tabelas_cnpjs',
        'app.cnpjs',
        'app.simulacoes_tabelas',
        'app.simulacoes',
        'app.status',
        'app.pdffiltros',
        'app.abrangencias',
        'app.tipos',
        'app.subprodutos',
        'app.tipos_produtos',
        'app.produtos',
        'app.filtros',
        'app.tabelas_filtradas',
        'app.simulacoes_comentarios',
        'app.alertas',
        'app.pf_calculos',
        'app.pf_profissoes',
        'app.pf_calculos_tabelas',
        'app.pf_tabelas',
        'app.pf_produtos',
        'app.pf_operadoras',
        'app.pf_carencias',
        'app.pf_formas_pagamentos',
        'app.pf_comercializacoes',
        'app.municipios_pf_comercializacoes',
        'app.pf_dependentes',
        'app.pf_documentos',
        'app.pf_observacoes',
        'app.pf_redes',
        'app.pf_reembolsos',
        'app.pf_entidades',
        'app.pf_entidades_profissoes',
        'app.pf_entidades_tabelas',
        'app.pf_entidades_pf_operadoras',
        'app.pf_coberturas',
        'app.pf_atendimentos',
        'app.pf_acomodacoes',
        'app.pf_filtros_tabelas',
        'app.pf_filtros',
        'app.pf_comercializacoes_municipios',
        'app.pf_calculos_dependentes',
        'app.pf_pdffiltros',
        'app.odonto_calculos',
        'app.odonto_tabelas',
        'app.odonto_comercializacoes',
        'app.odonto_operadoras',
        'app.odonto_redes',
        'app.odonto_produtos',
        'app.odonto_carencias',
        'app.odonto_reembolsos',
        'app.odonto_dependentes',
        'app.odonto_documentos',
        'app.odonto_observacaos',
        'app.odonto_formas_pagamentos',
        'app.municipios_odonto_comercializacoes',
        'app.odonto_atendimentos',
        'app.odonto_calculos_tabelas',
        'app.odonto_filtros',
        'app.odonto_filtros_tabelas',
        'app.coberturas',
        'app.links_tabelas',
        'app.links',
        'app.users_grupos',
        'app.grupos',
        'app.perfis_empresariais',
        'app.detalhes_perfis_empresariais',
        'app.indicados',
        'app.sms_envios',
        'app.sms_cabecalhos',
        'app.sms_listas',
        'app.envios',
        'app.sms_situacoes',
        'app.sms_respostas_envios',
        'app.formas_pagamentos',
        'app.observacoes',
        'app.opcionais',
        'app.redes',
        'app.reembolsos',
        'app.carencias',
        'app.informacoes',
        'app.municipios_regioes',
        'app.vendas_onlines_operadoras',
        'app.vendas_onlines',
        'app.funcionalidades',
        'app.grupos_funcionalidades',
        'app.vendas_onlines_operadoras_estados',
        'app.links_personalizados',
        'app.municipios_produtos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RegioesMunicipios') ? [] : ['className' => 'App\Model\Table\RegioesMunicipiosTable'];
        $this->RegioesMunicipios = TableRegistry::get('RegioesMunicipios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RegioesMunicipios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
