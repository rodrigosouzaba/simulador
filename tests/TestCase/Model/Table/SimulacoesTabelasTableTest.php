<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SimulacoesTabelasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SimulacoesTabelasTable Test Case
 */
class SimulacoesTabelasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SimulacoesTabelasTable
     */
    public $SimulacoesTabelas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.simulacoes_tabelas',
        'app.simulacoes',
        'app.tabelas_regioes',
        'app.tabelas',
        'app.produtos',
        'app.operadoras',
        'app.ramos',
        'app.modalidades',
        'app.carencias',
        'app.informacoes',
        'app.observacoes',
        'app.opcionais',
        'app.redes',
        'app.reembolsos',
        'app.abrangencias',
        'app.tipos',
        'app.subprodutos',
        'app.regioes',
        'app.estados'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SimulacoesTabelas') ? [] : ['className' => 'App\Model\Table\SimulacoesTabelasTable'];
        $this->SimulacoesTabelas = TableRegistry::get('SimulacoesTabelas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SimulacoesTabelas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
