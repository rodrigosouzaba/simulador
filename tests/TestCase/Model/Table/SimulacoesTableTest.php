<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SimulacoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SimulacoesTable Test Case
 */
class SimulacoesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SimulacoesTable
     */
    public $Simulacoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.simulacoes',
        'app.tabelas_regioes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Simulacoes') ? [] : ['className' => 'App\Model\Table\SimulacoesTable'];
        $this->Simulacoes = TableRegistry::get('Simulacoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Simulacoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
