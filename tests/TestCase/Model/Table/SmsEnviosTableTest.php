<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SmsEnviosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SmsEnviosTable Test Case
 */
class SmsEnviosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SmsEnviosTable
     */
    public $SmsEnvios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SmsEnvios',
        'app.SmsCabecalhos',
        'app.SmsListas',
        'app.Users',
        'app.SmsSituacoes',
        'app.SmsRespostasEnvios',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SmsEnvios') ? [] : ['className' => SmsEnviosTable::class];
        $this->SmsEnvios = TableRegistry::getTableLocator()->get('SmsEnvios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SmsEnvios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
