<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubprodutosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubprodutosTable Test Case
 */
class SubprodutosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubprodutosTable
     */
    public $Subprodutos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.subprodutos',
        'app.tipos',
        'app.tabelas',
        'app.produtos',
        'app.operadoras',
        'app.reembolsos',
        'app.carencias',
        'app.redes',
        'app.opcionais',
        'app.informacoes',
        'app.regioes',
        'app.estados',
        'app.tabelas_regioes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Subprodutos') ? [] : ['className' => 'App\Model\Table\SubprodutosTable'];
        $this->Subprodutos = TableRegistry::get('Subprodutos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Subprodutos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
