<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TabelasRegioesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TabelasRegioesTable Test Case
 */
class TabelasRegioesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TabelasRegioesTable
     */
    public $TabelasRegioes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tabelas_regioes',
        'app.tabelas',
        'app.produtos',
        'app.operadoras',
        'app.regioes',
        'app.estados'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TabelasRegioes') ? [] : ['className' => 'App\Model\Table\TabelasRegioesTable'];
        $this->TabelasRegioes = TableRegistry::get('TabelasRegioes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TabelasRegioes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
