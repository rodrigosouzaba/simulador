<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TabelasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TabelasTable Test Case
 */
class TabelasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TabelasTable
     */
    public $Tabelas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tabelas',
        'app.produtos',
        'app.operadoras',
        'app.regioes',
        'app.estados',
        'app.tabelas_regioes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tabelas') ? [] : ['className' => 'App\Model\Table\TabelasTable'];
        $this->Tabelas = TableRegistry::get('Tabelas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tabelas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
