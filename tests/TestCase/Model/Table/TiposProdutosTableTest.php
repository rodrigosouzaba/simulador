<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TiposProdutosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TiposProdutosTable Test Case
 */
class TiposProdutosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TiposProdutosTable
     */
    public $TiposProdutos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipos_produtos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TiposProdutos') ? [] : ['className' => 'App\Model\Table\TiposProdutosTable'];
        $this->TiposProdutos = TableRegistry::get('TiposProdutos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TiposProdutos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
