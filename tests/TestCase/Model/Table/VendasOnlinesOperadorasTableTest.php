<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VendasOnlinesOperadorasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VendasOnlinesOperadorasTable Test Case
 */
class VendasOnlinesOperadorasTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\VendasOnlinesOperadorasTable     */
    public $VendasOnlinesOperadoras;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vendas_onlines_operadoras',
        'app.estados',
        'app.regioes',
        'app.operadoras',
        'app.agendamentos',
        'app.users',
        'app.imagens',
        'app.municipios',
        'app.pf_comercializacoes',
        'app.pf_operadoras',
        'app.pf_carencias',
        'app.pf_formas_pagamentos',
        'app.pf_dependentes',
        'app.pf_documentos',
        'app.pf_observacoes',
        'app.pf_produtos',
        'app.tipos_produtos',
        'app.produtos',
        'app.tabelas',
        'app.tabelas_regioes',
        'app.agendamentos_tabelas',
        'app.tabelas_cnpjs',
        'app.cnpjs',
        'app.simulacoes_tabelas',
        'app.simulacoes',
        'app.status',
        'app.pdffiltros',
        'app.abrangencias',
        'app.tipos',
        'app.subprodutos',
        'app.filtros',
        'app.tabelas_filtradas',
        'app.simulacoes_comentarios',
        'app.alertas',
        'app.pf_calculos',
        'app.pf_profissoes',
        'app.pf_calculos_tabelas',
        'app.pf_tabelas',
        'app.pf_atendimentos',
        'app.pf_acomodacoes',
        'app.pf_entidades_tabelas',
        'app.pf_entidades',
        'app.pf_entidades_profissoes',
        'app.pf_filtros_tabelas',
        'app.pf_filtros',
        'app.pf_calculos_dependentes',
        'app.pf_pdffiltros',
        'app.odonto_calculos',
        'app.odonto_tabelas',
        'app.odonto_comercializacoes',
        'app.odonto_operadoras',
        'app.odonto_redes',
        'app.odonto_produtos',
        'app.odonto_carencias',
        'app.odonto_reembolsos',
        'app.odonto_dependentes',
        'app.odonto_documentos',
        'app.odonto_observacaos',
        'app.odonto_formas_pagamentos',
        'app.municipios_odonto_comercializacoes',
        'app.odonto_atendimentos',
        'app.odonto_calculos_tabelas',
        'app.odonto_filtros',
        'app.odonto_filtros_tabelas',
        'app.pf_redes',
        'app.pf_reembolsos',
        'app.pf_entidades_pf_operadoras',
        'app.municipios_pf_comercializacoes',
        'app.municipios_produtos',
        'app.tabelas_geradas',
        'app.tabelas_geradas_produtos',
        'app.links_tabelas',
        'app.links',
        'app.users_grupos',
        'app.grupos',
        'app.perfis_empresariais',
        'app.detalhes_perfis_empresariais',
        'app.indicados',
        'app.sms_envios',
        'app.sms_cabecalhos',
        'app.sms_listas',
        'app.envios',
        'app.sms_situacoes',
        'app.sms_respostas_envios',
        'app.formas_pagamentos',
        'app.observacoes',
        'app.opcionais',
        'app.redes',
        'app.reembolsos',
        'app.carencias',
        'app.informacoes',
        'app.municipios_regioes',
        'app.links_personalizados'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('VendasOnlinesOperadoras') ? [] : ['className' => 'App\Model\Table\VendasOnlinesOperadorasTable'];        $this->VendasOnlinesOperadoras = TableRegistry::get('VendasOnlinesOperadoras', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VendasOnlinesOperadoras);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
