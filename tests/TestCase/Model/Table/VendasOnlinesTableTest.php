<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VendasOnlinesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VendasOnlinesTable Test Case
 */
class VendasOnlinesTableTest extends TestCase
{

    /**
     * Test subject     *
     * @var \App\Model\Table\VendasOnlinesTable     */
    public $VendasOnlines;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vendas_onlines'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('VendasOnlines') ? [] : ['className' => 'App\Model\Table\VendasOnlinesTable'];        $this->VendasOnlines = TableRegistry::get('VendasOnlines', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VendasOnlines);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
