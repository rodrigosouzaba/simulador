$(function () {
    $('#modalProcessando').on('hide.bs.modal', function (e) {
        $('body').removeClass('modal-open');
    })

    $(".core").click(function () {
        $(".sub").css("left",);
    });

    $("#indicacao-celular").mask('99 99999 9999', {
        placeholder: ''
    })
    var step = 1;

    $(".icones").click(function () {
        $(".icones").removeClass('selecionado');
        $(this).addClass("selecionado");
        appendToInput($(this).attr("nota"));
    });

    $('#modalAvaliacao').on('hidden.bs.modal', function () {
        if ($("#nota").val() != "") {
            $.ajax({
                type: 'POST',
                url: baseUrl + 'avaliacaos/save',
                data: {
                    'nota': $("#nota").val(),
                    'tipo': $("input[name='tipo']:checked").val(),
                    'comentario': $("#comentario").val()
                },
                success: function () {
                    $(".icones").removeClass("selecionado");
                    $("input[name='tipo']").prop("checked", false);
                    $("#comentario").empty();
                }
            });
        }

        if (validaIndicacao()) {
            let nome = $("#indicacao-nome").val();
            let email = $("#indicacao-email").val();
            let celular = $("#indicacao-celular").val();
            $.ajax({
                type: 'POST',
                url: baseUrl + 'indicados/add',
                data: {
                    'nome': nome,
                    'email': email,
                    'celular': celular
                },
                success: function () {
                    $("#indicacao-nome").empty();
                    $("#indicacao-email").empty();
                    $("#indicacao-celular").empty();
                }
            });

        }
    });

    $(".icones").click(() => {
        avancar();
        controleBotoes();
    });

    $("#voltar-avaliacao").click(() => {
        voltar();
        controleBotoes();
    });


    var largura = $(window).width();
    if (largura < 800) {
        $('.banner').removeClass('bannerLg');
        $('.botaoCentralInterna').css('font-size', "9px");
    }

    $('[data-toggle="popover"]').popover();

    $(".confirmacao").click(function (evt) {
        evt.preventDefault();
        let url = $(this).attr("link");
        $("#modal-confirmacao").modal("show");
        $("#link-continuar").attr("href", url);
    });

    // $("a").click(function() {
    //     let tamanho = $(this).attr("href").length;
    //     if ($(this).attr("href") != "#" && $(this).attr("href")[0] != "#" && $(this).attr("target") != "_blank") {
    //         $("#modalProcessando").modal("show");
    //     }
    // });

    $('[data-toggle="tooltip"]').tooltip();

    $(".alerta").click(function () {
        $('#modal-alerta').modal('show');
    });

    try {
        $.datepicker.setDefaults($.datepicker.regional["pt-BR"]);
    } catch (error) {
        console.log(error)
    }

    $('#username').focus();
    //        $("#username").mask("999.999.999-99");
    $("#celular").mask("(99) 9 9999-9999");
    $(".celular").mask("(99) 9 9999-9999");

    $(".controle").click(function () {
        $(".fa-chevron-circle-up").toggleClass('fa-chevron-circle-down');
    });

    $("#EnderecoGelEstadoId").on('change', function () {
        var controller = 'gelCidades';
        var params = { 'gel_estado_id': $(this).val(), 'active': 1 };
        window.materialadmin.AppForm.updateSelectField(controller, $("#EnderecoGelCidadeId"), params);
    });

    $("#link-modal-user").click(function () {
        $.get(baseUrl + 'avaliacaos/get-user/' + userId, function (data) {
            $("#data-user").empty();
            $("#data-user").append(data);
            $("#modal-user").modal("show");
        });
    });

    if ($(window).width() <= 500) {
        $(".canvas-logo-usuario").css("width", "auto");
    }

    $(".botaoMenu").attr('style', 'min-height: unset !important; margin-bottom: 0 !important;padding: 0 !important;');
    $(".menumenu").attr('style', 'float: unset !important');
    if ($(window).width() <= 1080) {
        $(".userLogado").attr('class', 'userLogadoMobile');
        $(".espaco").remove();
        $(".quebra").append("<br/>");
        $(".menuCR").attr('style', 'float: unset !important');
        $(".botaoMenu").attr('style', 'float: unset !important');
        $(".botaoMenu").attr('style', 'min-height: unset !important');
    }

    $("#modalCliente").click(function () {
        $.ajax({
            type: "get",
            url: baseUrl + "/users/enviarformulario/" + userId,
            success: function (data) {
                $("#conteudoFormularioCliente").empty();
                $("#conteudoFormularioCliente").append(data);
            }
        });

    });

    $("#sim-faleConosco").click(() => {
        $("#faleConoscoContent").empty()
        $("#faleConoscoContent").append("<p class='centralizada' style='font-size: 18px;'>Obrigado! Em breve nossa equipe entrará em contato.<p/>")
        $("#botoes-resposta").hide();
        $.post(baseUrl + 'avaliacaos/add', {
            user_id: userId,
            nota: 99,
            tipo: 'ATENDIMENTO FALE CONOSCO'
        })
    })

    $("#btn-submenu").click(event => {
        event.stopPropagation()
        $("#submenu").collapse("toggle")
        $(".divisor").toggle();
        if ($("#submenu-direction").hasClass("fa-chevron-down")) {
            $("#submenu-direction").removeClass("fa-chevron-down")
            $("#submenu-direction").addClass("fa-chevron-up")
        } else {
            $("#submenu-direction").removeClass("fa-chevron-up")
            $("#submenu-direction").addClass("fa-chevron-down")
        }
    });

    if ($(window).width() > 767) {

        $("#btn-config").mouseenter(function () {
            $(this).addClass("open");
        });
        $("#btn-config").mouseleave(function () {
            $(this).removeClass("open");
        });

        $("#btn-gerencie").mouseenter(function () {
            $(this).addClass("open");
        });

        $("#btn-gerencie").mouseleave(function () {
            $(this).removeClass("open");
        });

        $("#btn-cotacoes").mouseenter(function () {
            $(this).addClass("open");
        });
        $("#btn-cotacoes").mouseleave(function () {
            $(this).removeClass("open");
        });

        $("#btn-vendas").mouseenter(function () {
            $(this).addClass("open");
        });
        $("#btn-vendas").mouseleave(function () {
            $(this).removeClass("open");
        });

        $("#btn-suporte").mouseenter(function () {
            $(this).addClass("open");
        });
        $("#btn-suporte").mouseleave(function () {
            $(this).removeClass("open");
        });
    } else {
        $("#btn-gerencie").click(function () {
            let icone = $(this).children("a").children(".direcao");
            if (icone.hasClass("fa-chevron-down")) {
                icone.addClass("fa-chevron-up");
                icone.removeClass("fa-chevron-down");
            } else {
                icone.addClass("fa-chevron-down");
                icone.removeClass("fa-chevron-up");
            }
        });

        $("#btn-cotacoes").click(function () {
            let icone = $(this).children("a").children(".direcao");
            if (icone.hasClass("fa-chevron-down")) {
                icone.addClass("fa-chevron-up");
                icone.removeClass("fa-chevron-down");
            } else {
                icone.addClass("fa-chevron-down");
                icone.removeClass("fa-chevron-up");
            }
        });

        $("#btn-vendas").click(function () {
            let icone = $(this).children("a").children(".direcao");
            if (icone.hasClass("fa-chevron-down")) {
                icone.addClass("fa-chevron-up");
                icone.removeClass("fa-chevron-down");
            } else {
                icone.addClass("fa-chevron-down");
                icone.removeClass("fa-chevron-up");
            }
        });

        $("#btn-suporte").click(function () {
            let icone = $(this).children("a").children(".direcao");
            if (icone.hasClass("fa-chevron-down")) {
                icone.addClass("fa-chevron-up");
                icone.removeClass("fa-chevron-down");
            } else {
                icone.addClass("fa-chevron-down");
                icone.removeClass("fa-chevron-up");
            }
        });
    }

    $(".dropdown-hamburger").click(function (evt) {
        evt.stopPropagation();
        var dest = $(this).attr("href")
        $(dest).collapse('toggle');
    })

    $("#ramo").change(function () {
        showInput();
    });
    $("#estado").change(function () {
        showInput();
    });

    $("#rede-input").keypress(function (e) {
        if (e.which == 13) {
            pesquisar();
        }
    });
    $("#rede-submit").click(function () {
        pesquisar();
    });
    $("#btn-rede-credenciada").click(function () {
        $("#rede-resposta").empty();
    });


});
var step = 1;
const avancar = function () {
    $(".content-" + step).removeClass('content-active-' + step)
    $(".step-" + step).removeClass('step-active')
    step++;
    $(".content-" + step).addClass('content-active-' + step)
    $(".step-" + step).addClass('step-active')
}

const ucfirst = function (str) {

    str += '';
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1);
}

const appendToInput = function (nota) {
    $("#nota").val(nota);
    if (nota) {
        $("#continuar-avaliacao").removeProp('disabled');
    }
}

const voltar = function () {
    $(".content-" + step).removeClass('content-active-' + step)
    $(".step-" + step).removeClass('step-active')
    step--;
    $(".content-" + step).addClass('content-active-' + step)
    $(".step-" + step).addClass('step-active')
}

const controleBotoes = function () {
    switch (step) {
        case 1:
            $("#continuar-avaliacao").css('visibility', 'visible');
            $("#voltar-avaliacao").css('visibility', 'hidden');
            $("#salvar-avaliacao").css('display', 'none');
            break;
        case 2:
            $("#voltar-avaliacao").css('visibility', 'visible');
            $("#continuar-avaliacao").css('visibility', 'visible');
            $("#salvar-avaliacao").css('display', 'block');
            break;
        case 3:
            $("#voltar-avaliacao").css('visibility', 'visible');
            $("#continuar-avaliacao").css('visibility', 'hidden');
            $("#salvar-avaliacao").css('display', 'none');
            break;
    }
}

const validaIndicacao = function () {

    let nome = $('#indicacao-nome').val();
    let email = $('#indicacao-email').val();
    let celular = $('#indicacao-celular').val();
    if (nome != "" && email != "") {
        return true;
    } else if (nome != "" && celular != "") {
        return true;
    } else {
        return false;
    }
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

const getScrollBarWidth = function () {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2)
        w2 = outer.clientWidth;

    document.body.removeChild(outer);

    return (w1 - w2);
};

const showInput = function () {
    if ($("#estado").val() != '' && $("#ramo").val() != '') {
        $("#input-group").show();
    }
}

const pesquisar = function () {
    var estado = $("#estado").val();
    var ramo = $("#ramo").val();
    var palavra = $("#rede-input").val();
    if (estado != '' && ramo != '' && palavra != "") {
        var submit = $("#rede-submit");
        var icone = $("#icon-search");
        var url;
        if ($("#ramo").val() == "PF") {
            url = baseUrl + "pfRedes/pesquisar/" + $("#rede-input").val() + "/" + $(
                "#estado").val()
        } else {
            url = baseUrl + "redes/pesquisar/ " + $("#rede-input").val() + " / " + $(
                "#estado").val()
        }
        $.ajax({
            type: 'GET',
            url: url,
            beforeSend: function () {
                submit.prop("disable", true);
                icone.removeClass("fa-search");
                //     icone.append('<?= $this->Html->image("ciranda.gif", ["style" => "height: 100%;"]); ?>');
            },
            success: function (data) {
                $("#rede-resposta").empty();
                $("#rede-resposta").append(data);
                icone.addClass("fa-search");
                icone.empty();
            }
        });
    } else {
        $("#error").append("<p class='text-danger'>Preencha todos os campos!</p>")
    }
}

const calculaIdade = function (dataInicial, dataFinal) {

    let m1 = moment(dataInicial, 'YYYY-MM-DD');
    let m2 = moment(dataFinal, 'YYYY-MM-DD');
    return moment.preciseDiff(m1, m2);
}

const showLoading = function () {
    swal({
        title: 'Carregando',
        allowEscapeKey: false,
        allowOutsideClick: false,
        timer: 8000,
        onOpen: () => {
            swal.showLoading();
        }
    }).then(
        () => { },
        (dismiss) => {
            if (dismiss === 'timer') {
                swal({
                    title: 'Finished!',
                    type: 'success',
                    timer: 2000,
                    showConfirmButton: false
                })
            }
        }
    )
};

$("#link-leads").click(function (event) {
    event.preventDefault()
    if (userPermissions.includes('leads.index')) {
        window.open('https://app.leadmark.com.br/login', "_blank")
    } else {
        $("#modal-leads").modal("show");
    }

})

const setCookie = function (cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

$(".icones").click(function () {
    $("#comentar").slideDown()
    $("#comentar").removeClass('hide')
})
const salvaAvaliacao = function (callback = null) {
    var data = $("#form-avaliacao").serialize();
    // console.log(data)
    $.post(baseUrl + 'avaliacaos/add', data)
        .then(() => {
            console.log("Salvou!")
            if (typeof callback === 'function') {
                callback();
            }
        })
}

$("#form-avaliacao .icones").click(function () {
    $("#form-avaliacao #nota").val($(this).attr('nota'))
    salvaAvaliacao()
});

$("#form-avaliacao input[type=radio]").click(() => { salvaAvaliacao() });
$("#form-avaliacao #comentario").focusout(() => {
    salvaAvaliacao($("#comentar").slideUp())
});

function filtra(trigger, action) {
    var nova = $("#nova").val();
    var estado = $("#estado").val();
    var operadora = $("#operadora-id").val();

    switch (trigger) {
        case "nova":
            console.log('nova:' + nova)
            if (nova) {
                // $("#estado").removeAttr('disabled')
                window.location.href = `${baseUrl}${action}/index/${nova}/${estado}`;
            } else {
                $("#estado").attr('disabled')
                $("#estado").val('')
            }
            break;
        case "estado":
            console.log('nova: ' + nova + ', estado: ' + estado)
            if (nova && estado)
                window.location.href = `${baseUrl}${action}/index/${nova}/${estado}`;
            break;
        case "operadora":
            console.log('nova: ' + nova + ', estado: ' + estado + ', operadora: ' + operadora)
            if (nova && estado && operadora)
                window.location.href = `${baseUrl}${action}/index/${nova}/${estado}/${operadora}`;
            break;
    }

}
