(function(namespace, $) {
    "use strict";

    var AppNavigation = function() {
        // Create reference to this instance
        var o = this;
        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });

    };
    var p = AppNavigation.prototype;

    // =========================================================================
    // MEMBERS
    // =========================================================================

    // Constant
    AppNavigation.MENU_MAXIMIZED = 1;
    AppNavigation.MENU_COLLAPSED = 2;
    AppNavigation.MENU_HIDDEN = 3;

    // Private
    p._lastOpened = null;

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        this._enableEvents();

    };

    // =========================================================================
    // HANDLE AJAX LOAD
    // =========================================================================

    p.ajaxLoad = function(link, destino, callback) {

        // VERIFICA SE O LINK TEM UMA URL DEFINIDA PARA SER REQUISITADA
        // CASO CONTRÁRIO NÃO CONTINUA O PROCESSO

        if (typeof link.attr('href') === 'undefined' || link.attr('href') == '#') {
            return false;
        } else {

            if (baseSub !== '/') {
                link.attr('href', link.attr('href').replace(baseSub, ''));
            }
            if (link.attr('href')[0] === '/') {
                link.attr('href', link.attr('href').replace('/', ''));
            }
        }

        // INSTANCIA VARIÁVEIS
        var history = (typeof link.attr('history') !== 'undefined') ? location.href.split('#')[0] + link.attr('history') : link.attr('href');
        var request = link.attr('requestJS');
        var modal = link.attr('modal');
        var destino = $(destino);
        p._removeClassMenuActive()
        link.addClass('menu_active');

        // VERIFICA O TIPO DE CARREGAMENTO, SE MODAL ABRI O 
        if (typeof link.attr('modal') !== 'undefined') {
            var modalObject = $('#nivel5');
            var url = link.attr('href');
            window.materialadmin.AppForm.loadModal(modalObject, url, '500px', function() {
                // CARREGA JS REQUISITADO NO MENU
                p.requestJS(modalObject, request);
            });
            return false;
        }

        // REMOVE A PÁGINA ATUAL E EXIBE MENSAGEM DE CARREGAMENTO DE PÁGINA
        // p.carregando(destino);

        // EXIBE PRELOADER
        $("#pre-loader").show()

        // CARREGA A PÁGINA VIA AJAX
        destino.load(baseUrl + link.attr('href'), function(page, textStatus, jqXHR) {

            if (jqXHR.status == 200) {
                // ALTERA O ENDEREÇO DA BARRA DE FERRAMENTAS                
                p.changeTitle(history, link);

                // CARREGA JS REQUISITADO NO MENU
                p.requestJS($(page), request);

                // OCULTA PRELOADER
                $("#pre-loader").hide()

                if(!!callback){
                    callback()
                }

            }
        });

    };

    p.changeTitle = function(history, link) {
        window.history.pushState({ url: "" + history + "" }, link.attr('title'), baseUrl + history);
        $('html, body').scrollTop(0);
    }

    p._removeClassMenuActive = function() {
        $('a.linkAjax').removeClass('menu_active');
    }

    p.requestJS = function(pageLoaded, request) {
        // VERIFICA SE TEVE PERMISSÃO DE ACESSO
        if (!pageLoaded.find('#AppSisConfiguracoes').length) {
            // CARREGA SCRIPTS REQUISITADOS NO MENU
            if (request !== 'undefined') {
                window.materialadmin.App.load(request);
            }
        } else {
            // CARREGA O SCRIPT RESTRITO
            window.materialadmin.App.load('AppSisConfiguracoes');
        }
    }

    p.carregando = function(element) {
        // SE EXISTE SECTION ACIMA NÃO USE O SECTION
        if (element.closest('section').length > 0) {
            element.html('<div class="alert alert-callout alert-info text-center" role="alert"><strong>Carregando...</strong> <i class="fa fa-spinner fa-spin"></i></div>');
        } else {
            element.html('<section><div class="section-body alert alert-callout alert-info text-center" role="alert"><strong>Carregando...</strong> <i class="fa fa-spinner fa-spin"></i></div></section>');
        }
    }

    // =========================================================================
    // EVENTS
    // =========================================================================

    // events
    p._enableEvents = function() {
        var o = this;
        // Menu events
        $('#navbar-collapse-menu-2').on('click', 'a', function(e) {
            if ($(this).attr('href') == '/app/users/logout') {
                if (!confirm('Deseja realmente sair do sistema ?')) {
                    return false;
                }
            } else if (!$(this).hasClass('link-externo')) {
                o._handleMenuLinkClick(e);
            }
        });

    };

    // =========================================================================
    // MAIN BAR
    // =========================================================================
    p._handleMenuLinkClick = function(e) {
        // Prevent the link from firing
        e.preventDefault();
        // Get correct object "a"
        var link = (e.target.tagName != 'A') ? $(e.target).parents('a') : $(e.target);
        var path = '#content';

        // Remove acive class
        $('#navbar-collapse-menu-2 li').removeClass('active');

        p._invalidateMenu(link);

        // Load page by ajax
        p.ajaxLoad(link, path);
    };


    // ========================================================================
    // UTILS
    // =========================================================================

    p._invalidateMenu = function(item) {
        // PEGA TODOS OS LI DO MENU
        var menu_li = $('#navbar-collapse-menu-2 li');

        // SE O ITEM É UM LINK OU UM A PASTA  
        // SE FOR UMA PASTA INTERROMPI O PROCESSO
        if (item.parent('li').hasClass('gui-folder')) {
            return false;
        }

        // REMOVE A CLASSE ACTIVE 
        menu_li.removeClass('active');

        // ADICIONA A CLASSE ACTIVE NO LINK CLICADO
        item.parents('li:not(.gui-folder)').addClass('active');

        // Trigger event
        $('#navbar-collapse-menu-2').triggerHandler('ready');

    };

    p.getMenuState = function() {
        // By using the CSS properties, we can attach 
        // states to CSS properties and therefor control states in CSS
        var matrix = $('#menubar').css("transform");
        var values = (matrix) ? matrix.match(/-?[\d\.]+/g) : null;

        var menuState = AppNavigation.MENU_MAXIMIZED;
        if (values === null) {
            if ($('#menubar').width() <= 100) {
                menuState = AppNavigation.MENU_COLLAPSED;
            } else {
                menuState = AppNavigation.MENU_MAXIMIZED;
            }
        } else {
            if (values[4] === '0') {
                menuState = AppNavigation.MENU_MAXIMIZED;
            } else {
                menuState = AppNavigation.MENU_HIDDEN;
            }
        }

        return menuState;
    };

    //FUNÇÃO AINDA ESTÁ EM TESTE
    p.ajaxPagination = function(element, fields = null, callBack) {
   
        $('.pagination a').click(function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                url,
                type: 'GET',
                data: fields,
                dataType: 'html',
                beforeSend: function () {
                    p.carregando(element);
                },
                success: function (data) {
                    // $(element).html(data);
                    element.html($(data).find('#' + element.attr('id') + ' >'));
                    if (typeof callBack !== "undefined") {
                        callBack();
                    }

                    p.ajaxPagination(element, fields, callBack);
                }
            })
        })
    };

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppNavigation = new AppNavigation;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):