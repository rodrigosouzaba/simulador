(function($) {
    var arrVerPer = [];
    for (var usPerm of userPermissions) {
        if (arrVerPer[usPerm.split('.')[0]] == undefined) {
            arrVerPer[usPerm.split('.')[0]] = [usPerm.split('.')[1]];
        } else {
            arrVerPer[usPerm.split('.')[0]].push(usPerm.split('.')[1]);
        }
    }

    $(document).find('[sid]').each(function(i, e) {
        var modulo = $(e).attr('sid').split('.')[0];
        var action = $(e).attr('sid').split('.')[1];
        if (!arrVerPer[modulo]) {
            if ($(e).parent()[0].localName == 'li') {
                $(e).parent().remove();
            } else {
                $(e).remove();
            }
        } else if (arrVerPer[modulo].indexOf(action) == -1) {
            if ($(e).parent()[0].localName == 'li') {
                $(e).parent().remove();
            } else {
                $(e).remove();
            }
        }
    });

    // $('.dropdown-menu').each(function(i, e) {
    //     if ($(e).find('li').length == 1 || $(e).find('li').length == 0) {
    //         $(e).parent().remove();
    //     }
    // });

    // //VALIDA SE EXISTE HEADER SOBRE HEADER
    // $(".sidebar-menu > li").each(function(i, e) {
    //     if ($(e).hasClass("header")) {
    //         var proximoli = $(".sidebar-menu > li")[i + 1];
    //         if ($(proximoli).hasClass('header') || $(proximoli).length == 0) {
    //             $(e).hide();
    //         }

    //     }
    // })
});