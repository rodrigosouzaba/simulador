(function(namespace, $) {
    "use strict";

    var AppAreasComercializacoes = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppAreasComercializacoes.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppAreasComercializacoes.objectId = '#AppAreasComercializacoes';
    AppAreasComercializacoes.modalFormId = '#nivel1';
    AppAreasComercializacoes.controller = 'areasComercializacoes';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function() {
        $(AppAreasComercializacoes.objectId+' #cadastrarAreasComecializacao').click(function() {
            p._loadFormAreasComecializacao();
        });

        $(AppAreasComercializacoes.objectId+' #pesquisarAreasComecializacao').submit(function() {
            p._loadConsAreasComecializacao();
            return false;
        });
    };

    p._habilitaBotoesConsulta = function() {
        $(AppAreasComercializacoes.objectId+' .btnClonar').click(function() {
            p._loadFormAreasComecializacao($(this).attr('id'), true);
        });

        $(AppAreasComercializacoes.objectId+' .btnEditar').click(function() {
            p._loadFormAreasComecializacao($(this).attr('id'));
        });

        $(AppAreasComercializacoes.objectId+' .btnDeletar').click(function() {
            var url = baseUrl + AppAreasComercializacoes.controller +'/delete/'+$(this).attr('id');
            window.materialadmin.AppGrid.delete(url, function(){
                p._loadConsAreasComecializacao();
            });
        });
    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsAreasComecializacao = function() {
        // INSTANCIA VARIÁREIS
        var form = $(AppAreasComercializacoes.objectId+' #pesquisarAreasComecializacao');
        var table = $(AppAreasComercializacoes.objectId+' #gridAreasComercializacoes');
        var url = baseUrl + AppAreasComercializacoes.controller;

        window.materialadmin.AppNavigation.carregando(table);

        $.post(url, form.serialize(), function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));

                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    // =========================================================================
    // CARREGA FORMULÁRIOS
    // =========================================================================

    p._loadFormAreasComecializacao = function(id, clonar) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppAreasComercializacoes.modalFormId);

        var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        var url = (typeof id === 'undefined') ? baseURL + 'areasComercializacoes/add' : baseURL + 'areasComercializacoes/' + action + '/' + id;
        var i = 0;

        window.materialadmin.AppForm.loadModal(modalObject, url, '70%', function() {
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                if (window.materialadmin.AppForm.getFormState()){
                    p._loadConsAreasComecializacao();
                }
            });
        });
    };
    
    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppAreasComercializacoes = new AppAreasComercializacoes;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
