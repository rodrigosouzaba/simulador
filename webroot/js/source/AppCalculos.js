(function (namespace, $) {
    "use strict";

    var AppCalculos = function () {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function () {
            o.initialize();
        });
    };

    var p = AppCalculos.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppCalculos.objectId = '#AppCalculos';
    AppCalculos.modalFormId = '#nivel1';
    AppCalculos.controller = 'pfCalculos';


    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function () {
        // INICIALIZA DEPENDÊNCIAS
        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppCalculos.objectId));
        window.materialadmin.AppVendor.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function () {

        $(AppCalculos.objectId + " #ramos").change(function () {
            $("#loader").click();
            $.ajax({
                type: "POST",
                url: baseUrl + AppCalculos.controller + "/filtroRamo/" + $(this).val(),
                success: function (data) {
                    $("#resultado").empty();
                    $("#resultado").append(data);
                    $("#fechar").click();
                }
            });

        });

        $(AppCalculos.objectId + " #pesquisar-calculo").click(function () {
            $("#loader").click();
            $.ajax({
                type: "POST",
                url: baseUrl + "users/pesquisarCalculo/" + $("#num-calculo").val(),
                success: function (data) {
                    $("#resultado").empty();
                    $("#resultado").append(data);
                    $("#fechar").click();
                }
            });

        });

        $(AppCalculos.objectId + " #pesquisar-cliente").click(function () {
            $("#loader").click();
            $.ajax({
                type: "POST",
                url: baseUrl + "users/pesquisarCliente/" + $("#nome-cliente").val(),
                success: function (data) {
                    $("#resultado").empty();
                    $("#resultado").append(data);
                    $("#fechar").click();
                }
            });

        });


        $(AppCalculos.objectId + " #filter-status-id").change(function () {
            $("#loader").click();
            $.ajax({
                type: "POST",
                url: baseUrl + "users/filtroStatus/" + $(this).val(),
                success: function (data) {
                    $("#resultado").empty();
                    $("#resultado").append(data);
                    $("#fechar").click();
                }
            });

        });

        $('#pf-tab').click(function () {
            p._loadPfCalculos();
        });

        $("#pme-tab").click(function () {
            p._loadSimulacoes();
        });

        $("#gerador-tab").click(function () {
            p._loadTabelasGeradas();
        });
    };

    p._habilitaBotoesConsulta = function () {

        $(AppCalculos.objectId + " .alerta-add").click(function () {
            $.ajax({
                type: "GET",
                url: baseUrl + 'alertas/add/' + $(this).attr("ramo") + "/" + $(this).attr("value") + "/true",
                success: function (data) {
                    $("#modal-alerta").empty();
                    $("#modal-alerta").append(data);
                    $("#myModalLabel").text("Comentários e Lembretes");
                    $('#modalLembrete').modal('show');
                    if ($(window).width() >= 800) {
                        $(".modal-dialog").css('width', "900px");
                    }
                }
            });
        });

        $(AppCalculos.objectId + " .mudar-status").change(function () {
            var ramo = null;
            switch ($(this).attr("ramo")) {
                case "SPF":
                    ramo = "pf-calculos";
                    break;
                case "SPJ":
                    ramo = "simulacoes";
                    break;
                case "OPF":
                    ramo = "odonto-calculos";
                    break;
                case "OPJ":
                    ramo = "odonto-calculos";
                    break;
            }

            var status = (this).value;

            $.ajax({
                type: "POST",
                url: baseUrl + ramo + "/alterar-status/" + $(this).attr("calculo_id") + "/" + $(this).val(),
                success: function (data) {
                    if (status == 1 || status == 2 || status == 3) {
                        $('#modalLembrete').modal('show');
                    }
                }
            });
            $.ajax({
                type: "GET",
                url: baseUrl + "alertas/add/" + $(this).attr("ramo") + "/" + $(this).attr("calculo_id"),
                success: function (data) {
                    $('#modal-alerta').empty();
                    $('#modal-alerta').append(data);
                }
            });
        });

        $(AppCalculos.objectId + " .btnViewCalculo").click(function () {
            p._loadViewPfCalulo($(this).attr('calculo_id'));
        });

        $(AppCalculos.objectId + " .btnViewCalculoPME").click(function () {
            p._loadViewPMECalulo($(this).attr('calculo_id'));
        });

        $(AppCalculos.objectId + " .editar-calculo").click(function () {
            p._loadEditPfCalculo($(this).attr('calculo_id'));
        });

        $(AppCalculos.objectId + " .btnEditCalculoPME").click(function () {
            p._loadEditPMECalculo($(this).attr('calculo_id'));
        });

        $(AppCalculos.objectId + " .btnViewCalculoOdonto").click(function () {
            window.materialadmin.App.load('AppOdontoCalculos');
            window.materialadmin.AppOdontoCalculos._loadViewOdontoCalulo($(this).attr('calculo_id'));
        });

    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsCalculos = function () {
        // INSTANCIA VARIÁREIS
        var form = $(AppCalculos.objectId + ' #pesquisarUser');
        var table = $(AppCalculos.objectId + ' #gridUser');
        var url = baseUrl + 'users/index';

        window.materialadmin.AppNavigation.carregando(table);

        $.post(url, form.serialize(), function (html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));

                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    p._loadPfCalculos = function () {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + "pfCalculos/add";

        window.materialadmin.AppNavigation.carregando(pagina);

        pagina.load(url, function (html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
                p._calculaIdade();

                var submit = $('form').find('button[type="submit"]');

                $('form#calculo').submit(function (e) {
                    submit.button('loading');
                    $.post($(this).attr('action'), $('form#calculo').serialize(), function (result, textStatus, jqXHR) {
                        if (jqXHR.status == 200) {
                            if (result.ok) {
                                toastr.success(result.msg);
                                p._loadViewPfCalulo(result.id);
                            } else {
                                toastr.error(result.msg);
                            }
                            submit.button('reset');
                        }
                    }, 'json');

                    e.preventDefault();
                });

            }
        }, 'html');
    };

    p._loadSimulacoes = function () {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + "simulacoes/add";

        window.materialadmin.AppNavigation.carregando(pagina);

        pagina.load(url, function (html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
                p._calculaIdade();

                var submit = $('form').find('button[type="submit"]');

                $('form#simulacao').submit(function (e) {
                    submit.button('loading');
                    $.post($(this).attr('action'), $('form#simulacao').serialize(), function (result, textStatus, jqXHR) {
                        if (jqXHR.status == 200) {
                            if (result.ok) {
                                toastr.success(result.msg);
                                p._loadViewPMECalulo(result.id);
                            } else {
                                toastr.error(result.msg);
                            }
                            submit.button('reset');
                        }
                    }, 'json');

                    e.preventDefault();
                });

            }
        }, 'html');
    };

    p._loadTabelasGeradas = function () {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + "tabelasGeradas/gerador";

        window.materialadmin.AppNavigation.carregando(pagina);

        pagina.load(url, function (html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {

                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    p._loadViewPfCalulo = function (id) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppCalculos.modalFormId);
        var url = baseUrl + 'pfCalculos/visualizarCotacao/' + id;

        window.materialadmin.AppForm.loadModal(modalObject, url, '90%', function () {
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function () {
                if (window.materialadmin.AppForm.getFormState()) {

                }
            });

            $('[data-toggle="tooltip"]').tooltip();
            $('.todas').click(function () {
                $('.' + (this).value).prop('checked', this.checked);
            });

            modalObject.find('button[type=submit]').text('Simular');

            // bloqueia o botao de visualizar cotacao caso o mesmo nao tenha selecionado nenhuma opcao de calculo
            modalObject.find('div.listaCalculos input[type=checkbox]').on('click', function () {
                if (modalObject.find('div.listaCalculos input[type=checkbox]').is(':checked')) {
                    modalObject.find('#btnVisualizarCotacao').attr('disabled', false);
                } else {
                    modalObject.find('#btnVisualizarCotacao').attr('disabled', true);
                }
            });

            var resultado = modalObject.find('#resultado');
            var simulacao_id = new FormData(document.getElementById("operadoras")).get('simulacao_id')
            var url2 = baseUrl + 'pfCalculos/visualizarCotacao/' + simulacao_id;
            modalObject.find(".filtroSimulacao").change(function () {

                window.materialadmin.AppNavigation.carregando(resultado);

                $.post(url2, $("#operadoras").serialize(), function (html, textStatus, jqXHR) {
                    if (jqXHR.status == 200) {
                        // RECARREGA FORMULÁRIO
                        resultado.html(html);
                    }
                }, 'html');
            });

            // Editar Cotacao
            modalObject.find("#btnModalEdit").click(function () {
                // $("#modalProcessando").modal("show");
                p._loadEditPfCalculo($(this).attr('simulacao_id'));

            });


        });
    };

    p._loadViewPMECalulo = function (id) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppCalculos.modalFormId);
        var url = baseUrl + 'simulacoes/tabelas/' + id;

        window.materialadmin.AppForm.loadModal(modalObject, url, '90%', function () {
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function () {
                if (window.materialadmin.AppForm.getFormState()) {

                }
            });

            $('[data-toggle="tooltip"]').tooltip();
            $('.todas').click(function () {
                $('.' + (this).value).prop('checked', this.checked);
            });

            modalObject.find('button[type=submit]').text('Simular');

            // bloqueia o botao de visualizar cotacao caso o mesmo nao tenha selecionado nenhuma opcao de calculo
            modalObject.find('div.listaCalculos input[type=checkbox]').on('click', function () {
                if (modalObject.find('div.listaCalculos input[type=checkbox]').is(':checked')) {
                    modalObject.find('#btnVisualizarCotacao').attr('disabled', false);
                } else {
                    modalObject.find('#btnVisualizarCotacao').attr('disabled', true);
                }
            });

            var resultado = modalObject.find('#resultado');
            var url2 = baseUrl + 'simulacoes/tabelas';
            modalObject.find(".selectFiltro").change(function () {

                var data = $("#operadoras").serialize();
                window.materialadmin.AppNavigation.carregando(resultado);
                $.post(url2, data, function (html, textStatus, jqXHR) {
                    if (jqXHR.status == 200) {
                        // RECARREGA FORMULÁRIO
                        resultado.html(html);
                    }
                }, 'html');
            });

            // Editar Cotacao
            modalObject.find("#btnModalEdit").click(function () {
                // $("#modalProcessando").modal("show");
                p._loadEditPMECalculo($(this).attr('simulacao_id'));
            });


        });
    };

    p._loadEditPfCalculo = function (id) {
        var modalObject2 = $('#nivel2');
        var urlNivel2 = '/pfCalculos/edit/' + id;
        moment.locale('pt-br');

        window.materialadmin.AppForm.loadModal(modalObject2, urlNivel2, '60%', function () {
            modalObject2.off('hide.bs.modal');
            modalObject2.on('hide.bs.modal', function () {
                if (window.materialadmin.AppForm.getFormState()) {
                    p._loadViewPfCalulo(id);
                }
            });

            p._calculaIdade();
        });
    };

    p._loadEditPMECalculo = function (id) {
        var modalObject2 = $('#nivel2');
        var urlNivel2 = '/simulacoes/edit/' + id;
        moment.locale('pt-br');

        window.materialadmin.AppForm.loadModal(modalObject2, urlNivel2, '60%', function () {
            modalObject2.off('hide.bs.modal');
            modalObject2.on('hide.bs.modal', function () {
                if (window.materialadmin.AppForm.getFormState()) {
                    p._loadViewPMECalulo(id);
                }
            });

            p._calculaIdade();

        });
    };

    p._calculaIdade = function () {
        $(".idade").mask("99/99/9999");

        $('#idade').change(function () {

            $("#respostaAniversario").html('');
            var diaNascimento = moment($(this).val(), "DD/MM/YYYY").format('YYYY-MM-DD');
            var proxAniversario = moment($(this).val(), "DD/MM/YYYY").format('MM-DD');
            proxAniversario = moment().format('Y') + '-' + proxAniversario;

            $("#respostaAniversario").html(calculaIdade(diaNascimento, moment().format('YYYY-MM-DD')));

            if (moment().format('YYYY-MM-DD') > moment(proxAniversario).format('YYYY-MM-DD')) {
                diaNascimento = moment(moment(proxAniversario)).add(1, 'year').format('YYYY-MM-DD');
            } else {
                diaNascimento = moment(moment(proxAniversario)).format('YYYY-MM-DD');
            }

            $("#respostaIdade").html('Falta ' + calculaIdade(moment().format('YYYY-MM-DD'), diaNascimento) + ' para o aniversário');
        });
    };
    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppCalculos = new AppCalculos;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):