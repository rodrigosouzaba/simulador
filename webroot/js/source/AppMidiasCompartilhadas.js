(function (namespace, $) {
    "use strict";

    var AppMidiasCompartilhadas = function () {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function () {
            o.initialize();
        });
    };

    var p = AppMidiasCompartilhadas.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppMidiasCompartilhadas.objectId = '#AppMidiasCompartilhadas';
    AppMidiasCompartilhadas.modalFormId = '#nivel1';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function () {
        // INICIALIZA DEPENDÊNCIAS
        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppMidiasCompartilhadas.objectId));
        window.materialadmin.AppVendor.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function () {

        $(AppMidiasCompartilhadas.objectId + " #addMidiasCompartilhadas").click(function () {
            p._loadForm();
        });

    };

    p._habilitaBotoesConsulta = function () {

        p._gerarThumbnail();

        $(AppMidiasCompartilhadas.objectId + ' .btnEditarMidiaCompartilhada').click(function () {
            p._loadForm($(this).attr('midia_compartilhada_id'))
        });

        $(AppMidiasCompartilhadas.objectId + ' .btnCompartilharMidia').click(function () {
            p._viewComoartilharMidia($(this).attr('midia_compartilhada_id'))
        });

        $(AppMidiasCompartilhadas.objectId + ' .addMidiaForOperadora').click(function () {
            p._loadFormMidiaCompartilhadaForOperadora($(this).attr('midia_compartilhada_id'))
        });

        $(AppMidiasCompartilhadas.objectId + ' .btnEditarMidiaOperadoraEstado').click(function () {
            p._loadFormMidiaCompartilhadaForOperadora($(this).attr('midia_compartilhada_id'), $(this).attr('id'))
        });

        $(AppMidiasCompartilhadas.objectId + ' .btnDeleteCategoria').click(function () {
            if (confirm('A exclusao da categoria excluirá as midias associadas. Confirma a exclusão do registro  ?')) {
                var url = baseUrl + 'midiasCompartilhadas/delete/' + $(this).attr('midia_compartilhada_id');
                window.materialadmin.AppGrid.delete(url, function () {
                    $('#btnModuloMidiaCompartilhada').click();
                });
            }
        });

        $(AppMidiasCompartilhadas.objectId + ' .btnDeleteMidiaOperadoraEstado').click(function () {
            if (confirm('Deseja realmente excluir?')) {
                var url = baseUrl + 'midiasCompartilhadasOperadoras/delete/' + $(this).attr('id');
                window.materialadmin.AppGrid.delete(url, function () {
                    $('#btnModuloMidiaCompartilhada').click();
                });
            }
        });


        $(AppMidiasCompartilhadas.objectId + ' .btnMaterialDivulgacao').click(function (e) {
            e.preventDefault();
            let modalObject = $(AppMidiasCompartilhadas.modalFormId);
            let id = $(this).attr('id');
            let url = baseUrl + 'midiasCompartilhadas/viewMaterial/' + id;
            window.materialadmin.AppForm.loadModal(modalObject, url, '60%', function () {

            });
        });


    };

    // =========================================================================
    // CARREGA CONSULTA
    // =========================================================================

    p._loadConsMidiasCompartilhadas = function () {
        // INSTANCIA VARIÁREIS
        var table = $(AppSisGrupos.objectId);
        var url = baseUrl + 'midiasCompartilhadas';

        window.materialadmin.AppNavigation.carregando(table);

        $.post(url, form.serialize(), function (html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));

                // HABILITA BOTÕES DA CONSULTA
                p._habilitaEventos();
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    p._loadForm = function (id, clonar) {
        // INSTANCIA VARIÁREIS
        var modalObject = $(AppMidiasCompartilhadas.modalFormId);
        var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        var url = (typeof id === 'undefined') ? 'midiasCompartilhadas/add' : 'midiasCompartilhadas/' + action + '/' + id;

        window.materialadmin.AppForm.loadModal(modalObject, url, '80%', function () {
            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function () {
                if (window.materialadmin.AppForm.getFormState()) {
                    $('#btnModuloMidiaCompartilhada').click();
                }
            });

            p._upMidia('midiasCompartilhadas');

            $(".data").mask("99/99/9999", {
                placeholder: "__/__/____"
            });

            $("#exibir-menu").change(function () {

                if ($(this).val() == '1') {
                    $('.input_exibe_menu').attr("disabled", false);
                } else {
                    $('.input_exibe_menu').attr("disabled", true);
                }
            });

            $("#ramo").change(function () {
                var ramo = $(this).val();
                if (ramo != '') {
                    $.ajax({
                        method: "GET",
                        url: baseUrl + `midiasCompartilhadas/findOperadoras/${ramo}`,
                        success: function (data) {
                            $("#operadoras-field").empty()
                            $("#operadoras-field").append(data)
                        }
                    })
                }
            });

        });
    };

    p._loadFormMidiaCompartilhadaForOperadora = function (midia_compartilhada_id, id, clonar) {
        // INSTANCIA VARIÁREIS
        var modalObject = $(AppMidiasCompartilhadas.modalFormId);
        var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        var url = (typeof id === 'undefined') ? 'midiasCompartilhadasOperadoras/add/' + midia_compartilhada_id : 'midiasCompartilhadasOperadoras/' + action + '/' + midia_compartilhada_id + '/' + id;

        window.materialadmin.AppForm.loadModal(modalObject, url, '80%', function () {
            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function () {
                if (window.materialadmin.AppForm.getFormState()) {
                    $('#btnModuloMidiaCompartilhada').click();
                }
            });

            p._upMidia('midiasCompartilhadasOperadoras');

            $(".data").mask("99/99/9999", {
                placeholder: "__/__/____"
            });

            $("#ramo").change(function () {
                var ramo = $(this).val();
                if (ramo != '') {
                    $.ajax({
                        method: "GET",
                        url: baseUrl + `midiasCompartilhadas/findOperadoras/${ramo}`,
                        success: function (data) {
                            $("#operadoras-field").empty()
                            $("#operadoras-field").append(data)
                        }
                    })
                }
            });
        });
    };

    p._loadMidiasCompartilhadas = function () {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + "midiasCompartilhadas/gerador";

        window.materialadmin.AppNavigation.carregando(pagina);

        $.get(url, function (html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                pagina.html(html);
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    p._viewComoartilharMidia = function (id) {
        // INSTANCIA VARIÁREIS
        var modalObject = $(AppMidiasCompartilhadas.modalFormId);
        var url = baseUrl + `midiasCompartilhadas/compartilhar/${id}`;

        // var imgLogoCorretor = new Image();
        // var srcLogoCorretor = urlOrigin + $('#photoUser').attr('src');
        // imgLogoCorretor.src = srcLogoCorretor;

        // window.imgLogoCorretor = imgLogoCorretor;

        window.materialadmin.AppForm.loadModal(modalObject, url, '30%', function () {
            $('.modal-content .card').css("border-width", '0px')
            var headerModal = $('div.card-head');
            var btnClose = $('button.close');
            headerModal.removeClass('style-primary').addClass('style-custom-1');
            headerModal.css("height", "40px");
            btnClose.css({ "color": "#fff", "padding-top": "3px", "padding-right": "10px" });
      
            var obj = modalObject.find('.imagens');
            var canvas = document.getElementById(`canvas-${id}`);
            var context = canvas.getContext("2d");
            var path = $(obj).attr("path");
            var img = new Image();

            canvas.height = 1230;
            canvas.width = 1080;

            img.src = path;
            img.crossOrigin = "anonymous";

            img.onload = function () {
                context.drawImage(
                    this, //imagem a ser utilizada
                    0, //X do inicio do recorte da imagem
                    0, //Y do inicio do recorte da imagem
                    canvas.width, //Até onde vai o corte da imagem em X
                    canvas.height, //Até onde vai o corte da imagem em Y,
                );

                // Marca d'agua caso o usuario nao tenha perfil necessario
                // let perfis = objPerfis.split(',');
                // if (perfis.filter(res => (res == 'midias-compartilhadas')).length == 0) {
                p._addMidiaInformacoes(context, 20, 1130)
                // }

                obj.remove();
            };

            $('.download').click(function () {
                // let tempCanvas = document.createElement('canvas');
                // tempCanvas.width = 1080;
                // tempCanvas.height = 1230;

                // const tempContext = tempCanvas.getContext('2d');

                // tempContext.drawImage(
                //     img,
                //     0,
                //     0,
                //     tempCanvas.width,
                //     tempCanvas.height
                // );

                // p._addMidiaInformacoes(tempContext, 20, 1130)

                // this.href = tempCanvas.toDataURL();
                // this.download = $(this).attr('nome');

                this.href = canvas.toDataURL();
                this.download = $(this).attr('nome');
            })

            $('.email').click(function () {
                window.open(`mailto:?subject=Corretor Parceiro&body=Conheça nosso portifólio`);
            });

            $('.facebook').click(function () {

                let nome = $(this).attr('nome');
                // Define a imagem como valor a ser enviado:
                var params = "image=" + document.getElementById(`canvas-${id}`).toDataURL("image/png");;
                http.open("GET", "https://www.facebook.com/sharer/sharer.php?u=https://corretorparceiro.com.br", true);
                http.setRequestHeader("Content-type", "text/plain");
                http.send(params);

            });

            $('.whatsapp').click(function () {
                let nome = $(this).attr('nome');
                this.href = "https://www.facebook.com/sharer/sharer.php?u=" + document.getElementById(`canvas-${id}`).toDataURL("image/png");
                this.download = nome;
            });

            $('.twitter').click(function () {
                let nome = $(this).attr('nome');
                this.href = document.getElementById(`canvas-${id}`).toDataURL("image/png");
                this.download = nome;
            });

        });
    };

    p._backgroundCanvasCorretor = function (ctx, cw) {

        let canvas = document.querySelector('.canva-preview');
        var w = canvas.clientHeight, // image height
            h = canvas.clientHeight, // image width
            rh = h / w * cw, // ratio height
            ch = rh, // canvas height
            mh = cw * 3.2; // canvas max height
        if (rh > mh) {
            ch = mh;
        }
        var logoCorretor = new Image();
        logoCorretor.src = $('.custom-logo').attr('src');
        ctx.globalAlpha = 0.4;
        ctx.drawImage(logoCorretor, (w / 2) - 120, (h / 2) - 100, cw + 100, rh - 60);
    }

    p._backgroundPrevia = function (ctx) {
        var previaCorretor = new Image();
        var src = urlOrigin + $('#imgPrevia').attr('src');
        previaCorretor.src = src;
        console.log('src', src);

        previaCorretor.onload = function () {
            ctx.drawImage(previaCorretor, 0, 0);
        }

    }

    p._logoParceiroOuLogoCorretor = function (ctx, x = 100, y = 970) {
    
        var img = new Image();
        var src = urlOrigin + $('#photoUser').attr('src');
        img.src = src;

        img.onload = function () {
            ctx.drawImage(img, x, y, img.width * (70 / img.height), 70)
        }
    }

    p._gerarThumbnail = function () {

        if ($('.imagens-preview').length > 0) {
            $('.imagens-preview').each((index, obj) => {

                var midia = $(obj).attr("midia");
                var canvas = document.getElementById(`canvas-preview-${midia}`);
                // canvas.fillStyle = 'grey';
                var context = canvas.getContext("2d");
                var path = $(obj).attr("path");

                var img = new Image();
                img.src = path;
                img.crossOrigin = "anonymous";
                img.onload = function () {
                    context.drawImage(
                        this, //imagem a ser utilizada
                        0, //X do inicio do recorte da imagem
                        0, //Y do inicio do recorte da imagem
                        canvas.width, //Até onde vai o corte da imagem em X
                        canvas.height, //Até onde vai o corte da imagem em Y
                    );
                    p._backgroundPrevia(context);
                };

                // img.width = 300
                $(obj).remove();

            })
        }
    }

    p._upMidia = function (model) {
        // INSTANCIA VARIÁREIS
        var myDropzone = new Dropzone("#imagem", {
            url: baseUrl + model + '/imagens'
        });

        myDropzone.options.acceptedFiles = 'image/*';
        myDropzone.options.maxFiles = 1;
        myDropzone.options.autoDiscover = true;
        myDropzone.options.maxFilesize = 5;

        myDropzone.on("addedfile", function (file) {
            $(".icon").removeClass("md-photo-camera");
            $(".title-dropzone").hide();
            $("#previewImg").remove();

            file.previewElement.append();
        });

        myDropzone.on("success", function (file, response) {
            console.info(file);
            if (response.ok) {
                $('#nome').val(`${response.name}`)
                $('#path').val(`${response.path}`)
            }
            // response.
            //VERIFICA E REMOVE CASO JA EXISTA UMA IMAGEM ADICIONADA
            // let prevImg = $("#postImg").val();
            // if(prevImg != ""){
            //     prevImg = prevImg.split('/').join('-');
            //     p._cleanImages("img/"+prevImg);
            // }

            // $(".removeImg").attr("id","img/"+response);
            // $("#imgPrev").attr("src","img/"+response);
            // $("#postImg").val(response);
            // $(".removeImg").show();
        });
    };


    p._addNome = function (context, x = 20, y = 350) {
        context.fillText($('#userNome').val(), x, y);
    }

    p._addCelular = function (context, x = 20, y = 367) {
        context.fillText(`Celular: ${$('#userCelular').val()}`, x, y);
    }

    p._addWhatsapp = function (context, x = 20, y = 384) {
        context.fillText("Whatsapp: " + $('#userWhatsapp').val(), x, y);
    }

    p._addEmail = function (context, x = 20, y = 401) {
        context.fillText("Email: " + $('#userEmail').val(), x, y);
    }

    p._addValidade = function (context, x = 20, y = 418) {
        context.fillText(`Validade: ${$('#validadeMidia').val()}`, x, y);
    }


    p._addMidiaInformacoes = function (context, x = 100, y = 940) {
        context.lineWidth = 1;
        context.fillStyle = "#808080";
        context.lineStyle = "#808080";
        context.font = "bold 13px sans-serif";
        p._addNome(context, (x * 11), y);
        context.font = "13px sans-serif";
        p._addCelular(context, (x * 11), y + (17));
        p._addWhatsapp(context, (x * 11), y + (17 * 2));
        p._addEmail(context, (x * 11), y + (17 * 3));
        p._addValidade(context, (x * 11), y + (17 * 4));
        p._logoParceiroOuLogoCorretor(context, x, y);
    }

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppMidiasCompartilhadas = new AppMidiasCompartilhadas;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
