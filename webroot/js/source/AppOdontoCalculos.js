(function(namespace, $) {
    "use strict";

    var AppOdontoCalculos = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppOdontoCalculos.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppOdontoCalculos.objectId = '#AppOdontoCalculos';
    AppOdontoCalculos.modalFormId = '#nivel1';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        // INICIALIZA DEPENDÊNCIAS
        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppOdontoCalculos.objectId));
        window.materialadmin.AppVendor.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function() {
        $("#odonto-pf-tab,#odonto-gerador-tab").height($("#odonto-pme-tab").height());

        $("#odonto-pf-tab").click(function() {
            p._loadPfCalculos();
        });

        $("#odonto-pme-tab").click(function() {
            p._loadPJCalculos();
        });

        $("#odonto-gerador-tab").click(function() {
            p._loadTabelasGeradas();
        });

    };

    p._habilitaBotoesConsulta = function() {

        $(AppOdontoCalculos.objectId + " .alerta-add").click(function() {
            $.ajax({
                type: "GET",
                url: baseUrl + '/alertas/add/' + $(this).attr("ramo") + "/" + $(this).attr("value") + "/true",
                success: function(data) {
                    $("#modal-alerta").empty();
                    $("#modal-alerta").append(data);
                    $("#myModalLabel").text("Comentários e Lembretes");
                    $('#modalLembrete').modal('show');
                    if ($(window).width() >= 800) {
                        $(".modal-dialog").css('width', "900px");
                    }
                }
            });
        });

        $(AppOdontoCalculos.objectId + " .editar-calculo").click(function() {
            $.ajax({
                type: "GET",
                url: baseUrl + 'odontoCalculos/edit/' + $(this).attr("calculo_id"),
                beforeSend: function() {
                    $("#modalProcessando").modal("show");
                },
                success: function(data) {
                    $("#modal-alerta").empty();
                    $("#modal-alerta").append(data);
                    $("#myModalLabel").text("Editar Cálculo");
                    if ($(window).width() >= 800) {
                        $(".modal-dialog").css('width', "90%");
                    }
                    $('#modalLembrete').modal('show');
                    $("#modalProcessando").modal("hide")
                }
            });
        });

        $(AppOdontoCalculos.objectId + " .btnViewCalculo").click(function() {
            p._loadViewPfCalulo($(this).attr('calculo_id'))
        });
    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadPfCalculos = function() {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + 'odontoCalculos/add/PF';

        window.materialadmin.AppNavigation.carregando(pagina);

        pagina.load(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // HABILITA BOTÕES DA CONSULTA
                var submit = $('form').find('button[type="submit"]');

                $('form#odonto-calculo').submit(function(e) {
                    submit.button('loading');
                    $.post($(this).attr('action'), $('form#odonto-calculo').serialize(), function(result, textStatus, jqXHR) {
                        if (jqXHR.status == 200) {
                            if (result.ok) {
                                toastr.success(result.msg);
                                p._loadViewOdontoCalulo(result.id);
                            } else {
                                toastr.error(result.msg);
                            }
                            submit.button('reset');
                        }
                    }, 'json');

                    e.preventDefault();
                });

            }
        }, 'html');

    };

    p._loadPJCalculos = function() {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + 'odontoCalculos/add/PJ';

        window.materialadmin.AppNavigation.carregando(pagina);

        pagina.load(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // HABILITA BOTÕES DA CONSULTA
                var submit = $('form').find('button[type="submit"]');

                $('form#odonto-calculo').submit(function(e) {
                    submit.button('loading');
                    $.post($(this).attr('action'), $('form#odonto-calculo').serialize(), function(result, textStatus, jqXHR) {
                        if (jqXHR.status == 200) {
                            if (result.ok) {
                                toastr.success(result.msg);
                                p._loadViewOdontoCalulo(result.id);
                            } else {
                                toastr.error(result.msg);
                            }
                            submit.button('reset');
                        }
                    }, 'json');

                    e.preventDefault();
                });

            }
        }, 'html');
    };

    p._loadTabelasGeradas = function() {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + "tabelasGeradas/gerador";

        window.materialadmin.AppNavigation.carregando(pagina);

        $.get(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                pagina.html(html);
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    p._loadViewOdontoCalulo = function(id) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppOdontoCalculos.modalFormId);
        var url = baseUrl + '/odontoCalculos/view/' + id;

        window.materialadmin.AppForm.loadModal(modalObject, url, '90%', function() {

            $('[data-toggle="tooltip"]').tooltip();
            $('.todas').click(function() {
                $('.' + (this).value).prop('checked', this.checked);
            });

            modalObject.find('button[type=submit]').text('Simular');

            // bloqueia o botao de visualizar cotacao caso o mesmo nao tenha selecionado nenhuma opcao de calculo
            $('div#resultado input[type=checkbox]').on('click', function() {
                if ($('div#resultado input[type=checkbox]').is(':checked')) {
                    $('#btnVisualizarCotacao').attr('disabled', false);
                } else {
                    $('#btnVisualizarCotacao').attr('disabled', true);
                }
            });

            // Editar Cotacao
            modalObject.find("#btnModalEdit").click(function() {
                // $("#modalProcessando").modal("show");
                p._loadEditOdontoCalculo($(this).attr('odonto_id'));
            });


        });
    };

    p._loadEditOdontoCalculo = function(id) {
        var modalObject2 = $('#nivel2');
        var urlNivel2 = '/odontoCalculos/edit/' + id;

        window.materialadmin.AppForm.loadModal(modalObject2, urlNivel2, '60%', function() {
            modalObject2.off('hide.bs.modal');
            modalObject2.on('hide.bs.modal', function() {
                if (window.materialadmin.AppForm.getFormState()) {
                    p._loadViewOdontoCalulo(id);
                }
            });
        });
    };
    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppOdontoCalculos = new AppOdontoCalculos;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):