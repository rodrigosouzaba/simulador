(function(namespace, $) {
    "use strict";

    var AppOdontos = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppOdontos.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppOdontos.objectId = '#AppOdontos';
    AppOdontos.modalFormId = '#nivel1';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        // INICIALIZA DEPENDÊNCIAS
        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppOdontos.objectId));
        window.materialadmin.AppVendor.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function() {

        // $(".linkAjax").click(function() {
        //     p._loadPfCalculos();
        // });

    };

    p._habilitaBotoesConsulta = function() { 

        $(AppOdontos.objectId + " .linkAjax").click(function(e) {
            e.preventDefault();
            p._loadConsulta($(this).attr('href'));
        });


    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsulta = function(urlLocal) {
        // INSTANCIA VARIÁREIS
        var pagina = $('#gridOdonto');
        window.materialadmin.AppNavigation.carregando(pagina);

        pagina.load(urlLocal, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                pagina.html(html);
            }
        }, 'html');
    };

    p._loadPJCalculos = function() {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + 'odontoCalculos/add/PJ';

        window.materialadmin.AppNavigation.carregando(pagina);

        $.get(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                pagina.html(html);
                // HABILITA BOTÕES DA CONSULTA
                // p._habilitaBotoesConsulta();
                
            }
        }, 'html');
    };

    p._loadOdontos = function() {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl +  "Odontos/gerador";

        window.materialadmin.AppNavigation.carregando(pagina);

        $.get(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                pagina.html(html);
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    
    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppOdontos = new AppOdontos;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):