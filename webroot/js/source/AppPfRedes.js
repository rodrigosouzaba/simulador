(function (namespace, $) {
    "use strict";

    var AppPfRedes = function () {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function () {
       
            o.initialize();
        });
    };

    var p = AppPfRedes.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppPfRedes.objectId = '#AppPfRedes';
    AppPfRedes.modalFormId = '#nivel1';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function () {
        // INICIALIZA DEPENDÊNCIAS
        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppPfRedes.objectId));
        window.materialadmin.AppVendor.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
        p._habilitaPaginate();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================
    p._habilitaBotoes = function () {

        p._habilitaEventos();
        p._habilitaBotoesConsulta();
        p._habilitaPaginate();
    }

    p._habilitaEventos = function () {

    };

    p._habilitaBotoesConsulta = function () {

      
    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._habilitaPaginate = function () {

        window.materialadmin.AppNavigation.ajaxPagination(
            $(AppPfRedes.objectId + ' #gridPfRedes'),
            $(AppPfRedes.objectId + ' #pesquisarPfCalculo'),
            () => window.materialadmin.AppPfRedes._habilitaBotoesConsulta()
        );
    };

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppPfRedes = new AppPfRedes;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):