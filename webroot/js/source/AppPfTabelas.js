(function (namespace, $) {
    "use strict";

    var AppPfTabelas = function () {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function () {
            o.initialize();
        });
    };

    var p = AppPfTabelas.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppPfTabelas.objectId = '#AppPfTabelas';
    AppPfTabelas.modalFormId = '#nivel1';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function () {
        // INICIALIZA DEPENDÊNCIAS
        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppPfTabelas.objectId));
        window.materialadmin.AppVendor.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
        p._habilitaPaginate();
    };

    p._habilitaMenu = function () {
        $("#AppPf .linkAjax").click(function (e) {
            e.preventDefault();
            var pagina = $('#selecaoTabelas');
            window.materialadmin.AppNavigation.carregando(pagina);
            window.materialadmin.AppNavigation.ajaxLoad($(this), pagina, () => window.materialadmin.AppPfTabelas._habilitaBotoes());
        });
    };


    // =========================================================================
    // EVENTS
    // =========================================================================
    p._habilitaBotoes = function () {

        p._habilitaEventos();
        p._habilitaBotoesConsulta();
        p._habilitaPaginate();
    }

    p._habilitaEventos = function () {

        $(AppPfTabelas.objectId + " #deleteLote").hide();
        $("#reajustarLote").hide();

        $(AppPfTabelas.objectId + ' #modalTabelas').on('shown.bs.modal', function () {
            $('#myInput').focus()
        });
        $(AppPfTabelas.objectId + ' #modalPrioridade').on('shown.bs.modal', function () {
            $('#myInput').focus()
        });
        $(AppPfTabelas.objectId + " #reajustarVigencia").click(function () {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "pfTabelas/reajusteVigencia/" + $("#selecaoTabelas").serialize(),
                success: function (data) {
                    $("#percentual").empty();
                    $("#percentual").append(data);

                }
            });
        });

        $(AppPfTabelas.objectId + " #gerarTabela").click(function () {
            $.ajax({
                type: "post",
                data: $('#selecaoTabelas').serialize(),
                url: baseUrl + "tabelas/gerarTabela/" + $('#selecaoTabelas').serialize(),
                beforeSend: function () {
                    $('#loader').trigger('click');
                },
                success: function (data) {
                    $('#fechar').trigger('click');
                }
            });
        });

        $(AppPfTabelas.objectId + " .filtros").change(function () {
            let estados = $(AppPfTabelas.objectId + " #pesquisarPfCalculo #estados");
            let operadoras = $(AppPfTabelas.objectId + " #pesquisarPfCalculo #operadora-id");
            let comercializacoes = $(AppPfTabelas.objectId + " #pesquisarPfCalculo #comercializacoes");

            let changer = $(this).attr('id');

            if (changer == 'estados') {
                if ((estados.val() != 'undefined' && estados.val() != '')) {
                    window.location.href = `${baseUrl}pfTabelas/new/${estados.val()}`;
                }
            }

            if (changer == 'operadora-id') {
                if ((estados.val() != 'undefined' && estados.val() != '') && (operadoras.val() != 'undefined' && operadoras.val() != '')) {
                    window.location.href = `${baseUrl}pfTabelas/new/${estados.val()}/${operadoras.val()}`;
                }
            }

            if (changer == 'comercializacoes') {
                if ((estados.val() != 'undefined' && estados.val() != '') && (operadoras.val() != 'undefined' && operadoras.val() != '') && (comercializacoes.val() != 'undefined' && comercializacoes.val() != '')) {
                    window.location.href = `${baseUrl}pfTabelas/new/${estados.val()}/${operadoras.val()}/${comercializacoes.val()}`;
                }
            }
        });

        $(AppPfTabelas.objectId + " #estados_old").change(function () {

            var value = $(this).val();
            if (value != '') {
                let url = baseURL + 'pfTabelas/filtroEstado'
                var element = $("#operadora-id-old");
                $.ajax({
                    type: "POST",
                    url: url,
                    data: { estado: value },
                    dataType: "JSON",
                    beforeSend: function () {
                        element.prop("disabled", true).html("<option value=''>Carregando...</option>");
                    },
                    success: function (response) {
                        element.prop("disabled", true).empty();
                        element.append(`<option value="" selected="selected"> Selecione </option>`);
                        Object.entries(response).forEach(([key, value]) => {
                            element.append(`<option value="${key}">  ${value} </option>`);
                        });
                    },
                    error: function () {
                        toastr.error("Ocorreu um erro interno,tente novamente ou abra um chamado");
                    },
                    complete: function () {
                        element.prop("disabled", false);
                    }
                });
            }

            p._loadConsPfTabelasOld();
        });

        $(AppPfTabelas.objectId + " #operadora-id").change(function () {
            // p._loadConsPfTabelas();
        });

        $(AppPfTabelas.objectId + " #operadora-id-old").change(function () {
            p._loadConsPfTabelasOld();
        });

        $(AppPfTabelas.objectId + " #deleteLote").click(function () {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + "produtos/deleteLote/",
                success: function (data) {
                    window.location = baseUrl + 'produtos/';

                }
            });
        });

        $(AppPfTabelas.objectId + " #btnValorPercentual").click(function () {
            p._loadViewValorPercent();
        });

    };

    p._habilitaBotoesConsulta = function () {

        $(AppPfTabelas.objectId + " .btnEditOld").click(function () {
            p._loadFormPfTabelasOld($(this).attr('id'));
        });

        $(AppPfTabelas.objectId + " .colocarAtlz").click(function () {
            $.ajax({
                type: "get",
                url: baseUrl + "pfTabelas/colocarAtlz/" + $(this).attr('value'),
                beforeSend: function () {
                    $("#loader").click();
                },
                success: function (data) {
                    $('#operadora-id').trigger('change');
                    if ($('#tableOld').length > 0) {
                        p._loadConsPfTabelasOld();
                    }
                }
            });
        });

        $(AppPfTabelas.objectId + " .removerAtlz").click(function () {
            $.ajax({
                type: "post",
                url: baseUrl + "pfTabelas/removerAtlz/" + $(this).attr('value'),
                beforeSend: function () {
                    $("#loader").click();
                },
                success: function (data) {
                    $('#operadora-id').trigger('change');
                    if ($('#tableOld').length > 0) {
                        p._loadConsPfTabelasOld();
                    }
                }
            });
        });

        $(AppPfTabelas.objectId + " .checkHabilita").click(function () {
            $("#deleteLote").show();
            $("#reajustarLote").show();
        });

        $(AppPfTabelas.objectId + " .invalidar").click(function () {
            $.ajax({
                type: "get",
                url: baseUrl + "pfTabelas/invalidar/" + $(this).attr("value"),
                success: function (data) {
                    $('#operadora-id').trigger('change');
                    if ($('#tableOld').length > 0) {
                        p._loadConsPfTabelasOld();
                    }
                }
            });
        });

        $(AppPfTabelas.objectId + " .validar").click(function () {
            $.ajax({
                type: "get",
                url: baseUrl + "pfTabelas/validar/" + $(this).attr("value"),
                success: function (data) {
                    $('#operadora-id').trigger('change');
                    if ($('#tableOld').length > 0) {
                        p._loadConsPfTabelasOld();
                    }
                }
            });
        });

        $(AppPfTabelas.objectId + " .alterarPrioridade").click(function () {
            p._loadFormAlterarPrioridade($(this).attr("value"))
        });
    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsPfTabelas = function () {
        // INSTANCIA VARIÁREIS
        var form = $('#pesquisarPfCalculo');
        var table = $('#gridPfCalculo');
        var url = baseUrl + 'pfTabelas/new';

        window.materialadmin.AppNavigation.carregando(table);

        $.post(url, form.serialize(), function (html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));
                // HABILITA OS BOTOES DA PAGINACAO PARA AJAX
                window.materialadmin.AppNavigation.ajaxPagination(table, form.serialize(), () => window.materialadmin.AppPfTabelas._habilitaBotoesConsulta());
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    p._loadConsPfTabelasOld = function () {
        // INSTANCIA VARIÁREIS
        var form = $('#pesquisarPfCalculo');
        var table = $('#gridPfCalculo');
        var url = baseUrl + 'pfTabelas/index';

        window.materialadmin.AppNavigation.carregando(table);

        $.post(url, form.serialize(), function (html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));
                // HABILITA OS BOTOES DA PAGINACAO PARA AJAX
                window.materialadmin.AppNavigation.ajaxPagination(table, form.serialize(), () => window.materialadmin.AppPfTabelas._habilitaBotoesConsulta());
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };


    p._loadViewValorPercent = function () {
        // INSTANCIA VARIÁREIS
        var modalObject = $(AppPfTabelas.modalFormId);
        var url = baseUrl + "pfTabelas/valorPercentual";
        window.materialadmin.AppForm.loadModal(modalObject, url, '80%', function () {
            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function () {

            });

            modalObject.find("#estado-id").change(function () {

                let url = baseURL + 'pfTabelas/filtroEstado'
                var element = modalObject.find("#operadora-id");
                var value = $(this).val();
                if (value != '') {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: { estado: value },
                        dataType: "JSON",
                        beforeSend: function () {
                            element.prop("disabled", true).html("<option value=''>Carregando...</option>");
                        },
                        success: function (response) {
                            element.prop("disabled", true).empty();
                            element.append(`<option value="" selected="selected"> Selecione </option>`);
                            Object.entries(response).forEach(([key, value]) => {
                                element.append(`<option value="${key}">  ${value} </option>`);
                            });
                        },
                        error: function () {
                            toastr.error("Ocorreu um erro interno,tente novamente ou abra um chamado");
                        },
                        complete: function () {
                            element.prop("disableda", false);
                        }
                    });
                }

            });



            modalObject.find("#buscar").click(function () {
                $("#tabelas").empty();
                $.ajax({
                    type: "POST",
                    url: baseUrl + "pfTabelas/buscaTabela",
                    data: $("#buscarGrupo").serialize(),
                    beforeSend: function () {
                        $("#mini-loader").show()
                    },
                    success: function (data) {
                        $("#mini-loader").hide()
                        $("#tabelas").append(data);
                    }
                });
            });

        });
    };

    p._habilitaPaginate = function () {
        window.materialadmin.AppNavigation.ajaxPagination(
            $(AppPfTabelas.objectId + ' #gridPfCalculo'),
            $(AppPfTabelas.objectId + ' #pesquisarPfCalculo'),
            () => window.materialadmin.AppPfTabelas._habilitaBotoesConsulta()
        );
    };

    // =========================================================================
    // CARREGA FORMULÁRIOS
    // =========================================================================

    p._loadFormPfTabelasOld = function (id, clonar) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppPfTabelas.modalFormId);

        var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        var url = (typeof id === 'undefined') ? baseURL + 'pfTabelas/add' : baseURL + 'pfTabelas/' + action + '/' + id;
        var i = 0;

        window.materialadmin.AppForm.loadModal(modalObject, url, '95%', function () {
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function () {
                if (window.materialadmin.AppForm.getFormState()) {
                    p._loadConsPfTabelasOld();
                }
            });

            $(".data").mask("99/99/9999", {
                placeholder: "__/__/____"
            });
        });
    };

    p._loadFormAlterarPrioridade = function (id) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppPfTabelas.modalFormId);
        var url = baseURL + 'pfTabelas/prioridade/' + id;

        window.materialadmin.AppForm.loadModal(modalObject, url, '40%', function () {
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function () {
                if (window.materialadmin.AppForm.getFormState()) {
                    if ($('#tableOld').length > 0) {
                        p._loadConsPfTabelasOld();
                    } else {
                        p._loadConsPfTabelas();
                    }
                }
            });

        });
    };

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppPfTabelas = new AppPfTabelas;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):