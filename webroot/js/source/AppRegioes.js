(function(namespace, $) {
    "use strict";

    var AppRegioes = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppRegioes.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppRegioes.objectId = '#AppRegioes';
    AppRegioes.modalFormId = '#nivel1';
    AppRegioes.controller = 'regioes';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        
        window.materialadmin.AppForm.initialize($(AppRegioes.objectId));

        p._habilitaEventos();
        p._habilitaBotoesConsulta();
        p._habilitaPaginate()
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaBotoes = function () {
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
        p._habilitaPaginate();
    }

    p._habilitaEventos = function() {
        
        $(AppRegioes.objectId+' #cadastrarRegiao').click(function() {
            // p._loadFormRegiao();
        });

        $(AppRegioes.objectId + " #estados").change(function() {
            p._loadConsRegiao();
        });
    
    };

    p._habilitaBotoesConsulta = function() {
        
        $(AppRegioes.objectId + ' .btnEditar').click(function() {
            // p._loadFormRegiao($(this).attr('id'));
        });

        $(AppRegioes.objectId+' .btnDeletar' + AppRegioes.controller).click(function() {
            var url = baseUrl+ AppRegioes.controller + '/delete/'+$(this).attr('id');
            window.materialadmin.AppGrid.delete(url, function(){
                p._loadConsRegiao();
            });
        });

    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsRegiao = function() {
        // INSTANCIA VARIÁREIS
        var form = $(AppRegioes.objectId+' #pesquisarRegiao');
        var table = $(AppRegioes.objectId+' #selecaoRegioes');
        var url = baseUrl + AppRegioes.controller;

        window.materialadmin.AppNavigation.carregando(table);
        $.post(url, form.serialize(), function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));
                // HABILITA OS BOTOES DA PAGINACAO PARA AJAX
                window.materialadmin.AppNavigation.ajaxPagination(table, form.serialize(), () => window.materialadmin.AppRegioes._habilitaBotoesConsulta());
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    // =========================================================================
    // CARREGA FORMULÁRIOS
    // =========================================================================

    p._loadFormRegiao = function(id, clonar) {
        // CHAMA A FUNÇÃO MODAL
        // var modalObject = $(AppRegioes.modalFormId);

        // var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        // var url = (typeof id === 'undefined') ? baseURL + 'sisGrupos/add' : baseURL + 'sisGrupos/' + action + '/' + id;
        // var i = 0;

        // window.materialadmin.AppForm.loadModal(modalObject, url, '70%', function() {
        //     modalObject.off('hide.bs.modal');
        //     modalObject.on('hide.bs.modal', function() {
        //         if (window.materialadmin.AppForm.getFormState()){
        //             p._loadConsRegiao();
        //         }
        //     });
        // });
    };
    

    p._habilitaPaginate = function() {
        window.materialadmin.AppNavigation.ajaxPagination(
            $(AppRegioes.objectId+' #selecaoRegioes'), 
            $(AppRegioes.objectId+' #pesquisarRegiao'), 
            () => window.materialadmin.AppRegioes._habilitaBotoesConsulta()
        );
    };
    

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppRegioes = new AppRegioes;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
