(function(namespace, $) {
    "use strict";

    var AppSaudePF = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppSaudePF.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppSaudePF.objectId = '#AppPf';
    AppSaudePF.modalFormId = '#nivel1';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        // INICIALIZA DEPENDÊNCIAS
        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppSaudePF.objectId));
        window.materialadmin.AppVendor.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaBotoesConsulta = function() { 

        $(AppSaudePF.objectId + " .linkAjax").click(function(e) {
            e.preventDefault();
            var pagina = $('#gridPf');
            window.materialadmin.AppNavigation.carregando(pagina);
            window.materialadmin.AppNavigation.ajaxLoad($(this), pagina);
        });

    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsulta = function(urlLocal) {
        // // INSTANCIA VARIÁREIS

    };

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppSaudePF = new AppSaudePF;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):