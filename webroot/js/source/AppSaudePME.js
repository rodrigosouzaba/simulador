(function(namespace, $) {
    "use strict";

    var AppSaudePME = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppSaudePME.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppSaudePME.objectId = '#AppPme';
    AppSaudePME.modalFormId = '#nivel1';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        // INICIALIZA DEPENDÊNCIAS
        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppSaudePME.objectId));
        window.materialadmin.AppVendor.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaBotoesConsulta = function() { 

        $(AppSaudePME.objectId + " .linkAjax").click(function(e) {
            e.preventDefault();
            var pagina = $('#gridPme');
            window.materialadmin.AppNavigation.carregando(pagina);
            window.materialadmin.AppNavigation.ajaxLoad($(this), pagina);
            
        });

    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsulta = function(urlLocal) {
        // // INSTANCIA VARIÁREIS

    };

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppSaudePME = new AppSaudePME;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):