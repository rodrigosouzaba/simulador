(function(namespace, $) {
    "use strict";

    var AppSisConfiguracoes = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppSisConfiguracoes.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppSisConfiguracoes.objectId = '#AppSisConfiguracoes';
    AppSisConfiguracoes.modalFormId = '#nivel1';


    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaEventos();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function() {
        $(AppSisConfiguracoes.objectId + ' .hover').hover(function(){
            $(this).addClass('style-primary');
        }, function(){
            $(this).removeClass('style-primary');
        });

        $(AppSisConfiguracoes.objectId + ' .row div div.hover').click(function() {
            var app = $(this).attr('id');
            var url = baseUrl+app;
            var destino = $('#content');
            var history = location.href.split('#')[0];
            var requestJS = $(this).attr('requestJS');

            window.materialadmin.AppNavigation.carregando($('.card-body'));
            
            // REMOVE TOOLTIPS ABERTOS
            $('.tooltip').hide();

            $.post(url, function(html, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    // RECARREGA FORMULÁRIO
                    destino.html(html);

                    // CARREGA SCRIPT DA PÁGINA
                    switch(app) {
                        case 'users/restrict':
                        case 'sisConfiguracoes/permissoes':
                            $.getScript(baseUrl+'js/sge/AppSisConfiguracoes.js');
                        break;
                        default:
                            if (typeof requestJS !== 'undefined'){
                                $.getScript(baseUrl+'js/source/'+requestJS+'.js');
                            }
                        break;
                    }

                    // ALTERA O ENDEREÇO DA BARRA DE FERRAMENTAS 
                    window.history.pushState({url: "" + history + ""}, '', baseUrl+app);
                }
            }, 'html');

            return false;
        });
        
        $(AppSisConfiguracoes.objectId+' #cadastrarFuncionalidade').off();
        $(AppSisConfiguracoes.objectId+' #cadastrarFuncionalidade').on('click',function() {
            // FECHA MODALS
            $('.modal').modal('hide');

            // CARREGA FUNCIONILIDADE
            p._loadConsRestrito();
        });

        $(AppSisConfiguracoes.objectId + ' #voltar').click(function() {
            // window.materialadmin.AppSisConfiguracoes.carregarCadastros();
        });

        $(AppSisConfiguracoes.objectId+' #addPermissoes').on('click',function() {
            p._loadConsPermissoes();
        });

        // CARREGA ADD FUNCIONALIDADES
        $(AppSisConfiguracoes.objectId+' #addFuncionalidades').on('click',function() {
            p._loadFormFuncionalidade();
        });

        // CARREGA LISTA DE USUARIOS
        $(AppSisConfiguracoes.objectId+' #listUsers').on('click',function() {
            p._loadConsUsersBasic();
        });

        // CARREGA EDIT FUNCIONALIDADES
        $(AppSisConfiguracoes.objectId+' .editFuncionalidades').on('click',function() {
            p._loadFormFuncionalidade($(this).attr('id'));
        });

        // CARREGA DELETE FUNCIONALIDADES
        $(AppSisConfiguracoes.objectId+' .delFuncionalidades').click(function() {
            if(confirm('Confirma a exclusao do registro ?')){
            let url = baseUrl + 'sisFuncionalidades/delete/'+$(this).attr('id');            
                window.materialadmin.AppGrid.delete(url, function(){
                     p._loadConsRestrito();
                });
            }
        });

        // CARREGA EDIT MÓDULOS
        $(AppSisConfiguracoes.objectId+' .editModulos').on('click',function() {
            p._loadFormModulo($(this).attr('id'));
        });

        // CARREGA LISTA MÓDULOS
        $(AppSisConfiguracoes.objectId+' #listModulos').on('click',function() {
            p._loadConsModulos();
        });

        // CARREGA LISTA MÓDULOS
        $(AppSisConfiguracoes.objectId+' #listGrupos').on('click',function() {
            p._loadConsGrupos();
        });

        // EXIBE O LOGIN
        $(AppSisConfiguracoes.objectId+' #lock').on('click',function() {
            p._loadLogin();
        });
    };

    p._loadConsRestrito = function() {
        // INSTANCIA VARIÁREIS
        var targetUrl = baseUrl + 'sisConfiguracoes/permissions';
        var gridObject = $('#AppSisConfiguracoes');

        window.materialadmin.AppNavigation.carregando(gridObject);

        $.get(targetUrl, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                gridObject.html(html);

                /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
                window.materialadmin.AppSisConfiguracoes.initialize();
            }
        }, 'html');
        
    };
    
    // =========================================================================
    // FUNÇÕES EXTERNAS
    // =========================================================================

    p.carregarCadastros = function() {
        var grid = $('#content section div.section-body div.tab-content, #content section div.section-body div.card');
        var app = 'sisConfiguracoes';
        var url = baseUrl+app;
        var destino = $('#content');
        var history = location.href.split('#')[0];

        grid.css('padding',0);
        grid.html('<div style="padding:24px"></div>');
        window.materialadmin.AppNavigation.carregando(grid.find('div'), 1);
        
        // REMOVE TOOLTIPS ABERTOS
        $('.tooltip').hide();

        $.post(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                destino.html(html);

                // CARREGA SCRIPT DA PÁGINA
                switch(app) {
                    case 'users/restrict':
                        $.getScript(baseUrl+'js/sge/AppSisConfiguracoes.js');
                    break;
                    default:
                        $.getScript(baseUrl+'js/sge/App'+ucfirst(app)+'.js');
                    break;
                }

                // ALTERA O ENDEREÇO DA BARRA DE FERRAMENTAS 
                window.history.pushState({url: "" + history + ""}, '', baseUrl+app);
            }
        }, 'html');
    };

    p._loadConsPermissoes = function() {
        // INSTANCIA VARIÁREIS
        var targetUrl = 'sisPermissoes/add';
        var modalObject = $('#nivel2');

        window.materialadmin.AppForm.loadModal(modalObject, targetUrl, '600px', function() {
            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                 p._loadConsRestrito();
            });
        });
    };

    p._loadConsModulos = function() {
        // INSTANCIA VARIÁREIS
        var targetUrl = 'sisModulos/index/1';
        var modalObject = $('#nivel2');

        window.materialadmin.AppForm.loadModal(modalObject, targetUrl, '60%', function() {
            window.materialadmin.App.load('AppSisModulos');
            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                 p._loadConsRestrito();
            });
        });
    };

    p._loadConsUsersBasic = function() {
        // INSTANCIA VARIÁREIS
        var targetUrl = 'users/list';
        var modalObject = $('#nivel2');

        window.materialadmin.AppForm.loadModal(modalObject, targetUrl, '75%', function() {
            
            window.materialadmin.App.load('AppUsers');
            window.materialadmin.AppUsers._habilitaBotoesConsultaConfiguracoes(modalObject);
            
            modalObject.find('#pesquisarUsers').submit(function() {
                
                window.materialadmin.AppUsers._loadListUsersConfiguracoes(modalObject);
                return false;
            });

            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                // p._loadConsRestrito();
            });
        });
    };

    p._loadConsGrupos = function() {
        // INSTANCIA VARIÁREIS
        var targetUrl = 'sisGrupos/index/1';
        var modalObject = $('#nivel2');

        window.materialadmin.AppForm.loadModal(modalObject, targetUrl, '60%', function() {
            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                // p._loadConsRestrito();
            });

            //window.materialadmin.App.initialize();
            window.materialadmin.AppVendor.initialize();
            window.materialadmin.AppSisGrupos.initialize();
        });
    };

    p._loadLogin = function() {
        // INSTANCIA VARIÁREIS
        var targetUrl = 'users/lock';
        var modalObject = $('#nivel2');

        window.materialadmin.AppForm.loadModal(modalObject, targetUrl, '400px', function() {
            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                // p._loadConsRestrito();
            });
        });
    };

    // =========================================================================
    // CARREGA FORMULÁRIOS
    // =========================================================================

    p._loadFormFuncionalidade = function(id, clonar) {
        // INSTANCIA VARIÁREIS
        var modalObject = $('#nivel2');
        var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        var targetUrl = (typeof id === 'undefined') ? 'sisFuncionalidades/add' : 'sisFuncionalidades/' + action + '/' + id;

        window.materialadmin.AppForm.loadModal(modalObject, targetUrl, '70%', function() {
            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                if (window.materialadmin.AppForm.getFormState()){
                    p._loadConsRestrito();
                }
            });
        });
    };

    p._loadFormModulo = function(id, clonar) {
        // INSTANCIA VARIÁREIS
        var modalObject = $('#nivel2');
        var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        var targetUrl = (typeof id === 'undefined') ? 'sisModulos/add' : 'sisModulos/' + action + '/' + id;

        window.materialadmin.AppForm.loadModal(modalObject, targetUrl, '70%', function() {
            /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                if (window.materialadmin.AppForm.getFormState()){
                    p._loadConsRestrito();
                }
            });
        });
    };

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppSisConfiguracoes = new AppSisConfiguracoes;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
