(function(namespace, $) {
    "use strict";

    var AppSisGrupos = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppSisGrupos.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppSisGrupos.objectId = '#AppSisGrupos';
    AppSisGrupos.modalFormId = '#nivel3';
    AppSisGrupos.controller = 'SisGrupos';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function() {
        $(AppSisGrupos.objectId+' #cadastrarSisGrupo').click(function() {
            p._loadFormSisGrupo();
        });

        $(AppSisGrupos.objectId+' #pesquisarSisGrupo').submit(function() {
            p._loadConsSisGrupo();
            return false;
        });
    };

    p._habilitaBotoesConsulta = function() {
        $(AppSisGrupos.objectId+' .btnClonar').click(function() {
            p._loadFormSisGrupo($(this).attr('id'), true);
        });

        $(AppSisGrupos.objectId+' .btnEditar' + AppSisGrupos.controller ).click(function() {
            p._loadFormSisGrupo($(this).attr('id'));
        });

        $(AppSisGrupos.objectId+' .btnDeletar' + AppSisGrupos.controller).click(function() {
            var url = baseUrl+'SisGrupos/delete/'+$(this).attr('id');
            window.materialadmin.AppGrid.delete(url, function(){
                p._loadConsSisGrupo();
            });
        });
    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsSisGrupo = function() {
        // INSTANCIA VARIÁREIS
        var form = $(AppSisGrupos.objectId+' #pesquisarSisGrupo');
        var table = $(AppSisGrupos.objectId+' #gridSisGrupos');
        var url = baseUrl + 'sisGrupos/';

        window.materialadmin.AppNavigation.carregando(table);

        $.post(url, form.serialize(), function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));

                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    // =========================================================================
    // CARREGA FORMULÁRIOS
    // =========================================================================

    p._loadFormSisGrupo = function(id, clonar) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppSisGrupos.modalFormId);

        var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        var url = (typeof id === 'undefined') ? baseURL + 'sisGrupos/add' : baseURL + 'sisGrupos/' + action + '/' + id;
        var i = 0;

        window.materialadmin.AppForm.loadModal(modalObject, url, '70%', function() {
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                if (window.materialadmin.AppForm.getFormState()){
                    p._loadConsSisGrupo();
                }
            });
        });
    };
    
    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppSisGrupos = new AppSisGrupos;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
