(function(namespace, $) {
    "use strict";

    var AppSisModulos = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppSisModulos.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppSisModulos.objectId = '#AppSisModulos';
    AppSisModulos.modalFormId = '#nivel4';
    AppSisModulos.controller = 'SisModulos';
    AppSisModulos.model = 'SisModulos';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {

        p._habilitaEventos();
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function() {
        $(AppSisModulos.objectId+' #cadastrarSisModulo').click(function() {
            p._loadFormModulo();
        });

        $(AppSisModulos.objectId+' #pesquisarSisModulo').submit(function() {
            p._loadConsModulo();
            return false;
        });
    };

    p._habilitaBotoesConsulta = function() {

        $(AppSisModulos.objectId+' .btnEditar' + AppSisModulos.controller).click(function() {
            p._loadFormModulo($(this).attr('id'));
        });

        $(AppSisModulos.objectId+' .btnDeletar' + AppSisModulos.controller).click(function() {
            var url = baseUrl+'sisModulos/delete/'+$(this).attr('id');
            window.materialadmin.AppGrid.delete(url, function(){
                p._loadConsModulo();
            });
        });
    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsModulo = function() {
        // INSTANCIA VARIÁREIS
        var form = $(AppSisModulos.objectId+' #pesquisarModulo');
        var table = $(AppSisModulos.objectId+' #gridSisModulos');
        var url = baseUrl + 'sisModulos/index/1';

        window.materialadmin.AppNavigation.carregando(table);

        $.post(url, form.serialize(), function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));

                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    // =========================================================================
    // CARREGA FORMULÁRIOS
    // =========================================================================

    p._loadFormModulo = function(id, clonar) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppSisModulos.modalFormId);
        var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        var url = (typeof id === 'undefined') ? 'sisModulos/add' : 'sisModulos/' + action + '/' + id;
        var i = 0;

        window.materialadmin.AppForm.loadModal(modalObject, url, '60%', function() {
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                if (window.materialadmin.AppForm.getFormState()){
                    p._loadConsModulo();
                }
            });
        });
    };
    
    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppSisModulos = new AppSisModulos;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
