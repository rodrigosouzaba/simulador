(function (namespace, $) {
    "use strict";

    var AppTabelas = function () {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function () {
            o.initialize();
        });
    };

    var p = AppTabelas.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppTabelas.objectId = '#AppTabelas';
    AppTabelas.modalFormId = '#nivel1';
    AppTabelas.controller = 'tabelas';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function () {

        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppTabelas.objectId));
        window.materialadmin.AppVendor.initialize();

        p._habilitaEventos();
        p._habilitaBotoesConsulta();
        p._habilitaPaginate()
    };

    p._habilitaMenu = function () {
        $("#AppPme .linkAjax").click(function (e) {
            e.preventDefault();
            var pagina = $('#selecaoTabelas');
            window.materialadmin.AppNavigation.carregando(pagina);
            window.materialadmin.AppNavigation.ajaxLoad($(this), pagina, () => window.materialadmin.AppTabelas._habilitaBotoes());
        });
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaBotoes = function () {
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
        p._habilitaPaginate();
    }

    p._habilitaEventos = function () {

        $(AppTabelas.objectId + ' #cadastrarTabela').click(function () {
            p._loadFormTabela();
        });

        $(AppTabelas.objectId + " .filtros").change(function () {

            let estados = $(AppTabelas.objectId + " #pesquisarTabela #estados");
            let operadoras = $(AppTabelas.objectId + " #pesquisarTabela #operadora-id");
            let comercializacoes = $(AppTabelas.objectId + " #pesquisarTabela #comercializacoes");

            let changer = $(this).attr('id');

            if (changer == 'estados') {
                if ((estados.val() != 'undefined' && estados.val() != '')) {
                    window.location.href = `${baseUrl}tabelas/new/${estados.val()}`;
                }
            }

            if (changer == 'operadora-id') {
                if ((estados.val() != 'undefined' && estados.val() != '') && (operadoras.val() != 'undefined' && operadoras.val() != '')) {
                    window.location.href = `${baseUrl}tabelas/new/${estados.val()}/${operadoras.val()}`;
                }
            }

            if (changer == 'comercializacoes') {
                if ((estados.val() != 'undefined' && estados.val() != '') && (operadoras.val() != 'undefined' && operadoras.val() != '') && (comercializacoes.val() != 'undefined' && comercializacoes.val() != '')) {
                    window.location.href = `${baseUrl}tabelas/new/${estados.val()}/${operadoras.val()}/${comercializacoes.val()}`;
                }
            }

        });


        $(AppTabelas.objectId + " #estados-old").change(function () {

            var value = $(this).val();
            if (value != '') {
                let url = baseURL + 'tabelas/filtroEstado'
                var element = $("#operadora-id-old");
                var data = { estado: value };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    dataType: "JSON",
                    beforeSend: function () {
                        element.prop("disabled", true).html("<option value=''>Carregando...</option>");
                    },
                    success: function (response) {
                        element.prop("disabled", true).empty();
                        element.append(`<option value="" selected="selected"> Selecione </option>`);
                        Object.entries(response).forEach(([key, value]) => {
                            element.append(`<option value="${key}">  ${value} </option>`);
                        });
                    },
                    error: function () {
                        toastr.error("Ocorreu um erro interno,tente novamente ou abra um chamado");
                    },
                    complete: function () {
                        element.prop("disabled", false);
                    }
                });
            }
            p._loadConsTabela();
        });

        $(AppTabelas.objectId + " #operadora-id-old").change(function () {
            p._loadConsTabela();
        });



        $(AppTabelas.objectId + " #deleteLote").click(function () {
            $.ajax({
                type: "post",
                data: $("#selecaoTabelas").serialize(),
                url: baseUrl + AppTabelas.controller + '/deleteLote',
                success: function (data) {
                    p._loadConsTabela();
                }
            });
        });

        $(AppTabelas.objectId + " #reajustarVigencia").click(function () {
            p._loadReajustarVigencia()
        });

        $(AppTabelas.objectId + " #gerarTabela").click(function () {
            $.ajax({
                type: "post",
                data: $('#selecaoTabelas').serialize(),
                url: baseUrl + AppTabelas.controller + '/gerarTabela',
                beforeSend: function () {
                    $('#loader').trigger('click');
                },
                success: function (data) {
                    $('#fechar').trigger('click');
                }
            });
        });

        $(AppTabelas.objectId + " #deleteLote").click(function () {
            if (confirm('Confirma exclusão?')) {
                $.ajax({
                    type: "post",
                    data: $("#selecaoTabelas").serialize(),
                    url: baseUrl + "produtos/deleteLote",
                    success: function (data) {
                        window.location = baseUrl + 'produtos';
                    }
                });
            }
        });



    };

    p._habilitaBotoesConsulta = function () {

        $(AppTabelas.objectId + ' .btnClonar').click(function () {
            p._loadFormTabela($(this).attr('id'), true);
        });

        $(AppTabelas.objectId + ' .btnEditar').click(function () {
            p._loadFormTabela($(this).attr('id'));
        });

        $(AppTabelas.objectId + ' .alterarPrioridade').click(function () {
            p._loadAlterarPrioridadePME($(this).attr("value"));
        });

        $(AppTabelas.objectId + ' .colocarAtlz').click(function () {
            if (confirm('Confirma atualização?')) {
                p._loadEmAtualizacao($(this).attr('value'), 'colocarAtlz');
            }
        });

        $(AppTabelas.objectId + ' .removerAtlz').click(function () {
            if (confirm('Confirma atualização?')) {
                p._loadEmAtualizacao($(this).attr('value'), 'removerAtlz');
            }
        });

        $(AppTabelas.objectId + ' .validar').click(function () {
            if (confirm('Confirma atualização?')) {
                p._loadValidar($(this).attr('value'), 'validar');
            }
        });

        $(AppTabelas.objectId + ' .invalidar').click(function () {
            if (confirm('Confirma atualização?')) {
                p._loadValidar($(this).attr('value'), 'invalidar');
            }
        });

    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsTabela = function () {
        // INSTANCIA VARIÁREIS
        var form = $(AppTabelas.objectId + ' #pesquisarTabela');
        var table = $(AppTabelas.objectId + ' #gridCalculo');
        if ($('#tablePmeOld').length > 0) {
            var url = baseUrl + AppTabelas.controller + '/index';
        } else {
            var url = baseUrl + AppTabelas.controller + '/new';
        }

        window.materialadmin.AppNavigation.carregando(table);
        $.post(url, form.serialize(), function (html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));
                // HABILITA OS BOTOES DA PAGINACAO PARA AJAX
                window.materialadmin.AppNavigation.ajaxPagination(table, form.serialize(), () => window.materialadmin.AppTabelas._habilitaBotoesConsulta());
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    // =========================================================================
    // CARREGA FORMULÁRIOS
    // =========================================================================

    p._loadFormTabela = function (id, clonar) {
        // CHAMA A FUNÇÃO MODAL
        // var modalObject = $(AppTabelas.modalFormId);

    };

    p._loadAlterarPrioridadePME = function (id) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppTabelas.modalFormId);
        var url = baseURL + AppTabelas.controller + '/prioridade/' + id;
        window.materialadmin.AppForm.loadModal(modalObject, url, '50%', function () {
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function () {
                if (window.materialadmin.AppForm.getFormState()) {
                    p._loadConsTabela();
                }
            });
        });
    };

    p._loadEmAtualizacao = function (id, metodo) {
        // CHAMA A FUNÇÃO MODAL
        var url = baseURL + AppTabelas.controller + '/' + metodo + '/' + id;
        $.ajax({
            type: "get",
            url: url,
            success: function (data) {
                if (data.ok) {
                    toastr.success(data.msg)
                } else {
                    toastr.error(data.msg)
                }
                p._loadConsTabela();
            }
        });
    };

    p._loadValidar = function (id, metodo) {
        // CHAMA A FUNÇÃO MODAL
        var url = baseURL + AppTabelas.controller + '/' + metodo + '/' + id;
        $.ajax({
            type: "get",
            url: url,
            success: function (data) {
                if (data.ok) {
                    toastr.success(data.msg)
                } else {
                    toastr.error(data.msg)
                }
                p._loadConsTabela();
            }
        });
    };

    p._loadReajustarLote = function () {
        // CHAMA A FUNÇÃO MODAL
        $.ajax({
            type: "post",
            data: $("#selecaoTabelas").serialize(),
            url: baseUrl + "tabelas/reajusteLote",
            success: function (data) {
                $("#percentual").empty();
                $("#percentual").append(data);

            }
        });
    };

    p._loadReajustarVigencia = function () {

        if ($('#selecaoTabelas').find(".checkHabilita:checked").length > 0) {
            var modalObject = $(AppTabelas.modalFormId);
            var url = baseURL + AppTabelas.controller + '/reajusteVigencia';
            window.materialadmin.AppForm.loadModal(modalObject, url, '50%', function () {
                modalObject.off('hide.bs.modal');
                modalObject.on('hide.bs.modal', function () {
                    if (window.materialadmin.AppForm.getFormState()) {
                        p._loadConsTabela();
                    }
                });

                $('#res').html('')
                var inputs = $('#selecaoTabelas').find(".checkHabilita:checked");
                for (var i = 0; i <= inputs.length - 1; i++) {
                    var input = `<input type="hidden" class='tabelas' name="tabelas" value="${$(inputs[i]).val()}">`;
                    $('#res').append(input);
                }
            });
        } else {
            toastr.warning('Selecione um registro', 'Atenção')
        }
    };

    p._habilitaPaginate = function () {
        window.materialadmin.AppNavigation.ajaxPagination(
            $(AppTabelas.objectId + ' #gridCalculo'),
            $(AppTabelas.objectId + ' #pesquisarTabela'),
            () => window.materialadmin.AppTabelas._habilitaBotoesConsulta()
        );
    };


    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppTabelas = new AppTabelas;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):
