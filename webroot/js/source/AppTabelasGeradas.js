(function(namespace, $) {
    "use strict";

    var AppTabelasGeradas = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppTabelasGeradas.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppTabelasGeradas.objectId = '#AppTabelasGeradas';
    AppTabelasGeradas.modalFormId = '#nivel1';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        // INICIALIZA DEPENDÊNCIAS
        window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppTabelasGeradas.objectId));
        window.materialadmin.AppVendor.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function() {
        $("#pf-tab,#gerador-tab").height($("#pme-tab").height());

        $("#pf-tab").click(function() {
            p._loadPfCalculos();
        });

        $("#pme-tab").click(function() {
            p._loadPJCalculos();
        });

        $("#gerador-tab").click(function() {
            p._loadTabelasGeradas();
        });

    };

    p._habilitaBotoesConsulta = function() { 

        $(AppTabelasGeradas.objectId + " .btnViewCalculo").click(function() {
            p._loadViewPfCalulo($(this).attr('calculo_id'))
        });
    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadPfCalculos = function() {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + 'odontoCalculos/add/PF';

        window.materialadmin.AppNavigation.carregando(pagina);

        $.get(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                pagina.html(html);
                // HABILITA BOTÕES DA CONSULTA
                // p._habilitaBotoesConsulta();
                
            }
        }, 'html');
    };

    p._loadPJCalculos = function() {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + 'odontoCalculos/add/PJ';

        window.materialadmin.AppNavigation.carregando(pagina);

        $.get(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                pagina.html(html);
                // HABILITA BOTÕES DA CONSULTA
                // p._habilitaBotoesConsulta();
                
            }
        }, 'html');
    };

    p._loadTabelasGeradas = function() {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl +  "tabelasGeradas/gerador";

        window.materialadmin.AppNavigation.carregando(pagina);

        $.get(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                pagina.html(html);
                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    
    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppTabelasGeradas = new AppTabelasGeradas;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):