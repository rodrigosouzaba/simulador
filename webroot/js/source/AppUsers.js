(function(namespace, $) {
    "use strict";

    var AppUsers = function() {
        // Create reference to this instance
        var o = this;

        // Initialize app when document is ready
        $(document).ready(function() {
            o.initialize();
        });
    };

    var p = AppUsers.prototype;

    // =========================================================================
    // CONFIG
    // =========================================================================

    AppUsers.objectId = '#AppUsers';
    AppUsers.modalFormId = '#nivel3';
    AppUsers.controller = 'users';
    AppUsers.model = 'Users';

    // =========================================================================
    // INIT
    // =========================================================================

    p.initialize = function() {
        // INICIALIZA DEPENDÊNCIAS
        //window.materialadmin.App.initialize();
        window.materialadmin.AppForm.initialize($(AppUsers.objectId));
        window.materialadmin.AppVendor.initialize();
        // window.materialadmin.Demo.initialize();

        // INIALIZA EVENTOS DA FUNCIONALIDADE
        p._habilitaEventos();
        p._habilitaBotoesConsulta();
    };

    // =========================================================================
    // EVENTS
    // =========================================================================

    p._habilitaEventos = function() {
        $(AppUsers.objectId + ' #cadastrarUser').click(function() {
            p._loadFormUser();
        });

        $(AppUsers.objectId + ' #pesquisarUser').submit(function() {
            p._loadConsUser();
            return false;
        });

        

    };

    p._habilitaBotoesConsulta = function() {

        $(AppUsers.objectId + ' .btnEditar' + AppUsers.controller).click(function() {
            p._loadFormUser($(this).attr('id'));
        });
        
        $(AppUsers.objectId + ' .btnDeletar' + AppUsers.controller).click(function() {
            var url = baseUrl + 'users/delete/' + $(this).attr('id');
            if (confirm('Confirma a exclusão do registro?')) {
                window.materialadmin.AppGrid.delete(url, function() {
                    p._loadConsUser();
                });
            }
        });
    };

    p._habilitaBotoesConsultaConfiguracoes = function(modalObj) {

        $('.btnEditarUsers').on('click',function() {
            var url2 = baseUrl + 'users/editBasic/' + $(this).attr('id') ;
            var modalObject2 = $('#nivel3');
            window.materialadmin.AppForm.loadModal(modalObject2, url2, '80%', function() {
                /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
                modalObject2.off('hide.bs.modal');
                modalObject2.on('hide.bs.modal', function() {
                     p._loadListUsersConfiguracoes();
                });
            });
        });

        
    };

    p._loadListUsersConfiguracoes = function(modalObj) {
        // INSTANCIA VARIÁREIS
        var targetUrl = baseUrl + 'users/list';
        var gridObject = $('#gridUsers');
        var form = $('#pesquisarUsers');
        
        window.materialadmin.AppNavigation.carregando(gridObject);

        $.post(targetUrl, form.serialize(), function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                gridObject.html($(html).find('#' + gridObject.attr('id') + ' >'));

                p._habilitaBotoesConsultaConfiguracoes(modalObj);
            }
        }, 'html');
    };

    // =========================================================================
    // CARREGA CONSULTA 
    // =========================================================================

    p._loadConsUser = function() {
        // INSTANCIA VARIÁREIS
        var form = $(AppUsers.objectId + ' #pesquisarUser');
        var table = $(AppUsers.objectId + ' #gridUser');
        var url = baseUrl + 'users/index';

        window.materialadmin.AppNavigation.carregando(table);

        $.post(url, form.serialize(), function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                table.html($(html).find('#' + table.attr('id') + ' >'));

                // HABILITA BOTÕES DA CONSULTA
                p._habilitaBotoesConsulta();
            }
        }, 'html');
    };

    p._loadPfCalculos = function() {
        // INSTANCIA VARIÁREIS
        var pagina = $('#respostaAjax');
        var url = baseUrl + "pfCalculos/add";

        window.materialadmin.AppNavigation.carregando(pagina);

        pagina.load(url, function(html, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                // RECARREGA FORMULÁRIO
                pagina.html(html);

                // HABILITA BOTÕES DA CONSULTA
                $('.btnVisiualizarCalculo').click(function () {
                    p._loadViewPfCalculos($(this).attr('id'));
                })
            }
        }, 'html');

    };

    p._loadViewPfCalculos = function (id) {
        var targetUrl = baseUrl + 'pfCalculos/view/' + id;
        var modalObject = $('#nivel1');

        window.materialadmin.AppForm.loadModal(modalObject, targetUrl, '90%', function() {
            
            window.materialadmin.App.load('AppCalculos');
            window.materialadmin.AppCalculos._habilitaBotoesConsultaConfiguracoes(modalObject);
            
            // modalObject.find('#pesquisarUsers').submit(function() {
                
            //     window.materialadmin.AppUsers._loadListUsersConfiguracoes(modalObject);
            //     return false;
            // });

            // /* SCRIPTS QUE SERÃO EXECUTADOS QUANDO PÁGINA FOR CARREGADA */
            // modalObject.off('hide.bs.modal');
            // modalObject.on('hide.bs.modal', function() {
            //     // p._loadConsRestrito();
            // });
        });
    }

    // =========================================================================
    // CARREGA FORMULÁRIOS
    // =========================================================================

    p._loadFormUser = function(id, clonar) {
        // CHAMA A FUNÇÃO MODAL
        var modalObject = $(AppUsers.modalFormId);
        var action = (typeof clonar !== 'undefined') ? 'add' : 'edit';
        var url = (typeof id === 'undefined') ? 'users/add' : 'users/' + action + '/' + id;
        var i = 0;

        window.materialadmin.AppForm.loadModal(modalObject, url, '70%', function() {
            modalObject.off('hide.bs.modal');
            modalObject.on('hide.bs.modal', function() {
                if (window.materialadmin.AppForm.getFormState()) {
                    p._loadConsUser();
                }
            });

            $('#UserAlterar').change(function() {
                if ($(this).val() == 1) {
                    $('#UserPassword2').removeAttr('readonly');
                    $('label[for=UserPassword2]').html('Nova Senha<span style="color:red;">*</span>');
                } else {
                    $('#UserPassword2').attr('readonly', 'true');
                    $('label[for=UserPassword2]').html('Nova Senha');
                    $('#UserPassword2').val('');
                }
            });
        });
    };

    
    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.materialadmin.AppUsers = new AppUsers;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):